allowedCharsUsuari='A��BCDE��FGHI��JKLMNO��PQRSTU��VWXYZa��bcde��fghi��jklmno��pqrstu��vwxyz0123456789-';
allowedCharsPasw='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

function resetCampsLogin()
{
	if(actiu1709)
	{	
		document.getElementById('i_pasw').style.color='#000000';
		document.getElementById('i_pasw').value='';
		document.getElementById('i_usuari').style.color='#000000';
		document.getElementById('i_usuari').value='';
		actiu1709=0;
	}
	return;
}

function checkLoginF()
{
	
	isValid=false;
	missatgeAlerta='';

	value=document.getElementById('i_usuari').value;
	if(value.length==0)
   	{
		missatgeAlerta+="\nCal introduir un nom d'usuari";			
    }
	else if(value.length<4)
   	{
		missatgeAlerta+="\nEl nom d'usuari ha de tenir m�s de quatre caracters";			
    }
	else	
	{
   		allowedChars=allowedCharsUsuari;
		$_ok=0;
   		for(i=0;i<value.length;i++)
      	{
           	for(j=0;j<allowedChars.length;j++)
           	{
               	if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
           	}
		}
       	if($_ok<value.length)
       	{
			missatgeAlerta+="\nS'han introduit caracters no permesos\n\n(caracters permesos: "+allowedCharsUsuari;			
       	}
	}
	
	value=document.getElementById('i_pasw').value;
	if(value.length==0)
   	{
		missatgeAlerta+="\nCal introduir una contrasenya";			

		return false;
   	}
	
		
	if (missatgeAlerta.length==0)
	{
		return true;
	}
    
	return false;
}	

//*v36.2-funcio
function editarCondicionsPeriode()
{
	visb=document.getElementById('t_editarCondicionsPeriode').style.visibility;
	if(visb=='hidden')
	{
		document.getElementById('t_editarCondicionsPeriode').style.visibility='inherit';
		document.getElementById('t_editarCondicionsPeriode').style.position='relative';
	}
	else
	{
		document.getElementById('t_editarCondicionsPeriode').style.visibility='hidden';
		document.getElementById('t_editarCondicionsPeriode').style.position='absolute';
	}
	
	return;
}

//*v36.2-funcio
function guardarCondicions()
{
	guardarCondicionsText=document.getElementById('ta_guardarCondicions').value;
	enviarFpars('index.php?sR='+ruta+'&gC='+guardarCondicionsText,'_self');
	
	return;
}


function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='index.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}

