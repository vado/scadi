<?php

//------------------------------------------------------------------------------
function db_getReservesZona($zona,$db)
{
	global  $mPars,
			$mProductes,
			$mRebostsRef,
			$mReservesComandes,
			$jaEntregatTkg,
			$jaEntregatTuts,
			$mRecepcions,
			$sz,
			$mParametres,
			$mPerfilsRef,
			$mGrupsZones,
			$mProductesJaEntregats;
	
	$mReservesProductes=array();
	$puntsEntregaZona='';
	$grupsPuntsEntregaZona='';
	
	if($zona!='')
	{
		//get grups dins zona
		if(!$result=mysql_query("select ref from rebosts_".$mPars['selRutaSufix']." WHERE LOCATE('".$zona.",',categoria)>0",$db))
		{
			//echo "<br> 17 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		}
		else
		{ 
			$i=0;
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$puntsEntregaZona.=','.$mRow['ref'].',';
			}
		}
	}
	else
	{
		$zona='totes';
		//get grups dins zona
		if(!$result=mysql_query("select ref from rebosts_".$mPars['selRutaSufix'],$db))
		{
			//echo "<br> 34 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		}
		else
		{ 
			$i=0;
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$puntsEntregaZona.=','.$mRow['ref'].',';
			}
		}
	}
	$puntsEntregaZona=str_replace(',,',',',$puntsEntregaZona);
	
	//get grups amb punts entrega en els grups anteriors
	if(!$result=mysql_query("select rebost,punt_entrega from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',punt_entrega,','),CONCAT(' ','".$puntsEntregaZona."'))>0",$db))
	{
			//echo "<br> 54 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			$grupsPuntsEntregaZona.=','.$grupId.',';
			$mComandesGrups[$grupId]['punt_entrega']=$mRow['punt_entrega'];
		}
	}
	$grupsPuntsEntregaZona=str_replace(',,',',',$grupsPuntsEntregaZona);
	
	//getComandes dels usuaris dels grups dels puntsentrega dins zona
	$do=true;
	
	if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',SUBSTRING_INDEX(rebost,'-',1),','),CONCAT(' ','".$grupsPuntsEntregaZona."'))>0 AND usuari_id!=0",$db))
	{
			//echo "<br> 70 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
					$do=true;
			
			if(!isset($mReservesComandes[$zona]))
			{
				$mReservesComandes[$zona]=array();
			}
			if(!isset($mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']]))
			{
				$mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']]=array();
			}
			if(!isset($mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]))
			{
				$mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]=array();
			}
			$mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]['resum']=$mRow['resum'];

			if($do)				
			{
				$mComanda=explode(';',$mRow['resum']);
				while(list($key,$mVal)=each($mComanda))
				{
					$mIndexQuantitat=explode(':',$mVal);
					
					$index=str_replace('producte_','',$mIndexQuantitat[0]);
					$do2=true;
					if($sz!='')
					{
						if($index!='')
						{
							$productorId=substr($mProductes[$index]['productor'],0,strpos($mProductes[$index]['productor'],'-'));

							$superZonaProductor=getSuperZona($mPerfilsRef[$productorId]['zona']);
							if($superZonaProductor==$sz)
							{
								$do2=true;
							}
							else
							{
								$do2=false;
							}
						}
						else
						{
							$do2=false;
						}
					}
					else
					{
						$do2=true;
					}
					
					
					if($do2)
					{
						$quantitat=@$mIndexQuantitat[1];
						if($index!='')
						{
							if(array_key_exists($index,$mReservesProductes))
							{
								$mReservesProductes[$index]+=$quantitat;
							}
							else
							{
								$mReservesProductes[$index]=$quantitat;
							}
						}
					}
				}
			}
		}
	}
	if($mPars['excloureProductesJaEntregats']=='1')
	{
		while(list($index,$mVal)=each($mProductesJaEntregats))
		{
			while(list($grupId,$mVal2)=each($mVal))
			{
						//echo "<br>".$mRebostsRef[$grupId]['nom']." (".$mRebostsRef[$grupId]['categoria'].") ".$mProductes[$index]['producte']."-".$mVal2['rebut'];
				if(substr_count($grupsPuntsEntregaZona,','.$grupId.',')>0)
				{
					if($index!='' && isset($mReservesProductes[$index]))
					{
						$mReservesProductes[$index]-=$mVal2['rebut'];
						$jaEntregatTkg+=$mVal2['rebut']*$mProductes[$index]['pes'];
						$jaEntregatTuts+=$mVal2['rebut'];
					}
				}
			}
		}
		reset($mProductesJaEntregats);
	}
	//vd($jaEntregatTkg);
	//incorporar reserves a productes
	
	while(list($index,$mProducte)=each($mProductes))
	{
		if(isset($mReservesProductes[$index]))
		{
			$mProductes[$index]['quantitat']=$mReservesProductes[$index];
		}
		else
		{
			$mProductes[$index]['quantitat']=0;
		}
	}
	reset($mProductes);


	return;
}


//------------------------------------------------------------------------------
function db_getComandesDesti($db)
{
	global 	$mPars,
			$mProductes,
			$mRebostsRef,
			$mReservesComandesDesti,
			$jaEntregatTkg,
			$jaEntregatTuts,
			$mRecepcions,
			$mParametres,
			$mPerfilsRef,
			$mGrupsZones,
			$mPuntsEntrega;

	$where=' WHERE ';
	$mysqlChain="";
	$zona='';
	$superZona='';
	
	if(isset($mPars['szDesti']) && $mPars['szDesti']!='')
	{
		$superZona=$mPars['szDesti'];
		$mysqlChain.=' (';
		while(list($key,$zona)=each($mGrupsZones[$mPars['szDesti']]))
		{
			$mysqlChain.=" LOCATE('".$zona.",',categoria)>0 OR ";
		}
		reset($mGrupsZones[$mPars['szDesti']]);
		$mysqlChain=substr($mysqlChain,0,strlen($mysqlChain)-3).')';
	
	}
	else if(isset($mPars['zDesti']) && $mPars['zDesti']!='')
	{
		$superZona=getSuperZona($mPars['zDesti']);
		$zona=$mPars['zDesti'];
		$mysqlChain.=' ';
		while(list($key,$zona_)=each($mGrupsZones[$superZona]))
		{
			if($zona_==$mPars['zDesti'])
			{
				$mysqlChain.=" LOCATE('".$zona_.",',categoria)>0 ";
			}
		}
		reset($mGrupsZones[$superZona]);
	}
	else if(isset($mPars['grupDesti']) && $mPars['grupDesti']!='')
	{
		$zona=db_getZonaGrup($mPars['grupDesti'],$db);
		$superZona=getSuperZona($zona);
		$mysqlChain.=' ';
		while(list($pe,$mPuntEntrega)=each($mPuntsEntrega))
		{
			if($pe==$mPars['grupDesti'])
			{
				$mysqlChain.=" id='".$pe."' ";
			}
		}
		reset($mPuntsEntrega);
	}
	else
	{
		$where='';
		$mysqlChain.="";
	}
						
	$mReservesProductes=array();
	$puntsEntrega='';
	$grupsPuntsEntrega='';
	
	//get punts entrega
	//echo "<br>"."select DISTINCT(ref) from rebosts_".$mPars['selRutaSufix']." ".$where." ".$mysqlChain."";
	if(!$result=mysql_query("select DISTINCT(id) from rebosts_".$mPars['selRutaSufix']." ".$where." ".$mysqlChain."",$db))
	{
		//echo "<br> 17 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			$puntsEntrega.=','.$mRow['0'].',';
		}
	}
	//get comandes dels grups amb punt d'entrega seleccionats amb punts entrega en els grups anteriors
	//echo "<br>select rebost,punt_entrega ".$mPars['taulaComandes']." where LOCATE(CONCAT(',',punt_entrega,','),CONCAT(' ','".$puntsEntrega."'))>0";
	if(!$result=mysql_query("select rebost,punt_entrega from ".$mPars['taulaComandes']." where LOCATE(CONCAT(',',punt_entrega,','),CONCAT(' ','".$puntsEntrega."'))>0",$db))
	{
		//echo "<br> 54 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			$grupsPuntsEntrega.=','.$grupId.',';
			$mComandesGrups[$grupId]['punt_entrega']=$mRow['punt_entrega'];
		}
	}
	$grupsPuntsEntrega=str_replace(',,',',',$grupsPuntsEntrega);
	//getComandes dels usuaris dels grups dels puntsentrega dins zona
	
	//echo "select * from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',SUBSTRING_INDEX(rebost,'-',1),','),CONCAT(' ','".$grupsPuntsEntrega."'))>0 AND usuari_id!=0";
	if(!$result=mysql_query("select * from ".$mPars['taulaComandes']." where LOCATE(CONCAT(',',SUBSTRING_INDEX(rebost,'-',1),','),CONCAT(' ','".$grupsPuntsEntrega."'))>0 AND usuari_id!=0",$db))
	{
		//echo "<br> 70 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			$puntEntregaId=db_getPuntEntrega($grupId,$db);


			$zona_=db_getZonaGrup($grupId,$db);
			$superZona_=getSuperZona($zona_);

			if
			(
				(
					(
						(
							$mPars['rc']=='1'
							&&
							$mRecepcions[$grupId]['acceptada']=='1'
						)
						||
						$mPars['rc']=='-1'
					)
					&&
					$grupId!='62'
					&&
					$grupId!='0' && $grupId!=0
					&&
					(
						(
							@$mPars['szDesti']=='' 
							&&
							@$mPars['zDesti']=='' 
							&&
							@$mPars['grupDesti']=='' 
						)
						|| 
						(
							@$mPars['szDesti']!='' 
							&&
							@$mPars['szDesti']==$superZona_
						)
						||
						(
							@$mPars['zDesti']!='' 
							&&
							@$mPars['zDesti']==$zona_
						)
						||
						(
							@$mPars['grupDesti']!='' 
							&&
							@$mPars['grupDesti']==$puntEntregaId
						)
					)
				)
			)
			{

				
				$mComandesGrups[$grupId]['punt_entrega']==$puntEntregaId;
				
				if($superZona_!='' && !isset($mReservesComandesDesti[$superZona_]))
				{
					$mReservesComandesDesti[$superZona_]=array();
				}
				
				if($zona_!='' && !isset($mReservesComandesDesti[$superZona_][$zona_]))
				{
					$mReservesComandesDesti[$superZona_][$zona_]=array();
				}
				
				if($mComandesGrups[$grupId]['punt_entrega']!='' && !isset($mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId]))
				{
					$mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId]=array();
				}
				
				if($grupId!='' &&   !isset($mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId][$grupId]))
				{
					$mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId][$grupId]=array();
				}
				if($mRow['usuari_id']!='' && !isset($mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]))
				{
					$mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]=array();
				}
				$mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]=$mRow;

				$mComanda=explode(';',$mRow['resum']);
				while(list($key,$mVal)=each($mComanda))
				{
					$mIndexQuantitat=explode(':',$mVal);
					
					$index=str_replace('producte_','',$mIndexQuantitat[0]);
					$quantitat=@$mIndexQuantitat[1];
					if
					(
						$index!='' 
					)
					{
						if(array_key_exists($index,$mReservesProductes))
						{
							$mReservesProductes[$index]+=$quantitat;
						}
						else
						{
							$mReservesProductes[$index]=$quantitat;
						}
				
						if(!isset($mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]['pesC']))
						{
							$mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]['pesC']=0;
						}
						@$mReservesComandesDesti[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]['pesC']+=$quantitat*$mProductes[$index]['pes'];
						
					}
				}
				reset($mComanda);
			}
			
		}
	}
	ksort($mReservesComandesDesti);
	

	if($mPars['excloureProductesJaEntregats']=='1')
	{
		$mProductesJaEntregats=db_getProductesJaEntregats2($db);
		
		while(list($index,$mProductesJaEntregatsPEs)=each($mProductesJaEntregats))
		{
			while(list($puntEntrega,$mProductesJaEntregatsPE)=each($mProductesJaEntregatsPEs))
			{
				while(list($grupId,$mProductesJaEntregatsGrup)=each($mProductesJaEntregatsPE))
				{
					while(list($incId,$mVal3)=each($mProductesJaEntregatsGrup))
					{
						//echo "<br>".$mRebostsRef[$grupId]['nom']." (".$mRebostsRef[$grupId]['categoria'].") ".$mProductes[$index]['producte']."-".$mVal2['rebut'];
						$zona_=db_getZonaGrup($puntEntrega,$db);
						$superZona_=getSuperZona($zona_);
				
						if(substr_count($grupsPuntsEntrega,','.$puntEntrega.',')>0)
						{
							if
							(
								$index!='' 
								&& 
								isset($mReservesProductes[$index])
								&&
								(
									
									(
										(!isset($mPars['szDesti']) || @$mPars['szDesti']=='')
										&&
										(!isset($mPars['zDesti']) || @$mPars['zDesti']=='')
										&&
										(!isset($mPars['grupDesti']) || @$mPars['grupDesti']=='')
									)
									||
									(
										@$mPars['szDesti']!='' 
										&&
										@$mPars['szDesti']==$superZona_
									)
									||
									(
										@$mPars['zDesti']!='' 
										&&
										@$mPars['zDesti']==$zona_
									)
									||
									(
										@$mPars['grupDesti']!='' 
										&&
										@$mPars['grupDesti']==$grupId
									)
								)
							)
							{
								if(isset($mProductes[$index]))
								{
									//$mReservesProductes[$index]-=$mVal3['rebut'];
									$jaEntregatTkg+=$mVal3['rebut']*$mProductes[$index]['pes'];
									$jaEntregatTuts+=$mVal3['rebut'];
								}
							}
						}
					}
					reset($mProductesJaEntregatsGrup);
				}
				reset($mProductesJaEntregatsPE);
			}
			reset($mProductesJaEntregatsPEs);
		}
		reset($mProductesJaEntregats);
	}
	//incorporar reserves a productes
	
	while(list($index,$mProducte)=each($mProductes))
	{
		if(isset($mReservesProductes[$index]))
		{
			$mProductes[$index]['quantitat']=$mReservesProductes[$index];
		}
		else
		{
			$mProductes[$index]['quantitat']=0;
		}
	}
	reset($mProductes);
	
	
	return;
}

//------------------------------------------------------------------------------
function db_getComandesOrigen($db)
{
	global 	$mPars,
			$mProductesOrigen,
			$mRebostsRef,
			$mReservesComandesOrigen,
			$jaEntregatTkg,
			$jaEntregatTuts,
			$mRecepcions,
			$mParametres,
			$mPerfilsRef,
			$mGrupsZones,
			$mPuntsEntrega,
			$mProductesJaEntregats;

	$where=' WHERE ';
	$mysqlChain="";
	$zona='';
	$superZona='';
	
	if(isset($mPars['szOrigen']) && $mPars['szOrigen']!='')
	{
		$superZona=$mPars['szOrigen'];
		$mysqlChain.=' (';
		while(list($key,$zona)=each($mGrupsZones[$mPars['szOrigen']]))
		{
			$mysqlChain.=" LOCATE('".$zona.",',categoria)>0 OR ";
		}
		reset($mGrupsZones[$mPars['szOrigen']]);
		$mysqlChain=substr($mysqlChain,0,strlen($mysqlChain)-3).')';
	
	}
	else if(isset($mPars['zOrigen']) && $mPars['zOrigen']!='')
	{
		$superZona=getSuperZona($mPars['zOrigen']);
		$zona=$mPars['zOrigen'];
		$mysqlChain.=' ';
		while(list($key,$zona_)=each($mGrupsZones[$superZona]))
		{
			if($zona_==$mPars['zOrigen'])
			{
				$mysqlChain.=" LOCATE('".$zona_.",',categoria)>0 ";
			}
		}
		reset($mGrupsZones[$superZona]);
	}
	else if(isset($mPars['grupOrigen']) && $mPars['grupOrigen']!='')
	{
		$zona=db_getZonaGrup($mPars['grupOrigen'],$db);
		$superZona=getSuperZona($zona);
		$mysqlChain.=' ';
		while(list($pe,$mPuntEntrega)=each($mPuntsEntrega))
		{
			if($pe==$mPars['grupOrigen'])
			{
				$mysqlChain.=" id='".$pe."' ";
			}
		}
		reset($mPuntsEntrega);
	}
	else
	{
		$where='';
		$mysqlChain.="";
	}
						
	$mReservesProductes=array();
	$puntsEntrega='';
	$grupsPuntsEntrega='';
	
	//get punts entrega
	//echo "<br>"."select DISTINCT(ref) from rebosts_".$mPars['selRutaSufix']." ".$where." ".$mysqlChain."";
	if(!$result=mysql_query("select DISTINCT(ref) from rebosts_".$mPars['selRutaSufix']." ".$where." ".$mysqlChain."",$db))
	{
		//echo "<br> 17 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			$puntsEntrega.=','.$mRow[0].',';
		}
	}
	//get comandes dels grups amb punt d'entrega seleccionats amb punts entrega en els grups anteriors
	if(!$result=mysql_query("select rebost,punt_entrega from ".$mPars['taulaComandes']." where LOCATE(CONCAT(',',punt_entrega,','),CONCAT(' ','".$puntsEntrega."'))>0",$db))
	{
		//echo "<br> 54 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			$grupsPuntsEntrega.=','.$grupId.',';
			$mComandesGrups[$grupId]['punt_entrega']=$mRow['punt_entrega'];
		}
	}
	$grupsPuntsEntrega=str_replace(',,',',',$grupsPuntsEntrega);
	//getComandes dels usuaris dels grups dels puntsentrega dins zona
	$do=true;
	

	if(!$result=mysql_query("select * from ".$mPars['taulaComandes']." where LOCATE(CONCAT(',',SUBSTRING_INDEX(rebost,'-',1),','),CONCAT(' ','".$grupsPuntsEntrega."'))>0 AND usuari_id!=0",$db))
	{
		//echo "<br> 70 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			$puntEntregaId=db_getPuntEntrega($grupId,$db);


			$zona_=db_getZonaGrup($grupId,$db);
			$superZona_=getSuperZona($zona_);
			if
			(
				(
					$grupId!='62'
					&&
					(
						(
							$mPars['szOrigen']=='' 
							&&
							$mPars['zOrigen']=='' 
							&&
							$mPars['grupOrigen']=='' 
						)
						|| 
						(
							$mPars['szOrigen']!='' 
							&&
							$mPars['szOrigen']==$superZona_
						)
						||
						(
							$mPars['zOrigen']!='' 
							&&
							$mPars['zOrigen']==$zona_
						)
						||
						(
							$mPars['grupOrigen']!='' 
							&&
							$mPars['grupOrigen']==$puntEntregaId
						)
					)
				)
			)
			{

				$do=true;
				
				$mComandesGrups[$grupId]['punt_entrega']==$puntEntregaId;
				
				if($superZona_!='' && !isset($mReservesComandesOrigen[$superZona_]))
				{
					$mReservesComandesOrigen[$superZona_]=array();
				}
				
				if($zona_!='' && !isset($mReservesComandesOrigen[$superZona_][$zona_]))
				{
					$mReservesComandesOrigen[$superZona_][$zona_]=array();
				}
				
				if($mComandesGrups[$grupId]['punt_entrega']!='' && !isset($mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId]))
				{
					$mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId]=array();
				}
				
				if($grupId!='' &&   !isset($mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId][$grupId]))
				{
					$mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId][$grupId]=array();
				}
				if($mRow['usuari_id']!='' && !isset($mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]))
				{
					$mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]=array();
				}
				$mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]=$mRow;
				if($do)				
				{
					$mComanda=explode(';',$mRow['resum']);
					while(list($key,$mVal)=each($mComanda))
					{
						$mIndexQuantitat=explode(':',$mVal);
						
						$index=str_replace('producte_','',$mIndexQuantitat[0]);
							$quantitat=@$mIndexQuantitat[1];
							if
							(
								$index!='' 
							)
							{
								if(array_key_exists($index,$mReservesProductes))
								{
									$mReservesProductes[$index]+=$quantitat;
								}
								else
								{
									$mReservesProductes[$index]=$quantitat;
								}
				
								if(!isset($mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]['pesC']))
								{
									$mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]['pesC']=0;
								}
								@$mReservesComandesOrigen[$superZona_][$zona_][$puntEntregaId][$grupId][$mRow['usuari_id']]['pesC']+=$quantitat*$mProductesOrigen[$index]['pes'];
							}
					}
				}
				reset($mComanda);
			}
		}
	}
	ksort($mReservesComandesOrigen);
	if($mPars['excloureProductesJaEntregats']=='1')
	{
		$mProductesJaEntregats=db_getProductesJaEntregats2($db);
		while(list($index,$mProductesJaEntregatsPEs)=each($mProductesJaEntregats))
		{
			while(list($puntEntrega,$mProductesJaEntregatsPE)=each($mProductesJaEntregatsPEs))
			{
				while(list($grupId,$mProductesJaEntregatsGrup)=each($mProductesJaEntregatsPE))
				{
					while(list($incId,$mVal3)=each($mProductesJaEntregatsGrup))
					{
						//echo "<br>".$mRebostsRef[$grupId]['nom']." (".$mRebostsRef[$grupId]['categoria'].") ".$mProductes[$index]['producte']."-".$mVal2['rebut'];
						$zona_=db_getZonaGrup($puntEntrega,$db);
						$superZona_=getSuperZona($zona_);
				
						if(substr_count($grupsPuntsEntrega,','.$puntEntrega.',')>0)
						{
							if
							(
								$index!='' 
								&& 
								isset($mReservesProductes[$index])
								&&
								(
									(
										$mPars['szOrigen']=='' 
										&&
										$mPars['zOrigen']=='' 
										&&
										$mPars['grupOrigen']=='' 
									)
									||
									(
										$mPars['szOrigen']!='' 
										&&
										$mPars['szOrigen']==$superZona_
									)
									||
									(
										$mPars['zOrigen']!='' 
										&&
										$mPars['zOrigen']==$zona_
									)
									||
									(
										$mPars['grupOrigen']!='' 
										&&
										$mPars['grupOrigen']==$grupId
									)
								)
							)
							{
								//$mReservesProductes[$index]-=$mVal3['rebut'];
								@$jaEntregatTkg+=$mVal3['rebut']*$mProductesOrigen[$index]['pes'];
								$jaEntregatTuts+=$mVal3['rebut'];
							}
						}
					}
					reset($mProductesJaEntregatsGrup);
				}
				reset($mProductesJaEntregatsPE);
			}
			reset($mProductesJaEntregatsPEs);
		}
		reset($mProductesJaEntregats);
	}
	//incorporar reserves a productes
	
	while(list($index,$mProducte)=each($mProductesOrigen))
	{
		if(isset($mReservesProductes[$index]))
		{
			$mProductesOrigen[$index]['quantitat']=$mReservesProductes[$index];
		}
		else
		{
			$mProductesOrigen[$index]['quantitat']=0;
		}
	}
	reset($mProductesOrigen);
	return;
}

//------------------------------------------------------------------------------
function confirmarRecepcions($cadenaRecepcionsAConfirmar,$grupId,$db)
{
	global $mPars,$mMissatgeAlerta;
	
	
	//echo "<br>"."update rebosts_".$mPars['selRutaSufix']." set recepcions=CONCAT(recepcions,',".$cadenaRecepcionsAConfirmar."') where id='".$grupId."'";
	if(!$result=mysql_query("update rebosts_".$mPars['selRutaSufix']." set recepcions=CONCAT(recepcions,',".$cadenaRecepcionsAConfirmar."') where id='".$grupId."'",$db))
	{
		//echo "<br> 154 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		
		$mMissatgeAlerta['missatge1'].="<p class='pAlertaNo4'>Atenci�, no s'ha pogut acceptar la recepci� del grup seleccionat. Posa't en contacte amb l'administrador</p>";
		$mMissatgeAlerta['result1']=false;
	}
	else
	{ 
		mail_recepcioAcceptada($cadenaRecepcionsAConfirmar,$grupId,$db);
	}

	return $mMissatgeAlerta;
}
//------------------------------------------------------------------------------
function db_getReservesCSV($db)
{
	global $mPars,$mGrupsRef,$mReservesCsv,$mReservesComandesDesti,$jaEntregatTkg,$destiText,$origenText;
	
	$dir0=getcwd();
	$dir="docs".$mPars['selRutaSufix'];
	if(!@chdir($dir))
	{
		mkdir($dir);
	}
	else
	{
		if(!chdir('reserves')){mkdir('reserves');}
		chdir($dir0);
	}
	@unlink("docs".$mPars['selRutaSufix']."/reserves/reserves_".$mPars['usuari_id'].".csv");

	if(!$fp=fopen("docs".$mPars['selRutaSufix']."/reserves/reserves_".$mPars['usuari_id'].".csv",'a'))
	{
		return false;
	}


	if($origenText=='')
	{
		$mH1=array('vista reserves de : '.$destiText);
		fputcsv($fp, $mH1,',','"');
		$mH1=array('');
		fputcsv($fp, $mH1,',','"');
	}
	else
	{
		$mH1=array('llista de productes que cal portar de '.(strtoupper($origenText)).' a '.(strtoupper($destiText)));
		fputcsv($fp, $mH1,',','"');
		$mH1=array('');
		fputcsv($fp, $mH1,',','"');
	}
	
	$mH1=array(date('d-m-Y H:i'));
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	
	//info comandes incloses en aquesta entrega
	$mH1=array('COMANDES INCLOSES:');
	fputcsv($fp, $mH1,',','"');
	$mH1=array('Superzona','Zona','Punt Entrega','GRUP','Kg comanda');
	fputcsv($fp, $mH1,',','"');

	while(list($sz,$mSz)=each($mReservesComandesDesti))
	{
		while(list($z,$mZ)=each($mSz))
		{
			while(list($pe,$mPe)=each($mZ))
			{
				while(list($grupId,$mG)=each($mPe))
				{
					$pesC=0;
					while(list($usuariId,$mU)=each($mG))
					{
						$pesC+=@$mU['pesC'];
					}
					reset($mG);
					$mH1=array($sz,$z,urldecode($mGrupsRef[$pe]['nom']),urldecode($mGrupsRef[$grupId]['nom']),number_format($pesC,2,',',''));
					fputcsv($fp, $mH1,',','"');
				}
				reset($mPe);
			}
			reset($mZ);
		}
		reset($mSz);
	}
	reset($mReservesComandesDesti);
	
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');


	if($mPars['excloureProductesJaEntregats']==1)
	{
		$mH1=array("* Mostrar els productes ja entregats a ".$destiText." (".$jaEntregatTkg." kg): NO");
		fputcsv($fp, $mH1,',','"');
	}
	else
	{
		$mH1=array("* Mostrar els productes ja entregats a ".$destiText." (".$jaEntregatTkg." kg): SI");
		fputcsv($fp, $mH1,',','"');
	}


	if($mPars['elp']==1)
	{
		$mH1=array("* Mostrar els productes que no cal portar perqu� en algun magatzem de ".$destiText." hi ha estoc positiu suficient: NO");
		fputcsv($fp, $mH1,',','"');
	}
	else
	{
		$mH1=array("* Mostrar els productes que no cal portar perqu� en algun magatzem de ".$destiText."l hi ha estoc positiu suficient: SI");
		fputcsv($fp, $mH1,',','"');
	}
	/*
	if($sz==''){$sz='(cap superZona seleccionada)';}
	$mH1=array("* Mostrar nom�s els productes dels productors gestionats desde la superzona seleccionada: ".$sz);
	fputcsv($fp, $mH1,',','"');
	*/
	
	
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');

	while(list($key,$mReservaCsv)=each($mReservesCsv))
	{
    	if(!fputcsv($fp, $mReservaCsv,',','"'))
		{
			return false;
		}
	}	
	reset($mReservesCsv);
	fclose($fp);
	return;
}

//*v36-28-11-15 funcio
//------------------------------------------------------------------------------
function db_infoFullRutaDistribucioCSV($db)
{
	global $mPars,$mInfoRutaDistribCsv,$destiText,$jaEntregatTkg;
	$dir0=getcwd();
	$dir="docs".$mPars['selRutaSufix'];
	if(!@chdir($dir))
	{
		mkdir($dir);
	}
	else
	{
		if(!chdir('reserves')){mkdir('reserves');}
		chdir($dir0);
	}
	@unlink("docs".$mPars['selRutaSufix']."/reserves/infoFullRutaDistribucio_".$mPars['usuari_id'].".csv");

	if(!$fp=fopen("docs".$mPars['selRutaSufix']."/reserves/infoFullRutaDistribucio_".$mPars['usuari_id'].".csv",'a'))
	{
		return false;
	}


	$mH1=array((date('d-m-Y H:i')));
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	
	//info comandes incloses en aquesta entrega
	$mH1=array($mPars['selRutaSufix'].' INFO FULL RUTA DISTRIBUCIO:');
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');

	while(list($key,$mInfoRutaDistribCsv_)=each($mInfoRutaDistribCsv))
	{
		fputcsv($fp,$mInfoRutaDistribCsv_,',','"');
	}
	reset($mInfoRutaDistribCsv);
	
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');


	if($mPars['excloureProductesJaEntregats']==1)
	{
		$mH1=array("* Mostrar els productes ja entregats a ".$destiText." (".$jaEntregatTkg." kg): NO");
		fputcsv($fp, $mH1,',','"');
	}
	else
	{
		$mH1=array("* Mostrar els productes ja entregats a ".$destiText." (".$jaEntregatTkg." kg): SI");
		fputcsv($fp, $mH1,',','"');
	}


	if($mPars['elp']==1)
	{
		$mH1=array("* Mostrar els productes que no cal portar perqu� en algun magatzem de ".$destiText." hi ha estoc positiu suficient: NO");
		fputcsv($fp, $mH1,',','"');
	}
	else
	{
		$mH1=array("* Mostrar els productes que no cal portar perqu� en algun magatzem de ".$destiText."l hi ha estoc positiu suficient: SI");
		fputcsv($fp, $mH1,',','"');
	}
	/*
	if($sz==''){$sz='(cap superZona seleccionada)';}
	$mH1=array("* Mostrar nom�s els productes dels productors gestionats desde la superzona seleccionada: ".$sz);
	fputcsv($fp, $mH1,',','"');
	*/
	
	
	fclose($fp);
	return;
}
?>

		