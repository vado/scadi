allowedCharsEmail='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.@';
allowedCharsCompteEcos_lletres='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
emailNeededCharacters='@.';
allowedCharsEnter='0123456789';
allowedCharsFloat='0123456789.';
allowedCharsCategories='A��BCDE��FGHI��JKLMNO��PQRSTU��VWXYZa��bcde��fghi��jklmno��pqrstu��vwxyz0123456789- ';
allowedCharsRuta="A��BCDE��FGHI��JKLMNO��PQRSTU��VWXYZa��bcde��fghi��jklmno��pqrstu��vwxyz0123456789-':, ()/";

function hihaCaractersNoPermesos(caracters,cadena,camp)
{
   	allowedChars=caracters;
	missatgeAlerta1='';
	$_ok=0;
	
	//check for prohibited characters
    for(i=0;i<cadena.length;i++)
    {
       for(j=0;j<allowedChars.length;j++)
       {
           if(cadena.charAt(i)==allowedChars.charAt(j)){$_ok++;}
       }
	}
	if($_ok<cadena.length-1)
    {
		missatgeAlerta1="\nEl camp "+camp+" cont� un valor incorrecte \n\n(caracters permesos: "+allowedChars+")";			
    }
	
	return missatgeAlerta1;
}
		
function checkFormGuardarTrams()
{
	isValid=false;
	missatgeAlerta='';
	value=document.getElementById('i_capacitat_pes').value;
	capacitat_pes=value;
	if(value*1>0)
	{
		unitatsContractadesPes=document.getElementById('i_unitatsContractadesPes').value;
		if(value*1<unitatsContractadesPes*1)
		{
			missatgeAlerta+="\nEl camp 'capacitat pes' ha de tenir un valor igual o major a la capacitat de pes contractada";			
		}
		if(value*1>capacitat_pes_max*1)
		{
			missatgeAlerta+="\nEl camp 'capacitat pes' ha de tenir un valor igual o menor a la capacitat de pes del vehicle";			
		}
	}	
	missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsEnter,value,'capacitat_pes');
	
	
	value=document.getElementById('i_capacitat_volum').value;
	capacitat_volum=value;
	if(value*1>0)
	{
		unitatsContractadesVolum=document.getElementById('i_unitatsContractadesVolum').value;
		if(value*1<unitatsContractadesVolum*1)
		{
			missatgeAlerta+="\nEl camp 'capacitat volum' ha de tenir un valor igual o major a la capacitat de volum contractada";			
		}
		if(value*1>capacitat_volum_max*1)
		{
			missatgeAlerta+="\nEl camp 'capacitat volum' ha de tenir un valor igual o menor a la capacitat de volum del vehicle";			
		}
	}
	missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsEnter,value,'capacitat_volum');
	
	
	
	value=document.getElementById('i_capacitat_places').value;
	capacitat_places=value;
	if(value*1>0)
	{
		unitatsContractadesPlaces=document.getElementById('i_unitatsContractadesPlaces').value;
		if(value*1<unitatsContractadesPlaces*1)
		{
			missatgeAlerta+="\nEl camp 'capacitat places' ha de tenir un valor igual o major a la capacitat de places contractada";			
		}
		if(value*1>capacitat_places_max*1)
		{
			missatgeAlerta+="\nEl camp 'capacitat places' ha de tenir un valor igual o menor a la capacitat de places del vehicle";			
		}
	}
	missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsEnter,value,'capacitat_places');

	
	value=document.getElementById('i_preu_pes').value;
	value2=document.getElementById('i_capacitat_pes').value;
	if(value2*1>0 && (value*1<=0 || value*1==''))
	{
		missatgeAlerta+="\nEl camp 'preu_pes' ha de contenir un valor positiu";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'preu_pes');
	}

	value=document.getElementById('i_preu_volum').value;
	value2=document.getElementById('i_capacitat_volum').value;
	if(value2*1>0 && (value*1<=0 || value*1==''))
	{
		missatgeAlerta+="\nEl camp 'preu_volum' ha de contenir un valor positiu";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'preu_volum');
	}

	value=document.getElementById('i_preu_places').value;
	value2=document.getElementById('i_capacitat_places').value;
	if(value2*1>0 && (value*1<=0 || value*1==''))
	{
		missatgeAlerta+="\nEl camp 'preu_places' ha de contenir un valor positiu";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'preu_places');
	}
	
	value=document.getElementById('i_pc_ms').value;
	if(value.length==0 || value*1<50)
	{
		missatgeAlerta+="\nEl camp '% ms' ha de contenir un valor igual o major que 50";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'pc_ms');
	}

	if(capacitat_pes*1==0 && capacitat_places*1==0 && capacitat_volum*1==0)
	{
		missatgeAlerta+="\nEl tram no es pot crear si no s'ofereix cap recurs (pes, volum o places)";			
	}
	
		
	if (missatgeAlerta.length==0)
	{
		return true;
	}

	alert(missatgeAlerta);
	
  return false;
}	

function guardarTram()
{
	document.getElementById('f_guardarTram').submit();
	return;
}

function guardarNouTram()
{
	document.getElementById('i_opcio').value='crear';
	document.getElementById('f_guardarTram').submit();
	return;
}

function eliminarTram(tramId)
{
	document.getElementById('i_opcio').value='eliminar';
	document.getElementById('f_guardarTram').submit();

	return;
}

function anularContractes(tramId)
{
	document.getElementById('i_opcio').value='anularContractes';
	document.getElementById('f_guardarTram').submit();

	return;
}

function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
		
	return;
}

		