6-12-14
gestio productes
	taula productes 1412: nou camp 'propietats' (text)
	creat: gestioProductes.php
	creat: html_gestioProductes.php
	creat: db_gestioProductes.php
	creat: js_gestioProductes.js
	
	
	pendent actualitzar resta de rutes
	


19-11-14
reunio cac d'ahir:

productes:
- edicio condicional pels productors
- mailing a grups
	*** revisar (mostrar a responsables si els usuaris han fet comandes en altres grups o nom�s en el seu?)
	
		modificat:
		db.php
		html.php
		mail.php
		html_mail.php
		js_mail.js (?)	
		
		
20-11-14
modificat:
	mail.php
nou - html_mail.php

FET - edicio de l'oferta de productes dip�sit
20-11-14:
modificat i pujat:
	html_productes.php
	db.php
	nou js_html_productes.js
	comandes.php
	html.php
	css1.css
	**********>>>>>>>>>>>>>>>>>  pendent modificar comanda cac de forma segura despr�s que el productor canvii l'estoc previst

comptes:
- completar amb %ms productores (afegir columna abastiment i produccio i quadrar totals al final)
15-11-14: deduir l'ingres per marges cac als preus (distribucio-abastiment-cost transport-%fons despeses cac

inventari:
- transferencia de productes: sistema de validaci� per part del receptor, visible a l'emisor
- historial registre inventari: que apareixi el nom del producte

- trams ruta: que cadascu entri els trams, afegir sistema validacio epr admin a partir d'acord en assemblea cac

- form interrebost a p�gina comandes

mail
- avisar usuaris automatic, opcional: obertura precomandes, comunicats cac, altres
- llista de mails dels membres dels grups accessibles pels responsables de grup
10-11-14: missatge d'avis si l'usuari nou utilitza un correu ja utilitzat

ap (16-11-14)
- decisio ap 16-11-14, aplicacio 5% opcional de marge sobre el preu de venda a usuaris per a fons pels grups
- enviar mail a responsables de grup eprqu� facin arribar consulta de la cac a l'assemblea, amb les opcinions aportades durant l'ap, per tal de saber qu� prefereixen per aplicar aquest marge. cal enviarlo primer a coordinacio per consensuar contingut


15-11-14

FET no apareix l'inventari total a pagina comandes CAC
modificat:
gestioMagatzems.php
db_gestioMagatzems.php
comandes.php

*** login: no mira el nom d'usuari????

FET -forma pagament eu transf usuaris a grup
modificat:

- html.php
- js1.js
	
	14-11-14
	
	>> altes d'usuaris, caracters, form control
	
	FET:inventari total en veure inventari d'un magatzem
		modificat:
		gestioMagatzems.php
		db_gestioMagatzems.php
		html_gestioMagatzems.php
		db.php
	
	FET abonaments i carrecs:
		modificat:
		incidencies.php
		db_incidencies.php
		vistaAlbara.php
		html_vistaAlbara.php
		db.php
		html.php
		comandes.php
		comptes.php
	
	10-11-14
	
	FET-pre-llista de productes
	- revisar les comandes que es mostren en rutes anteriors (mar�, es veu comanda d'octubre de vad� i aina ?)
	FET - forma de pagament (pere anton)
	FET - modificat 'js1.js' i 'html.php'
	
	5-4-14
	**** substituir ftp per pujar arxiu php, js, html)
	
	FET - afegir columna inventari total, quan es visualitza la d'un rebost especific
	- revisar tancament ruta> iniciar amb estoc_previst=0 i estoc disponible=0
	- problemes per crear grup i perfil i assignar nou usuari responsable grup
	FET - problemes per guardar el grup
	
	23-10-14 formulari. nou perfil de productor
			 - modificada taula productors: 
			 		- afegit camp grup_vinculat (final)
	
					- modificat camp adre�a a adressa
					- modificat camp nom a usuari_id (int 10)
					- modificat camp municipi a municipi_id (int 10)
	
					- eliminats camps cognoms, email i mobil
					- eliminat camp 'comarca'
					- eliminat camp 'telefon'
	
			FET aplicar de 1403 a 1411:
	ALTER TABLE `productors_1411`
	DROP `cognoms`,
	DROP `email`,
	DROP `mobil`,
	DROP `comarca`,
	DROP `telefon`,
	CHANGE `adre�a` `adressa` VARCHAR( 300 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
	CHANGE `nom` `usuari_id` INT(10) NOT NULL ,
	CHANGE `municipi` `municipi_id` INT(10) NOT NULL ,
	ADD `descripcio` mediumtext AFTER `municipi_id`,
	ADD `avals` text AFTER `descripcio`,
	ADD `grup_vinculat` INT(10) COMMENT '�nic grup vinculat per balan� consum-producci�' AFTER `avals`,
	ADD `estat` BOOL  AFTER `grup_vinculat`,
	ADD `acord1` FLOAT  AFTER `estat`
	
					
			 - modificada taula rebosts:
			 
			FET aplicar de 1403 a 1411:
	ALTER TABLE `rebosts_1403`
	DROP `conectat`,
	DROP `contrassenya`,
	ADD `c_ecos` FLOAT NOT NULL COMMENT 'consum en ecos dins ruta actual' AFTER `mobil` ,
	ADD `c_eb` FLOAT NOT NULL COMMENT 'consum en ecobasics dins ruta actual' AFTER `c_ecos` ,
	ADD `c_eu` FLOAT NOT NULL COMMENT 'consum en euros dins ruta actual' AFTER `c_eb`,
	ADD `p_ecos` FLOAT NOT NULL COMMENT 'produccio en ecos dins ruta actual' AFTER `c_eu` ,
	ADD `p_eb` FLOAT NOT NULL COMMENT 'produccio en ecobasics dins ruta actual' AFTER `p_ecos` ,
	ADD `p_eu` FLOAT NOT NULL COMMENT 'produccio en euros dins ruta actual' AFTER `p_eb`,
	ADD `s_ecos` FLOAT NOT NULL COMMENT 'saldo acumulat en ecos' AFTER `p_eu` ,
	ADD `s_eb` FLOAT NOT NULL COMMENT 'saldo acumulat en ecobasics' AFTER `s_ecos` ,
	ADD `s_eu` FLOAT NOT NULL COMMENT 'saldo acumulat en euros' AFTER `s_eb`
	
			- modificat db.php, l:2400
				insert a rebosts, afegits nous camps (9)
			- modificat db_distribucioGrups.php l:87 (afegida)
	
	//establir vincles entre perfils i grups a 1411, i aplicar de 1403 a 1410:
	UPDATE `productors_1410` INNER JOIN `productors_1411` ON `productors_1410`.id = `productors_1411`.id
	SET `productors_1410`.grup_vinculat = `productors_1411`.grup_vinculat
	UPDATE `productors_1410` INNER JOIN `productors_1411` ON `productors_1410`.id = `productors_1411`.id
	SET `productors_1410`.acord1 = `productors_1411`.acord1
	
	16-10-14 data object, agafar de parametres ruta
	
	10-10-14
	29_dev	- FET: TOTAL RESERVAT ZONES: ERRONI
			Forma de pagament:
			- FET: no es guarda el punt d'entrega en guardar desde usuari si grup no ha fet comanda
			- *** generar avis si el grup no ha guardat opcions de pagament i punt d'entrega (comandes,resum imports)
			- grup: get UsuarisAmb Comanda
			- millorar form pagament (inexactitut imports suma grups i grup)
			- FET: revisar si es guarda opcio pagament IntegralCES
			- FET: que surti per defecte la forma de pagament del grup ja guardada en la ruta anterior, si nhi ha
			- FET: que surti per defecte la forma de pagament de l0usuari ja guardada en la ruta anterior, si nhi ha
	
			- coord pot guardar forma pagament de grup i d'usuaris (?)
			- *** els coordinadors no poden editar el seu perfil d'usuari: que nom�s pugui editar el seu perfil, com la resta d'usuaris
	
			- afegir opcio integralces a resum comandes (imports)
			FET - afegir num abonaments/c�rrecs a resum comandes (imports)
			- afegir num peticions a resum comandes per imports
			
			- *** ? que el responsable de grup pugui treure un membre del grup
			
	**************** assegurar comandes perque no s'interfereixin comandes simultanees:
					- bloqueig comandes usuaris mentre te lloc una comanda cac
					- assegurar no interferencia de comandes simultanees d'usuaris
					
					
	5-10-14
	29_dev
	(testing: acces coord-carlest)
	- incidencies
	FET - resum comandes imports
	FET - actualitzar info a full de ruta acces 
	- cara magatzem
	- vista albara: els globals a $mUsuarisRef s'han de canviar a matriu suma d'usuaris grup + usuaris team
	
	2-9-14	del usuari: admin solicita baixa dels grups de l'usuari als responsables de grup
		nom�s es pot borrar si no pertany a cap grup i per tant no te comanda en cap grup
			
	
	29-09-14
	28_dev	ACTUALITZAR DADES de online: comandes i fp setembre
	
	29-09-14
	28_dev	editargrup: eliminar contrasenya del form de nouGrup
			FET - editargrup: substituir camp responsable de grup per selector d'usuaris
			FET - editargrup: dades responsable grup -> dades grup
			editargrup: multiselect de responsable grup
			FET - comandes: reconeixer responsable de grup
			editargrup: per responsable grup
			FET vistaalbara: bbdd, especificar autor a aplicar abonaments i carrecs (taula incidencies).
			FET usuari: solicitar acces a grup (o canvi), a pag menu editar usuari
			FET responsable grup: gestionar usuaris pendents
			coord: limitar drets guardar i llegir a usuaris i grups
			coord: limitar drets guardar comandes i forma pagament a grups i usuaris
			coord: limitar drets guardar inventaris a grups
			coord: limitar drets aplicar/desaplicar + editar/resoldre incidencies grups (?) abonaments i carrecs  a grups
			coord: ampliar drets lectura grups
			coord, errors acces incidencies
			coord, errors acces resum comandes imports i zones
			coord, errors acces resum productes especials
			*** extensi� assoc-usuari_id<=>comanda a taules no 09
			*** revisio operacions inventaris
			*** integrar totes les dades actuals quadrar totes les addes amb nova versi�
			BLOQUEJAR activitat a usuaris
	
	17-09-14
	28_dev  carpetes noves: historial/usuaris
			logfiles per historial canvis usuaris		
	
			
	15-09-14
	28-treb FET pagina magatzem, 'que hi ha als magatzems': nom�s surt vilafranca
	28-treb FET en guardar comanda (cac) no es mostra l'inventari (valors a zero). Fins que es refresca
	28-treb FET no surten totes les reserves a 'que han demanat els altres rebosts' (juliol, agost)
	28-treb	FET tampoc fa els sumatoris be, errors tant si un productor com si tots (juliol, agost)
	
	14-9-14
	28-dev  FET compactar vista resum comandes
	28-dev  FET(revisar) qu� han demanat els altres rebosts, pagina independent
	28-dev  sistema d'usuaris
			FET - taula usuaris: modificada
			FET - taula rebosts_9: modificats camps
			FET - telefon passa a  productors_associats
			FET - taules comandes_9 i comandes_seg_9, afegit camp usuari_id
			FET - taula comandesespecials_9 afegit camp usuari_id
			FET - taula incidencies_9 afegit camp usuari_id, i sel_usuari_id
			FET - taula rebosts_09, afegit camp usuari_id
			Modificades celine, comandes iluro, terrassa, equitativa, olot, vilafranca i GE		treb 28 online-> passar a 14 local
			Modificats trams ruta treb 28 online, passar a 14 local
			
	
	11-9-14
	*** estat: totes les funcions bloquejades (per manteniment)
	
	*** form nou producte (cac), propostes de productes dels grups (la cac aprova). 
	*** edicio de productes pels grups (els seus)
	
	*** html: error navegacio: entre rutes i entre index i distribucioComandes , si us navegacio navegador no agafa pag distribucio sino index
	FET html - eliminar recordatori guardar comanda a visitant, si precomandes obert i seleccio de producte
	*** exportar proposta comandes
	FET pagina index: veure columna inventari inventari al canto de la quantitat
	FET p�gina magatzem, marcar en gris les linies amb estoc=0;
	FET revisar operacions proc�s tancarcomandes (productes diposit, dvd dreceres)
	
	*** pujar imatge productor
	
	FET	pujar taules setembre i agost, excepte inventaris
	
	*** editar productes: eliminar imatge
	*** paginaci� (+ guardar comanda...)
	*** toni: comandes a proveidors separades internament de l'oferta. 
		se sumen a l'estoc previst procedent de l'estoc disponible de l'�ltima ruta 
		el responsable de fer l'inventari ha de confirmarles quan arriba el producte, realitzant l'entrada
		operaci� carregar inventari a estoc previst , on estoc previst = inventari + comandes cac a proveidors pendents d'entrega
	
	
	10-09-14
	
	*** form alta nou producte, per grup. Admin: posar etiqueta cac als productes que van pujant els grups
	*** problema guardar comanda si tots els productes i seleccio productor - online
	FET i boto guardar comanda
	
	6-9-14
	FET revisar error fer estoc_previst=1 a productes dip�sit ( si nova ruta, si no es troba $mParametres)
	FET    que els grups accedeixin a les rutes anteriors
	FET	gestio incidencies: corregir selector de grup
			opcio veure productes actius i no actius
	FET	incloure tipus 'solicitarAbonament' a incidencies
	
	5-9-14
	FET operacio automatica de modificacio d'inventari (enlla� autom�tic de les rutes): 
			estoc gestor:
			- estoc disponible->estoc previst
			- les reserves es registren als inventaris com a sortida distribuci� i es resta de l'inventari, excepte productes dip�sit i especials
	
	3-9-14
	FET *** online - canviar incidencies 'eliminada' a 'resolta'
	FET	*** taula incidencies, canviar tipus id_producte, demanat i rebut, de int a char 20, totes les taules
	FET	*** passar canvis online
	FET	**** operacions d'abonament sobre vistes albara
	
	FET	**** tester pagina incidencies: comprovar que nom�s admin pot resoldre incidencies en estat 'eliminada'
	FET	**** error inventari: pagina magatzem, no es mostra estoc inventari productes no oferits i amb estoc positiu
	FET:**** separar vista albara usuari cac de taula d'incidencies; pagina apart amb recerca flexible i acces a totes les incidencies; les operacions d'abonament son incidencies resoltes d'una certa forma. podrien enregistrarse amb referencia al productor a la taula d'incidencies
	
	24-8-14
	FET:	capo: error link productes especials
	FET:	canviar taula parametres: (online)
	FET:	afegir parametre compte bank a totes les rutes (online)
	FET:	canviar taula comandes: camp forma_pagament -> char 300 (totes les rutes) (online)
	FET	**** toni: resum de comandes de ruta	
	
		**** pagina trams ruta, incorporar despeses cac personals de transport, i fer balan�os despeses/ingressos
	FET: imprimir albara: surt taula grisa
	FET: inventari: operacions entrada, productes amb estoc previst 0
	FET: nou inventari: nom�s admin
		**** login, si usuari!='', nom�s t� en compte el passw i accedeix a usuari corresponent a aquest passw
		
	FET: link a comandes desde resum comanda
		
	FET:	afegir zonasud2 web
	FET:	afegir categoria magatzem a r. tarragona
	FET:	modificar categoria zona r. tarragona a zona sud
	FET:	comprovar usuari rebost tarragona
			preparar 'vista albara' de comandes de zona
	FET:	reparar forms ti, te, teio
	FET: online: copia arxius i modificar taules trams_ruta
	 
	FET: LOCAL Afegir camp 'data_hora' a trams_ruta i modificar codi
	FET: visibilitzar full de ruta a usuaris magatzem
	
	
	18-08-14
	FET **** entrada inventaris: recuperacio i abastiment - partir de llista de productes sencera, i no de l'inventari
	FET **** error guardar inventari usuari:martina
	
	14-08-14
	FET: taula parametres, no s'actualitza en guardar parametre
	FET: revisar condicions forms IO
	
	*** mostra vistaAlbaraMagatzem.php (per a entrades cac a magatzems -> proposta m. AS a CC i AP)
																			- tasca: persona AS que controli entrades CAC i distribucio local a grups cac zonaCentral1
																			- sessio gestor a responsables rebost AS
	***>>> toni: separar introduccio noves comandes cac a proveidors de l'estoc existent
	
	13-08-14
	FET: aplicar 5% nom�s a rutes a partir de juliol (no incl�s)
	
	12-08-14
	*** inventaris-transferencies de producte: el producte desti no ha de tenir estoc positiu (pot no estar en l'inventari)
	FET: pag magatzem agafa 'veure nomes productes disponibles' de pag comandes, pero aquesta opcio esta desabilitada. Habilitar?
	FET: pag magatzem: els productes en diposit, marcar amb color suau casella inventari, i no vermell, encara que DifD<0
	
	07-08-14
	FET:introduir inventari per cada magatzem, com si fos comanda
	
	28-07-14
	FET:resum ruta segons zones punts entrega i no segons zona rebosts amb comanda
	FET:distribucio magatzem segons zones punts entrega i no segons zona rebosts amb comanda
	FET:accessibles online rutes juliol a mar�, nom�s admin
	
	23-07-14
	- integrar usuaris per a stocs magatzem cac a diferents punts
	- IMPORTANT: Ordre de prioritats
	FET	- creaci�/edicio usuaris/grups desde usuari CAC
		- acces usuaris/grups a perfil usuari: canvi/recuperaci� contrasenya (email)
		- els grups de consum, les comissions i els productors passen a ser usuaris d'un rebost
		- versionar un gestor de comandes com el de la cac per cada grup, i mantenir-los 'enxarxats'
	
	
	19-07-14
	FET - mostrar info productes - no arriven mPars
	--------------------------------------------------------------------------------
	18-7-14
	dev juliol4
	FET - seleccio de taules - historic
	canvis:
	FET - index - error eval, ok
	--------------------------------------------------------------------------------
	15-3-14
	- revisar navegadors
	
	- completar vista albar� amb info intercanvis rebost
	- form admin per introduir servei interrebost
	1/2 FET - opcions pagament + intercanvis rebosts a pagina productes a l'esquerra a dalt
	1/2- vista resum ruta
	--- llistar serveis interrebost desde 'resumDeRuta.php'
	--- forma de pagament % ms , eb, euro -> passar info a vistalbar�
	--- passar funcions implicades en us taula municipis a taula municipis2, eliminar taula municipis, renombrar taula municipis2 a municipis
	- generar gestor d'usuaris i permisos
	- vincular dss a gestor comandes i usar el seu sistema de login i usuaris
	- iniciar funcions email
	FET - fix: no surt PE-AureaSocial a selector puntsEntrega (html.php, 276)
		- * no es pot seleccionar '', cas rvc
	
	FET - productes especials: revisar info demanen rebosts, import i % en ums: no ha d'incloure preu productes especials - perqu� no esta en estoc
	
	- incidencies: introduir incidencies productes demanats que no hi ha
		
	**** - pagina info: revisar visibilitat info segons login
	**** - sincronitzar temps servidor amb temps gestor
	**** - diversificacio estocs-magatzems
	**** - comptes d'usuaris dins els grups i integracio a comandes grup
	**** - edicio condicional info de grup, i productes cac del grup
	
	
	FET - colocar avis d'activacio autom�tica de la limitacio de l'estoc a estoc positiu
	FET --modt. estructura taula rebosts, sense efectes: afegit camp 'ref' (rebost)
	FET --modt. estruct taula comandes i comandes_seg (dues vegades)
	FET- vado: el que demanen els altres rebosts nom�s ha de ser visibles als rebosts
	FET - formulari: forma de pagament, opcio-> passar info a vistalbar�
	FET - punt d'entrega (nom�s rebosts actius) -> passar info a vistalbar�
	- completar fitxes productors
	1/2 FET- basar cerca fitxa productor en id i no en nom projecte
	FET - admin pugui pujar fotos productes
	FET - modificar html menu vista albara, etc... a firefox
	FET - modificar boto login perqu� permeti accedir prement 'enter'
	FET - corregir error td_missatges
	FET - corregir de nou problema caixa info productes
	FET - bloquejar reserves sobre estoc negatiu als 4 dies de que es tanqui el periode de precomandes
	FET - desapareixen els productes especials -> 'actiu' es posa  a cero. Es en fer comanda CAC? ok
	FET- visualitzar fitxa productor (i gc i altres usuaris...)
	FET- crear usuari visites per permetre usuaris individuals acc�s info productes
	FET- completar vista albar� amb data reserva 
	FET- dates reserva rebosts, detallar data revisions i productes
	FET - vista albara - ordenar segons ordre pagina llista productes
	
	--------------------
	
	13-01-13> integraci� ulleres graduades:
	
	- camp 'tipus': valor->'pb, especial, nou'
	- els productes amb tipus=  pb, o especial, o nou, colocats a dalt taula, remarcats amb color tipus
	PENDENT- linkar cada producte especial a la seva p�gina de formulari
	FET - crear taula reserves productes_especials1. Basada en taula 'comandes' + 10 camps comodi. 
	FET - creat productesEspecials1.php; cont� info i form per a reserva de productes especials en general
	FET - crear js1_2.js : javascript per controlar caracters a formulari
	FET - c0: codi reserva per l'usuari que fa la comanda, per entregar a �ptica

- camps form:

c: Rebost (no editable)
receptor del producte:
c: nom
c: cognoms
c: mobil
c: email
c: codi ecoxarxa
productes:
pack 1


- taula 'productes', camp 'notes rebosts'> cont� info producte (condicions)

FET - pagina de reserva, imprimible, amb el codi. Es el mateix form + missatge alerta despr�s de comanda amb �xit











--------------------

1- 'disponibles'->'actius' (amb estoc disponible>0)
lin 177, html.php
2- limit selector quantitat*3; passa de constant (500) a variable: '$limitq=estoc_disponible*3
lin 1107 html.php
3- variable limitq:
lin 1142, 1235 (es deixa fixe en el cas de CAC)



