allowedCharsMobil='0123456789';
allowedCharsCompteEcos_lletres='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';



function selectPag(numPag)
{
	if(calGuardar)
	{
		alert(calGuardarText);	
		calGuardar=false;
	}
	else
	{	
		calGuardar=false;
		document.getElementById('i_pagS').value=numPag;

		enviarFpars('contractesTrams.php?tip='+vTipus,'_self');
	}
	
	return;
}


function avansarPag(increment)
{
	if(calGuardar)
	{
		alert(calGuardarText);	
		calGuardar=false;
	}
	else
	{	
		calGuardar=false;

		nouPagS=pagS+increment;

		valor261214=document.getElementById('i_itemsPag').value;
	
		if((nouPagS>=0 && nouPagS<numPags) || numItemsPag!=valor261214)
		{
			document.getElementById('i_pagS').value=nouPagS;
			enviarFpars('contractesTrams.php?tip='+vTipus,'_self');
		}
	}
	return;
}


function saveItemsPag(itemsPag)
{
	document.getElementById('i_itemsPag').value=itemsPag;
	
	return;
}

quantitatOld=0;
quantitat=0;


function selectVehicle(assocIndex)
{
	// nom�s DEMANDES
	vehicleId=document.getElementById('sel_vehicle_'+assocIndex).value;
	mTrams[assocIndex]['vehicle']=vehicleId;
	if(vehicleId!='')
	{
		if(mTrams[assocIndex]['quantitat_pes']!=0 || mTrams[assocIndex]['quantitat_volum']!=0 || mTrams[assocIndex]['quantitat_places']!=0)
		{
			document.getElementById('td_tramvehicle_id_'+assocIndex).style.backgroundColor='#DCFBCE';
			recordatoriGuardarContractes();
		}
		else
		{
			document.getElementById('sel_quantitat_pes_'+assocIndex).style.backgroundColor='#FD1402';
			document.getElementById('sel_quantitat_volum_'+assocIndex).style.backgroundColor='#FD1402';
			document.getElementById('sel_quantitat_places_'+assocIndex).style.backgroundColor='#FD1402';
			document.getElementById('sel_quantitat_pes_'+assocIndex).style.color='#ffffff';
			document.getElementById('sel_quantitat_volum_'+assocIndex).style.color='#ffffff';
			document.getElementById('sel_quantitat_places_'+assocIndex).style.color='#ffffff';
		}
	}
	else
	{
		document.getElementById('td_tramvehicle_id_'+assocIndex).style.backgroundColor='#ffffff';
	}


	return;
}

function selectTram(assocIndex,parametre)
{
	quantitatOld=1*mTrams[assocIndex]['quantitat_'+parametre];
	quantitat=document.getElementById('sel_quantitat_'+parametre+'_'+assocIndex).value;
	quantitat=quantitat*1;

	if(vTipus=='DEMANDES')
	{
		vehicleId=document.getElementById('sel_vehicle_'+assocIndex).value;
		if(vehicleId=='')
		{
			alert('Atenci�: per seleccionar una quantitat has de seleccionar primer el teu vehicle amb el que vols respondre a aquesta demanda de transport');		
			document.getElementById('sel_quantitat_'+parametre+'_'+assocIndex).value=quantitatOld;
			return;
		}
		else if(mVehicleRef[vehicleId]['capacitat_'+parametre]<quantitat)
		{
			alert("Atenci�, no es pot seleccionar aquesta quantitat, ja que excedeix la capacitat del vehicle assignat.\n\nCapacitat del vehicle: "+mVehicleRef[vehicleId]['capacitat_pes']+" kg, "+mVehicleRef[vehicleId]['capacitat_volum']+" litres, "+mVehicleRef[vehicleId]['capacitat_places']+" places");
			document.getElementById('sel_quantitat_'+parametre+'_'+assocIndex).value=quantitatOld;
			return;
		}
		else
		{
			mTrams[assocIndex]['quantitat_'+parametre]=quantitat;
			calculaContractes(assocIndex);
		}
	}
	else
	{
		mTrams[assocIndex]['quantitat_'+parametre]=quantitat;
		calculaContractes(assocIndex);
	}
	
	return;
}

canvis=0;
function recordatoriGuardarContractes()
{
	document.getElementById('td_recordatoriGuardarContractes').innerHTML="<p style='font-family:verdana; font-size:12px; color:red; text-align:center;'>&nbsp;Recorda guardar els canvis abans de canviar de p�gina<br><font style='font-family:verdana; font-size:25px; color:red; text-decoration:blink;'>^</font></p>";
	document.getElementById('t_principal').style.border='2px solid red'
	document.getElementById('t_img_guardar_1').style.border='2px solid red'
	document.getElementById('t_img_guardar_2').style.border='2px solid red'
	document.getElementById('p_img_guardar_1').innerHTML='guardar';
	document.getElementById('p_img_guardar_2').innerHTML='guardar';

	missatgeAlerta='';
	f_missatgeAlerta();
	canvis=1;
	calGuardar=true;	
	return;
}

idAcordMostrat=0;
function mostrarAcords(idTram)
{
	//ocultar ultim acord mostrat
	document.getElementById('d_acords_explicits_'+idAcordMostrat).style.visibility='hidden';
	document.getElementById('d_acords_explicits_'+idAcordMostrat).style.position='absolute';
	document.getElementById('d_acords_explicits_'+idAcordMostrat).style.zIndex='0';

	idAcordMostrat=idTram;

	document.getElementById('d_acords_explicits_'+idAcordMostrat).style.visibility='inherit';
	document.getElementById('d_acords_explicits_'+idAcordMostrat).style.position='relative';
	document.getElementById('d_acords_explicits_'+idAcordMostrat).style.zIndex='1';

	
	return;
}

totalFacturaUnitats=0;
totalFacturaEcos=0;
totalFacturaEuros=0;
	totalQuantitatPes=0;
	totalQuantitatVolum=0;
	totalQuantitatPlaces=0;

function calculaContractes(numIndex)
{

	totalUnitats=0;
	totalEcos=0;
	totalEuros=0;

	totalCostUnitats=0;
	totalCostEcos=0;
	totalCostEuros=0;
	
	sumaPerc=0;
	if(1*quantitat!=1*quantitatOld) // ... si calculaContractes() no s'executa al carregar-se l'index, sino en seleccionar una quantitat d'un tram
	{
		if(mTrams[numIndex]['tipus']=='DEMANDES')
		{
			vehicleId=document.getElementById('sel_vehicle_'+numIndex).value;
			if(vehicleId=='')
			{
				if(quantitat>0)
				{
					document.getElementById('t_vehicle_'+numIndex).style.border='2px solid red';
				}
			}
			else 
			{
				if(quantitat>0)
				{
					document.getElementById('t_vehicle_'+numIndex).style.border='0px';
					document.getElementById('sel_quantitat_pes_'+numIndex).style.backgroundColor='#ffffff';
					document.getElementById('sel_quantitat_volum_'+numIndex).style.backgroundColor='#ffffff';
					document.getElementById('sel_quantitat_places_'+numIndex).style.backgroundColor='#ffffff';
					document.getElementById('sel_quantitat_pes_'+numIndex).style.color='#000000';
					document.getElementById('sel_quantitat_volum_'+numIndex).style.color='#000000';
					document.getElementById('sel_quantitat_places_'+numIndex).style.color='#000000';
				}
			}
		}
		recordatoriGuardarContractes();
	}

	for(i=0;i<mTrams.length;i++)
	{
		quantitatPes=mTrams[i]['quantitat_pes'];
		quantitatVolum=mTrams[i]['quantitat_volum'];
		quantitatPlaces=mTrams[i]['quantitat_places'];

		preuUnitats=mTrams[i]['quantitat_pes']*mTrams[i]['preu_pes'];
		preuUnitats+=mTrams[i]['quantitat_volum']*mTrams[i]['preu_volum'];
		preuUnitats+=mTrams[i]['quantitat_places']*mTrams[i]['preu_places'];
		preuEcos=(preuUnitats*mTrams[i]['ms'])/100;
		preuEuros=preuUnitats-preuEcos;

		mTrams[i]['preuUnitats']=preuUnitats;
		mTrams[i]['preuEcos']=preuEcos;
		mTrams[i]['preuEuros']=preuEuros;

			totalQuantitatPes+=quantitatPes;
			totalQuantitatVolum+=quantitatVolum;
			totalQuantitatPlaces+=quantitatPlaces;

			totalUnitats+=preuUnitats;
			totalEcos+=preuEcos;
			totalEuros+=preuEuros;
	}

	//afegir c�rrec fons despeses CAC
	totalUnitatsFDC=totalUnitats*FDCpp/100;
	totalEcosFDC=totalEcos*(FDCpp/100)*(msFDCpp/100);
	totalEurosFDC=totalUnitatsFDC-totalEcosFDC;


	totalFacturaUnitats=totalUnitatsFDC+totalUnitats;
	totalFacturaEcos=totalEcosFDC+totalEcos;
	totalFacturaEuros=totalEurosFDC+totalEuros;
	
	pcMs=(totalFacturaEcos/totalFacturaUnitats)*100;
	
				
	dif1545=0;
	for(i=0;i<mTrams.length;i++)
	{
		if(mTrams[i]['visible']==1)
		{
			if(mTrams[i]['quantitat_pes']!=0 || mTrams[i]['quantitat_volum']!=0 || mTrams[i]['quantitat_places']!=0)
			{
				//document.getElementById('td_producteproducte_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramid_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramsortida_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_trammunicipi_origen_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramarribada_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_trammunicipi_desti_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_trammunicipis_ruta_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_tramperiodicitat_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_trampreu_pes_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_trampreu_volum_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_trampreu_places_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_trampc_ms_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_trampes_disponible_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_tramvolum_disponible_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_tramplaces_disponibles_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_tramcapacitat_pes_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_tramcapacitat_volum_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_tramcapacitat_places_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramkm_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_producteproductor_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_tramtipus_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramcategoria0_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_tramcategoria10_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramvehicle_id_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramQuantitatPes_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramQuantitatVolum_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramQuantitatPlaces_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_tramestat_'+i).style.backgroundColor='#DCFBCE';
				//document.getElementById('td_producteformat_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramPreuUnitats_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramPreuEcos_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramPreuEuros_'+i).style.backgroundColor='#DCFBCE';
				
			}
			else
			{
				//document.getElementById('td_producteproducte_'+i).style.backgroundColor='#DCFBCE';
				document.getElementById('td_tramid_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramsortida_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_trammunicipi_origen_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramarribada_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_trammunicipi_desti_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_trammunicipis_ruta_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_tramperiodicitat_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramvehicle_id_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_trampreu_pes_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_trampreu_volum_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_trampreu_places_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_trampc_ms_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_trampes_disponible_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_tramvolum_disponible_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_tramplaces_disponibles_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_tramcapacitat_pes_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_tramcapacitat_volum_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_tramcapacitat_places_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramkm_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_producteproductor_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_tramtipus_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramcategoria0_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_tramcategoria10_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramQuantitatPes_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramQuantitatVolum_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramQuantitatPlaces_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_tramestat_'+i).style.backgroundColor='#ffffff';
				//document.getElementById('td_producteformat_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramPreuUnitats_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramPreuEcos_'+i).style.backgroundColor='#ffffff';
				document.getElementById('td_tramPreuEuros_'+i).style.backgroundColor='#ffffff';
			}
			
			document.getElementById('td_tramPreuUnitats_'+i).innerHTML='<p>'+(Math.round(mTrams[i]['preuUnitats']*100)/100)+'</p>';
			document.getElementById('td_tramPreuEcos_'+i).innerHTML='<p>'+(Math.round(mTrams[i]['preuEcos']*100)/100)+'</p>';
			document.getElementById('td_tramPreuEuros_'+i).innerHTML='<p>'+(Math.round(mTrams[i]['preuEuros']*100)/100)+'</p>';
		}
	}
	
	
	//document.getElementById('td_totalQuantitatPes').innerHTML='<p>'+(Math.round(totalQuantitatPes*100)/100)+'</p>';
	//document.getElementById('td_totalQuantitatVolum').innerHTML='<p>'+(Math.round(totalQuantitatVolum*100)/100)+'</p>';
	//document.getElementById('td_totalQuantitatPlaces').innerHTML='<p>'+(Math.round(totalQuantitatPlaces*100)/100)+'</p>';

	//document.getElementById('td_totalUnitatsFDC').innerHTML='<p>'+(Math.round(totalUnitatsFDC*100)/100)+'</p>';
	//document.getElementById('td_totalEcosFDC').innerHTML='<p>'+(Math.round(totalEcosFDC*100)/100)+'</p>';
	//document.getElementById('td_totalEurosFDC').innerHTML='<p>'+(Math.round(totalEurosFDC*100)/100)+'</p>';
	
	//document.getElementById('td_totalUnitats').innerHTML='<p>'+(Math.round(totalUnitats*100)/100)+'</p>';
	//document.getElementById('td_totalEcos').innerHTML='<p>'+(Math.round(totalEcos*100)/100)+'</p>';
	//document.getElementById('td_totalEuros').innerHTML='<p>'+(Math.round(totalEuros*100)/100)+'</p>';

	//document.getElementById('td_pcMs').innerHTML='<p>('+(Math.round(pcMs*100)/100)+'% MS)</p>';
	
	//document.getElementById('td_totalFacturaUnitats').innerHTML='<p>'+(Math.round(totalFacturaUnitats*100)/100)+'</p>';
	//document.getElementById('td_totalFacturaEcos').innerHTML='<p>'+(Math.round(totalFacturaEcos*100)/100)+'</p>';
	//document.getElementById('td_totalFacturaEuros').innerHTML='<p>'+(Math.round(totalFacturaEuros*100)/100)+'</p>';

	//totalQuantitatPes=0;
	//totalQuantitatVolum=0;
	//totalQuantitatPlaces=0;

	return;
}



function guardarContractes()
{
	chain='';
	
	if((totalQuantitatPes==0 && totalQuantitatVolum==0 && totalQuantitatPlaces==0) && canvis==0)
	{
		alert("Cal que seleccionis alguna de les "+vTipus+" o facis algun canvi per poder guardar");
		
		return false;
	}
	
	missatgeAlerta='';
	missatgeAlertaDE_='';
	missatgeAlertaOF_='';
	
	missatgeAlertaDE="Cal que ofereixis un vehicle per poder contractar la/es seg�ent/s DEMANDA/ES de transport:\n";
	missatgeAlertaOF="La/es  seg�ent/s OFERTA/ES de transport  no tenen cap vehicle assignat i no es poden contractar:\n";
	
	for(i=0;i<mTrams.length;i++)
	{
		if
		(
			mTrams[i]['quantitat_pes']!=mTrams[i]['estoc_inicial_pes'] 
			|| 
			mTrams[i]['quantitat_volum']!=mTrams[i]['estoc_inicial_volum']
			||
			mTrams[i]['quantitat_places']!=mTrams[i]['estoc_inicial_places']
		)
		{
			chain+='tram_'+mTrams[i]['id']+':'+mTrams[i]['quantitat_pes']+'|'+mTrams[i]['quantitat_volum']+'|'+mTrams[i]['quantitat_places']+'|'+mTrams[i]['vehicle']+'|'+mTrams[i]['tipus']+'|pendent;';
			
			if(mTrams[i]['vehicle']=='')
			{
				if(mTrams[i]['tipus']=='DEMANDES')
				{
					missatgeAlertaDE_+="\nid:"+mTrams[i]['id'];
				}
				else
				{
					missatgeAlertaOF_+="\nid:"+mTrams[i]['id'];
				}
			}
		}
	}
	if(missatgeAlertaDE_!='')
	{
		missatgeAlerta+=missatgeAlertaDE+missatgeAlertaDE_;
	}
	if(missatgeAlertaOF_!='')
	{
		missatgeAlerta+=missatgeAlertaOF+missatgeAlertaOF_;
	}

	document.getElementById('i_guardarContractesBit').value='1';

	if (missatgeAlerta.length!=0)
	{
		alert(missatgeAlerta);
		
		return false;
	}
	else
	{
		document.getElementById('i_guardarContractes').value=chain;
		document.getElementById('i_guardarContractesBit').value='1';

		document.getElementById('f_pars').submit();
		calGuardar=false;
	}
	
	return;
}

function sortBy(clau,direccio)
{
	document.getElementById('i_sortBy').value=clau;
	document.getElementById('i_ascdesc').value=direccio;
	document.getElementById('f_pars').submit();
	
	return;
}


function f_vistaVehicle()
{
	var1523=document.getElementById('sel_vVehicle').value
	document.getElementById('i_vVehicle').value=var1523;

	document.getElementById('f_pars').submit();

	return;
}

function f_vistaUsuari()
{
	var1523=document.getElementById('sel_vUsuari').value
	document.getElementById('i_vUsuariId').value=var1523;

	document.getElementById('f_pars').submit();

	return;
}

function f_vistaCategoria()
{
	var11071523=document.getElementById('sel_vCategoria').value
	document.getElementById('i_vCategoria').value=var11071523;

	document.getElementById('f_pars').submit();

	return;
}

function f_vistaSubCategoria()
{
	//s'envien tant la categoria com la subcategoria com a unica cadena
	var11071524=document.getElementById('sel_vSubCategoria').value
	document.getElementById('i_vSubCategoria').value=var11071524;

	document.getElementById('f_pars').submit();

	return;
}


function f_missatgeAlerta()
{
	document.getElementById('td_missatgeAlerta').innerHTML=missatgeAlerta;
	return;
}

key_mem='';
key0_mem='';
function mostrarTaulaFlotant(mousex,mousey,idTram,key,key0,mostrar)
{
	if(mostrar)
	{
		document.getElementById('d_ifrMostrarTram').style.top=mousey-200;
		document.getElementById('d_ifrMostrarTram').style.left=mousex+30;
		document.getElementById('d_ifrMostrarTram').style.visibility='inherit';
		iPars=document.getElementById('i_pars').value;

		window.frames['ifr_mostrarTram'].document.forms['f_ifrMostrarTram'].elements['id'].value=idTram;
		window.frames['ifr_mostrarTram'].document.forms['f_ifrMostrarTram'].elements['i_pars'].value=iPars;
		window.frames['ifr_mostrarTram'].document.forms['f_ifrMostrarTram'].submit();
		key_mem=key;
		key0_mem=key0;
	}
	else
	{
		document.getElementById('d_ifrMostrarTram').style.visibility='hidden';
	}

	return;
}


function veureResumContractes()
{
	if(haFetContracte)
	{
		document.getElementById('i_vistaAlbara').value=1;
		document.getElementById('f_pars').action='veureResumContractes.php';
		document.getElementById('f_pars').target='_blank';
		document.getElementById('f_pars').submit();
		document.getElementById('f_pars').action='contractesTrams.php?tip='+vTipus;
		document.getElementById('f_pars').target='_self';
	}
	else
	{
		alert("Atenci�: Heu de contractar algun tram per poder veure la 'vista albar�'");
		return false;
	}
		
	return;
}


function marcaSelectors()
{
	val1555=document.getElementById('sel_vVehicle').value;
	val1556=document.getElementById('sel_vCategoria').value;
	val1557=document.getElementById('sel_vSubCategoria').value;
	//val1558=document.getElementById('sel_etiqueta').value;
	//val1559=document.getElementById('sel_etiqueta2').value;
	if(val1555!='TOTS')	{	document.getElementById('sel_vVehicle').style.backgroundColor='#47AFFF';	}
	if(val1556!='TOTS')	{	document.getElementById('sel_vCategoria').style.backgroundColor='#47AFFF';	}
	if(val1557!='TOTS')	{	document.getElementById('sel_vSubCategoria').style.backgroundColor='#47AFFF';	}
	//if(val1558!='TOTS')	{	document.getElementById('sel_etiqueta').style.backgroundColor='#47AFFF';	}
	//if(val1559!='CAP')	{	document.getElementById('sel_etiqueta2').style.backgroundColor='#47AFFF';	}

	return;
}

function cb_confirmarResposta(propietariDemandaId,tramId,usuariRespostaId,accio)
{
	valor=document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_'+accio).value;
	if(accio=='c')
	{
		document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_c').checked=true;
		document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_c').value=1;
		document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_d').checked=false;
		document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_d').value=0;
	}
	else if(accio=='d')
	{
		document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_d').checked=true;
		document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_d').value=1;
		document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_c').checked=false;
		document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_c').value=0;
	}
	return;
}

function guardarConfirmacioResposta(propietariDemandaId,tramId,usuariRespostaId,contracteId)
{
	valorC=document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_c').value;
	valorD=document.getElementById('cb_respostes_p_'+propietariDemandaId+'_'+tramId+'_'+usuariRespostaId+'_d').value;
	if(valorC==0 && valorD==0)
	{
		alert("Cal seleccionar una acci� per aquesta resposta ('confirmar' o 'descartar')");
	}
	else
	{
		if(valorC=='1')
		{
			document.getElementById('i_confirmarResposta').value=propietariDemandaId+','+tramId+','+usuariRespostaId+','+contracteId+',c';
		}
		else if(valorD=='1')
		{
			document.getElementById('i_confirmarResposta').value=propietariDemandaId+','+tramId+','+usuariRespostaId+','+contracteId+',d';
		}
	}
	
	enviarFpars('contractesTrams.php?tip='+vTipus,'_self');
	
	return;
}


function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='contractesTrams.php?tip='+vTipus;
	document.getElementById('f_pars').target='_self';
		
	return;
}

