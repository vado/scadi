<?php

function html_formZones()
{
	global 	$mPars,
			$mReservesComandes,
			$zona,
			$mRebostsRef,
			$jaEntregatTkg,
			$jaEntregatUts,
			$mRecepcions,
			$mParametres,
			$mGrupsZones,
			$mReservesCSV;


	echo "
	<div>
	<p>AGRUPAMENT DE ZONES</p>
	<table  align='center' style='width:30%;' bgcolor='#dddddd'>
		<tr style='background-color:#ffffff;'>
			<th align='left' valign='top' style='width:40%;'>
			<p class='p_micro'>SUPER-ZONA (SZ)</p>
			</th>
		
			<th align='left' valign='top'>
			<p class='p_micro'>ZONA</p>
			</th>
		</tr>
		";
		reset($mGrupsZones);
		while (list($sz_,$mZones)=each($mGrupsZones))
		{
			echo "
		<tr style='background-color:#ffffff;' style='width:15%;'>
			<th align='left' valign='top'>
			<p class='p_micro'>".$sz_."</p>
			</th>

			<th align='left' valign='top' style='width:85%;'>
			";
			while (list($key,$zona_)=each($mZones))
			{
				if($zona_==$zona)
				{
					echo "
			<p  class='pAlertaOk4'>".$zona_."</p>
					";
				}
				else
				{
					echo "
			<p  class='p_micro'>".$zona_."</p>
					";
				}
			}
			reset($mZones);
			
			echo "
			</th>
		</tr>
			";
		}
		reset($mGrupsZones);
		echo "
	</table>
	<br>	
	<p>RESERVES ZONA: <b>".$zona."</b> </p>
	<table  align='center' style='width:50%;' bgcolor='#dddddd'>
	";
		echo "
		<tr style='background-color:#ffffff;' style='width:15%;'>
			<th align='left' valign='top'>
			<p class='p_micro'>ZONA</p>
			</th>
			<th align='left' valign='top' style='width:85%;'>
			<p  class='p_micro'>PUNT ENTREGA</p>
			</th>
		</tr>
		";
	while(list($zona_,$mReservesZona)=each($mReservesComandes))
	{
		echo "
		<tr style='background-color:#ffffff;'>
			<td align='left' valign='top'>
			<p class='p_micro'>".$zona."</p>
			</td>
			<td align='left' valign='top'  >
			<table  style='width:100%;'>
		";
		while(list($puntEntregaId,$mReservesPuntEntrega)=each($mReservesZona))
		{
				echo "
				<tr style='background-color:#ffffff;'>
					<td align='left' valign='top' style='width:25%;'>
					<p class='p_micro' style='text-align:left;'>".(urldecode($mRebostsRef[$puntEntregaId]['nom']))."</p>
					</td>

					<td align='left' valign='top'  style='width:75%;'>
					<p class='p_micro'><b>Comandes:</b></p>
				";
			while(list($grupId,$mReservesGrup)=each($mReservesPuntEntrega))
			{
				echo "
					<table  style='width:100%;'>		
						<tr>
							<td align='left' valign='top'  style='width:60%;'>
							<p class='p_micro' style='text-align:left;'>".(urldecode($mRebostsRef[$grupId]['nom']))."</p>
							</td>
					
							<td align='left' valign='top'  style='width:40%;'>
				";
				if($mRecepcions[$grupId]['acceptada']=='1')
				{
					echo "
							<p class='pAlertaOk4'>ACCEPTADA</p>
					";
				}
				else
				{
					echo "
							<p class='pAlertaNo4'>NO ACCEPTADA</p>
					";
				}
					
				echo "
							</td>
						</tr>
					</table>
				";
			}
			reset($mReservesPuntEntrega);		
			echo "
					</td>
				</tr>
			";
		}
		reset($mReservesZona);		
		echo "
			</table>
			</td>
		</tr>
			";
	}
	reset($mReservesComandes);		
	echo "
	</table>
	</div>
	";
	return;
}

function html_formPuntEntrega()
{
	global 	$mPars,
			$mReservesComandes,
			$mRebostsRef,
			$pE,
			$mGrupsZones;
	
	echo "
	<div>
	<br>
	<p>COMANDES QUE S'ENTREGUEN AL GRUP <b>".(urldecode($mRebostsRef[$pE]['nom']))."</b>: </p>

	<table  align='center' width='50%' style='background-color:#C0E4E3;'>
		<tr>
			<td align='left' valign='top'  width='45%'>
			<form id='f_validarRecepcio' action='' method='POST'>
			<table  align='center'  width='100%'>
				<tr>
					<th  align='center' valign='top'>
					<p>Acceptar<br>recepci�</p>
					</th>
		
					<th  align='center' valign='top'>
					<p>Nom grup</p>
					</th>
		
					<th  align='center' valign='top'>
					<p>Link albar�</p>
					</th>
				</tr>
		";

		while(list($puntEntregaId,$mReservesPuntEntrega)=each($mReservesComandes))
		{
			while(list($grupId,$mReservesGrup)=each($mReservesPuntEntrega))
			{
				echo "
				<tr >
					<td align='center'>
					";
					if(substr_count($mRebostsRef[$pE]['recepcions'],','.$grupId.',')>0)
					{
						echo "
						<p class='pAlertaOk4'>ACCEPTADA</p>
						";
					}
					else
					{
						echo "					
					<input id='cb_recepcioValidada_".$grupId."' onClick=\"javascript:val=this.value;val*=-1;this.value=val;afegirRecepcioAconfirmar(".$grupId.",this.value);\" type='checkbox' value='0'>
						";
					}
					echo "
					</td>
		
					<td align='center' valign='top' >
					<p class='p_micro'>".(urldecode($mRebostsRef[$grupId]['nom']))."</p>
					</td>
		
					<td align='center'>
					<p class='p_micro' style='text-align:center; cursor:pointer;' onClick=\"enviarFpars('vistaAlbara.php?sR=".$mPars['selRutaSufix']."&gRef=".$grupId."&op=totals','_blank');\" style='cursor:pointer;'>&nbsp;<u>albar�</u>&nbsp;</p>
					</td>
				</tr>
				";
			}
			reset($mReservesPuntEntrega);		
		}
		reset($mReservesComandes);		

	echo "
				<tr>
					<td>
					</td>
		
					<td align='center' valign='top'>
					<br><br>
					";
					if(count($mReservesComandes)>0)
					{
						echo "
					<input type='button' onClick=\"javascript:
													if(cadenaRecepcionsAConfirmar!='')
													{
														document.getElementById('i_cadenaRecepcionsAConfirmar').value=cadenaRecepcionsAConfirmar;
														enviarFpars('vistaReserves.php?pE=".$pE."','_self');
													}
													else
													{
														alert('Atenci�: per poder guardar els canvis cal que seleccionis alguna recepci� per confirmar');
													};
													\" value='guardar'>
						";
					}
					echo "
					</td>
		
					<td>
					</td>
				</tr>
			</table>
			</td>
			
			<td  width='45%'>
			<p class='nota'>* seleccioneu els grups a qui accepteu que recullin la seva comanda al vostre espai. 
			<br><br> 
			Una vegada marqueu un grup com a acceptat i guardeu, es considerar� concedit el vostre permis. 
			<br><br>
			Si voleu rectificar caldr� que us poseu en contacte amb l'administrador del gestor de comandes.
			<br><br>
			Us recomanem examinar les comandes que els altres grups volen recollir al vostre, i que si voleu 
			establir un acord amb ells els contacteu abans d'acceptar la recepci� de la seva comanda.
			<br><br>
			Podeu veure els kg de cada comanda al seu albar�
			<br><br>
			Podeu veure el total de kg dels grups a qui ja heu acceptat la recepci�, al final de l'albar� d'entrega de la CAC
			</p>
			</td>
		</tr>
	</table>
	</form>
	<br>
	</div>
	";
	return;
}

function html_mostrarProductes($db)
{
	global 	$mPars,
			$mProductes,
			$mProductors,
			$rc,
			$pE,
			$sz,
			$jaEntregatTkg,
			$jaEntregatTuts,
			$zona,
			$mRebostsRef,
			$mGrupsZones,
			$mInventarisRef,
			$superZona,
			$mReservesCsv;

	$mNomsColumnes=array('id','producte','productor','format','pes','pesT');
	$pesT=0;
	
		if($rc=='1'){$checkedRc='checked';}else{$checkedRc='';}
		if($mPars['elp']=='1'){$checkedElp='checked';}else{$checkedElp='';}
		echo "
	<br>
	<center>
	<p><b>Albar� de la CAC pel GRUP receptor</b>:<br>(amb el total de productes que els altres grups hi recolliran)</p>
	<p class='p_micro' style='text-align:center;'>
	<input ".$checkedRc." type='checkbox' value='".$rc."' onClick=\"javascript:val=this.value;val*=-1;this.value=val;enviarFpars('vistaReserves.php?pE=".$pE."&rc='+this.value+'&elp=".$mPars['elp']."','_self')\"> mostrar nom�s els productes de les recepcions confirmades per cada punt d'entrega
	</p>
	";
	
	if($mPars['grup_id']!='0')
	{
		echo "
	<p class='p_micro' style='text-align:center;'>
	<input ".$checkedElp." type='checkbox' value='".$mPars['elp']."' onClick=\"javascript:val=this.value;val*=-1;this.value=val;enviarFpars('vistaReserves.php?pE=".$pE."&rc=".$mPars['rc']."&elp='+this.value,'_self')\"> no mostrar els productes que no cal portar perqu� en algun magatzem de la Superzona local hi ha estoc positiu suficient
	</p>
		";
	}
	
	if($mPars['grup_id']!='0')
	{
		echo "
	<p class='p_micro' style='text-align:center;'>
	Selecciona la superzona per mostrar nom�s els productes dels productors gestionats desde la superzona seleccionada:
	<br>
	<select	id='sel_sz' onChange=\"javascript:enviarFpars('vistaReserves.php?sz='+this.value+'&pE=".$pE."&rc=".$rc."','_self');\">
		";
		$selected2='selected';
		reset($mGrupsZones);
		while(list($sz_,$mZones_)=each($mGrupsZones))
		{
			if($sz_==$sz){$selected='selected';$selected2='';}else{$selected='';}
			echo "
		<option ".$selected." value='".$sz_."'>".$sz_." (".(implode(',',$mZones_)).")</option>
			";
		}
		reset($mGrupsZones);
		echo "
		<option ".$selected2." value=''>- cap -</option>
	</select>
	</p>
		";
	}
	
	if($mPars['grup_id']!='0')
	{
		$pEtext=' a la zona <b>'.$zona.'</b>';
	}
	else
	{
		$pEtext=' a <b>totes les zones</b>';
	}

	if($pE!=''){$pEtext=" al punt d'entrega <b>".(urldecode($mRebostsRef[$pE]['nom']))."</b>";}

	if($mPars['excloureProductesJaEntregats']=='1')
	{
		echo "
		<p class='pAlerta4' style='text-align:center;'>* s'exclouen els productes ja entregats (".$jaEntregatTkg." kg, ".$jaEntregatTuts." uts.) ".$pEtext."</p>
		";
	}
	else
	{
		echo "
		<p class='pAlerta4' style='text-align:center;'>* <b>NO</b> s'exclouen els productes ja entregats (".$jaEntregatTkg." kg, ".$jaEntregatTuts." uts.) ".$pEtext."</p>
		";
	}

	if(isset($mPars['vProductor']) && $mPars['vProductor']!='TOTS')
	{
		echo "
		<p class='pAlerta4' style='text-align:center; color:red;'>* nom�s es mostren els productes del productor <b>".(urldecode($mProductors[$mPars['vProductor']]['projecte']))."</b></p>
		";
	}
	echo "
	</center>
	";

	$mTaula=array();
	$mMagatzemMostrar=array();

	while(list($key,$val)=each($mNomsColumnes))
	{
		if($val=='pes'){$val_=$val.' (kg)';}else{$val_=$val;}
		$mTaula[0][$val]=$val;
	}
	reset($mNomsColumnes);

	while(list($magatzemRef,$mInventariRef)=each($mInventarisRef))
	{
		$mTaula[0][$magatzemRef]=$magatzemRef;
	}
	reset($mInventarisRef);

	$mTaula[0]['reservat']="reservat<p class='p_micro2'>[*distribuci�]</p>";
	$mTaula[0]['cal_portar']="cal portar<p class='p_micro2'>[*intercanvi<br>superZones]</p>";
	$mTaula[0]['unitat_facturacio']='unitat facturaci�';
				
	while(list($index,$mProducte)=each($mProductes))
	{
		if($mProducte['quantitat']*1>0)
		{
			while(list($key,$val)=each($mNomsColumnes))
			{
				if($val=='producte'){$valor=urldecode($mProducte[$val]);}
				else if($val=='productor' || $val=='unitat_facturacio'){$valor=urldecode($mProducte[$val]);}
				//else if($val=='quantitat'){$valor='<b>'.$mProducte[$val].'</b>';}
				else if($val=='pesT')
				{
					$valor=$mProducte['pes']*$mProducte['quantitat']; 
				}
				else {$valor=$mProducte[$val];}

				$mTaula[$index][$val]=$valor;
			}
			reset($mNomsColumnes);

			$inventariRefIndexQuantitatLocal=0;

			while(list($magatzemRef,$mInventariRef)=each($mInventarisRef))
			{
				$zonaMagatzem=db_getZonaGrup($magatzemRef,$db);
				$sZmagatzem=getSuperZona($zonaMagatzem);
				if(isset($mInventariRef['inventariRef'][$index]))
				{
					$inventariRefIndexQuantitat=$mInventariRef['inventariRef'][$index];
				}
				else
				{
					$inventariRefIndexQuantitat=0;
				}
						
				if($inventariRefIndexQuantitat>0)
				{
					if(!in_array($magatzemRef,$mMagatzemMostrar)){array_push($mMagatzemMostrar,$magatzemRef);}
						
					if($zona==$zonaMagatzem)
					{
						$inventariRefIndexQuantitatLocal+=$inventariRefIndexQuantitat;
					}
				}

				$mTaula[$index][$magatzemRef]=$inventariRefIndexQuantitat;
			}
			reset($mInventarisRef);
					
			//quantitats:

			$mTaula[$index]['reservat']=$mProducte['quantitat'];

			if($mProducte['quantitat']>$inventariRefIndexQuantitatLocal)
			{
				$calPortar=$mProducte['quantitat']-$inventariRefIndexQuantitatLocal;
			}
			else
			{
				$calPortar=0;
			}
			$mTaula[$index]['cal_portar']=$calPortar;
			if($mPars['elp']=='1')
			{
				$pesT+=$mProducte['pes']*$calPortar;
			}
			else
			{
				$pesT+=$mProducte['pes']*$mProducte['quantitat'];
			}
			
			$mTaula[$index]['unitat_facturacio']=$mProducte['unitat_facturacio'];

		}
	}
	reset($mProductes);		


	//--------------------------------------------------------------------------
	$mCols=array_merge($mNomsColumnes,$mMagatzemMostrar,array('reservat','cal_portar','unitat_facturacio'));
	reset($mMagatzemMostrar);

	echo "
	<table border='0' width='70%' align='center'>
		<tr>
			<td align='right' valign='bottom' >
			<a href='docs".$mPars['selRutaSufix']."/reserves/reserves_".$mPars['usuari_id'].".csv' target='_blank'>Descarregar aquesta vista de reserves de zona (.csv)</a>
			</td>
		</tr>
	</table>
	
	<table border='1' width='70%' align='center'>
	";
	
	$pesTT=0;
	
	$linCsv=1;
	while(list($idProducte,$mLinia)=each($mTaula))		
	{
		if($idProducte==0)
		{
			echo "
		<tr>
			";
			while(list($col,$val)=each($mLinia))		
			{
				if(in_array($col,$mCols))
				{
					$zonaMagatzem=db_getZonaGrup($col,$db);
					$sZmagatzem=getSuperZona($col);
					if(in_array($col,$mMagatzemMostrar))
					{
						echo "
			<th align='left' valign='top' >
			";
			if($superZona==$sZmagatzem){$color='LightGreen';}else{$color='LightSkyBlue';}
			if($zona==$zonaMagatzem){$color='orange';}
			echo "
			<p class='p_micro2' style='color:".$color.";'>-inventari-<br>(".$zonaMagatzem.")</p>
			";
			
			echo "
			<p>".(urldecode($mRebostsRef[$col]['nom']))."</p>
			</th>
						";
						$mReservesCsv[0][$col]=urldecode($mRebostsRef[$col]['nom']);
					}
					else
					{
						echo "
			<th align='center' valign='top' >
			<p >".$mTaula[0][$col]."</p>
			</th>
						";
						$mReservesCsv[0][$col]=$col;
					}
				}
			}
			reset($mLinia);

			echo "
		</tr>
			";
		}
		else
		{
			echo "
		<tr>
			";
			while(list($col,$val)=each($mLinia))		
			{
				if($mPars['elp']=='-1' || ($mPars['elp']=='1' && $mLinia['cal_portar']>0))
				{
					if(in_array($col,$mCols))
					{
						$zonaMagatzem=db_getZonaGrup($col,$db);
						$sZmagatzem=getSuperZona($col);

						//pels magatzems
						if(in_array($col,$mMagatzemMostrar))
						{
							if($superZona==$sZmagatzem){$color='LightGreen';}else{$color='LightSkyBlue';}
							if($zona==$zonaMagatzem){$color='orange';}

									if($val>0)
									{
										echo "
			<th align='center' valign='middle' style='background-color:".$color.";'>
			<p >".$val."</p>
			</th>
										";
										$mReservesCsv[$linCsv][$col]=$val;
									}
									else
									{
										echo "
			<td align='center' valign='middle'>
			<p >".$val."</p>
			</td>
										";
										$mReservesCsv[$linCsv][$col]=$val;
									}
						}
						else if($col=='producte' || $col=='productor' || $col=='unitat_facturacio')
						{
							echo "
			<td>
			<p>".(urldecode($val))."</p>
			</td>
							";
						$mReservesCsv[$linCsv][$col]=urldecode($val);
							
						}
						else if($col=='pes')
						{
							echo "
			<td>
			<p>".(number_format($val,2))."</p>
			</td>
							";
						$mReservesCsv[$linCsv][$col]=str_replace('.',',',(number_format($val,2)));
						}
						else if($col=='pesT')
						{
							$pesTT+=$val;
							echo "
			<td>
			<p>".(number_format($val,2))."</p>
			</td>
							";
						$mReservesCsv[$linCsv][$col]=str_replace('.',',',(number_format($val,2)));
						}
						else
						{
							echo "
			<td>
			<p>".$val."</p>
			</td>
							";
						$mReservesCsv[$linCsv][$col]=$val;
						}
					}
				}
			}
			reset($mLinia);
			echo "
		</tr>
			";
		}
		$linCsv+=1;
	}
	reset($mTaula);
	echo "
	</table>

	<table border='1' width='70%' align='center'>
		<tr>
			<th width='80%' align='right'>
			<p>Pes Total:</p>
			</th>
			<td width='20%' align='right'>
			<p><b>".(number_format($pesTT,2))."</b> kg</p>
			</td>
		</tr>
	</table>
	<br>&nbsp;
	";
						$mReservesCsv[$linCsv]['Pes Total']='Pes Total: '.(number_format($pesTT,2));
	
	return;
}

//------------------------------------------------------------------------------
function html_mostrarNotes()
{
	echo "
	<table style='width:70%;' align='center'>
		<tr>
			<td style='width:100%;' align='center'>
			<p class='nota2'><u>Notes:</u></p>
			<p class='nota2'>
			* es mostren tots els magatzems amb estoc positiu d'algun dels productes visualitzats 
			<br>
			** els magatzems que pertanyen a la SuperZona del la zona seleccionada apareixen en <font style='background-color:LightGreen;'>verd</font>
			<br>
			** els magatzems que no pertanyen a la SuperZona del la zona seleccionada apareixen en <font style='background-color:LightSkyBlue;'>blau</font>
			<br>
			** els magatzems que pertanyen a la zona de la zona seleccionada apareixen en <font style='background-color:orange;'>taronja</font>
			</p>
			</td>
		</tr>
	</table>
		";

	return;
}

?>

		