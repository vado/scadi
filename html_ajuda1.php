<?php
function html_ajuda1($fitxer,$marca)
{
	global $mAjuda,$mPars;
	
	if(@$mPars['nivell']=='sadmin' || @$mPars['nivell']=='admin' || @$mPars['nivell']=='trad')
	{
		$cadenaHtml="
	
		<a class='a_ajuda1' style='cursor:pointer;' onClick=\"javascript: fixarAjuda();\" onMouseOver=\"javascript:cursor=getMouse(event); mostrarAjuda(cursor.x,cursor.y,'".$fitxer."','".$marca."',1);\" onMouseOut=\"javascript:cursor=getMouse(event);  mostrarAjuda(cursor.x,cursor.y,'".$fitxer."','".$marca."',0);\"><b>(?)</b></a>
		<div id='d_aj_".$fitxer."_".$marca."' style='z-indez:-1; position:absolute; top:0px;  visibility:hidden;'>
		".(@urldecode($mAjuda[$fitxer][$marca]))."
		</div>
		
		";
	}
	else
	{
		$cadenaHtml="
	
		<a class='a_ajuda1' style='cursor:pointer;'  onMouseOver=\"javascript:cursor=getMouse(event); mostrarAjuda(cursor.x,cursor.y,'".$fitxer."','".$marca."',1);\" onMouseOut=\"javascript:cursor=getMouse(event);  mostrarAjuda(cursor.x,cursor.y,'".$fitxer."','".$marca."',0);\"><b>(?)</b></a>
		<div id='d_aj_".$fitxer."_".$marca."' style='z-indez:-1; position:absolute; top:0px; visibility:hidden;'>
		".(@urldecode($mAjuda[$fitxer][$marca]))."
		</div>
		
		";
	}

	return $cadenaHtml;
}

function html_helpRecipient()
{
	global $mPars;

	echo "
	<div id='d_ajuda' style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
		";
		//<div style='width:700px; height:200px; overflow:auto;'>
		//<table width='100%'  height='100%' id='t_ajuda'   bgcolor='LightBlue' >
		echo "
		<div>
		<table id='t_ajuda' width='700px' height='200px'  bgcolor='LightBlue' >
			<tr>
				<td width='100%' height='100%'  bgcolor='orange'>
				<table width='100%'  height='100%'  bgcolor='LightBlue'>
					<tr>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>		
						<td width='90%'>
						</td>
						<td width='5%'>
						";
						if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
						{
							echo "
							<input id='i_crearAjuda' class='i_micro' type='button' value='nou' style='visibility:hidden;' onClick=\"javascript: nouAjuda();\">
							";
						}
						if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='trad')
						{
							echo "
							<input id='i_editarAjuda' class='i_micro' type='button' value='editar' style='visibility:hidden;' onClick=\"javascript: editarAjuda();\">
							";
						}
						echo "
						</td>
					</tr>
					<tr>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>
						<td >
						<p id='p_ajuda' class='ajuda1'></p>
						</td>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>
						<td width='90%'>
						<p>&nbsp;</p>
						</td>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</div>
		";
		html_editarAjuda();
		html_crearAjuda();
		echo "
	</div>
	";

	return ;
}

function html_editarAjuda()
{
	global $mPars,$parsChain;

	echo "
	<div id='d_editarAjuda' style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
		<form id='f_guardarAjuda' name='f_guardarAjuda' action='' target='_self' method='post'>
		<table  width='500px'  bgcolor='LightBlue' align='center'>
			<tr>
				<td align='right'></td>
				<td>
				<table width='100%'>
					<tr>
						<td align='left'>
						<p>
						marca:&nbsp;&nbsp;<input style='background-color:LightBlue;' type='text' READONLY id='i_ajuda_marca' name='i_ajuda_marca' value=''>
						</p>
						<p>
						fitxer:&nbsp;&nbsp;<input style='background-color:LightBlue;' type='text' READONLY id='i_ajuda_fitxer' name='i_ajuda_fitxer' value=''>
						</p>
						</td>
						
						<td align='right' valign='top'>
						<input type='button' value='guardar' onClick=\"javascript: guardarAjuda();\">					
						</td>
					</tr>
				</table>
				&nbsp;
				</td>
				<td>&nbsp;<input type='button' value='X' onClick=\"javascript: tancarEdicioAjuda();\"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align='center'>
				<textArea id='ta_ajuda_text' name='ta_ajuda_text' cols='50' rows='20'></textArea>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<input type='hidden' name='i_pars' value='".$parsChain."'>
		</form>
	</div>
	";

	return ;
}

function html_crearAjuda()
{
	global $mPars,$parsChain,$mAjuda;

	echo "
	<div id='d_crearAjuda' style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
		<form id='f_crearAjuda' name='f_crearAjuda' action='' target='_self' method='post'>
		<table  width='500px'  bgcolor='LightBlue' align='center'>
			<tr>
				<td align='right'></td>
				<td>
				<table width='100%'>
					<tr>
						<td align='left'>
						<p>
						fitxer:&nbsp;&nbsp;
						<select  name='i_ajuda_fitxerNou'>
						";
						while(list($fitxer,$mVal)=each($mAjuda))
						{
							echo "
						<option value='".$fitxer."'>".$fitxer." (nova marca:".(count($mVal)+1).")</option>
							";
							
						}
						reset($mAjuda);
						
						echo "
						</select>
						</p>
						</td>
						
						<td align='right' valign='top'>
						<input type='button' value='crear' onClick=\"javascript: crearAjuda();\">					
						</td>
					</tr>
				</table>
				&nbsp;
				</td>
				<td>&nbsp;<input type='button' value='X' onClick=\"javascript: tancarEdicioAjuda();\"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align='center'>
				<textArea  name='ta_ajuda_text' cols='50' rows='20'></textArea>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<input type='hidden' name='i_pars' value='".$parsChain."'>
		</form>
	</div>
	";

	return ;
}
?>

		