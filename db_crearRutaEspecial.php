<?php
//s'utilitza desde parametres.php (tancament de ruta);
// s'utilitza autiomaticament en cas d'emenrgencia desde index.php

//*v36.2-tot

function crearTaulesNovesESP($db)
{
	global $mParams,$ruta0,$rutaNova,$mPars,$mTaulesBDmensualESP;

	while(list($key,$taula)=each($mTaulesBDmensualESP))
	{
		$taula_=$taula.'_'.$rutaNova;
		if($result=mysql_query("CREATE TABLE ".$taula_." LIKE ".$taula.'_'.$ruta0,$db))
		{
			$result=mysql_query("INSERT ".$taula_." SELECT * FROM ".$taula.'_'.$ruta0,$db);
			echo "<br>OK: ".$taula_." - 16 db_crearRutaEspecial.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//echo " <br>ok CREATE TABLE ".$taula_." LIKE ".$taula.'_'.$ruta0;
		}
		else
		{
			echo "<br>ERROR: ".$taula_." - 21 db_crearRutaEspecial.php ".mysql_errno() . ": " . mysql_error(). "\n";
			error_log ( "db_crearRutaEspecial 17 CREATE TABLE - ",3, $mParams['logFile']);
			//echo " <br>error CREATE TABLE ".$taula_." LIKE ".$taula.'_'.$ruta0;
			
			return false;
		}
		
		//$result=mysql_query("DROP TABLE ".$taula_,$db);

	}
	reset($mTaulesBDmensualESP);

	return true;
}


function modificarContingutProductesESP($db)
{
	global $mParams,$mPars,$ruta0,$rutaNova,$perfilsIdChain;


	//mantenim actius els que s'han oferit el mes passat
	//echo "<br>delete from productes_".$rutaNova." WHERE LOCATE(CONCAT(',',SUBSTRING(productor,1,LOCATE('-',productor)-1)),',',CONCAT(' ','".$perfilsIdChain."'))=0";
	if(!$result=mysql_query("delete from productes_".$rutaNova." WHERE 
	LOCATE
	(
		CONCAT
		(
			','
			,
			SUBSTRING(productor,1,LOCATE('-',productor)-1)
			,
			','
		)
		,
		CONCAT
		(
			' '
			,
			'".$perfilsIdChain."'
		)
	)
	=0
	&&
	(
		SUBSTRING(productor,1,LOCATE('-',productor)-1)!=63
	)
	
	
	",$db))
	{
		//echo "<br> 45 ".mysql_errno() . ": " . mysql_error(). "\n";
		error_log ( "db_crearRutaEspecial.php 45 delete productes - ",3, $mParams['logFile']);
	}
	if(!$result=mysql_query("update productes_".$rutaNova." set actiu='0', estoc_previst='0',estoc_disponible='0' WHERE LOCATE(CONCAT(' ',SUBSTRING(productor,1,LOCATE('-',productor)-1)),',',CONCAT(',','".$perfilsIdChain."'))=0",$db))
	{
		//echo "<br> 50 ".mysql_errno() . ": " . mysql_error(). "\n";
		error_log ( "db_crearRutaEspecial.php 45 delete productes - ",3, $mParams['logFile']);
	}

	return;
}


	
function modificarContingutProductorsESP($db)
{
	global $mParams,$mPars,$ruta0,$rutaNova,$perfilsIdChain;


	//echo "<br>delete from productors_".$rutaNova." WHERE LOCATE(CONCAT(',',id,','),CONCAT(' ','".$perfilsIdChain."'))=0";
	if(!$result=mysql_query("delete from productors_".$rutaNova." WHERE LOCATE(CONCAT(',',id,','),CONCAT(' ','".$perfilsIdChain."'))=0 AND id!='63'",$db))
	{
		echo "<br> 45 ".mysql_errno() . ": " . mysql_error(). "\n";
		error_log ( "db_crearRutaEspecial.php 45 delete productores - ",3, $mParams['logFile']);
	}


	return;
}
	

function modificarContingutComandesESP($db)
{
	global $mParams,$mPars,$ruta0,$rutaNova,$mParametres;

	$mProductes=array();
	$guardarComanda='';

	//$mComanda=db_getComanda('0-CAC',$db);
	if(!$result=mysql_query("delete from comandes_".$rutaNova,$db))
	{
		error_log ( "db_crearRutaEspecial.php 92 delete comandes - ",3, $mParams['logFile']);
	}
	if(!$result=mysql_query("delete from comandes_seg_".$rutaNova,$db))
	{
		error_log ( "db_crearRutaEspecial.php 92 delete comandes_seg - ",3, $mParams['logFile']);
	}

	
	
	return;
}

function modificarContingutIncidenciesESP($db)
{
	global $mParams,$mPars,$ruta0,$rutaNova,$mParametres;

	$mProductes=array();
	$guardarComanda='';

	$mComanda=db_getComanda('0-CAC',$db);
	if(!$result=mysql_query("delete from incidencies_".$rutaNova,$db))
	{
		error_log ( "db_crearRutaEspecial.php 92 delete incidencies - ",3, $mParams['logFile']);
	}

	
	
	return;
}


function modificarContingutParametresESP($db)
{
	global $mParams,$mPars,$ruta0,$rutaNova,$mDies,$mM,$mParametres,$etiquetaRutaEspecial,$condicionsPeriode;

	while(list($parametre,$valor)=each($mM))
	{
		if(!$result=mysql_query("update parametres_".$rutaNova." set valor='".$valor."' where parametre='".$parametre."'",$db))
		{
			error_log ( "db_crearRutaEspecial.php 84 update parametres - ",3, $mParams['logFile']);
		}
	}


	//parametres ruta vella ----------------------------------------------------
	if(!$result=mysql_query("update parametres_".$ruta0." set valor='0' where parametre='zonesProvisionals'",$db))
	{
			error_log ( "db_crearRutaEspecial.php 92 update parametres - ",3, $mParams['logFile']);
	}

	//parametres ruta nova -----------------------------------------------------
	if(!$result=mysql_query("update parametres_".$rutaNova." set valor='1' where parametre='zonesProvisionals'",$db))
	{
			error_log ( "db_crearRutaEspecial.php 98 update parametres - ",3, $mParams['logFile']);
	}

	if(!$result=mysql_query("update parametres_".$rutaNova." set valor='1' where parametre='precomandaTancada'",$db))
	{
			error_log ( "db_crearRutaEspecial.php 103 update parametres - ",3, $mParams['logFile']);
	}
	
	if(!$result=mysql_query("update parametres_".$rutaNova." set valor='".$etiquetaRutaEspecial."' where parametre='rutaEspecial'",$db))
	{
			error_log ( "db_crearRutaEspecial.php 150 update parametres - ",3, $mParams['logFile']);
	}

	if(!$result=mysql_query("update parametres_".$rutaNova." set valor='".$condicionsPeriode."' where parametre='condicionsPeriode'",$db))
	{
		error_log ( "db_crearRutaEspecial.php 184 update parametres - ",3, $mParams['logFile']);
	}
	

	$iniciRutaAbastiment_=$mParametres['iniciRutaAbastiment']['valor'];
	$iniciRutaAbastiment_=suma1MesAdata($iniciRutaAbastiment_);//F:'10-12-2015'
	if(!$result=mysql_query("update parametres_".$rutaNova." set valor='".$iniciRutaAbastiment_."' where parametre='iniciRutaAbastiment'",$db))
	{
			error_log ( "db_crearRutaEspecial.php 110 update parametres - ",3, $mParams['logFile']);
	}


	return;
}


function db_crearRutaEspecial($db)
{
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['result']=true;
	$mMissatgeAlerta['missatge']='';
	if(crearTaulesNovesESP($db))
	{
		modificarContingutProductesESP($db);
		modificarContingutProductorsESP($db);
		modificarContingutComandesESP($db);
		modificarContingutIncidenciesESP($db);
		modificarContingutParametresESP($db);
	}
	else
	{
		$mMissatgeAlerta['missatge'].="<p class='pAlertaNo'>Error: no s'han pogut crear les taules noves</p>";
		$mMissatgeAlerta['result']=false;
	}
	
	
	return $mMissatgeAlerta;
}	
	
?>

		