<?php
//------------------------------------------------------------------------
function db_generarComandaApartirDestocPrevist($db)
{
	global $mPars,$mParametres;

	$mProductes=db_getProductes($db);
	$comanda='';
	$missatgeAlerta='';
	
	while(list($key,$mProducte)=each($mProductes))
	{
		if(substr_count($mProducte['tipus'],'especial')==0)
		{
			$comanda.='producte_'.$mProducte['id'].':'.$mProducte['estoc_previst'].';';
		}
	}
	reset($mProductes);

	//guardar comanda anterior:

	if($result=@mysql_query("select * from comandes_".$mPars['selRutaSufix']." where rebost='0-CAC'",$db))
	{
		//echo "<br> 27 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mComandaAnteriorCopiar=mysql_fetch_array($result,MYSQL_ASSOC);
	
		if(!$result=mysql_query("insert into comandes_seg values('','".$mComandaAnteriorCopiar['num_comanda']."','".$mComandaAnteriorCopiar['periode_comanda']."','".$mComandaAnteriorCopiar['rebost']."','".$mComandaAnteriorCopiar['usuari_id']."','".$mComandaAnteriorCopiar['data']."','".$mComandaAnteriorCopiar['resum']."','".$mComandaAnteriorCopiar['ctiar']."','".$mComandaAnteriorCopiar['ms_ctiar']."','".$mComandaAnteriorCopiar['ctear']."','".$mComandaAnteriorCopiar['ms_ctear']."','".$mComandaAnteriorCopiar['f_pagament']."','".$mComandaAnteriorCopiar['punt_entrega']."','','','','')",$db))
		{
			//echo "<br> 32 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";

			$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no es pot guardar la comanda anterior</p>";
		}
		else
		{
			if(!$result=@mysql_query("delete from comandes_".$mPars['selRutaSufix']." where rebost='0-CAC'",$db))
			{
				//echo "<br> 43 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";

				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no es pot eliminar la comanda cac anterior</p>";
			}
			if(!$result=@mysql_query("insert into comandes_".$mPars['selRutaSufix']." values('','".(date('YmdHis'))."','".$mParametres['periodeComanda']['valor']."','0-CAC','".$mPars['usuari_id']."','".$mComandaAnteriorCopiar['data']."','".$comanda."','".$mComandaAnteriorCopiar['ctiar']."','".$mComandaAnteriorCopiar['ms_ctiar']."','".$mComandaAnteriorCopiar['ctear']."','".$mComandaAnteriorCopiar['ms_ctear']."','".$mComandaAnteriorCopiar['f_pagament']."','".$mComandaAnteriorCopiar['punt_entrega']."','','','','')",$db))
			{
				//echo "<br> 40 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";

				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no es pot guardar la comanda a partir de l'estoc previst</p>";
			}
			else
			{
				$missatgeAlerta="<p class='pAlertaOk'>S'ha guardat correctament la comanda a partir de l'estoc previst</p>";
			}
		}
	}	

	return $missatgeAlerta;
}

//------------------------------------------------------------------------
function db_estocPrevistIgualEstocDisponible($db)
{
	global $mPars,$mParametres;

	$missatgeAlerta='';
	
	//guardar comanda anterior:

	if($result=@mysql_query("update productes_".$mPars['selRutaSufix']." set estoc_previst=estoc_disponible where LOCATE('dip�sit',tipus)='0'",$db))
	{
		//echo "<br> 65 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";
	
		if($result=@mysql_query("update productes_".$mPars['selRutaSufix']." set estoc_disponible=estoc_previst",$db))
		{
			//echo "<br> 65 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";
	
			$missatgeAlerta="<p class='pAlertaOk'>S'han modificat els valors d'estoc de cada producte a estoc_previst=estoc_disponible</p>";
		}	
	}
	else
	{
		$missatgeAlerta="<p class='pAlertaNo'>Atenci�: No s'han pogut modificat els valors d'estocs de cada producte a estoc_previst=estoc_disponible</p>";
	}	
	

	
	return $missatgeAlerta;
}
//--------------------------------------------------------------------
function db_generarComandaUsuariRespDeComandaGrup($db)
{
	global $mPars,$mParametres,$mGrupsRef;

	$missatgeAlerta='';
$ruta='1403';
	if($result=@mysql_query("select * from comandes_".$ruta,$db))
	{
		//echo "<br> 101 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";
	
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			if(!isset($mComandes[$grupId])){$mComandes[$grupId]=array();}
			
			$mComandes[$grupId][$mRow['usuari_id']]=$mRow;
			//echo "<br>".$grupId;
		}
	}
	
	$mComandes2=array();
	
	while(list($grupId,$mComanda)=each($mComandes))
	{
		if($grupId!=0)
		{
			$cadenaFpagament='';
			if(!isset($mComandes2[$grupId])){$mComandes2[$grupId]=array();}
			if(!isset($mComandes2[$grupId][0])){$mComandes2[$grupId][0]=array();}
			$mComandes2[$grupId]['0']=$mComanda['0'];
			$mComandes2[$grupId]['0']['usuari_id']=0;
			
			$mComandes2[$grupId]['0']['resum']='';
			$mFpagament=explode(',',$mComandes2[$grupId][0]['f_pagament']);
			$ecos=$mFpagament[0];
			$eb=$mFpagament[1];
			$eu=$mFpagament[2];
			$ums=$ecos+$eb+$eu;
			$ecosPp=$ecos*100/$ums;
			$ebPp=$eb*100/$ums;
			$euPp=$eu*100/$ums;
			if($eu>0){$euTransf=1;}else{$euTransf=0;}
			if($ecos>0){$compteEcos=$mGrupsRef[$grupId]['compte_ecos'];}
			if($eb>0){$compteCieb=$mGrupsRef[$grupId]['compte_cieb'];}
		
			$cadenaFpagament=$ecos.','.$eb.','.$eu.','.$euTransf.',0,0,'.$ecosPp.','.$ebPp.','.$euPp;
		

			$mComandes2[$grupId][$mGrupsRef[$grupId]['usuari_id']]=$mComandes2[$grupId]['0']; //Agafa els responsables de grup segons ruta actual
			$mComandes2[$grupId][$mGrupsRef[$grupId]['usuari_id']]['usuari_id']=$mGrupsRef[$grupId]['usuari_id'];
			$mComandes2[$grupId][$mGrupsRef[$grupId]['usuari_id']]['f_pagament']=$cadenaFpagament;
			$mComandes2[$grupId][$mGrupsRef[$grupId]['usuari_id']]['punt_entrega']='';
			$mComandes2[$grupId][$mGrupsRef[$grupId]['usuari_id']]['resum']=$mComanda['0']['resum'];
			$mComandes2[$grupId]['0']['f_pagament']=$cadenaFpagament;

		}
	}
	reset($mComandes);
	
	while(list($grupId,$mComanda)=each($mComandes2))
	{
			while(list($usuariId,$mComandaUsuari)=each($mComanda))
			{
				if($usuariId=='0')
				{
					if(!$result=@mysql_query("update comandes_".$ruta." set resum='' where SUBSTRING_INDEX(rebost,'-',1)='".$grupId."' AND usuari_id='0'",$db))
					{
						//echo "<br> 159 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";
						//echo "update comandes_".$ruta." set resum='' where SUBSTRING_INDEX(rebost,'-',1)='".$grupId."' AND usuari_id='0'";
						$missatgeAlerta.=$ruta." comanda: ".(urldecode($mGrupsRef[$grupId]['nom']))." > ha fallat<br>";
					}
					else
					{
						$missatgeAlerta.=$ruta." comanda: ".(urldecode($mGrupsRef[$grupId]['nom']))." > Ok<br>";
						//echo "update comandes_".$ruta." set resum='' where SUBSTRING_INDEX(rebost,'-',1)='".$grupId."' AND usuari_id='0'";
					}
				}
				else
				{
					if(!$result=@mysql_query("insert into comandes_".$ruta." values('','".(date('YmdHis'))."','".$mComandaUsuari['periode_comanda']."','".$grupId.'-'.$mGrupsRef[$grupId]['nom']."','".$usuariId."','".(date('dmY'))."','".$mComandaUsuari['resum']."','','','','','".$mComandaUsuari['f_pagament']."','','','0','')",$db))
					{
						//echo "<br> 173 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";
						//echo "insert into comandes_".$ruta." values('','".(date('YmdHis'))."','".$mComandaUsuari['periode_comanda']."','".$grupId.'-'.$mGrupsRef[$grupId]['nom']."','".$mComandaUsuari['usuari_id']."','".(date('dmY'))."','','','','','','".$mComandaUsuari['f_pagament']."','')";
						$missatgeAlerta.=$ruta." comanda: ".(urldecode($mGrupsRef[$grupId]['nom']))." > ha fallat<br>";
					}
					else
					{
						$missatgeAlerta.=$ruta." comanda: ".(urldecode($mGrupsRef[$grupId]['nom']))." > Ok<br>";
					}
				}
			}
	}
	reset($mComandes2);
	
	//pendent guardar resum i fpagament a comanda usuari
	return $missatgeAlerta;
}


//------------------------------------------------------------------------------
function db_importarTaula($rutaImportar,$db)
{
	global $source,$mPars,$mParams,$mTaulesBDmensual;


	$missatgeAlerta='';

	$cDir=getcwd();
	if(!chdir('sql'))
	{
		mkdir('sql');
		if(!chdir('sql/'.$rutaImp))
		{
			mkdir('sql/'.$rutaImp);
		}
	}
	chdir($cDir);
	
	while(list($key,$taula)=each($mTaulesBDmensual))
	{
		//if(!$fitxer=file("sql/".$rutaImportar."/".$taula."_".$rutaImportar.".sql", FILE_IGNORE_NEW_LINES))
		if(!$mFitxer= file("sql/".$rutaImportar."/".$taula."_".$rutaImportar.".sql"))
		{
			$missatgeAlerta.="<p class='pAlertaNo4'>ERROR: obrir fitxer: "."sql/".$rutaImportar."/".$taula."_".$rutaImportar.".sql</p>";
		}
		else
		{
		$fitxer=implode('',$mFitxer);
		vd($fitxer);
			if(!$result=mysql_query($fitxer,$db))
			{
				echo "<br> 224 operacionsPuntuals.php  ".mysql_errno() . ": " . mysql_error(). "\n";
				$missatgeAlerta.="<p class='pAlertaNo'>ERROR: ha fallat importaci� sql/".$rutaImportar."/".$taula.'_'.$rutaImportar.".sql > ".$taula.'_'.$rutaImportar."</p>";
			}	
			else
			{
				$missatgeAlerta['missatge'].="<p class='pAlertaOk'>OK: importaci� sql/".$rutaImportar."/".$taula.'_'.$rutaImportar.".sql > ".$taula.'_'.$rutaImportar."</p>";
			}	
		}
	}
	reset($mTaulesBDmensual);
	
	
	return $missatgeAlerta;
}

?>

		