<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include "html_ajuda1.php";
include "db_ajuda.php";

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();
$parsChain=makeParsChain($mPars);

	
$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}


$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db); //inicialitza variables anteriors;

$login=false;
$login=checkLogin($db);
if(!($login && ($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')))
{
	echo "<p>usuaria no autoritzada</p>";
	exit();
}
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['comptesCes.php']=db_getAjuda('comtesCes.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$mUsuarisRef=db_getUsuarisRef($db);
$mGrupsRef=db_getGrupsRef($db);
$mRutesSufixes=getRutesSufixes($db);
//*v36 5-1-16 assignacio
$mPeriodesInfo=db_getPeriodesInfo($db);


echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>Comptes CES comandes actuals</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<script type='text/javascript'  language='JavaScript'>
calGuardar=false;
function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='compteCes.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}
</script>
</head>
<body bgcolor='".$mColors['body']."'>
";
html_demo('comptesCes.php?');
echo "
	<table align='center' style='width:80%;' bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			<br>
	";
//*v36.5-funcio call
	mostrarSelectorRuta(1,'comptesCes.php');
	echo "
			</td>
		</tr>
		<tr>
			<td style='width:100%;' align='center'>
			<br><br>
		<center><p style='font-size:18px;'>Llistat de comptes en ecos de les comandes actuals:</p></center>
		<br>
			<table border='1' align='center' style='width:80%;'>
	";
	$mComandesComptesCes=db_getComandesComptesCes($db);
	ksort($mComandesComptesCes);

	$mCodisEcx=db_getCodisEcoxarxes($db);
	ksort($mCodisEcx);


	while(list($compteCes,$mComandesCompteCes)=each($mComandesComptesCes))
	{
		while(list($grupId,$mComandesGrupCompteCes)=each($mComandesCompteCes))
		{
			while(list($usuariId,$mComandesUsuariCompteCes)=each($mComandesGrupCompteCes))
			{
				$mPars['grup_id']=$grupId;
				$mPars['selUsuariId']=$usuariId;
			
				db_getFormaPagament($db);
				$color='red';
				$bgcolor='white';
				if(!array_key_exists(substr($compteCes,0,4),$mExcConfiables))
				{
					if(array_key_exists(substr($compteCes,0,4),$mExcNoConfiables))
					{
						$bgColor='black';
						$color='white';
					}
					else
					{
						$bgColor='white';
						$color='red';
					}
				}
				else
				{
					$bgColor='#'.$mCodisEcx[substr($compteCes,0,4)]['color']; 
					$valor=dechex(hexdec('FFFFFF')-hexdec($mCodisEcx[substr($compteCes,0,4)]['color']));
					$color='#'.($valor);
				}
				echo "
				<tr >
					<td style='width:15%; background-color:".$bgColor.";'>
					<p style='color:".$color."; text-shadow: 1px 1px #000000;'>".$compteCes."
					<br> 
					(";
					if($mPars['fPagamentEcosIntegralCES']=='1')
					{
						echo "I-CES";
					}
					else
					{
						echo "CES";
					}
					echo ")</p>
					</td>
					
					<td>
					<p>".(urldecode($mGrupsRef[$grupId]['nom']))."</p>
					</td>
					
					";
					if($usuariId!='0')
					{
						echo "
					<td>
					<p>".(urldecode($mUsuarisRef[$usuariId]['usuari']))."
					<br>
					".(urldecode($mUsuarisRef[$usuariId]['email']))."</p>
					</td>
						";
					}
					else
					{
						echo "
					<td>
					<p>
					<b>RG: ".@$mUsuarisRef[$mGrupsRef[$grupId]['usuari_id']]['usuari']."</b>
					<br>
					".@$mUsuarisRef[$mGrupsRef[$grupId]['usuari_id']]['email']."</p>
					</td>
						";
					}
					
					echo "
					<td>
					<p>".(@number_format($mPars['fPagamentEcos'],2,'.',''))." ecos (".(number_format($mPars['fPagamentEcosPerc'],2,'.',''))." %)</p>
					</td>
					
					<td>
					<p>".(@number_format($mPars['fPagamentEb'],2,'.',''))." M3 (".(number_format($mPars['fPagamentEbPerc'],2,'.',''))." %)</p>
					</td>
					
					<td>
					<p>".(@number_format($mPars['fPagamentEu'],2,'.',''))." euros (".(number_format($mPars['fPagamentEuPerc'],2,'.',''))." %)</p>
					</td>
				</tr>
					";
					$color='red';
					$bgcolor='white';
				}
				reset($mComandesGrupCompteCes);
			}
			reset($mComandesCompteCes);
		}
		reset($mComandesComptesCes);

		echo "
				
			</table>
			<br>
			<br>
			</td>
		</tr>
	</table>
	<table width='50%' align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td>
			<p>Notes:</p>
			<p>
			* Les ecoxarxes no valorades surten en <font style='color:red;'>lletra vermella</font>
			<br>
			<br>
			* Les ecoxarxes no confiables surten en <font style='background-color:black; color:white;'>fons negre i lletra blanca</font>
			<br>
			<br>
			** el text en negreta <b>RG</b> indica que l'usuaria es la responsable de grup</b>
			</p>
			</td>
		</tr>
	</table>

	<br><br>
	<center><p style='font-size:18px;'>Llistat d'ecoxarxes NO confiables segons la CAC</p></center>
	<table align='center' style='width:30%;'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<br>
			
			<table border='1' align='center' style='width:100%;'>
	";

	while(list($codiEcx,$mEcx)=each($mExcNoConfiables))
	{
		echo "
				<tr>
					<td style='width:50%;' align='center'>
					<p style='color:white; background-color:black;'>".$codiEcx."</p>
					</td>
					<td style='width:50%; ' align='center'>
					<p>".$mEcx[0]."</p>
					</td>
				</tr>
		";
	}
	reset($mExcNoConfiables);

	echo "
				
			</table>
			</td>
			

		</tr>
	</table>
	
	<br><br><br>&nbsp;
";
html_helpRecipient();

echo "
<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='comandes.php?".(md5(date('dmyhis')))."'>
<input type='hidden' id='i_usuariId' name='i_usuariId' value='".@$mPars['usuariId']."'>
<input type='hidden' id='i_grupId' name='i_grupId' value='".@$mPars['grupId']."'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='i_selRuta' name='i_selRuta' value='".$mPars['selRutaSufix']."'>
</form>
</div>
	
</body>
</html>

";

?>

		