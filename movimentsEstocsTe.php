<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "html_movimentsEstocs.php";
include "html_ajuda1.php";
include "db_ajuda.php";
include "eines.php";
include "db_gestioMagatzems.php";
include "db_movimentsEstocs.php";

$mode='magatzem';


$missatgeAlerta='';
$parsChain=@$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

if(!isset($mPars['refMagatzemDesti'])){$mPars['refMagatzemDesti']='';}

$RMD=@$_GET['RMD'];
if(isset($RMD)){$mPars['refMagatzemDesti']=$RMD;}


	$mPars['sortBy']='id';
	$mPars['ascdesc']='ASC';

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db);
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['movimentsEstocsTe.php']=db_getAjuda('movimentsEstocsTe.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}


$mUsuari=db_getUsuari($mPars['usuari_id'],$db);

$mRebost=db_getRebost($db);

$mMagatzems=db_getMagatzems($db);


$mProductors=db_getPerfilsRef($db);
$mProductes=db_getProductes2($db);

$mRebosts=db_getMagatzems($db);
$mRebostsRef=db_getRebostsRef($db);

$mInventariG=db_getInventari($mPars['grup_id'],$db);

	$enviamentsPendents=0;
	$recepcionsPendents=0;
$mRevisionsRef=getRevisionsRef($mInventariG['revisions']);

$mInventariGDesti=array();
if($mPars['refMagatzemDesti']!='')
{
	$mInventariGDesti=db_getInventari($mPars['refMagatzemDesti'],$db);
}
	//gets

	$TEpO='';
	$TEq='';
	$TEc='';

	if($OptGET=@$_GET['OG'])
	{
		if($OptGET=='TE' && $timeGuardarRevisio=@$_GET['TEtgr'])
		{
			$TEpO=@$_GET['TEpO']; // Transferencia externa - id producte Origen
			$TEq=@$_GET['TEq']; // Transferencia externa - quantitat a transferir
			$TEc=urlencode(@$_GET['TEc']); // Transferencia externa - concepte de la transf. externa
			//guardar transferencia externa
			
			$mMissatgeAlerta=guardarTransferenciaExterna('TE',$timeGuardarRevisio,$TEpO,$TEq,$TEc,$db); //sempre guarda sobre l�ltim inventari
			$mInventariG=db_getInventari($mPars['grup_id'],$db);
			$mInventariGDesti=db_getInventari($mPars['refMagatzemDesti'],$db);
			$enviamentsPendents=0;
			$recepcionsPendents=0;
			$mRevisionsRef=getRevisionsRef($mInventariG['revisions']);
		}
	}
	
	if($VR=@$_GET['VR'])
	{
		$iId=@$_GET['iId'];
		$iIo=@$_GET['iIo'];
		if($VR!='' && $iId!='' && $iIo!='')
		{
			$mMissatgeAlerta=db_validarTE($VR,$iIo,$iId,$db);
			if(!$mMissatgeAlerta['result'])
			{
				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut val.lidar la recepci� del producte</p>";
			}
			else
			{
				$missatgeAlerta=$mMissatgeAlerta['missatge'];
			}
			$mInventariG=db_getInventari($mPars['grup_id'],$db);
			$mInventariGDesti=db_getInventari($mPars['refMagatzemDesti'],$db);
			$enviamentsPendents=0;
			$recepcionsPendents=0;
			$mRevisionsRef=getRevisionsRef($mInventariG['revisions']);
		}
	}

	if($AR=@$_GET['AR'])
	{
		$iId=@$_GET['iId'];
		$iIo=@$_GET['iIo'];
		if($AR!='' && $iId!='' && $iIo!='')
		{
			$mMissatgeAlerta=db_anularTE($AR,$iIo,$iId,$db);
			if(!$mMissatgeAlerta['result'])
			{
				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut val.lidar la recepci� del producte</p>";
			}
			else
			{
				$missatgeAlerta=$mMissatgeAlerta['missatge'];
			}
			$mInventariG=db_getInventari($mPars['grup_id'],$db);
			$mInventariGDesti=db_getInventari($mPars['refMagatzemDesti'],$db);
			$enviamentsPendents=0;
			$recepcionsPendents=0;
			$mRevisionsRef=getRevisionsRef($mInventariG['revisions']);
		}
	}

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>"; echo $htmlTitolPags; echo " - Transferencies de producte</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js1_movimentsEstocsTe.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<script type='text/javascript'  language='JavaScript'>

navegador();
missatgeAlerta=\"".$mMissatgeAlerta['missatge']."\";
confirmacioAnularText='Cal que seleccionis el checkBox per confirmar que vols anul.lar la transferencia';

var mProductes=new Array();
//mProductesI=new Array();
mProductesIO=new Array();

	//definir mProductes jasvascript
";
$n=0;
while(list($index,$val)=each($mProductes))
{
	echo "
mProductes[".$n."]=new Array();
mProductes[".$n."]['id']='".$index."';
mProductes[".$n."]['pes']=".(str_replace(',','.',$mProductes[$index]['pes'])).";
mProductes[".$n."]['volum']=".(str_replace(',','.',$mProductes[$index]['volum'])).";
mProductes[".$n."]['categoria10']=\"".$mProductes[$index]['categoria10']."\";
mProductes[".$n."]['format']=\"".$mProductes[$index]['format']."\";
mProductesIO['".$index."']=new Array();
	";
	//carregar inventari origen sobre productes magatzem origen:
	if(isset($mInventariG['inventariRef'][$index]))
	{
		echo "
mProductes[".$n."]['quantitat']='".$mInventariG['inventariRef'][$index]."';
mProductesIO['".$index."']['quantitat']='".$mInventariG['inventariRef'][$index]."';
		";
	}
	else
	{
		echo "
mProductes[".$n."]['quantitat']='0';
mProductesIO['".$index."']['quantitat']='0';
		";
	}

	$n++;
}
reset($mProductes);

echo "
</script>
</head>
<body onload=\"javascript: f_missatgeAlerta();\" bgcolor='".$mColors['body']."'>
";
html_demo('movimentsEstocsTe.php?');
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			<p>(".$mContinguts['form']['titol'].")</p>
			</td>
		</tr>
	</table>
	
	<table style='width:80%;' align='center' v>
		<tr>
			<td style='width:10%'>
			</td>
			<td id='td_missatgeAlerta' style=' width:80%;' align='center'  valign='top'>
			</td>
		</tr>
	</table>
	
	<table border='0' align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td align='center' style='width:100%'>
			<center><p>&nbsp;&nbsp;[ Magatzem-CAC <b>".(urldecode($mRebost['nom']))."</b>: Transferencies de producte ] </p></center>

			<table bgcolor='#FFFFFF' BORDER='0' align='center'  style='width:100%'>
			<tr>
				<td align='center'>
				<p>Seleccionar magatzem-CAC de dest�: 
				<select id='sel_TEidMagatzemDesti' onChange=\"javascript: selectMagatzemDesti();\">
";
$selected1='';
$selected2='selected';
while(list($key,$mMagatzem)=each($mRebosts))
{
	if($mMagatzem['ref']!=$mPars['grup_id'])
	{
		if($mMagatzem['ref']==$mPars['refMagatzemDesti'])
		{
			$selected1='selected';
			$selected2='';
		}
		echo "
				<option ".$selected1." value='".$mMagatzem['ref']."'>".(urldecode($mMagatzem['nom']))."</option>
		";
		$selected1='';
	}
}
reset($mRebosts);
echo "
				<option ".$selected2." value=''></option>
				</select> 
				</p>
				</td>
			</tr>
			</table>
";
if($mPars['vProductor']!='TOTS')
{
	echo "
			<center><p class='pAlertaNo4'>* Nom�s es mostren els registres del Productor <b>".(urldecode($mProductors[$mPars['vProductor']]['projecte']))."</b></p></center>
	";
}

if(isset($mPars['refMagatzemDesti']) && $mPars['refMagatzemDesti']!='')
{
	echo "
			<table bgcolor='#FFFFFF' BORDER='1' align='center'  style='width:100%'>
				<tr>
					<td align='center'>
					<p>Transferir unitats desde productes amb estoc positiu d'aquest magatzem-CAC al magatzem-CAC seleccionat:</p>

					<table style='width:90%' align='center'>
						<tr>
							<td align='left' valign='top'>
							<p>
							</p>
							</td>

							<td align='left' valign='top' width='30%'>
							<font color='#0000ff'>[Inventari: ".(urldecode($mRebostsRef[$mPars['grup_id']]['nom']))."]</font>
							<p>
							Selecciona producte a transferir:
							</p>
							<p>
							<select id='sel_TE_idProducteOrigen' onChange=\"javascript: document.getElementById('sel_TE_idProducteDesti').value=this.value;document.getElementById('sel_TE_idProducteDesti').selected=true;\">
	";
	$selected='';
	$selected2='selected';
	while(list($index,$quantitat)=each($mInventariG['inventariRef']))
	{
		if(isset($mProductes[$index]))
		{
			if($index==$TEpO)
			{
				$selected='selected';
				$selected2='';
			}
			$espais10='          ';
			$indexProducte=$index.' - '.(urldecode($mProductes[$index]['producte']));
			$indexProducte.='...'.$espais10.$espais10.$espais10.$espais10.$espais10.$espais10;  //60 caracters
			$indexProducte=substr($indexProducte,0,60);
			echo "
							<option ".$selected." value='".$index."'>".$indexProducte."&nbsp;&nbsp;&nbsp;[estoc:".$quantitat."]&nbsp;&nbsp;&nbsp;[Format:".$mProductes[$index]['format']."]&nbsp;&nbsp;&nbsp;[Subcategoria:".$mProductes[$index]['categoria0']."]</option>
			";
			$selected='';
		}
	}
	reset($mInventariG['inventariRef']);
	echo "
							<option ".$selected2." value=''></option>
						</select>
						</p>
						</td>

						<td   valign='top'  width='10%'>
						</td>
					</tr>

					<tr>
						<th align='left' valign='top'><p>&nbsp;</p></th>
						<td valign='top' align='left' >	</td>
						<td   valign='top'>	</td>
					</tr>

					<tr>

						<td align='left' valign='top'>
						</td>

						<td valign='top' align='middle' valign='middle'>
						<table border='0' style='width:100%;'>
							<tr>
								<td align='left' valign='top'  style='width:40%;'>
								<p>
								Quantitat a transferir:
								</p>
								<p><input id='i_TE_quantitat' type='text'  size='6' value=''> (uts.)</p>
								</td>
								<td align='center' valign='middle'   >
								<font size='6'><b>&darr;</b></font>
								</td>
								<td align='center' valign='middle'   style='width:10%;'>
								<input type='button' 
								";
								if($mPars['selRutaSufix']!=date('ym')){echo "disabled";}
								echo "
								onClick=\"javascript:transferenciaExterna();\" value='transferir'>
								</td>
								<td align='center' valign='middle'   >
								<font size='6'><b>&darr;</b></font>
								</td>
								<td align='left' valign='middle'  style='width:40%;'>
								</td>
							</tr>								
						</table>
						</td>

						<td   valign='top'>
						</td>
					</tr>
					
					<tr>
						<th align='left' valign='top'><p>&nbsp;</p></th>
						<td valign='top' align='left' >	</td>
						<td   valign='top'>	</td>
					</tr>

					<tr>
						<td align='left' valign='top'>
						<p>
						</p>
						</td>

						<td align='left' valign='bottom'>
						<font color='#0000ff'>[Inventari: ".(urldecode($mRebostsRef[$mPars['refMagatzemDesti']]['nom']))."]</font>
						<p>
						(nom�s informatiu)
						</p>
						<p>
						<select DISABLED id='sel_TE_idProducteDesti'>
	";
	$selected='';
	$selected2='selected';
	while(list($index,$mProducte)=each($mProductes))
	{
		if($index==$TEpO)
		{
			$selected='selected';
			$selected2='';
		}

		if(!isset($mInventariGDesti['inventariRef'][$TEpO]))
		{
			$quantitat=0;
		}
		else
		{
			$quantitat=$mInventariGDesti['inventariRef'][$TEpO];
		}
		$espais10='          ';
		$indexProducte=$index.' - '.(urldecode($mProductes[$index]['producte']));
		$indexProducte.='...'.$espais10.$espais10.$espais10.$espais10.$espais10.$espais10;  //60 caracters
		$indexProducte=substr($indexProducte,0,60);
		echo "
						<option ".$selected." value='".$index."'>".$indexProducte."&nbsp;&nbsp;&nbsp;<b>[estoc:".$quantitat."]</b>&nbsp;&nbsp;&nbsp;[Format:".$mProductes[$index]['format']."]&nbsp;&nbsp;&nbsp;[Subcategoria:".$mProductes[$index]['categoria0']."]</option>
		";
		$selected='';
	}
	reset($mProductes);
	echo "
						<option ".$selected2." value=''></option>
						</select>
						</p></td>

						<td   valign='top'>
						</td>
					</tr>

					<tr>
						<th align='left' valign='top'><p>&nbsp;</p></th>
						<td valign='top' align='left' >	</td>
						<td   valign='top'>	</td>
					</tr>

					<tr>
						<td  valign='top' align='left'>
						</td>

						<td valign='top' align='left' >
						<p>
						Notes:
						</p>
						<textarea id='i_TE_concepte' type='text'  cols='50' rows='5'>Reubicaci�</textarea>
						</td>
					</tr>
				</table>
				<br>
	";
}
echo "
			<table bgcolor='#FFFFFF' BORDER='0' align='center'  style='width:100%'>
			<tr>
				<td align='center' width:100%;>
				<p>[ Historic de revisions De transferencies de producte ]</p>
				
				<p class='p_micro2'> [<b><font style='color:";
				if($recepcionsPendents>0){echo "red";}else{echo "#000000";}
				echo ";'>".$recepcionsPendents."</font></b>] recepcions que encara no has val.lidat</p>
				
				<p class='p_micro2'> [<b><font style='color:";
				if($enviamentsPendents>0){echo "red";}else{echo "#000000";}
				echo ";'>".$enviamentsPendents."</font></b>] enviaments que encara no t'han val.lidat</p>
				
				<p class='nota'>* �ltim inventari: <i>".$mInventariG['data'].'-'.(urldecode($mInventariG['autor']))."</i></p>
";
$mRevisions=html_getRevisions($mInventariG['revisions'],$OptGET);
//vd($mRevisions);
if(count($mRevisions)>0)
{
	echo "
				<div style='background-color:#eeeeee; width:100%;'>
				<table  style='width:100%' border='0' align='center'>
	";
	$cont=0;
	while(list($key,$mRevisio)=each($mRevisions))
	{
		if(array_key_exists($mRevisio[4],$mProductes))
		{
			if($cont==0)
			{
				echo "
					<tr>
						<th align='left' valign='top' width='5%'>
						<p style='font-size:10px;'>Data</p>
						</td>

						<th align='left' valign='top' width='5%'>
						<p style='font-size:10px;'>Autor</p>
						</td>
						
						<th align='left' valign='top' width='10%'>
						<p style='font-size:10px;'>Origen</p>
						</td>

						<th align='left' valign='top' width='5%'>
						<p style='font-size:10px;'>id<br>producte<br>origen</p>
						</td>

						<th align='left' valign='top' width='15%'>
						<p style='font-size:10px;'>nom<br>producte<br>origen</p>
						</td>

						<th align='left' valign='top' width='10%'>
						<p style='font-size:10px;'>Quantitat<br>transferida</p>
						</td>

						<th align='left' valign='top' width='31%'>
						<p style='font-size:10px;'>Notes</p>
						</td>

						<th align='left' valign='top' width='10%'>
						<p style='font-size:10px;'>Desti</p>
						</td>

						<th align='left' valign='top' width='15%'>
						<p style='font-size:10px;'>Estat</p>
						</td>
					</tr>
					";
					$cont++;
				}
				echo "
					<tr>
						<td align='left' valign='top' width='5%'>
						<p style='font-size:10px;'>".date('h:i:s d/m/Y',($mRevisio[1])/1000)."</p>
						</td>

						<td align='left' valign='top' width='5%'>
						<p style='font-size:10px;'>".(urldecode($mRevisio[2]))."</p>
						</td>

						<td align='left' valign='top' width='10%'>
						<p style='font-size:10px;'>".(@urldecode($mRebostsRef[$mRevisio[3]]['nom']))."</p>
						</td>

						<td align='left' valign='top' width='5%'>
						<p  style='font-size:10px;'>".$mRevisio[4]."</p>
						</td>

						<td align='left' valign='top' width='15%'>
						<p style='font-size:10px;'>".(urldecode($mProductes[$mRevisio[4]]['producte']))."</p>
						</td>

						<td align='left' valign='top' width='10%'>
						<p style='font-size:10px;'>".$mRevisio[5]."</p>
						</td>

						<td align='left' valign='top' width='31%'>
						<p style='font-size:10px;'>".(urldecode($mRevisio[7]))."</p>
						</td>

						<td align='left' valign='top' width='10%'>
						<p style='font-size:10px;'>".(@urldecode($mRebostsRef[$mRevisio[8]]['nom']))."</p>
						</td>

						<th align='left' valign='top' width='14%'>
			";
			if
			(
				@$mRevisio[9]=='pendent' 
			)
			{
					echo "
						<p>PENDENT</p>
					";
				if($mPars['grup_id']==@$mRevisio[8])
				{
					echo "
						<input type='button' class='i_micro' style='color:red;' onClick=\"javascript: enviarFpars('movimentsEstocsTe.php?VR=".$mRevisio[1]."&iIo=".@$mRevisio[10]."&iId=".@$mRevisio[11]."','_self');\" value='val.lidar'>
					";
					if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
					{
						echo "
						<br>
						<table  bgcolor='orange'>
							<tr>
								<td>
								<input type='checkBox' id='ck_anular_".$mRevisio[1]."' value='-1' onClick=\"val=this.value;val*=-1;this.value=val;\">&nbsp;&nbsp;&nbsp;
								</td>
								
								<td>
								<p class='p_micro2'>						
								<input type='button' class='i_micro' style='color:red;' onClick=\"
								javascript: 
								val=document.getElementById('ck_anular_".$mRevisio[1]."').value;
								if(val==1)
								{
									enviarFpars('movimentsEstocsTe.php?AR=".$mRevisio[1]."&iIo=".@$mRevisio[10]."&iId=".@$mRevisio[11]."','_self');
								}
								else
								{
									alert (confirmacioAnularText);
								}
								\" value='anul.lar'>
								[".$mPars['nivell']."]
								</p>
								</td>
							</tr>
						</table>
						";	
					}
				}
			}
			else if(@substr($mRevisio[9],0,4)=='anul')
			{
				echo "
							<p class='pAlertaNo5'>".@$mRevisio[9]."</p>
				";
			}
			else if(@substr($mRevisio[9],0,4)=='val.')
			{
				echo "
							<p class='pAlertaOk5'>".@$mRevisio[9]."</p>
				";
			}
			else
			{
				echo "
							<p class='p_micro2'><b>".@$mRevisio[9]."</b></p>
				";
			}
			
			echo "
						</td>
				
					</tr>
			";
		}
	}
	reset($mRevisions);
	echo "
				</table>
				</div>
	";
}
else
{
	echo "
					<p>No hi ha revisions d'aquest Inventari</p>
	";
}
echo "
					<br><br>&nbsp;
				</td>
			</tr>
		</table>
";
html_helpRecipient();

$parsChain=makeParsChain($mPars);

echo "		
<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='movimentsEstocs.php?".(md5(date('dmyhis')))."'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='i_selRuta' name='i_selRuta' value='".$mPars['selRutaSufix']."'>

</form>
</div>		
	</body>
</html>
";
	

?>

		