<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "html_movimentsEstocs.php";
include "html_ajuda1.php";
include "db_ajuda.php";
include "eines.php";
include "db_gestioMagatzems.php";
include "db_movimentsEstocs.php";

$mode='magatzem';

$missatgeAlerta='';
$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();
$parsChain=makeParsChain($mPars);

	$mPars['sortBy']='id';
	$mPars['ascdesc']='ASC';

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db);
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['movimentsEstocsTi.php']=db_getAjuda('movimentsEstocsTi.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}


$mRebost=db_getRebost($db);
	$mPars['veureProductesDisponibles']=0;
$mProductors=db_getPerfilsRef($db);
$mProductes=db_getProductes2($db);
$mInventariG=db_getInventari($mPars['grup_id'],$db);
	//gets
	if($OptGET=@$_GET['OG'])
	{
		if($OptGET=='TI' && $timeGuardarRevisio=@$_GET['TItgr'])
		{
			$TIpO=@$_GET['TIpO']; // Transferencia Interna - id producte Origen
			$TIpD=@$_GET['TIpD']; // Transferencia Interna - id producte Desti
			$TIq=@$_GET['TIq']; // Transferencia Interna - quantitat a transferir
			$TIc=urlencode(@$_GET['TIc']); // Transferencia Interna - concepte de la transf. interna

			//guardar transferencia interna
			$mMissatgeAlerta=guardarTransferenciaInterna('TI',$timeGuardarRevisio,$TIpO,$TIpD,$TIq,$TIc,$db); //sempre guarda sobre l�ltim inventari
			$mInventariG=db_getInventari($mPars['grup_id'],$db);
		}
	}


echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>"; echo $htmlTitolPags; echo " - Canvis de format</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js1_movimentsEstocsTi.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<script type='text/javascript'  language='JavaScript'>

navegador();
missatgeAlerta=\"".$mMissatgeAlerta['missatge']."\";

var mProductes=new Array();
mProductesI=new Array();

	//definir mProductes jasvascript
";
$n=0;
while(list($index,$val)=each($mProductes))
{
	echo "
mProductes[".$n."]=new Array();
mProductes[".$n."]['id']='".$index."';
mProductes[".$n."]['pes']=".(str_replace(',','.',$mProductes[$index]['pes'])).";
mProductes[".$n."]['volum']=".(str_replace(',','.',$mProductes[$index]['volum'])).";
mProductes[".$n."]['categoria10']=\"".$mProductes[$index]['categoria10']."\";
mProductes[".$n."]['format']=\"".$mProductes[$index]['format']."\";
	";
	//carregar inventari origen sobre productes magatzem origen:
	if(isset($mInventariG['inventariRef'][$index]))
	{
		echo "
mProductes[".$n."]['quantitat']='".$mInventariG['inventariRef'][$index]."';
mProductesI['".$index."']=new Array();
mProductesI['".$index."']['quantitat']=".$mInventariG['inventariRef'][$index]."
		";
	}
	else
	{
		echo "
mProductes[".$n."]['quantitat']='0';
		";
	}
	$n++;
}
reset($mProductes);

echo "
</script>
</head>
<body onload=\"javascript: f_missatgeAlerta();\" bgcolor='".$mColors['body']."'>
";
html_demo('movimentsEstocsTi.php?');
echo "
	<table align='center' style='width:80%' bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			<p>(".$mContinguts['form']['titol'].")</p>
			</td>
		</tr>
	</table>

	<table style='width:80%;' align='center' bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:10%'>
			</td>
			<td id='td_missatgeAlerta' style=' width:80%;' align='center'  valign='top'>
			</td>
		</tr>
	</table>
";
if($mPars['vProductor']!='TOTS')
{
	echo "<center><p  class='pAlertaNo4'>* Nom�s es mostren els registres del Productor <b>".(urldecode($mProductors[$mPars['vProductor']]['projecte']))."</b></p></center>";
}
echo "	
	<table border='0' align='center' style='width:80%' bgcolor='".$mColors['table']."'>
		<tr>
			<td align='center' style='width:100%'>
			<center><p>&nbsp;&nbsp;[ Magatzem-CAC <b>".(urldecode($mRebost['nom']))."</b>: Canvis de format ] </p></center>
			<table bgcolor='#FFFFFF' BORDER='1' align='center'  style='width:100%'>
			<tr>
				<td align='center'>
				<br>
				<table style='width:90%' align='center'>
					<tr>
						<th align='left' valign='top' width='30%'>
						<p>Producte origen</p>
						</th>

						<td align='left' valign='top'  width='60%'>
						<select id='sel_TI_idProducteOrigen'>
						";
						$selected='';
						$selected2='selected';
						while(list($index,$quantitat)=each($mInventariG['inventariRef']))
						{
							if(isset($mProductes[$index]['producte']))
							{
								if($quantitat>0)
								{
									if($index==@$TIpO)
									{
										$selected='selected';
										$selected2='';
									}
									echo "
						<option ".$selected." value='".$index."'>".$index." - ".(urldecode($mProductes[$index]['producte']))."&nbsp;&nbsp;&nbsp;[estoc:".$quantitat."]&nbsp;&nbsp;&nbsp;[Format:".$mProductes[$index]['format']."]&nbsp;&nbsp;&nbsp;[Subcategoria:".$mProductes[$index]['categoria0']."]</option>
									";
									$selected='';
								}
							}
						}
						reset($mInventariG['inventariRef']);
						echo "
						<option ".$selected2." value=''></option>
						</select>
						</td>

					</tr>
					<tr>

						<th align='left' valign='middle'>
						<p>
						Quantitat a transferir
						</p>
						</th>

						<td align='center' valign='middle'>
						<table border='0' style='width:100%;'>
							<tr>
								<td align='left' valign='middle'  style='width:40%;'>
								<p><input id='i_TI_quantitat' type='text'  size='6' value=''> (uts.)</p>
								</td>
								<td align='center' valign='middle'   >
								<font size='6'><b>&darr;</b></font>
								</td>
								<td align='center' valign='middle'   style='width:10%;'>
								<input type='button' "; 
								
								if($mPars['selRutaSufix']!=date('ym')){echo "disabled";}
								
								echo " onClick=\"javascript:transferenciaInterna();\" value='transferir'>
								</td>
								<td align='center' valign='middle'   >
								<font size='6'><b>&darr;</b></font>
								</td>
								<td align='left' valign='middle'  style='width:40%;'>
								</td>
							</tr>								
						</table>
						</td>

						<td   valign='top'>
						</td>
					</tr>
					
					<tr>
						<th align='left' valign='top'>
						<p>Producte desti</p>
						</th>

						<td align='left' valign='top'>
						<select id='sel_TI_idProducteDesti'>
						";
						$selected='';
						$selected2='selected';
						while(list($index,$mProducte)=each($mProductes))
						{
							if($index==@$TIpD)
							{
								$selected='selected';
								$selected2='';
							}
							if(!@isset($mInventariG['inventariRef'][$TIpD]))
							{
								$quantitat=0;
							}
							else
							{
								$quantitat=$mInventariG['inventariRef'][$TIpD];
							}
							echo "
						<option ".$selected." value='".$index."'>".$index." - ".(substr(urldecode($mProducte['producte']),0,50))."...&nbsp;&nbsp;&nbsp;[estoc:".$quantitat."]&nbsp;&nbsp;&nbsp;[Format:".$mProducte['format']."]&nbsp;&nbsp;&nbsp;[Subcategoria:".$mProducte['categoria0']."]</option>
							";
							$selected='';
						}
						reset($mProductes);
						echo "
						<option ".$selected2." value=''></option>
						</select>
						</td>
					</tr>
					<tr>
						<th  valign='top' align='left'>
						<p>
						Notes
						</p>
						</th>

						<td valign='top' align='left' >
						<textarea id='i_TI_concepte' type='text'  cols='50' rows='5'>Canvi de format</textarea>
						</td>
					</tr>
				</table>
				<br>
			<table bgcolor='#FFFFFF' BORDER='0' align='center'  style='width:100%'>
			<tr>
				<td align='center'>
				<p>[ Historic de revisions de canvi de format ]</p>
				<p class='nota'>�ltim inventari: <i>".$mInventariG['data'].'-'.(urldecode($mInventariG['autor']))."</i></p>
				";
				$mRevisions=html_getRevisions($mInventariG['revisions'],$OptGET);
				if(count($mRevisions>0))
				{
					echo "
				<table   style='background-color:#dddddd; width:100%' align='center'>
					<tr>
						<th align='center' valign='top' width='10%'>
						<p>data</p>
						</td>

						<th align='center' valign='top' width='8%'>
						<p>autor</p>
						</td>
						
						<th align='center' valign='top' width='5%'>
						<p>id producte<br>origen</p>
						</td>

						<th align='center' valign='top' width='15%'>
						<p>producte<br>origen</p>
						</td>

						<th align='center' valign='top' width='10%'>
						<p>quantitat<br>transferida</p>
						</td>

						<th align='center' valign='top' width='15%'>
						<p>id producte<br>desti</p>
						</td>

						<th align='center' valign='top' width='5%'>
						<p>producte<br>desti</p>
						</td>
						
						<th align='center' valign='top' width='40%'>
						<p>Notes</p>
						</td>

						<th align='center' valign='top' width='2%'>
						<p></p>
						</td>
					</tr>
				</table>
				<div style='background-color:#eeeeee; width:100%; height:250px; overflow-y:scroll; overflow-x:hidden;'>
				<table style='width:100%' align='center'>
				";
					while(list($key,$mRevisio)=each($mRevisions))
					{
						if(array_key_exists($mRevisio[4],$mProductes))
						{
						//$date = new DateTime();
						//$date->setTimestamp($mRevisio[1]);
						//cal script
							echo "
					<tr>
						<td align='left' valign='top' width='10%'>
						<p style='font-size:10px;'>".date('h:i:s d/m/Y',$mRevisio[1])."</p>
						</td>
						
						<td align='left' valign='top' width='10%'>
						<p style='font-size:10px;'>".(urldecode($mRevisio[2]))."</p>
						</td>

						<td align='left' valign='top' width='5%'>
						<p style='font-size:10px;'>".$mRevisio[3]."</p>
						</td>

						<td align='left' valign='top' width='15%'>
						<p style='font-size:10px;'>".(urldecode($mProductes[$mRevisio[3]]['producte']))."</p>
						</td>

						<td align='left' valign='top' width='10%'>
						<p style='font-size:10px;'>".$mRevisio[4]."</p>
						</td>
						
						<td align='left' valign='top' width='15%'>
						<p style='font-size:10px;'>".(urldecode($mProductes[$mRevisio[4]]['producte']))."</p>
						</td>

						<td align='left' valign='top' width='5%'>
						<p style='font-size:10px;'>".$mRevisio[5]."</p>
						</td>

						<td align='left' valign='top' width='60%'>
						<p style='font-size:10px;'>".(urldecode($mRevisio[6]))."</p>
						</td>
					</tr>
					";
						}
					}
					reset($mRevisions);
					echo "
				</table>
				<br>
				<br>
				&nbsp;
					";
				}
				else
				{
					echo "
					<p>No hi ha revisions d'aquest Inventari</p>
					";
				
				}
					echo "
				</td>
			</tr>
		</table>
";
html_helpRecipient();
echo "
<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='movimentsEstocs.php?".(md5(date('dmyhis')))."'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='i_selRuta' name='i_selRuta' value='".$mPars['selRutaSufix']."'>

</form>
</div>		
	</body>
</html>
";
	

?>

		