function mostrarSelectorsData(idTipus)
{
	if(idTipus!='')
	{
		if(mTipusUpload[idTipus]['periodicitat']=='variable' || mTipusUpload[idTipus]['periodicitat']=='mensual' || mTipusUpload[idTipus]['periodicitat']=='anual')
		{
			document.getElementById('d_data').style.visibility='inherit';
			document.getElementById('d_data').disabled=false;
			document.getElementById('d_dir').style.visibility='hidden';
			document.getElementById('sel_directori').disabled=true;
		}
		else
		{
			document.getElementById('d_data').style.visibility='hidden';
			document.getElementById('d_data').disabled=true;
			document.getElementById('d_dir').style.visibility='inherit';
			document.getElementById('sel_directori').disabled=false;
		}
	}
	else
	{
		document.getElementById('d_data').style.visibility='hidden';
		document.getElementById('d_data').disabled=true;
		document.getElementById('d_dir').style.visibility='inherit';
		document.getElementById('sel_directori').disabled=false;
	}
	
	return;
}
		