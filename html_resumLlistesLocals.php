<?php

//------------------------------------------------------------------------------
function html_resumLlistesLocals()
{
	global $mPars,$mComandesLlistesLocals,$mGrupsRef,$mUsuarisRef,$mPeriodes,$mParametres;
	
	
	$mColor=array();
	$mColor[1]='#9FdEe0'; //CadetBlue
	$mColor[-1]='#7FbEc0';
	$colorIndex=1;
	$mEstat=array();
	$mEstat[1]="<font color='red'><b>TANCAT</b></font>";
	$mEstat[0]='------';
	$mEstat[-1]="<font color='green'><b>OBERT</b></font>";
	
	$dataFiReservesLocals = new DateTime();
	$iniciMesEnCurs = new DateTime();
	$iniciMesEnCurs->setDate('20'.(substr($mPars['selRutaSufix'],0,2)), (substr($mPars['selRutaSufix'],2,4))-5, 0 );
	$instantIniciMesEnCurs=$iniciMesEnCurs->getTimestamp();
	$fiMesEnCurs = new DateTime();
	$fiMesEnCurs->setDate('20'.(substr($mPars['selRutaSufix'],0,2)), (substr($mPars['selRutaSufix'],2,4))+1, 0 );
	$instantFiMesEnCurs=$fiMesEnCurs->getTimestamp();
	
	echo "
	<table border='0' align='center' width='35%' bgcolor='#DB9EFA'>
		<tr>
			<th  style='width:70%;' valign='top'>
			<p class='p_micro'>activitat Total (6 darrers mesos)</p>
			</th>
			<td  style='width:15%;' align='left'  valign='top'>
			<p class='p_micro' id='p_activitatTotalUtsC'></p>
			<p class='p_micro' id='p_activitatTotalEcosC'></p>
			</td>
			<td  style='width:15%;' align='left'  valign='top'>
			<p class='p_micro'> unitats</p>
			<p class='p_micro'> ecos</p>
			</td>
		</tr>
		<tr>
			<th  valign='top'>
			<p class='p_micro'>maxim num productes assignats periode actual</p>
			</th>
			<td  align='left'  valign='top'>
			<p class='p_micro' id='p_maxNumProductes' >".$mParametres['numProductesGrupsMes']['valor']."</p>
			</td>
			<td  align='left'  valign='top'>
			<p class='p_micro'> productes</p>
			</td>
		</tr>
		<tr>
			<th  valign='top'>
			<p class='p_micro'>factorMax</p>
			</th>
			<td  align='left'  valign='top'>
			<p class='p_micro' id='p_factorMax'></p>
			</td>
			<td  align='left'  valign='top'>
			<p class='p_micro'> punts</p>
			</td>
		</tr>
	</table>
	<br><br>

	<table border='1' style='width:90%;'>
		<tr>
			<th  style='width:39%;'>
			<p class=''>Llista ".(html_ajuda1('html_resumLlistesLocals.php',1))."</p>
			</th>
			<th  style='width:30%;'>
			<p class=''>periodes locals</p>
			</th>
			<th  style='width:30%;'>
			<p class=''>comandes</p>
			</th>
		</tr>
	</table>
	";
	while(list($grupId,$mComandesGrup)=each($mComandesLlistesLocals))
	{
		echo "
	<div  style='width:90%; height:110px; overflow-y:scroll;' >
	<table style='width:100%;' border='1' bgcolor='".$mColor[$colorIndex]."'>
		<tr>
		";
		$cont1=0;

		$totalUtsGrup6mesos=0;
		$totalEcosGrup6mesos=0;

		while(list($periodeId,$mComandesPeriode)=each($mComandesGrup))
		{
			$utsGrup6mesos=0;
			$ecosGrup6mesos=0;
			
			$bgColorError=$mColor[$colorIndex];
			$cont2=0;
				$mPropietatsPeriodeLocal=getPropietatsPeriodeLocal($mPeriodes[$periodeId]['propietats']);
				
				$mDates=@explode(':',$mPeriodes[$periodeId]['periode_comanda']);
				//$mDataIniciReservesLocals=@explode('-',$mDates[0]);
				$mDataFiReservesLocals=@explode('-',$mDates[1]);

				$dataFiReservesLocals->setDate('20'.$mDataFiReservesLocals[2], $mDataFiReservesLocals[1], $mDataFiReservesLocals[0] );
				$instantFi=$dataFiReservesLocals->getTimestamp();
				//si el moment de fi de reserves del periode esta dins el mes 
				////en curs s'inclouen dades grup per a assignacio nous productes
				
				if($instantFi>=$instantIniciMesEnCurs && $instantFi<=$instantFiMesEnCurs)
				{
					while(list($usuariId,$mComandaUsuari)=each($mComandesPeriode))
					{
						
						$mProductesComanda=explode(';',$mComandaUsuari['resum']);
						$n=0;
						for($i=0;$i<count($mProductesComanda);$i++)
						{
							$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
							$index=str_replace('producte_','',$mIndexQuantitat[0]);
							$quantitat=@$mIndexQuantitat[1];
							if($index!='' && $index!=0)
							{
								$mComanda[$n]['index']=$index;
								$mComanda[$n]['quantitat']=$quantitat;
								$n++;
								$utsGrup6mesos+=$quantitat;
							}
						}
						$mFormaPagament=explode(',',$mComandaUsuari['f_pagament']);
						if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
						if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
						if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
						if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
						if(!isset($mFormaPagament[4])){$mFormaPagament[4]=0;}
						if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
						if(!isset($mFormaPagament[6]))
						{
							$mFormaPagament[6]=100;
							$mFormaPagament[7]=0;
							$mFormaPagament[8]=0;
						}
						$mFormaPagament[9]=0;
						
						$ecosGrup6mesos+=$mFormaPagament[0]*1;
					}
					reset($mComandesPeriode);
				}
				$totalUtsGrup6mesos+=$utsGrup6mesos;
				$totalEcosGrup6mesos+=$ecosGrup6mesos;
					echo "
		<tr>
			<td style='width:39%;' valign='top' >
					";
					if($cont1==0)
					{
						echo "
			<p class=''><b>".(urldecode($mGrupsRef[$grupId]['nom']))."</b></p>
			<table width='95%'>
				<tr>
					<th width='70%' valign='bottom' align='left'>
					<p style='font-size:10px;'>Activitat local (6 darrers mesos) :</p>
					</th>
					<td valign='top' align='left'>
					<p style='font-size:10px;'>".$utsGrup6mesos." uts | ".$ecosGrup6mesos." ecos</p>
					</td>
				</tr>
				<tr>
					<th width='70%' valign='bottom' align='left'>
					<p style='font-size:10px;'>factor: </p>
					</th>
					<td valign='top' align='left'>
					<p id='p_factor_".$grupId."' style='font-size:10px;'>".($utsGrup6mesos*$ecosGrup6mesos)."</p>
					</td>
				</tr>
				<tr>
					<th width='70%' valign='bottom' align='left'>
					<p style='font-size:10px;'>Nombre de productes assignats: </p>
					</th>
					<td valign='top' align='left'>
					<p id='p_numProductes_".$grupId."' style='font-size:10px;'></p>
					</td>
				</tr>
			</table>
						";
					}
					echo "
			</td>
			<td style='width:30%;'  valign='top' bgcolor='".$mColor[$colorIndex]."'>
					";
					if($cont2==0)
					{
						echo "
				<p class='p_micro2'><b>";
						if(($instantFi-time())<0)			
						{
							$caducat='CADUCAT';
							$colorPeriode='red';
							if($mPropietatsPeriodeLocal['comandesLocalsTancades']==-1)
							{
								$bgColorError='black';
							}
						}
						else
						{
							$caducat='';
							$colorPeriode='black';
						}
							echo "<font color='".$colorPeriode."' style='background-color:".$bgColorError.";'>".$mPeriodes[$periodeId]['periode_comanda']."</font>";

						echo "
					</b> ".$mEstat[$mPropietatsPeriodeLocal['comandesLocalsTancades']]." (id: ".$periodeId.")
					<br>".$caducat."
					</p>
						";
					}
					echo "
			</td>
			<td style='width:30%;'  valign='top' bgcolor='".$mColor[$colorIndex]."'>
			<p class='p_micro'>
			";
			if
			(
				$mPars['nivell']=='sadmin' 
				||
				$mPars['nivell']=='admin'
			)
			{
				echo "
				<select onChange=\"javascript: val=this.value; if(val!=''){enviarFpars('comandes.php?sUid='+this.value+'&gId=".$grupId."&sL=".$grupId."&sPl=".$periodeId."','_blank');}\">
				";
			}
			while(list($usuariId,$mComanda)=each($mComandesPeriode))
			{
				echo "
			<option value='".$usuariId."'>".(urldecode($mUsuarisRef[$mComanda['usuari_id']]['usuari']))." (".$mUsuarisRef[$mComanda['usuari_id']]['email'].")</option>
				";
			}
			reset($mComandesPeriode);
			
			echo "
			<option selected value=''></option>
			</select>
			<br>[<b>".(count($mComandesPeriode))."</b> comandes]
			<br>
			&nbsp;
			</td>
		</tr>
					";
					$cont2++;
					$cont1++;
			
		}
		reset($mComandesGrup);
		$colorIndex*=-1;
	echo "
	</table>
	<p style='visibility:hidden;' id='p_activitatTotalUts0'>".$totalUtsGrup6mesos."</p>
	<p style='visibility:hidden;' id='p_activitatTotalEcos0'>".$totalEcosGrup6mesos."</p>
	</div>
	";
	}
	reset($mComandesLlistesLocals);
	


	return;
}
?>

		