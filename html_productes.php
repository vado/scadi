<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include "html_segells.php";
include "html_ajuda1.php";
include "db_ajuda.php";
include "db_gestioGrup.php";


//POST
$parsChain=@$_POST['i_pars'];
if(!isset($parsChain))
{
	$parsChain=@$_GET['iPars'];
}
$idProducte=@$_POST['id'];
$mPars=getPars($parsChain);
$mParams=getParams();
$mRuta=explode('_',$mPars['selRutaSufix']);
if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

if($mPars['selLlistaId']==0)
{
	$mPars['taulaProductes']='productes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandes']='comandes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandesSeg']='comandes_seg';
	$mPars['taulaIncidencies']='incidencies_'.$mPars['selRutaSufix'];
	if(count($mRuta)==2)//periode especial
	{
		$mPars['taulaProductors']='productors_'.$mPars['selRutaSufix'];
	}
	else
	{
		$mPars['taulaProductors']='productors';
	}
}
else
{
	$colorLlista=1;
	$mPars['taulaProductes']='productes_grups';
	$mPars['taulaProductors']='productors';
	$mPars['taulaComandes']='comandes_grups';
	$mPars['taulaComandesSeg']='comandes_seg_grups';
	$mPars['taulaIncidencies']='incidencies_grups';
}

//DB
$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db);


//VARS
$missatgeAlerta='';
$login=false;

//*v36.6-elements matriu
$mColsProductes2['visiblesRebosts']=array('id','segell','preu', 'ms','pes', 'estoc_previst', 'tipus','categoria0','categoria10','format','estoc_disponible');

$mColsProductes2['visiblesCAC']=array('id','segell','producte','productor','tipus','categoria0','categoria10','unitat_facturacio','format','pes','volum','preu','ms','estoc_previst','estoc_disponible','cost_transport_extern_kg','cost_transport_intern_kg');

$mNomsColumnes2=array(
'id'=>'Id',
'segell'=>'Segell ECOCIC',
'producte'=>'Producte',
'productor'=>'Productor',
'actiu'=>'Actiu',
'tipus'=>'Tipus',
'categoria0'=>'Categoria',
'categoria10'=>'Sub-categoria',
'unitat_facturacio'=>'Ut.fact',
'format'=>'Format(pack uts)',
'pes'=>'Pes ut.fact(kg)',
'volum'=>'Volum ut.fact (l)',
'preu'=>'Preu(uts)',
'ms'=>'% MS',
'estoc_previst'=>'Estoc previst',
'estat'=>'estat',
'estoc_disponible'=>'Estoc dispon.',
'cost_transport_extern_kg'=>'cost transp extern (uts/kg)',
'ms_ctek'=>'% ms del cost transport extern per kg',
'cost_transport_intern_kg'=>'cost transp intern (uts/kg)',
'ms_ctik'=>'% ms del cost transport intern per kg'
);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	$login=false;
}
else
{
	$login=true;

}

$mProducte=db_getProducte($idProducte,$db);
$perfilId=substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'));
	$mPars['selPerfilRef']=$perfilId;
$mPerfil=db_getPerfil($db);
	$mPars['selProducteId']=$idProducte;
$mPropietatsSegellProducte=db_getPropietatsSegellProducte($db);	
$mPropietatsSegellPerfil=db_getPropietatsSegellPerfil($db);	
$mPerfilResp=db_getUsuari($mPerfil['usuari_id'],$db);

$mGrup=getGrup($db);
$mSelUsuari=@db_getUsuari($mPars['selUsuariId'],$db);
$mUsuari=db_getUsuari($mPars['usuari_id'],$db);
//*v36.6- assignacio
$mMunicipis=db_getMunicipis2Id($db);

$mPropietatsSegellProducte=db_getPropietatsSegellProducte($db);	
		$mCategoria0perfil=db_getCategoria0Perfil($db);
		$mPropietatsSegellPerfil=db_getPropietatsSegellPerfil($db);	
		$maxValorSegellPerfil=getValorMaxSegell($mPropietatsSegellPerfil,$mCategoria0perfil);

$mAjuda['html_productes.php']=db_getAjuda('html_productes.php',$db);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_html_productes.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>

<script type='text/javascript'  language='JavaScript'>

missatgeAlerta=\"".$missatgeAlerta."\";

</script>


<body bgcolor='#ffffff'  >
";
html_helpRecipient();
echo "
<table style='width:100%;'>
	<tr>
		<th style='width:100%;' valign='middle' align='center'>
		<p style='font-size:15px;'>".(urldecode(marcarRecerca($mProducte['producte'])))."</p>
		</th>
		<td>
		</td>
	</tr>
	<tr>
";

if($mPars['grup_id']=='0'){$mCampsMostrar=$mColsProductes2['visiblesCAC'];}else{$mCampsMostrar=$mColsProductes2['visiblesRebosts'];}

echo "
	
		<td style='width:60%;' valign='top'>
		<table border='0' style='width:100%;'>
			<tr>
				<td style='width:100%;' align='center' valign='middle'>
";
if($mProducte['notes_rebosts']!='')
{
	echo "
				<br>
				<p style='text-align:justify; margin-top:10px; margin-right:10px; margin-bottom: 10px; margin-left: 10px;'>
				".(urldecode(marcarRecerca($mProducte['notes_rebosts'])))."
				</p>";
}
else
{
	echo "		<p style='font-size:25px; color:#aaaaaa;'>
				<i>
				Encara no tenim la descripció d'aquest producte
				</i>
				</p>
	";
} 

if($mPars['selLlistaId']==0)
{
	if(file_exists("productes/".$mProducte['id']."/".$mProducte['imatge']))
	{
		echo "
				<img   src='productes/".$mProducte['id']."/".$mProducte['imatge']."'>
		";
	}
}
else
{
	if(file_exists("llistes/".$mPars['selLlistaId']."/productes/".$mProducte['id']."/".$mProducte['imatge']))
	{
		echo "
				<img   src='llistes/".$mPars['selLlistaId']."/productes/".$mProducte['id']."/".$mProducte['imatge']."'>
		";
	}
}

echo "
				</td>
			</tr>
		</table>
		</td>

		<td  style='width:20%;' valign='top'  >
		<table style='width:100%;' >
	";
while (list($key,$val)=each($mProducte))
{
	if(in_array($key,$mCampsMostrar))
	{
		if($key=='segell')
		{
			echo "
			<tr>
				<th style='width:33%;' align='left' valign='top' bgcolor='#CCD4FA'>
				<p class='nota'>".$mNomsColumnes2[$key]."&nbsp;".(html_ajuda1('html_productes.php',1))."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top' bgcolor='#CCD4FA' >
				<p class='nota'>".(number_format($val,2,'.',''))." %</p>
				</td>
			</tr>
			";
		}
		else if
		(
			$key=='categoria0'
			||
			$key=='categoria10'
			||
			$key=='tipus'
		)
		{			
			echo "
			<tr>
				<th style='width:33%;' align='left' valign='top' bgcolor='#CCD4FA'>
				<p class='nota'>".$mNomsColumnes2[$key]."&nbsp;".(html_ajuda1('html_productes.php',1))."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top' bgcolor='#CCD4FA' >
				<p class='nota'>".(urldecode(marcarRecerca($val)))."</p>
				</td>
			</tr>
			";
		}
		else
		{
			echo "
			<tr>
				<th style='width:33%;' align='left' valign='top' bgcolor='#CCD4FA'>
				<p class='nota'>".$mNomsColumnes2[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top' bgcolor='#CCD4FA' >
				<p class='nota'>".(urldecode($val))."</p>
				</td>
			</tr>
			";
		}
	}
}
reset($mProducte);

echo "

		</table>
		</td>
	</tr>
</table>


	
	<p style='color:#CCD4FA; text-align:left;'><b>_____________________________</b></p>
	<table width='100%'>
		<tr>
			<td  width='30%' valign='top'>
			<table width='100%'>
				<tr>
					<td align='left'>
					<p>Projecte:</p>
					</td>
					<td align='left'>
					<p><b>".(urldecode(marcarRecerca($mPerfil['projecte'])))."</b></p>
					</td>
				</tr>
				<tr>
					<td align='left' valign='top'>
					<p>Ram:</p>
					</td>
					<td align='left'  valign='top'>
					";
					$mRam=explode(',',$mPerfil['ram']);
					while(list($key,$val)=each($mRam))
					{
						echo "
					<p><b>".(urldecode($val))."</b></p>
						";
					}
					reset($mRam);
					echo "
					</td>
				</tr>
				<tr>
					<td align='left' valign='top'>
					<p>Avals:</p>
					</td>
					<td align='left'>
					";
					$mAvals=explode(',',$mPerfil['avals']);
					while(list($key,$val)=each($mAvals))
					{
						echo "
					<p><b>".(urldecode($val))."</b></p>
						";
					}
					reset($mAvals);
					echo "
					</td>
				</tr>
				<tr>
					<td align='left'  valign='top'>
					<p>Responsable del perfil:</p>
					</td>
					<td align='left'  valign='top'>
					<p><b>".(urldecode($mPerfilResp['nom']))." ".(urldecode($mPerfilResp['cognoms']))."</b></p>
					<p>( ".$mPerfilResp['email']." )</p>
					</td>
				</tr>
				<tr>
					<td align='left'  valign='top'>
					<p>Web:</p>
					</td>
					<td align='left'  valign='top'>
					<a href='http://".$mPerfil['web']."' target='_blank'>".$mPerfil['web']."</a>
					</td>
				</tr>
";
//*v36.6- dos linies - urldecode 
echo "
				<tr>
					<td align='left'  valign='top'>
					<p>municipi:</p>
					</td>
					<td align='left'  valign='top'>
					<p><b>".(@urldecode($mMunicipis[$mPerfil['municipi_id']]['municipi']))."</b></p>
					</td>
				</tr>
				<tr>
					<td align='left'  valign='top'>
					<p>Comarca:</p>
					</td>
					<td align='left'  valign='top'>
					<p><b>".(@urldecode($mMunicipis[$mPerfil['municipi_id']]['comarca']))."</b></p>
					</td>
				</tr>
			</table>
			</td>
			
			<td width='40%'>
			";
			if($mPerfil['descripcio']!='')
			{
				echo "
			<table width='100%'>
				<tr>
					<td align='left' valign='top'>
					<p>".(urldecode(marcarRecerca($mPerfil['descripcio'])))."</p>
					</td>
				</tr>
			</table>
				";
			}
			else
			{
				echo "
			<table width='100%'>
				<tr>
					<td align='left' valign='top'>
					<p>[ Encara no disposem de la descripció d'aquest projecte ]</p>
					</td>
				</tr>
			</table>
				";
			}
			
			echo "
			</td>
		</tr>
	</table>

	<br>
	<table width='80%' align='center'>
		<tr>
			<td width='100%'>
			";
			html_segellEcoCicProducte('','');
			echo "
			</td>
		</tr>
	</table>

	<br>
	<table width='80%' align='center'>
		<tr>
			<td width='100%'>
			";
			html_segellEcoCicPerfil('','');
			echo "
			</td>			
		</tr>
	</table>
	



<form id='f_ifrMostrarProducte' name='f_ifrMostrarProducte' method='post' action='html_productes.php' target='_self' style='visibility:inherit;'>
<input type='hidden' id='id' name='id' value=''>
<input type='hidden' id='i_pars1' name='i_pars' value='".$parsChain."'>
</form>

<form id='f_ifrEditarProducte' name='f_ifrEditarProducte' method='post' action='html_editarProducte.php' target='_self' style='visibility:hidden;'>
<input type='hidden' id='i_pars2' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='i_editarProducte_id' name='i_editarProducte_id' value='".$idProducte."'>
</form>

<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='html_productes.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='id' name='id' value='".$idProducte."'>
</form>
</div>

</body>
</html>
";

?>

		