<?php
//////////////////////////////////
//
//  Els usuaris entren sempre directament a la ruta nova, quan s'ha creat en tancar el periode de precomandes
//  Per tant, no poden modificar  la taula contractes de la ruta anterior, ni la taula trams, ja que s'han copiat a les taules noves
//  all� s� es poden modificar si no han estat contractades
//
//////////////////////////////////

include "config.php";
include "einesConfig.php";
include "db.php";
include "html_contractesTrams.php";
include "db_contractesTrams.php";
include "db_gestioVehicles.php";
include "eines.php";
include "html_ajuda1.php";
include "db_ajuda.php";
include "db_mail.php";
	require_once('phpmailer/class.phpmailer.php');
	include("phpmailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mMissatgeAlerta=array();
$mMissatgeAlerta['result']=false;
$mMissatgeAlerta['missatge']="";


$parsChain=@$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$mSelUsuari=array();
	
$mPars['selRutaSufix']=$ruta;

// a transport nom�s es pot accedir a la ruta en curs o a les anteriors
$data1= new DateTime();
$t1=$data1->format('y').$data1->format('m');

if(($t1*1-$mPars['selRutaSufix']*1)<0)
{
	$mPars['selRutaSufix']=$t1;
}

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
getConfig($db); //inicialitza variables anteriors;

	$mPars['sortBy']='sortida';
	$mPars['vh_sortBy']='id';
	$mPars['ascdesc']='DESC';
	$mPars['veureVehiclesActius']=1;
	$mPars['veureTramsActius']=1;
	$mPars['vUsuariId']='TOTS';
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vVehicle']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';
	$mPars['vTipus']='DEMANDES';

	$mPars['fPagamentUms']=0.00;
	$mPars['fPagamentEcos']=0.00;
	$mPars['fPagamentEb']=0.00;
	$mPars['fPagamentEu']=0.00;
	$mPars['fPagamentEuMetalic']=0;
	$mPars['fPagamentEuTransf']=0;
	$mPars['fPagamentEcosIntegralCES']=0;

	$mPars['fPagamentEcosPerc']=0.00;
	$mPars['fPagamentEbPerc']=0.00;
	$mPars['fPagamentEuPerc']=0.00;

	$mPars['paginacio']=1;
	if(!isset($mPars['pagS'])){$mPars['pagS']=0;}
	if(!isset($mPars['pagFiltre'])){$mPars['pagFiltre']=$mPars['sortBy'].$mPars['veureTramsActius'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vVehicle'].$mPars['vSubCategoria'].$mPars['vCategoria'];}
	if(!isset($mPars['numPags'])){$mPars['numPags']=0;} //ve de db_gettrams()
	if(!isset($mPars['numItemsPag'])){$mPars['numItemsPag']=20;} //ve de db_gettrams()
	
	$mPars['veureTramsNoCaducats']=1;


	
if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat (login)</p>
	";
	exit();
	$login=false;
}
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['contractesTrams.php']=db_getAjuda('contractesTrams.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);


//accions get:

$optGet=@$_GET['opt'];
if(isset($optGet))
{
	if($optGet=='delt') //eliminar tram
	{
		$delTramId=@$_GET['tId'];	
		
		$mMissatgeAlerta=db_anularContractesTram($delTramId,$db);
		if(!$mMissatgeAlerta['result'])
		{
			$missatgeAlerta=$mMissatgeAlerta['result'];
		}
		else
		{
			$missatgeAlerta="<p class='pAlertaOk'><i>S'han anul.lat tots els contractes vinculats a la ".$mStTipusPluralAsingular[$mPars['vTipus']].": ".$delTramId." i s'ha notificat a les parts</i></p>";
		}
	}
	
}


$mPars['vRutaIncd']=$mPars['selRutaSufix'];

$mUsuari=db_getUsuari($mPars['usuari_id'],$db);
$mTramsUsuariRef=db_getTramsUsuariRef($db);
$mUsuarisRef=db_getUsuarisRef($db);
$mUsuarisTrams=db_getUsuarisTrams($db);
$mMunicipis=db_getMunicipisId($db);
$mTeam=getTeam($db);
$mCategories=getCategoriesTrams($db);
$mSubCategories=getSubCategoriesTrams($db);
$mRutesSufixes=getRutesSufixes($db);

$mVehiclesUsuari=db_getVehiclesUsuari($mPars['usuari_id'],$db);
$vUsuariId=$mPars['vUsuariId']; 
$mPars['vUsuariId']='TOTS';
$mVehicles=db_getVehicles($db); 
$mPars['vUsuariId']=$vUsuariId;

$missatgeAlerta='';
$infoVisibility='inherit';
$infoPosition='relative';
	
	$tancarSessio=@$_POST['i_tancarSessio'];
	$guardarContractesBit=@$_POST['i_guardarContractesBit'];
	$mGuardarFormaPagament['fPagamentEcos']=array();
	
	$mGuardarFormaPagament['fPagamentEcos']=0.00;
	$mGuardarFormaPagament['fPagamentEb']=0.00;
	$mGuardarFormaPagament['fPagamentEu']=0.00;
	$mGuardarFormaPagament['fPagamentEuMetalic']='';
	$mGuardarFormaPagament['fPagamentCompteEcos']='';
	$mGuardarFormaPagament['fPagamentCompteEb']='';
	$mGuardarFormaPagament['fPagamentEcosIntegralCES']='';
	

	if($vCategoria=@$_POST['i_vCategoria']) //afecta a db_gettrams($db); 
	{
		$mPars['vCategoria']=$vCategoria;
	}

	if($vSubCategoria=@$_POST['i_vSubCategoria']) //afecta a db_gettras($db);
											//s'envien tant la categoria com la subcategoria com a unica cadena
	{
		$mPars['vSubCategoria']=$vSubCategoria;
	}

	if($vVehicle=@$_POST['i_vVehicle'])
	{
		$mPars['vVehicle']=$vVehicle;
	}

	if($vUsuariId=@$_POST['i_vUsuariId'])
	{
		$mPars['vUsuariId']=$vUsuariId;
	}

	//filtre trams
	if($etiqueta=@$_POST['i_etiqueta']) //afecta a db_gettrams($db);
	{
		$mPars['etiqueta']=$etiqueta;
	}

	if($etiqueta2=@$_POST['i_etiqueta2']) //afecta a db_gettrams($db);
	{
		$mPars['etiqueta2']=$etiqueta2;
	}

	if($vTipus=@$_POST['i_vTipus']) //afecta a db_gettrams($db);
	{
		$mPars['vTipus']=$vTipus;
	}
	else if($vTipus=@$_GET['tip']) //afecta a db_gettrams($db);
	{
		$mPars['vTipus']=$vTipus;
	}

	if($veureTramsDisponibles=@$_POST['i_veureTramsDisponibles'])
	{
		$mPars['veureTramsDisponibles']=$veureTramsDisponibles;
	}

	if($ascdesc=@$_POST['i_ascdesc'])
	{
		$mPars['ascdesc']=$ascdesc;
	}
	
	if($sortBy=@$_POST['i_sortBy'])
	{
		$mPars['sortBy']=$sortBy;
	}

	//paginacio
	$pagFiltre_=$mPars['sortBy'].$mPars['veureTramsActius'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vVehicle'].$mPars['vSubCategoria'].$mPars['vCategoria'];

	if($pagFiltre_!=$mPars['pagFiltre'])
	{
		$mPars['pagS']=0;
		$mPars['pagFiltre']=$pagFiltre_;
	}
	else 
	{
		$pagS_=@$_POST['i_pagS'];
		if(isset($pagS_))
		{
			$mPars['pagS']=$pagS_;		
		}
	}
	$numItemsPag_=@$_POST['i_itemsPag'];
	if(isset($numItemsPag_))
	{
		if($numItemsPag_!=$mPars['numItemsPag'])
		{
			$mPars['pagS']=0;
		}
		$mPars['numItemsPag']=$numItemsPag_;
	}

	$mTrams=db_getTrams($db);
	
	$mContracteTrams=db_getContractes($db); // contractats per l'usuari
	$mContracteTramsAltri=db_getContractesAltri($db); // contractats a l'usuari per altres usuaris
	$infoVisibility='hidden';
	$infoPosition='absolute';
	if($sortBy!='')
	{
		$mPars['sortBy']=$sortBy;
	}
	
	
	if(isset($guardarContractesBit) && $guardarContractesBit==1)
	{
			$guardarContractes=@$_POST['i_guardarContractes'];
			//vd($guardarContractes);
			$mMissatgeAlerta=db_guardarContractes($guardarContractes,$db);
			if(!$mMissatgeAlerta['result'])
			{
				$missatgeAlerta="<p class='pAlertaNo'><i>".$mMissatgeAlerta['missatge']."</i></p>";
			}
			else
			{
				$missatgeAlerta="<p class='pAlertaOk'><i>".$mMissatgeAlerta['missatge']."</i></p>";
				$mContracteTrams=db_getContractes($db);
				$mTrams=db_getTrams($db);
			}
	}

	$i_confirmarResposta=@$_POST['i_confirmarResposta'];
	if(isset($i_confirmarResposta) && $i_confirmarResposta!='')
	{
		$mPars['i_confirmarResposta']=$i_confirmarResposta;
		//l'usuari confirma una resposta a la seva demanda publicada, acceptant-la o descartant-la:
		$missatgeAlerta=confirmarResposta($db);
		$mContracteTrams=db_getContractes($db);
		$mTrams=db_getTrams($db);
		$mContracteTramsAltri=db_getContractesAltri($db); // contractats a l'usuari per altres usuaris
	}

$mContracteTramsCopiar=db_getContractesCopiar($db);

$parsChain=makeParsChain($mPars);

$mNomsColumnes=array(
'id' => 'id',
//'codi' => 'codi',
'sortida' => 'instant<br>sortida',
'municipi_origen' => 'municipi<br>origen',
'arribada' => 'instant<br>arribada',
'municipi_desti' => 'municipi<br>desti',
'municipis_ruta' => 'municipis<br>ruta',
'km' => 'km', 
//'periodicitat' => 'periodicitat',
//'actiu' => 'actiu',
'vehicle_id' => 'vehicle<br>assignat',
'usuari_id' => 'usuari',
'categoria0' => 'categoria0',
//'categoria10' => 'categoria10',
'tipus' => 'tipus',
	//'capacitat_pes' => 'total<br>pes<br>(kg)',
	//'pes_disponible' => 'pes<br>no assignat<br>(kg)',
	//'capacitat_volum' => 'total<br>volum<br>(l.)',
	//'volum_disponible' => 'volum<br>no assignat<br>(l.)',
	//'capacitat_places' => 'total<br>places',
	//'places_disponibles' => 'places<br>no assignades',
//'estat' => 'respostes<br>usuaris',
//'descripcio' => 'descripci�',
//'acords_explicits' => 'acords<b>expl�cits',
	//'preu_pes' => 'preu<br>(ums/kg.km)',
	//'preu_volum' => 'preu<br>(ums/l..km)',
	//'preu_places' => 'preu<br>(ums/pla�a.km)',
'pc_ms' => 'max. % ecos',
//'propietats' => 'propietats'
);
echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>SERVEI TRANSPORT - Llistat de ".$mPars['vTipus']." de transport</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_eines.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_contractesTrams.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>

<script type='text/javascript'  language='JavaScript'>

navegador();
ruta=".$mPars['selRutaSufix'].";
var nivell='';
var quantitat_pes=0;
var quantitat_volum=0;
var quantitat_places=0;
var mTrams=new Array();
var vTipus='".$mPars['vTipus']."';
chain836='';
FDCpp=".$mParametres['FDCpp']['valor']."; //recaptaci� fons despeses cac aplicat com a % sobre el preu dels trams
msFDCpp=".$mParametres['msFDCpp']['valor']."; //% de ms sobre fons despeses cac
pagS=".$mPars['pagS'].";
numItemsPag=".$mPars['numItemsPag'].";
numPags=".$mPars['numPags'].";
calGuardar=false;
calGuardarText=\"Atenci�, has modificat la selecci� i no has guardat els canvis.\\n\\nGuarda els canvis o canvia de p�gina sense guardar els canvis per continuar.\";

//limits vehicles usuari
mVehicleRef=new Array();
";
while(list($key,$mVehicleUsuari)=each($mVehiclesUsuari))
{
	echo "
	mVehicleRef[".$mVehicleUsuari['id']."]=new Array();
	mVehicleRef[".$mVehicleUsuari['id']."]['capacitat_pes']='".$mVehicleUsuari['capacitat_pes']."';
	mVehicleRef[".$mVehicleUsuari['id']."]['capacitat_volum']='".$mVehicleUsuari['capacitat_volum']."';
	mVehicleRef[".$mVehicleUsuari['id']."]['capacitat_places']='".$mVehicleUsuari['capacitat_places']."';
	";
}
reset($mVehiclesUsuari);
	
	// carregar contractes sobre trams:
	while(list($key,$val)=each($mTrams))
	{
		$mTrams[$key]['quantitat_pes']=0;
		$mTrams[$key]['quantitat_volum']=0;
		$mTrams[$key]['quantitat_places']=0;
		$mTrams[$key]['peticions_pendents']=array();
		$mTrams[$key]['peticions_confirmades']=array();
		$mTrams[$key]['estat']='';
		for($j=0;$j<count($mContracteTrams);$j++)
		{
			if($mTrams[$key]['id']==$mContracteTrams[$j]['index'])
			{
				$mTrams[$key]['quantitat_pes']=$mContracteTrams[$j]['quantitat_pes'];
				$mTrams[$key]['quantitat_volum']=$mContracteTrams[$j]['quantitat_volum'];
				$mTrams[$key]['quantitat_places']=$mContracteTrams[$j]['quantitat_places'];
				$mTrams[$key]['estat']=$mContracteTrams[$j]['estat'];
				if(in_array($mTrams[$key]['id'],$mContracteTramsAltri))
				{
					if($mContracteTrams[$j]['estat']=='pendent'){array_push($mTrams[$key]['peticions_pendents'],$mContracteTrams[$j]);}
					else if($mContracteTrams[$j]['estat']=='acceptat'){array_push($mTrams[$key]['peticions_confirmades'],$mContracteTrams[$j]);}
				}
				if($mTrams[$key]['tipus']=='DEMANDES')
				{
					$mTrams[$key]['vehicle_id']=$mContracteTrams[$j]['vehicle_id'];
				}
			}
		}
	}
	reset($mTrams);

	//definir mTrams jasvascript
	$i=0;
	while(list($key,$val)=each($mTrams))
	{
		echo "
mTrams[".$key."]=new Array();
mTrams[".$key."]['id']='".$mTrams[$key]['id']."';
mTrams[".$key."]['preuUnitats']=0;
mTrams[".$key."]['preuEcos']=0;
mTrams[".$key."]['preuEuros']=0;
mTrams[".$key."]['estoc_inicial_pes']=0;
mTrams[".$key."]['estoc_disponible0_pes']=".(str_replace(',','.',$mTrams[$key]['pes_disponible'])).";
mTrams[".$key."]['estoc_disponible_pes']=".(str_replace(',','.',$mTrams[$key]['pes_disponible'])).";
mTrams[".$key."]['quantitat_pes']=".(str_replace(',','.',$mTrams[$key]['quantitat_pes'])).";
mTrams[".$key."]['estoc_inicial_volum']=0;
mTrams[".$key."]['estoc_disponible0_volum']=".(str_replace(',','.',$mTrams[$key]['volum_disponible'])).";
mTrams[".$key."]['estoc_disponible_volum']=".(str_replace(',','.',$mTrams[$key]['volum_disponible'])).";
mTrams[".$key."]['quantitat_volum']=".(str_replace(',','.',$mTrams[$key]['quantitat_volum'])).";
mTrams[".$key."]['estoc_inicial_places']=0;
mTrams[".$key."]['estoc_disponible0_places']=".(str_replace(',','.',$mTrams[$key]['places_disponibles'])).";
mTrams[".$key."]['estoc_disponible_places']=".(str_replace(',','.',$mTrams[$key]['places_disponibles'])).";
mTrams[".$key."]['quantitat_places']=".(str_replace(',','.',$mTrams[$key]['quantitat_places'])).";
mTrams[".$key."]['preu_pes']=".(str_replace(',','.',$mTrams[$key]['preu_pes'])).";
mTrams[".$key."]['preu_volum']=".(str_replace(',','.',$mTrams[$key]['preu_volum'])).";
mTrams[".$key."]['preu_places']=".(str_replace(',','.',$mTrams[$key]['preu_places'])).";
mTrams[".$key."]['ms']=".(str_replace(',','.',$mTrams[$key]['pc_ms'])).";
mTrams[".$key."]['vehicle']=\"".$mTrams[$key]['vehicle_id']."\";
mTrams[".$key."]['tipus']=\"".$mTrams[$key]['tipus']."\";
mTrams[".$key."]['visible']=".$mTrams[$key]['visible'].";
		";
		$i++;
	}
	reset($mTrams);



echo "
pasw=\"".$mPars['pasw']."\";
nivell=\"".$mPars['nivell']."\";
usuariId=\"".$mPars['usuari_id']."\";
contractamentTancat=\"".$mParametres['contractamentTransportTancat']['valor']."\";
fPagamentEcosPerc='".$mPars['fPagamentEcosPerc']."';
fPagamentEbPerc='".$mPars['fPagamentEbPerc']."';
fPagamentEuPerc='".$mPars['fPagamentEuPerc']."';


missatgeAlerta=\"".$missatgeAlerta."\";
if(contractamentTancat==1)
{
	missatgeAlerta=missatgeAlerta+\"<p style='font-size:15px; color:#ff7700'><i>- El servei de transport est� tancat -</i></p>\";
}

</script>
</head>
<body  onload=\"javascript:
					calculaContractes('-');
					marcaSelectors();
					f_missatgeAlerta();
					\";	>
<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('contractesTrams.php?tip='.$mPars['vTipus'].'&');
echo "
	<table align='center' style='width:100%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='left'>
			<p style='font-size:16px;'><b>".$mContinguts['index']['titol0']."  - </b> ".$mContinguts['index']['titol1']." - SERVEI de TRANSPORT > <b>".$mPars['vTipus']."</b> </p>
			</td>
		</tr>
	</table>
	
	<div id='d_tot' style='visibility:inherit;'>
	";

	mostrarDades($db);
	mostrarFormTrams();
	html_mostrarNotesTrams();

	$parsChain=makeParsChain($mPars);
	$mPars=getPars($parsChain);

echo "
</div>
";
html_helpRecipient();

echo "



<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='contractesTrams.php?tip=".$mPars['vTipus']."'>
<input type='hidden' id='i_usuariId' name='i_usuariId' value='".@$mPars['usuariId']."'>
<input type='hidden' id='i_guardarContractesBit' name='i_guardarContractesBit' value='-1'>
<input type='hidden' id='i_guardarContractes' name='i_guardarContractes' value=''>

<input type='hidden' id='i_fPagamentEcos' name='i_fPagamentEcos' value='".$mPars['fPagamentEcos']."'>
<input type='hidden' id='i_fPagamentEb' name='i_fPagamentEb' value='".$mPars['fPagamentEb']."'>
<input type='hidden' id='i_fPagamentEu' name='i_fPagamentEu' value='".$mPars['fPagamentEu']."'>

<input type='hidden' id='i_fPagamentEuMetalic' name='i_fPagamentEuMetalic' value='".$mPars['fPagamentEuMetalic']."'>
<input type='hidden' id='i_fPagamentEuTransf' name='i_fPagamentEuTransf' value='".$mPars['fPagamentEuTransf']."'>
<input type='hidden' id='i_fPagamentEcosIntegralCES' name='i_fPagamentEcosIntegralCES' value='".$mPars['fPagamentEcosIntegralCES']."'>

<input type='hidden' id='i_fPagamentCompteEcos' name='i_fPagamentCompteEcos' value='".@$mPars['compte_ecos']."'>
<input type='hidden' id='i_fPagamentCompteEb' name='i_fPagamentCompteEb' value='".@$mPars['compte_cieb']."'>

<input type='hidden' id='i_sortBy' name='i_sortBy' value='".$mPars['sortBy']."'>
<input type='hidden' id='i_ascdesc' name='i_ascdesc' value='".@$mPars['ascdesc']."'>
<input type='hidden' id='i_veureTramsDisponibles' name='i_veureTramsDisponibles' value='".$mPars['veureTramsActius']."'>
<input type='hidden' id='i_etiqueta' name='i_etiqueta' value='".$mPars['etiqueta']."'>
<input type='hidden' id='i_etiqueta2' name='i_etiqueta2' value='".$mPars['etiqueta2']."'>
<input type='hidden' id='i_vVehicle' name='i_vVehicle' value='".$mPars['vVehicle']."'>
<input type='hidden' id='i_vUsuariId' name='i_vUsuariId' value='".$mPars['vUsuariId']."'>
<input type='hidden' id='i_vCategoria' name='i_vCategoria' value='".$mPars['vCategoria']."'>
<input type='hidden' id='i_vSubCategoria' name='i_vSubCategoria' value='".$mPars['vSubCategoria']."'>
<input type='hidden' id='i_info' name='i_info' value='-1'>


<input type='hidden' id='i_pagS' name='i_pagS' value='".$mPars['pagS']."'>
<input type='hidden' id='i_pagFiltre' name='i_pagFiltre' value='".$mPars['pagFiltre']."'>
<input type='hidden' id='i_itemsPag' name='i_itemsPag' value='".$mPars['numItemsPag']."'>
<input type='hidden' id='i_vistaAlbara' name='i_vistaAlbara' value='-1'>
<input type='hidden' id='i_tancaSessio' name='i_tancaSessio' value='-1'>
<input type='hidden' id='i_confirmarResposta' name='i_confirmarResposta' value=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>

<input type='hidden' id='i_selRuta' name='i_selRuta' value='".$mPars['selRutaSufix']."'>


</form>
</div>


</body>
</html>
";
?>

		