<?php
//------------------------------------------------------------------------------

function html_menuVehicles($db)
{
	global $mPars,$mVehicles,$mVehicle,$mUsuari,$mColors;
	echo "
	<table width='80%' border='1' align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td width='33%' align='center'>
			<p>Editar Vehicle:<br>
			<select name='sel_VehicleId' onChange=\"javascript:if(this.value!=''){enviarFpars('gestioVehicles.php?vUs=".$mPars['vUsuariId']."&vId='+this.value,'_self');}\">
	";
	$selected1='';
	$selected2='selected';
	$parsGrupId_=$mPars['grup_id'];
	while(list($key,$mVehicle_)=each($mVehicles))
	{
		$mUsuariVehicle=db_getUsuari($mVehicle_['usuari_id'],$db);
		
		$mUsuariVehicle['grups']=str_replace(',,',',',$mUsuariVehicle['grups']);
		$grupIdUsuari=substr($mUsuariVehicle['grups'],strpos($mUsuariVehicle['grups'],',')+1);
		$grupIdUsuari=substr($grupIdUsuari,0,strpos($grupIdUsuari,','));
		if($grupIdUsuari!='')
		{
			$mPars['grup_id']=$grupIdUsuari;
			$mGrupUsuari=getGrup($db);
		}
		else
		{
			$mGrupUsuari['nom']='no-grup';
			$mUsuariVehicle['usuari']='no-usuari';
		}
		if($mVehicle_['id']==@$mVehicle['id'])
		{
			$selected1='selected';$selected2='';}else{$selected1='';
		}
		echo "
			<option "; 
		if($mVehicle_['actiu']==1)
		{
			echo " class='oGrupActiu' ";
		} 	
		else if($mVehicle_['actiu']==0)
		{
			echo " class='oGrupInactiu' ";
		} 
		echo $selected1."  value=\"".$mVehicle_['id']."\">".(urldecode($mVehicle_['marca']))." ".(urldecode($mVehicle_['model']))."</option>
		";					
	}
	reset($mVehicles);
	$mPars['grup_id']=$parsGrupId_;
		echo "
			<option ".$selected2." value=''></option>
			</select>
			<br>
			( * en verd els Vehicles actius a la CAC)
			</p>
			</td>

			<td width='33%' align='center'>
			<p class='compacte' onClick=\"javascript: enviarFpars('gestioVehicles.php?opt=nou','_self');\" 	style='color:#885500; cursor:pointer;'><u>crear nou Vehicle</u></p>
			</td>

			<td width='33%' align='center'>
			</td>
		</tr>
	</table>

	";

	return;
}

//------------------------------------------------------------------------------
function html_mostrarPropietatsVehicle($db)
{
	global $mPars,$mVehicle;
	
	$propietats=@substr($mVehicle['propietats'],1);
	$propietats=substr($propietats,0,strlen($propietats)-1);
	$mHistorial=array();
	$mHistorial_=explode('}{',$propietats);
	while(list($key,$val)=each($mHistorial_))
	{
		$mVal=explode(';',$val);
	
		if($mVal[0]!='')
		{
			if(!isset($mHistorial[$mVal[0]])){$mHistorial[$mVal[0]]=array();}
			array_push($mHistorial[$mVal[0]],$mVal);
		}
	}
	echo "
	<p>[sadmin]</p>
	<div style='width:100%; height:100px; overflow-y:scroll; overflow-x:hidden;'>
	<table border='1' style='width:100%;'>
	";
	while(list($tipus,$mVal)=each($mHistorial))
	{
		echo "
				<tr>
					<td valign='top'>
					<p style='font-size:10px;'>".$tipus."</p>
					</td>
					<td>
					<p style='font-size:10px;'>
					";
					while(list($index,$mVal2)=each($mVal))
					{
						unset($mVal2[0]);
						$registre_=implode('|',$mVal2);
						if(substr($mVal2[1],0,strpos($mVal2[1],':'))=='us')
						{
							$usId=substr($mVal2[1],strpos($mVal2[1],':')+1);
							$mUsuariRegistre=db_getUsuari($usId,$db);
							$registre_=str_replace($mVal2[1],"<font style='cursor:pointer; size:11px;' onClick=\"enviarFpars('nouUsuari.php?iUed=".$usId."&op=editarUsuari','_blank');\"><u>".$mUsuariRegistre['usuari']."</u></font>",$registre_);
						}
						echo "
						".urldecode($registre_)."<br>
						";
					}
					echo "
					</p>
					</td>
				</tr>
	";
	}
	reset($mHistorial);
	echo "
	</table>
	</div>
	
	";	




	return;
}
?>

		