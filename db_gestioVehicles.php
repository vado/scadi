<?php

//------------------------------------------------------------------------------
function getCategoriesVehicles($db)
{
	global $mPars;
	
	$result=mysql_query("select DISTINCT categoria0 from vehicles  order by categoria0 ASC",$db);
	//echo "<br>  2329 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		if($mRow['categoria0']!='')
		{
			$mCategories[$i]=$mRow['categoria0'];
			$i++;
		}
	}

	return $mCategories;
}

//------------------------------------------------------------------------------
function getSubCategoriesVehicles($db)
{
	global $mPars;
	
	$result=mysql_query("select DISTINCT categoria0,categoria10 from vehicles order by categoria0,categoria10 ASC",$db);
	//echo "<br>  2263 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		if($mRow['categoria0']!='' && $mRow['categoria10']!='')
		{
			$mSubCategories[$i]['categoria0']=$mRow['categoria0'];
			$mSubCategories[$i]['categoria10']=$mRow['categoria10'];
			$i++;
		}
	}

	return $mSubCategories;
}

//------------------------------------------------------------------------------
function getSubCategoriesVehicles2($db)
{
	global $mPars;
	
	$result=mysql_query("select DISTINCT(categoria0) from vehicles  order by categoria0 ASC",$db);
	//echo "<br>  2292 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_NUM))
	{
		if($mRow[0]!='')
		{
			$mSubCategories['categoria0'][$i]=$mRow[0];
			$i++;
		}
	}
	$result=mysql_query("select DISTINCT(categoria10) from vehicles  order by categoria10 ASC",$db);
	//echo "<br>  2300 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_NUM))
	{
		if($mRow[0]!='')
		{
			$mSubCategories['categoria10'][$i]=$mRow[0];
			$i++;
		}
	}

	return $mSubCategories;
}
//------------------------------------------------------------------------------
function db_getVehicle($db)
{
	global $mPars;
	
	$mVehicle=array();
	
	if(!$result=mysql_query("select * from vehicles where id='".$mPars['selVehicleId']."'",$db))
	{
		//echo "<br> 49 db_gestioVehicles.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
    }
    else
    {
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		$mVehicle=$mRow;
	}

	return $mVehicle;
}


//---------------------------------------------------------------
function db_getVehicles($db)
{
	global $mPars;

	$orderBy='';
	$where_=" id!=0 "; //redundant
	$limit='';
	$mVehicles=array();
	
	if($mPars['vh_sortBy']!='')
	{
		$orderBy=" order by ".$mPars['vh_sortBy']." ".$mPars['ascdesc'];
	}

	if($mPars['veureVehiclesActius']==1)
	{
		$where_.=" AND actiu=1 ";
	}

	//aplicacio del filtre
	if($mPars['vUsuariId']!='TOTS')
	{
		$where_.=" AND  usuari_id='".$mPars['vUsuariId']."' ";
	}
 
 	if($mPars['etiqueta']==$mPars['etiqueta2']){$mPars['etiqueta']='TOTS';}

	if($mPars['etiqueta2']!="CAP"){$mPars['etiqueta']='TOTS';}

 	if($mPars['vVehicle']!='TOTS')
	{
		$where_.=" AND  vehicle_id='".$mPars['vVehicle']."' ";
	}

	if($mPars['vCategoria']!='TOTS')
	{
		$where_.=" AND  categoria0='".$mPars['vCategoria']."' ";
		
	}

	if($mPars['vSubCategoria']!='TOTS')
	{
		$where_.=" AND LOCATE('".$mPars['vSubCategoria']."',categoria10)!=0 ";
	}
	
	if($mPars['etiqueta']!='TOTS')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta']."',tipus)!=0 ";
	}

	if($mPars['etiqueta2']!='CAP')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta2']."',tipus)=0 ";
	}
	
	
	
	//echo "select * from vehicles where ".$where_." ".$orderBy." ".$limit;
	if(!$result=mysql_query("select * from vehicles where ".$where_." ".$orderBy." ".$limit,$db))
	{
		//echo "<br> 19 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mRow['id']!=''){$mVehicles[$mRow['id']]=$mRow;}
		}
	}

	return $mVehicles;
}

//---------------------------------------------------------------
function db_getVehiclesUsuari($usuariId,$db)
{
	global $mPars;

	$mVehiclesUsuari=array();
	
	if(!$result=mysql_query("select * from vehicles where usuari_id='".$usuariId."' order by id ASC",$db))
	{
		echo "<br>179 db_gestioVehicles.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mVehiclesUsuari[$i]=$mRow;
			$i++;
		}
	}

	return $mVehiclesUsuari;
}

//----------------------------------------------------------------
function db_guardarVehicle2($mGP,$db)
{
	global $mPars;
	
	$mysqlChain='';
	$registre='';

	//obtenim dades Vehicle actual.
	if(!$result=mysql_query("select * from vehicles where id='".$mPars['selVehicleId']."'",$db))
	{
		echo "<br> 145 db_gestioVehicles.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
    }
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	
		//$mGP['usuari_id']=$mPars['usuari_id'];
		while(list($key,$val)=each($mGP))
		{
			if(@isset($mRow[$key]))
			{
				if($mGP[$key]!=$mRow[$key])
				{
					$mysqlChain.=",".$key."='".$val."'";
					if($key=='descripcio'){$val='(...)';$mRow[$key]=$val;}
					$registre='{p:historial;us:'.$mPars['usuari_id'].';'.$key.':'.$mRow[$key].'->'.$mGP[$key].';dt:'.date('d/m/Y H:i:s').';}'.$registre;
				}
			}
		}
		reset($mGP);
		
		$mysqlChain=substr($mysqlChain,1);
		
		$coma='';
		
		if($mysqlChain!='' || $registre!='')
		{
			if($mysqlChain!='' && $registre!=''){$coma=',';}
			if(!$result=mysql_query("update vehicles set  ".$mysqlChain.$coma."propietats=CONCAT('".$registre."',propietats) where id='".$mPars['selVehicleId']."'",$db))
			{
				//echo "<br> 155 db_gestioVehicles.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
				return false;
    		}
			else
			{
				return true;
			}
			
		}
		else
		{
			return true;
		}
	}

	return false;
}


//----------------------------------------------------------------
function db_crearVehicle($mGP,$db)
{
	global $mPars;

	$mysqlChain='';
	$mysqlChainCheck='';

	$mGP['categoria0']=urldecode($mGP['categoria0']);
	//$mGP['categoria10']=urldecode($mGP['categoria10']);
	$mGP['usuari_id']=$mPars['usuari_id'];
	
	$mCamps=array(
'id'=>'',
'marca'=>'',
'model'=>'',
'any'=>'',
'descripcio'=>'',
'consum'=>'',
'combustible'=>'',
'categoria0'=>'',
'categoria10'=>'',
'vehicle'=>'',
'municipi_id'=>'',
'usuari_id'=>'',
'actiu'=>'',
'capacitat_pes'=>'',
'capacitat_volum'=>'',
'capacitat_places'=>'',
'matricula'=>'',
'estat'=>'',
'notes'=>'',
'propietats'=>'',
'imatge'=>''
);

	
	while(list($key,$val)=each($mGP))
	{
		if($key!='' && $val!='')
		{
			$mCamps[$key]=$val;
		}
	}
	reset($mGP);
		
	while(list($key,$val)=each($mCamps))
	{
		if($key!='')
		{
			$mysqlChain.=",'".$val."'";
		}

		if($key=='usuari_id' || $key=='marca' || $key=='model' || $key=='matricula')
		{
			$mysqlChainCheck.=" AND ".$key."='".$val."'";
		}
	}	
	reset($mCamps);
	$mysqlChain=substr($mysqlChain,1);
	$mysqlChainCheck=substr($mysqlChainCheck,5);

	//evitar reenviament de formulari
	//echo "<br>select * from vehicles where ".$mysqlChainCheck;
	$result=mysql_query("select * from vehicles where ".$mysqlChainCheck,$db);
	//echo "<br> 327 db_gestioVehicles.php ".mysql_errno() . ": " . mysql_error(). "\n";
	//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
	
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	if($mRow)
	{
		return false;
	}
	
	
	//echo "insert into vehicles values(".$mysqlChain;
	if(!$result=mysql_query("insert into vehicles values(".$mysqlChain.")",$db))
	{
		//echo "<br> 397 db_gestioVehicles.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
    }
	
	return true;
}

//----------------------------------------------------------------
function db_eliminarVehicle($eliminarId,$db)
{
	global $mPars;
	
	//si hi ha trams actius no es pot eliminar
	if(!$result=mysql_query("select id from trams_".$mPars['selRutaSufix']." where vehicle_id='".$eliminarId."' AND capacitat_pes!=pes_disponible AND capacitat_volum!=volum_disponible AND capacitat_places!=places_disponibles AND actiu=1",$db))
	{
		//echo "<br> 116 db_gestioTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		if($mRow)
		{
			return false;
		}
		else
		{
			if(!$result=mysql_query("delete from vehicles where id='".$eliminarId."'",$db))
			{
				//echo "<br> 432 db_gestiotrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
	
				return false;
			}
		}
	}

	return true;
}
//------------------------------------------------------------------------------
function db_getTramsActiusContractats($db)
{
	global $mPars;
	
	$mTramsActiusContractats=array();
	if(!$result=mysql_query("select * from trams_".$mPars['selRutaSufix']." where vehicle_id='".$mPars['selVehicleId']."' AND capacitat_pes!=pes_disponible AND capacitat_volum!=volum_disponible AND capacitat_places!=places_disponibles AND actiu=1",$db))
	{
		//echo "<br> 299 db_gestioVehicles.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			array_push($mTramsActiusContractats,$mRow);
		}
	}
	return $mTramsActiusContractats;
}


?>

		