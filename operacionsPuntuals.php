<?php
include "config.php";
include "db.php";
include "html.php";
include "eines.php";
include "db_incidencies.php";
include "db_operacionsPuntuals.php";


$missatgeAlerta='';


$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);

$parsChain=makeParsChain($mPars);
$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];

$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}
getConfig($db); //inicialitza variables anteriors;

$mRutesSufixes=getRutesSufixes($db);

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';

	$mPars['fPagamentUms']=0;
	$mPars['fPagamentEcos']=0;
	$mPars['fPagamentEb']=0;
	$mPars['fPagamentEu']=0;
	$mPars['fPagamentEuMetalic']=0;
	$mPars['fPagamentEuTransf']=0;

	$mPars['fPagamentEcosPerc']=0;
	$mPars['fPagamentEbPerc']=0;
	$mPars['fPagamentEuPerc']=0;
	$mPars['paginacio']=0;
	if(!isset($mPars['pagS'])){$mPars['pagS']=0;}
	if(!isset($mPars['pagFiltre'])){$mPars['pagFiltre']=$mPars['sortBy'].$mPars['veureProductesDisponibles'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vProductor'].$mPars['vSubCategoria'].$mPars['vCategoria'];}
	if(!isset($mPars['numPags'])){$mPars['numPags']=0;} //ve de db_getProductes()
	if(!isset($mPars['numItemsPag'])){$mPars['numItemsPag']=30;}

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}

$mRutesSufixesExportades=getRutesSufixesExportades($db);
$mRutesSufixes=getRutesSufixes($db);

$mGrupsRef=db_getRebostsRef($db);

$idOperacio=@$_GET['iOp'];
if(isset($idOperacio))
{
	if($idOperacio=='1') 
	{
		$missatgeAlerta=db_estocPrevistIgualEstocDisponible($db);
	}
	else if($idOperacio=='2')
	{
		$missatgeAlerta=db_generarComandaApartirDestocPrevist($db);
	}
	else if($idOperacio=='3')
	{
		$missatgeAlerta=db_generarComandaUsuariRespDeComandaGrup($db);
	}
	else if($idOperacio=='4')
	{
		$rutaImp=@$_POST['i_ruta'];
		$missatgeAlerta=db_importarTaula($rutaImp,$db);
	}
}

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>Operacions Puntuals</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_operacionsPuntuals.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
ruta=".$ruta.";
</SCRIPT>
</head>
<body onLoad=\"\">

	<table align='center' style='width:80%'>
		<tr>
			<th style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</p>
			</td>
		</tr>
	</table>

	<table align='center' style='width:80%'>
		<tr>
			<th style='width:100%;' align='center'>
			".$missatgeAlerta."
			</td>
		</tr>
	</table>
<center><p style='font-size:16px;'>[ Ruta <b>".$mPars['selRutaSufix']."</b>: Operacions Puntuals ]</p></center>
";
mostrarSelectorRuta('operacionsPuntuals.php');
echo "
<table align='center' bgcolor='#dddddd' width='60%'>
		<tr>
			<th  bgcolor='#eeeeee'><p>id</p></th>
			<th  bgcolor='#eeeeee'><p>operaci�</p></th>
			<th  bgcolor='#eeeeee'><p>activar</b></p></th>
			<th  bgcolor='#eeeeee'><p>executar</p></th>
			<th  bgcolor='#eeeeee'><p>notes</p></th>
		</tr>

		<tr>
			<td  bgcolor='#eeeeee'><p>1</p></p></td>
			<td  bgcolor='#eeeeee'><a title=\"Modifica tots els productes  'no-diposit' a estoc_previst=estoc_disponible, i tots els productes a estoc_disponible=estoc_previst\"><p>Modifica tots els productes 'no-diposit' a estoc_previst=estoc_disponible, i tots els productes a estoc_disponible=estoc_previst</p></a></td>
			<td  bgcolor='#eeeeee'><input type='button' onClick=\"

			javascript:
				document.getElementById('i_activar_1').value='1';
				this.style.backgroundColor='#F9E39F';
				
				\" value='activar'><input type='hidden' id='i_activar_1' value='0'></p>
			</td>
			
			<td  bgcolor='#eeeeee'><input type='button' onClick=\"
			
			javascript:
				this.style.backgroundColor='#ffffff';
				val0525=document.getElementById('i_activar_1').value;
				if(val0525=='1')
				{
					enviarFpars('operacionsPuntuals.php?iOp=1','_self');
					document.getElementById('i_activar_1').value='0';
				}
				else
				{
					alert('activa el bot�, primer');
				}
				\" value='executar'>
				
				</p>
			</td>
			<td  bgcolor='#eeeeee'><p></p></td>
		</tr>

		<tr>
			<td  bgcolor='#eeeeee'><p>2</p></p></td>
			<td  bgcolor='#eeeeee'><a title=\"Genera una comanda CAC a partir de l'estoc previst i guarda una copia de la comanda anterior, recuperable amb la operacio id:3\"><p>Generar comanda CAC a partir d'estoc previst</p></a></td>
			<td  bgcolor='#eeeeee'><input type='button' onClick=\"

			javascript:
				document.getElementById('i_activar_2').value='1';
				this.style.backgroundColor='#F9E39F';
				
				\" value='activar'><input type='hidden' id='i_activar_2' value='0'></p>
			</td>
			
			<td  bgcolor='#eeeeee'><input type='button' onClick=\"
			
			javascript:
				this.style.backgroundColor='#ffffff';
				val0525=document.getElementById('i_activar_2').value;
				if(val0525=='1')
				{
					enviarFpars('operacionsPuntuals.php?iOp=2','_self');
					document.getElementById('i_activar_1').value='0';
				}
				else
				{
					alert('activa el bot�, primer');
				}
				\" value='executar'>
				
				</p>
			</td>
			<td  bgcolor='#eeeeee'><p></p></td>
		</tr>
		<tr>
			<td  bgcolor='#eeeeee'><p>3</p></p></td>
			<td  bgcolor='#eeeeee'><a title=\"s'incorpora la ruta objectiu manualment a operacionsPuntuals.php\"><p>Generar comanda usuari(responsable grup) a partir de comanda grup (usuariId=0) per a rutes anteriors a setembre (canvi sistema usuaris)</p></a></td>
			<td  bgcolor='#eeeeee'><input type='button' onClick=\"

			javascript:
				document.getElementById('i_activar_3').value='1';
				this.style.backgroundColor='#F9E39F';
				
				\" value='activar'><input type='hidden' id='i_activar_3' value='0'></p>
			</td>
			
			<td  bgcolor='#eeeeee'><input type='button' onClick=\"
			
			javascript:
				this.style.backgroundColor='#ffffff';
				val0525=document.getElementById('i_activar_3').value;
				if(val0525=='1')
				{
					enviarFpars('operacionsPuntuals.php?iOp=3','_self');
					document.getElementById('i_activar_3').value='0';
				}
				else
				{
					alert('activa el bot�, primer');
				}
				\" value='executar'>
				
				</p>
			</td>
			<td  bgcolor='#eeeeee'><p></p></td>
		</tr>
		
		<tr>
			<td  bgcolor='#eeeeee'><p>4</p></p></td>
			<td  bgcolor='#eeeeee'><a title=\"importa les taules del periode seleccionat a partir de les taules exportades en tancar els periodes de precomandes\"><p>importar a bbdd les taules del periode especificat</p></a></td>
			<td  bgcolor='#eeeeee'><input type='button' onClick=\"

			javascript:
				document.getElementById('i_activar_4').value='1';
				this.style.backgroundColor='#F9E39F';
				
				\" value='activar'><input type='hidden' id='i_activar_4' value='0'></p>
				<select id='sel_ruta' name='sel_ruta'>
				";
				while(list($key,$val)=each($mRutesSufixesExportades))
				{
					echo "
				<option value='".$val."'>".$val."</option>	
					";
				}
				reset($mRutesSufixesExportades);
				echo "
				<option selected value=''></option>	
				</select>
			</td>
			
			<td  bgcolor='#eeeeee'><input type='button' onClick=\"
			
			javascript:
				this.style.backgroundColor='#ffffff';
				val0525=document.getElementById('i_activar_4').value;
				if(val0525=='1')
				{
					selRuta=document.getElementById('sel_ruta').value;
					if(selRuta=='')
					{
						alert('cal seleccionar un periode per importar');
					}
					else
					{
						document.getElementById('i_ruta').value=selRuta;
						enviarFpars('operacionsPuntuals.php?iOp=4','_self');
						document.getElementById('i_activar_4').value='0';
					}
				}
				else
				{
					alert('activa el bot�, primer');
				}
				\" value='executar'>
				
				</p>
			</td>
			<td  bgcolor='#eeeeee'><p></p></td>
		</tr>
	</table>
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_ruta' name='i_ruta' value=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>
";

?>

		