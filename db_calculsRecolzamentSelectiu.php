<?php
//------------------------------------------------------------------------------
function tallarIredistribuirFactorRP($valorPuntFactorRP)
{
	global $mGrupsProductorsRef;
	
	$puntsFactorRPnoAssignats=0;
	$recolzamentRedistribuit=1;
	$numGrupsNoMaximRecolzament=count($mGrupsProductorsRef);
	
	$puntsFactorRPnoAssignats=0;
	while(true)
	{
		//tallar
		While(list($grupId,$mGrupP)=each($mGrupsProductorsRef))
		{
			$reserves=$mGrupP['reservesEc']+$mGrupP['reservesEu'];
			$compres=$mGrupP['compresEc']+$mGrupP['compresEb']+$mGrupP['compresEu'];
			$difRC=ABS($reserves-$compres);
			$maxPuntsFactorRP=($compres/6)*1.5/$valorPuntFactorRP;

			if($mGrupsProductorsRef[$grupId]['factorRP']>$maxPuntsFactorRP)
			{
				$puntsFactorRPnoAssignats+=$mGrupsProductorsRef[$grupId]['factorRP']-$maxPuntsFactorRP;
				$mGrupsProductorsRef[$grupId]['factorRP']=$maxPuntsFactorRP;
				$mGrupsProductorsRef[$grupId]['maxRecolzament']=TRUE;
				$numGrupsNoMaximRecolzament--;
			}
		}
		reset($mGrupsProductorsRef);
		//calcular redistribucio:
		if($numGrupsNoMaximRecolzament==0 || $puntsFactorRPnoAssignats<=1){break;}
		
		$sumaFactorRP2=0;
		While(list($grupId,$mGrupP)=each($mGrupsProductorsRef))
		{
			if($mGrupsProductorsRef[$grupId]['maxRecolzament']===FALSE)
			{
				if(!isset($mGrupsProductorsRef[$grupId]['factorRP2'])){$mGrupsProductorsRef[$grupId]['factorRP2']=0;}
			
				$reserves=$mGrupsProductorsRef[$grupId]['reservesEc']+$mGrupsProductorsRef[$grupId]['reservesEu'];
				$compres=$mGrupsProductorsRef[$grupId]['compresEc']+$mGrupsProductorsRef[$grupId]['compresEb']+$mGrupsProductorsRef[$grupId]['compresEu'];
				$difRC=ABS($reserves-$compres);
		
				if($reserves!=0 && $compres!=0 && $difRC>=1)
				{
					//$factorR=($mGrupP['acord1']/100)*$reserves*($compres*$compres)/(100*(sqrt($difRC)));
					$mGrupsProductorsRef[$grupId]['factorRP2']=(($mGrupsProductorsRef[$grupId]['%ms']/100)*$reserves+$compres*pow($compres,1/3))/pow($difRC,1/2);
				}
				else if($reserves!=0 && $compres!=0)
				{
					$mGrupsProductorsRef[$grupId]['factorRP2']=(($mGrupsProductorsRef[$grupId]['%ms']/100)*$reserves+$compres*pow($compres,1/2));
				}
				else
				{
					$mGrupsProductorsRef[$grupId]['factorRP2']=0;
				}
				$sumaFactorRP2+=$mGrupsProductorsRef[$grupId]['factorRP2'];
			}
		}
		reset($mGrupsProductorsRef);
		if($sumaFactorRP2<=1){break;}
		
		$valorPuntFactorRP2=$puntsFactorRPnoAssignats/$sumaFactorRP2;

		//redistribuir
		While(list($grupId,$mGrupP)=each($mGrupsProductorsRef))
		{
				@$mGrupsProductorsRef[$grupId]['factorRP']+=@$mGrupsProductorsRef[$grupId]['factorRP2']*$valorPuntFactorRP2;
		}
		reset($mGrupsProductorsRef);
		$puntsFactorRPnoAssignats=0;
	}
	
	return;
}

//------------------------------------------------------------------------------
function distribuirRecolzament($db)
{
	global 	$mParametres,
			$mRecolzament,
			$mSaldosUsuarisComissions,
			$mSaldosRebostsCal,
			$mSaldosProductores,
			$mSaldosGrups,
			$ecosTotalMembresComissions,
			$mUsuarisComissionsRef,
			$mGrupsCALref,
			$mGrupsProductorsRef,
			$mGrupsNoProductorsRef,
			$mComptesUsuarisRef,
			$mComptesGrupsRef;
			
	
	//distribucio a membres de comissio:
	
	if($ecosTotalMembresComissions==0){$ecosTotalMembresComissions=1;}
	$f1=$mParametres['PPRECM']['valor']/$ecosTotalMembresComissions;
	//$sobrantR=0;
	
	While(list($usuariId,$mUsuariComissio)=each($mUsuarisComissionsRef))
	{
		$mPropietats=getPropietats($mUsuariComissio['propietats']);
		if(!isset($mComptesUsuarisRef[$usuariId]['ecos_ab'])){$mComptesUsuarisRef[$usuariId]['ecos_ab']=0;}
		if(!isset($mPropietats['comissio'])){$mPropietats['comissio']='';}
		if(!isset($mComptesUsuarisRef[$usuariId]['saldo_ecos_r']) || $mComptesUsuarisRef[$usuariId]['saldo_ecos_r']==''){$mComptesUsuarisRef[$usuariId]['saldo_ecos_r']=0;}
		
		//$mUsuariComissio['ecosAb']=$mComptesUsuari['ecos_ab'];
		$mUsuariComissio['saldoEcosRc']=$mComptesUsuarisRef[$usuariId]['ecos_ab']*$f1;
		//$mUsuariComissio['saldoEcosR']=$mComptesUsuari['saldo_ecos_r'];

		//limit
		if($mParametres['activarLimitFABLRMC']['valor']=='1')
		{
			if($mUsuariComissio['saldoEcosRc']>$mComptesUsuarisRef[$usuariId]['ecos_ab']*$mParametres['FABLRMC']['valor'])
			{
				$mUsuariComissio['saldoEcosRc']=$mComptesUsuarisRef[$usuariId]['ecos_ab']*$mParametres['FABLRMC']['valor'];
				//$sobrantR+=$mPropietats['saldoEcosR']-$mUsuariComissio['ecosAb']*$mParametres['FABLRMC']['valor'];
			}
		}
		$mUsuarisComissionsRef[$usuariId]['saldoEcosRc']=$mUsuariComissio['saldoEcosRc'];
		$mUsuarisComissionsRef[$usuariId]['saldoEcosR']=$mComptesUsuarisRef[$usuariId]['saldo_ecos_r'];
		$mUsuarisComissionsRef[$usuariId]['ecosAb']=$mComptesUsuarisRef[$usuariId]['ecos_ab'];
		$mUsuarisComissionsRef[$usuariId]['comissio']=$mPropietats['comissio'];
	}
	reset($mUsuarisComissionsRef);
	
				
	//distribucio a rebosts-CAL: MANUAL

	//distribucio a productores: AUTO
	
	While(list($grupId,$mGrupP)=each($mGrupsProductorsRef))
	{
		if(!isset($mComptesGrupsRef[$grupId]['saldo_ecos_r'])){$mComptesGrupsRef[$grupId]['saldo_ecos_r']=0;}
		$mGrupsProductorsRef[$grupId]['saldoEcosR']=$mComptesGrupsRef[$grupId]['saldo_ecos_r'];
	}
	reset($mGrupsProductorsRef);

	//distribucio a grups no productores: MANUAL
	

	return;
}

//------------------------------------------------------------------------------
function db_aplicarDistribucioGrupsProductors($db)
{
	global $mPars,$mGrupsProductorsRef,$recolzamentGPchain;
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=false;
	
	$mRecolzamentGPchain=explode(';',$recolzamentGPchain);
	
	//m�ltiple update a grups productors
	while(list($key,$val)=each($mRecolzamentGPchain))
	{
		if($val!='')
		{
			$mVal=explode(':',$val);
			$grupId=$mVal[0];
			$saldoEcosR=$mVal[1];
		
			if(isset($grupId) && $grupId!='' && array_key_exists($grupId,$mGrupsProductorsRef) && $saldoEcosR!='')
			{
				$mMissatgeAlerta_=db_aplicarRecolzamentGrup($saldoEcosR,$grupId,$db);
				$mMissatgeAlerta['missatge'].=$mMissatgeAlerta_['missatge'];
			}
		}
	}
	reset($mRecolzamentGPchain);

	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function db_aplicarDistribucioComissions($distribucioComissions,$db)
{
	global $mPars,$mUsuarisComissionsRef;
		
	$mDistribucioComissions=explode(';',$distribucioComissions);
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=false;

	//m�ltiple update a usuaris comissi�
	while(list($key,$cadena)=each($mDistribucioComissions))
	{
		$usuariId=substr($cadena,0,strpos($cadena,':'));
		$saldoEcosR=substr($cadena,strpos($cadena,':')+1);

		if(isset($usuariId) && $usuariId!='' && array_key_exists($usuariId,$mUsuarisComissionsRef) && $saldoEcosR!='')
		{
			if(!$result=mysql_query("update comptes_".(substr($mPars['selRutaSufix'],0,2))." set saldo_ecos_r='".$saldoEcosR."' where sel_usuari_id='".$usuariId."' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db))
			{
				//echo "<br> 109 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>ERROR > ".(urldecode($mUsuarisComissionsRef[$usuariId]['usuari']))." [saldo ecos recolzats: ".(number_format($saldoEcosR,2,'.',''))."]</p>";
			}
			else
			{
				if($saldoEcosR>0)
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>OK >".(urldecode($mUsuarisComissionsRef[$usuariId]['usuari']))." [saldo ecos recolzats: ".(number_format($saldoEcosR,2,'.',''))."]</p>";
				}
			}
		}
	}
	reset($mDistribucioComissions);

	return	$mMissatgeAlerta;
}


//------------------------------------------------------------------------------
function db_aplicarRecolzamentGrup($saldoEcosR,$grupId,$db)
{
	global $mPars,$mComptesGrupsRef;
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=false;
	
	$mPars['grup_id']=$grupId;
	$mGrup=getGrup($db);


	$result2=mysql_query("select id from comptes_".substr($mPars['selRutaSufix'],0,2)." where grup_id='".$grupId."' AND sel_usuari_id='0' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db);
	//echo "<br> 264 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow2=mysql_fetch_array($result2,MYSQL_ASSOC);
						
	if(!$mRow2)
	{
		if(!$result=mysql_query("insert into comptes_".substr($mPars['selRutaSufix'],0,2)." values('','".(date('Y-m-d h:i:s'))."','".$mPars['selRutaSufix']."','".$mPars['usuari_id']."','".$grupId."','','config','".$saldoEcosR."','0','0','0','0','0','0','distribucio recolzament','','','','0','0')",$db))
		{
			//echo "<br> 138 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
			$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>ERROR: ".(urldecode($mGrup['nom'])).", ".(number_format($saldoEcosR,2,'.',''))." ecos (r)</p>";
		}
		else
		{
			$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>OK: ".(urldecode($mGrup['nom'])).", ".(number_format($saldoEcosR,2,'.',''))." ecos (r)</p>";
		}
	}
	else
	{
		if(!$result=mysql_query("update  comptes_".substr($mPars['selRutaSufix'],0,2)." set saldo_ecos_r='".$saldoEcosR."' where id='".$mRow2['id']."'",$db))
		{
			//echo "<br> 151 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
			$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>ERROR: ".(urldecode($mGrup['nom'])).", ".(number_format($saldoEcosR,2,'.',''))." ecos (r)</p>";
		}
		else
		{
			$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>OK: ".(urldecode($mGrup['nom'])).", ".(number_format($saldoEcosR,2,'.',''))." ecos (r)</p>";
		}
	}
	
	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function db_guardarParametreRecolzament($nom,$valor,$db)
{
	global $mPars;
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=false;
	
	if(!$result=mysql_query("update parametres_".$mPars['selRutaSufix']." set valor='".$valor."' where parametre='".$nom."'",$db))
	{
		//echo "<br> 92 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
		$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>ERROR: No s'ha pogut guardar el par�metre</p>";
	}
	else
	{
		$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>OK:s'ha guardat el par�metre correctament</p>";
	}

	return $mMissatgeAlerta;
}
//------------------------------------------------------------------------
function recepcioFitxerDadesRecolzament($db)
{
    global $mParametres,$mPars,$mDades;

	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=false;
	$mDadesRecolzament=array();

	$dir=getcwd();
	if(!@chdir('dadesRecolzament'))
	{
		@mkdir('dadesRecolzament');
	}
	@chdir($dir);
	
    if($_FILES['f_dadesRecolzament']['name']!='')
    {
        if (is_uploaded_file($_FILES['f_dadesRecolzament']['tmp_name']))
        {
            //mirar si l'extensio �s correcta:
            $mInfo=@pathinfo($_FILES['f_dadesRecolzament']['name']);
  			if($mInfo['extension']=='csv')
	        {
				if(!copy($_FILES['f_dadesRecolzament']['tmp_name'],'dadesRecolzament/dadesRecolzament_'.$mPars['usuari_id'].'.csv'))
            	{
                	$missatgeError.="<p class='pAlertaNo4'> Atenci�: Error en llegir el fitxer</p>";
            	}
				else
				{
					if (($fp=fopen('dadesRecolzament/dadesRecolzament_'.$mPars['usuari_id'].'.csv','r')) !== FALSE)
					{
    					$n=0;
						while (($mCamps = fgetcsv($fp, 1000, ",")) !== FALSE) 
						{
							$mDades[$n]=$mCamps;
							$n++;
        				}
    				}
    				fclose($fp);
				}
			}
			else
           	{
               	$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'> Atenci�: Nom�s es permeten fitxers .csv</p>";
				$mMissatgeAlerta['result']=false;
				return $mMissatgeAlerta;
           	}
		}
    }

    $mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'> Ok: El fitxer s'ha importat correctament.</p>";
	$mMissatgeAlerta['result']=true;

	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function guardarDadesRecolzamentComissions($db)
{
	global $mPars,$mDades;

	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=false;

	$mDades_=array();
	
	//reduir a una linia per usuari (membres de mes d'una comissio)
	
	while(list($key,$mCamps)=each($mDades))
	{
		if(!isset($mDades_[strtolower($mCamps[0])])){$mDades_[strtolower($mCamps[0])]=0;}
		$mDades_[strtolower($mCamps[0])]+=$mCamps[1];
	}
	reset($mDades);
	
	while(list($compteCoop,$ecosAbs)=each($mDades_))
	{
		if($compteCoop!='' && strlen($compteCoop)==8 && strtolower(substr($compteCoop,0,4))=='coop' && $ecosAbs!='')
		{
			if(!$result=mysql_query("select * from usuaris where LOCATE('comissio',CONCAT(' ',propietats))>0 AND LOCATE('compteSoci:".(strtolower($compteCoop)).";',propietats)>0",$db))
			{
				//echo "<br> 237 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
			}
			else
			{
				$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
				if($mRow)
				{
					// guardar a entrada de ruta corresponent, si existeix
					$result2=mysql_query("select id from comptes_".substr($mPars['selRutaSufix'],0,2)." where sel_usuari_id='".$mRow['id']."' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db);
					//echo "<br> 264 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
					$mRow2=mysql_fetch_array($result2,MYSQL_ASSOC);
					
					if(!$mRow2)
					{
						if(!$result=mysql_query("insert into comptes_".substr($mPars['selRutaSufix'],0,2)." values('','".(date('Y-m-d h:i:s'))."','".$mPars['selRutaSufix']."','".$mPars['usuari_id']."','','".$mRow['id']."','config','0','0','0','0','0','0','0','distribucio recolzament','','','".$ecosAbs."','0','0')",$db))
						{
							//echo "<br> 271 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
							//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
							$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>ERROR: ".$compteCoop.", ".(number_format($ecosAbs,2,'.',''))." ecos (ab)</p>";
						}
						else
						{
							$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>OK: ".$compteCoop.", ".(number_format($ecosAbs,2,'.',''))." ecos (ab)</p>";
						}
					}
					else
					{
						if(!$result=mysql_query("update  comptes_".substr($mPars['selRutaSufix'],0,2)." set ecos_ab='".$ecosAbs."' where id='".$mRow2['id']."'",$db))
						{
							//echo "<br> 287 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
							$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>ERROR: ".$compteCoop.", ".(number_format($ecosAbs,2,'.',''))." ecos (ab)</p>";
						}
						else
						{
							$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>OK: ".$compteCoop.", ".(number_format($ecosAbs,2,'.',''))." ecos (ab)</p>";
						}
					}
				}
			}
		}
		else
		{
			$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>ERROR > ".$compteCoop.", ".$ecosAbs." ecos (ab) (r) [dades importades] </p>";
		}
	}
	reset($mDades);

	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function db_afegirImports6mesosAnteriors($db)
{
	global $mPars,$mGrupsProductorsRef,$mProductes;
	
	
	$contRuta=0;
	$mRutesSufixes=getRutesSufixes($db);
	$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);	
	rsort($mRutesSufixes);
	$parSelRutaSufix=$mPars['selRutaSufix'];
	$taulaComandes_mem=$mPars['taulaComandes'];
	$taulaProductes_mem=$mPars['taulaProductes'];
	while(list($key,$rutaSufix)=each($mRutesSufixes))
	{
		if
		(
			$rutaSufix!='grups'
			&&
			$rutaSufix*1<=(date('ym'))*1 
		)	
		{
		if($contRuta==6){break;}
		if($contRuta>0)
		{
		
			//imports compres dels grups----------------------------------------
			
			//echo "<br>select SUBSTRING_INDEX(rebost,'-',1),f_pagament from ".$mPars['taulaComandes']." where usuari_id='0'";
			$result=mysql_query("select SUBSTRING_INDEX(rebost,'-',1),f_pagament from ".$mPars['taulaComandes']." where usuari_id='0'",$db);
			//echo "<br> 341 db_calculsRecolzamentSelectiu.php ".mysql_errno() . ": " . mysql_error(). "\n";
				
				$mPars['taulaComandes']='comandes_'.$rutaSufix;
				$mPars['taulaProductes']='productes_'.$rutaSufix;

			while($mRow=mysql_fetch_array($result,MYSQL_NUM))
			{
				$mFpagament=explode(',',$mRow[1]);
				
				if(isset($mGrupsProductorsRef[$mRow[0]]))
				{
					if(!isset($mGrupsProductorsRef[$mRow[0]]['compresEc'])){$mGrupsProductorsRef[$mRow[0]]['compresEc']=0;}
					if(!isset($mGrupsProductorsRef[$mRow[0]]['compresEb'])){$mGrupsProductorsRef[$mRow[0]]['compresEb']=0;}
					if(!isset($mGrupsProductorsRef[$mRow[0]]['compresEu'])){$mGrupsProductorsRef[$mRow[0]]['compresEu']=0;}
					$mGrupsProductorsRef[$mRow[0]]['compresEc']+=1*$mFpagament[0];
					$mGrupsProductorsRef[$mRow[0]]['compresEb']+=1*@$mFpagament[1];
					$mGrupsProductorsRef[$mRow[0]]['compresEu']+=1*@$mFpagament[2];
				}
			}

			//imports vendes dels grups-----------------------------------------
				$mInfoRutaAbastCsv=array();
				$mProductes_=db_getProductes2($db);
				$mProductes=array();
				$i=0;
				while(list($key,$mVal)=each($mProductes_))
				{
					$mProductes[$i]=$mVal;
					$i++;
				}
				reset($mProductes_);

			$mComandaPerProductors=db_getComandaPerProductors($db);
			
			//calcular totals per productor
			//excloure els grups no productors encara que tinguin comanda
			$mTotalReservatPerProductors=array();
			while(list($perfilId,$mReservat)=each($mComandaPerProductors))
			{
				if(!isset($mTotalReservatPerProductors[$perfilId]['ecosT'])){$mTotalReservatPerProductors[$perfilId]['ecosT']=0;}
				if(!isset($mTotalReservatPerProductors[$perfilId]['eurosT'])){$mTotalReservatPerProductors[$perfilId]['eurosT']=0;}

				$ecos=0;
				$euros=0;
				
				while(list($idProducte,$mProducte)=each($mReservat))
				{
					$ecos=$mProducte['quantitatT']*$mProducte['preu']*$mProducte['ms']/100;
					$euros=$mProducte['quantitatT']*$mProducte['preu']*(100-$mProducte['ms'])/100;
					$mTotalReservatPerProductors[$perfilId]['ecosT']+=$ecos;
					$mTotalReservatPerProductors[$perfilId]['eurosT']+=$euros;
				}
			}
			reset($mComandaPerProductors);

			
			// afegir vendes a matriu grups
			while(list($grupId,$mGrup)=each($mGrupsProductorsRef))
			{
				if(!isset($mGrupsProductorsRef[$grupId]['reservesEc'])){$mGrupsProductorsRef[$grupId]['reservesEc']=0;}
				if(!isset($mGrupsProductorsRef[$grupId]['reservesEu'])){$mGrupsProductorsRef[$grupId]['reservesEu']=0;}
				if(!isset($mGrupsProductorsRef[$grupId]['%ms'])){$mGrupsProductorsRef[$grupId]['%ms']=0;}
							
				if(isset($mGrup['perfil_vinculat']) && $mGrup['perfil_vinculat']!='')
				{
					if(isset($mComandaPerProductors[$mGrup['perfil_vinculat']]))
					{
						$mGrupsProductorsRef[$grupId]['reservesEc']+=$mTotalReservatPerProductors[$mGrup['perfil_vinculat']]['ecosT'];
						$mGrupsProductorsRef[$grupId]['reservesEu']+=$mTotalReservatPerProductors[$mGrup['perfil_vinculat']]['eurosT'];
						
						if($mGrupsProductorsRef[$grupId]['reservesEc']+$mGrupsProductorsRef[$grupId]['reservesEu']==0)
						{
							$mGrupsProductorsRef[$grupId]['%ms']=0;
						}
						else
						{
							$mGrupsProductorsRef[$grupId]['%ms']=100*$mGrupsProductorsRef[$grupId]['reservesEc']/($mGrupsProductorsRef[$grupId]['reservesEc']+$mGrupsProductorsRef[$grupId]['reservesEu']);
						}
					}
				}
			}
			reset($mGrupsProductorsRef);

		}
		$contRuta++;
		
		}
	}
	$mPars['selRutaSufix']=$parSelRutaSufix;
	$mPars['taulaComandes']=$taulaComandes_mem;
	$mPars['taulaProductes']=$taulaProductes_mem;
	
	
	return $mGrupsProductorsRef;
}
?>

		