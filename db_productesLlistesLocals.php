<?php
//*v36 20-1-16   modificada funcio

function db_getProductesLlistesLocals($db)
{
	global $mPars,$mSl,$perfilsRefActiusChain;

	$orderBy='';
	$where_=" id!=0 "; //redundant
	
//*v36-15-11-26 condicio
	if($mPars['sortBy']!='')
	{
		$orderBy=$mPars['sortBy'];
		
		if($mPars['sortBy']=='productor')
		{
			$orderBy=" (SUBSTRING(productor,LOCATE('-',productor))) ";
		}
		
		$orderBy=" order by ".$orderBy." ".$mPars['ascdesc'];
	}

	if($mPars['veureProductesDisponibles']==1)
	{
		$where_.=" AND actiu=1 ";
	}

	//aplicacio del filtre
	if($mPars['etiqueta']==$mPars['etiqueta2']){$mPars['etiqueta']='TOTS';}

	if($mPars['etiqueta2']!="CAP"){$mPars['etiqueta']='TOTS';}

	if($mPars['vProductor']!='TOTS')
	{
		if(str_replace(',','',$mPars['vProductor'])!=''){$where_productors=' AND (';$where_productors_fi=') ';}else{$where_productors='';$where_productors_fi='';}
		
		$mProductorsId=explode(',',$mPars['vProductor']);
		while(list($key,$productorId)=each($mProductorsId))
		{
			if($productorId!='' && 	substr_count($perfilsRefActiusChain,','.$productorId.',')>0)
			{
				$where_productors.=" SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$productorId."' OR ";
			}
		}
		if($where_productors!='')
		{
			$where_productors=substr($where_productors,0,strlen($where_productors)-3);
		}
		$where_.=$where_productors.$where_productors_fi;
	}

	if($mPars['vCategoria']!='TOTS')
	{
		$where_.=" AND  categoria0='".$mPars['vCategoria']."' ";
		
	}

	if($mPars['vSubCategoria']!='TOTS')
	{
		$categoria0=substr($mPars['vSubCategoria'],0,strpos($mPars['vSubCategoria'],'-'));
		$categoria10=substr($mPars['vSubCategoria'],strpos($mPars['vSubCategoria'],'-')+1);

		$where_.=" AND  categoria0='".$categoria0."' AND categoria10='".$categoria10."' ";
	}
	
	if($mPars['etiqueta']!='TOTS')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta']."',tipus)!=0 ";
	}

	if($mPars['etiqueta2']!='CAP')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta2']."',tipus)=0 ";
	}

		if(count($mSl)>0)
		{
			$where_.=" AND (";		
			while(list($key,$val)=each($mSl))
			{
				$where_.=" llista_id=".$val." || ";
			}
			$where_=substr($where_,0,strlen($where_)-3).")";		
		}
	$mProductes=array();
	
	//echo "<br>select * from productes_grups where ".$where_." ".$orderBy;
	if(!$result=mysql_query("select * from productes_grups where ".$where_." ".$orderBy,$db))
	{
		//echo "<br> 91 db_productesLlistesLocals.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mPars['paginacio']==1)
			{
				if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
				{
					$mRow['visible']=1;
					$mProductes[$mRow['id']]=$mRow;
				}
				else
				{
					$mRow['visible']=0;
					$mProductes[$mRow['id']]=$mRow;
				}
			}
			else
			{
				$mRow['visible']=1;
				$mProductes[$mRow['id']]=$mRow;
			}
			
			$i++;
		}
	}
	return $mProductes;
}
//------------------------------------------------------------------------------
function db_getProduccio($db) // de momenmt no s'utilitza
{
	global 	$mPars,
			$mSelRutes,
			$mProductes,
			$sb,
			$sbd,
			$sg,
			$sZ,
			$sSz,
			$mProductesNoTrobats;	

	$mProduccio=array();
	$rutaActual=$mPars['selRutaSufix'];
	$cont=0;

	$mProduccio=db_getProductesLlistesLocals($db);

	$productesSelSzChain='';
	$productesSelZchain='';
	if(isset($sSz) && $sSz!='' && $sSz!='TOTS')
	{
		$productesSelSzChain=db_getProductesSzona($sSz,$db);
	}
	else if(isset($sZ) && $sZ!='' && $sZ!='TOTS')
	{
		$productesSelZchain=db_getProductesZona($sZ,$db);
	}

	while(list($index,$mProduccio_)=each($mProduccio))
	{
		$mProduccio[$index]['quantitat']=0;
		$mProduccio[$index]['ums']=0;
		$mProduccio[$index]['ecos']=0;
		$mProduccio[$index]['euros']=0;
							
	}
	reset($mProduccio);

	$mProduccioX=array();
		
	while(list($index,$mVal)=each($mProduccio))
	{
		if(!isset($mProduccioX[$mVal[$sb]])){$mProduccioX[$mVal[$sb]]=array();}
		$mProduccioX[$mVal[$sb]][$mVal['id']]=$mVal;
	}
	reset($mProduccio);

	if(in_array($sb,array('producte','productor','unitat_facturacio')))
	{
		ksort($mProduccioX,SORT_STRING);
	}
	else
	{
		ksort($mProduccioX,SORT_NUMERIC);
	}

	if($sbd=='descendent')
	{
		$mProduccioX=array_reverse($mProduccioX,true);
	}
	
	return $mProduccioX;
}

//------------------------------------------------------------------------

function db_getEstadistiquesPcsv($db) //de moment no s'utilitza
{
	global $mPars,$mProduccioXcsv,$mSb,$mSelRutes,$sb,$sbd,$sg,$mPerfilsRef,
//*v36 20-1-16 2 decls globals
	$sSz,$sZ;
	
	$dir0=getcwd();
	$dir="docs".$mPars['selRutaSufix'];
	if(!@chdir($dir))
	{
		mkdir($dir);
	}
	else
	{
		chdir($dir0);
	}
	@unlink("docs".$mPars['selRutaSufix']."/estadistiquesP_".$mPars['usuari_id'].".csv");

	if(!$fp=fopen("docs".$mPars['selRutaSufix']."/estadistiquesP_".$mPars['usuari_id'].".csv",'w'))
	{
		return false;
	}

	$mH1=array('','resum sobre rutes: '.(implode(',',$mSelRutes)));
	fputcsv($fp, $mH1,',','"');
//*v36 20-1-16 2 linies csv
	if($sSz!='' && $sSz!='TOTS' )
	{
		$mH1=array('','SuperZona: '.$sSz);
		fputcsv($fp, $mH1,',','"');
	}
	if($sZ!='' && $sZ!='TOTS' )
	{
		$mH1=array('','Zona: '.$sZ);
		fputcsv($fp, $mH1,',','"');
	}
	if($sg!='')
	{
		$mH1=array('',(urldecode($mPerfilsRef[$sg]['projecte'])));
		fputcsv($fp, $mH1,',','"');
	}
	$mH1=array('','ordenat per: '.$sb);
	fputcsv($fp, $mH1,',','"');
	$mH1=array('','en sentit: '.$sbd);
	fputcsv($fp, $mH1,',','"');
	$mH1=array('',date('d/m/Y hh:mm'));
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	fputcsv($fp, $mSb,',','"');
	while(list($key,$mProducteLlista)=each($mProduccioXcsv))
	{
    	if(!fputcsv($fp, $mProducteLlista,',','"'))
		{
			return false;
		}
	}	
	reset($mProduccioXcsv);
	fclose($fp);
	return;
}

//------------------------------------------------------------------------------
function db_getComandesLlistesLocals($db)
{
	global $mPars,$mPeriodes;
	
	$mComandesLlistesLocals=array();
	
	if(!$result=mysql_query("select * from comandes_grups WHERE usuari_id='0' ORDER by id DESC",$db))
	{
		echo "<br> 262 db_productesLlistesLocals.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPeriodes[$mRow['id']]=$mRow;
		}
	}

	if(!$result=mysql_query("select * from comandes_grups WHERE usuari_id!='0' ORDER by id DESC",$db))
	{
		echo "<br> 279 db_productesLlistesLocals.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			if(!isset($mComandesLlistesLocals[$grupId])){$mComandesLlistesLocals[$grupId]=array();}
			if(!isset($mComandesLlistesLocals[$grupId][$mRow['periode_comanda']])){$mComandesLlistesLocals[$grupId][$mRow['periode_comanda']]=array();}

			$mComandesLlistesLocals[$grupId][$mRow['periode_comanda']][$mRow['usuari_id']]=$mRow;
		}
	}

	return $mComandesLlistesLocals;
}
?>

		