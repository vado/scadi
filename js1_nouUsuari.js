allowedCharsEmail='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.@';
allowedCharsCompteSoci='0123456789copCOP';
allowedCharsCompteEcos_lletres='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
emailNeededCharacters='@.';
allowedCharsMobil='0123456789';
allowedCharsCategoriesGrups='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-';
		
function checkFormNouUsuari()
{
	isValid=false;
	missatgeAlerta='';
	
	// camps obligatoris
	value=document.getElementById('i_usuari').value;
	if(value.length<2)
	{
		missatgeAlerta+="\nEl camp 'usuari' ha de tenir almenys 2 car�cters";			
	}

	value=document.getElementById('i_nom').value;
	if(value.length<2)
	{
		missatgeAlerta+="\nEl camp 'nom' ha de tenir almenys 2 car�cters";			
	}
			

	value=document.getElementById('sel_municipi').value;
	if(value.length==0)
    {
		missatgeAlerta+="\nCal seleccionar un municipi";			
    }

	mValue=document.getElementById('sel_grups').value;
	if(mValue.length==0)
    {
		missatgeAlerta+="\nCal seleccionar un grup";			
    }

	value=document.getElementById('i_email').value;
	if(value.length<5)
    {
		missatgeAlerta+="\nEl camp 'email' ha de tenir m�s de 4 car�cters";
    }
	else
	{
       	allowedChars=allowedCharsEmail;
		isValid='false';
		caractersNecessaris=2;        	
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){isValid=true;}
				if(value.charAt(i)==allowedChars.charAt(j) && value.charAt(i)=='.'){caractersNecessaris--;}
				if(value.charAt(i)==allowedChars.charAt(j) && value.charAt(i)=='@'){caractersNecessaris--;}
            }
		}
        if(caractersNecessaris>0)
        {
			missatgeAlerta+="\nEl camp 'email' cont� un valor incorrecte";			
        }
	}

	value=document.getElementById('sel_compteSoci').value;
	if(value.length>0 && (value.length<8 || value.length>8))
    {
		missatgeAlerta+="\nEl camp 'compteSoci' ha de tenir 8 car�cters";
    }
	else if(value.length==8)
	{
       	allowedChars=allowedCharsCompteSoci;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
				
            }
		}
        if($_ok<8)
        {
			missatgeAlerta+="\nEl camp 'compteSoci' cont� un valor incorrecte";			
        }
		else
		{
			value=' '+value.toLowerCase(value);
			if(value.indexOf('coop')!=1)
			{
				missatgeAlerta+="\nEl camp 'compteSoci' ha de comen�ar per 'coop'";			
			}
		}
	}

	value=document.getElementById('i_mobil').value;
	if(value.length>0 && value.length!=9)
    {
		missatgeAlerta+="\nEl camp 'mobil' ha de tenir 9 car�cters";			
    }
	else if(value.length==9)
	{
       	allowedChars=allowedCharsMobil;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<9)
        {
			missatgeAlerta+="\nEl camp 'mobil' cont� un valor incorrecte";			
        }
	}
		
	value=document.getElementById('i_compteEcos').value;
	if(value.length!=0 && value.length!=8)
    {
		missatgeAlerta+="\nEl camp 'compte ecos' ha de tenir 0 � 8 car�cters";			
    }
	else if( value.length>0) 
	{
	   	allowedChars=allowedCharsCompteEcos_lletres;
		ok=0;
		//check for prohibited characters
       	for(i=0;i<4;i++)
        {
           for(j=0;j<allowedChars.length;j++)
           {
               if(value.charAt(i)==allowedChars.charAt(j)){ok++;}
           }
		}
	   	allowedChars=allowedCharsMobil;
		if(ok<4)
		{
			missatgeAlerta+="\nCamp 'compte ecos': els primers 4 d�gits han de ser lletres (car�cters permesos: "+allowedChars+" )";			
		}
		ok=0;
		//check for prohibited characters
       	for(i=4;i<9;i++)
        {
           for(j=0;j<allowedChars.length;j++)
           {
               if(value.charAt(i)==allowedChars.charAt(j)){ok++;}
           }
		}
		if(ok<4)
		{
			missatgeAlerta+="\nCamp 'compte ecos': els �ltims 4 d�gits han de ser nombres (car�cters permesos: "+allowedChars+" )";			
		}
	}
	
	if(moneda3Activa==1)
	{
		value=document.getElementById('i_compteCieb').value;
		if(value.length!=0 && value.length!=moneda3DigitsCodi)
    	{
			missatgeAlerta+="\nEl camp 'Compte "+moneda3Nom+"' ha de tenir 0 � "+moneda3DigitsCodi+" car�cters";			
    	}
		else if (value.length>0)
		{
	   		allowedChars=allowedCharsMobil;
			$_ok=0;
			//check for prohibited characters
       		for(i=0;i<moneda3DigitsCodi;i++)
        	{
            	for(j=0;j<allowedChars.length;j++)
            	{
                	if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            	}
			}
        	if($_ok<moneda3DigitsCodi)
        	{
				missatgeAlerta+="\nEl camp 'Compte "+moneda3Nom+"' cont� un valor incorrecte";			
        	}
		}
	}
		
	value=document.getElementById('i_contrasenya').value;
	if(value.length<6 || value.length>12)
    {
		missatgeAlerta+="\nEl camp 'Contrasenya' ha de tenir entre 6 i 12 car�cters";			
    }
	else
	{
	   	allowedChars=allowedCharsCategoriesGrups;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<value.length-1)
        {
			missatgeAlerta+="\nEl camp 'Contrasenya' cont� un valor incorrecte \n\n(car�cters permesos:"+allowedCharsCategoriesGrups+")";			
        }
	}

	if (missatgeAlerta.length==0)
	{
		return true;
	}
  return false;
}	




function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
		
	return;
}


function veureContrasenyaFormNouUsuari()
{
	val2307141227=document.getElementById('cb_veureContrasenya').value;
	if(val2307141227==0)
	{
		val2307141227=1;
		document.getElementById('cb_veureContrasenya').value=1;
		document.getElementById('i_contrasenya').type='text';
		document.getElementById('i_contrasenya').checked=true;
	}
	else
	{
		document.getElementById('cb_veureContrasenya').value=0;
		document.getElementById('i_contrasenya').type='password';
		document.getElementById('i_contrasenya').checked=false;
	}
	
	return;	
}
		

		
		
		
					