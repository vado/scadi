
SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
CREATE TABLE IF NOT EXISTS `trams_1404` (
`id` int(10) NOT NULL  auto_increment,
`codi` int(20) NOT NULL  ,
`sortida` datetime NOT NULL  ,
`municipi_origen` int(10) NOT NULL  ,
`arribada` datetime NOT NULL  ,
`municipi_desti` int(10) NOT NULL  ,
`municipis_ruta` varchar(300) NOT NULL  ,
`periodicitat` varchar(300) NOT NULL  ,
`actiu` tinyint(1) NOT NULL  ,
`vehicle_id` varchar(200) NOT NULL  ,
`usuari_id` int(10) NOT NULL  ,
`categoria0` varchar(300) NOT NULL  ,
`categoria10` varchar(300) NOT NULL  ,
`tipus` varchar(300) NOT NULL  ,
`capacitat_pes` int(10) NOT NULL  ,
`pes_disponible` int(10) NOT NULL  ,
`capacitat_volum` int(10) NOT NULL  ,
`volum_disponible` int(10) NOT NULL  ,
`capacitat_places` int(10) NOT NULL  ,
`places_disponibles` int(10) NOT NULL  ,
`km` int(10) NOT NULL  ,
`estat` varchar(300) NOT NULL  ,
`descripcio` varchar(300) NOT NULL  ,
`acords_explicits` text NOT NULL  ,
`preu_pes` int(11) NOT NULL  ,
`preu_volum` int(11) NOT NULL  ,
`preu_places` int(11) NOT NULL  ,
`preu_combustible` float NOT NULL  ,
`pc_ms` float NOT NULL  ,
`propietats` text NOT NULL  ,
PRIMARY KEY (`id`),
KEY `id` (`id`)			
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;
				