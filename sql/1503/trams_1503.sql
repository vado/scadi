
SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
CREATE TABLE IF NOT EXISTS `trams_1503` (
`id` int(10) NOT NULL  auto_increment,
`codi` int(20) NOT NULL  ,
`sortida` datetime NOT NULL  ,
`municipi_origen` int(10) NOT NULL  ,
`arribada` datetime NOT NULL  ,
`municipi_desti` int(10) NOT NULL  ,
`municipis_ruta` varchar(300) NOT NULL  ,
`periodicitat` varchar(300) NOT NULL  ,
`actiu` tinyint(1) NOT NULL  ,
`vehicle_id` varchar(200) NOT NULL  ,
`usuari_id` int(10) NOT NULL  ,
`categoria0` varchar(300) NOT NULL  ,
`categoria10` varchar(300) NOT NULL  ,
`tipus` varchar(300) NOT NULL  ,
`capacitat_pes` int(10) NOT NULL  ,
`pes_disponible` int(10) NOT NULL  ,
`capacitat_volum` int(10) NOT NULL  ,
`volum_disponible` int(10) NOT NULL  ,
`capacitat_places` int(10) NOT NULL  ,
`places_disponibles` int(10) NOT NULL  ,
`km` int(10) NOT NULL  ,
`estat` varchar(300) NOT NULL  ,
`descripcio` varchar(300) NOT NULL  ,
`acords_explicits` text NOT NULL  ,
`preu_pes` int(11) NOT NULL  ,
`preu_volum` int(11) NOT NULL  ,
`preu_places` int(11) NOT NULL  ,
`preu_combustible` float NOT NULL  ,
`pc_ms` float NOT NULL  ,
`propietats` text NOT NULL  ,
PRIMARY KEY (`id`),
KEY `id` (`id`)			
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;
				
INSERT	into `trams_1503` (`id`,`codi`,`sortida`,`municipi_origen`,`arribada`,`municipi_desti`,`municipis_ruta`,`periodicitat`,`actiu`,`vehicle_id`,`usuari_id`,`categoria0`,`categoria10`,`tipus`,`capacitat_pes`,`pes_disponible`,`capacitat_volum`,`volum_disponible`,`capacitat_places`,`places_disponibles`,`km`,`estat`,`descripcio`,`acords_explicits`,`preu_pes`,`preu_volum`,`preu_places`,`preu_combustible`,`pc_ms`,`propietats`)	VALUES 
						('18','0','2015-02-02+18%3A00%3A00','945','2015-02-02+23%3A00%3A00','940','%2C946','','1','3','2','CAC','','OFERTES','0','0','0','0','5','5','100','','','','0','0','5','0','50','%7Bp%3Ahistorial%3Bus%3A2%3Bmunicipis_ruta%3A%252C946-%3E%2C946%3Bdt%3A02%2F02%2F2015+15%3A42%3A41%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bactiu%3A0-%3E1%3Bdt%3A02%2F02%2F2015+15%3A42%3A41%3B%7D'),
('17','0','2015-02-10+10%3A10%3A00','945','2015-02-10+11%3A10%3A00','944','%2C944%2C946','%25FAnic','1','','2','JJAA-CIC','','DEMANDES','0','0','0','0','5','3','100','PENDENT','','','0','0','5','1.23','50','%7Bp%3Ahistorial%3Bus%3A2%3Bmunicipis_ruta%3A%2C944%2C945-%3E%25252C947%252C944%252C945%2C944%2C946%3Bdt%3A17%2F01%2F2015+12%3A18%3A34%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bmunicipis_ruta%3A%2C944-%3E%2C944%2C945%3Bdt%3A17%2F01%2F2015+12%3A17%3A19%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bmunicipis_ruta%3A%252C944-%3E%2C944%3Bdt%3A17%2F01%2F2015+12%3A15%3A32%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bmunicipis_ruta%3A-%3E%252C944%3Bdt%3A17%2F01%2F2015+12%3A08%3A58%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bmunicipis_ruta%3A%25252C947%252C944%252C945-%3E%2525252C947%25252C944%25252C945%252C944%252C946%3Bdt%3A17%2F01%2F2015+12%3A07%3A51%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bmunicipi_desti%3A946-%3E944%3Bdt%3A17%2F01%2F2015+12%3A06%3A21%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bmunicipis_ruta%3A%252C947-%3E%25252C947%252C944%252C945%3Bdt%3A17%2F01%2F2015+12%3A06%3A21%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bmunicipis_ruta%3A-%3E%252C947%3Bdt%3A17%2F01%2F2015+12%3A02%3A12%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bperiodicitat%3A-%3E%25FAnic%3Bdt%3A17%2F01%2F2015+11%3A53%3A29%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bperiodicitat%3A%25FAnic-%3E%3Bdt%3A17%2F01%2F2015+11%3A41%3A47%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bperiodicitat%3A-%3E%25FAnic%3Bdt%3A17%2F01%2F2015+11%3A37%3A20%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bperiodicitat%3A%25FAnic-%3E%3Bdt%3A17%2F01%2F2015+11%3A37%3A07%3B%7D%7Bp%3Ahistorial%3Bus%3A2%3Bactiu%3A0-%3E1%3Bdt%3A17%2F01%2F2015+11%3A37%3A07%3B%7D'),
('19','0','2015-02-19+07%3A30%3A00','514','2015-02-19+08%3A15%3A00','886','','','1','4','1','CAC','','OFERTES','200','200','0','0','5','3','43','','','Tram%2BCAC%253A%2Bpendent%2Bvalidar','0','0','0','0','50','%7Bp%3Ahistorial%3Bus%3A1%3Bactiu%3A0-%3E1%3Bdt%3A18%2F02%2F2015+13%3A26%3A43%3B%7D'),
('20','0','2015-02-19+08%3A30%3A00','886','2015-02-19+09%3A00%3A00','224','','','1','4','1','CAC','','OFERTES','200','200','0','0','5','5','29','','','Tram%2BCAC%253A%2Bpendent%2Bvalidar','0','0','0','0','50','%7Bp%3Ahistorial%3Bus%3A1%3Bactiu%3A0-%3E1%3Bdt%3A18%2F02%2F2015+13%3A31%3A08%3B%7D'),
('21','0','2015-02-19+09%3A20%3A00','224','2015-02-19+10%3A30%3A00','514','','','1','4','1','CAC','','OFERTES','200','150','0','0','5','4','39','','','Tram%2BCAC%253Apendent%2Bvalidar','0','0','0','0','50','%7Bp%3Ahistorial%3Bus%3A1%3Bactiu%3A0-%3E1%3Bdt%3A18%2F02%2F2015+13%3A35%3A34%3B%7D');
