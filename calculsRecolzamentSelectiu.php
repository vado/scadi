<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include "db_calculsRecolzamentSelectiu.php";
include "db_distribucioGrups.php";
include "html_ajuda1.php";
include "db_ajuda.php";

$modeSimulacre=1;

$mMissatgeAlerta=array();
$mMissatgeAlerta['missatge']='';
$mMissatgeAlerta['result']=false;

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

	
$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}

	$mPars['taulaProductes']='productes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandes']='comandes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandesSeg']='comandes_seg';
	$mPars['taulaIncidencies']='incidencies_'.$mPars['selRutaSufix'];
	$mPars['taulaProductors']='productors';

	$mPars['vProducte']='TOTS';

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db); //inicialitza variables anteriors;


if
(
	$modeSimulacre==0 && date('ym')*1>$mPars['selRutaSufix']*1
)
{
	$accionsButton='disabled';
	$dadesVisibility='hidden';
}
else
{
	$accionsButton='';
	$dadesVisibility='inherit';
}

$login=false;
$login=checkLogin($db);
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['calculsRecolzamentSelectiu.php']=db_getAjuda('calculsRecolzamentSelectiu.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$mProductes=array();
$mRutesSufixes=getRutesSufixes($db);

//*v36 5-1-16 assignacio
$mPeriodesInfo=db_getPeriodesInfo($db);


$mPerfilsRef=db_getPerfilsRef($db);
$perfilsGrupIdChain=db_getPerfilsGrupIdChain($db);
$mUsuarisRef=db_getUsuarisRef($db);
$mGrupsRef=db_getGrupsRef($db);
$mGrupsProductorsRef=db_getGrupsProductorsRef($db);
$mGrupsNoProductorsRef=db_getGrupsNoProductorsRef($db);
$mRutesSufixes_=array();
$i=0;
while(list($key,$val)=each($mRutesSufixes))
{
//*v36 5-1-16 modificat contingut while
	$mRuta=explode('_',$val);
	if($key*1>=$mParametres['periodeIniciRecolzamentSelectiu']['valor'] && count($mRuta)==1)
	{
		$mRutesSufixes_[$i]=$val;
		$i++;
	}
}
reset($mRutesSufixes);
unset($mRutesSufixes);
$mRutesSufixes=$mRutesSufixes_;
//*v36 5-1-16 condicional
if($mPars['selRutaSufixPeriode']*1<$mParametres['periodeIniciRecolzamentSelectiu']['valor'])
{

	echo "<p>el recolzament selectiu no est� habilitat per aquest periode</p>";
	exit();
}

//nom�s a partir de 1507

if(!isset($mParametres['PPRECM']['valor'])){$mParametres['PPRECM']['valor']='0';}
if(!isset($mParametres['PPREGM']['valor'])){$mParametres['PPREGM']['valor']='0';}
if(!isset($mParametres['PPREGPM']['valor'])){$mParametres['PPREGPM']['valor']='0';}
if(!isset($mParametres['PPRERM']['valor'])){$mParametres['PPRERM']['valor']='0';}
if(!isset($mParametres['FABLRMC']['valor'])){$mParametres['FABLRMC']['valor']='1';}

$PPRETM=$mParametres['PPRECM']['valor']+$mParametres['PPREGM']['valor']+$mParametres['PPRERM']['valor']+$mParametres['PPREGPM']['valor'];
/*
PPRECM- partida pressupostaria recolzament ecos comissions mes en curs   ex. 6000
PPREGM- partida pressupostaria recolzament ecos productors mes en curs   ex: 1000
PPRERM- partida pressupostaria recolzament ecos rebosts-cal mes en curs  ex: 1000
FABLRMC- limit recolzament selectiu a un membre d'una comissi�, suposant pressupost suficient, ex: 2
*/
$mUsuarisComissionsRef=db_getUsuarisComissionsRef($db);
$mComptesUsuarisRef=db_getComptesUsuarisRef($db);
$mComptesGrupsNoProductorsRef=db_getComptesGrupsNoProductorsRef($db);
$mComptesGrupsProductorsRef=db_getComptesGrupsProductorsRef($db);
$distribucioComissions_=@$_POST['i_distribucioComissions'];
if($mPars['nivell']=='sadmin')
{
	if($modeSimulacre==1 || date('ym')<=$mPars['selRutaSufix'])
	{
	
	if(isset($distribucioComissions_) && $distribucioComissions_!='')
	{
		if($mPars['nivell']=='sadmin')
		{
			$mMissatgeAlerta=db_aplicarDistribucioComissions($distribucioComissions_,$db);
			$mComptesUsuarisRef=db_getComptesUsuarisRef($db);
		}
		else
		{
			$mMissatgeAlerta['missatge']="<p class='p_alertaNo4'> Per realitzar aquesta operaci� has de ser l'administrador </p>";
		}
	}

	$grupRid=@$_POST['i_grupRid'];
	$saldoEcosR=@$_POST['i_saldoEcosR'];
	if(isset($grupRid) && $grupRid!='' && isset($saldoEcosR) && $saldoEcosR!='')
	{
		if($mPars['nivell']=='sadmin')
		{
			$mMissatgeAlerta=db_aplicarRecolzamentGrup($saldoEcosR,$grupRid,$db);
		}
		else
		{
		$mMissatgeAlerta['missatge']="<p class='p_alertaNo4'> Per realitzar aquesta operaci� has de ser l'administrador </p>";
		}
		unset($mComptesGrupsRef);
		$mComptesGrupsRef=db_getComptesGrupsRef($db);
		$mComptesGrupsNoProductorsRef=db_getComptesGrupsNoProductorsRef($db);
	}

	$parametreNom=@$_POST['i_parametreNom'];
	$parametreValor=@$_POST['i_parametreValor'];
	if(isset($parametreNom) && $parametreNom!='' && isset($parametreValor) && $parametreValor!='')
	{
		if($mPars['nivell']=='sadmin')
		{
			$mMissatgeAlerta=db_guardarParametreRecolzament($parametreNom,$parametreValor,$db);
		}
		else
		{
			$mMissatgeAlerta['missatge']="<p class='p_alertaNo4'> Per realitzar aquesta operaci� has de ser l'administrador </p>";
		}
		$mParametres=getParametres($db);
	}

	$aplicarRecolzamentGP='';
	$aplicarRecolzamentGP=@$_POST['i_aplicarDistribucioGrupsProductors'];
	if(isset($aplicarRecolzamentGP) && $aplicarRecolzamentGP=='1')
	{
		$recolzamentGPchain=@$_POST['i_distribucioGrupsProductors'];
		$mMissatgeAlerta=db_aplicarDistribucioGrupsProductors($db);
		$mComptesGrupsProductorsRef=db_getComptesGrupsProductorsRef($db);
	}

	$mDades=array();
	if(isset($_FILES['f_dadesRecolzament']['name']))
	{
		$mMissatgeAlerta=recepcioFitxerDadesRecolzament($db);
		if(count($mDades)>0)
		{
			$mMissatgeAlerta=guardarDadesRecolzamentComissions($db);
			$mUsuarisRef=db_getUsuarisRef($db);
			$mComptesUsuarisRef=db_getComptesUsuarisRef($db);
			$mComptesGrupsRef=db_getComptesGrupsRef($db);
		}
	}
	}
}

$mGrupsCALref=db_getGrupsCALref($db);
$mGrupsNoProductorsRef=db_getGrupsNoProductorsRef($db);
$mGrupsProductorsRef=db_getGrupsProductorsRef($db);
$mGrupsProductorsRef=afegirPerfilsVinculats($mGrupsProductorsRef);
	$mPars['paginacio']=0; // <<<< b�sic!!!
	$mPars['calculsRecolzament']=1;
$mGrupsProductorsRef=db_afegirImports6mesosAnteriors($db);


//camps rebosts: els rebosts, grups i productors 

	$ecosTotalMembresComissions=0;
	$maxEcosRecolzatsTotalMembresComissions=0;
	While(list($usuariId,$mUsuariComissio)=each($mUsuarisComissionsRef))
	{
		$mPropietats=getPropietats($mUsuariComissio['propietats']);
		if(!isset($mComptesUsuarisRef[$usuariId]['ecos_ab'])){$mComptesUsuarisRef[$usuariId]['ecos_ab']=0;}
		if(!isset($mComptesUsuarisRef[$usuariId]['saldo_ecos_r'])){$mComptesUsuarisRef[$usuariId]['saldo_ecos_r']=0;}
		if(!isset($mComptesUsuarisRef[$usuariId]['utilitzat_ecos_r'])){$mComptesUsuarisRef[$usuariId]['utilitzat_ecos_r']=0;}
		if(!isset($mComptesUsuarisRef[$usuariId]['disponible_ecos_r'])){$mComptesUsuarisRef[$usuariId]['disponible_ecos_r']=0;}
		if(!isset($mComptesUsuarisRef[$usuariId]['utilitzat_ecos_r_chain'])){$mComptesUsuarisRef[$usuariId]['utilitzat_ecos_r_chain']='';}
		$ecosTotalMembresComissions+=$mComptesUsuarisRef[$usuariId]['ecos_ab'];
		
		$maxEcosRecolzatsTotalMembresComissions+=$mComptesUsuarisRef[$usuariId]['ecos_ab']*$mParametres['FABLRMC']['valor'];
	}
	reset($mUsuarisComissionsRef);

	
	$ecosTotalGrupsCAL=0;
	$maxEcosRecolzatsTotalGrupsCAL=0;
	

$disabledNotSadmin='DISABLED';
$readonlyNotSadmin='READONLY';
$visibilityNotSadmin='hidden';

if($mPars['nivell']=='sadmin')
{
	distribuirRecolzament($db);
	$disabledNotSadmin='';
	$readonlyNotSadmin='';
	$visibilityNotSadmin='';
}


echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>C�lculs Recolzament selectiu</title>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_calculsRecolzamentSelectiu.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<script type='text/javascript'  language='JavaScript'>
PPRECM=1*'".$mParametres['PPRECM']['valor']."'; 
PPREGM=1*'".$mParametres['PPREGM']['valor']."';
PPRERM=1*'".$mParametres['PPRERM']['valor']."';
PPREGPM=1*'".$mParametres['PPREGPM']['valor']."';
FABLRMC=1*'".$mParametres['FABLRMC']['valor']."';
PPRETM='".$PPRETM."';

PPRECM_=1*'".$mParametres['PPRECM']['valor']."'; 
PPREGM_=1*'".$mParametres['PPREGM']['valor']."';
PPRERM_=1*'".$mParametres['PPRERM']['valor']."';
PPREGPM_=1*'".$mParametres['PPREGPM']['valor']."';
FABLRMC_=1*'".$mParametres['FABLRMC']['valor']."';
PPRETM_='".$PPRETM."';

trg=0; // total recolzament grups no productores
srg=0; //sobrant recolzament grups no productors
trr=0; // total recolzament rebosts-cal
srr=0; //sobrant recolzament rebosts-cal

nr=0;	// nombre de rebosts-cal
ng=0;   //nombre de grups no productors
ngp=0;  //nompre de grups productors

";
$distribucioComissions='';
while(list($usuariId,$mUsuariComissio)=each($mUsuarisComissionsRef))
{
	$distribucioComissions.=$usuariId.':'.@$mUsuariComissio['saldoEcosRc'].';';
}
reset($mUsuarisComissionsRef);

echo "
distribucioComissions='".$distribucioComissions."';


</script>
</head>
<body onload=\"javascript:mostrarCalculRecolzamentProductors();mostrarResum();\" bgcolor='".$mColors['body']."'>
";
html_demo('calculsRecolzamentSelectiu.php?');
echo "
	
	<table align='center' style='width:80%;' bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			<br>
	";
//*v36 5-1-16 afegit parametre 1 a call
	mostrarSelectorRuta(1,'calculsRecolzamentSelectiu.php');
	echo "
			</th>
		</tr>

		<tr>
			<td style='width:100%;' align='center'>
			";
			if(strlen($mMissatgeAlerta['missatge'])>0)
			{
				echo "
			<div style='height:80px; width:300px; overflow-y:scroll;'>
			".$mMissatgeAlerta['missatge']."
			</div>
				";
			}
			echo "
			</td>
		</tr>

		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>Configuraci� del recolzament per aquest periode (".$mPars['selRutaSufix']."):
			</p>
			</td>
		</tr>
		<tr>
			<td style='width:100%;' align='center'>
			<table width='60%'>
				<tr>
					<td width='100%'>
					<p style='font-size:10px; text-align:justify;'>
					* Els ecos recolzats no son acumulables. Els que no s'utilitzin dins el mes pel
					que s'han assignat es sumaran als que es distribueixin en els mesos successius 
					fins a finalitzar el periode pressupostat. Els que no s'hagin utilitzat en 
					finalitzar el periode pressupostat retornaran al com�.
					<br> 
					El periode de recolzament actual correspon als mesos d'agost fins a desembre del 2015, ambd�s inclosos. 
					<br>
					L'import total a repartir entre aquests cinc mesos es de 12500 euros, que correspon 
					a 2500 euros per mes. La CAC es reserva 400 euros per cobrir el recolzament impl�cit 
					en els intercanvis. El sobrant no utilitzat es gestionar� de la mateixa forma que
					els ecos recolzats distribuits que no s'utilitzin.
					</p>
					</td>
				</tr>
			</table>
			<br>
			<div id='d_resum2'  align='center' style='width:80%;'>
			</div>
			<br>
			<center><p style='font-size:20px;'>Distribuci� actual d'ecos recolzats</p></center>

			<center><p>[ <b>Distribuci� recolzament Rebosts-CAL</b>: ]</p></center>
			<table border='0' align='center' style='width:80%;'>
				<tr>
					<td width='100%'>
					<table  border='1' align='center' style='width:100%;'>
						<tr>
							<th align='left' valign='top'>
							</th>
					
							<th align='left' valign='top'>
							<p>rebost-CAL:</p>
							</th>
							
							<th align='left' valign='top'>
							<p>saldo ecos recolzats:</p>
							</th>
					
							<th align='left' valign='top'>
							<p>sobrant ecos recolzats:</p>
							</th>
						</tr>
			";
			$nr=0;
			$sumaEcosRgrupsCAL=0;
			While(list($grupId,$mGrupCAL)=each($mGrupsCALref))
			{
			
				echo "
						<tr>
							<td align='left' valign='top'>
							<p>".$nr."</p>
							</td>
					
							<td align='left' valign='top'>
							<p>".(urldecode($mGrupCAL['nom']))."</p>
							</td>
							
							<td align='right' valign='top'>
							<input size='10' id='i_grupCAL_".$nr."' type='text'  value='".(number_format($mComptesGrupsRef[$grupId]['saldo_ecos_r'],2,'.',''))."'>
							<input type='hidden' id='i_grupCAL2_".$nr."' value='".(number_format($mComptesGrupsRef[$grupId]['saldo_ecos_r'],2,'.',''))."'>
							<input class='i_micro' type='button' ".$accionsButton." ".$disabledNotSadmin." style='visibility:".$visibilityNotSadmin.";' onClick=\"javascript:if(comprovarCanvisRecolzamentRebost('i_grupCAL_".$nr."','i_grupCAL2_".$nr."')){aplicarRecolzamentGrup('i_grupCAL_".$nr."','".$grupId."');}\" value='guardar'>
							</td>
					
							<td align='left' valign='top'>
							</td>
						</tr>
				";
				$sumaEcosRgrupsCAL+=$mComptesGrupsRef[$grupId]['saldo_ecos_r'];
				$nr++;
			}
			reset($mGrupsCALref);
			echo "
						<tr>
							<td align='left' valign='top'>
							</td>
							
							<td align='left' valign='top'>
							<p>totals:</p>
							</td>
							
							<td align='right' valign='top'>
							<p><b>".(number_format($sumaEcosRgrupsCAL,2,'.',''))."</b></p>
							</td>
					
							<td align='right' valign='top'>
							<p><b>".(number_format(($mParametres['PPRERM']['valor']-$sumaEcosRgrupsCAL),2,'.',''))."</b></p>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>

			<br>
			<br>
			<center><p>[ <b>Distribuci� recolzament Grups Productors</b> ]</p></center>
			<div style='height:400px; width:80%; overflow-y:scroll;'>
			<table border='1' align='center' style='width:100%;' bgcolor='#eeeeee'>
				<tr>
					<th align='left' valign='top'>
					</th>
					<th align='left' valign='top'>
					<p>Grup:</p>
					</th>

					<th align='left' valign='top' style='visibility:".$dadesVisibility.";'>
					<p>%MS acord CAC:</p>
					</th>

					<th align='left' valign='top' style='visibility:".$dadesVisibility.";'>
					<p>%MS acceptat:</p>
					</th>

					<th align='left' valign='top' style='visibility:".$dadesVisibility.";'>
					<p>Import<br>distribuci� CAC<br>6 mesos<br>anteriors</p>
					</th>

					<th align='left' valign='top' style='visibility:".$dadesVisibility.";'>
					<p>Import<br>Comandes a CAC<br>6 mesos<br>anteriors</p>
					</th>

					<th align='left' valign='top' style='visibility:".$dadesVisibility.";'>
					<p>Factor de recolzament:</p>
					</th>

					<th align='left' valign='top' bgcolor='LightYellow' style='visibility:".$visibilityNotSadmin.";'  style='visibility:".$dadesVisibility.";'>
					<p>saldo ecos<br>recolzats:
					<br>(c�lcul)</p>
					<input type='button'  ".$accionsButton." ".$disabledNotSadmin."  value='aplicar' onClick=\"aplicarDistribucioGrupsProductors();\">
			
					</th>
					
					<th align='left' valign='top'>
					<p>ecos<br>recolzats:
					<br>(aplicat)</p>
					</th>

				</tr>
			";
			$sumaEcosRgrupsP=0;
			$sumaFactorRP=0;
			$maxPuntsFactorRP=0;
			While(list($grupId,$mGrupP)=each($mGrupsProductorsRef))
			{
				if(!isset($mGrupsProductorsRef[$grupId]['compresEc'])){$mGrupsProductorsRef[$grupId]['compresEc']=0;}
				if(!isset($mGrupP['compresEb'])){$mGrupsProductorsRef[$grupId]['compresEb']=0;}
				if(!isset($mGrupP['compresEu'])){$mGrupsProductorsRef[$grupId]['compresEu']=0;}
				if(!isset($mGrupP['reservesEc'])){$mGrupsProductorsRef[$grupId]['reservesEc']=0;}
				if(!isset($mGrupP['reservesEu'])){$mGrupsProductorsRef[$grupId]['reservesEu']=0;}
				if(!isset($mGrupP['factorRP'])){$mGrupsProductorsRef[$grupId]['factorRP']=0;}
				if(!isset($mGrupP['%ms'])){$mGrupsProductorsRef[$grupId]['%ms']=0;}
				$mGrupsProductorsRef[$grupId]['maxRecolzament']=FALSE;
				$reserves=$mGrupsProductorsRef[$grupId]['reservesEc']+$mGrupsProductorsRef[$grupId]['reservesEu'];
				$compres=$mGrupsProductorsRef[$grupId]['compresEc']+$mGrupsProductorsRef[$grupId]['compresEb']+$mGrupsProductorsRef[$grupId]['compresEu'];
				$difRC=ABS($reserves-$compres);
		
				if($reserves!=0 && $compres!=0 && $difRC>=1)
				{
					//$factorR=($mGrupP['acord1']/100)*$reserves*($compres*$compres)/(100*(sqrt($difRC)));
					$mGrupsProductorsRef[$grupId]['factorRP']=(($mGrupsProductorsRef[$grupId]['%ms']/100)*$reserves+$compres*pow($compres,1/3)/pow($difRC,1/3));
				}
				else if($reserves!=0 && $compres!=0)
				{
					$mGrupsProductorsRef[$grupId]['factorRP']=(($mGrupsProductorsRef[$grupId]['%ms']/100)*$reserves+$compres*pow($compres,1/3));
				}
				else
				{
					$mGrupsProductorsRef[$grupId]['factorRP']=0;
				}
				$sumaEcosRgrupsP+=@$mComptesGrupsProductorsRef[$grupId]['saldo_ecos_r'];
				$sumaFactorRP+=@$mGrupsProductorsRef[$grupId]['factorRP'];
			}
			reset($mGrupsProductorsRef);
			$valorPuntFactorRP=$mParametres['PPREGPM']['valor']/$sumaFactorRP;
			//$maxPuntsFactorRP=($compres/6)*1.5/$valorPuntFactorRP;
			
			//tallar i redistribuir
			tallarIredistribuirFactorRP($valorPuntFactorRP);			
			

			$ngp=0;
			$sumaFactorRP=0;
			While(list($grupId,$mGrupP)=each($mGrupsProductorsRef))
			{
				$sumaFactorRP+=$mGrupsProductorsRef[$grupId]['factorRP'];
				echo "
				<tr>
					<td align='left' valign='top'>
					<p>".$ngp."</p>
					<input type='hidden' id='i_gp_id_".$ngp."' value='".$mGrupP['id']."'
					</td>

					<td align='left' valign='top'>
					<p>".(urldecode($mGrupP['nom']))."</p>
					</td>

					<td align='right' valign='top'  style='visibility:".$dadesVisibility.";'>
					<p>".(@$mGrupP['acord1'])." </p>
					</td>

					<td align='right' valign='top'  style='visibility:".$dadesVisibility.";'>
					<p>".(number_format($mGrupsProductorsRef[$grupId]['%ms'],2,'.',''))." </p>
					</td>

					<td align='right' valign='top'  style='visibility:".$dadesVisibility.";'>
					<p>".(number_format(($mGrupP['reservesEc']+$mGrupP['reservesEu']),'2','.',''))." </p>
					</td>

					<td align='right' valign='top'  style='visibility:".$dadesVisibility.";'>
					<p>".(number_format(($mGrupP['compresEc']+$mGrupP['compresEb']+$mGrupP['compresEu']),'2','.',''))."</p>
					</td>
					
					<td align='right' valign='top' style='visibility:".$dadesVisibility.";'>
					<p id='p_factorRP_".$ngp."'>".(number_format($mGrupP['factorRP'],'2','.',''))."</p>
					</td>

					<td align='right' valign='top' bgcolor='LightYellow' style='visibility:".$visibilityNotSadmin.";'  style='visibility:".$dadesVisibility.";'>
					<p id='p_recolzamentGP_calcul_".$ngp."'></p>
					</td>

					<td align='right' valign='top'>
					<p id='p_recolzamentGP_actual_".$ngp."'>".(@number_format($mComptesGrupsProductorsRef[$grupId]['saldo_ecos_r'],2,'.',''))."</p>
					</td>
				</tr>
				";
				$ngp++;
			}
			reset($mGrupsProductorsRef);
			echo "
				<tr>
					<td align='left' valign='top'>
					</td>
					<td align='left' valign='top'>
					</td>
					<td align='left' valign='top'  style='visibility:".$dadesVisibility.";'>
					</td>
					<td align='left' valign='top' style='visibility:".$dadesVisibility.";'>
					</td>
					<td align='left' valign='top' style='visibility:".$dadesVisibility.";'>
					</td>
					<td align='left' valign='top style='visibility:".$dadesVisibility.";''>
					</td>

					<td align='left' valign='top'>
					<p>totals:</p>
					</td>

					<td align='right' valign='top'  bgcolor='LightYellow' style='visibility:".$visibilityNotSadmin.";'>
					<p id='p_sumaEcosRGrupsP_calcul'></p>
					</td>

					";
					if(number_format(($sumaEcosRgrupsP),2,'.','')!=number_format($mParametres['PPREGPM']['valor'],2,'.',''))
					{
						$color='red';
					}
					else{	$color='blue';}
					
					echo"
					<td align='right' valign='top'>
					<p style='color:".$color.";'><b>".(number_format($sumaEcosRgrupsP,2,'.',''))."/".(number_format($mParametres['PPREGPM']['valor'],2,'.',''))."</b></p>
					</td>
					";
					echo "

				</tr>
			</table>
			</div>
			
			<br>
			<br>
			<center><p>[ <b>Distribuci� recolzament Grups No Productors</b> ]</p></center>
			<div style='height:400px; width:80%; overflow-y:scroll;'>
			<table border='1' align='center' style='width:100%;' bgcolor='#eeeeee'>
				<tr>
					<th align='left' valign='top'>
					</th>
					<th align='left' valign='top'>
					<p>Grup:</p>
					</th>
					<th align='left' valign='top'>
					<p>saldo ecos recolzats:</p>
					</th>
					<th align='left' valign='top'>
					<p>sobrant ecos recolzats:</p>
					</th>
				</tr>
			";
			$ng=0;
			$sumaEcosRgrupsNp=0;
			While(list($grupId,$mGrupRnP)=each($mGrupsNoProductorsRef))
			{
				if(!isset($mComptesGrupsNoProductorsRef[$grupId])){$mComptesGrupsNoProductorsRef[$grupId]=array();}
				if(!isset($mComptesGrupsNoProductorsRef[$grupId]['saldo_ecos_r'])){$mComptesGrupsNoProductorsRef[$grupId]['saldo_ecos_r']=0;}
				
					echo "
				<tr>
					<td align='left' valign='top'>
					<p>".$ng."</p>
					</td>
					<td align='left' valign='top'>
					<p>".(urldecode($mGrupRnP['nom']))."</p>
					</td>
					<td align='right' valign='top'>
					<input size='10' id='i_grupNpR_".$ng."' type='text' value='".(number_format($mComptesGrupsNoProductorsRef[$grupId]['saldo_ecos_r'],2,'.',''))."'>
					<input type='hidden' id='i_grupNpR2_".$ng."' value='".(number_format($mComptesGrupsNoProductorsRef[$grupId]['saldo_ecos_r'],2,'.',''))."'>
					<input class='i_micro' ".$disabledNotSadmin." ".$accionsButton." style='visibility:".$visibilityNotSadmin.";' type='button'  onClick=\"javascript:if(comprovarCanvisRecolzamentGrupNp('i_grupNpR_".$ng."','i_grupNpR2_".$ng."')){aplicarRecolzamentGrup('i_grupNpR_".$ng."','".$grupId."');}\" value='guardar'>
					</td>
					<td align='left' valign='top'>
					</td>
				</tr>
					";
					$sumaEcosRgrupsNp+=$mComptesGrupsNoProductorsRef[$grupId]['saldo_ecos_r'];
					$ng++;
			}
			reset($mGrupsNoProductorsRef);
			echo "
				<tr>
					<td align='left' valign='top'>
					</td>
					<td align='left' valign='top'>
					<p>totals:</p>
					</td>
					<td align='right' valign='top'>
					<p><b>".(number_format($sumaEcosRgrupsNp,2,'.',''))."</b></p>
					</td>
					<td align='right' valign='top'>
					<p><b>".(number_format(($mParametres['PPREGM']['valor']-$sumaEcosRgrupsNp),2,'.',''))."</b></p>
					</td>
				</tr>
			</table>
			</div>

			<br>
			<br>
			<center><p>[ <b>Distribuci� recolzament membres comissions</b> ]</p></center>
			";
			if($mPars['nivell']=='sadmin')
			{
				echo "
			<FORM name='f_pujarDadesrecolzament' METHOD='POST' action='calculsRecolzamentSelectiu.php' target='_self'  ENCTYPE='multipart/form-data'>
			<input type='hidden' name='i_pars' value='".$parsChain."'>
			<table width='70%' align='center'>
				<tr>
					<td width='50%' valign='bottom'>
					<p class='p_micro2'>Importar fitxer <b>.csv</b> amb dades de recolzament  (membres comissions) :</p>
					</td>
							
					<td width='25%' valign='bottom'>
					<INPUT class='i_micro' TYPE='file' size='30' NAME='f_dadesRecolzament' accept='.csv'>
					</td>
							
					<td width='25%' valign='bottom'>
					<INPUT class='i_micro' type='submit' value='importar'>
					</td>
				</tr>
			</table>
			</form>
				";	
			}
			echo "
			
			<div style='height:400px; width:80%; overflow-y:scroll;'>
			<table border='1' align='center' style='width:100%;' bgcolor='#eeeeee'>
				<tr>
					<th align='left' valign='top'>
					</th>
					
					<th align='left' valign='top'>
					<p>usuaria:</p>
					</th>
					
					<th align='left' valign='top'>
					<p>comissi�:</p>
					</th>
					
					<th align='left' valign='top'>
					<p>Compte COOP:</p>
					</th>
					
					<th align='left' valign='top'>
					<p>ecos (ab):</p>
					</th>
					
					<th align='left' valign='top' bgcolor='lightYellow' style='visibility:".$visibilityNotSadmin.";'>
					<p>
					saldo ecos recolzats:
					<br>(c�lcul)
					<input type='button' ".$accionsButton." value='aplicar' onClick=\"aplicarDistribucioComissions();\">
					</p>
					</th>
					
					<th align='left' valign='top'>
					<p>saldo ecos recolzats:
					<br>(aplicat)
					</p>
					</th>
					
					<th align='left' valign='top'>
					<p>ecos recolzats
					<br>disponibles
					</p>
					</th>
					
					<th align='left' valign='top'>
					<p>ecos recolzats
					<br>utilitzats
					</p>
					</th>
					
					<th align='left' valign='top'>
					<p>ecos recolzats
					<br>utilitzats (resum)
					</p>
					</th>
				</tr>
			";
			$sumaEcosAbUsuarisComissions=0;
			$sumaEcosRusuarisComissions=0;
			$sumaEcosRusuarisComissionsC=0;
			$cont=0;
			While(list($usuariId,$mUsuariComissio)=each($mUsuarisComissionsRef))
			{
				$mPropietatsUsuari=getPropietats($mUsuariComissio['propietats']);
								echo "
				<tr>
					<td align='left' valign='top'>
					<p>".$cont."</p>
					</td>
					
					<td align='left' valign='top'>
					<p>".(urldecode($mUsuariComissio['usuari']))."</p>
					</td>
					
					<td align='left' valign='top'>
					<p class='p_micro2'>";
					
					$mComissionsUsuari=@explode(',',$mUsuariComissio['comissio']);
					while(list($key,$val)=each($mComissionsUsuari))
					{
						if($val!='')
						{
							echo "<br>".@$mPropietatsUsuaris['comissio'][$val];
						}
					}
					reset($mComissionsUsuari);
					
					echo "</p>
					</td>
					
					<td align='right' valign='top'>
					<p>"; if(isset($mPropietatsUsuari['compteSoci']) && $mPropietatsUsuari['compteSoci']!=''){echo $mPropietatsUsuari['compteSoci'];} echo "</p>
					</td>
					
					<td align='right' valign='top'>
					<p>".(number_format($mComptesUsuarisRef[$usuariId]['ecos_ab'],2,'.',''))."</p>
					</td>
					";
					
					if((number_format(@$mUsuariComissio['saldoEcosRc'],2,'.',''))!=(number_format($mComptesUsuarisRef[$usuariId]['saldo_ecos_r'],2,'.',''))){$fColor='red';}else{$fColor='black';}	
					echo "
					<td align='right' valign='top' bgcolor='lightYellow'  style='visibility:".$visibilityNotSadmin.";'>
					<p style='color:".$fColor.";'>".(number_format(@$mUsuariComissio['saldoEcosRc'],2,'.',''))."</p>
					</td>
					
					<td align='right' valign='top'>
					<p style='color:".$fColor.";'>".(number_format($mComptesUsuarisRef[$usuariId]['saldo_ecos_r'],2,'.',''))."</p>
					</td>
					
					<td align='right' valign='top'>
					<p style='color:".$fColor.";'>".(number_format($mComptesUsuarisRef[$usuariId]['disponible_ecos_r'],2,'.',''))."</p>
					</td>
					
					<td align='right' valign='top'>
					<p style='color:".$fColor.";'>".(number_format($mComptesUsuarisRef[$usuariId]['utilitzat_ecos_r'],2,'.',''))."</p>
					</td>
					
					<td align='right' valign='top'>
					<p style='color:".$fColor.";'>".$mComptesUsuarisRef[$usuariId]['utilitzat_ecos_r_chain']."</p>
					</td>
				</tr>
				";
				$sumaEcosAbUsuarisComissions+=$mComptesUsuarisRef[$usuariId]['ecos_ab'];
				$sumaEcosRusuarisComissions+=$mComptesUsuarisRef[$usuariId]['saldo_ecos_r'];
				$sumaEcosRusuarisComissionsC+=@$mUsuariComissio['saldoEcosRc'];
				$cont++;
			}
			reset($mUsuarisComissionsRef);
			echo "
				<tr>
					<td align='left' valign='top'>
					</td>

					<td align='left' valign='top'>
					</td>

					<td align='left' valign='top'>
					</td>
					
					<td align='right' valign='top'>
					<p>totals:</p>
					</td>
					
					<td align='right' valign='top'>
					<p><b>".(number_format($sumaEcosAbUsuarisComissions,2,'.',''))."</b></p>
					</td>

					<td align='right' valign='top' bgcolor='lightYellow' style='visibility:".$visibilityNotSadmin.";'>
					<p><b>".(number_format($sumaEcosRusuarisComissionsC,2,'.',''))."</b></p>
					</td>

					<td align='left' valign='top'  >
					</td>

					<td align='left' valign='top'>
					</td>

					<td align='left' valign='top'>
					</td>


					<td align='right' valign='top'>
					<p><b>".(number_format($sumaEcosRusuarisComissions,2,'.',''))."</b></p>
					</td>
				</tr>

				<tr>
					<td align='left' valign='top'>
					</td>

					<td align='left' valign='top'>
					</td>

					<td align='left' valign='top'>
					</td>
					

					<td align='right' valign='top'>
					<p>sobrants:</p>
					</td>

					<td align='right' valign='top'>
					<p><b>".(number_format(($mParametres['PPRECM']['valor']-$sumaEcosRusuarisComissions),2,'.',''))."</b></p>
					</td>

					<td align='right' valign='top' bgcolor='lightYellow' style='visibility:".$visibilityNotSadmin.";'>
					<p><b>".(number_format(($mParametres['PPRECM']['valor']-$sumaEcosRusuarisComissionsC),2,'.',''))."</b></p>
					</td>

					<td align='left' valign='top'>
					</td>

					<td align='left' valign='top'  >
					</td>
					<td align='left' valign='top'>
					</td>
					<td align='left' valign='top'>
					</td>

				</tr>
			</table>
			</div>
			</td>
		</tr>
	</table>
";
$recolzamentTotalAssignat=$sumaEcosRusuarisComissions+$sumaEcosRgrupsP+$sumaEcosRgrupsNp+$sumaEcosRgrupsCAL;

$recolzamentDisponible=$PPRETM-$recolzamentTotalAssignat;

if(number_format($sumaEcosRusuarisComissions,2,'.','')*1>1*number_format($mParametres['PPRECM']['valor'],2,'.','')){	$colorValorMC='red';}else{	$colorValorMC='black';}

if(number_format($sumaEcosRgrupsCAL,2,'.','')*1>1*number_format($mParametres['PPRERM']['valor'],2,'.','')){	$colorValorRC='red';}else{	$colorValorRC='black';}

if(number_format($sumaEcosRgrupsP,2,'.','')*1>1*number_format($mParametres['PPREGPM']['valor'],2,'.','')){	$colorValorGP='red';}else{	$colorValorGP='black';}

if(number_format($sumaEcosRgrupsNp,2,'.','')*1>1*number_format($mParametres['PPREGM']['valor'],2,'.','')){	$colorValorG='red';}else{	$colorValorG='black';}
echo "
	<br><br><br>&nbsp;
			<div id='d_resum1' style='z-index:-1; position:absolute; visibility:hidden;'>
			<table border='1' align='center' style='width:80%;'>
				<tr>
					<th align='left' valign='top'>
					<p>Recolzament d'ecos</p>
					</th>

					<th align='left' valign='top'>
					<p>Total<br>(ecos recolzats)</p>
					</th>

					<th align='left' valign='top'>
					<p>Assignat<br>(ecos recolzats)</p>
					</th>

					<th align='left' valign='top'>
					<p>Disponible<br>(ecos recolzats)</p>
					</th>

				</tr>
				
				<tr>
					<td align='left' valign='top'>
					<p>Membres Comissions:</p>					
					<p class='p_micro2'>
					total ecos ab membres comissions:<b>".(number_format($ecosTotalMembresComissions,2,'.',''))."</b>
					";
					if($mParametres['activarLimitFABLRMC']['valor']=='1')
					{
						echo "
					m�xim total ecos recolzats membres comissions:<b>".(number_format($maxEcosRecolzatsTotalMembresComissions,2,'.',''))."</b>
						";
					}
					else
					{
						$f1=$mParametres['PPRECM']['valor']/$ecosTotalMembresComissions;
						
						echo "
					<br>
					m�xim total ecos recolzats membres comissions:<b>".(number_format($mParametres['PPRECM']['valor'],2,'.',''))."</b><br>(100% pressupost, <b>".(number_format($f1,2,'.',''))."</b> x ecos ab)
						";
					}
					echo "
					</p>					
					<p class='p_micro2'><b>Factor limitant ecos recolzats
					<br> 'FABLRMC'(aplicat sobre ab ecos):</b></p>
					<table>
						<tr>
							<th align='left'><p class='p_micro2'>valor</p></th>
							<th align='left'><p class='p_micro2'>actiu</p></th>
						</tr>
						<tr>
							<td align='left'>
					<input class='i_micro' type='text'  ".$readonlyNotSadmin." size='10' id='i_FABLRMC' value='".(number_format($mParametres['FABLRMC']['valor'],2,'.',''))."'>
					<br>
					<input type='hidden' id='i_FABLRMC2' value='".(number_format($mParametres['FABLRMC']['valor'],2,'.',''))."'>
					<input class='i_micro' type='button'  ".$disabledNotSadmin." ".$accionsButton." style='visibility:".$visibilityNotSadmin.";' onClick=\"javascript:guardarParametre('FABLRMC');\" value='guardar'>
							</td>

							<td align='left'>
					";
					if($mParametres['activarLimitFABLRMC']['valor']=='0'){$selected0='selected';$selected1='';}
					else {$selected0='';$selected1='selected';}
					echo "
					<select class='sel_micro' id='i_activarLimitFABLRMC'  ".$disabledNotSadmin.">
					<option ".$selected0." value='0'>no</option>
					<option ".$selected1." value='1'>si</option>
					</select>
					<br>
					<input type='hidden' id='i_activarLimitFABLRMC2' value='".(number_format($mParametres['activarLimitFABLRMC']['valor'],2,'.',''))."'>
					<input class='i_micro' type='button'  ".$disabledNotSadmin."  ".$accionsButton." style='visibility:".$visibilityNotSadmin.";' onClick=\"javascript:guardarParametre('activarLimitFABLRMC');\" value='guardar'>
							</td>
						</tr>
					</table>
					</td>
					
					<td align='left' valign='top'>
					<input type='text' style='color:".$colorValorMC.";' size='10' id='i_PPRECM'value='".(number_format($mParametres['PPRECM']['valor'],2,'.',''))."'>
					<input type='hidden' id='i_PPRECM2' value='".(number_format($mParametres['PPRECM']['valor'],2,'.',''))."'>
					<input class='i_micro' type='button'  ".$disabledNotSadmin."  ".$accionsButton." style='visibility:".$visibilityNotSadmin.";' onClick=\"javascript:guardarParametre('PPRECM');\" value='guardar'>
					</td>
					
					
					<td align='left' valign='top'>
					<p id='p_tmc' style='color:".$colorValorMC.";' >".(number_format($sumaEcosRusuarisComissions,2,'.',''))."</p>
					</td>
					
					<td align='left' valign='top'>
					<p style='color:".$colorValorMC.";'>  ".(number_format(($mParametres['PPRECM']['valor']-$sumaEcosRusuarisComissions),2,'.',''))."</p>
					</td>
				</tr>
			
				<tr>
					<td align='left' valign='top'>
					<p>Rebosts-CAL:</p>					
					</td>
					
					<td align='left' valign='top'>
					<input type='text'  style='color:".$colorValorRC.";' size='10' id='i_PPRERM' value='".(number_format($mParametres['PPRERM']['valor'],2,'.',''))."'>
					<input type='hidden' id='i_PPRERM2' value='".(number_format($mParametres['PPRERM']['valor'],2,'.',''))."'>
					<input class='i_micro' type='button'  ".$disabledNotSadmin."  ".$accionsButton." style='visibility:".$visibilityNotSadmin.";' onClick=\"javascript:guardarParametre('PPRERM');\" value='guardar'>
					</td>

					<td align='left' valign='top'>
					<p id='p_trr' style='color:".$colorValorRC.";'>".(number_format($sumaEcosRgrupsCAL,2,'.',''))."</p>
					</td>

					<td align='left' valign='top'>
					<p  style='color:".$colorValorRC.";'  >".(number_format(($mParametres['PPRERM']['valor']-$sumaEcosRgrupsCAL),2,'.',''))."</p>
					</td>
				</tr>

				<tr>
					<td align='left' valign='top'>
					<p>Grups Productors:</p>					
					</td>

					<td align='left' valign='top'>
					<input type='text' style='color:".$colorValorGP.";' size='10' id='i_PPREGPM' value='".(number_format($mParametres['PPREGPM']['valor'],2,'.',''))."'>
					<input type='hidden' id='i_PPREGPM2' value='".(number_format($mParametres['PPREGPM']['valor'],2,'.',''))."'>
					<input class='i_micro' type='button'  ".$disabledNotSadmin."  ".$accionsButton." style='visibility:".$visibilityNotSadmin.";' onClick=\"javascript:guardarParametre('PPREGPM');\" value='guardar'>
					</td>

					<td align='left' valign='top'>
					<p id='p_trgp' style='color:".$colorValorGP.";'>".(number_format($sumaEcosRgrupsP,2,'.',''))."</p>
					</td>

					<td align='left' valign='top'>
					<p  style='color:".$colorValorGP.";' >".(number_format(($mParametres['PPREGPM']['valor']-$sumaEcosRgrupsP),2,'.',''))."</p>
					</td>
				</tr>

				<tr>
					<td align='left' valign='top'>
					<p>Grups No Productors:</p>					
					</td>

					<td align='left' valign='top'>
					<input type='text' style='color:".$colorValorG.";' size='10' id='i_PPREGM' value='".(number_format($mParametres['PPREGM']['valor'],2,'.',''))."'>
					<input type='hidden' id='i_PPREGM2' value='".(number_format($mParametres['PPREGM']['valor'],2,'.',''))."'>
					<input class='i_micro' type='button'  ".$disabledNotSadmin."  ".$accionsButton." style='visibility:".$visibilityNotSadmin.";' onClick=\"javascript:guardarParametre('PPREGM');\" value='guardar'>
					</td>

					<td align='left' valign='top'>
					<p id='p_trg' style='color:".$colorValorG.";'>".(number_format($sumaEcosRgrupsNp,2,'.',''))."</p>
					</td>

					<td align='left' valign='top'>
					<p  style='color:".$colorValorG.";' >".(number_format(($mParametres['PPREGM']['valor']-$sumaEcosRgrupsNp),2,'.',''))."</p>
					</td>
				</tr>

				<tr>
					<td align='left' valign='top'>
					<p>total</p>					
					</td>

					<td align='left' valign='top'>
					<p>".(number_format($PPRETM,2,'.',''))."</p>
					</td>

					<td align='left' valign='top'>
					<p>".(number_format(($sumaEcosRusuarisComissions+$sumaEcosRgrupsP+$sumaEcosRgrupsNp+$sumaEcosRgrupsCAL),2,'.',''))."</p>
					</td>

					<td align='left' valign='top'>
					<p style='color:blue;'><b>".(number_format(($PPRETM-($sumaEcosRusuarisComissions+$sumaEcosRgrupsP+$sumaEcosRgrupsNp+$sumaEcosRgrupsCAL)),2,'.',''))."</b></p>
					</td>
				</tr>
			</table>
			</div>
			
			<table style='width:60%;' align='center'>
				<tr>
					<td  style='width:80%;'>
			<p >
			<b>Recolzament a membres de comissi�</b>:
			<br>
			<br>- Els ecos recolzats de cada membre d'una comissi� es calculen a partir de la seva assignaci� en ecos.
			<br>- Un usuari pot ser membre de m�s d'una comissi�, i es comptar� la suma dels ecos procedents de cada assignaci�

			";
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
			{

				echo "
			<br>- Per actualitzar les dades de les assignacions en ecos de cada membre cal importar des d'aqui un fitxer .csv 
			amb dues columnes. En la primera hi ha d'haver el seu nombre coop de soci de la CIC, i en la segona la seva assignaci� en ecos.
			<br> Per cada comissi� de la que rebi ecos hi ha d'haver una linia al .csv amb el nombre coop de soci i el valor de l'assignaci� en ecos
			<br>- Quan s'hagi importat la llista d'assignacions, nom�s es tindran en compte els valors corresponents a usuaris 
			del gestor que hagin especificat al seu perfil d'usuari a quines comissions pertanyen i quin es el seu nombre coop de soci.
			 Si no es aix� l'aplicaci� no els reconeixer� i no els podr� assignar ecos recolzats.
			 <br>- tamb� s'esborrar�n les comissions dels perfils d'usuaris que ja no estiguin en comissions a la llista de dades importada
			 <br>- els nous membres de comissions hauran de seleccionar les seves comissions i introduir el nombre coop de soci per rebre el recolzament d'ecos que els correspon.
			<br>
				";
			}
			
			echo "
			<br>
			<br>
			<b>Recolzament a grups productors</b>:
			<br>
			<br>
			A les JJAA de mas Pujou (2015) l'assemblea permanent va decidir destinar el 100% del pressupost de recolzament a les productores.
			<br>
			F�rmula de distribuci�:
			<br><br>			
			<i><b>
			Recolzament= ( totalEcosAcceptatsProductora x vendes6mesos x compres6mesos*C ) / D
			</b></i>
			<br><br>			
			on:
			<br>			
			<u>totalEcosAcceptatsProductora</u>: el total d'ecos acceptats per la productora els darrers 6 mesos.
			<br>
			<u>vendes6mesos</u>: import total de vendes a la cac dels darrers 6 mesos.
			<br>
			<u>compres6mesos</u>: import total de compres a la cac dels darrers 6 mesos. Aquest factor rep un 	pes major per tal de donar mes recolzament als productors que compren. S'aplica com: 	totalCompres x (arrel quadrada de 'totalCompres').
			<br>
			<u>D</u>: arrel c�bica de vendes-compres (difer�ncia entre els imports de compres i ventes dels darrers 6 mesos). Fem l'arrel c�bica per reduir el pes d'aquest factor, que en estat pur es molt elevat.
			<br>
			<u>C</u>: arrel c�bica de 'vendes6mesos', per fer pesar m�s les vendes que les compres.
			<br>
			<br>
			</p>
			<table style='background-color:LightBlue;'>
				<tr>
					<td>
					<p>
					La distribuci� actual, revisable i millorable, premia els productores que:
					<br>
					- tenen un balan� d'intercanvi menor, essent el balan� �ptim de zero ums
					<br>
					- compren m�s a la cac
					<br>
					- accepten mes ms pels seus productes
					<br>
					- venen mes a la cac
					</p>
					</td>
				</tr>
			</table>
			<br><br>			
			<p >
			<b>Recolzament a rebosts-CAL,  altres grups</b>:
			<br>
			<br>- els ecos recolzats es guarden manualment per cada grup, en base als acords establerts amb la CAC i al pressupost disponible. No es poden guardar valors que facin excedir al conjunt respecte el pressupost assignat
			";
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
			{
				echo "			
			<br> Perqu� tinguin efecte els canvis guardats de la configuraci� del recolzament i la llista d'assignacions que s'hagi importat,
			cal executar novament la distribuci� del recolzament.
				";
			}
			echo "
			<br>
			<br>* Els membres de comissions no rebran ecos recolzats dels grups dels que siguin membres i disposin d'un recolzament assignat, a menys que el seu saldo en ecos recolzats com a membre de comissi� sigui zero. Aquest recolzament
			es repartir� entre els membres del grup que no son membres d'una comissi�, o que son membres de comissi� amb recolzament zero..
			<br>** Els membres de comissions amb saldo positiu d'ecos recolzats poden fer comanda desde qualsevol grup del que siguin membres, i es descomptaran els ecos recolzats que utilitzin del total disponible del seu saldo, no del saldo del grup.
			</p>
			<br><br><br><br>&nbsp;
					</td>
				</tr>
			</table>
									
";
$parsChain=makeParsChain($mPars);
html_helpRecipient();
echo "
<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='calculsRecolzamentSelectiu.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='i_distribucioComissions' name='i_distribucioComissions' value=''>
<input type='hidden' id='i_saldoEcosR' name='i_saldoEcosR' value=''>
<input type='hidden' id='i_grupRid' name='i_grupRid' value=''>
<input type='hidden' id='i_parametreNom' name='i_parametreNom' value=''>
<input type='hidden' id='i_parametreValor' name='i_parametreValor' value=''>
<input type='hidden' id='i_aplicarDistribucioGrupsProductors' name='i_aplicarDistribucioGrupsProductors' value='0'>
<input type='hidden' id='i_distribucioGrupsProductors' name='i_distribucioGrupsProductors' value=''>
</form>
<input type='hidden' id='i_nr' value='".$nr."'>
<input type='hidden' id='i_ng' value='".$ng."'>
<input type='hidden' id='i_ngp' value='".$ngp."'>
<input type='hidden' id='i_sumaFactorRp' value='".$sumaFactorRP."'>

</div>
	";



?>

		