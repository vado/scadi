<?php


//------------------------------------------------------------------------------
function html_mostrarAlbaraGrupDesglossat($mProductes,$db)
{
	global 	$mColors,
			$missatgeAlerta,
			$parsChain,
			$puntEntregaId,
			$quantitatTotalCac,
			$quantitatTotalRebosts,
			$ruta,
			$mAbonamentsCarrecs,
			$mComanda,
			$mContinguts,
			$mCr,
			$mIncidencies,
			$mFormesPagament,
			$mPars,
			$mParsCAC,
			$mParametres,
			$mProductes2,
			$mPeriodesLocalsInfo,
			$mPropietatsPeriodeLocal,
			$mPuntsEntrega,
			$mRebostsRef,
			$mRebost,
			$mRutes,
			$mRutesSufixes,
			$mUsuarisRef,
			$mUsuarisGrupRef,
			$mUsuari,
			$mSelUsuari,
			$mTipusIncidencies,
			$mAnticsUsuarisGrupAmbComanda,
			$mostrarIncidencies,
			$llistaText;

	$mostrarAbonamentsCarrecsIreserves=true;
	
	if($mParametres['moneda3Activa']['valor']==1)
	{
		$moneda3ElementsStyle=" style='visibility:inherit; z-index:0; position:relative;' ";
	}
	else
	{
		$moneda3ElementsStyle=" style='visibility:hidden; z-index:1;  position:absolute;' ";
	}
	
	if($mPropietatsPeriodeLocal['comandesLocalsTancades']==1)
	{
		$estatPeriodeText="TANCAT";
		$mostrarIncidencies=false;
		$mostrarAbonamentsCarrecsIreserves=false;
	}
	else if($mPropietatsPeriodeLocal['comandesLocalsTancades']==0)
	{
		$estatPeriodeText="Reserves tancades";
	}
	if($mPropietatsPeriodeLocal['comandesLocalsTancades']==-1)
	{
		$estatPeriodeText="OBERT";
	}

	$class=" class='p.albara' ";

	echo "
	<table border='0' width='100%' style='z-index:10;' bgcolor='".$mColors['table']."'>
		<tr>
			<td align='left' valign='top' width='30%'>
			<p>
			<font color='red'>- ALBAR� ".$llistaText." (<b>GRUP</b> , desglossat) -</font>
			<br>Periode: <b>".@$mPeriodesLocalsInfo[$mPars['sel_periode_comanda_local']]['periode_comanda']."(".$mPars['sel_periode_comanda_local'].")(".$estatPeriodeText.")</b>
			<br>CIC - Central d'Abastiment Catal� (CAC)<br>
			(".(date('d-m-Y h:i:s')).")
			</td>

			<td align='left' valign='top' width='35%'>
			<table>
				<tr>
					<td align='left' valign='top'>
					<p>GRUP:</p> 
					</td>
					<td>
					<p><b>".(urldecode($mRebost['nom']))."</b></p>
					</td>
				</tr>
				<tr>
					<td align='left'>
					<p class='nota'>Responsable:</p> 
					</td>
					<td>
					<p  class='nota'>".(urldecode($mUsuarisGrupRef[$mRebost['usuari_id']]['usuari']['usuari']))."</p>
					</td>
				</tr>
				<tr>
					<td align='left'>
					<p  class='nota'>Mobil:</p> 
					</td>
					<td>
					<p class='nota'>".$mRebost['mobil']."</p>
					</td>
				</tr>
				<tr>
					<td align='left'>
					<p class='nota'>Responsable:</p> 
					</td>
					<td>
					<p  class='nota'>".$mRebost['email_rebost']."</p>
					</td>
				</tr>
			</table>
			</td>
				
			<td valign='top' align='left' width='35%'>
			<table>
				<tr>
					<td valign='top' align='left'>
					<p>Punt&nbsp;Entrega:</p>
					</td>

					<td align='left' valign='top'>
					<p>
					".(urldecode($mPropietatsPeriodeLocal['apeLocal']))."
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>

	<table width='100%' align='right' bgcolor='".$mColors['table']."'>
		<tr>
			<td id='td_missatgeAlerta' style=' width:90%;' align='center'>
			".$missatgeAlerta."
			</td>
	
			<td align='right' style='width:10%;'>
			";
			$dViStyle=" style='visibility:inherit;' ";
			if($mPars['albaraVistaImpressio']==1)
			{
				$dViStyle=" style='visibility:hidden;' ";
			}
			echo "
			<div ".$dViStyle.">
			<input type='checkbox' id='cb_vistaImpressio2' value='0' onClick=\"javascript:vistaImpressio2();\"> vista impressi�</p>
			</div>
			</td>
		</tr>
	</table>

	";
	if($mPars['selUsuariId']=='0' && $mostrarIncidencies)
	{
		echo "
	<table align='center' border='0' id='t_registreIncidencies'>
		<tr>
			<td align='center' width='100%'>
			<p>[ Registre d'incid�ncies, GRUP<==>usuaries]</p>
			<table bgcolor='#cccccc' align='center'>
				<tr>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>id</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>data</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>ruta</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>autor</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>usuari</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>grup</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>tipus<br>incid�ncia</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>ID<br>producte</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>producte</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>demanat</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>rebut</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>indicacions</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>estat</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					</th>
				</tr>
		";
		$numBotonsEditar=0;
		while(list($key,$mIncidencia)=each($mIncidencies))
		{
			echo "
				<tr>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mIncidencia['id']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mIncidencia['data']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mPeriodesLocalsInfo[$mIncidencia['ruta']]['periode_comanda']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".(@urldecode($mUsuarisRef[$mIncidencia['usuari_id']]['usuari']))."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">"; if($mIncidencia['sel_usuari_id']==0){echo 'GRUP';}else{echo @urldecode($mUsuarisGrupRef[$mIncidencia['sel_usuari_id']]['usuari']['usuari']);} echo "</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".(@urldecode($mRebostsRef[$mIncidencia['grup_id']]['nom']))."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mIncidencia['tipus']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mIncidencia['producte_id']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".(@urldecode($mProductes2[$mIncidencia['producte_id']]['producte']))."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">";if($mIncidencia['tipus']=='producte' || $mIncidencia['tipus']=='jaEntregat' || $mIncidencia['tipus']=='pendentEntrega'){echo $mIncidencia['demanat'];} echo "</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">";if($mIncidencia['tipus']=='producte' || $mIncidencia['tipus']=='jaEntregat' || $mIncidencia['tipus']=='pendentEntrega'){echo $mIncidencia['rebut'];} echo "</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p id='p_comentaris_".$numBotonsEditar."'  ".$class.">".(urldecode($mIncidencia['comentaris']))."</p>
					<form id='f_guardarIncidencia_".$numBotonsEditar."' name='f_guardarIncidencia_".$numBotonsEditar."' method='post'  target='_self' action=''>
					<textArea style='visibility:hidden; position:absolute;' id='ta_comentaris_".$numBotonsEditar."' name='ta_comentaris_".$numBotonsEditar."' cols='40' rows='5'></textArea>
					<input type='hidden' id='i_pars_".$numBotonsEditar."' name='i_pars' value=''>
					</form>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class. " style='"; if($mIncidencia['estat']=='pendent'){echo "color:#ff0000;";} echo "'>".$mIncidencia['estat']."</p>
					</td>
				
					<td bgcolor='#ffffff' align='left' valign='middle'>
			";
				
			if($mIncidencia['estat']!='resolta')
			{
				if
				(
					$mIncidencia['estat']=='pendent'
					&& 
					(
						$mPars['usuari']==$mIncidencia['usuari'] 
						|| 
						$mPars['nivell']=='sadmin'
					)
				)
				{
					echo "
					<input type='button'  class='i_botoEditar' id='i_botoEditarE_".$numBotonsEditar."'  onClick=\"javascript:  editarIncidencia('".$numBotonsEditar."','".$mIncidencia['id']."');\" value='editar'>
					<br>
					<input type='button'  class='i_botoEditar' id='i_botoEditarR_".$numBotonsEditar."' onClick=\"javascript: eliminarIncidencia('".$mIncidencia['id']."');\" value='resoldre'>
					<br>
					<input type='button' style='visibility:hidden;' class='i_botoEditar' id='i_botoEditarG_".$numBotonsEditar."'  onClick=\"javascript:  guardarIncidencia('".$numBotonsEditar."','".$mIncidencia['id']."');\" value='guardar'>
					";
					$numBotonsEditar++;
				}
			}
			echo "
					</td>
				</tr>			
			";
		}
		reset($mIncidencies);
			
		echo "
			</table>
			</form>
			<script>numBotonsEditar=".$numBotonsEditar.";</script>
			</td>
		</tr>
	</table>

	<table align='center' border='0' id='t_seleccionarTipusIncidencia'>
		<tr>
			<td align='center' width='50%'>
			<p>[ Seleccionar Tipus Incidencia ]</p>
			<table bgcolor='#cccccc' align='center'>
				<tr>
					<td bgcolor='#ffffff'>
					<p>
					<select  onChange=\"javascript:seleccionarTipusIncidencia();\" id='sel_tipusIncidencia' name='sel_tipusIncidencia'>
		";
		while(list($key,$val)=each($mTipusIncidencies))
		{
			echo "
					<option value='".$val."'>".$val."</option>
			";
		}
		reset($mTipusIncidencies);
		echo "
					<option selected value=''></option>
					</select>
					</p>
					</td>
				</tr>
			</table>
			</td>
		";
		if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['usuari_id']==$mRebost['usuari_id'])
		{
			echo "
			<td align='left' width='50%'>
			<p class='compacte' style='cursor:pointer;' onClick=\"javascript:seleccionarAbonament();\" id='p_selAbonament' name='p_selAbonament'>&nbsp;&nbsp;&nbsp;<u>Aplicar Abonament</u>&nbsp;(Grup a usuaria)"; if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){echo "<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";} echo "</p>
			<p class='compacte' style='cursor:pointer;' onClick=\"javascript:seleccionarCarrec();\" id='p_selCarrec' name='p_selCarrec'>&nbsp;&nbsp;&nbsp;<u>Aplicar C�rrec</u>&nbsp;(Grup a usuaria)"; if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){echo "<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";} echo "</p>
			<p class='compacte' style='cursor:pointer;' onClick=\"javascript:seleccionarAnularReserva();\" id='p_selAnularReserva' name='p_selAnularReserva'>&nbsp;&nbsp;&nbsp;<u>Anul.lar Reserva</u>&nbsp;(Grup a usuaria)"; if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){echo "<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";} echo "</p>
			</td>
			";
		}
		echo "
		</tr>
	</table>

	<div width='100%'>
		<table align='center' border='1' id='t_afegirIncidencia' width='50%' style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Afegir Incid�ncia ]</p>
				<form id='f_afegirIncidencia' name='f_afegirIncidencia' action='' method='post'>
				<input type='hidden' name='i_form' value='afegirIncidencia'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' id='i_aIdata' name='i_aIdata' value='".(date('d/m/Y  H:i:s'))."'>

				<table align='center'  width='100%'>
					<tr>
						<td  width='100%'>
						<table  width='100%'>
							<tr>
								<td  width='50%'>
								<table  width='100%'>
									<tr>
										<td>
										<p>periode local:</p>
										</td>
										<td>
										<select id='sel_ruta' READONLY name='sel_ruta' style='background-color:#dddddd;'>						
										<option selected value='".$mPars['sel_periode_comanda_local']."'>".@$mPeriodesLocalsInfo[$mPars['sel_periode_comanda_local']]['periode_comanda']." (".$mPars['sel_periode_comanda_local'].")</option>	
										</select>
										</td>
									</tr>
									<tr>
										<td>
										<p>tipus:</p>
										</td>
										<td>
										<input type='text' readonly id='i_tipusIncidencia' name='i_tipusIncidencia' value='producte' style='background-color:#dddddd;'>						
										</td>
									</tr>
									<tr id='tr_producteId' style='z-Index:0; top:0px; position:relative;'>
										<td>
										<p>id:</p>
										</td>
										<td>
										<input type='text' size='5'  id='i_producteId' name='i_producteId' value=''>
										</td>
									</tr>
									<tr id='tr_producteNom'>
										<td>
										<p>producte:</p>
										</td>
										<td>
										<p id='p_producteNom'></p>
										</td>
									</tr>
									<tr id='tr_producteDemanat' style='z-Index:0; top:0px; position:relative;'>
										<td>
										<p>demanat:</p>
										</td>
										<td>
										<input type='text'  size='5' READONLY style='background-color:#dddddd;' id='i_producteDemanat' name='i_producteDemanat' value=''>
										</td>
									</tr>
									<tr id='tr_producteRebut' style='z-Index:0; top:0px; position:relative;'>
										<td>
										<p>rebut:</p>
										</td>
										<td>
										<input type='text'  size='5' id='i_producteRebut' name='i_producteRebut' value=''>
										</td>
									</tr>
								</table>
								</td>
								<td align='center' valign='top'  width='50%'>
								<table width='100%'>
									<tr>
										<td  width='100%' valign='top' align='right'>
										<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
										</td>
									</tr>
									<tr>
										<td  width='100%' align='center' valign='middle'>
										<p class='nota' style='text-align:justify;'>* Clica el bot� amb l'identificador del producte que vols guardar a la incid�ncia, per carregar les dades del producte<br>
										<br>* Tamb� pots canviar el tipus d'incidencia des del selector de mes amunt
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						<table  width='100%'>
							<tr>
								<td valign='top'  width='15%'>
								<p>Comentari:</p>
								</td>
								<td  width='85%'>
								<textArea id='ta_comentaris' name='ta_comentaris' cols='70' rows='5'></textArea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAfegirIncidencia()){document.getElementById('f_afegirIncidencia').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='guardar'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>
		
		";
		if($mostrarAbonamentsCarrecsIreserves && $mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord' || $mPars['usuari_id']=$mRebost['usuari_id'])
		{
			echo "
		<table align='center'  border='1' width='50%' id='t_aplicarAbonament'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Aplicar Abonament (Grup a usuaria)]</p>
				<form id='f_aplicarAbonament' name='f_aplicarAbonament' action='' method='post'>
				<input type='hidden' name='i_form' value='abonamentGrup'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' id='i_aAdata' name='i_aAdata' value='".(date('d/m/Y  H:i:s'))."'>

				<table align='center'>
					<tr>
						<td>
						<table>
							<tr>
								<td>
								</td>
								<td align='right'>
								<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
								</td>
							</tr>
							<tr>
								<td>
								<p>usuari:</p>
								</td>
								<td>
								<select id='sel_usuariIncidenciaAa' name='sel_usuariIncidenciaAa'>
								";
								while(list($usuariId_,$mUsuari_)=each($mUsuarisGrupRef))
								{
									if(count($mUsuarisGrupRef[$usuariId_]['comanda'])>0){$color='green';}else{$color='black';}
									if($mUsuari_['usuari']['estat']!='actiu'){$disabledInactius='disabled';}else{$disabledInactius='';}
									echo "
									<option ".$disabledInactius." style='color:".$color.";' value='".$usuariId_."'>".(urldecode($mUsuari_['usuari']['usuari']))." - ".($mUsuari_['usuari']['email'])."</option>
									";
								}
								reset($mUsuarisGrupRef);
								echo "
									<option selected value=''></option>
								</select>
								</td>
							</tr>
							<tr>
								<td>
								<p>tipus:</p>
								</td>
								<td>
								<input type='text' readonly id='i_tipusIncidenciaAa' name='i_tipusIncidencia' value='abonamentGrup'>						
								</td>
							</tr>
							<tr>
								<td>
								<p>ecos:</p>
								</td>
								<td>
								<input type='text' id='i_ecosAa' name='i_ecosAa' value=''>						
								</td>
							</tr>
							<tr ".$moneda3ElementsStyle.">
								<td>
								<p>".$mParametres['moneda3Nom']['valor'].":</p>
								</td>
								<td>
								<input type='text' id='i_ebAa' name='i_ebAa' value=''>						
								</td>
							</tr>
							<tr>
								<td>
								<p>euros:</p>
								</td>
								<td>
								<input type='text' id='i_eurosAa' name='i_eurosAa' value=''>						
								</td>
							</tr>
							<tr>
								<td valign='top'>
								<p>Comentari:</p>
								</td>
								<td>
								<textArea id='ta_comentarisAa' name='ta_comentarisAa' cols='60' rows='5'></textArea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAplicarAbonament()){document.getElementById('f_aplicarAbonament').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='guardar'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>

		<table align='center'  border='1' width='50%' id='t_aplicarCarrec'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Aplicar C�rrec (Grup a usuaria)]</p>
				<form id='f_aplicarCarrec' name='f_aplicarCarrec' action='' method='post'>
				<input type='hidden' name='i_form' value='carrecGrup'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' id='i_aCdata' name='i_aCdata' value='".(date('d/m/Y  H:i:s'))."'>

				<table align='center'>
					<tr>
						<td>
						<table>
							<tr>
								<td>
								</td>
								<td align='right'>
								<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
								</td>
							</tr>
";
//*v36-15-12-15 afegir select a form
echo " 							
							<tr>
								<td>
								<p>usuari:</p>
								</td>
								<td>
								<select id='sel_usuariIncidenciaAc' name='sel_usuariIncidenciaAc'>
								";
								while(list($usuariId_,$mUsuari_)=each($mUsuarisGrupRef))
								{
									if(count($mUsuarisGrupRef[$usuariId_]['comanda'])>0){$color='green';}else{$color='black';}
									if($mUsuari_['usuari']['estat']!='actiu'){$disabledInactius='disabled';}else{$disabledInactius='';}
									echo "
									<option ".$disabledInactius." style='color:".$color.";' value='".$usuariId_."'>".(urldecode($mUsuari_['usuari']['usuari']))." - ".($mUsuari_['usuari']['email'])."</option>
									";
								}
								reset($mUsuarisGrupRef);
								echo "
									<option selected value=''></option>
								</select>
								</td>
							</tr>
							<tr>
								<td>
								<p>tipus:</p>
								</td>
								<td>
								<input type='text' readonly id='i_tipusIncidenciaAc' name='i_tipusIncidencia' value='carrecGrup'>						
								</td>
							</tr>
							<tr>
								<td>
								<p>ecos:</p>
								</td>
								<td>
								<input type='text' id='i_ecosAc' name='i_ecosAc' value=''>						
								</td>
							</tr>
							<tr ".$moneda3ElementsStyle.">
								<td>
								<p>".$mParametres['moneda3Nom']['valor'].":</p>
								</td>
								<td>
								<input type='text' id='i_ebAc' name='i_ebAc' value=''>						
								</td>
							</tr>
							<tr>
								<td>
								<p>euros:</p>
								</td>
								<td>
								<input type='text' id='i_eurosAc' name='i_eurosAc' value=''>						
								</td>
							</tr>
							<tr>
								<td valign='top'>
								<p>Comentari:</p>
								</td>
								<td>
								<textArea id='ta_comentarisAc' name='ta_comentarisAc' cols='60' rows='5'></textArea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAplicarCarrec()){document.getElementById('f_aplicarCarrec').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='guardar'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>

		<table align='center'  border='1' width='50%' id='t_anularReserva'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Anul.lar Reserva (a usuari)]</p>
				<form id='f_anularReserva' name='f_anularReserva' action='' method='post'>
				<input type='hidden' name='i_form' value='anularReserva'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' name='sel_ruta' value='".$mPars['sel_periode_comanda_local']."'>
				<input type='hidden' id='i_aRdata' name='i_aRdata' value='".(date('d/m/Y  H:i:s'))."'>

				<table align='center'>
					<tr>
						<td>
						<table>
							<tr>
								<td>
								</td>
								<td align='right'>
								<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
								</td>
							</tr>
							<tr>
								<td>
								<p>tipus:</p>
								</td>
								<td>
								<input type='text' readonly id='i_tipusIncidenciaAc' name='i_tipusIncidencia' value='anularReserva'>						
								</td>
							</tr>
							<tr>
								<td>
								<p>id producte:</p>
								</td>
								<td>
								<input type='text' id='i_anularId' name='i_anularId' value=''>						
								</td>
							</tr>
							<tr>
								<td>
								<p>unitats a anul.lar:</p>
								</td>
								<td>
								<input type='text' id='i_anularUts' name='i_anularUts' value=''>						
								</td>
							</tr>
							<tr>
								<td>
								<p>usuari:</p>
								</td>
								<td>
								<select id='i_anularUs' name='i_anularUs'>
			";
		
			while(list($key,$mUsuari_)=each($mUsuarisGrupRef))
			{
				if($mUsuari_['usuari']['estat']=='actiu')
				{
					echo "
								<option   value='".$mUsuari_['usuari']['id']."'>".(urldecode($mUsuari_['usuari']['usuari']))." (".$mUsuari_['usuari']['email'].")</option>
					";
				}
			}
			reset($mUsuarisGrupRef);
			echo "
								<option selected value=''></option>
								</select>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAnularReserva()){document.getElementById('f_anularReserva').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='anul.lar reserva'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>
		</td>
			";
		}
		else
		{
			echo "
		<div id='t_aplicarAbonament'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
		<div id='t_aplicarCarrec'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
		<div id='t_anularReserva'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
			";
		}

		echo "
	</div>
		";
	}
	else
	{
		echo "
		<div id='t_seleccionarTipusIncidencia'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
		";
	}
	echo "
	<center><p style='font-size:16px;'>[ ALBAR� ]</p></center>
	";
	$numBotonsEditar2=0;
	echo "
	<table width='100%'>
		<tr>
			<td align='left' width='50%'>
			<p>[ordre:<b>".$mPars['sortBy']."-".$mPars['ascdesc']."</b>]</p>
			</td>
			<td align='right' width='50%'>
			<p>[Data �ltima revisi� comanda:<b>".(formatarData(getDataComanda($db)))."]</b></p>
			</td>
		</tr>
	</table>
	
	<table bgcolor='#dddddd' width='100%' bgcolor='".$mColors['table']."'>
		<tr>
			<th bgcolor='#ffffff' style='width:40px;'><p class='albara'><i><b>-&nbsp;id&nbsp;-</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>producte</p></th>
			<th bgcolor='#ffffff'><p class='albara'>productor</p></th>
			<th bgcolor='#ffffff'><p class='albara'>tipus</p></th>
			<th bgcolor='#ffffff'><p class='albara'>categoria10</p></th>
			<th bgcolor='#ffffff'><p class='albara'>format</p></th>
			<th bgcolor='#ffffff'><p class='albara'>unitat_facturacio</p></th>
			<th bgcolor='#ffffff'><p class='albara'>pes</p></th>
			<th bgcolor='#ffffff'><p class='albara'>preu</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ms</p></th>
			<th bgcolor='#ffffff'><p class='albara2'><i><b>- id -</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'  style='width:40px;'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ums</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ecos</p></th>
			<th bgcolor='#ffffff'><p class='albara'>euros</p></th>
			<th bgcolor='#ffffff'><p class='albara'></p></th>
		</tr>
	";
	$cont=0;
	$quantitatT=0;
	$umst=0;
	$ecost=0;
	$eurost=0;
	$ctkTums=0;
	$ctkTecos=0;
	$ctkTeuros=0;
	$ctrTums=0;
	$ctrTecos=0;
	$ctrTeuros=0;
		
	$mResumUsuaris=array();
	while(list($key,$val)=each($mProductes))
	{
		//$mRevisioRebosts=getDatesReservaProducte($db,$mProductes[$key]['id']);
		//$dataUltimaRevisioF=formatarData($mRevisioRebosts[$mPars['rebost']][$mProductes[$key]['id']]['num_comanda']);
		
		if(substr_count($mProductes[$key]['tipus'], 'especial')==0)
		{
			while(list($usuariId,$quantitat)=@each($mComanda[$mProductes[$key]['id']]))
			{
				$ums_=$mProductes[$key]['preu']*$quantitat;
				$ecos_=$ums_*$mProductes[$key]['ms']/100;
				$euros_=$ums_-$ecos_;
				echo "
		<tr>
			<td bgcolor='#ffffff'>
			";
			if($mPars['selUsuariId']=='0' && $mPars['grup_id']!='0' && $mostrarIncidencies)
			{
				echo "
			<input type='button' value='".$mProductes[$key]['id']."' id='i_botoEditar2_".$numBotonsEditar2."'  onClick=\"javascript: afegirIncidencia('".$mProductes[$key]['id']."','".$usuariId."','GD');\">
				";
			}
			else
			{
				echo "
			<input type='text' READONLY value='".$mProductes[$key]['id']."' id='i_botoEditar2_".$numBotonsEditar2."'  >
				";
			}
			echo "
			</td>
			<td  bgcolor='".$mColors['table']."'><p id='p_nomProducte_".$mProductes[$key]['id']."' class='albara'>".(urldecode($mProductes[$key]['producte']))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(urldecode(substr($mProductes[$key]['productor'],strpos($mProductes[$key]['productor'],'-')+1)))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(str_replace(',jjaa,',',<b>jjaa</b>,',$mProductes[$key]['tipus']))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['categoria10']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['format']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(urldecode($mProductes[$key]['unitat_facturacio']))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['pes']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['preu']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['ms']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara2'><i><b>".$mProductes[$key]['id']."</i></b></p></td>
			<td  bgcolor='".$mColors['table']."'><p id='p_demanat_".$mProductes[$key]['id']."_".$usuariId."' class='albara'>".(number_format($quantitat,0))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>&nbsp;</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(number_format($ums_,2))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(number_format($ecos_,2))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(number_format($euros_,2))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>";
			if(array_key_exists($usuariId,$mUsuarisGrupRef))
			{
				echo urldecode($mUsuarisGrupRef[$usuariId]['usuari']['usuari']);
			}
			else
			{
				echo "(".(urldecode($mAnticsUsuarisGrupAmbComanda[$usuariId]['usuari']['usuari'])).')';
			}
			echo "			
			</p></td>
		</tr>
				";
				if(!array_key_exists($usuariId,$mResumUsuaris))
				{
					$mResumUsuaris[$usuariId]=array();
					$mResumUsuaris[$usuariId]['umst_']=0;
					$mResumUsuaris[$usuariId]['ecost_']=0;
					$mResumUsuaris[$usuariId]['eurost_']=0;
					$mResumUsuaris[$usuariId]['quantitatT']=0;
				}
				$mResumUsuaris[$usuariId]['umst_']+=$ums_;
				$mResumUsuaris[$usuariId]['ecost_']+=$ecos_;
				$mResumUsuaris[$usuariId]['eurost_']+=$euros_;
				$mResumUsuaris[$usuariId]['quantitatT']+=$quantitat*$mProductes[$key]['pes'];
				
				$umst+=$ums_;
				$ecost+=$ecos_;
				$eurost+=$euros_;
				$quantitatT+=$quantitat*$mProductes[$key]['pes'];
				$ctkTums+=$mProductes[$key]['pes']*$quantitat*$mPropietatsPeriodeLocal['ctikLocal'];
				$ctkTecos+=$mProductes[$key]['pes']*$quantitat*$mPropietatsPeriodeLocal['ctikLocal']*$mPropietatsPeriodeLocal['ms_ctikLocal']/100;
				$ctkTeuros+=$mProductes[$key]['pes']*$quantitat*$mPropietatsPeriodeLocal['ctikLocal']*((100-$mPropietatsPeriodeLocal['ms_ctikLocal'])/100);
				//$ctrTums+=  $mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']+$mParametres['cost_transport_intern_repartit']['valor'])/($quantitatTotalCac+$quantitatTotalRebosts));
				//$ctrTecos+= $mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100+$mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)/($quantitatTotalCac+$quantitatTotalRebosts));
				//$ctrTeuros+=$mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']*(100-$mParametres['ms_ctear']['valor'])/100+$mParametres['cost_transport_intern_repartit']['valor']*(100-$mParametres['ms_ctiar']['valor'])/100)/($quantitatTotalCac+$quantitatTotalRebosts));
					
				$cont++;
				if($cont==15)
				{
					echo "
		<tr>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>id</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>producte</p></th>
			<th bgcolor='#ffffff'><p class='albara'>productor</p></th>
			<th bgcolor='#ffffff'><p class='albara'>tipus</p></th>
			<th bgcolor='#ffffff'><p class='albara'>categoria10</p></th>
			<th bgcolor='#ffffff'><p class='albara'>format</p></th>
			<th bgcolor='#ffffff'><p class='albara'>unitat_facturacio</p></th>
			<th bgcolor='#ffffff'><p class='albara'>pes</p></th>
			<th bgcolor='#ffffff'><p class='albara'>preu</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ms</p></th>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>- id -</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ums</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ecos</p></th>
			<th bgcolor='#ffffff'><p class='albara'>euros</p></th>
			<th bgcolor='#ffffff'><p class='albara'>usuari</p></th>
		</tr>
					";
					$cont=0;
				}
				$numBotonsEditar2++;
			}
		}
		@reset($mComanda[$mProductes[$key]['id']]);
	}
	reset($mProductes);


	
	
	
	
		//fons despeses local:
		
		$umstFDC=$umst*$mPropietatsPeriodeLocal['fdLocal']/100;
		$ecostFDC=$umst*($mPropietatsPeriodeLocal['fdLocal']/100)*($mPropietatsPeriodeLocal['ms_fdLocal']/100);
		$eurostFDC=$umstFDC-$ecostFDC;

	
		echo "
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>Total<br>productes</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($umst,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ecost,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($eurost,2))."</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
		</tr>
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>Total Fons Despeses Local<br>(".$mPropietatsPeriodeLocal['fdLocal']."% sobre preus, ".$mPropietatsPeriodeLocal['ms_fdLocal']."% MS)</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($umstFDC,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ecostFDC,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($eurostFDC,2))."</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
		</tr>
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>total<br>transport</p></td>
			<td  bgcolor='#ffffff'><p>(".(number_format($quantitatT,2))." kg)</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ctkTums,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ctkTecos,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ctkTeuros,2))."</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
		</tr>
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>Total Comanda</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<th  bgcolor='#ffffff'><p>".(number_format(($umst+$ctkTums+$umstFDC),2))."</p></th>
			<th  bgcolor='#ffffff'><p>".(number_format(($ecost+$ctkTecos+$ecostFDC),2))."</p></th>
			<th  bgcolor='#ffffff'><p>".(number_format(($eurost+$ctkTeuros+$eurostFDC),2))."</p></th>
			<th  bgcolor='#ffffff'><p></p></th>
		</tr>
	</table>
		";
		echo "<script>numBotonsEditar2=".$numBotonsEditar2.";</script>
	<p class='nota'>* No s'inclouen les comandes de productes especials</p>
		";
		$umsTotalFp=$mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu'];
		$umsTotalComanda=$umst+$ctkTums+$umstFDC;
		$ecosTotalComanda=$ecost+$ctkTecos+$ecostFDC;
		$eurosTotalComanda=$eurost+$ctkTeuros+$eurostFDC;
		//Total comanda :
		echo "
	<center></center>
	<table width='80%' bgcolor='#eeeeee' align='center'>	
		<tr>
			<td  bgcolor='#ffffff' align='right' style='width:30%;'><p></p></td>
			<th  bgcolor='#ffffff' align='right' style='width:10%;'><p>ecos</p></th>
		";
		if($mParametres['moneda3Activa']['valor']==1)
		{
			echo "
			<th  bgcolor='#ffffff' align='right' ".$moneda3ElementsStyle."><p>".$mParametres['moneda3Abrev']['valor']."</p></th>
			";
		}
		echo "
			<th  bgcolor='#ffffff' align='right' style='width:10%;'><p>euros</p></th>
			<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
			<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
		</tr>
		";

		$fpEcos=$ecosTotalComanda;
		$fpEu=$eurosTotalComanda;

		echo "
		<tr>
			<th  bgcolor='#ffffff' align='left' style='width:30%;'><p>Total Comanda</th>
			<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(@number_format($fpEcos,2))."</p></td>
			<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(@number_format($fpEu,2))."</p></td>
			<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
			<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
		</tr>
		";
	
		//Total comanda + abonaments i c�rrecs :
		if(count($mAbonamentsCarrecs['grup'])>0)
		{
			echo "
		<tr>
			<td  bgcolor='#ffffff' align='left' style='width:30%;'><p>Abonaments i C�rrecs <br> (Grup a Usuari):</p></td>
			<th  bgcolor='#ffffff' align='right' style='width:10%;'></th>
			<th  bgcolor='#ffffff' style='width:10%;'></th>
			<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
			<td  bgcolor='#ffffff' align='left' style='width:45%;'><p>Concepte:</p></td>
		</tr>
			";
		}
		
		//abonaments i c�rrecs:
		$signe=1;
		$abonamentCarrecEcosT=0;
		$abonamentCarrecEbT=0;
		$abonamentCarrecEurosT=0;
		$operacioText='';
		while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
		{
			if($mAbonamentCarrec['tipus']=='abonamentGrup'){$signe=-1;$operacioText='abonament';}else if ($mAbonamentCarrec['tipus']=='carrecGrup'){$signe=1;$operacioText='c�rrec';}
			$abonamentCarrecEcosT+=$mAbonamentCarrec['producte_id']*$signe;
			$abonamentCarrecEbT+=$mAbonamentCarrec['demanat']*$signe;
			$abonamentCarrecEurosT+=$mAbonamentCarrec['rebut']*$signe;
			echo "
			<tr>
				<td  bgcolor='#ffffff' align='right' style='width:30%;'><p class='nota'>".$mAbonamentCarrec['data']." - ".$operacioText."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p  class='nota'><i>".(number_format($mAbonamentCarrec['producte_id']*$signe,2))."</i></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p class='nota'><i>".(number_format($mAbonamentCarrec['rebut']*$signe,2))."</i></p></td>
				<td  bgcolor='#ffffff' align='left' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='left' style='width:45%;'><p>
				<p class='nota'>".(urldecode($mAbonamentCarrec['comentaris']))."</p>
				</td>
			</tr>
			";
		}
		reset($mAbonamentsCarrecs['grup']);
		echo "
			<tr>
				<th  bgcolor='#ffffff' align='left' style='width:30%;'><p>Total Abonaments i C�rrecs (GRUP a usuaris)</p></th>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(number_format($abonamentCarrecEcosT,2))."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(number_format($abonamentCarrecEurosT,2))."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
		";
	
		//Total Comanda + Abonaments i C�rrecs
		echo "
			<tr>
				<th  bgcolor='#ffffff' align='left' style='width:30%;'><p>Total cobrar de les usuaries</p></th>
				<th  bgcolor='#ffffff' align='right' style='width:10%;'><p id='p_fPagamentEcos1'>".(number_format($abonamentCarrecEcosT+$fpEcos,2))."</p></th>
				<th  bgcolor='#ffffff' align='right' style='width:10%;'><p id='p_fPagamentEu1'>".(number_format($abonamentCarrecEurosT+$fpEu,2))."</p></th>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
	</table>
	<br>
	<br>
	<br>
	&nbsp;
	";
	

	return;
}

//------------------------------------------------------------------------------
function html_mostrarAlbaraGrupTotals($mProductes,$db)
{
	global 
	$mColors,
	$mUsuarisRef,
	$mUsuari,
	$mUsuarisGrupRef,
	$mSelUsuari,
	$mAbonamentsCarrecs,
	$missatgeAlerta,
	$mParametres,
	$mRutesSufixes,
	$puntEntregaId,
	$parsChain,
	$ruta,
	$mRutes,
	$mTipusIncidencies,
	$mPars,
	$mProductes2,
	$mRebostsRef,
	$mRebost,
	$mIncidencies,
	$mPeriodesLocalsInfo,
	$mPropietatsPeriodeLocal,
	$mPuntsEntrega,
	$mFormesPagament,
	$mComanda,
	$mContinguts,
	$mCr,
	$mParsCAC,
	$quantitatTotalCac,
	$quantitatTotalRebosts,
	$mProductesJaEntregatsGrup,
	$llistaText;
	
	$mostrarIncidencies=true;

	if($mParametres['moneda3Activa']['valor']==1)
	{
		$moneda3ElementsStyle=" style='visibility:inherit; z-index:0; position:relative;' ";
	}
	else
	{
		$moneda3ElementsStyle=" style='visibility:hidden; z-index:1;  position:absolute;' ";
	}

	if($mPropietatsPeriodeLocal['comandesLocalsTancades']==1)
	{
		$estatPeriodeText="TANCAT";
		$mostrarIncidencies=false;
		$mostrarAbonamentsCarrecsIreserves=false;
	}
	else if($mPropietatsPeriodeLocal['comandesLocalsTancades']==0)
	{
		$estatPeriodeText="Reserves tancades";
	}
	if($mPropietatsPeriodeLocal['comandesLocalsTancades']==-1)
	{
		$estatPeriodeText="OBERT";
	}

	$class=" class='p.albara' ";

	echo "
	<table border='0' width='100%' style='z-index:10;'  bgcolor='".$mColors['table']."'>
		<tr>
			<td align='left' valign='top' width='30%'>
			<p>
			<font color='red'>- ALBAR� ".$llistaText." (<b>GRUP</b> , totals) -</font>
			<br>Periode: <b>".@$mPeriodesLocalsInfo[$mPars['sel_periode_comanda_local']]['periode_comanda']."(".$mPars['sel_periode_comanda_local'].")(".$estatPeriodeText.")</b>
			<br>CIC - Central d'Abastiment Catal� (CAC)<br>
			(".(date('d-m-Y h:i:s')).")
			</td>

			<td align='left' valign='top' width='35%'>
			<table>
				<tr>
					<td align='left' valign='top'>
					<p>GRUP:</p> 
					</td>
					<td>
					<p><b>".(urldecode($mRebost['nom']))."</b></p>
					</td>
				</tr>
				<tr>
					<td align='left'>
					<p class='nota'>Responsable:</p> 
					</td>
					<td>
					<p  class='nota'>".(urldecode($mUsuarisGrupRef[$mRebost['usuari_id']]['usuari']['usuari']))."</p>
					</td>
				</tr>
				<tr>
					<td align='left'>
					<p  class='nota'>Mobil:</p> 
					</td>
					<td>
					<p class='nota'>".$mRebost['mobil']."</p>
					</td>
				</tr>
				<tr>
					<td align='left'>
					<p  class='nota'>Email:</p> 
					</td>
					<td>
					<p class='nota'>".$mRebost['email_rebost']."</p>
					</td>
				</tr>
			</table>
			</td>
				
			<td valign='top' align='left' width='35%'>
			<table border='0'>
				<tr>
					</td>
				</tr>
				<tr>
					<td valign='bottom' align='left'>
					<p>Punt&nbsp;Entrega:</p>
					</td>

					<td align='left' valign='top'>
					<p>
					".(@urldecode($mPropietatsPeriodeLocal['apeLocal']))."
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>

	<table width='100%' align='right'>
		<tr>
			<td id='td_missatgeAlerta' style=' width:90%;' align='center'>
			".$missatgeAlerta."
			</td>
	
			<td align='right' style='width:10%;'>
			<div>
			<input type='checkbox' id='cb_vistaImpressio2' value='0' onClick=\"javascript:vistaImpressio2();\"> vista impressi�</p>
			</div>
			</td>
		</tr>
	</table>

	<table align='center' border='0' id='t_registreIncidencies'>
		<tr>
			<td align='center' width='100%'>
			<p>[ Registre d'incid�ncies, GRUP<==>usuaries]</p>
			<table bgcolor='#cccccc' align='center'>
				<tr>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>id</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>data</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>ruta</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>autor</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>usuari</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>grup</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>tipus<br>incid�ncia</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>ID<br>producte</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>producte</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>demanat</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>rebut</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>indicacions</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					<p>estat</p>
					</th>
					<th bgcolor='#ffffff' valign='top' align='center'>
					</th>
				</tr>
	";
	$numBotonsEditar=0;
	while(list($key,$mIncidencia)=each($mIncidencies))
	{
		echo "
				<tr>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mIncidencia['id']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mIncidencia['data']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mPeriodesLocalsInfo[$mIncidencia['ruta']]['periode_comanda']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".(@urldecode($mUsuarisRef[$mIncidencia['usuari_id']]['usuari']))."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">"; if($mIncidencia['sel_usuari_id']==0){echo 'GRUP';}else{echo @urldecode($mUsuarisGrupRef[$mIncidencia['sel_usuari_id']]['usuari']['usuari']);} echo "</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".(@urldecode($mRebostsRef[$mIncidencia['grup_id']]['nom']))."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mIncidencia['tipus']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">".$mIncidencia['producte_id']."</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">";
					if(isset($mProductes2[$mIncidencia['producte_id']]))
					{
						echo urldecode($mProductes2[$mIncidencia['producte_id']]['producte']);
					}
					echo "</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">";if($mIncidencia['tipus']=='producte' || $mIncidencia['tipus']=='jaEntregat' || $mIncidencia['tipus']=='pendentEntrega'){echo $mIncidencia['demanat'];} echo "</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">";if($mIncidencia['tipus']=='producte' || $mIncidencia['tipus']=='jaEntregat' || $mIncidencia['tipus']=='pendentEntrega'){echo $mIncidencia['rebut'];} echo "</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p id='p_comentaris_".$numBotonsEditar."'  ".$class.">".(urldecode($mIncidencia['comentaris']))."</p>
					<form id='f_guardarIncidencia_".$numBotonsEditar."' name='f_guardarIncidencia_".$numBotonsEditar."' method='post'  target='_self' action=''>
					<textArea style='visibility:hidden; position:absolute;' id='ta_comentaris_".$numBotonsEditar."' name='ta_comentaris_".$numBotonsEditar."' cols='40' rows='5'></textArea>
					<input type='hidden' id='i_pars_".$numBotonsEditar."' name='i_pars' value=''>
					</form>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class. " style='"; if($mIncidencia['estat']=='pendent'){echo "color:#ff0000;";} echo "'>".$mIncidencia['estat']."</p>
					</td>
				
					<td bgcolor='#ffffff' align='left' valign='middle'>
		";
				
		if($mIncidencia['estat']!='resolta')
		{
			if
			(
				$mIncidencia['estat']=='pendent'
				&& 
				(
					$mPars['usuari']==$mIncidencia['usuari']
					|| 
					$mPars['nivell']=='sadmin'
				)
			)
			{
				echo "
					<input type='button'  class='i_botoEditar' id='i_botoEditarE_".$numBotonsEditar."'  onClick=\"javascript:  editarIncidencia('".$numBotonsEditar."','".$mIncidencia['id']."');\" value='editar'>
					<br>
					<input type='button'  class='i_botoEditar' id='i_botoEditarR_".$numBotonsEditar."' onClick=\"javascript: eliminarIncidencia('".$mIncidencia['id']."');\" value='resoldre'>
					<br>
					<input type='button' style='visibility:hidden;' class='i_botoEditar' id='i_botoEditarG_".$numBotonsEditar."'  onClick=\"javascript:  guardarIncidencia('".$numBotonsEditar."','".$mIncidencia['id']."');\" value='guardar'>
				";
				$numBotonsEditar++;
			}
		}
		echo "
					</td>
				</tr>			
		";
	}
	reset($mIncidencies);
			
	echo "
			</table>
			</form>
			<script>numBotonsEditar=".$numBotonsEditar.";</script>
			</td>
		</tr>
	</table>
	";

	if($mostrarIncidencies)
	{
		echo "
	<table align='center' border='0' id='t_seleccionarTipusIncidencia'>
		<tr>
			<td align='center' width='50%'>
			<p>[ Seleccionar Tipus Incidencia ]</p>
			<table bgcolor='#cccccc' align='center'>
				<tr>
					<td bgcolor='#ffffff'>
					<p>
					<select  onChange=\"javascript:seleccionarTipusIncidencia();\" id='sel_tipusIncidencia' name='sel_tipusIncidencia'>
		";
		while(list($key,$val)=each($mTipusIncidencies))
		{
			echo "
					<option value='".$val."'>".$val."</option>
			";
		}
		reset($mTipusIncidencies);
		echo "
					<option selected value=''></option>
					</select>
					</p>
					</td>
				</tr>
			</table>
			</td>
		";
	}
	else
	{
		echo "
	<div id='t_seleccionarTipusIncidencia'></div>
		";
	}
	if($mostrarIncidencies && $mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'  || $mPars['usuari_id']==$mRebost['usuari_id'])
	{
		echo "
			<td align='left' width='50%'>
			<p class='compacte' style='cursor:pointer;' onClick=\"javascript:seleccionarAbonament();\" id='p_selAbonament' name='p_selAbonament'>&nbsp;&nbsp;&nbsp;<u>Aplicar Abonament</u>&nbsp;(Grup a usuaria)"; if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){echo "<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";} echo "</p>
			<p class='compacte' style='cursor:pointer;' onClick=\"javascript:seleccionarCarrec();\" id='p_selCarrec' name='p_selCarrec'>&nbsp;&nbsp;&nbsp;<u>Aplicar C�rrec</u>&nbsp;(Grup a usuaria)"; if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){echo "<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";} echo "</p>
			</td>
		";
	}
	echo "
		</tr>
	</table>

	<div width='100%'>
		<table align='center' border='1' id='t_afegirIncidencia' width='50%' style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Afegir Incid�ncia ]</p>
				<form id='f_afegirIncidencia' name='f_afegirIncidencia' action='' method='post'>
				<input type='hidden' name='i_form' value='afegirIncidencia'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' id='i_aIdata' name='i_aIdata' value='".(date('d/m/Y  H:i:s'))."'>

				<table align='center'  width='100%'>
					<tr>
						<td  width='100%'>
						<table  width='100%'>
							<tr>
								<td  width='50%'>
								<table  width='100%'>
									<tr>
										<td>
										<p>ruta:</p>
										</td>
										<td>
										<select id='sel_ruta' disabled name='sel_ruta' style='background-color:#dddddd;'>						
										<option selected value='".$mPars['sel_periode_comanda_local']."'>".$mPars['sel_periode_comanda_local']."</option>	
										</select>
										</td>
									</tr>
									<tr>
										<td>
										<p>tipus:</p>
										</td>
										<td>
										<input type='text' readonly id='i_tipusIncidencia' name='i_tipusIncidencia' value='producte' style='background-color:#dddddd;'>						
										</td>
									</tr>
									<tr id='tr_producteId' style='z-Index:0; top:0px; position:relative;'>
										<td>
										<p>id:</p>
										</td>
										<td>
										<input type='text' size='5'  id='i_producteId' name='i_producteId' value=''>
										</td>
									</tr>
									<tr id='tr_producteNom'>
										<td>
										<p>producte:</p>
										</td>
										<td>
										<p id='p_producteNom'></p>
										</td>
									</tr>
									<tr id='tr_producteDemanat' style='z-Index:0; top:0px; position:relative;'>
										<td>
										<p>demanat:</p>
										</td>
										<td>
										<input type='text'  size='5' READONLY style='background-color:#dddddd;' id='i_producteDemanat' name='i_producteDemanat' value=''>
										</td>
									</tr>
									<tr id='tr_producteRebut' style='z-Index:0; top:0px; position:relative;'>
										<td>
										<p>rebut:</p>
										</td>
										<td>
										<input type='text'  size='5' id='i_producteRebut' name='i_producteRebut' value=''>
										</td>
									</tr>
								</table>
								</td>
								<td align='center' valign='top'  width='50%'>
								<table width='100%'>
									<tr>
										<td  width='100%' valign='top' align='right'>
										<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
										</td>
									</tr>
									<tr>
										<td  width='100%' align='center' valign='middle'>
										<p class='nota' style='text-align:justify;'>* Clica el bot� amb l'identificador del producte que vols guardar a la incid�ncia, per carregar les dades del producte<br>
										<br>* Tamb� pots canviar el tipus d'incidencia des del selector de mes amunt
										</p>
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						<table  width='100%'>
							<tr>
								<td valign='top'  width='15%'>
								<p>Comentari:</p>
								</td>
								<td  width='85%'>
								<textArea id='ta_comentaris' name='ta_comentaris' cols='70' rows='5'></textArea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAfegirIncidencia()){document.getElementById('f_afegirIncidencia').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='guardar'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>
		
	";
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord' || $mPars['usuari_id']==$mRebost['usuari_id'])
	{
		echo "
		<table align='center'  border='1' width='50%' id='t_aplicarAbonament'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Aplicar Abonament Grup a Usuaria]</p>
				<form id='f_aplicarAbonament' name='f_aplicarAbonament' action='' method='post'>
				<input type='hidden' name='i_form' value='abonamentGrup'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' id='i_aAdata' name='i_aAdata' value='".(date('d/m/Y  H:i:s'))."'>

				<table align='center'>
					<tr>
						<td>
						<table>
							<tr>
								<td>
								</td>
								<td align='right'>
								<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
								</td>
							</tr>
							<tr>
								<td>
								<p>tipus:</p>
								</td>
								<td>
								<input type='text' readonly id='i_tipusIncidenciaAa' name='i_tipusIncidencia' value='abonamentGrup'>						
								</td>
							</tr>
							<tr>
								<td>
								<p>ecos:</p>
								</td>
								<td>
								<input type='text' id='i_ecosAa' name='i_ecosAa' value=''>						
								</td>
							</tr>
							<tr ".$moneda3ElementsStyle.">
								<td>
								<p>".$mParametres['moneda3Nom']['valor'].":</p>
								</td>
								<td>
								<input type='text' id='i_ebAa' name='i_ebAa' value=''>						
								</td>
							</tr>
							<tr>
								<td>
								<p>euros:</p>
								</td>
								<td>
								<input type='text' id='i_eurosAa' name='i_eurosAa' value=''>						
								</td>
							</tr>
							<tr>
								<td valign='top'>
								<p>Comentari:</p>
								</td>
								<td>
								<textArea id='ta_comentarisAa' name='ta_comentarisAa' cols='60' rows='5'></textArea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAplicarAbonament()){document.getElementById('f_aplicarAbonament').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='guardar'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>

		<table align='center'  border='1' width='50%' id='t_aplicarCarrec'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Aplicar C�rrec (Grup a usuaria)]</p>
				<form id='f_aplicarCarrec' name='f_aplicarCarrec' action='' method='post'>
				<input type='hidden' name='i_form' value='carrecGrup'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' id='i_aCdata' name='i_aCdata' value='".(date('d/m/Y  H:i:s'))."'>

				<table align='center'>
					<tr>
						<td>
						<table>
							<tr>
								<td>
								</td>
								<td align='right'>
								<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
								</td>
							</tr>
							<tr>
								<td>
								<p>tipus:</p>
								</td>
								<td>
								<input type='text' readonly id='i_tipusIncidenciaAc' name='i_tipusIncidencia' value='carrecGrup'>						
								</td>
							</tr>
							<tr>
								<td>
								<p>ecos:</p>
								</td>
								<td>
								<input type='text' id='i_ecosAc' name='i_ecosAc' value=''>						
								</td>
							</tr>
							<tr ".$moneda3ElementsStyle.">
								<td>
								<p>".$mParametres['moneda3Nom']['valor'].":</p>
								</td>
								<td>
								<input type='text' id='i_ebAc' name='i_ebAc' value=''>						
								</td>
							</tr>
							<tr>
								<td>
								<p>euros:</p>
								</td>
								<td>
								<input type='text' id='i_eurosAc' name='i_eurosAc' value=''>						
								</td>
							</tr>
							<tr>
								<td valign='top'>
								<p>Comentari:</p>
								</td>
								<td>
								<textArea id='ta_comentarisAc' name='ta_comentarisAc' cols='60' rows='5'></textArea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAplicarCarrec()){document.getElementById('f_aplicarCarrec').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='guardar'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>
		<div id='t_anularReserva'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
		</td>
		";
	}
	else
	{
		echo "
		<div id='t_aplicarAbonament'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
		<div id='t_aplicarCarrec'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
		<div id='t_anularReserva'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
		";

	}


	echo "
	</div>
	<center><p style='font-size:16px;'>[ ALBAR� ]</p></center>
	";
	$numBotonsEditar2=0;
	echo "
	<table width='100%'>
		<tr>
			<td align='left' width='50%'>
			<p>[ordre:<b>".$mPars['sortBy']."-".$mPars['ascdesc']."</b>]</p>
			</td>
			<td align='right' width='50%'>
			<p>[Data �ltima revisi� comanda:<b>".(formatarData(getDataComanda($db)))."]</b></p>
			</td>
		</tr>
	</table>
	
	<table bgcolor='#dddddd' width='100%'>
		<tr>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>- id -</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>producte</p></th>
			<th bgcolor='#ffffff'><p class='albara'>productor</p></th>
			<th bgcolor='#ffffff'><p class='albara'>tipus</p></th>
			<th bgcolor='#ffffff'><p class='albara'>categoria10</p></th>
			<th bgcolor='#ffffff'><p class='albara'>format</p></th>
			<th bgcolor='#ffffff'><p class='albara'>unitat_facturacio</p></th>
			<th bgcolor='#ffffff'><p class='albara'>pes</p></th>
			<th bgcolor='#ffffff'><p class='albara'>preu</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ms</p></th>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>- id -</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'><p class='albara'><font color='red'>JE</font></p></th>
			<th bgcolor='#ffffff'><p class='albara'>ums</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ecos</p></th>
			<th bgcolor='#ffffff'><p class='albara'>euros</p></th>
		</tr>
	";
	$cont=0;
	$quantitatT=0;
	$umst=0;
	$ecost=0;
	$eurost=0;
	$ctkTums=0;
	$ctkTecos=0;
	$ctkTeuros=0;
	$ctrTums=0;
	$ctrTecos=0;
	$ctrTeuros=0;

	$mProductesJaEntregatsGrup_=array();

	while(list($key,$val)=each($mProductes))
	{
		//$mRevisioRebosts=getDatesReservaProducte($db,$mProductes[$key]['id']);
		//$dataUltimaRevisioF=formatarData($mRevisioRebosts[$mPars['rebost']][$mProductes[$key]['id']]['num_comanda']);
		
		$color='black';
		$color2='black';
		if(substr_count($mProductes[$key]['tipus'], 'especial')==0)
		{

			$ums_=$mProductes[$key]['preu']*$mComanda[$mProductes[$key]['id']];
			$ecos_=$ums_*$mProductes[$key]['ms']/100;
			$euros_=$ums_-$ecos_;
			if(isset($mProductesJaEntregatsGrup[$mProductes[$key]['id']]))
			{
				while(list($incdId,$mIncd)=each($mProductesJaEntregatsGrup[$mProductes[$key]['id']]))
				{
					if(!isset($mProductesJaEntregatsGrup_[$mProductes[$key]['id']])){$mProductesJaEntregatsGrup_[$mProductes[$key]['id']]=0;}
					$mProductesJaEntregatsGrup_[$mProductes[$key]['id']]+=$mIncd['rebut'];
				}
				if($mProductesJaEntregatsGrup_[$mProductes[$key]['id']]>0)
				{
					if($mProductesJaEntregatsGrup_[$mProductes[$key]['id']]!=$mComanda[$mProductes[$key]['id']])
					{
						$color2='red';
					}
					else
					{
						$color2='black';
					}
					$color='red';
				}
				else
				{
					$color='black';
					$color2='black';
				}
			}
			else
			{
				$mProductesJaEntregatsGrup_[$mProductes[$key]['id']]='';
					$color='black';
					$color2='black';
			}
		echo "
		<tr>
			<td  bgcolor='".$mColors['table']."'>
			<input type='button' value='".$mProductes[$key]['id']."' id='i_botoEditar2_".$numBotonsEditar2."'  onClick=\"javascript: afegirIncidencia('".$mProductes[$key]['id']."','','GT');\">
			</td>
			<td  bgcolor='".$mColors['table']."'><p id='p_nomProducte_".$mProductes[$key]['id']."' class='albara'  style='color:".$color.";'>".(urldecode($mProductes[$key]['producte']))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".(urldecode(substr($mProductes[$key]['productor'],strpos($mProductes[$key]['productor'],'-')+1)))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".(str_replace(',jjaa,',',<b>jjaa</b>,',$mProductes[$key]['tipus']))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".$mProductes[$key]['categoria10']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".$mProductes[$key]['format']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".(urldecode($mProductes[$key]['unitat_facturacio']))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".$mProductes[$key]['pes']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".$mProductes[$key]['preu']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".$mProductes[$key]['ms']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'><i><b>".$mProductes[$key]['id']."</i></b></p></td>
			<td  bgcolor='".$mColors['table']."'><p id='p_demanat_".$mProductes[$key]['id']."' class='albara' style='color:".$color2.";'>".(number_format($mComanda[$mProductes[$key]['id']],0))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".$mProductesJaEntregatsGrup_[$mProductes[$key]['id']]."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".(number_format($ums_,2))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".(number_format($ecos_,2))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'  style='color:".$color.";'>".(number_format($euros_,2))."</p></td>

		</tr>
			";
			$umst+=$ums_;
			$ecost+=$ecos_;
			$eurost+=$euros_;
			$quantitatT+=$mComanda[$mProductes[$key]['id']]*$mProductes[$key]['pes'];
			$ctkTums+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*$mPropietatsPeriodeLocal['ctikLocal'];
			$ctkTecos+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*$mPropietatsPeriodeLocal['ctikLocal']*$mPropietatsPeriodeLocal['ms_ctikLocal']/100;
			$ctkTeuros+=$mProductes[$key]['pes']*$mPropietatsPeriodeLocal['ctikLocal']*(100-$mPropietatsPeriodeLocal['ms_ctikLocal'])/100;
			//$ctrTums+=  $mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*(($mParametres['cost_transport_extern_repartit']['valor']+$mParametres['cost_transport_intern_repartit']['valor'])/($quantitatTotalCac+$quantitatTotalRebosts));
			//$ctrTecos+= $mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*(($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100+$mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)/($quantitatTotalCac+$quantitatTotalRebosts));
			//$ctrTeuros+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*(($mParametres['cost_transport_extern_repartit']['valor']*(100-$mParametres['ms_ctear']['valor'])/100+$mParametres['cost_transport_intern_repartit']['valor']*(100-$mParametres['ms_ctiar']['valor'])/100)/($quantitatTotalCac+$quantitatTotalRebosts));
					
			$cont++;
			if($cont==15)
			{
				echo "
		<tr>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>id</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>producte</p></th>
			<th bgcolor='#ffffff'><p class='albara'>productor</p></th>
			<th bgcolor='#ffffff'><p class='albara'>tipus</p></th>
			<th bgcolor='#ffffff'><p class='albara'>categoria10</p></th>
			<th bgcolor='#ffffff'><p class='albara'>format</p></th>
			<th bgcolor='#ffffff'><p class='albara'>unitat_facturacio</p></th>
			<th bgcolor='#ffffff'><p class='albara'>pes</p></th>
			<th bgcolor='#ffffff'><p class='albara'>preu</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ms</p></th>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>- id -</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'><p class='albara'><font color='red'>JE</font></p></th>
			<th bgcolor='#ffffff'><p class='albara'>ums</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ecos</p></th>
			<th bgcolor='#ffffff'><p class='albara'>euros</p></th>
		</tr>
				";
				$cont=0;
			}
			$numBotonsEditar2++;
		}
	}
	reset($mProductes);


	
	$umstFDC=$umst*$mPropietatsPeriodeLocal['fdLocal']/100;
	$ecostFDC=$umst*$mPropietatsPeriodeLocal['fdLocal']*$mPropietatsPeriodeLocal['ms_fdLocal']/100;
	$eurostFDC=$umstFDC-$ecostFDC;

	
	echo "
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>Total<br>productes</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($umst,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ecost,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($eurost,2))."</p></td>
		</tr>
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>Total Fons Despeses Local<br>(".$mPropietatsPeriodeLocal['fdLocal']."% sobre preus, ".$mPropietatsPeriodeLocal['ms_fdLocal']."% MS)</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($umstFDC,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ecostFDC,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($eurostFDC,2))."</p></td>
		</tr>
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>total<br>transport</p></td>
			<td  bgcolor='#ffffff'><p>(".(number_format($quantitatT,2))." kg)</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ctkTums,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ctkTecos,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ctkTeuros,2))."</p></td>
		</tr>
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>Total Comanda</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<th  bgcolor='#ffffff'><p>".(number_format(($umst+$ctkTums+$umstFDC),2))."</p></th>
			<th  bgcolor='#ffffff'><p>".(number_format(($ecost+$ctkTecos+$ecostFDC),2))."</p></th>
			<th  bgcolor='#ffffff'><p>".(number_format(($eurost+$ctkTeuros+$eurostFDC),2))."</p></th>
		</tr>
	</table>
	";
	echo "<script>numBotonsEditar2=".$numBotonsEditar2.";</script>
	<p class='nota'>* No s'inclouen les comandes de productes especials</p>
	<p class='nota'><font color='red'>** els productes ja entregats (totalment o parcial) apareixen en vermell. La quantitat ja entregada s'indica a la columna 'JE'</font></p>
	";

	$umsTotalFp=$mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu'];
	$umsTotalComanda=$umst+$ctkTums+$umstFDC;
	$ecosTotalComanda=$ecost+$ctkTecos+$ecostFDC;
	$eurosTotalComanda=$eurost+$ctkTeuros+$eurostFDC;
	//Total comanda:
	echo "
	<center></center>
	<table width='80%' bgcolor='#eeeeee' align='center'>	
			<tr>
				<td  bgcolor='#ffffff' align='right' style='width:30%;'><p></p></td>
				<th  bgcolor='#ffffff' align='right' style='width:10%;'><p>ecos</p></th>
				<th  bgcolor='#ffffff' align='right' style='width:10%;'><p>euros</p></th>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
	";
	
	$fpEcos=$umsTotalComanda;
	$fpEu=$ecosTotalComanda;

		echo "
			<tr>
				<th  bgcolor='#ffffff' align='left' style='width:30%;'><p>Total Comanda (forma de pagament)</th>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(@number_format($fpEcos,2))."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(@number_format($fpEu,2))."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
		";
	
	//Total comanda + abonaments i c�rrecs :
	if(count($mAbonamentsCarrecs['grup'])>0)
	{
		echo "
			<tr>
				<td  bgcolor='#ffffff' align='left' style='width:30%;'><p>Abonaments i C�rrecs (Grup a usuaries):</p></td>
				<th  bgcolor='#ffffff' align='right' style='width:10%;'></th>
				<th  bgcolor='#ffffff' style='width:10%;'></th>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='left' style='width:45%;'><p>Concepte:</p></td>
			</tr>
			";
	}
	//abonaments i c�rrecs:
			$signe=1;
			$abonamentCarrecEcosT=0;
			$abonamentCarrecEbT=0;
			$abonamentCarrecEurosT=0;
			$operacioText='';
			while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
			{
				if($mAbonamentCarrec['tipus']=='abonamentGrup'){$signe=-1;$operacioText='abonament';}else if ($mAbonamentCarrec['tipus']=='carrecGrup'){$signe=1;$operacioText='c�rrec';}
				$abonamentCarrecEcosT+=$mAbonamentCarrec['producte_id']*$signe;
				$abonamentCarrecEbT+=$mAbonamentCarrec['demanat']*$signe;
				$abonamentCarrecEurosT+=$mAbonamentCarrec['rebut']*$signe;
				echo "
			<tr>
				<td  bgcolor='#ffffff' align='right' style='width:30%;'><p class='nota'>".$mAbonamentCarrec['data']." - ".$operacioText."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p  class='nota'><i>".(number_format($mAbonamentCarrec['producte_id']*$signe,2))."</i></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p class='nota'><i>".(number_format($mAbonamentCarrec['rebut']*$signe,2))."</i></p></td>
				<td  bgcolor='#ffffff' align='left' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='left' style='width:45%;'><p>
				<p class='nota'>".(urldecode($mAbonamentCarrec['comentaris']))."</p>
				</td>
			</tr>
				";
			}
			reset($mAbonamentsCarrecs['grup']);

		echo "
			<tr>
				<th  bgcolor='#ffffff' align='left' style='width:30%;'><p>Total Abonaments i C�rrecs (GRUP a usuaries)</p></th>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(number_format($abonamentCarrecEcosT,2))."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(number_format($abonamentCarrecEurosT,2))."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
		";
	
		//Total Comanda + Abonaments i C�rrecs
		echo "
			<tr>
				<th  bgcolor='#ffffff' align='left' style='width:30%;'><p>Total a cobrar de les usuaries</p></th>
				<th  bgcolor='#ffffff' align='right' style='width:10%;'><p id='p_fPagamentEcos1'>".(number_format($abonamentCarrecEcosT+$fpEcos,2))."</p></th>
				<th  bgcolor='#ffffff' align='right' style='width:10%;'><p id='p_fPagamentEu1'>".(number_format($abonamentCarrecEurosT+$fpEu,2))."</p></th>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
		</table>
		";

	

	return;
}

//------------------------------------------------------------------------------
function html_mostrarAlbaraUsuari($mProductes,$db)
{
	global 
	$mColors,
	$mUsuarisRef,
	$mUsuari,
	$mUsuarisGrupRef,
	$mSelUsuari,
	$mAbonamentsCarrecs,
	$missatgeAlerta,
	$mParametres,
	$mRutesSufixes,
	$puntEntregaId,
	$parsChain,
	$ruta,
	$mRutes,
	$mTipusIncidencies,
	$mPars,
	$mProductes2,
	$mPeriodesLocalsInfo,
	$mPropietatsPeriodeLocal,
	$mRebostsRef,
	$mRebost,
	$mIncidencies,
	$mPuntsEntrega,
	$mFormesPagament,
	$mComanda,
	$mContinguts,
	$mCr,
	$mParsCAC,
	$quantitatTotalCac,
	$quantitatTotalRebosts,
	$llistaText;
	$mostrarIncidencies=true;
	
	if
	(
		$mPars['nivell']=='sadmin'
		||
		$mPars['nivell']=='admin'
		||
		$mPars['nivell']=='coord'
		||
		$mPars['usuari_id']==$mRebost['usuari_id']
	)
	{
		$styleIncidencies=" style='z-index:0; top:0px; position:relative; visibility:inherit;' ";
		$styleAbonaments=" style='z-index:0; top:0px; position:relative; visibility:inherit;' ";
		$styleCarrecs=" style='z-index:0; top:0px; position:relative; visibility:inherit;' ";
	}
	else
	{
		if($mPars['usuari_id']==$mPars['selUsuariId'])
		{
			$styleIncidencies=" style='z-index:0; top:0px; position:relative; visibility:inherit;' ";
		}
		else
		{
			$styleIncidencies=" style='z-index:0; top:0px; position:absolute; visibility:hidden;' ";
		}
		$styleAbonaments=" style='z-index:0; top:0px; position:absolute; visibility:hidden;' ";
		$styleCarrecs=" style='z-index:0; top:0px; position:absolute; visibility:hidden;' ";
	}

		if($mParametres['moneda3Activa']['valor']==1)
		{
			$moneda3ElementsStyle=" style='visibility:inherit; z-index:0; position:relative;' ";
		}
		else
		{
			$moneda3ElementsStyle=" style='visibility:hidden; z-index:1;  position:absolute;' ";
		}

	if($mPropietatsPeriodeLocal['comandesLocalsTancades']==1)
	{
		$estatPeriodeText="TANCAT";
		$mostrarIncidencies=false;
		$mostrarAbonamentsCarrecsIreserves=false;
	}
	else if($mPropietatsPeriodeLocal['comandesLocalsTancades']==0)
	{
		$estatPeriodeText="Reserves tancades";
	}
	if($mPropietatsPeriodeLocal['comandesLocalsTancades']==-1)
	{
		$estatPeriodeText="OBERT";
	}

	$class=" class='p.albara' ";

	echo "
	<table border='0' width='100%' style='z-index:10;'  bgcolor='".$mColors['table']."'>
		<tr>
			<td align='left' valign='top' width='30%'>
			<p>
			<font color='red'>- ALBAR� ".$llistaText." (<b>USUARIA</b>) -</font>
			<br>Periode: <b>".@$mPeriodesLocalsInfo[$mPars['sel_periode_comanda_local']]['periode_comanda']."(".$mPars['sel_periode_comanda_local'].")(".$estatPeriodeText.")</b>
			<br>CIC - Central d'Abastiment Catal� (CAC)<br>
			(".(date('d-m-Y h:i:s')).")
			</td>

			<td align='left' valign='top' width='20%'>
			<table>
				<tr>
					<td align='left' valign='top'>
					<p>Usuari:</p>
					</td>
					<td align='left' valign='top'>
					<p><b>".(strtoupper(urldecode($mSelUsuari['usuari'])))."</b></p>
				</tr>
				<tr>
					<td align='left' valign='top'>
					<p  class='nota'>mobil:</p>
					</td>
					<td align='left' valign='top'>
					<p  class='nota'>".$mSelUsuari['mobil']."</p>
				</tr>
				<tr>
					<td align='left' valign='top'>
					<p  class='nota'>Email:</p>
					</td>
					<td align='left' valign='top'>
					<p  class='nota'>".$mSelUsuari['email']."</p>
				</tr>
			</table>
			</td>

			<td align='left' valign='top' width='20%'>
			<table>
				<tr>
					<td align='left' valign='top'>
					<p>GRUP:</p> 
					</td>
					<td>
					<p><b>".(urldecode($mRebost['nom']))."</b></p>
					</td>
				</tr>
				<tr>
					<td align='left'>
					<p class='nota'>Responsable:</p> 
					</td>
					<td>
					<p  class='nota'>".(urldecode($mUsuarisGrupRef[$mRebost['usuari_id']]['usuari']['usuari']))."</p>
					</td>
				</tr>
				<tr>
					<td align='left'>
					<p  class='nota'>Mobil:</p> 
					</td>
					<td>
					<p class='nota'>".$mRebost['mobil']."</p>
					</td>
				</tr>
				<tr>
					<td align='left'>
					<p class='nota'>Email:</p> 
					</td>
					<td>
					<p  class='nota'>".$mRebost['email_rebost']."</p>
					</td>
				</tr>
			</table>
			</td>
				
			<td valign='top' align='left' width='40%'>
			<table>
				<tr>
					<td align='left' valign='top'>
					<p>Forma&nbsp;pagament:</p>
					</td>
					<td align='left' valign='top'>
					<table>
						<tr  id='tr_fPagamentEcos' style='visibility:hidden; position:absolute;'>
							<th align='right' valign='top'>
							<p class='compacte' id='p_fPagamentEcos'>".$mPars['fPagamentEcos']."</p>
							</th>
							<th align='right' valign='top'>
							<p class='compacte'>ecos</p>
							</th>
							<td align='right' valign='top'>
							<p class='compacte'>(".$mPars['compte_ecos'].")</p>
							</td>
							<td align='right' valign='top'>
							<p class='compacte'>("; if($mPars['fPagamentEcosIntegralCES']=='1'){echo 'INTEGRALCES';}else{ echo 'CES';} echo ")</p>
							</td>
						</tr>

						<tr  id='tr_fPagamentEb' style='visibility:hidden; position:absolute;'>
							<th align='right' valign='top'>
							<p class='compacte' id='p_fPagamentEb'>".$mPars['fPagamentEb']."</p>
							</th>
							<th align='right' valign='top'>
							<p class='compacte'>".$mParametres['moneda3Nom']['valor']."</p>
							</th>
							<td align='right' valign='top'>
							<p class='compacte'>(".$mPars['compte_cieb'].")</p>
							</td>
							<td align='right' valign='top'>
							</td>
						</tr>
		";
		if($mPars['fPagamentEuMetalic']==1)
		{
			echo "
						<tr  id='tr_fPagamentEu' style='visibility:hidden; position:absolute;'>
							<th align='right' valign='top'>
							<p class='compacte' id='p_fPagamentEu'>".$mPars['fPagamentEu']."</p>
							</th>
							<th align='right' valign='top'>
							<p class='compacte'>euros</p>
							</th>
							<td align='right' valign='top'>
							<p class='compacte'>(en met�l.lic)</p>
							</td>
							<td align='right' valign='top'>
							</td>
						</tr>
							";
		}
		else if($mPars['fPagamentEuTransf']==1)
		{							
			echo "
						<tr  id='tr_fPagamentEu' style='visibility:hidden; position:absolute;'>
							<th align='right' valign='top'>
							<p class='compacte' id='p_fPagamentEu'>".$mPars['fPagamentEu']."</p>
							</th>
							<th align='right' valign='top'>
							<p class='compacte'>euros</p>
							</th>
							<td align='right' valign='top'>
							<p class='compacte'>(per transfer�ncia)</p>
							</td>
							<td align='right' valign='top'>
							</td>
						</tr>
			";
		}
		else
		{							
			echo "
						<tr  id='tr_fPagamentEu' style='visibility:hidden; position:absolute;'>
							<th align='right' valign='top'>
							<p class='compacte' id='p_fPagamentEu'>".$mPars['fPagamentEu']."</p>
							</th>
							<th align='right' valign='top'>
							<p class='compacte'>euros</p>
							</th>
							<td align='right' valign='top'>
							<p class='compacte'></p>
							</td>
							<td align='right' valign='top'>
							</td>
						</tr>
			";
		}
		echo "
					</table>					
					</td>
				</tr>
				<tr>
					<td valign='top' align='left'>
					<p>Punt&nbsp;Entrega:</p>
					</td>

					<td align='left' valign='top'>
					<p>
					".(@urldecode($mPropietatsPeriodeLocal['apeLocal']))."
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>

	<table width='100%' align='right'>
		<tr>
			<td id='td_missatgeAlerta' style=' width:90%;' align='center'>
			".$missatgeAlerta."
			</td>
	
			<td align='right' style='width:10%;'>
			<input type='checkbox' id='cb_vistaImpressio2' value='0' onClick=\"javascript:vistaImpressio2();\"> vista impressi�</p>
			</td>
		</tr>
	</table>


<table align='center' border='0' id='t_registreIncidencies' >
	<tr>
		<td align='center' width='100%'>
		<p>[ Registre d'incid�ncies , USUARIA<==>GRUP]</p>
		<table bgcolor='#cccccc' align='center'>
			<tr>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>id</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>data</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>ruta</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>autor</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>usuari</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>grup</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>tipus<br>incid�ncia</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>ID<br>producte</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>producte</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>demanat</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>rebut</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>indicacions</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				<p>estat</p>
				</th>
				<th bgcolor='#ffffff' valign='top' align='center'>
				</th>
			</tr>
		";
		$numBotonsEditar=0;
		while(list($key,$mIncidencia)=each($mIncidencies))
		{
			
			echo "
			<tr>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['id']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['data']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mPeriodesLocalsInfo[$mIncidencia['ruta']]['periode_comanda']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".(@urldecode($mUsuarisRef[$mIncidencia['usuari_id']]['usuari']))."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">"; if($mIncidencia['sel_usuari_id']==0){echo 'GRUP';}else{echo @urldecode($mUsuarisGrupRef[$mIncidencia['sel_usuari_id']]['usuari']['usuari']);} echo "</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".(@urldecode($mRebostsRef[$mIncidencia['grup_id']]['nom']))."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['tipus']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['producte_id']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".(@urldecode($mProductes2[$mIncidencia['producte_id']]['producte']))."</p>
				</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">";if($mIncidencia['tipus']=='producte' || $mIncidencia['tipus']=='jaEntregat' || $mIncidencia['tipus']=='pendentEntrega'){echo $mIncidencia['demanat'];} echo "</p>
					</td>
					<td bgcolor='#ffffff' valign='top' align='left'>
					<p ".$class.">";if($mIncidencia['tipus']=='producte' || $mIncidencia['tipus']=='jaEntregat' || $mIncidencia['tipus']=='pendentEntrega'){echo $mIncidencia['rebut'];} echo "</p>
					</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p id='p_comentaris_".$numBotonsEditar."'  ".$class.">".(urldecode($mIncidencia['comentaris']))."</p>
				<form id='f_guardarIncidencia_".$numBotonsEditar."' name='f_guardarIncidencia_".$numBotonsEditar."' method='post'  target='_self' action=''>
				<textArea style='visibility:hidden; position:absolute;' id='ta_comentaris_".$numBotonsEditar."' name='ta_comentaris_".$numBotonsEditar."' cols='40' rows='5'></textArea>
				<input type='hidden' id='i_pars_".$numBotonsEditar."' name='i_pars' value=''>
				</form>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class. " style='"; if($mIncidencia['estat']=='pendent'){echo "color:#ff0000;";} echo "'>".$mIncidencia['estat']."</p>
				</td>
				
				<td bgcolor='#ffffff' align='left' valign='middle'>
			";
				
			if($mIncidencia['estat']!='resolta')
			{
				if
				(
					$mIncidencia['estat']=='pendent' 
					&& 
					(
						$mPars['usuari']==$mIncidencia['usuari'] 
						|| 
						$mPars['nivell']=='sadmin' 
					)
				)
				{
					echo "
				<input type='button'  class='i_botoEditar' id='i_botoEditarE_".$numBotonsEditar."'  onClick=\"javascript:  editarIncidencia('".$numBotonsEditar."','".$mIncidencia['id']."');\" value='editar'>
				<br>
				<input type='button'  class='i_botoEditar' id='i_botoEditarR_".$numBotonsEditar."' onClick=\"javascript: eliminarIncidencia('".$mIncidencia['id']."');\" value='resoldre'>
				<br>
				<input type='button' style='visibility:hidden;' class='i_botoEditar' id='i_botoEditarG_".$numBotonsEditar."'  onClick=\"javascript:  guardarIncidencia('".$numBotonsEditar."','".$mIncidencia['id']."');\" value='guardar'>
					";
					$numBotonsEditar++;
				}
			}
			echo "
				</td>
			</tr>			
			";
		}
		reset($mIncidencies);
			
			echo "
		</table>
		</form>
		<script>numBotonsEditar=".$numBotonsEditar.";</script>
		</td>
	</tr>
</table>

";
if($mostrarIncidencies)
{
	echo "
<table align='center' border='0' id='t_seleccionarTipusIncidencia' >
	<tr>
		<td align='center' width='50%'>
		<p>[ Seleccionar Tipus Incidencia ]</p>
		<table bgcolor='#cccccc' align='center'>
			<tr>
				<td bgcolor='#ffffff'>
				<p>
				<select  onChange=\"javascript:seleccionarTipusIncidencia();\" id='sel_tipusIncidencia' name='sel_tipusIncidencia'>
				";
				while(list($key,$val)=each($mTipusIncidencies))
				{
					if($val!='jaEntregat')
					{
						echo "
				<option value='".$val."'>".$val."</option>
						";
					}
				}
				reset($mTipusIncidencies);
				echo "
				<option selected value=''></option>
				</select>
				</p>
				</td>
			</tr>
		</table>
		</td>
	";
}
else
{
	echo "
	<div id='t_seleccionarTipusIncidencia' ></div>
	";
}
if($mostrarIncidencies && $mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord' || ($mPars['selUsuariId']!='0' && $mPars['usuari_id']==$mRebost['usuari_id']))
{
	echo "
		<td align='left' width='50%'>
		<p class='compacte' style='cursor:pointer;' onClick=\"javascript:seleccionarAbonament();\" id='p_selAbonament' name='p_selAbonament'>&nbsp;&nbsp;&nbsp;<u>Aplicar Abonament</u>&nbsp;(Grup a Usuari)"; if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){echo "<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";} echo "</p>
		<p class='compacte' style='cursor:pointer;' onClick=\"javascript:seleccionarCarrec();\" id='p_selCarrec' name='p_selCarrec'>&nbsp;&nbsp;&nbsp;<u>Aplicar C�rrec</u>&nbsp;(Grup a Usuari)"; if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){echo "<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";} echo "</p>
		</td>
	";
}
echo "
	</tr>
</table>

<div width='100%' >

		<table align='center' border='1' id='t_afegirIncidencia' width='50%' style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Afegir Incid�ncia ]</p>
				<form id='f_afegirIncidencia' name='f_afegirIncidencia' action='' method='post'>
				<input type='hidden' name='i_form' value='afegirIncidencia'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' id='i_aIdata' name='i_aIdata' value='".(date('d/m/Y  H:i:s'))."'>

				<table align='center'  width='100%'>
					<tr>
						<td  width='100%'>
						<table  width='100%'>
							<tr>
								<td  width='50%'>
								<table  width='100%'>
									<tr>
										<td>
										<p>ruta:</p>
										</td>
										<td>
										<select id='sel_ruta' READONLY name='sel_ruta' style='background-color:#dddddd;'>						
										<option selected value='".$mPars['sel_periode_comanda_local']."'>".@$mPeriodesLocalsInfo[$mPars['sel_periode_comanda_local']]['periode_comanda']."</option>	
										</select>
										</td>
									</tr>
									<tr>
										<td>
										<p>tipus:</p>
										</td>
										<td>
										<input type='text' readonly id='i_tipusIncidencia' name='i_tipusIncidencia' value='producte' style='background-color:#dddddd;'>						
										</td>
									</tr>
									<tr id='tr_producteId' style='z-Index:0; top:0px; position:relative;'>
										<td>
										<p>id:</p>
										</td>
										<td>
										<input type='text' size='5'  id='i_producteId' name='i_producteId' value=''>
										</td>
									</tr>
									<tr id='tr_producteNom'>
										<td>
										<p>producte:</p>
										</td>
										<td>
										<p id='p_producteNom'></p>
										</td>
									</tr>
									<tr id='tr_producteDemanat' style='z-Index:0; top:0px; position:relative;'>
										<td>
										<p>demanat:</p>
										</td>
										<td>
										<input type='text'  size='5' READONLY style='background-color:#dddddd;' id='i_producteDemanat' name='i_producteDemanat' value=''>
										</td>
									</tr>
									<tr id='tr_producteRebut' style='z-Index:0; top:0px; position:relative;'>
										<td>
										<p>rebut:</p>
										</td>
										<td>
										<input type='text'  size='5' id='i_producteRebut' name='i_producteRebut' value=''>
										</td>
									</tr>
								</table>
								</td>
								<td align='center' valign='top'  width='50%'>
								<table width='100%'>
									<tr>
										<td  width='100%' valign='top' align='right'>
										<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
										</td>
									</tr>
									<tr>
										<td  width='100%' align='center' valign='middle'>
										<p class='nota' style='text-align:justify;'>* Clica el bot� amb l'identificador del producte que vols guardar a la incid�ncia, per carregar les dades del producte<br>
										<br>* Tamb� pots canviar el tipus d'incidencia des del selector de mes amunt
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						<table  width='100%'>
							<tr>
								<td valign='top'  width='15%'>
								<p>Comentari:</p>
								</td>
								<td  width='85%'>
								<textArea id='ta_comentaris' name='ta_comentaris' cols='70' rows='5'></textArea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAfegirIncidencia()){document.getElementById('f_afegirIncidencia').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='guardar'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>
		
";
if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord' || ($mPars['selUsuariId']!='0' && $mPars['usuari_id']==$mRebost['usuari_id']))
{
	echo "
		<table align='center'  border='1' width='50%' id='t_aplicarAbonament'  style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Aplicar Abonament (Grup a Usuari)]</p>
				<form id='f_aplicarAbonament' name='f_aplicarAbonament' action='' method='post'>
				<input type='hidden' name='i_form' value='abonamentGrup'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' id='i_aAdata' name='i_aAdata' value='".(date('d/m/Y  H:i:s'))."'>
				<input type='hidden' id='sel_usuariIncidenciaAa' name='sel_usuariIncidenciaAa' value='".$mPars['selUsuariId']."'>

				<table align='center'>
					<tr>
						<td>
						<table>
							<tr>
								<td>
								</td>
								<td align='right'>
								<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
								</td>
							</tr>
							<tr>
								<td>
								<p>tipus:</p>
								</td>
								<td>
								<input type='text' readonly id='i_tipusIncidenciaAa' name='i_tipusIncidencia' value='abonamentGrup'>						
								</td>
							</tr>
							<tr>
								<td>
								<p>ecos:</p>
								</td>
								<td>
								<input type='text' id='i_ecosAa' name='i_ecosAa' value=''>						
								</td>
							</tr>
							<tr ".$moneda3ElementsStyle.">
								<td>
								<p>".$mParametres['moneda3Nom']['valor'].":</p>
								</td>
								<td>
								<input type='text' id='i_ebAa' name='i_ebAa' value=''>						
								</td>
							</tr>
							<tr>
								<td>
								<p>euros:</p>
								</td>
								<td>
								<input type='text' id='i_eurosAa' name='i_eurosAa' value=''>						
								</td>
							</tr>
							<tr>
								<td valign='top'>
								<p>Comentari:</p>
								</td>
								<td>
								<textArea id='ta_comentarisAa' name='ta_comentarisAa' cols='60' rows='5'></textArea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAplicarAbonament()){document.getElementById('f_aplicarAbonament').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='guardar'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>

		<table align='center'  border='1' width='50%' id='t_aplicarCarrec'  style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
			<tr>
				<td align='center'>
				<p>[ Aplicar C�rrec (Grup a Usuari)]</p>
				<form id='f_aplicarCarrec' name='f_aplicarCarrec' action='' method='post'>
				<input type='hidden' name='i_form' value='carrecGrup'>
				<input type='hidden' name='i_pars' value='".$parsChain."'>
				<input type='hidden' id='i_aCdata' name='i_aCdata' value='".(date('d/m/Y  H:i:s'))."'>
				<input type='hidden' id='sel_usuariIncidenciaAc' name='sel_usuariIncidenciaAc' value='".$mPars['selUsuariId']."'>

				<table align='center'>
					<tr>
						<td>
						<table>
							<tr>
								<td>
								</td>
								<td align='right'>
								<input type='button' value='X' onClick=\"resetCampsIncidencies();\"
								</td>
							</tr>
							<tr>
								<td>
								<p>tipus:</p>
								</td>
								<td>
								<input type='text' readonly id='i_tipusIncidenciaAc' name='i_tipusIncidencia' value='carrecGrup'>						
								</td>
							</tr>
							<tr>
								<td>
								<p>ecos:</p>
								</td>
								<td>
								<input type='text' id='i_ecosAc' name='i_ecosAc' value=''>						
								</td>
							</tr>
							<tr ".$moneda3ElementsStyle.">
								<td>
								<p>".$mParametres['moneda3Nom']['valor'].":</p>
								</td>
								<td>
								<input type='text' id='i_ebAc' name='i_ebAc' value=''>						
								</td>
							</tr>
							<tr>
								<td>
								<p>euros:</p>
								</td>
								<td>
								<input type='text' id='i_eurosAc' name='i_eurosAc' value=''>						
								</td>
							</tr>
							<tr>
								<td valign='top'>
								<p>Comentari:</p>
								</td>
								<td>
								<textArea id='ta_comentarisAc' name='ta_comentarisAc' cols='60' rows='5'></textArea>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								<input type='button' onClick=\"javascript: if(checkAplicarCarrec()){document.getElementById('f_aplicarCarrec').submit();}else{alert(missatgeAlerta);missatgeAlerta='';}\" value='guardar'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
				</td>
			</tr>
		</table>
		</td>
	";
}
else
{
	echo "
		<div id='t_aplicarAbonament'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
		<div id='t_aplicarCarrec'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
	";

}


echo "
		<div id='t_anularReserva'  style='z-index:0; position:absolute; top:0px; visibility:hidden;'></div>
</div>
<center><p style='font-size:16px;'>[ ALBAR� ]</p></center>
";
	$numBotonsEditar2=0;
	echo "
	<table width='100%'>
		<tr>
			<td align='left' width='50%'>
			<p>[ordre:<b>".$mPars['sortBy']."-".$mPars['ascdesc']."</b>]</p>
			</td>
			<td align='right' width='50%'>
			<p>[Data �ltima revisi� comanda:<b>".(formatarData(getDataComanda($db)))."]</b></p>
			</td>
		</tr>
	</table>
	
	<table bgcolor='#dddddd' width='100%'>
		<tr>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>- id -</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>producte</p></th>
			<th bgcolor='#ffffff'><p class='albara'>productor</p></th>
			<th bgcolor='#ffffff'><p class='albara'>tipus</p></th>
			<th bgcolor='#ffffff'><p class='albara'>categoria10</p></th>
			<th bgcolor='#ffffff'><p class='albara'>format</p></th>
			<th bgcolor='#ffffff'><p class='albara'>unitat_facturacio</p></th>
			<th bgcolor='#ffffff'><p class='albara'>pes</p></th>
			<th bgcolor='#ffffff'><p class='albara'>preu</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ms</p></th>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>- id -</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ums</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ecos</p></th>
			<th bgcolor='#ffffff'><p class='albara'>euros</p></th>
		</tr>
		";
		$cont=0;
		$quantitatT=0;
		$umst=0;
		$ecost=0;
		$eurost=0;
		$ctkTums=0;
		$ctkTecos=0;
		$ctkTeuros=0;
		$ctrTums=0;
		$ctrTecos=0;
		$ctrTeuros=0;
		
	while(list($key,$val)=each($mProductes))
	{
		//$mRevisioRebosts=getDatesReservaProducte($db,$mProductes[$key]['id']);
		//$dataUltimaRevisioF=formatarData($mRevisioRebosts[$mPars['rebost']][$mProductes[$key]['id']]['num_comanda']);
		
		if(substr_count($mProductes[$key]['tipus'], 'especial')==0)
		{

		$ums_=$mProductes[$key]['preu']*$mComanda[$mProductes[$key]['id']];
		$ecos_=$ums_*$mProductes[$key]['ms']/100;
		$euros_=$ums_-$ecos_;
		echo "
		<tr>
			<td  bgcolor='".$mColors['table']."'>
			<input type='button' value='".$mProductes[$key]['id']."' id='i_botoEditar2_".$numBotonsEditar2."'  onClick=\"javascript: afegirIncidencia('".$mProductes[$key]['id']."','','U');\">
			</td>
			<td  bgcolor='".$mColors['table']."'><p id='p_nomProducte_".$mProductes[$key]['id']."' class='albara'>".(urldecode($mProductes[$key]['producte']))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(urldecode(substr($mProductes[$key]['productor'],strpos($mProductes[$key]['productor'],'-')+1)))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(str_replace(',jjaa,',',<b>jjaa</b>,',$mProductes[$key]['tipus']))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['categoria10']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['format']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(urldecode($mProductes[$key]['unitat_facturacio']))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['pes']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['preu']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".$mProductes[$key]['ms']."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'><i><b>".$mProductes[$key]['id']."</i></b></p></td>
			<td  bgcolor='".$mColors['table']."'><p id='p_demanat_".$mProductes[$key]['id']."' class='albara'>".(number_format($mComanda[$mProductes[$key]['id']],0))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>&nbsp;</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(number_format($ums_,2))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(number_format($ecos_,2))."</p></td>
			<td  bgcolor='".$mColors['table']."'><p class='albara'>".(number_format($euros_,2))."</p></td>

		</tr>
		";
		$umst+=$ums_;
		$ecost+=$ecos_;
		$eurost+=$euros_;
		$quantitatT+=$mComanda[$mProductes[$key]['id']]*$mProductes[$key]['pes'];
		$ctkTums+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*$mPropietatsPeriodeLocal['ctikLocal'];
		$ctkTecos+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*$mPropietatsPeriodeLocal['ctikLocal']*$mPropietatsPeriodeLocal['ms_ctikLocal']/100;
		$ctkTeuros+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*$mPropietatsPeriodeLocal['ctikLocal']*(100-$mPropietatsPeriodeLocal['ms_ctikLocal'])/100;
		//$ctrTums+=  $mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*(($mParametres['cost_transport_extern_repartit']['valor']+$mParametres['cost_transport_intern_repartit']['valor'])/($quantitatTotalCac+$quantitatTotalRebosts));
		//$ctrTecos+= $mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*(($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100+$mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)/($quantitatTotalCac+$quantitatTotalRebosts));
		//$ctrTeuros+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*(($mParametres['cost_transport_extern_repartit']['valor']*(100-$mParametres['ms_ctear']['valor'])/100+$mParametres['cost_transport_intern_repartit']['valor']*(100-$mParametres['ms_ctiar']['valor'])/100)/($quantitatTotalCac+$quantitatTotalRebosts));
					
		$cont++;
		if($cont==15)
		{
			echo "
		<tr>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>id</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>producte</p></th>
			<th bgcolor='#ffffff'><p class='albara'>producte</p></th>
			<th bgcolor='#ffffff'><p class='albara'>tipus</p></th>
			<th bgcolor='#ffffff'><p class='albara'>categoria10</p></th>
			<th bgcolor='#ffffff'><p class='albara'>format</p></th>
			<th bgcolor='#ffffff'><p class='albara'>unitat_facturacio</p></th>
			<th bgcolor='#ffffff'><p class='albara'>pes</p></th>
			<th bgcolor='#ffffff'><p class='albara'>preu</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ms</p></th>
			<th bgcolor='#ffffff' ><p class='albara'><i><b>- id -</i><b></p></th>
			<th bgcolor='#ffffff'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'><p class='albara'>uts</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ums</p></th>
			<th bgcolor='#ffffff'><p class='albara'>ecos</p></th>
			<th bgcolor='#ffffff'><p class='albara'>euros</p></th>
		</tr>
			";
			$cont=0;
		}
		$numBotonsEditar2++;
		}
	}
	reset($mProductes);

	//fons despeses cac:
	
	$umstFDC=$umst*$mPropietatsPeriodeLocal['fdLocal']/100;
	$ecostFDC=$umst*($mPropietatsPeriodeLocal['fdLocal']/100)*($mPropietatsPeriodeLocal['ms_fdLocal']/100);
	$eurostFDC=$umstFDC-$ecostFDC;

	
	echo "
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>Total<br>productes</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($umst,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ecost,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($eurost,2))."</p></td>
		</tr>
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>Total Fons Despeses Local<br>(".$mPropietatsPeriodeLocal['fdLocal']."% sobre preus, ".$mPropietatsPeriodeLocal['ms_fdLocal']."% MS)</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($umstFDC,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ecostFDC,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($eurostFDC,2))."</p></td>
		</tr>
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>total<br>transport</p></td>
			<td  bgcolor='#ffffff'><p>(".(number_format($quantitatT,2))." kg)</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ctkTums,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ctkTecos,2))."</p></td>
			<td  bgcolor='#ffffff'><p>".(number_format($ctkTeuros,2))."</p></td>
		</tr>
		<tr>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p>Total Comanda</p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<td  bgcolor='#ffffff'><p></p></td>
			<th  bgcolor='#ffffff'><p>".(number_format(($umst+$ctkTums+$umstFDC),2))."</p></th>
			<th  bgcolor='#ffffff'><p>".(number_format(($ecost+$ctkTecos+$ecostFDC),2))."</p></th>
			<th  bgcolor='#ffffff'><p>".(number_format(($eurost+$ctkTeuros+$eurostFDC),2))."</p></th>
		</tr>
	</table>
	";	
	
	echo "<script>numBotonsEditar2=".$numBotonsEditar2.";</script>
	";
	$umsTotalFp=$mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu'];
	$umsTotalComanda=$umsTotalFp;
	$ecosTotalComanda=$mPars['fPagamentEcos'];
	$eurosTotalComanda=$mPars['fPagamentEu'];

	//Total comanda (forma de pagament) :
	echo "
	<center></center>
	<table width='80%' bgcolor='#eeeeee' align='center'>	
			<tr>
				<td  bgcolor='#ffffff' align='right' style='width:30%;'><p></p></td>
				<th  bgcolor='#ffffff' align='right' style='width:10%;'><p>ecos</p></th>
	";
	if($mParametres['moneda3Activa']['valor']==1)
	{
		echo "
				<th  bgcolor='#ffffff' align='right' ".$moneda3ElementsStyle."><p>".$mParametres['moneda3Abrev']['valor']."</p></th>
		";
	}
	echo "
				<th  bgcolor='#ffffff' align='right' style='width:10%;'><p>euros</p></th>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
	";
		$fpEcos=$ecosTotalComanda;
		$fpEu=$eurosTotalComanda;

		echo "
			<tr>
				<th  bgcolor='#ffffff' align='left' style='width:30%;'><p>Total Comanda (forma de pagament)</th>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(number_format($fpEcos,2))."</p></td>
	";
	if($mParametres['moneda3Activa']['valor']==1)
	{
		echo "
				<td  bgcolor='#ffffff' align='right' ".$moneda3ElementsStyle."><p>".(number_format($fpEb,2))."</p></td>
		";
	}
	echo "
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(number_format($fpEu,2))."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
		";
	
		
	
		//Total comanda + abonaments i c�rrecs :
		if(count($mAbonamentsCarrecs)>0)
		{
			echo "
			<tr>
				<td  bgcolor='#ffffff' align='left' style='width:30%;'><p>Abonaments i C�rrecs - <b>llista local</b> <br> (Grup a Usuaria):</p></td>
				<th  bgcolor='#ffffff' align='right' style='width:10%;'></th>
			";
			if($mParametres['moneda3Activa']['valor']==1)
			{
				echo "
				<th  bgcolor='#ffffff' align='right' ".$moneda3ElementsStyle."></th>
				";
			}
			echo "
				<th  bgcolor='#ffffff' style='width:10%;'></th>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='left' style='width:45%;'><p>Concepte:</p></td>
			</tr>
			";
		}
		//abonaments i c�rrecs - llista local:
		$signe=1;
		$abonamentCarrecEcosT=0;
		$abonamentCarrecEbT=0;
		$abonamentCarrecEurosT=0;
		$operacioText='';
		while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
		{
			if($mAbonamentCarrec['sel_usuari_id']==$mPars['selUsuariId'])
			{
				if($mAbonamentCarrec['tipus']=='abonamentGrup'){$signe=-1;$operacioText='abonament';}else if ($mAbonamentCarrec['tipus']=='carrecGrup'){$signe=1;$operacioText='c�rrec';}
				$abonamentCarrecEcosT+=$mAbonamentCarrec['producte_id']*$signe;
				$abonamentCarrecEbT+=$mAbonamentCarrec['demanat']*$signe;
				$abonamentCarrecEurosT+=$mAbonamentCarrec['rebut']*$signe;
				echo "
			<tr>
				<td  bgcolor='#ffffff' align='right' style='width:30%;'><p class='nota'>".$mAbonamentCarrec['data']." - ".$operacioText."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p  class='nota'><i>".(number_format($mAbonamentCarrec['producte_id']*$signe,2))."</i></p></td>
				";
				if($mParametres['moneda3Activa']['valor']==1)
				{
					echo "
				<td  bgcolor='#ffffff' align='right' ".$moneda3ElementsStyle."><p class='nota'><i>".(number_format($mAbonamentCarrec['demanat']*$signe,2))."</i></p></td>
					";
				}
				echo "
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p class='nota'><i>".(number_format($mAbonamentCarrec['rebut']*$signe,2))."</i></p></td>
				<td  bgcolor='#ffffff' align='left' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='left' style='width:45%;'><p>
				<p class='nota'>".(urldecode($mAbonamentCarrec['comentaris']))."</p>
				</td>
			</tr>
				";
			}
		}
		reset($mAbonamentsCarrecs['grup']);
		echo "
			<tr>
				<th  bgcolor='#ffffff' align='left' style='width:30%;'><p>Total Abonaments i C�rrecs <br>(Grup a Usuari)</p></th>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(number_format($abonamentCarrecEcosT,2))."</p></td>
		";
		if($mParametres['moneda3Activa']['valor']==1)
		{
			echo "
				<td  bgcolor='#ffffff' align='right' ".$moneda3ElementsStyle."><p>".(number_format($abonamentCarrecEbT,2))."</p></td>
			";
		}
		echo "
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p>".(number_format($abonamentCarrecEurosT,2))."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
		";
	
		//Total a pagar al grup:
		echo "
			<tr>
				<th  bgcolor='#ffffff' align='left' style='width:30%;'><p>Total a pagar al grup:</th>
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p id='p_fPagamentEcos1'>".(number_format($abonamentCarrecEcosT+$fpEcos,2,'.',''))."</p></td>
		";
		if($mParametres['moneda3Activa']['valor']==1)
		{
			echo "
				<td  bgcolor='#ffffff' align='right'  ".$moneda3ElementsStyle."><p  id='p_fPagamentEb1'>".(number_format($abonamentCarrecEbT+$fpEb,2,'.',''))."</p></td>
			";
		}
		echo "
				<td  bgcolor='#ffffff' align='right' style='width:10%;'><p  id='p_fPagamentEu1'>".(number_format($abonamentCarrecEurosT+$fpEu,2,'.',''))."</p></td>
				<td  bgcolor='#ffffff' align='right' style='width:5%;'><p></p></td>
				<td  bgcolor='#ffffff' align='right' style='width:45%;'><p></p></td>
			</tr>
	</table>
	<br>
	<br>
	<br>
	&nbsp;
	";
	

	return;
}

//------------------------------------------------------------------------------
function html_mostrarAlbaraResumUsuaris($mProductes,$db)
{
	global 
	$mColors,
	$mUsuarisRef,
	$mUsuari,
	$mUsuarisGrupRef,
	$mSelUsuari,
	$mAbonamentsCarrecs,
	$missatgeAlerta,
	$mParametres,
	$mRutesSufixes,
	$puntEntregaId,
	$parsChain,
	$ruta,
	$mRutes,
	$mTipusIncidencies,
	$mPars,
	$mProductes2,
	$mPeriodesLocalsInfo,
	$mPropietatsPeriodeLocal,
	$mRebostsRef,
	$mRebost,
	$mIncidencies,
	$mPuntsEntrega,
	$mFormesPagament,
	$mComanda,
	$mContinguts,
	$mCr,
	$mParsCAC,
	$quantitatTotalCac,
	$quantitatTotalRebosts,
	$llistaText;

	if($mParametres['moneda3Activa']['valor']==1)
	{
		$moneda3ElementsStyle=" style='visibility:inherit; z-index:0; position:relative;' ";
	}
	else
	{
		$moneda3ElementsStyle=" style='visibility:hidden; z-index:1;  position:absolute;' ";
	}

	$class=" class='p.albara' ";
	
	echo "
	<table width='90%' align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td align='left' width='40%' valign='top'>
			<font color='red'><p>	- ALBAR� ".$llistaText."(<b>Resum Usuaris</b>) -</font>
			<br>Periode: <b>".@$mPeriodesLocalsInfo[$mPars['sel_periode_comanda_local']]['periode_comanda']."</b>
			<br>CIC - Central d'Abastiment Catal� (CAC)	(".(date('d-m-Y h:i:s')).")
			</td>
			
			<td align='right'  width='60%'>	
			<table width='100%' border='1' align='center'>
				<tr>
					<td align='left' valign='top' width='33%'>
					<table width='100%'>
						<tr>
							<td align='left' valign='top'>
							<p>GRUP:</p> 
							</td>
							<td>
							<p><b>".(urldecode($mRebost['nom']))."</b></p>
							</td>
						</tr>
				
						<tr>
							<td align='left'>
							<p class='nota'>Responsable:</p> 
							</td>
							<td>
							<p  class='nota'>".(urldecode($mUsuarisGrupRef[$mRebost['usuari_id']]['usuari']['usuari']))."</p>
							</td>
						</tr>
					</table>
					</td>

					<td align='left' valign='top' width='33%'>
					<table width='100%'>
						<tr>
							<td align='left'>
							<p  class='nota'>Mobil:</p> 
							</td>
							<td>
							<p class='nota'>".$mRebost['mobil']."</p>
							</td>
						</tr>
				
						<tr>
							<td align='left'>
							<p class='nota'>Email:</p> 
							</td>
							<td>
							<p  class='nota'>".$mRebost['email_rebost']."</p>
							</td>
						</tr>
					</table>
					</td>

					<td align='left' valign='top' width='33%'>
					<p>Punt&nbsp;Entrega:
					<br>
					".(@urldecode($mPropietatsPeriodeLocal['apeLocal']))."
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	";

	
	
		$numComandes=1;
		$selUsuariId_mem=$mPars['selUsuariId'];
		
		while(list($usuariId,$mUsuariGrup)=each($mUsuarisGrupRef))
		{
			$mPars['selUsuariId']=$usuariId;
			db_getFormaPagamentLocal($db);
			if($mPars['fPagamentUms']*1>0)
			{
				$mProductes=getProductesVistaAlbaraUsuari($db);
				$mSelUsuari=db_getUsuari($mPars['selUsuariId'],$db);

				echo "
	<table width='90%' border='0' bgcolor='#dddddd'  align='center'>			
		<tr>
			<td width='1%' valign='top'  bgcolor='".$mColors['table']."'>			
			<p><b>".$numComandes."</b></p>
			</td>
			
			<td width='33%' valign='top'  bgcolor='".$mColors['table']."'>			
			<table border='0' width='100%'>
				<tr>
					<td align='left' valign='top' width='100%'>
					<table>
						<tr>
							<td align='left' valign='top'>
							<p  class='nota'>Usuari:</p>
							</td>
							<td align='left' valign='top'>
							<p  class='nota'><b>".(strtoupper(urldecode($mUsuariGrup['usuari']['usuari'])))."</b></p>
							</td>
						</tr>
						<tr>
							<td align='left' valign='top'>
							<p  class='nota'>Mobil:</p>
							</td>
							<td align='left' valign='top'>
							<p  class='nota'>".$mUsuariGrup['usuari']['mobil']."</p>
							</td>
						</tr>
						<tr>
							<td align='left' valign='top'>
							<p  class='nota'>Email:</p>
							</td>
							<td align='left' valign='top'>
							<p  class='nota'>".$mUsuariGrup['usuari']['email']."</p>
							</td>
						</tr>
						<tr>
							<td align='left' valign='top'>
							<p  class='nota'>Data �ltima<br>revisi� comanda:</p>
							</td>
							<td align='left' valign='bottom'>
							<p  class='nota'>".(formatarData(getDataComanda($db)))."</p>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>

			<td width='33%' valign='top'  bgcolor='".$mColors['table']."'>			
			<p>Import segons productes:</p>
			<table  width='100%'>
				<tr>
					<td width='100%'>
					<table  width='100%'>
				";
				$cont=0;
				$quantitatT=0;
				$umst=0;
				$ecost=0;
				$eurost=0;
				$ctkTums=0;
				$ctkTecos=0;
				$ctkTeuros=0;
				$numBotonsEditar2=0;
				while(list($key,$val)=each($mProductes))
				{
					if(substr_count($mProductes[$key]['tipus'], 'especial')==0)
					{
						$ums_=$mProductes[$key]['preu']*$mComanda[$mProductes[$key]['id']];
						$ecos_=$ums_*$mProductes[$key]['ms']/100;
						$euros_=$ums_-$ecos_;

						$umst+=$ums_;
						$ecost+=$ecos_;
						$eurost+=$euros_;
						$quantitatT+=$mComanda[$mProductes[$key]['id']]*$mProductes[$key]['pes'];
						$ctkTums+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*$mPropietatsPeriodeLocal['ctikLocal'];
						$ctkTecos+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*$mPropietatsPeriodeLocal['ctikLocal']*$mPropietatsPeriodeLocal['ms_ctikLocal']/100;
						$ctkTeuros+=$mProductes[$key]['pes']*$mComanda[$mProductes[$key]['id']]*$mPropietatsPeriodeLocal['ctikLocal']*(100-$mPropietatsPeriodeLocal['ms_ctikLocal'])/100;
						
						$cont++;
						if($cont==15)
						{
							$cont=0;
						}
						$numBotonsEditar2++;
					}
				}
				reset($mProductes);
	
				echo "						
						<tr>
							<td   bgcolor='".$mColors['table']."'><p align='left'    class='nota'>total productes</p></td>
							<td   bgcolor='".$mColors['table']."'><p align='right'   class='nota'></p></td>
							<td   bgcolor='".$mColors['table']."'><p align='right' class='nota'><i>ums</i><br>".(number_format($umst,2,'.',''))."</p></td>
							<td   bgcolor='".$mColors['table']."'><p align='right' class='nota'><i>ecos</i><br>".(number_format($ecost,2,'.',''))."</p></td>
							<td   bgcolor='".$mColors['table']."'><p align='right' class='nota'><i>euros</i><br>".(number_format($eurost,2,'.',''))."</p></td>
				";
				if($mParametres['moneda3Activa']['valor']==1)
				{
					echo "
							<td   bgcolor='".$mColors['table']."'><p></p></td>
					";
				}
				echo "
						</tr>
						
				";
				// despeses gestio grup:
				
				$umsFd=$ums_*$mPropietatsPeriodeLocal['fdLocal']/100;
				$ecosFd=$ums_*($mPropietatsPeriodeLocal['fdLocal']/100)*($mPropietatsPeriodeLocal['ms_fdLocal']/100);
				$eurosFd=$ums_*($mPropietatsPeriodeLocal['fdLocal']/100)*(100-$mPropietatsPeriodeLocal['ms_fdLocal'])/100;

				echo "
				<tr>
					<td   bgcolor='".$mColors['table']."' align='left' ><p  class='nota'>Despeses gesti� grup<br>(".$mPropietatsPeriodeLocal['fdLocal']." %, ".$mPropietatsPeriodeLocal['fdLocal']." % MS)</b></p></td>
					<td   bgcolor='".$mColors['table']."'><p></p></td>
					<td   bgcolor='".$mColors['table']."' align='right' ><p class='nota'>".(number_format($umsFd,2,'.',''))."</p></td>
					<td   bgcolor='".$mColors['table']."' align='right' >
					<p class='nota'>".(number_format($ecosFd,2,'.',''))."</p>
					</td>
					
					<td   bgcolor='".$mColors['table']."' align='right' >
					<p class='nota'>".(number_format($eurosFd,2,'.',''))."</p>
					</td>
				";
				if($mParametres['moneda3Activa']['valor']==1)
				{
					echo "
					<td   bgcolor='".$mColors['table']."' align='right'>
					<p id='p_fPagamentEb1'>".(number_format(0,2,'.',''))."</p>
					</td>
					";
				}
				echo "
				</tr>
						<tr>
							<td   bgcolor='".$mColors['table']."'>
								<p align='left'    class='nota'>total transport</p>
								<p align='right'   class='nota'>(".(number_format($quantitatT,2,'.',''))." kg)</p>
							</td>
							<td   bgcolor='".$mColors['table']."'></td>
							<td   bgcolor='".$mColors['table']."' valign='top'><p align='right'   class='nota'>".(number_format($ctkTums,2,'.',''))."</p></td>
							<td   bgcolor='".$mColors['table']."' valign='top'><p align='right'   class='nota'>".(number_format($ctkTecos,2,'.',''))."</p></td>
							<td   bgcolor='".$mColors['table']."' valign='top'><p align='right'   class='nota'>".(number_format($ctkTeuros,2,'.',''))."</p></td>
				";
				if($mParametres['moneda3Activa']['valor']==1)
				{
					echo "
							<td   bgcolor='".$mColors['table']."' valign='top'><p></p></td>
					";
				}
				echo "
						</tr>
						
						<tr>
							<td   bgcolor='".$mColors['table']."'><p  align='left'   class='nota'>Total Comanda</p></td>
							<td   bgcolor='".$mColors['table']."'><p></p></td>
							<td   bgcolor='".$mColors['table']."'><p align='right'   class='nota'>".(number_format(($umst+$ctkTums+$umsFd),2,'.',''))."</p></td>
							<td   bgcolor='".$mColors['table']."'><p align='right'   class='nota'>".(number_format(($ecost+$ctkTecos+$ecosFd),2,'.',''))."</p></td>
							<td   bgcolor='".$mColors['table']."'><p align='right'   class='nota'>".(number_format(($eurost+$ctkTeuros+$eurosFd),2,'.',''))."</p></td>
				";
				if($mParametres['moneda3Activa']['valor']==1)
				{
					echo "
							<td   bgcolor='".$mColors['table']."'><p></p></td>
					";
				}
				echo "
						</tr>
				";	
	
				$umsTotalComanda=$umst+$ctkTums+$umsFd;
				$ecosTotalComanda=$ecost+$ctkTecos+$ecosFd;
				$eurosTotalComanda=$eurost+$ctkTeuros+$eurosFd;
			
				//Total comanda (forma de pagament) :
				$umsTotalFp=$mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu'];
				$umsTotalComanda=$umst+$ctkTums+$umsFd;
				if($umsTotalFp==0)
				{
					$fpEcos=0;
					$fpEu=0;
				}
				else
				{
					$fpEcos=$umsTotalComanda*$mPars['fPagamentEcos']/$umsTotalFp;
					$fpEu=$umsTotalComanda*($umsTotalFp-$mPars['fPagamentEcos'])/$umsTotalFp;
				}

				echo "
					</table>
					</td>
				</tr>
			</table>
			</td>
		
			<td width='33%' valign='top'  bgcolor='".$mColors['table']."'>			
			<p>Import segons forma de pagament:</p>
			<table border='0' width='100%'>
				<tr>
					<td   bgcolor='".$mColors['table']."' align='left'  ><p class='nota'>Total Comanda </b></td>
					<td   bgcolor='".$mColors['table']."'><p></p></td>
					<td   bgcolor='".$mColors['table']."' align='right'><p  class='nota'><i>ums</i><br>".(number_format($fpEcos+$fpEu,2,'.',''))."</p></td>
					<td   bgcolor='".$mColors['table']."' align='right'><p  class='nota'><i>ecos</i><br>".(number_format($fpEcos,2,'.',''))."</p></td>
					<td   bgcolor='".$mColors['table']."' align='right' ><p  class='nota'><i>euros</i><br>".(number_format($fpEu,2,'.',''))."</p></td>
				";
				if($mParametres['moneda3Activa']['valor']==1)
				{
					echo "
					<td   bgcolor='".$mColors['table']."' align='right' ".$moneda3ElementsStyle."><p  class='nota'><i>".$mParametres['moneda3nom']['valor']."</i><br>".(number_format($fpEb,2,'.',''))."</p></td>
					";
				}
				echo "
				</tr>
				";
	
				//Total abonaments i c�rrecs:
				$signe=1;
				$abonamentCarrecEcosT=0;
				$abonamentCarrecEbT=0;
				$abonamentCarrecEurosT=0;
				$operacioText='';
				while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
				{
					if($mAbonamentCarrec['sel_usuari_id']==$mUsuariGrup['usuari']['id'])
					{
						if($mAbonamentCarrec['tipus']=='abonamentGrup'){$signe=-1;$operacioText='abonament';}else if ($mAbonamentCarrec['tipus']=='carrecGrup'){$signe=1;$operacioText='c�rrec';}
						$abonamentCarrecEcosT+=$mAbonamentCarrec['producte_id']*$signe;
						$abonamentCarrecEbT+=$mAbonamentCarrec['demanat']*$signe;
						$abonamentCarrecEurosT+=$mAbonamentCarrec['rebut']*$signe;
						echo "
				<tr>
					<td   bgcolor='".$mColors['table']."' align='right' ><p class='p_micro'>".$mAbonamentCarrec['data']." - ".$operacioText."</p></td>
					<th   bgcolor='".$mColors['table']."' align='right' ></th>
					<th   bgcolor='".$mColors['table']."' align='right' ></th>
					<td   bgcolor='".$mColors['table']."' align='right' ><p style='font-size:10px;'><i>".(number_format($mAbonamentCarrec['producte_id']*$signe,2,'.',''))."</i></p></td>
					<td   bgcolor='".$mColors['table']."' align='right'><p style='font-size:10px;'><i>".(number_format($mAbonamentCarrec['rebut']*$signe,2,'.',''))."</i></p></td>
						";
						if($mParametres['moneda3Activa']['valor']==1)
						{
							echo "
					<td   bgcolor='".$mColors['table']."' align='right' ".$moneda3ElementsStyle."><p class='nota'><i>".(number_format($mAbonamentCarrec['demanat']*$signe,2,'.',''))."</i></p></td>
							";
						}
						echo "
					</td>
				</tr>
						";
					}
				}
				reset($mAbonamentsCarrecs['grup']);
				echo "
				<tr>
					<td   bgcolor='".$mColors['table']."' align='left' ><p  class='nota'>Total Abonaments i C�rrecs <br>(Grup a Usuari)</p></td>
					<td   bgcolor='".$mColors['table']."'><p></p></td>
					<td   bgcolor='".$mColors['table']."' align='right' ><p  class='nota'></td>
					<td   bgcolor='".$mColors['table']."' align='right' ><p  class='nota'>".(number_format($abonamentCarrecEcosT,2,'.',''))."</p></td>
					<td   bgcolor='".$mColors['table']."' align='right' ><p  class='nota'>".(number_format($abonamentCarrecEurosT,2,'.',''))."</p></td>
				";
				if($mParametres['moneda3Activa']['valor']==1)
				{
					echo "
					<td   bgcolor='".$mColors['table']."' align='right'><p  class='nota'>".(number_format($abonamentCarrecEbT,2,'.',''))."</p></td>
					";
				}
				echo "
				</tr>
				";
	
				// Total a pagar al grup - llista local:
				echo "
				<tr>
					<td   bgcolor='".$mColors['table']."' align='left' ><p  class='nota'>Total a pagar al grup </p></td>
					<td   bgcolor='".$mColors['table']."'><p></p></td>
					<th   bgcolor='".$mColors['table']."' align='right' ><p class='nota'>".(number_format($abonamentCarrecEcosT+$abonamentCarrecEbT+$abonamentCarrecEurosT+$fpEcos+$fpEu,2,'.',''))."</p></th>
					<th   bgcolor='".$mColors['table']."' align='right' >
					<p class='nota'>".(number_format($abonamentCarrecEcosT+$fpEcos,2,'.',''))."</p>
					</th>
					
					<th   bgcolor='".$mColors['table']."' align='right' >
					<p class='nota'>".(number_format($abonamentCarrecEurosT+$fpEu,2,'.',''))."</p>
					</th>
				";
				if($mParametres['moneda3Activa']['valor']==1)
				{
					echo "
					<th   bgcolor='".$mColors['table']."' align='right'>
					<p id='p_fPagamentEb1'>".(number_format($abonamentCarrecEbT+$fpEb,2,'.',''))."</p>
					</th>
					";
				}
				echo "
				</tr>
				<tr>
					<td   bgcolor='".$mColors['table']."' align='left' ></td>
					<td   bgcolor='".$mColors['table']."'><p></p></td>
					<td   bgcolor='".$mColors['table']."' align='right' ></td>
					<td   bgcolor='".$mColors['table']."' align='right' valign='top'>
				";
				if($abonamentCarrecEcosT+$fpEcos>0)
				{
					if($mUsuariGrup['comanda'][$mPars['grup_id']]['compte_ecos']=='')
					{
						echo "
					<p class='p_micro' style='color:red'>falta<br>compte<br>ecos</p>
						";
					}
					else
					{
						echo "
					<p class='p_micro'>".$mUsuariGrup['comanda'][$mPars['grup_id']]['compte_ecos']."</p>
					<p class='p_micro'>"; if($mPars['fPagamentEcosIntegralCES']=='1'){echo 'INTEGRALCES';}else{ echo 'CES';} echo "</p>
						";
					}
				}
				echo "
					</td>
					
					<td   bgcolor='".$mColors['table']."' align='right' valign='top'>
				";
				if($abonamentCarrecEurosT+$fpEu>0)
				{
					echo "
					<p class='p_micro'>"; if($mPars['fPagamentEuTransf']=='1'){echo 'transfer�ncia';}else{ echo 'met�l.lic';} echo "</p>
					";
				}
				echo "
					</td>
				";
				if($mParametres['moneda3Activa']['valor']==1)
				{
					echo "
					<td   bgcolor='".$mColors['table']."' align='right' valign='top'>
					";
					if($abonamentCarrecEbT+$fpEb>0)	
					{	
						if($mUsuariGrup['comanda'][$mPars['grup_id']]['compte_cieb']=='')
						{
							echo "
					<p class='p_micro' style='color:red'>falta<br>compte<br>".$mParametres['moneda3nom']['valor']."</p>
							";
						}
						else
						{
							echo "
					<p class='p_micro'>".$mUsuariGrup['comanda'][$mPars['grup_id']]['compte_cieb']."</p>
							";
						}
					}
					echo "
					</td>
					";
				}
				echo "
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<br>
				";
				$numComandes++;
			}
		}
		$mPars['selUsuariId']=$selUsuariId_mem;
		echo "
	<br><br><br>&nbsp;
		";
	return;
}
//------------------------------------------------------------------------------
function html_mostrarNotesAlbara()
{
	echo "
	<table style='width:70%;' align='center'>
		<tr>
			<td style='width:100%;' align='center'>
			<p class='nota2'><u>Condicions:</u></p>
			<p class='nota2'>* Etiqueta especial '<b>jjaa</b>':
			<br> - Els productes de la llista de la CAC amb aquesta etiqueta, com el vi del Mas Itinerant, 
			nom�s es distribu�ran a les jornades assemblearies de la CIC.
			Aquest productes tenen un cost de transport zero a la llista de productes de la CAC perqu� no
			 es la CAC qui assumeix la distribuci� si no el productor, o el Mas Itinerant en el cas del vi. 
			Per tant, el cost de transport est� ja incl�s en el preu i la CAC no el cobrar�.
			<br><br>- El tancament de les reserves dels productes jjaa es produir� de forma autom�tica dos dies abans de les jjaa
			<br>
			<p class='nota2'>** <b>Distribuci� de vi pel Mas Itinerant</b>:
			<br>- El preu del vi s'indica en el nom del producte. 
			<br>- Excepcionalment,la CAC no gestionar� el cobrament del vi reservat i distribuit des del gestor
			 de comandes durant l'<b>abril 2015</b>. El motiu es que no s'ha decidit en assemblea permanent que la
			  CAC recolzi en euros la part en ecos d'aquest producte amb un alt % d'alcohol, com es propi del vi.
			<br> Per tant <b> es Mas Itinerant</b> qui s'ocupar� de fer els cobraments tant de la part en euros
			 com de la part en ecos. El pagament de la part en euros cal fer-lo en met�l.lic en el moment de
			  l'entrega, a les jjaa d'abril. Per la part en ecos faciliteu el vostre nombre en ecos de de compte
			   al Mas Itinerant perqu� us faci el c�rrec corresponent. 
			Aix� doncs, de moment, el vi no es pot pagar 100% en ecos.
			</p>
			</td>
		</tr>
	</table>
	";

	return;
}


?>

		