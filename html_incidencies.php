<?php
//------------------------------------------------------------------------------
function html_mostrarFiltres()
{
	global  $opt2,
			$gRef,
			$colorLlista,
			$mColors,
			$mUsuarisRef,
			$mUsuarisGrupRef,
			$mPars,
			$mRutesSufixes,
			$mRebostsRef,
			$mProductes,
			$mPropietatsPeriodesLocals,
			$mPeriodesLocalsInfo,
			$mSelUsuarisIncd,
			$mUsuarisIncd,
			$mTipusIncd,
			$mEstatsIncd,
//*v36.4-2 declaracions global
			$mSzones,
			$mZones;
	
	
	$stylePeriodeLocal='';
	$styleUsuari='';
	$styleSelUsuari='';
	$styleTipus='';
	$styleProducteId='';
	$styleEstat='';
	$styleRuta='';
	$styleGrup='';
	$styleLlista='';
//*v36.4-2 declaracions
	$styleSzona='';
	$styleZona='';
	$styleLlista='';
	

	if(isset($mPars['vPeriodeLocalIncd']) && $mPars['vPeriodeLocalIncd']!='TOTS'){$stylePeriodeLocal=" style='background-color:#47AFFF;' ";}
	if(isset($mPars['vUsuariIncd']) && $mPars['vUsuariIncd']!='TOTS'){$styleUsuari=" style='background-color:#47AFFF;' ";}
	if(isset($mPars['vSelUsuariIncd']) && $mPars['vSelUsuariIncd']!='TOTS'){$styleSelUsuari=" style='background-color:#47AFFF;' ";}
	if(isset($mPars['vTipusIncd']) && $mPars['vTipusIncd']!='TOTS'){$styleTipus=" style='background-color:#47AFFF;' ";}
	if(isset($mPars['vProducteIdIncd']) && $mPars['vProducteIdIncd']!='TOTS'){$styleProducteId=" style='background-color:#47AFFF;' ";}
	if(isset($mPars['vEstatIncd']) && $mPars['vEstatIncd']!='TOTS'){$styleEstat=" style='background-color:#47AFFF;' ";}
	if(isset($mPars['vRutaIncd']) && $mPars['vRutaIncd']!='TOTS'){$styleRuta=" style='background-color:#F9E39F;' ";}
	if(isset($mPars['vGrupIncd']) && $mPars['vGrupIncd']!='TOTS'){$styleGrup=" style='background-color:#47AFFF;' ";}
	if(isset($mPars['vLlistaIncd']) && $mPars['vLlistaIncd']!=''){$styleLlista=" style='background-color:#F9E39F;' ";}
//*v36.4-2 assignacions condicionals
	if(isset($mPars['vSzonaIncd']) && $mPars['vSzonaIncd']!='TOTS'){$styleSzona=" style='background-color:#47AFFF;' ";}
	if(isset($mPars['vZonaIncd']) && $mPars['vZonaIncd']!='TOTS'){$styleZona=" style='background-color:#47AFFF;' ";}
	if(isset($mPars['vLlistaIncd']) && $mPars['vLlistaIncd']!='TOTS'){$styleLlista=" style='background-color:#47AFFF;' ";}

if(!isset($mPars['selLlistaId']) || $mPars['selLlistaId']='' || $mPars['selLlistaId']!='0')
{
	$visibilityPeriodeLocal='inherit';
	$positionPeriodeLocal='relative';
	$widthPeriodeLocal='0%';
	$visibilityRuta='hidden';
	$positionRuta='absolute';
	$widthRuta='0%';
	$visibilitySzona='hidden';
	$positionSzona='absolute';
	$widthSzona='0%';
	$mTextEstatPeriode=array();
	$mTextEstatPeriode[0]='';
	$mTextEstatPeriode[1]='TANCAT';
	$mTextEstatPeriode[-1]='OBERT';
	
}
else
{
	$visibilityPeriodeLocal='hidden';
	$positionPeriodeLocal='absolute';
	$widthPeriodeLocal='11.1%';
	$visibilityRuta='inherit';
	$positionRuta='relative';
	$widthRuta='11.1%';
	$visibilitySzona='inherit';
	$positionSzona='relative';
	$widthSzona='11.1%';
	$mTextEstatPeriode=array();
}

	echo "	
		<table width='100%' align='center'>
			<tr>
				<td align='left'>
				<p class='compacte' >
				Usuari:<b> ".(urldecode($mPars['usuari']))."</b> (".$mPars['nivell'].")
				</p>
				</td>
				<td align='right'>
				<p class='compacte' >
				<p onClick=\"javascript:enviarFpars('cvs_incidencies.php','_blank');\" style='cursor:pointer;'><u>exportar .cvs de la vista actual</u></p>
				</p>
				</td>
			</tr>
		</table>

		<table  bgcolor='".$mColors['incidencies.php'][$colorLlista]['t1_bg']."' width='100%' align='center'>
";
//*v36.4- totes files taula
echo "
			<tr>
				<td align='left' valign='top'>
				<p>Veure nom�s incid�ncies...</p>
				</td>

				<td align='left' valign='top'>
				<p></p>
				</td>

				<td align='left' valign='top'>
				<p></p>
				</td>

				<td align='left' valign='top'>
				<p></p>
				</td>

				<td align='left' valign='top'>
				<p></p>
				</td>

			</tr>
				
			<tr>
				<td align='left' valign='top' style='width:".$widthPeriodeLocal."; visibility:".$visibilityPeriodeLocal."; z-index:0px, top:0px; position:".$positionPeriodeLocal.";' >
				<p>
				del periode local:
				<br> 
				<select id='sel_vPeriodeLocal' ".$stylePeriodeLocal." onChange=\"javascript: enviarFpars('incidencies.php?opt2=".$opt2."&gRef=".$gRef."&vPeriodeLocal='+this.value,'_self');\">
		";
		$selected='';
		$selected2='selected';
		while(list($id,$mPeriodeLocal)=each($mPeriodesLocalsInfo))
		{
			if($mPars['vPeriodeLocalIncd']==$id)
			{
				$selected='selected';$selected2='';}else{$selected='';
			}
			echo "
				<option ".$selected." value='".$id."'>".@$mPeriodesLocalsInfo[$id]['periode_comanda']." (".$id.") (".@$mTextEstatPeriode[$mPropietatsPeriodesLocals[$id]['comandesLocalsTancades']].")</option>
			";
		}
		reset($mPeriodesLocalsInfo);
		echo "
				</select>
				</p>		
				</td>

				<td align='left' valign='top' style='width:".$widthRuta."; visibility:".$visibilityRuta."; z-index:0px, top:0px; position:".$positionRuta.";' >
				<p>
				de la ruta:
				<br> 
				<select id='sel_vRuta' ".$styleRuta." onChange=\"javascript: enviarFpars('incidencies.php?opt2=".$opt2."&gRef=".$gRef."&vRuta='+this.value,'_self');\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$rutaSufixe)=each($mRutesSufixes))
		{
			if($mPars['vRutaIncd']==$rutaSufixe)
			{
				$selected='selected';$selected2='';}else{$selected='';
			}
			echo "
				<option ".$selected." value='".$rutaSufixe."'>".$rutaSufixe."</option>
			";
		}
		reset($mRutesSufixes);
		echo "
				</select>
				</p>		
				</td>

";
//*v36.4-columnes de sz i z

echo "
				<td align='left' valign='top' style='width:".$widthSzona."; visibility:".$visibilitySzona."; z-index:0px, top:0px; position:".$positionSzona.";' >
				<p>
				de la SuperZona:
				<br> 
				<select id='sel_vSzona' ".$styleSzona." onChange=\"javascript: enviarFpars('incidencies.php?gRef=".$gRef."&vSzona='+this.value+'&vZona=TOTS','_self');\">
		";
			$selected='';
			$selected2='selected';
			while(list($index,$sZona)=each($mSzones))
			{
				if(@$mPars['vSzonaIncd']==$sZona)
				{
					$selected='selected';$selected2='';}else{$selected='';
				}
					
				echo "
				<option ".$selected." value='".$sZona."'>".$sZona."</option>
				";
			}
			reset($mSzones);
			echo "
				<option ".$selected2." value='TOTS'>- totes les SuperZones - </option>
				</select>
				<br>
				de la Zona:
				<br> 
				<select id='sel_vZona' ".$styleZona." onChange=\"javascript: enviarFpars('incidencies.php?gRef=".$gRef."&vZona='+this.value+'&vSzona=TOTS','_self');\">
		";
			$selected='';
			$selected2='selected';
			while(list($index,$zona)=each($mZones))
			{
				if(@$mPars['vZonaIncd']==$zona)
				{
					$selected='selected';$selected2='';}else{$selected='';
				}
					
				echo "
				<option ".$selected." value='".$zona."'>".$zona."</option>
				";
			}
			reset($mZones);
			echo "
				<option ".$selected2." value='TOTS'>- totes les Zones - </option>
				</select>
				</p>		
				</td>
				
				<td align='left' valign='top'>
				<p>
				del Grup:
				<br>
				<select id='sel_vGrup' ".$styleGrup." onChange=\"javascript: enviarFpars('incidencies.php?opt2=".$opt2."&gRef=".$gRef."&vGrup='+this.value,'_self');\">
		";
		if(!isset($gRef) || $gRef=='' || $gRef=='TOTS' || $gRef=='0')
		{
			$selected='';
			$selected2='selected';
			while(list($grupId,$mRebost)=each($mRebostsRef))
			{
				if($grupId!='0')
				{
					if(@$mPars['vGrupIncd']==$grupId)
					{
						$selected='selected';$selected2='';}else{$selected='';
					}
					
					echo "
				<option ".$selected." value='".$grupId."'>".(urldecode($mRebost['nom']))."</option>
					";
				}
			}
			reset($mRebostsRef);
			echo "
				<option ".$selected2." value='TOTS'>- tots els grups - </option>
				";
			}
			else
			{
					echo "
				<option selected value='".$gRef."'>".(urldecode($mRebostsRef[$gRef]['nom']))."</option>
					";
			}
			echo "
				</select>
				<br>
				De la Llista:
				<br>
				<select id='sel_vLlista' ".$styleLlista." onChange=\"javascript: enviarFpars('incidencies.php?opt2=".$opt2."&gRef=".$gRef."&vLlista='+this.value,'_self');\">
		";
		if(!isset($mPars['vLlistaIncd']) || $mPars['vLlistaIncd']=='' || $mPars['vLlistaIncd']=='0')
		{
			$mPars['vLlistaIncd']=0;
			$selected1='selected';
			$selected2='';
		}
		else
		{
			$mPars['vLlistaIncd']=$gRef;
			$selected1='';
			$selected2='selected';
		}
			echo "
				<option ".$selected1." value='0'>CAC</option>
				<option ".$selected2." value='".$gRef."'>LOCAL</option>
				</select>
				</td>
				
				<td align='left' valign='top' >
				<p> de l'autor:
				<br>
				<select id='sel_vUsuari' ".$styleUsuari." onChange=\"javascript: enviarFpars('incidencies.php?opt2=".$opt2."&gRef=".$gRef."&vUsuari='+this.value,'_self');\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mUsuariIncd)=each($mUsuarisIncd))
		{
			if(isset($mPars['vSelUsuariIncd']) && @$mPars['vUsuariIncd']!='0' && @$mPars['vUsuariIncd']==$mUsuariIncd['usuari_id']){$selected='selected';$selected2='';}else{$selected='';}
			if($mUsuariIncd['usuari_id']!='0')
			{
				echo "
				<option ".$selected." value='".$mUsuariIncd['usuari_id']."'>".(urldecode($mUsuarisRef[$mUsuariIncd['usuari_id']]['usuari']))."</option>
				";
			}
		}
		reset($mUsuarisIncd);
		echo "
				<option ".$selected2." value='TOTS'>- tots els autors -</option>
				</select>
				
				<br>
				de l'usuari:
				<br>
				<select id='sel_vSelUsuari' ".$styleSelUsuari." onChange=\"javascript: enviarFpars('incidencies.php?opt2=".$opt2."&gRef=".$gRef."&vSelUsuari='+this.value,'_self');\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mSelUsuariIncd)=each($mSelUsuarisIncd))
		{
			if(@$mPars['vSelUsuariIncd']!='0' && @$mPars['vSelUsuariIncd']==$mSelUsuariIncd['sel_usuari_id'])
			{
				$selected='selected';$selected2='';}else{$selected='';
			}
			if($mSelUsuariIncd['sel_usuari_id']!='' && $mSelUsuariIncd['sel_usuari_id']!='0' && array_key_exists($mSelUsuariIncd['sel_usuari_id'],$mUsuarisGrupRef))
			{
				echo "
				<option ".$selected." value='".$mSelUsuariIncd['sel_usuari_id']."'>".(urldecode($mUsuarisRef[$mSelUsuariIncd['sel_usuari_id']]['usuari']))."</option>
				";
			}
		}
		reset($mSelUsuarisIncd);
		echo "
				<option ".$selected2." value='TOTS'>- tots els usuaris -</option>
				</select>
				<br>
				del tipus:
				<br>
				<select id='sel_vTipus' ".$styleTipus." onChange=\"javascript: enviarFpars('incidencies.php?opt2=".$opt2."&gRef=".$gRef."&vTipus='+this.value,'_self');\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mTipus)=each($mTipusIncd))
		{
			if(@$mPars['vTipusIncd']==$mTipus['tipus']){$selected='selected';$selected2='';}else{$selected='';}
			if($opt2=='incd')
			{
				if($mTipus['tipus']!='abonamentCAC' && $mTipus['tipus']!='abonamentGrup' && $mTipus['tipus']!='carrecCAC' && $mTipus['tipus']!='carrecGrup')
				{
					echo "
				<option ".$selected." value='".$mTipus['tipus']."'>".$mTipus['tipus']."</option>
					";
				}
			}
			else if($opt2=='abCa')
			{
				if($mTipus['tipus']=='abonamentCAC' || $mTipus['tipus']=='abonamentGrup' || $mTipus['tipus']=='carrecCAC' || $mTipus['tipus']=='carrecGrup')
				{
					echo "
				<option ".$selected." value='".$mTipus['tipus']."'>".$mTipus['tipus']."</option>
					";
				}
			}
		}
		echo "
				<option ".$selected2." value='TOTS'>- tots els tipus -</option>
		";
		reset($mTipusIncd);
		echo "
				</select>
				</p>		
				</td>

				<td align='left' valign='top'>
				<p>
				del producte ID:
				<br> 
				<select id='sel_vProducteId' ".$styleProducteId." onChange=\"javascript: enviarFpars('incidencies.php?opt2=".$opt2."&gRef=".$gRef."&vProducteId='+this.value,'_self');\">
		";
		$selected='';
		$selected2='selected';
		ksort($mProductes,SORT_REGULAR);
		while(list($key,$mProducte)=each($mProductes))
		{
			if(@$mPars['vProducteIdIncd']==$mProducte['id']){$selected='selected';$selected2='';}else{$selected='';}
			$producte_=urldecode($mProducte['producte']);
			$producte_=substr($producte_,0,30);
			echo "
				<option ".$selected." value='".$mProducte['id']."'>".$mProducte['id']." - ".$producte_."...</option>
			";
		}
		reset($mProductes);
		echo "
				<option ".$selected2." value='TOTS'>- tots els productes -</option>
				</select>
				</p>		
				</td>

				<td align='left' valign='top'>
				<p>
				amb 'estat':
				<br> 
				<select id='sel_vEstat' ".$styleEstat." onChange=\"javascript: enviarFpars('incidencies.php?opt2=".$opt2."&gRef=".$gRef."&vEstat='+this.value,'_self');\">
		";
		$selected='';
		$selected2='selected';
		
		while(list($key,$estat)=each($mEstatsIncd))
		{
			if($mPars['vEstatIncd']==$estat){$selected='selected';$selected2='';}else{$selected='';}
			if($opt2=='incd')
			{
				if($estat!='aplicat' && $estat!='noAplicat')
				{
					echo "
				<option ".$selected." value='".$estat."'>".$estat."</option>
					";
				}
			}
			else if($opt2=='abCa')
			{
				if($estat=='aplicat' || $estat=='noAplicat')
				{
					echo "
				<option ".$selected." value='".$estat."'>".$estat."</option>
					";
				}
			}
		}
		echo "
				<option ".$selected2." value='TOTS'>- tots els estats -</option>
		";
		reset($mEstatsIncd);
		echo "
				</select>
				</p>		
				</td>
			</tr>
		</table>
		";

	return;
}


//------------------------------------------------------------------------------

function html_mostrarTaules($db)
{
	global $opt2,$mUsuarisRef,$mParametres,$mRutesSufixes,$precomandaTancada,$parsChain,$ruta,$mRutes,$mTipusIncidencies,$mPars,$mProductes,$mRebostsRef,$mRebost,$mIncidencies,$mContinguts;
	$class=" class='p.albara' ";

	$mTextTipusOperacio['incd'][0]='ID<br>producte';
	$mTextTipusOperacio['incd'][1]='producte';
	$mTextTipusOperacio['incd'][2]='demanat';
	$mTextTipusOperacio['incd'][3]='rebut';
	$mTextTipusOperacio['abCa'][0]='ecos';
	$mTextTipusOperacio['abCa'][1]='';
	$mTextTipusOperacio['abCa'][2]='ecob�sics';
	$mTextTipusOperacio['abCa'][3]='euros';
	
	echo "
<table align='center' border='0' id='t_registreIncidencies' width='100%'>
	<tr>
		<td align='center' width='100%'>
		<table bgcolor='#cccccc' align='center' width='100%'>
			<tr>
				<th bgcolor='#ffffff'>
				<p>id</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>data</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>ruta</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>autor</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>usuari</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>grup</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>llista</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>tipus<br>incid�ncia</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>".$mTextTipusOperacio[$opt2][0]."</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>".$mTextTipusOperacio[$opt2][1]."</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>".$mTextTipusOperacio[$opt2][2]."</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>".$mTextTipusOperacio[$opt2][3]."</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>indicacions</p>
				</th>
				<th bgcolor='#ffffff'>
				<p>estat</p>
				</th>
				<th bgcolor='#ffffff'>
				</th>
			</tr>
		";
		$numBotonsEditar=0;
		while(list($key,$mIncidencia)=each($mIncidencies))
		{
			
			echo "
			<tr>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['id']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['data']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['ruta']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".(@urldecode($mUsuarisRef[$mIncidencia['usuari_id']]['usuari']))."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">"; if($mIncidencia['sel_usuari_id']==0){echo 'GRUP';}else{echo @urldecode($mUsuarisRef[$mIncidencia['sel_usuari_id']]['usuari']);} echo "</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".(urldecode($mRebostsRef[$mIncidencia['grup_id']]['nom']))."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".(urldecode($mRebostsRef[$mIncidencia['llista_id']]['nom']))."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['tipus']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['producte_id']."</p>
				</td>
	";
	if($opt2=='incd' && $mIncidencia['producte_id']!='')
	{
		echo "
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".(@urldecode($mProductes[$mIncidencia['producte_id']]['producte']))."</p>
				</td>
		";
	}
	else
	{
		echo "
				<td bgcolor='#ffffff' valign='top' align='left'>
				</td>
		";
	}
		echo "
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['demanat']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class.">".$mIncidencia['rebut']."</p>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				";
				if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
				{
					echo " 
				<input class='i_micro' type='button' onClick=\"javascript:cursor=getMouse(event); mostrarAfegirComentari(cursor.x,cursor.y,'".$mIncidencia['id']."');\" value=\"afegir comentari [coord]\">
					";
				}
				
				echo "
				<div id='p_comentaris_".$numBotonsEditar."'  ".$class."><p>".(@urldecode($mIncidencia['comentaris']))."</p></div>
				<form id='f_guardarIncidencia_".$numBotonsEditar."' name='f_guardarIncidencia_".$numBotonsEditar."' method='post'  target='_self' action=''>
				<textArea style='visibility:hidden; position:absolute;' id='ta_comentaris_".$numBotonsEditar."' name='ta_comentaris_".$numBotonsEditar."' cols='40' rows='5'></textArea>
				<input type='hidden' id='i_pars_".$numBotonsEditar."' name='i_pars' value=''>
				</form>
				</td>
				<td bgcolor='#ffffff' valign='top' align='left'>
				<p ".$class. " style='"; if($mIncidencia['estat']=='pendent'){echo "color:#ff0000;";} echo "'>".$mIncidencia['estat']."</p>
				</td>
				
				<td bgcolor='#ffffff' align='left' valign='middle'>
			";
				
			if
			(
				$mIncidencia['estat']=='pendent'
				&& 
				(
					$mPars['usuari_id']==$mIncidencia['usuari_id'] 
					|| 
					$mPars['nivell']=='sadmin' 
					|| 
					$mPars['nivell']=='admin'
				)
			)
			{
				echo "
				<input type='button'  class='i_botoEditar' id='i_botoEditarR_".$numBotonsEditar."' onClick=\"javascript: eliminarIncidencia('".$mIncidencia['id']."');\" value='resoldre'>
				";
			}
			else if
			( 
				$mIncidencia['estat']=='aplicat'
				&& 
				(
					$mPars['usuari_id']==$mIncidencia['usuari_id']
					|| 
					$mPars['nivell']=='sadmin' 
					|| 
					$mPars['nivell']=='admin'
				)
			)
			{
				echo "
				<input type='button'  class='i_botoEditar' id='i_botoEditarR_".$numBotonsEditar."' onClick=\"javascript: noAplicarIncidencia('".$mIncidencia['id']."');\" value='no aplicar'>
				";
			}
			else if
			( 
				$mIncidencia['estat']=='noAplicat' 
				&& 
				(
					$mPars['usuari_id']==$mIncidencia['usuari_id']
					|| 
					$mPars['nivell']=='sadmin' 
					|| 
					$mPars['nivell']=='admin'
				)
			)
			{
				echo "
				<input type='button'  class='i_botoEditar' id='i_botoEditarR_".$numBotonsEditar."' onClick=\"javascript: aplicarIncidencia('".$mIncidencia['id']."');\" value='aplicar'>
				";
			}
				$numBotonsEditar++;
				echo "
				</td>
			</tr>			
				";
		}
		reset($mIncidencies);
			
			echo "
		</table>
		</form>
		<script>numBotonsEditar=".$numBotonsEditar.";</script>
		</td>
	</tr>
</table>
	";
	return;
}


//------------------------------------------------------------------------------
function html_recipient()
{
	global $mPars,$parsChain,$opt2;

	echo "
	<div id='d_recipient' style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
		<div>
				<form id='f_afegirComentariIncd' name='f_afegirComentariIncd' method='POST' action='incidencies.php?opt=afegirComentariIncd&opt2=".$opt2."' target='_self'>
		<table id='t_recipient' width='500px' height='200px'  >
			<tr>
				<td width='100%' height='100%'  bgcolor='LightBlue'>
				<table width='100%'  height='100%'  >
					<tr>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>		
						<td width='90%' align='center'>
						<br>
						<p class='p_micro' >[ Afegir comentari ]</p>
						</td>
						<td width='5%'>
						</td>
					</tr>
					<tr>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>
						<td >
						<table width='90%'>
						<tr>
						<td>
						<p class='p_micro'>ID incid�ncia: </p>
						</td>
						<th>
						<input type='text' READONLY size='5' class='i_micro' id='i_incidenciaId' name='i_incidenciaId' value=''>
						</th>
						<td>
						<p class='p_micro'>Autor: </p>
						</td>
						<th>
						<p class='p_micro' id='p_autor'></p>
						</th>
						<td>
						<p class='p_micro' id='p_nivell'></p>
						</td>
						<td align='right'>
						<input type='submit' class='i_micro'  value='guardar'>
						<input type='hidden' name='i_pars' value='".$parsChain."'>
						</td>
						</tr>
						</table>
						<textArea id='ta_comentariIncd' name='ta_comentariIncd' cols='55' rows='8'></textArea>
						</td>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>
						<td width='90%'>
						<p>&nbsp;</p>
						</td>
						<td width='5%'>
						<p>&nbsp;</p>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
				</form>
		</div>
	</div>
	";

	return ;
}

?>

		