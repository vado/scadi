<?php
//------------------------------------------------------------------------------

function html_menuProductes()
{
	global 	$parsChain,
			$thumbCode,
			$mColors,
			$mPars,
			$mProductes,
			$mProducte,
			$mGrupsAssociatsRef,
			$mGrupsRef,
			$mGrup,
			$mLlistesRef,
			$mAjuda,
			$mUsuari,
			$mOpcionsOrdreActivacioRapida,
			$mParametres,
			$mPerfil,
			$numProductesLlista,
			$mPropietatsPeriodesLocals,
			$modificarValorsImportantsProductes,
			$db;

	echo "
	
	<table width='80%' border='0' align='center' bgcolor='green'>
		<tr>
	";
	if
	(
		(
			$mPars['selLlistaId']==0
			&&
			(
				(
					(
						$mPars['selRutaSufixPeriode']==date('ym')
						&&
						$modificarValorsImportantsProductes
					)
					|| 
					$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
				)
				&& 
				(
					$mUsuari['id']==$mPerfil['usuari_id']
					||
					($mPars['nivell']=='sadmin' ||	$mPars['nivell']=='admin')
				)
			)
		)
		||
		(
			$mPars['selLlistaId']!=0
			&&
			(
				($mPars['nivell']=='sadmin'  || $mPars['nivell']=='admin')
				|| 
				$mPars['usuari_id']==$mGrup['usuari_id']
				||
				(
					$mPars['perfil_id']!=0
					&&
					$mPerfil['usuari_id']==$mPars['usuari_id'] //responsable del grup/llista
				)
			)
		)
	)
	{
	
		echo "
			<td align='center' width='15%'>
			<table>
				<tr>
					<td>
					<p style='cursor:pointer; font-size:10px; color:white;' onClick=\"javascript: cursor=getMouse(event);mostrarFormAccio(cursor.x,cursor.y,'activacioRapida');\" >&nbsp;&nbsp;Activaci� r�pida&nbsp;&nbsp;
					".(html_ajuda1('html_gestioProductes.php',5))."
					</p>
					</td>
				</tr>
				<tr>
					<td>
					<select id='sel_ordreAr' name='sel_ordreAr' style='font-size:10px; background-color:orange;' onChange=\"javascript:reordenarActivacioRapida(this.value);\">
					";
					while(list($key,$text)=each($mOpcionsOrdreActivacioRapida))
					{
						if($mPars['ordreAr']==$key){$selected='selected';$class="class='o_selected'";}else{$selected='';$class="";}
						echo "
					<option ".$selected." ".$class." value='".$key."'>".$text."</option>
						";
					}
					reset($mOpcionsOrdreActivacioRapida);
					echo "
					</select>
					</td>
				</tr>
			</table>
			</td>
		";
		if
		(
			isset($mProducte['id'])
			&&
			$mProducte['id']!=''
			&&
			$mProducte['id']!=0
			&&
			(
				$mUsuari['id']==$mPerfil['usuari_id']
				||	
				($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
			)
		)	
		{
			echo "
			<td align='center' width='15%'>
			<table>
				<tr>
					<td>
					<p style='cursor:pointer; font-size:10px; color:white;' onClick=\"javascript: cursor=getMouse(event);mostrarFormAccio(cursor.x,cursor.y,'segellEcocicProducte');\" >&nbsp;&nbsp;Segell ECOCIC<br>del producte&nbsp;&nbsp;</p>
					</td>
					
					<td valign='bottom'>
					".(html_ajuda1('html_gestioProductes.php',6))."
					</td>
				</tr>
			</table>
			</td>
			";
		}

		if
		(
			($mPars['nivell']=='sadmin'  || $mPars['nivell']=='admin')
			|| 
			(
				$mPars['perfil_id']!=0
				&&
				$mPerfil['usuari_id']==$mPars['usuari_id'] //responsable del grup/llista
			)
		)
		{
			echo "
			<td align='center' width='15%'>
			<table>
				<tr>
					<td>
					<p style='cursor:pointer; font-size:10px; color:white;' onClick=\"javascript: cursor=getMouse(event);mostrarFormAccio(cursor.x,cursor.y,'associarPerfilAgrup');\" >&nbsp;&nbsp;Associar el perfil<br>a un grup&nbsp;&nbsp;</p>
					</td>
					
					<td valign='bottom'>
					".(html_ajuda1('html_gestioProductes.php',2))."
					</td>
				</tr>
			</table>
			</td>

			<td align='center' width='15%'>
			<table>
				<tr>
					<td>
					<p style='cursor:pointer; font-size:10px; color:white;' onClick=\"javascript: cursor=getMouse(event);mostrarFormAccio(cursor.x,cursor.y,'transferirFitxes');\" >&nbsp;&nbsp;Transferir fitxes<br>a una llista&nbsp;&nbsp;</p>
					</td>
					
					<td valign='bottom'>
					".(html_ajuda1('html_gestioProductes.php',3))."
					</td>
				</tr>
			</table>
			</td>
			";
		}

		if
		(
			($mPars['nivell']=='sadmin'	||	$mPars['nivell']=='admin' || $mPars['nivell']=='coord')
			||
			(
				isset($mPars['grup_id'])
				&&
				$mPars['grup_id']!=0
				&&
				$mPars['grup_id']!=''
				&&
				$mGrupsRef[$mPars['grup_id']]['usuari_id']==$mPars['usuari_id'] //responsable del grup/llista
			)
			||
			(
				$mPars['selLlistaId']!=0
				&&
				$mPerfil['usuari_id']==$mPars['usuari_id']
			)
		)
		{
			echo "
			<td align='center' width='15%'>
			<table>
				<tr>
					<td>
					<p style='cursor:pointer; font-size:10px; color:white;' onClick=\"javascript: cursor=getMouse(event);mostrarFormAccio(cursor.x,cursor.y,'peticioNovesPlantilles');\" >&nbsp;&nbsp;Sol.licitar noves<br>plantilles&nbsp;&nbsp;</p>
					</td>
					
					<td valign='bottom'>
					".(html_ajuda1('html_gestioProductes.php',1))."
					</td>
				</tr>
			</table>
			</td>
			";
		}
	}

	if
	(
		isset($mProducte['id']) && $mProducte['id']!='' && $mProducte['id']!=0
		&&
		(
			(
				$mPars['selLlistaId']==0
				&&
				(
					(
						$mPars['selRutaSufixPeriode']==date('ym')
						&&
						$modificarValorsImportantsProductes
					)
					||
					$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
				)
				&&
				$mUsuari['id']==$mPerfil['usuari_id']
			)
			||
			(
				$mPars['selLlistaId']!=0
				&&
				$mUsuari['id']==$mPerfil['usuari_id']
			)
			||
			($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
		)

	)
	{
		echo "
			<td align='center' width='20%'>
			<table>
				<tr>
					<td>
		";
		if($mPars['selLlistaId']==0 && $mProducte['imatge']!='')
		{
			echo "
					<img  src='productes/".$mProducte['id']."/".$thumbCode.'_'.$mProducte['imatge']."'>
			";
		}
		else if( $mProducte['imatge']!='')
		{
			echo "
			
					<img  src='llistes/".$mPars['selLlistaId']."/productes/".$mProducte['id'].'/'.$thumbCode.'_'.$mProducte['imatge']."'>
			";
		}
		echo "
					</td>
					<td>
		";
		if($mPars['demo']==-1)
		{
			echo "
					<table>
						<tr>
							<td>
							<p style='cursor:pointer; font-size:10px; color:white;' onClick=\"javascript: cursor=getMouse(event);mostrarFormAccio(cursor.x,cursor.y,'pujarImatge');\" >&nbsp;&nbsp;Pujar una<br>imatge&nbsp;&nbsp;</p>
							</td>
					
							<td valign='bottom'>
							".(html_ajuda1('html_gestioProductes.php',8))."
							</td>
						</tr>
					</table>
			";
		}
		echo "
					</td>
				</tr>
			</table>
			</td>
		";
	}
		echo "
 
 			<td align='left'>
			</td>
		</tr>
	</table>


	<table width='80%' border='1' align='center'  bgcolor='white'>
		<tr>
			<td width='33%' align='center' valign='top'>
			<br>
			<p>Editar Producte:</p>
			<select name='sel_producteId' onChange=\"javascript:if(this.value!=''){enviarFpars('gestioProductes.php?plId=".$mPars['selPerfilRef']."&pId='+this.value,'_self');}\">
	";
	$selected1='';
	$selected2='selected';
	while(list($key,$mProducte_)=each($mProductes))
	{
		if($mProducte_['id']==@$mProducte['id'])
		{
			$selected1='selected';$selected2='';}else{$selected1='';
		}
		echo "
			<option "; 
		if($mProducte_['actiu']==1)
		{
			echo " class='oGrupActiu' ";
		} 	
		else if($mProducte_['actiu']==0)
		{
			echo " class='oGrupInactiu' ";
		} 
		echo $selected1."  value=\"".$mProducte_['id']."\">".(urldecode($mProducte_['id']." - ".$mProducte_['producte']))."</option>
		";					
	}
	reset($mProductes);
	echo "
			<option ".$selected2." value=''></option>
			</select>
			<p style='font-size:10px;'>
			(* en <font color='green'><b>verd</b></font> els productes actius a la llista)
			</p>
			<br>
			<br>
			</td>

			<td width='33%' align='center' valign='top'>
			<br>
	";
		if
		(
			(
				($mPars['nivell']=='sadmin'  || $mPars['nivell']=='admin')
				|| 
				@$mGrupsRef[$mPars['selLlistaId']]['usuari_id']==$mPars['usuari_id'] //responsable del grup/llista
			)
		)
		{
			if($mPars['selLlistaId']!=0)
			{
				if(	$numProductesLlista>=$mParametres['limitNombreProductesLocals']['valor'])
				{
					echo "
				<table width='80%' align='center'>
					<tr>
						<td  width='100%' >
						<p style='color:DarkOrange; text-align:justify;'>S'ha arribat al limit de productes que es poden crear en aquesta llista.<br> No es poden crear m�s productes. <br>Pots revisar els diferents perfils de productor i eliminar productes que no s'estiguin utilitzant.
						<br><br>[ total en llista local: <b>".$numProductesLlista."</b> / ".$mParametres['limitNombreProductesLocals']['valor']." productes]
						</p>
						</td>
					</tr>
				</table>
					";
				}
				else
				{
					if(count($mPropietatsPeriodesLocals)>0)
					{			
						echo "
			<p class='compacte' onClick=\"javascript: enviarFpars('gestioProductes.php?opt=nou','_self');\" 	style='color:#885500; cursor:pointer;'><u>crear nou producte</u></p>
			<p>[ total en llista local: ".$numProductesLlista." / ".$mParametres['limitNombreProductesLocals']['valor']." productes]</p>			
						";
					}
					else
					{			
						echo "
			<p class='pAlertaNo4' >Atenci�: encara no s'ha creat cap periode <br>de reserves en aquesta llista local</p>
						";
					}
				}
			}
			else
			{
				if($mPars['nivell']=='sadmin'  || $mPars['nivell']=='admin')
				{			
					echo "
			<p class='compacte' onClick=\"javascript: enviarFpars('gestioProductes.php?opt=nou','_self');\" 	style='color:#885500; cursor:pointer;'><u>crear nou producte</u></p>
			<p>[ total en llista local: ".$numProductesLlista." / ".$mParametres['limitNombreProductesLocals']['valor']." productes]</p>			
					";
				}
			}
		}
				
			
	
	echo "
			</td>

			<td width='33%' align='center' valign='top'>
			</td>
		</tr>
	</table>

	";

	return;
}

//------------------------------------------------------------------------------
function html_mostrarPropietatsProducte($db)
{
	global $mPars,$mProducte;

	$propietats=@substr($mProducte['historial'],1);
	$propietats=substr($propietats,0,strlen($propietats)-1);
	$mHistorial=array();
	$mHistorial_=explode('}{',$propietats);
	while(list($key,$val)=each($mHistorial_))
	{
		$mVal=explode(';',$val);
	
		if($mVal[0]!='')
		{
			if(!isset($mHistorial[$mVal[0]])){$mHistorial[$mVal[0]]=array();}
			array_push($mHistorial[$mVal[0]],$mVal);
		}
	}
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
	{
		echo "
	<p> Hist�ric <font style='color:DarkOrange;'>[sadmin][RG]</font></p>
		";
	}
	else
	{
		echo "
	<p> Hist�ric <font style='color:DarkOrange;'>[RG]</font></p>
		";
	}
	echo "
	
	<div style='width:100%; height:100px; overflow-y:scroll; overflow-x:hidden;'>
	<table border='1' style='width:100%;'>
	";
	while(list($tipus,$mVal)=each($mHistorial))
	{
		echo "
				<tr>
					<td valign='top'>
					<p style='font-size:10px;'>".$tipus."</p>
					</td>
					<td>
					<p style='font-size:10px;'>
					";
					while(list($index,$mVal2)=each($mVal))
					{
						unset($mVal2[0]);
						$registre_=implode('|',$mVal2);
						if(substr($mVal2[1],0,strpos($mVal2[1],':'))=='us')
						{
							$usId=substr($mVal2[1],strpos($mVal2[1],':')+1);
							$mUsuariRegistre=db_getUsuari($usId,$db);
							$registre_=str_replace($mVal2[1],"<font style='cursor:pointer; size:11px;' onClick=\"enviarFpars('nouUsuari.php?iUed=".$usId."&op=editarUsuari','_blank');\"><u>".$mUsuariRegistre['usuari']."</u></font>",$registre_);
						}
						echo "
						".urldecode($registre_)."<br>
						";
					}
					echo "
					</p>
					</td>
				</tr>
	";
	}
	reset($mHistorial);
	echo "
	</table>
	</div>
	
	";	

	return;
}

//------------------------------------------------------------------------------
function html_resumComandaCac($db)
{
	global  $mColors,
			$mPars,
			$mParametres,
			$mPerfil,
			$mPerfilResp,
			$mProductes,
			$mInventari,
			$mInventariPerZones,
			$mMesos,
			$mZones,
			$mParametres,
			$mGrupsRef,
			$mProductesJaEntregats,
			$mProductesJaEntregatsTot;
	
	$mUnitatsReservadesTotal=array();
	$mUnitatsReservades=array();
	
	
	echo "
	<br>
	";
	if($mParametres['zonesProvisionals']['valor']==1)
	{
		echo "
		<center>
		<p class='pAlertaNo4'>Atenci�: encara estem estructurant la ruta i els noms i els albarans de zona son provisionals.<br>No separeu els productes per zona fins que siguin definitives.</p>
		</center>
		";
	}
	else
	{
		echo "
		<center>
		<p class='pAlertaOk4'>Els noms i albarans de zona son definitius.<br>Ja podeu separar els productes per zona</p>
		</center>
		";
	}
	
	echo "
	<table align='center' width='80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td width='25%'>
			<p><b>Albarans de zona<br> per entregar a la CAC</b></p>
			</td>

			<td width='15%'>
			<p>[ ruta: <b>".($mMesos[(substr($mPars['selRutaSufix'],2,2))])." ".(substr($mPars['selRutaSufix'],0,2))."</b> ]</p>
			</td>

			<td width='25%'>
			<p>(data actual: ".(date('h:i:s d/m/Y')).")</p>
			</td>

			<td width='35%' align='right'>
			<p style='align:left;' class='nota'>* nom�s es mostren els productes actius</p>
			</td>
		</tr>
	</table>	
	<br><br>		
		
	";

	$mUnitatsReservadesPerZona2=array();
	while(list($key,$mProducte)=each($mProductes))
	{
			if(!isset($mInventari['inventariRef'][$mProducte['id']]))
			{
				$mInventari['inventariRef'][$mProducte['id']]=0;
			}
			
			if(!isset($mUnitatsReservadesPerZona[$mProducte['id']])){$mUnitatsReservadesPerZona[$mProducte['id']]=array();}
			if(!isset($mUnitatsReservades[$mProducte['id']])){$mUnitatsReservades[$mProducte['id']]=array();}
			if(!isset($mUtsInventariPendentsAssignar[$mProducte['id']])){$mUtsInventariPendentsAssignar[$mProducte['id']]=0;}

			$mUnitatsReservadesPerZona[$mProducte['id']]=getUnitatsReservadesPerZona($mProducte['id'],$db);

			$mPars['selProducteId']=$mProducte['id'];
			$mUnitatsReservades[$mProducte['id']]=getUnitatsReservades($db);


			if(!isset($mProductesJaEntregatsTot[$mProducte['id']])){$mProductesJaEntregatsTot[$mProducte['id']]=0;}
			$mUtsInventariPendentsAssignar[$mProducte['id']]=$mProductesJaEntregatsTot[$mProducte['id']]+$mInventari['inventariRef'][$mProducte['id']];

	}
	reset($mProductes);
	$mUtsInventariPendentsAssignar_=$mUtsInventariPendentsAssignar;
	$mUnitatsReservadesPerZona2=array();
	//obtenir unitats reservades per zona i producte:
	while(list($producteId,$mUnitatsReservadesPerZona_)=each($mUnitatsReservadesPerZona))
	{
		while(list($zona,$quantitat)=each($mUnitatsReservadesPerZona_))
		{
			if(!isset($mUnitatsReservadesPerZona2[$zona])){$mUnitatsReservadesPerZona[$zona]=array();}
			if(!isset($mUnitatsReservadesPerZona2[$zona][$producteId])){$mUnitatsReservadesPerZona2[$zona][$producteId]=0;}
			$mUnitatsReservadesPerZona2[$zona][$producteId]=$quantitat;
		}
		reset($mUnitatsReservadesPerZona_);
	}
	reset($mUnitatsReservadesPerZona);
	
	//per cada zona un albara:
	while(list($key,$zona)=each($mZones))
	{
		if(isset($mUnitatsReservadesPerZona2[$zona]))
		{
			echo "
		<table align='center' width='70%' border='0'>
			<tr>
				<td style='width:20%;'  align='left'>
				<p><b> - Albar� de zona - </b></p>
				</td>
				<td style='width:20%;'  align='left'>
				<p >ruta:<b>".$mPars['selRutaSufix']."</b></p>
				</td>
				<td style='width:40%;' align='left'>
				<p>productora: <i><b>".(urldecode($mPerfil['projecte']))."</b></i>
				<br>
				<font style='font-size:10px;'>
				usuaria: ".(urldecode($mPerfilResp['usuari']))." - mobil: ".(urldecode($mPerfilResp['mobil']))." - email: ".(urldecode($mPerfilResp['email']))."
				</font>
				</p>
				</td>
				<td style='width:20%;' align='right'  valign='bottom'>
				<p>".(date('d-m-Y H:i:s'))."</p>
				</td>
			</tr>
		</table>
		<table align='center' width='70%' border='1'>
			<tr>
				<th valign='top' align='center' style='width:15%'>
				<p><b><u>".$zona."</u></b></p>
				<p>id producte</p>
				</th>

				<th valign='top' align='left' style='width:35%'>
				<p>&nbsp;</p>
				<p> producte</p>
				</th>

				<th valign='top' align='left' style='width:10%'>
				<p>&nbsp;</p>
				<p>unitat facturaci�</p>
				</th>

				<th valign='top' align='left' style='width:5%'>
				<p>&nbsp;</p>
				<p>format</p>
				</th>

			";
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' ||  $mPars['nivell']=='coord')
			{
				echo "
				<th valign='top' align='center' style='width:5%'>
				<p>&nbsp;</p>
				<p>inventari<br>cac<br><b>GLOBAL</b><br>[".$mPars['nivell']."]</p>
				</th>

 				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>inventari<br>cac<br><b>".$zona."</b><br>[".$mPars['nivell']."]</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>quantitat reservada</p>
				</th>
				";
			}
			echo "

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>quantitat a<br>entregar a la CAC</p>
				</th>
				
				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>quantitat que<br>entrego a la CAC<br>(albar�)</p>
				</th>
			</tr>
			";

			while(list($producteId,$quantitat)=each($mUnitatsReservadesPerZona2[$zona]))
			{
				$esDiposit=false;
				if(substr_count($mProductes[$producteId]['tipus'],'dip�sit')>0)
				{
					$esDiposit=true;
				}
				$utsInventariZonaPendentsAssignar=0;
				
				if(!isset($mInventariPerZones[$zona][$producteId])){$mInventariPerZones[$zona][$producteId]=0;}
				if(!isset($mUtsInventariZonaPendentsAssignar[$zona])){$mUtsInventariZonaPendentsAssignar[$zona]=array();}
				if(!isset($mUtsInventariZonaPendentsAssignar[$zona][$producteId])){$mUtsInventariZonaPendentsAssignar[$zona][$producteId]=0;}
				
				if($quantitat>0 && $quantitat<=($mUtsInventariPendentsAssignar[$producteId]))
				{
					$entregarAcac=0;
					$mUtsInventariPendentsAssignar[$producteId]-=$quantitat;
					if($mInventariPerZones[$zona][$producteId]+(getProductesJaEntregatsZona($mProductesJaEntregats,$producteId,$zona))>$quantitat)
					{
						$mUtsInventariZonaPendentsAssignar[$zona][$producteId]=$mInventariPerZones[$zona][$producteId]+(getProductesJaEntregatsZona($mProductesJaEntregats,$producteId,$zona))-$quantitat;
					}
					else
					{
						$mUtsInventariZonaPendentsAssignar[$zona][$producteId]=$quantitat-$mInventariPerZones[$zona][$producteId]-(getProductesJaEntregatsZona($mProductesJaEntregats,$producteId,$zona));
					}
				}
				else if($quantitat>0 && $quantitat>$mUtsInventariPendentsAssignar[$producteId])
				{
					$entregarAcac=$quantitat-$mUtsInventariPendentsAssignar[$producteId];
					$mUtsInventariPendentsAssignar[$producteId]=0;
				}
				else
				{
					$entregarAcac='';
				}
					
				if($quantitat>0 && $quantitat>$mUtsInventariZonaPendentsAssignar[$zona][$producteId])
				{
					$mUtsInventariZonaPendentsAssignar[$zona][$producteId]=0;
				}
				else
				{
					$mUtsInventariZonaPendentsAssignar[$zona][$producteId]-=$quantitat;
				}
				
				echo "
			<tr>
				<td  align='center' valign='top'>
				<p>".$producteId."</p>
				</td>

				<td  align='left' valign='top'>
				<p >".(urldecode($mProductes[$producteId]['producte']))."</p>
				</td>

				<td align='left' style='width:10%'  valign='top'>
				<p style='font-size:10px;'>".(urldecode($mProductes[$producteId]['unitat_facturacio']))."</p>
				</td>

				<td align='center' style='width:5%'  valign='top'>
				<p style='font-size:10px;'>".$mProductes[$producteId]['format']."</p>
				</td>
				";
				if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' ||  $mPars['nivell']=='coord')
				{
					echo "
				<td  align='center'  valign='top'>
				<p style='font-size:10px;'>".$mInventari['inventariRef'][$producteId]."<br>[".$mPars['nivell']."]</p>
					";
					if($mUtsInventariPendentsAssignar[$producteId]>0)
					{
						echo "<p class='pAlertaNo5'>no<br>assignat<br>a GLOBAL:<br><b>".$mUtsInventariPendentsAssignar[$producteId]."</b> uts</p>";
					}
					echo "
				</td>

				<td  align='center'  valign='top'>
				<p style='font-size:10px;'>".($mInventariPerZones[$zona][$producteId]+(getProductesJaEntregatsZona($mProductesJaEntregats,$producteId,$zona)))."<br>[".$mPars['nivell']."]</p>
					";
					if($utsInventariZonaPendentsAssignar>0)
					{
						echo "<p class='pAlertaNo5'>no<br>assignat<br>a LOCAL:<br><b>".$utsInventariZonaPendentsAssignar."</b> uts</p>";
					}
					echo "
				</td>

				<td  align='center'  valign='top'>
				<p style='font-size:10px;'><b>".$quantitat."</b></p>
				</td>
					";
				}
				
				echo "

				<td  align='center'  valign='top'>
				";
				if($esDiposit)
				{
					echo "
				<pstyle='font-size:14px;'><b>".$entregarAcac."</b></p>
					";
				}
				else
				{
					echo "
				<p class='p_micro'>* no dip�sit *</b></p>
					";
				}
				echo "
				</td>

				<td align='center'  valign='top'>
				<p></p>
				</td>
			</tr>
				";	
			}
			reset($mUnitatsReservadesPerZona2[$zona]);
			echo "
		</table>
		<br>
		<br>
			";
			
		}
	}
	reset($mZones);
	
	echo "

		</table>
		<br>
	";

	//Albar� totals:
	if(isset($mUnitatsReservades))
	{
		echo "
		<div id='d_totals' style='z-index:0; top:0px;'>
		<table align='center' width='70%' border='0'>
			<tr>
				<td style='width:20%;'  align='left'>
				<p><b> - Albar� Total per la CAC- </b></p>
				</td>
				<td style='width:20%;'  align='left'>
				<p>ruta:<b>".$mPars['selRutaSufix']."</b></p>
				</td>
				<td style='width:40%;' align='left'>
				<p>productora: <i><b>".(urldecode($mPerfil['projecte']))."</b></i>
				<br>
				<font style='font-size:10px;'>
				usuaria: ".(urldecode($mPerfilResp['usuari']))." - mobil: ".(urldecode($mPerfilResp['mobil']))." - email: ".(urldecode($mPerfilResp['email']))."
				</font>
				</p>
				</td>
				<td style='width:20%;' align='right'  valign='bottom'>
				<p>".(date('d-m-Y H:i:s'))."</p>
				</td>
			</tr>
		</table>
		<table align='center' width='80%' border='1' >
			<tr>
				<th valign='top' align='center' style='width:15%'>
				<p>&nbsp;</p>
				<p>id producte</p>
				</th>

				<th valign='top' align='left' style='width:35%'>
				<p>&nbsp;</p>
				<p> producte</p>
				</th>

				<th valign='top' align='left' style='width:5%'>
				<p>&nbsp;</p>
				<p>unitat facturaci�</p>
				</th>

				<th valign='top' align='left' style='width:5%'>
				<p>&nbsp;</p>
				<p>format</p>
				</th>

				<td valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
	";
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
	{
		echo "
				<p><b>Inventari:</b><br>
				<div id='d_inventari' style='height: 100px; overflow-y:scroll;'>
				<p class='p_micro2'>
		";
		$mInventariData=explode('}',$mInventari['data']);
			
		$inventariData='';
		while(list($key,$inventariData_)=each($mInventariData))
		{
			if($inventariData_!='')
			{
				$inventariData_=str_replace('{','',$inventariData_);
				$mInventariData_2=explode(';',$inventariData_);
				if(isset($mGrupsRef[$mInventariData_2[0]]))
				{
					$inventariData.=(urldecode($mGrupsRef[$mInventariData_2[0]]['nom'])).'<br>'.$mInventariData_2[1].'<br>'.$mInventariData_2[2].'<br><br>';
				}
			}
		}
			
		echo $inventariData."
				</p>			
				</div>
				
		";
	}
	else
	{
		echo  "
				<p><b>inventari<br>CAC<br><b>GLOBAL</b></p>
		";
	}
	echo "
				</td>

	";
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
	{
		echo "
				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>quantitat reservada</p>
				</th>
		";
	}
	echo "
				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>quantitat a<br>entregar a la CAC</p>
				</th>


				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>preu/ut
				</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>% ms</p>
				<p class='p_micro'>(acord: <b>".$mPerfil['acord1']."</b>% ms)</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>
				ums
				</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>ecos</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>euros</p>
				</th>
				
			</tr>
	";

	$ums=0;
	$ecos=0;
	$euros=0;
	$umsT=0;
	$ecosT=0;
	$eurosT=0;

	while(list($producteId,$quantitat)=each($mUnitatsReservades))
	{
		$esDiposit=false;
		if(substr_count($mProductes[$producteId]['tipus'],'dip�sit')>0)
		{
			$esDiposit=true;
		}
			
		if($quantitat>0)
		{
			//if(!isset($mProductesJaEntregatsTOT[$producteId])){$mProductesJaEntregatsTOT[$producteId]=0;}
			if(!isset($mInventari['inventariRef'][$producteId])){$mInventari['inventariRef'][$producteId]=0;}

			if($quantitat<=($mInventari['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId]))
			{
				$entregarAcac=0;
			}
			else if($quantitat>$mInventari['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])
			{
				$entregarAcac=$quantitat-$mInventari['inventariRef'][$producteId]-$mProductesJaEntregatsTot[$producteId];
			}
			else
			{
				$entregarAcac='';
			}
			echo "
			<tr>
				<td  align='center' valign='top'>
				<p>".$producteId."</p>
				</td>

				<td  align='left' valign='top'>
				<p>".(urldecode($mProductes[$producteId]['producte']))."</p>
				</td>

				<td align='left' style='width:10%'  valign='top'>
				<p style='font-size:10px;'>".(urldecode($mProductes[$producteId]['unitat_facturacio']))."</p>
				</td>

				<td align='center' style='width:5%'  valign='top'>
				<p style='font-size:10px;'>".$mProductes[$producteId]['format']."</p>
				</td>
				<td  align='center'  valign='top'>
				<p style='font-size:10px;'>".$mInventari['inventariRef'][$producteId]."</p>
				</td>

			";
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
			{
				echo "
				<td  align='center'  valign='top'>
				<p style='font-size:10px;'><b>".$quantitat."</b></p>
				</td>
				";
			}
			if($esDiposit)
			{
				$ums=$entregarAcac*$mProductes[$producteId]['preu'];
				$ecos=$ums*$mProductes[$producteId]['ms']/100;
				$euros=$ums-$ecos;
				$umsT+=$ums;
				$ecosT+=$ecos;
				$eurosT+=$euros;
	
				echo "
				<td  align='center'  valign='top'>
				<p  style='font-size:14px;'><b>".$entregarAcac."</b></p>
				</td>
				
				<td  align='center'  valign='top'>
				<p style='font-size:10px;'>".(number_format($mProductes[$producteId]['preu'],2,'.',''))."</p>
				</td>

				<td  align='right'  valign='top'>
				<p style='font-size:10px;'>".(number_format($mProductes[$producteId]['ms'],2,'.',''))."%</p>
				</td>

				<td  align='right'  valign='top'>
				<p style='font-size:10px;'>".(number_format($ums,2,'.',''))."</p>
				</td>

				<td  align='right'  valign='top'>
				<p style='font-size:10px;'>".(number_format($ecos,2,'.',''))."</p>
				</td>

				<td  align='right'  valign='top'>
				<p style='font-size:10px;'>".(number_format($euros,2,'.',''))."</p>
				</td>
				";
			}
			else
			{
				$ums=$quantitat*$mProductes[$producteId]['preu'];
				$ecos=$ums*$mProductes[$producteId]['ms']/100;
				$euros=$ums-$ecos;
				$umsT+=$ums;
				$ecosT+=$ecos;
				$eurosT+=$euros;
	
				echo "
				<td  align='center'  valign='top'>
				<p  style='font-size:14px;'><b>".$quantitat."</b></p>
				</td>
				
				<td  align='center'  valign='top'>
				<p style='font-size:10px;'>".(number_format($mProductes[$producteId]['preu'],2,'.',''))."</p>
				</td>

				<td  align='right'  valign='top'>
				<p style='font-size:10px;'>".(number_format($mProductes[$producteId]['ms'],2,'.',''))."%</p>
				</td>

				<td  align='right'  valign='top'>
				<p style='font-size:10px;'>".(number_format($ums,2,'.',''))."</p>
				</td>

				<td  align='right' valign='top'>
				<p style='font-size:10px;'>".(number_format($ecos,2,'.',''))."</p>
				</td>

				<td  align='right' valign='top'>
				<p style='font-size:10px;'>".(number_format($euros,2,'.',''))."</p>
				</td>
				";
			}
			echo "
			</tr>
			";	
		}
	}
	reset($mUnitatsReservades);
	echo "
			<tr>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

	";
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
	{
		echo "
				<td  align='center'  valign='top'>
				<p><b>Total:</b></p>
				</td>
		";
	}
	echo "
				<td  align='right' valign='top'>
				<p><b>".(number_format($umsT,2,'.',''))."</b</p>
				</td>

				<td  align='right' valign='top'>
				<p><b>".(number_format($ecosT,2,'.',''))."</b></p>
				</td>

				<td  align='right' valign='top'>
				<p><b>".(number_format($eurosT,2,'.',''))."</b></p>
				</td>
			</tr>
		</table>
	";
}

$totalPendentAssignarUms=0;
$totalPendentAssignarUms_=0;
$totalPendentAssignarZonesUms=0;

if(!isset($mUtsInventariZonaPendentsAssignar)){$mUtsInventariZonaPendentsAssignar=array();}

while(list($producteId,$quantitat)=each($mUtsInventariPendentsAssignar_))
{
	$totalPendentAssignarUms_+=$quantitat*$mProductes[$producteId]['preu'];
}
reset($mUtsInventariPendentsAssignar_);

$mProductesForaDeZonaEntrega=array();
while(list($producteId,$quantitat)=each($mUtsInventariPendentsAssignar))
{
	$totalPendentAssignarUms+=$quantitat*$mProductes[$producteId]['preu'];
}
reset($mUtsInventariPendentsAssignar);

while(list($zona,$mUtsInventariZonaPendentsAssignar1)=each($mUtsInventariZonaPendentsAssignar))
{
	while(list($producteId,$quantitat)=each($mUtsInventariZonaPendentsAssignar1))
	{
		$totalPendentAssignarZonesUms+=$quantitat*$mProductes[$producteId]['preu'];
		if($quantitat>0){$mProductesForaDeZonaEntrega[$producteId]=$quantitat;}
	}
	reset($mUtsInventariZonaPendentsAssignar1);
}
reset($mUtsInventariZonaPendentsAssignar);

	echo "	
		</div>
		<br>
		<table align='center' width='90%'  bgcolor='".$mColors['table']."'>
			<tr>
				<td width='100%' align='right'>
				<p style='color:blue;'><input type='checkbox' id='ck_ocultarNotes' value='0' onClick=\"javascript:ocultarNotes();\"> ocultar notes (per imprimir)</p>
				</td>
			</tr>
		</table>			
		</td>
	</tr>
</table>

	";



	return;
}

//------------------------------------------------------------------------------
function html_resumComandaGrup($db)
{
	global  $mColors,
			$mPars,
			$mParametres,
			$mPerfil,
			$mPerfilResp,
			$mProductes,
			$mPropietatsPeriodeLocal,
			$mPropietatsPeriodesLocals,
			$mPeriodesLocalsInfo,
			$mInventari,
			$mInventariPerZones,
			$mMesos,
			$mZones,
			$mParametres,
			$mGrupsRef,
			$mProductesJaEntregats,
			$mProductesJaEntregatsTot;

	$mUnitatsReservadesTotal=array();
	$mUnitatsReservades=array();

	while(list($key,$mProducte)=each($mProductes))
	{
		if(!isset($mUnitatsReservades[$mProducte['id']])){$mUnitatsReservades[$mProducte['id']]=array();}

		$mPars['selProducteId']=$mProducte['id'];
		$mUnitatsReservades[$mProducte['id']]=db_getUnitatsReservadesLocal($db);
	}
	reset($mProductes);

	//Albar� totals:
	if(count($mUnitatsReservades)>0 && count($mPropietatsPeriodesLocals)>0)
	{
		echo "
		<div id='d_totals' style='z-index:0; top:0px;'>
		<table align='center' width='80%' border='0'  bgcolor='".$mColors['table']."'>
			<tr>
				<td style='width:20%;'  align='left' valign='top'>
				<p><b> - Albar� pel GRUP - </b></p>
				</td>
				<td style='width:20%;'  align='left'>
				<p>periode local:
				<br><b>".$mPeriodesLocalsInfo[$mPars['sel_periode_comanda_local']]['periode_comanda']." (".$mPars['sel_periode_comanda_local'].")</b></p>
				</td>
				<td style='width:40%;' align='left'>
				<p>productora: <i><b>".(urldecode($mPerfil['projecte']))."</b></i>
				<br>
				<font style='font-size:10px;'>
				usuaria: ".(urldecode($mPerfilResp['usuari']))." - mobil: ".(urldecode($mPerfilResp['mobil']))." - email: ".(urldecode($mPerfilResp['email']))."
				</font>
				</p>
				</td>
				<td style='width:20%;' align='right'  valign='bottom'>
				<p>".(date('d-m-Y H:i:s'))."</p>
				</td>
			</tr>
		</table>
		<table align='center' width='80%' border='1'  bgcolor='".$mColors['table']."'>
			<tr>
				<th valign='top' align='center' style='width:15%'>
				<p>&nbsp;</p>
				<p>id producte</p>
				</th>

				<th valign='top' align='left' style='width:35%'>
				<p>&nbsp;</p>
				<p> producte</p>
				</th>

				<th valign='top' align='left' style='width:5%'>
				<p>&nbsp;</p>
				<p>unitat facturaci�</p>
				</th>

				<th valign='top' align='left' style='width:5%'>
				<p>&nbsp;</p>
				<p>format</p>
				</th>

				<th valign='top' align='left' style='width:5%'>
				<p>&nbsp;</p>
				<p>pes<br>(kg)</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>quantitat reservada</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>preu/ut
				</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>% ms</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>
				ums
				</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>ecos</p>
				</th>

				<th valign='top' align='center' style='width:10%'>
				<p>&nbsp;</p>
				<p>euros</p>
				</th>
				
			</tr>
	";

	$ums=0;
	$ecos=0;
	$euros=0;
	$pes=0;
	
	$umsT=0;
	$ecosT=0;
	$eurosT=0;
	$pesT=0;

	while(list($producteId,$quantitat)=each($mUnitatsReservades))
	{
		if($quantitat>0)
		{
			echo "
			<tr>
				<td  align='center' valign='top'>
				<p>".$producteId."</p>
				</td>

				<td  align='left' valign='top'>
				<p>".(urldecode($mProductes[$producteId]['producte']))."</p>
				</td>

				<td align='left' style='width:10%'  valign='top'>
				<p style='font-size:10px;'>".(urldecode($mProductes[$producteId]['unitat_facturacio']))."</p>
				</td>

				<td align='center' style='width:5%'  valign='top'>
				<p style='font-size:10px;'>".$mProductes[$producteId]['format']."</p>
				</td>

				<td align='center' style='width:5%'  valign='top'>
				<p style='font-size:10px;'>".$mProductes[$producteId]['pes']."</p>
				</td>

				<td  align='center'  valign='top'>
				<p  style='font-size:14px;'><b>".$quantitat."</b></p>
				</td>
				";

				$ums=$quantitat*$mProductes[$producteId]['preu'];
				$ecos=$ums*$mProductes[$producteId]['ms']/100;
				$euros=$ums-$ecos;
				$pes=$mProductes[$producteId]['pes']*$quantitat;
				$umsT+=$ums;
				$ecosT+=$ecos;
				$eurosT+=$euros;
				$pesT+=$pes;
	
				echo "
				
				<td  align='center'  valign='top'>
				<p style='font-size:10px;'>".(number_format($mProductes[$producteId]['preu'],2,'.',''))."</p>
				</td>

				<td  align='right'  valign='top'>
				<p style='font-size:10px;'>".(number_format($mProductes[$producteId]['ms'],2,'.',''))."%</p>
				</td>

				<td  align='right'  valign='top'>
				<p style='font-size:10px;'>".(number_format($ums,2,'.',''))."</p>
				</td>

				<td  align='right' valign='top'>
				<p style='font-size:10px;'>".(number_format($ecos,2,'.',''))."</p>
				</td>

				<td  align='right' valign='top'>
				<p style='font-size:10px;'>".(number_format($euros,2,'.',''))."</p>
				</td>
			</tr>
			";	
		}
	}
	reset($mUnitatsReservades);
	echo "
			<tr>

				<td  align='center'  valign='top'>
				</td>

				<td  align='right'  valign='top'>
				<p><b>Total:</b></p>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				<p><b>".(number_format($pesT,2,'.',''))."</b</p>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='center'  valign='top'>
				</td>

				<td  align='right' valign='top'>
				<p><b>".(number_format($umsT,2,'.',''))."</b</p>
				</td>

				<td  align='right' valign='top'>
				<p><b>".(number_format($ecosT,2,'.',''))."</b></p>
				</td>

				<td  align='right' valign='top'>
				<p><b>".(number_format($eurosT,2,'.',''))."</b></p>
				</td>
			</tr>
		</table>
	";
}
else
{
 	echo "
		<table width='50%' align='center' border='1'  bgcolor='".$mColors['table']."'>
			<tr>
				<td align='center'>
	 ";
	if(count($mPropietatsPeriodesLocals)==0)
	{
		echo "
				<p>Encara no hi ha cap periode local en la llista seleccionada</p>
		";
	}
 	else if(count($mUnitatsReservades)==0)
	{
		echo "
				<p>No hi ha cap unitat reservada en aquest periode per aproductes d'aquest perfil de productora.</p>
		";
	}
	echo "
				</td>
			</tr>
		</table>
	";

}

	echo "	
		</div>
		<br>
		<table align='center' width='90%' bgcolor='".$mColors['table']."'>
			<tr>
				<td width='100%' align='right'>
				<p style='color:blue;'><input type='checkbox' id='ck_ocultarNotes' value='0' onClick=\"javascript:ocultarNotesGrup();\"> ocultar notes (per imprimir)</p>
				</td>
			</tr>
		</table>			
		</td>
	</tr>
</table>

	";



	return;
}

//------------------------------------------------------------------------------
function html_fitxaProducte($db)
{
	global 	$modificarValorsImportantsProductes,
			$opt,
			$parsChain,
			$thumbCode,
			$mAjuda,
			$mColors,
			$mColsProductes3,
			$mInventari,
			$mNomsColumnes3,
			$mParametres,
			$mPars,
			$mPerfilsRef,
			$mPerfil,
			$mProducte,
			$mSubCategories,
			$mUsuari;

	if($opt!='inici' && $opt!='aE' && $opt!='aEg'  && isset($mProducte['id']))
	{
		echo "
<form id='f_guardarProducte' name='f_guardarProducte' method='post' action='gestioProductes.php' target='_self' >
<table style='width:80%;' align='center' border='1'  bgcolor='white'>
	<tr>
		<td  style='width:49%;' valign='top' >
		<img  src='productes/".$mProducte['id']."/".$mProducte['imatge']."'>
		<table style='width:100%;' >
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>Estat:</p>
				</th>
				<td style='width:66%;' align='right'  valign='top'>
		";	
		if($mProducte['actiu']=='1')
		{
			echo "
				<p class='pVactiu'>ACTIU</p>
			";
		}
		else if($mProducte['actiu']=='0')
		{
			echo "
				<p class='pVinactiu'>INACTIU</p>
			";
		}
		echo "
				</td>
		";
		if($mPars['selLlistaId']==0)
		{
			if($mParametres['aplicarValorTallSegell']['valor']*1==1) //aplicar valortallsegell si est� actiu
			{
				if($mProducte['segell']*1>=$mParametres['valorTallSegell']['valor']*1)
				{
					echo "
				<td bgcolor='green' align='center'>
				<p style='color:white;'>SEGELL OK</p>
				</td>
					";
				}
				else
				{
					echo "
				<td bgcolor='red' align='center'>
				<p style='color:white;'>SEGELL KO</p>
				</td>
					";
				}
			}
		}
		echo "
			</tr>

			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>Id:</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<p>".$mPars['selProducteId']."</p>
				</td>
			</tr>
		";
		$campsGuardar='';
		$unitatsReservades=0;
		while (list($key,$val)=each($mProducte))
		{
			$unitatsReservades=getUnitatsReservades($db);
			if(in_array($key,$mColsProductes3))
			{
				if($key=='estoc_previst')
				{
					if
					(
						substr_count($mProducte['tipus'],'dip�sit')>0
						||
						substr_count(urldecode($mProducte['tipus']),'dip�sit')>0
						|| 
						($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
					)
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
						";
						if
						(
							(
								(
									$mPars['selRutaSufixPeriode']==date('ym')
									&&
									$modificarValorsImportantsProductes
								)
								||
								$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
							)
							&&
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								||
								($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							)
						)
						{
							echo "
				<p class='nota'><input type='text' id='i_estoc_previst' name='i_estoc_previst' size='5' value=\"".(urldecode($val))."\"> (uts.)"; if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){echo "  [sadmin]";} echo "
				<p class='nota'>reservat:<b>".$unitatsReservades."</b>(uts)</p>
				<p class='nota'>inventari:<b>"; if(isset($mInventari['inventariRef'][$mPars['selProducteId']])){echo $mInventari['inventariRef'][$mPars['selProducteId']];}else{$mInventari['inventariRef'][$mPars['selProducteId']]=0;echo "0";} echo "</b> uts.</p>
							";
							$campsGuardar.=',estoc_previst';
						}
						else
						{
							echo "
				<p class='nota'><input type='text'  id='i_estoc_previst' name='i_estoc_previst' "; if($mPars['nivell']!='sadmin'){echo "READONLY style='background-color:#eeeeee;' ";}else{$campsGuardar.=',estoc_previst';} echo " size='5' value=\"".(urldecode($val))."\"> (uts.)</p>
				<p class='nota'>reservat:<b>".$unitatsReservades."</b>(uts)</p>
				<p class='nota'>inventari:<b>"; if(isset($mInventari['inventariRef'][$mPars['selProducteId']])){echo $mInventari['inventariRef'][$mPars['selProducteId']];}else{$mInventari['inventariRef'][$mPars['selProducteId']]=0;echo "0";} echo "</b> (uts.)</p>
							";
						}

						if
						(
							$opt=='vell'
							&&
							(
								($mPars['nivell']=='sadmin'  || $mPars['nivell']=='admin')
								||
								$mPars['selLlistaId']!=0
								&&
								$mPars['usuari_id']==$mGrupsRef[$mPars['selLlistaId']]['usuari_id']
								&&
								$unitatsReservades>0
							)
						)
						{
							echo "
					<br>
				<input type='button' onClick=\"javascript: anularReserves('".$mPars['selProducteId']."');\" value='anul.lar reserves'>	[admin]			
							";
						}
						echo "
				</td>
			</tr>
						";
					}
					else
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'>
				<input type='text'  id='i_estoc_previst' name='i_estoc_previst' "; if($mPars['nivell']!='sadmin'){echo "READONLY style='background-color:#eeeeee;' ";}else{$campsGuardar.=',estoc_previst';} echo " size='5' value=\"".(urldecode($val))."\"> (uts.)</p>
				<p class='nota'>
				reservat:<b>".$unitatsReservades."</b> uts.</p>
						";
						if
						(
							$opt=='vell'
							&&
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							&&
							$unitatsReservades>0
						)
						{
							echo "
					<br>
				<input type='button' onClick=\"javascript: anularReserves('".$mPars['selProducteId']."');\" value='anul.lar reserves'>	[admin]			
							";
						}
						echo "
				</td>
			</tr>
						";
					}
				}
				else if($key=='productor')
				{
					if
					(
						($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
					)
					{
						
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				"; 
				$selected='';
				$selected2='selected';
				echo"
				<p>
				<select id='i_".$key."' name='i_".$key."'>
				";
				while(list($perfilId,$mPerfil_)=each($mPerfilsRef))
				{
					if(substr($val,0,strpos($val,'-'))==$mPerfil_['id']){$selected='selected';$selected2='';}else{$selected='';}
					echo "
				<option ".$selected." value='".$perfilId.'-'.(urldecode($mPerfil_['projecte']))."'>".$perfilId.'-'.(urldecode($mPerfil_['projecte']))."</option>
					";
				}
				reset($mPerfilsRef);
				
				echo "
				<option ".$selected2." value=''></option>
				</select>
				</p>
				</td>
			</tr>
						";
						$campsGuardar.=','.$key;
					}
					else
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<input type='text' readonly style='background-color:#eeeeee;' size='30' id='i_".$key."' name='i_".$key."' value=\"".(urldecode($val))."\">
				</td>
			</tr>
					";
						$campsGuardar.=','.$key;
					}
				}
				else if($key=='actiu')
				{
					$selected1='';
					$selected2='selected';
					$mActiu[0]='inactiu';
					$mActiu[1]='actiu';
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='middle'>
				<select 
					";
					if($mPars['nivell']!='sadmin')
					{
						if
						(
							(
								(
									$mPars['selRutaSufixPeriode']==date('ym')
									&&
									$modificarValorsImportantsProductes
								)
								|| 
								$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
							)
							&& 
							$mUsuari['id']==$mPerfil['usuari_id']
						)
						{
							if($mProducte['actiu']==0)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								if($unitatsReservades==0)
								{
									echo " name='i_".$key."' ";
									$campsGuardar.=','.$key;
								}
								else
								{						
									echo " name='i_".$key."' ";
									echo " DISABLED style='background-color:#eeeeee;' ";
								}
							}
						}
						else
						{
							echo " name='i_".$key."' ";
							echo " DISABLED style='background-color:#eeeeee;' ";
						}
					}
					else
					{
						echo " name='i_".$key."' ";
						$campsGuardar.=','.$key;
					}
	
					echo "
				 id='i_".$key."' size='1'>
					";
					for($i=0;$i<2;$i++)
					{
						if($mProducte['actiu']==$i)
						{
							$selected1='selected';$selected2='';}else{$selected1='';
						}
						echo "
				<option ".$selected1."  value=\"".$i."\">".$mActiu[$i]."</option>
						";
					}
					echo "
				</select>
					";
				
					if($mProducte['actiu']=='1')
					{
						echo "
				<img src='imatges/okp.gif'>
						";
					}
					else
					if($mProducte['actiu']=='0')
					{
						echo "
				<img src='imatges/nop.gif'>
						";
					}
					echo "
				</td>
			</tr>
				";
			}
			else if($key=='format')
			{
				$selected1='';
				$selected2='selected';
			
				echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				
				<td style='width:66%;' align='left'  valign='top'>
				<select 
				";
				if($mPars['nivell']!='sadmin')
				{
					if($modificarValorsImportantsProductes)
					{
						if($mParametres['precomandaTancada']['valor']=='0')					
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								(
									$mPars['selRutaSufixPeriode']==date('ym')
									||
									$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
								)
								&&
								$mProducte['actiu']=='0'
							)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
						else 
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								(
									$mPars['selRutaSufixPeriode']==date('ym')
									||
									$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
								)
							)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
					}
					else
					{
						echo " DISABLED style='background-color:#eeeeee;' ";
					}
				}
				else
				{
					echo " name='i_".$key."' ";
					$campsGuardar.=','.$key;
				}

				echo "
				 id='i_".$key."' size='3'>
				";
				for($i=1;$i<2000;$i++)
				{
					if($mProducte['format']*1==$i)
					{
						$selected1='selected';$selected2='';}else{$selected1='';
					}
					echo "
				<option ".$selected1." value=\"".$i."\">".$i."</option>
					";
				}
				echo "
				<option ".$selected2." value=''></option>
				</select>
				</td>
			</tr>
				";
			}
			else if($key=='categoria0' || $key=='categoria10')
			{
				$selected='';
				if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<p>
				<select id='i_".$key."' name='i_".$key."' size='3'>
					";
					while(list($key2,$categoria_)=each($mSubCategories[$key]))
					{
						if($categoria_==$val){$selected='selected';}else{$selected='';}

						echo "
				<option ".$selected." value=\"".$categoria_."\">".(urldecode($categoria_))."</option>
						";
					}
					reset($mSubCategories[$key]);
				
					echo "
				</select>[sadmin]
				</p>
				</td>
				<td>
				<table>
					<tr>
					<td valign='top'>
					";
					if($key=='categoria0')
					{
						echo "
						<p class='nota'> o la categoria: ".(html_ajuda1('html_gestioProductes.php',10))."</p>
						<input type='text' id='i_ncategoria0' name='i_ncategoria0' value=''>
						";
					}
					else
					if($key=='categoria10')
					{
						echo "
						<p class='nota'> o la sub-categoria: ".(html_ajuda1('html_gestioProductes.php',11))."</p>
						<input type='text' id='i_ncategoria10' name='i_ncategoria10' value=''>
						";
					}
					echo "
						</td>
					</tr>
				</table>
				</td>
			</tr>
						";
						$campsGuardar.=','.$key.',n'.$key;
				}
				else
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<p>
					";
					echo "
				<select id='i_".$key."' 
					";
					$disabled='DISABLED';
					if($modificarValorsImportantsProductes)
					{
						if($mParametres['precomandaTancada']['valor']=='0')					
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								(
									$mPars['selRutaSufixPeriode']==date('ym')
									||
									$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
								)
								&&
								$mProducte['actiu']=='0'
							)
							{
								echo " name='i_".$key."' ";
								$disabled='';
								$campsGuardar.=','.$key;
							}
							else
							{
								$disabled='DISABLED';
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
						else 
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								(
									$mPars['selRutaSufixPeriode']==date('ym')
									||
									$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
								)
								&&
								$mProducte['actiu']=='0'
							)
							{
								$disabled='';
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								$disabled='DISABLED';
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
					}
					else
					{
						echo " DISABLED style='background-color:#eeeeee;' ";
					}
			
					echo "
				size='3'>
					";
					while(list($key2,$categoria_)=each($mSubCategories[$key]))
					{
						if($categoria_==$val){$selected='selected';}else{$selected='';}

						echo "
				<option ".$selected." value=\"".$categoria_."\">".(urldecode($categoria_))."</option>
						";
					}
					reset($mSubCategories[$key]);
					echo "
				<option  value=\"altres\">-altres-</option>
				</select>
				</p>
				</td>
				
				<td>
				<table>
					<tr>
					<td valign='top'>
					";
					if($key=='categoria0')
					{
						echo "
						<p class='nota'> o la categoria: ".(html_ajuda1('html_gestioProductes.php',10))."</p>
						<input type='text' ".$disabled." id='i_ncategoria0' name='i_ncategoria0' value=''>
						";
					}
					else
					if($key=='categoria10')
					{
						echo "
						<p class='nota'> o la sub-categoria: ".(html_ajuda1('html_gestioProductes.php',11))."</p>
						<input type='text' ".$disabled." id='i_ncategoria10' name='i_ncategoria10' value=''>
						";
					}
					echo "
						</td>
					</tr>
				</table>
				</td>
			</tr>
					";
				}
			}
			else if($key=='producte' )
			{
				if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'><input type='text' id='i_".$key."' name='i_".$key."' size='30' value=\"".(urldecode($val))."\"> [sadmin]</p>
				</td>
			</tr>
					";
					$campsGuardar.=','.$key;
				}
				else
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'><input type='text'
					";
						if($modificarValorsImportantsProductes)
						{
							if($mParametres['precomandaTancada']['valor']=='0')					
							{
								if
								(
									$mUsuari['id']==$mPerfil['usuari_id']
									&&
									(
										$mPars['selRutaSufixPeriode']==date('ym')
										||
										$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
									)
									&&
									$mProducte['actiu']=='0'
									&&
									substr_count($val,'plantilla')>0
								)
								{
									echo " name='i_".$key."' ";
									$campsGuardar.=','.$key;
								}
								else
								{
									echo " DISABLED style='background-color:#eeeeee;' ";
								}
							}
							else 
							{
								if
								(
									$mUsuari['id']==$mPerfil['usuari_id']
									&&
									(
										$mPars['selRutaSufixPeriode']==date('ym')
										||
										$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
									)
									&&
									substr_count($val,'plantilla')>0
								)
								{
									echo " name='i_".$key."' ";
									$campsGuardar.=','.$key;
								}
								else
								{
									echo " DISABLED style='background-color:#eeeeee;' ";
								}
							}
						}
						else
						{
							echo " DISABLED style='background-color:#eeeeee;' ";
						}
					echo "
				  size='30' id='i_".$key."' name='i_".$key."' value=\"".(urldecode($val))."\"> </p>
				</td>
			</tr>
					";
				}
			}
			else if($key=='pes' || $key=='unitat_facturacio' || $key=='preu' || $key=='ms' || $key=='volum' )
			{
				$uts=' ';
				if($key=='pes' || $key=='preu' || $key=='ms' || $key=='volum' ){$size=5;}else{$size=30;}
				if($key=='pes'){$uts=' (kg.) ';}
				else if($key=='preu'){$uts=' (ums.) ';}
				else if($key=='volum'){$uts=' (l.) ';}
				else if($key=='ms'){$uts=' (%) ';}
			
				if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'><input type='text' id='i_".$key."' name='i_".$key."' size='".$size."' value=\"".(urldecode($val))."\"> ".$uts." [sadmin]</p>
				</td>
			</tr>
					";
					$campsGuardar.=','.$key;
				}
				else
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'><input type='text'
					";
					if($modificarValorsImportantsProductes)
					{
						if($mParametres['precomandaTancada']['valor']=='0')					
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								(
									$mPars['selRutaSufixPeriode']==date('ym')
									||
									$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
								)
								&&
								$mProducte['actiu']=='0'
							)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
						else 
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								(
									$mPars['selRutaSufixPeriode']==date('ym')
									||
									$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
								)
							)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
					}
					else
					{
						echo " DISABLED style='background-color:#eeeeee;' ";
					}
					echo "
				  size='".$size."' id='i_".$key."' name='i_".$key."' value=\"".(urldecode($val))."\"> ".$uts."</p>
				</td>
			</tr>
					";
				}
			}
			else if($key=='estoc_disponible')
			{
				$size='5';
		
				if
				(
					$opt=='vell'
				)
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p><input type='text' READONLY  style='background-color:#eeeeee;' id='i_".$key."' name='i_".$key."' size='".$size."' value=\"".(urldecode($val))."\"> [sadmin]</p>
				</td>
			</tr>
					";
				}
			}
			else if($key=='tipus' || $key=='cost_transport_extern_kg' || $key=='cost_transport_intern_kg')
			{
				$size='5';
				if($key=='tipus'){$size=30;$valor=urldecode($val);}
				else{$valor=$val;}
		
				if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p><input type='text' id='i_".$key."' name='i_".$key."' size='".$size."' value=\"".(urldecode($val))."\"> [sadmin]</p>
				</td>
			</tr>
					";
					$campsGuardar.=','.$key;
				}
				else
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<input type='text' readonly style='background-color:#dddddd;' size='".$size."' id='i_".$key."' value=\"".$valor."\">
				</td>
			</tr>
					";
				}
			}
		}
	}
	reset($mProducte);
	echo "
		</table>
	";		
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
	{
		html_mostrarPropietatsProducte($db);
	}

	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
	{
		echo "
		<td style='width:50%;' valign='top' align='center'>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�: (vista previa)</b></p>
		<div style='width:100%; height:250px; overflow-y:scroll; overflow-x:hidden;'>
		<table style='width:100%;'>
			<tr>
				<td style='width:100%;'>
				<p>".(urldecode($mProducte['notes_rebosts']))."<br><br></p>
				</td>
			</tr>
		</table>
		</div>
		<textArea cols='60' rows='20' type='text' id='i_notes_rebosts' name='i_notes_rebosts'>".(urldecode($mProducte['notes_rebosts']))."</textArea>[sadmin]
		";
		html_notesHTML();
		echo "
		</td>
		";
		$campsGuardar.=',notes_rebosts';
	}
	else if
	(
		$mUsuari['id']==$mPerfil['usuari_id']
		&&
		$modificarValorsImportantsProductes
		&&
		(
			$mPars['selRutaSufixPeriode']==date('ym')
			||
			$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
		)
	)
	{
		echo "
		<td style='width:50%;' valign='top' align='center'>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�: (vista previa)</b></p>
		<div style='width:500px; height:250px; overflow-y:scroll;'>
		<table style='width:100%;'>
			<tr>
				<td style='width:100%;'>
				<p>".(urldecode($mProducte['notes_rebosts']))."</p>
				</td>
			</tr>
		</table>
		</div>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�: (HTML)</b></p>
		<textArea cols='60' rows='20' ";
		if($mParametres['precomandaTancada']['valor']==0 && $mProducte['actiu']=='1')
		{
			echo " READONLY style='background-color:#eeeeee;' ";
		}
		else
		{
			$campsGuardar.=',notes_rebosts';
		}
		
		echo "
 		type='text' id='i_notes_rebosts' name='i_notes_rebosts'>".(urldecode($mProducte['notes_rebosts']))."</textArea>
		";
		html_notesHTML();
		echo "
		</td>
		";
	}
	else
	{
		echo "
		<td style='width:50%;' valign='top' align='center'>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�:</b></p>
		<div style='width:100%; height:250px; overflow-y:scroll; overflow-x:hidden;'>
		<table style='width:100%;'>
			<tr>
				<td style='width:100%;'>
				<p>".(urldecode($mProducte['notes_rebosts']))."</p>
				</td>
			</tr>
		</table>
		</div>
		</td>
		";
	}
	
	echo "
	</tr>

	<tr>
		<td>
		</td>

		<td align='center'>
	";
	
	if
	(
		$opt=='vell'
		&&
		(
			($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
			||
			(
				(
					(
						$mPars['selRutaSufixPeriode']==date('ym')
						&&
						$modificarValorsImportantsProductes
					)
					||
					$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
				)
				&&
				$mUsuari['id']==$mPerfil['usuari_id']
			)
		)
	)
	{
		echo "
		<br>
		<input type='button' onClick=\"javascript: if(checkFormGuardarProductes()){guardarProducte();}\" value='guardar producte'>
		<br>
		";
	}
	
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
	{
		if(($mPars['selRutaSufixPeriode']==date('ym') || $mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))))
		{
			echo "
		<br>
		<a title=\"En guardar el producte actual com a nou producte es canviar� l'identificador, i el producte de partida no es modificar�\"><input type='button' onClick=\"javascript: if(checkFormGuardarProductes()){guardarNouProducte();}\" value='guardar com a nou producte'></a>
		<br>
			";
		}
		if($opt=='vell' && ($mPars['selRutaSufixPeriode']==date('ym') || $mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))))
		{
			echo "
		<br>
		<a title=\"Aquest producte nom�s es pot eliminar si no hi ha cap reserva en les comandes actuals\"><input type='button' onClick=\"javascript: eliminarProducte('".$mPars['selProducteId']."');\" value='eliminar producte'></a>
		<br>
			";
		}		
	}
	$campsGuardar=substr($campsGuardar,1);
	echo "
		<input type='hidden' id='i_guardarProducte_id' name='i_guardarProducte_id' value=\"".$mPars['selProducteId']."\">
		<input type='hidden' id='i_opcio' name='i_opcio' value='guardar'>
		<input type='hidden' name='i_pars' value='".$parsChain."'>
		<input type='hidden' name='i_campsGuardar' value='".$campsGuardar."'>
		<input type='hidden' id='i_unitatsReservades' value='".$unitatsReservades."'>
		<input type='hidden' id='i_unitatsInventari' value='".@$mInventari['inventariRef'][$mPars['selProducteId']]."'>
		<br><br>
		</td>
	</tr>
</table>
</form>
<br>
	";
	
	

}


	return;
}


//------------------------------------------------------------------------------
function html_fitxaProducteLocal($db)
{
	global 	$modificarValorsImportantsProductes,
			$opt,
			$parsChain,
			$thumbCode,
			$mAjuda,
			$mColsProductes3,
			$mInventari,
			$mNomsColumnes3,
			$mParametres,
			$mPars,
			$mPerfil,
			$mPerfilsRef,
			$mPeriodeNoTancat,
			$mProducte,
			$mSubCategories,
			$mUsuari,
			$mGrupsRef,
			$mGrup;
			
	if($opt!='inici' && $opt!='aE' && $opt!='aEg' && $mProducte)
	{
		echo "
<form id='f_guardarProducte' name='f_guardarProducte' method='post' action='gestioProductes.php' target='_self' >
<table style='width:80%;' align='center' border='1' bgcolor='white'>
	<tr>
		<td  style='width:49%;' valign='top' >
		<img  src='llistes/".$mPars['selLlistaId']."/productes/".$mProducte['id']."/".$mProducte['imatge']."'>
		<table style='width:100%;' >
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>Estat:</p>
				</th>
				<td style='width:66%;' align='right'  valign='top'>
		";	
		if($mProducte['actiu']=='1')
		{
			echo "
				<p class='pVactiu'>ACTIU</p>
			";
		}
		else if($mProducte['actiu']=='0')
		{
			echo "
				<p class='pVinactiu'>INACTIU</p>
			";
		}
		echo "
				</td>
			</tr>

			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>Id:</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<p>".$mPars['selProducteId']."</p>
				</td>
			</tr>
		";
		$campsGuardar='';
		$unitatsReservades=0;
		while (list($key,$val)=each($mProducte))
		{
			$unitatsReservades=db_getUnitatsReservadesLocal($db);
			if(in_array($key,$mColsProductes3))
			{
				if($key=='estoc_previst')
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
					";
						
					if
					(
						$mPeriodeNoTancat!==false
						&&
						$mPeriodeNoTancat['comandesLocalsTancades']!=1							
						&&
						(
							$mUsuari['id']==$mPerfil['usuari_id']
							||
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							||
							$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup, nomes info i per anul.lar reserves
						)
					)
					{
						echo "
				<p class='nota'><input type='text' id='i_estoc_previst' name='i_estoc_previst' size='5' value=\"".(urldecode($val))."\"> (uts.)"; if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){echo "  [sadmin]";} echo "
				<p class='nota'>reservat:<b>".$unitatsReservades."</b>(uts)</p>
						";
						$campsGuardar.=',estoc_previst';
					}
					else
					{
						echo "
				<p class='nota'><input type='text'  id='i_estoc_previst' name='i_estoc_previst' READONLY style='background-color:#eeeeee;'  size='5' value=\"".(urldecode($val))."\"> (uts.)</p>
				<p class='nota'>reservat:<b>".$unitatsReservades."</b>(uts)</p>
						";
					}

					if
					(
						$mPeriodeNoTancat!==false
						&&
						$mPeriodeNoTancat['comandesLocalsTancades']!=1							
						&&
						$opt=='vell'
						&&
						$unitatsReservades>0
						&&
						(
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							||
							$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
						)
					)
					{
						echo "
					<br>
				<input type='button' onClick=\"javascript: anularReserves('".$mPars['selProducteId']."');\" value='anul.lar reserves'>	[admin]			
						";
					}
					echo "
				</td>
			</tr>
					";
				}
				else if($key=='productor')
				{
					if
					(
						$mPeriodeNoTancat!==false
						&&
						$mPeriodeNoTancat['comandesLocalsTancades']!=1
						&& 
						(
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							||
							$mPars['usuari_id']==$mGrup['usuari_id']
						)
					)
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				"; 
				$selected='';
				$selected2='selected';
				echo"
				<p>
				<select id='i_".$key."' name='i_".$key."'>
				";
				while(list($perfilId,$mPerfil_)=each($mPerfilsRef))
				{
					
					if(substr($val,0,strpos($val,'-'))==$mPerfil_['id']){$selected='selected';$selected2='';}else{$selected='';}
					echo "
				<option ".$selected." value='".$mPerfil_['id'].'-'.($mPerfil_['projecte'])."'>".$mPerfil_['id'].'-'.(urldecode($mPerfil_['projecte']))."</option>
					";
				}
				reset($mPerfilsRef);
				
				echo "
				<option ".$selected2." value=''></option>
				</select>
				</p>
				</td>
			</tr>
			</tr>
						";
						$campsGuardar.=','.$key;
					}
					else
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<input type='text' readonly style='background-color:#eeeeee;' size='30' id='i_".$key."' name='i_".$key."' value=\"".(urldecode($val))."\">
				</td>
			</tr>
						";
						$campsGuardar.=','.$key;
					}
				}
				else if($key=='actiu')
				{
					$selected1='';
					$selected2='selected';
					$mActiu[0]='inactiu';
					$mActiu[1]='actiu';
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='middle'>
				<select 
					";
					if
					(
						$mPeriodeNoTancat!==false
						&&
						$mPeriodeNoTancat['comandesLocalsTancades']!=1							
						&&
						(
							$mUsuari['id']==$mPerfil['usuari_id']
							||
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							||
							$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
						)
					)
					{
						if($mProducte['actiu']==0)
						{
							echo " name='i_".$key."' ";
							$campsGuardar.=','.$key;
						}
						else
						{
							if($unitatsReservades==0)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{						
								echo " name='i_".$key."' ";
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
					}
					else
					{
						echo " name='i_".$key."' ";
						echo " DISABLED style='background-color:#eeeeee;' ";
					}
	
					echo "
				 id='i_".$key."' size='1'>
					";
					for($i=0;$i<2;$i++)
					{
						if($mProducte['actiu']==$i)
						{
							$selected1='selected';$selected2='';}else{$selected1='';
						}
						echo "
				<option ".$selected1."  value=\"".$i."\">".$mActiu[$i]."</option>
						";
					}
					echo "
				</select>
					";
				
					if($mProducte['actiu']=='1')
					{
						echo "
				<img src='imatges/okp.gif'>
						";
					}
					else
					if($mProducte['actiu']=='0')
					{
						echo "
				<img src='imatges/nop.gif'>
						";
					}
					echo "
				</td>
			</tr>
					";
				}
				else if($key=='format')
				{
					$selected1='';
					$selected2='selected';
			
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				
				<td style='width:66%;' align='left'  valign='top'>
				<select 
					";
					if
					(
						$mPeriodeNoTancat!==false
						&&
						$mPeriodeNoTancat['comandesLocalsTancades']!=1
						&&
						(
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							||
							(
								(
									$mUsuari['id']==$mPerfil['usuari_id']
									||
									$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
								)
								&&
								$mProducte['actiu']=='0'
							)
						)
					)
					{
						echo " name='i_".$key."' ";
						$campsGuardar.=','.$key;
					}
					else
					{
						echo " DISABLED style='background-color:#eeeeee;' ";
					}
				
					echo "
				 id='i_".$key."' size='3'>
					";
					for($i=1;$i<2000;$i++)
					{
						if($mProducte['format']*1==$i)
						{
							$selected1='selected';$selected2='';}else{$selected1='';
						}
						echo "
				<option ".$selected1." value=\"".$i."\">".$i."</option>
						";
					}
					echo "
				<option ".$selected2." value=''></option>
				</select>
				</td>
			</tr>
					";
				}
				else if($key=='categoria0' || $key=='categoria10')
				{
					$selected='';
					if
					(
						$mPeriodeNoTancat!==false
						&&
						$mPeriodeNoTancat['comandesLocalsTancades']!=1
						&&
						(
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							||
							$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
						)
					)
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]." </p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<table>
					<tr>
						<td valign='top'>
				<p>
				<select id='i_".$key."' name='i_".$key."' size='3'>
						";
						while(list($key2,$categoria_)=each($mSubCategories[$key]))
						{
							if($categoria_==$val){$selected='selected';}else{$selected='';}

							echo "
				<option ".$selected." value=\"".$categoria_."\">".(urldecode($categoria_))."</option>
							";
						}
						reset($mSubCategories[$key]);
				
						echo "
				</select>[sadmin]
				</p>
						</td>
						<td valign='top'>
						";
						if($key=='categoria0')
						{
							echo "
						<p class='nota'> o la categoria: ".(html_ajuda1('html_gestioProductes.php',10))."</p>
						<input type='text' id='i_ncategoria0' name='i_ncategoria0' value=''>
							";
						}
						else
						if($key=='categoria10')
						{
							echo "
						<p class='nota'> o la sub-categoria: ".(html_ajuda1('html_gestioProductes.php',11))."</p>
						<input type='text' id='i_ncategoria10' name='i_ncategoria10' value=''>
							";
						}
						echo "
						</td>
					</tr>
				</table>
				</td>
			</tr>
						";
						$campsGuardar.=','.$key.',n'.$key;
					}
					else
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<p>
				<select id='i_".$key."' 
						";
						if($mPeriodeNoTancat!==false && $mPeriodeNoTancat['comandesLocalsTancades']==-1)					
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								$mProducte['actiu']=='0'
							)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
						else if($mPeriodeNoTancat!==false && $mPeriodeNoTancat['comandesLocalsTancades']==0)
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
							)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
						else
						{
							echo " DISABLED style='background-color:#eeeeee;' ";
						}
			
						echo "
				size='3'>
						";
						while(list($key2,$categoria_)=each($mSubCategories[$key]))
						{
							if($categoria_==$val){$selected='selected';}else{$selected='';}

							echo "
				<option ".$selected." value=\"".$categoria_."\">".(urldecode($categoria_))."</option>
							";
						}
						reset($mSubCategories[$key]);
						echo "
				<option  value=\"altres\">-altres-</option>
				</select>
				</p>
						<input type='hidden' id='i_ncategoria0' name='i_ncategoria0' value=''>
						<input type='hidden' id='i_ncategoria10' name='i_ncategoria10' value=''>
				</td>
			</tr>
						";
					}
					
				}
				else if($key=='producte' )
				{
					if
					(
						$mPeriodeNoTancat!==false
						&&
						$mPeriodeNoTancat['comandesLocalsTancades']!=1
						&&
						(
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							||
							$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
						)
					)
					{	
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'><input type='text' id='i_".$key."' name='i_".$key."' size='30' value=\"".(urldecode($val))."\"> [sadmin]</p>
				</td>
			</tr>
						";
						$campsGuardar.=','.$key;
					}
					else
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'><input type='text'
						";
						if($mPeriodeNoTancat!==false && $mPeriodeNoTancat['comandesLocalsTancades']==-1)					
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								$mProducte['actiu']=='0'
								&&
								substr_count($val,'plantilla')>0
							)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
						else 
						{
							if
							(
								$mPeriodeNoTancat!==false
								&&
								$mPeriodeNoTancat['comandesLocalsTancades']==0
								&&
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								substr_count($val,'plantilla')>0
							)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
						echo "
				  size='30' id='i_".$key."' name='i_".$key."' value=\"".(urldecode($val))."\"> </p>
				</td>
			</tr>
						";
					}
				}
				else if($key=='pes' || $key=='unitat_facturacio' || $key=='preu' || $key=='ms' || $key=='volum' )
				{
					$uts=' ';
					if($key=='pes' || $key=='preu' || $key=='ms' || $key=='volum' ){$size=5;}else{$size=30;}
					if($key=='pes'){$uts=' (kg.) ';}
					else if($key=='preu'){$uts=' (ums.) ';}
					else if($key=='volum'){$uts=' (l.) ';}
					else if($key=='ms'){$uts=' (%) ';}
			
					if
					(
						$mPeriodeNoTancat!==false
						&&
						$mPeriodeNoTancat['comandesLocalsTancades']!=1
						&&
						(
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							||
							$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
						)
					)
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'><input type='text' id='i_".$key."' name='i_".$key."' size='".$size."' value=\"".(urldecode($val))."\"> ".$uts." [sadmin]</p>
				</td>
			</tr>
						";
						$campsGuardar.=','.$key;
					}
					else
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'><input type='text'
						";
						if($mPeriodeNoTancat!==false && $mPeriodeNoTancat['comandesLocalsTancades']==-1)					
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
								&&
								$mProducte['actiu']=='0'
							)
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
						else if($mPeriodeNoTancat!==false && $mPeriodeNoTancat['comandesLocalsTancades']==0)
						{
							if
							(
								$mUsuari['id']==$mPerfil['usuari_id']
							)	
							{
								echo " name='i_".$key."' ";
								$campsGuardar.=','.$key;
							}
							else
							{
								echo " DISABLED style='background-color:#eeeeee;' ";
							}
						}
						else
						{
							echo " DISABLED style='background-color:#eeeeee;' ";
						}
						echo "
				  size='".$size."' id='i_".$key."' name='i_".$key."' value=\"".(urldecode($val))."\"> ".$uts."</p>
				</td>
			</tr>
						";
					}
				}
				else if($key=='estoc_disponible')
				{
					$size='5';
		
					if
					(
						$opt=='vell'
					)
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p><input type='text' READONLY   style='background-color:#eeeeee;'id='i_".$key."' name='i_".$key."' size='".$size."' value=\"".(urldecode($val))."\"> [sadmin]</p>
				</td>
			</tr>
						";
					}
				}
				else if($key=='tipus')
				{
					$size='5';
					if($key=='tipus'){$size=30;$valor=urldecode($val);}
					else{$valor=$val;}
		
					if
					(
						$mPeriodeNoTancat!==false
						&&
						$mPeriodeNoTancat['comandesLocalsTancades']!=1 
						&& 
						(
							($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
							||
							$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
						)
					)
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<p><input type='text' id='i_".$key."' name='i_".$key."' size='".$size."' value=\"".(urldecode($val))."\"> [sadmin]</p>
				</td>
			</tr>
						";
						$campsGuardar.=','.$key;
					}
					else
					{
						echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>

				<td style='width:66%;' align='left'  valign='top'>
				<input type='text' readonly style='background-color:#dddddd;' size='".$size."' id='i_".$key."' value=\"".$valor."\">
				</td>
			</tr>
						";
					}
				}
				else if($key=='cost_transport_extern_kg' || $key=='cost_transport_intern_kg')
				{
					echo "
				<input type='hidden' DISABLED style='z-index:0; top:0px; position:absolute; visibility:hidden; background-color:#dddddd;' size='".$size."' id='i_".$key."' value=\"0\">
					";
				}			
			}
		}
		reset($mProducte);
		echo "
		</table>
		";		
		if
		(
			($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
			||
			$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
		)
		{
			html_mostrarPropietatsProducte($db);
		}

		if
		(
			$mPeriodeNoTancat!==false
			&&
			$mPeriodeNoTancat['comandesLocalsTancades']!=1
			&&
			(
				($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
				||
				$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
			)
		)
		{
			echo "
		<td style='width:50%;' valign='top' align='center'>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�: (vista previa)</b></p>
		<div style='width:100%; height:250px; overflow-y:scroll; overflow-x:hidden;'>
		<table style='width:100%;'>
			<tr>
				<td style='width:100%;'>
				<p>".(urldecode($mProducte['notes_rebosts']))."<br><br></p>
				</td>
			</tr>
		</table>
		</div>
		<textArea cols='60' rows='20' type='text' id='i_notes_rebosts' name='i_notes_rebosts'>".(urldecode($mProducte['notes_rebosts']))."</textArea>[sadmin]
			";
			html_notesHTML();
			echo "
		</td>
			";
			$campsGuardar.=',notes_rebosts';
		}
		else if
		(	
			$mUsuari['id']==$mPerfil['usuari_id']
		)
		{
			echo "
		<td style='width:50%;' valign='top' align='center'>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�: (vista previa)</b></p>
		<div style='width:500px; height:250px; overflow-y:scroll;'>
		<table style='width:100%;'>
			<tr>
				<td style='width:100%;'>
				<p>".(urldecode($mProducte['notes_rebosts']))."</p>
				</td>
			</tr>
		</table>
		</div>
		
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�: (HTML)</b></p>
		<textArea cols='60' rows='20' ";
			if($mPeriodeNoTancat!==false && $mPeriodeNoTancat['comandesLocalsTancades']==1 && $mProducte['actiu']=='1')
			{
				echo " READONLY style='background-color:#eeeeee;' ";
			}
			else
			{
				$campsGuardar.=',notes_rebosts';
			}
		
			echo "
 		type='text' id='i_notes_rebosts' name='i_notes_rebosts'>".(urldecode($mProducte['notes_rebosts']))."</textArea>
			";
			html_notesHTML();
			echo "
		</td>
			";
		}
		else
		{
			echo "
		<td style='width:50%;' valign='top' align='center'>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�:</b></p>
		<div style='width:100%; height:250px; overflow-y:scroll; overflow-x:hidden;'>
		<table style='width:100%;'>
			<tr>
				<td style='width:100%;'>
				<p>".(urldecode($mProducte['notes_rebosts']))."</p>
				</td>
			</tr>
		</table>
		</div>
		</td>
			";
		}
	
		echo "
	</tr>

	<tr>
		<td>
		</td>

		<td align='center'>
		";
	
		if
		(
			$opt=='vell'
			&&
			(
				$mPeriodeNoTancat!==false
				&&
				$mPeriodeNoTancat['comandesLocalsTancades']!=1
				&&
				(
					($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
					||
					$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
					||
					(
						$mUsuari['id']==$mPerfil['usuari_id']
					)
				)
			)
		)
		{
			echo "
		<br>
		<input type='button' onClick=\"javascript: if(checkFormGuardarProductes()){guardarProducte();}\" value='guardar producte'>
		<br>
			";
		}
	
		if
		(
			$mPeriodeNoTancat!==false
			&&
			$mPeriodeNoTancat['comandesLocalsTancades']!=1							
			&&
			(
				($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
				||
				$mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
			)
		)
		{
			echo "
		<br>
		<a title=\"En guardar el producte actual com a nou producte es canviar� l'identificador, i el producte de partida no es modificar�\"><input type='button' onClick=\"javascript: if(checkFormGuardarProductes()){guardarNouProducte();}\" value='guardar com a nou producte'>
			";
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
			{
				echo "
		<font style='color:DarkOrange;'>[sadmin][RG]</font>
				";
			}
			else
			{
				echo "
		<font style='color:DarkOrange;'>[RG]</font>
				";
			}
			echo "
		</a>
		<br>
			";
		
			if
			(
				$opt=='vell' 
			)
			{
				echo "
		<br>
		<a title=\"Aquest producte nom�s es pot eliminar si no hi ha cap reserva en les comandes actuals\"><input type='button' onClick=\"javascript: eliminarProducte('".$mPars['selProducteId']."');\" value='eliminar producte'>
			";
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
			{
				echo "
		<font style='color:DarkOrange;'>[sadmin][RG]</font>
				";
			}
			else
			{
				echo "
		<font style='color:DarkOrange;'>[RG]</font>
				";
			}
			echo "
		</a>
		<br>
				";
			}		
		}
		$campsGuardar=substr($campsGuardar,1);
		echo "
		<input type='hidden' id='i_guardarProducte_id' name='i_guardarProducte_id' value=\"".$mPars['selProducteId']."\">
		<input type='hidden' id='i_opcio' name='i_opcio' value='guardar'>
		<input type='hidden' name='i_pars' value='".$parsChain."'>
		<input type='hidden' name='i_campsGuardar' value='".$campsGuardar."'>
		<input type='hidden' id='i_unitatsReservades' value='".$unitatsReservades."'>
		<input type='hidden' id='i_unitatsInventari' value='".@$mInventari['inventariRef'][$mPars['selProducteId']]."'>
		<br><br>
		</td>
	</tr>
</table>
</form>
<br>
	";
	}
	
	


	return;
}


//------------------------------------------------------------------------------
function html_notesAproductors()
{
	global $mPars,$mRutesSufixes,$mMesos,$mParametres;
	
//*v36 5-1-16 marca1 1
//*v36.2-marca1
	$mRuta=explode('_',array_pop($mRutesSufixes));
	if(count($mRuta)==2)
	{
		$ultimPeriodeAccessible=$mRuta[1];
	}
	else
	{
		$ultimPeriodeAccessible=$mRuta[0];
	}
	$mes=substr($ultimPeriodeAccessible,2);
	$any=substr($ultimPeriodeAccessible,0,2);
//marca1
//*v36 5-1-16 marca1 2
	echo "
	<table id='t_notes' align='center' width='80%' style='z-index:0; top:0px;'>
		<tr>
			<td width='100%' align='left'>
			<p class='nota' style='color:red;'><b>IMPORTANT</b></p>
			
			<p class='nota'><b>Edici� dels productes:</b>
			<br>
			<i>Quan es poden canviar les propietats d'un producte?</i>
			</p>
			<p class='nota'>
			El nom del producte nom�s es pot canviar quan es posa per primera vegada i es guarda. Hem aplicat aquesta mesura per evitar la reutilitzaci� 
			d'identificadors de producte per a productes diferents, ja que afectaria l'acc�s a l'historial del producte. Si necessiteu fer una modificaci� 
			en el nom del producte o necessiteu plantilles per a nous productes adreceu-vos a l'administrador. 
			</p>
			<p class='nota'>
			<i>Per editar els productes, si seleccioneu el periode en curs</i>,
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;<i> si el periode de precomandes est� obert</i>,
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>si el producte est� inactiu</i>,  <b>podeu modificar la majoria de valors i activar el producte</b>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>si el producte est� actiu</i>,
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i> si hi ha 0 unitats reservades</i>, <b>podeu modificar la majoria de valors i desactivar el producte</b>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i> si hi ha una � m�s unitats reservades</i>, <b>nom�s podeu modificar l'estoc previst i la imatge. No podeu desactivar el producte</b>
			<br>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;<i> si el periode de precomandes est� tancat</i>,
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i> si es abans del periode de precomandes, i encara s'ha d'obrir</i>, <b>podeu modificar la majoria de valors i activar/desactivar el producte</b>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i> si es despr�s del periode de precomandes, i ja s'ha tancat</i>,  <b>no podeu modificar cap valor, pero ja podeu modificar tots els valors d'aquest producte si accediu al periode seg�ent</b>
			<br>
			<br>
			<i>'estoc_previst' (quantitat que oferiu)
			<br>
			Per activar un producte 'estoc_previst' ha de ser major que zero i major o igual que el nombre d'unitats que la CAC t� en inventari.
			<br>
			<br>
			Dins el periode seg�ent (si ja est� creat),	donat que el periode seg�ent est� tancat i no hi ha reserves, podeu modificar gaireb� tots els camps i activar o desactivar els productes.
			<br>
			<br>
			<br>
			Si us trobeu que heu de canviar un preu o una dada err�nia i no podeu perqu� el periode de precomandes esta obert, teniu el producte actiu i s'ha reservat alguna unitat, poseu-vos en contacte amb l'administrador per tal que decideixi si cal anul.lar aquella reserva, o be canviar la dada err�nia i comunicar-ho a la part reservant.						
			<br><br>
			* �ltim periode accessible: ".$mMesos[$mes]." ".$any." (periode <b>".$ultimPeriodeAccessible."</b>)</p>

			<br>
			<p class='nota'><b>Etiquetatge de productes:</b></p>
			<p class='nota'>Per tal de reduir els errors en l'abastiment i distribuci�, necessitem un sistema d'etiquetatge estandard. 
			<br>Us demanem que les etiquetes dels vostres productes continguin la informaci� seg�ent (que ha de ser id�ntica a la que apareix a la fitxa del producte al gestor de comandes de la CAC:
			<br>
			- identificador CAC del producte (el podeu trobar a la fitxa del producte), especificat com : id:[identificador]
			<br>
			- nom del producte (tal com apareix a la llista de la CAC)
			<br>
			- ingredients
			<br>
			- data elaboraci�
			<br>
			- data caducitat
			<br>
			- pes (kg o grams)
			<br>
			- volum (si es tracta d'un liquid, o d'un volum major a 8 litres)
			<br>
			- contacte del productor (el telefon o email que apareix al perfil del productor)
			</p>
			
			<br>
			<p class='nota'><b>Etiquetatge de caixes per zona:</b></p>
			<p class='nota'> 
			Per tal de facilitar la ruta d'abastiment de la CAC us demanem des del mar� 2015 que prepareu 
			la comanda que us fa la CAC en paquets separats per zones.
			<br>
								A la nova versi� de l'albar� per la CAC la comanda est� ja separada per zones. Simplement cal que 
			empaqueteu els productes per zona i afegint l'albar� de zona, indicant:
			<br>
			Nom projecte
			<br>
			per la CAC
			<br>
			zona
			<br>
				nombre de paquets i	en quin heu posat l'albar� de zona
			<br>(ex: 1/3 +albar�, 2/3, etc...)
			<br>
			</p>
			<br>
			<p class='nota'><b>L'albar� de zona:</b></p>
			<p class='nota'> 
			Podeu obtenir l'albar� de zona imprimint el full d'albar� per la cac, accessible des d'aquesta p�gina, 
			i retallant el de cada zona per incloure'l al paquet corresponent
			</p>
			

			<br>
			<p class='nota'><b>Lliurament a la CAC:</b></p>
			<p class='nota'>* El lliurament a la CAC dels vostres productes que s'hagin reservat
			dins el periode en curs, s'ha de fer abans de la ruta d'abastiment de la CAC, que sol 
			ser dos o tres dies despr�s del tancament del periode de precomandes. Per el periode que ara teniu 
			seleccionat (".$mPars['selRutaSufix'].") la ruta d'abastiment es el dia <b>".$mParametres['iniciRutaAbastiment']['valor']."</b>.
			<br>
			El lliurament:
			<br>
			- s'ha de realitzar al lloc acordat amb la CAC
			<br>
			- ha d'estar adequadament empaquetat, de forma segura, 
			sense perill que es trenqui res en transportar-ho, amb un contenidor suficientment robust.
			<br>
			- cada paquet no pot pesar m�s de 20 kg, per tal de no carregar excessivament l'esquena de la gent que els ha de moure
			</p>

			<br>
			<p class='nota'><b>L'albar� de Totals:</b></p>
			<p class='nota'> 
			La taula amb el total reservat per aquest productor que apareix al final i es pot ocultar per imprimir els albarans de zona, 
			<br><b>Cal entregar-lo a la CAC en m�, el dia de ruta abastiment</b>. Si escollim un producte i sumem les quantitats a entregar a cada zona, 
			el resultat ha de coincidir amb la quantitat total a entregar a la CAC, que mostra aquesta taula.
			<br>
			Aquesta albar� pot variar a mesura que anem resolent incid�ncies, per tant <b>us recomanem que guardeu una copia fisica o digital d'aquesta p�gina en el moment de tancament de les precomandes</b>. Ja que es el vostre comprovant de la informaci� de la CAC de la que partiu per a fer l'entrega.
			<br>
			<br>
			Si un producte no apareix com a estoc, i us n'han reservat alguna unitat, i a 'quantitat a entregar a la CAC' la quantitat es menor que la quantitat reservada, significa que aquest producte estava al nostre inventari i ja s'ha entregat; per tant no l'heu d'entregar encara que no estigui en estoc a la CAC. La refer�ncia per al productor es sempre la columna '<b>quantitat a entregar a la CAC</b>'
			</p>
			</td>
		</tr>
	</table>			
	";
	
	return;
	
}

//------------------------------------------------------------------------------
function html_notesAproductorsGrup()
{
	global $mPars,$mMesos,$mParametres;
	
	
	
	echo "
	<table id='t_notes' align='center' width='80%' style='z-index:0; top:0px;'>
		<tr>
			<td width='100%' align='left'>
			<p class='nota' style='color:red;'><b>IMPORTANT</b></p>
			
			<p class='nota'><b>Edici� dels productes:</b>
			<br>
			<i>Quan es poden canviar les propietats d'un producte?</i>
			</p>
			<p class='nota'>
			El nom del producte nom�s es pot canviar quan es posa per primera vegada i es guarda. Hem aplicat aquesta mesura per evitar la reutilitzaci� 
			d'identificadors de producte per a productes diferents, ja que afectaria l'acc�s a l'historial del producte. Si necessiteu fer una modificaci� 
			en el nom del producte o necessiteu plantilles per a nous productes adreceu-vos al responsable del grup al que pertany la llista de productes que heu seleccionat. 
			</p>
			<p class='nota'>
			<i>Per editar els productes, si seleccioneu el periode en curs</i>,
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>si el producte est� inactiu</i>,  <b>podeu modificar la majoria de valors i activar el producte</b>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>si el producte est� actiu</i>,
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i> si hi ha 0 unitats reservades</i>, <b>podeu modificar la majoria de valors i desactivar el producte</b>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i> si hi ha una � m�s unitats reservades</i>, <b>nom�s podeu modificar l'estoc previst i la imatge. No podeu desactivar el producte</b>
			<br>
			<br>
			<i>'estoc_previst' (quantitat que oferiu)
			<br>
			Per activar un producte 'estoc_previst' ha de ser major que zero.
			<br>
			<br>
			Si us trobeu que heu de canviar un preu o una dada err�nia i no podeu perqu� el periode de precomandes esta obert, teniu el producte actiu i s'ha reservat alguna unitat, poseu-vos en contacte amb el responsable del grup al que pertany la llista de productes que heu seleccionat per tal que decideixi si cal anul.lar aquella reserva, o be canviar la dada err�nia i comunicar-ho a la part reservant.						
			<br><br>

			<br>
			<p class='nota'><b>Etiquetatge de productes:</b></p>
			<p class='nota'>Per tal de reduir els errors en l'abastiment i distribuci�, necessitem un sistema d'etiquetatge estandard. 
			<br>Us demanem que les etiquetes dels vostres productes continguin la informaci� seg�ent (que ha de ser id�ntica a la que apareix a la fitxa del producte al gestor de comandes de la CAC:
			<br>
			- identificador CAC del producte (el podeu trobar a la fitxa del producte), especificat com : id:[identificador]
			<br>
			- nom del producte (tal com apareix a la llista de productes del GRUP)
			<br>
			- ingredients
			<br>
			- data elaboraci�
			<br>
			- data caducitat
			<br>
			- pes (kg o gr)
			<br>
			- volum (l o ml)
			<br>
			- contacte del productor (el telefon o email que apareix al perfil del productor)
			</p>
			<br>
			<p class='nota'><b>L'albar� pel GRUP:</b></p>
			<p class='nota'> 
			Es la taula amb el total reservat per aquest productor que apareix al final de la p�gina. Podeu seleccionar un periode de la llista local (adalt) per veure el corresponent albar�.
			<br>
			Aquesta albar� pot variar a mesura que es resolguin incid�ncies, per tant <b>us recomanem que guardeu una copia fisica o digital d'aquesta p�gina en el moment de tancament de les reserves</b>. 
			</p>
			</td>
		</tr>
	</table>			
	";
	
	return;
	
}

//------------------------------------------------------------------------------
function html_activacioRapida($db)
{
	global $mPars,$mProductes,$mParametres,$parsChain;
	
	$mColBG=array();
	$mColBG['id']='';
	$mColBG['producte']='';
	$mColBG['format']='';
	$mColBG['unitat_facturacio']='';
	$mColBG['preu']='';
	$mColBG['ms']='';
	$mOrdreAr=explode('-',$mPars['ordreAr']);
	$mColBG[$mOrdreAr[0]]=" style='background-color:orange;' ";

	echo "
	<table id='t_activacioRapida' border='1' borderColor='green' width='60%' align='center' bgcolor='white'   style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
		<tr>
			<td align='left' width='100%' >
			<br>
			<center><p style='font-size:11px;'><b>Activaci� r�pida de productes</b>&nbsp;".(html_ajuda1('html_gestioProductes.php',4))."</center>
			<br>
			<table width='70%' align='center'>
				<tr>
					<td align='right'>
					</td>
					<td align='right'>
					</td>
					<td align='right'>
					<input type='button' value='X' onClick=\"javascript: ocultarFormAccio();\"
					</td>
				</tr>
				<tr>
					<td align='center'>
					</td>
					<td align='center'>
					<input type='button' value='guardar' onClick=\"javascript: checkActivacioRapida();\">
					</td>	
					<td align='center'>
					<input type='button' value='valors inicials' onClick=\"javascript: resetActivacioRapida();\">
					</td>
				</tr>
			</table>
			<br>
			<div style='height:300px; overflow-y:auto;' > 
			<form id='f_activacioRapida' name='f_activacioRapida' method='post' action='gestioProductes.php' target='_self'>
			<input type='hidden' name='i_pars' value='".$parsChain."'>
			<table width='90%'  bgcolor='LightGreen' align='center'>
				<tr>
					<td width='100%'>
					<table border='1' width='100%'>
						<tr>
							<th valign='bottom' >
							<p class='p_micro' ".$mColBG['id'].">id</p>
							</th>

							<th valign='bottom' >
							<p class='p_micro' ".$mColBG['producte'].">producte</p>
							</th>
	
							<td valign='bottom'>
							<p class='p_micro'>(altres dades)</p>
							</td>

							<th valign='bottom'>
							<p class='p_micro'>estat</p>
							</th>

							<th valign='bottom'>
							<p class='p_micro'>activar</p>
							</th>

							<th valign='bottom'>
							<p class='p_micro'>desactivar</p>
							</th>

							<th valign='bottom'>
							<p class='p_micro'>estoc previst</p>
							</th>

							<th valign='bottom'>
							<p class='p_micro'>SEGELL&nbsp;".(html_ajuda1('html_gestioProductes.php',9))."</p>
							</th>

						</tr>
	";
	$idsARchain=0;
	$cont=0;
	$mProductesRefKey=getProductesRefByKey($mOrdreAr[0],$mOrdreAr[1],$mProductes);
	while(list($id,$mProducte)=each($mProductesRefKey))
	{
		if(substr_count(strtolower($mProducte['producte']),'plantilla')==0)
		{
				$mPars['selProducteId']=$id;
			$unitatsReservades_=getUnitatsReservades($db);
			$estocGlobalProducte=db_getEstocGlobalProducte($id,$db);

			if($mProducte['actiu']=='1')
			{
				$estat="<p class='pVactiu' style='font-size:10px;'>actiu</p>";
				$cbActivar='DISABLED';
				$cbDesactivar='';
				if($unitatsReservades_>0 || $estocGlobalProducte>0)
				{
					$cbDesactivar='DISABLED';
				}
				
			}
			else if($mProducte['actiu']=='0')
			{
				$estat="<p class='pVinactiu' style='font-size:10px;'>inactiu</p>";
				$cbActivar='';
				$cbDesactivar='DISABLED';
			}
			echo "
						<tr>
							<td valign='top'>
							<input class='i_micro' size='6' id='i_ar_id_".$cont."' name='i_ar_id_".$mProducte['id']."' READONLY size='6' type='text' value='".$mProducte['id']."'>
							</td>

							<td valign='top' width='30%'>
							<p class='p_micro2'>".(urldecode($mProducte['producte']))."
			";
			if(substr_count($mProducte['tipus'],'dip�sit'))
			{
							echo "<font color='violet'><i>dip�sit</i></font>";
				$diposit=1;
			}
			echo "
							</p>
							</td>

							<td valign='top'>
							<p class='p_micro2'>
							<font  ".$mColBG['format']."><i><b>format</b></i>: ".$mProducte['format']." (unitats facturaci�)</font>
							<br>
							<font  ".$mColBG['unitat_facturacio']."><i><b>unitat facturaci�</b></i>: ".(urldecode($mProducte['unitat_facturacio']))."</font>
							<br>
							<font  ".$mColBG['preu']."><i><b>preu</b></i>: ".$mProducte['preu']." ums</font>
							<br>
							<font  ".$mColBG['ms']."><i><b>% ms</b></i>: ".$mProducte['ms']." %</font>
							</p>
							</td>

							<td valign='top'>
							<p>".$estat."</p>
							</td>

							<td valign='top'>
							<input type='checkbox' ".$cbActivar." id='cb_ar_ac_".$cont."' name='cb_ar_ac_".$mProducte['id']."'  value='-1' onClick=\"javascript: val=this.value;this.value*=-1;\">
							<input type='hidden' id='cb_ar_ac_".$cont."_mem' value='-1'>
							</td>

							<td valign='top'>
							<input type='checkbox' ".$cbDesactivar." id='cb_ar_d_".$cont."' name='cb_ar_d_".$mProducte['id']."'  value='-1' onClick=\"javascript: val=this.value;this.value*=-1;\">
							<input type='hidden' id='cb_ar_d_".$cont."_mem' value='-1'>
							</td>

							<td valign='top'>
							<p class='p_micro'>
							<input class='i_micro' id='i_ar_ep_".$cont."' name='i_ar_ep_".$mProducte['id']."' size='6' type='text' value='".$mProducte['estoc_previst']."'>
							<input type='hidden' id='i_ar_ep_".$cont."_mem'  value='".$mProducte['estoc_previst']."'>
							<br>
							Reservat:&nbsp;
			";
			$idsARchain.=','.$mProducte['id'];
							echo "<b>".$unitatsReservades_."</b>
							<br>
							Estoc&nbsp;CAB:&nbsp;<b>".$estocGlobalProducte."</b>
							</p>
							</td>
							
			";
			if($mPars['selLlistaId']==0)
			{
				if($mParametres['aplicarValorTallSegell']['valor']*1==1) //aplicar valortallsegell si est� actiu
				{
					if($mProducte['segell']*1>=$mParametres['valorTallSegell']['valor']*1)
					{
						echo "
							<td bgcolor='green' align='center'  valign='top'>
							<p class='p_micro' style='color:white;'>".$mProducte['segell']." % OK</p>
							</td>
						";
					}
					else
					{
						echo "
							<td bgcolor='red' align='center'  valign='top'>
							<p class='p_micro' style='color:white;'>".$mProducte['segell']." % KO</p>
							</td>
						";
					}
				}
				else
				{
					echo "
							<td  align='center' valign='top'>
							<p class='p_micro'>".$mProducte['segell']." %</p>
							</td>
					";
				}
				echo "
						</tr>
				";
			}
			$cont++;
		}
	}
	reset($mProductesRefKey);
		echo "
					</table>
					<input type='hidden' id='i_numProductesActivacioRapida' name='i_numProductesActivacioRapida' value='".$cont."'>
					<input type='hidden' id='i_ids_ar_chain' name='i_ids_ar_chain' value='".substr($idsARchain,1)."'>
					</td>
				</tr>
			</table>
			</form>
			</div>
			</td>
		</tr>
	</table>
	";

	return;
}

//------------------------------------------------------------------------------
function html_activacioRapidaLocal($db)
{
	global $mPars,$mProductes,$parsChain,$mPropietatsPeriodesLocals;
	
	$mColBG=array();
	$mColBG['id']='';
	$mColBG['producte']='';
	$mColBG['format']='';
	$mColBG['unitat_facturacio']='';
	$mColBG['preu']='';
	$mColBG['ms']='';
	$mOrdreAr=explode('-',$mPars['ordreAr']);
	$mColBG[$mOrdreAr[0]]=" style='background-color:orange;' ";

	echo "
	<table id='t_activacioRapida' border='1' borderColor='green' width='60%' align='center' bgcolor='white'   style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
		<tr>
		<td align='center' width='100%' >
		<br>
		<center><p style='font-size:11px;'><b>Activaci� r�pida de productes</b>&nbsp;".(html_ajuda1('html_gestioProductes.php',4))."</center>
		<br>
		<table width='70%'>
			<tr>
				<td align='right'>
				</td>
				<td align='right'>
				</td>
				<td align='right'>
				<input type='button' value='X' onClick=\"javascript: ocultarFormAccio();\"
				</td>
			</tr>
			<tr>
				<td align='center'>
				<input type='button' value='guardar' onClick=\"javascript: checkActivacioRapida();\">
				</td>
	
				<td align='center'>
				</td>

				<td align='center'>
				<input type='button' value='valors inicials' onClick=\"javascript: resetActivacioRapida();\">&nbsp;".(html_ajuda1('html_gestioProductes.php',9))."
				</td>
			</tr>
		</table>
		<br>
		<div style='height:300px; overflow-y:auto;' > 
		<form id='f_activacioRapida' name='f_activacioRapida' method='post' action='gestioProductes.php' target='_self'>
		<input type='hidden' name='i_pars' value='".$parsChain."'>
		<table width='90%' bgcolor='LightGreen' align='center'>
			<tr>
				<td width='100%'>
				<table border='1' width='100%'>
					<tr>
						<th valign='bottom' >
						<p class='p_micro' ".$mColBG['id'].">id</p>
						</th>

						<th valign='bottom' >
						<p class='p_micro' ".$mColBG['producte'].">producte</p>
						</th>
	
						<td valign='bottom'>
						<p class='p_micro'>(altres dades)</p>
						</td>

						<th valign='bottom'>
						<p class='p_micro'>estat</p>
						</th>
	
						<th valign='bottom'>
						<p class='p_micro'>activar</p>
						</th>
	
						<th valign='bottom'>
						<p class='p_micro'>desactivar</p>
						</th>
	
						<th valign='bottom'>
						<p class='p_micro'>estoc previst</p>
						</th>
					</tr>
	";
	$idsARchain=0;
	$cont=0;
	$mOrdreAr=explode('-',$mPars['ordreAr']);
	$mProductesRefKey=getProductesRefByKey($mOrdreAr[0],$mOrdreAr[1],$mProductes);
	while(list($id,$mProducte)=each($mProductesRefKey))
	{
		if(substr_count(strtolower($mProducte['producte']),'plantilla')==0)
		{
			if($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['comandesLocalsTancades']==1) // la taula de productes no correspon al periode
			{
				$unitatsReservadesVisibility='hidden'; 
			}
			else // la taula de productes correspon al periode
			{
				$unitatsReservadesVisibility='inherit';
			}
				$mPars['selProducteId']=$id;
			$unitatsReservades_=db_getUnitatsReservadesLocal($db);
			$estocGlobalProducte=0;

			if($mProducte['actiu']=='1')
			{
				$estat="<p class='pVactiu' style='font-size:10px;'>actiu</p>";
				$cbActivar='DISABLED';
				$cbDesactivar='';
				if($unitatsReservades_>0 || $estocGlobalProducte>0)
				{
					$cbDesactivar='DISABLED';
				}
				
			}
			else if($mProducte['actiu']=='0')
			{
				$estat="<p class='pVinactiu' style='font-size:10px;'>inactiu</p>";
				$cbActivar='';
				$cbDesactivar='DISABLED';
			}
			echo "
					<tr>
						<td valign='top'>
						<input class='i_micro' size='6' id='i_ar_id_".$cont."' name='i_ar_id_".$mProducte['id']."' READONLY size='6' type='text' value='".$mProducte['id']."'>
						</td>

						<td valign='top' width='30%'>
						<p class='p_micro'>".(urldecode($mProducte['producte']))."
					";
					if(substr_count($mProducte['tipus'],'dip�sit'))
					{
						echo "<font color='violet'><i>dip�sit</i></font>";
						$diposit=1;
					}
					echo "
						</p>
						</td>
	
						<td valign='top'>
						<p class='p_micro2'>
						<font  ".$mColBG['format']."><i><b>format</b></i>: ".$mProducte['format']." (unitats facturaci�)</font>
						<br>
						<font  ".$mColBG['unitat_facturacio']."><i><b>unitat facturaci�</b></i>: ".(urldecode($mProducte['unitat_facturacio']))."</font>
						<br>
						<font  ".$mColBG['preu']."><i><b>preu</b></i>: ".$mProducte['preu']." ums</font>
						<br>
						<font  ".$mColBG['ms']."><i><b>% ms</b></i>: ".$mProducte['ms']." %</font>
						</p>
						</td>
													
						<td valign='top'>
						".$estat."
						</td>

						<td valign='top'>
						<input type='checkbox' ".$cbActivar." id='cb_ar_ac_".$cont."' name='cb_ar_ac_".$mProducte['id']."'  value='-1' onClick=\"javascript: val=this.value;this.value*=-1;\">
						<input type='hidden' id='cb_ar_ac_".$cont."_mem' value='-1'>
						</td>

						<td valign='top'>
						<input type='checkbox' ".$cbDesactivar." id='cb_ar_d_".$cont."' name='cb_ar_d_".$mProducte['id']."'  value='-1' onClick=\"javascript: val=this.value;this.value*=-1;\">
						<input type='hidden' id='cb_ar_d_".$cont."_mem' value='-1'>
						</td>

						<td valign='top'>
						<input class='i_micro' id='i_ar_ep_".$cont."' name='i_ar_ep_".$mProducte['id']."' size='6' type='text' value='".$mProducte['estoc_previst']."'>
						<input type='hidden' id='i_ar_ep_".$cont."_mem'  value='".$mProducte['estoc_previst']."'>
						<p class='p_micro' style='visibility:".$unitatsReservadesVisibility."'>
						Reservat:&nbsp;
					";
					$idsARchain.=','.$mProducte['id'];
					echo "<b>".$unitatsReservades_."</b>
						</p>
						</td>
					</tr>
			";
			$cont++;
		}
	}
	reset($mProductesRefKey);
		echo "
				</table>
				<input type='hidden' id='i_numProductesActivacioRapida' name='i_numProductesActivacioRapida' value='".$cont."'>
				<input type='hidden' id='i_ids_ar_chain' name='i_ids_ar_chain' value='".substr($idsARchain,1)."'>
				</td>
			</tr>
		</table>
		</form>
	</div>
	</table>
	
	";

	return;
}

//------------------------------------------------------------------------------
function html_associarPerfilAgrup()
{
	global 	$mPars,
			$mGrupsRef,
			$mGrupsAssociatsRef;

		echo "

	<table id='t_associarPerfilAgrup' border='1' borderColor='green' width='50%' align='center' bgcolor='white'   style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
		<tr>
			<td align='center' width='100%' >
			<br>
			<p style='font-size:11px;'><b>Enviar una petici� per associar aquest perfil de productor al/s grup/s</b>: ".(html_ajuda1('html_gestioProductes.php',2))."</p>
			<table  width='40%' >
				<tr>
					<td align='right'>
					<input type='button' value='X' onClick=\"javascript: ocultarFormAccio();\"
					</td>
				</tr>
				<tr>
					<td>
					<select id='sel_grupsAssociar' name='sel_grupsAssociar[]' multiple style='font-size:10px;'>
		";
		
		while(list($grupId,$mGrup)=each($mGrupsRef))
		{
			$color='black';
			$bgColor='white';
			$disabled='';
			
			if(in_array($grupId,$mGrupsAssociatsRef))
			{
				$textAssociat="[associat]";
				$bgColor='LightGreen';
				$disabled='DISABLED';
			}
			else
			{
				$textAssociat="";
				//$bgColor='LightGreen';
				//$disabled='DISABLED';
			}

			echo "
					<option id='opt_grupAssociar_".$grupId."' ".$disabled." value='".$grupId."' style='color:".$color."; backGround-color:".$bgColor.";'>".(urldecode($mGrupsRef[$grupId]['nom']))." ".$textAssociat."</option>
			";
		}
		reset($mGrupsRef);
		
			echo "
					<option selected value=''></option>
					</select>
					<input type='button' onClick=\"javascript: enviarPeticioAssociarAperfilAgrup();\" value='enviar'>
					</td>
				</tr>
			</table>
			<br><br>
			&nbsp;
			</td>
		</tr>
	</table>
		";

	return;
}

//------------------------------------------------------------------------------

function html_transferirFitxes($db)
{
	global 	$parsChain,
			$mPars,
			$mPerfil,
			$mLlistesRef,
			$mProductes,
			$mGrupsRef,
			$mGrupsAssociatsRef;
	
	echo "
	<table id='t_transferirFitxes' border='1' borderColor='green' width='50%' align='center' bgcolor='white'   style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
		<tr>
			<td align='center' width='100%' >
			<br>
			<p style='font-size:11px;'><b>Sol.licitar la transfer�ncia de fitxes de productes a altres grups </b>: ".(html_ajuda1('html_gestioProductes.php',3))."</p>
			<form id='f_transferirFitxesProductes' name='f_transferirFitxesProductes' action='gestioProductes.php?pId=".$mPerfil['id']."' method='POST' target='_self'>
			<table  width='50%'>
				<tr>
					<td align='right'>
					<input type='button' value='X' onClick=\"javascript: ocultarFormAccio();\"
					</td>
				</tr>
				<tr>
					<td>
					<p style='font-size:11px;'>seleccionar la/es fitxa/es a transferir:</p>
					<select id='sel_fitxes' name='sel_fitxes[]' multiple  style='font-size:10px;'>
		";
		while(list($key,$mProducte_)=each($mProductes))
		{
			if(substr_count($mProducte_['producte'],'plantilla')==0)
			{
				echo "
					<option value=\"".$mProducte_['id']."\">".(urldecode($mProducte_['id']." - ".$mProducte_['producte']))."</option>
				";
			}
		}
		reset($mProductes);
		
		echo "
					<option selected value=''></option>
					</select>
					</td>
				</tr>

				<tr>
					<td>
					<p style='font-size:11px;'>seleccionar llista/es de desti:</p>
					<select id='sel_llistes' name='sel_llistes[]' multiple  style='font-size:10px;'>
					";
					if($mPars['selLlistaId']==0 && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin')
					{
						$llistaCabDisabled='DISABLED';
					}
					else
					{
						$llistaCabDisabled='';
					}
					echo "
					<option ".$llistaCabDisabled." style='color:DarkOrange;' value='0'>CAC (llista global)</option>
					";
		
		while(list($llistaId,$mLlista)=each($mLlistesRef))
		{
			if(!($mPars['usuari_id']==$mPerfil['usuari_id'] && $llistaId==0))
			{
				$color='black';
				$bgColor='white';
			
				if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
				{
					if(!in_array($llistaId,$mGrupsAssociatsRef))
					{
						$textNivell='';
					}
					else
					{
						$textNivell='';
						$color='black';
						$bgColor='LightOrange';
					}
				}
				else
				{
					$textNivell='';
				}
				if($llistaId==$mPars['selLlistaId'])
				{
					$disabled='DISABLED';
				}
				else
				{
					$disabled='';
				}	
			
				$numProductes=db_getNumProductesPerfilAllista($mPerfil['id'],$llistaId,$db);
				if($numProductes>0){$color='white'; $bgColor='green';}

				if($mPars['selLlistaId']==$llistaId)
				{
					$selected='selected';
				}
				else
				{
					$selected='';
				}

				echo "
			<option id='opt_selLlista_".$llistaId."' ".$selected." ".$disabled." value='".$llistaId."' style='color:".$color."; backGround-color:".$bgColor.";'>".(urldecode($mGrupsRef[$llistaId]['nom']))." (".$numProductes." productes) ".$textNivell."</option>
				";
				$numProductes=0;
			}
			
		}
		reset($mLlistesRef);
		echo "
					<option selected value=''></option>
					</select>
					</td>
				</tr>
		";
		if
		(
			($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
		)
		{
			$periodeStyle="style='top:0px; z-indez:0; position:relative; visibility:inherit;'";
		}
		else
		{
			$periodeStyle="style='top:0px; z-indez:0; position:absolute; visibility:hidden;'";
		}
		echo "
				<tr ".$periodeStyle.">
					<td align='left'>
					<p style='font-size:11px;'>selecciona un periode de desti <font color='DarkOrange'>[sadmin]</font>:</p>
		";
		mostrarSelectorRuta(0,'gestioProductes.php');
		echo "
					</td>
				</tr>
				<tr>
					<td align='right'>
					<input type='button' onClick=\"javascript: peticioTransferenciaProductes();\" value='enviar'>
					</td>
				</tr>
			</table>
			<br><br><br>&nbsp;
			<input type='hidden' name='i_pars' value='".$parsChain."'>
			</form>
			</td>
		</tr>
	</table>
		";
	
	return;
}

//------------------------------------------------------------------------------		
		
function html_peticioNovesPlantilles()
{
	global 	$numProductesLlista,	
			$mPars,
			$mGrupsRef,
			$mParametres;

	echo "
	<table id='t_peticioNovesPlantilles' border='1' borderColor='green' width='40%' align='center' bgcolor='white'   style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
		<tr>
			<td align='center' width='100%' >
			<br>
			<p  style='font-size:11px;'><b>Solicitar noves plantilles a la llista ".(urldecode($mGrupsRef[$mPars['selLlistaId']]['nom']))."</b>: 
			&nbsp;".(html_ajuda1('html_gestioProductes.php',1))."
			</p>
			<table width='50%' align='center'>
				<tr>
					<td align='right'>
					</td>
					<td align='right'>
					<input type='button' value='X' onClick=\"javascript: ocultarFormAccio();\"
					</td>
				</tr>
				<tr>
					<td>
					<p class='p_micro2'>Nombre de plantilles:</p>
					</td>
					<td>
					<select id='i_numPlantilles' name='i_numPlantilles'>
					";
					for($i=0;$i<10;$i++)
					{
						echo "
					<option value='".$i."'>".$i."</option>
						";
					}
					echo "
					</select>
					</td>
				</tr>
			</table>
			<textArea class='ta10' id='ta_comentariNP' name='ta_comentariNP' cols='30' rows='4'>[Notes]
			</textArea>
			<input type='button' onClick=\"javascript: peticioNovesPlantilles();\" value='enviar'>
			<p>[ total en llista local: ".$numProductesLlista." / ".$mParametres['limitNombreProductesLocals']['valor']." productes]</p>			
				<br>
				<br>
				<br>
				&nbsp;
			</td>
		</tr>
	</table>
	";
	
	return;
}


function html_pujarImatge()
{
	global $mPars,$mProducte,$mGrupsRef,$parsChain,$thumbCode;
	
	echo "
	<table id='t_pujarImatge' border='1' borderColor='green' width='40%' align='center' bgcolor='white'   style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
		<tr>
			<td align='center' width='100%' >
			<br>
			<p  style='font-size:11px;'>Pujar una imatge pel producte 
			<br>
			<b>id:".$mProducte['id']." (".(urldecode($mProducte['producte'])).")</b>
			<br> 
			de la llista: <b>".(urldecode($mGrupsRef[$mPars['selLlistaId']]['nom']))."</b>: 
			</p>
			<table width='50%' align='center'>
				<tr>
					<td align='right'>
					</td>
					<td align='right'>
					<input type='button' value='X' onClick=\"javascript: ocultarFormAccio();\"
					</td>
				</tr>

				<tr>
					<td style='width:15%;' align='left'  valign='top'>
					<FORM name='f_pujarImatge' METHOD='post' ACTION='gestioProductes.php' ENCTYPE='multipart/form-data'>
					<INPUT TYPE='hidden' NAME='i_imatgeIdProducte' value='".$mProducte['id']."'>
					<INPUT TYPE='file' size='8' NAME='i_imatge' ACCEPT='image/jpeg'>
					<input type='hidden' name='i_pars' value='".$parsChain."'>
					<br>
					<br>
					<input type='submit' value='pujar imatge'>
					</form>
					<br>
					<table align='center' width='80%'>
						<tr>
							<td align='center' width='30%'>
							<p><b>Imatge:</b></p>
							</td>
							<td align='center' width='70%'>
	";
	if($mProducte['imatge']!='')
	{
		if($mPars['selLlistaId']==0)
		{
			echo "
							<img  src='productes/".$mProducte['id']."/".$thumbCode.'_'.$mProducte['imatge']."'>
			";
		}
		else
		{
			echo "
							<img  src='llistes/".$mPars['selLlistaId']."/productes/".$mProducte['id']."/".$thumbCode.'_'.$mProducte['imatge']."'>
			";
		}
		echo "
							</td>
						</tr>
					</table>
		";
	}
	else
	{
		echo "
					<p>- No s'ha pujat cap imatge -</p>
		";
	}
	echo "
					<br><br>&nbsp;
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	";
		
	return;
}
?>

		