<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include "csv_local.php";
include "html_ajuda1.php";
include "db_ajuda.php";

$parsChain=@$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db); //inicialitza variables anteriors;
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['db_llistaProductesCSV.php']=db_getAjuda('db_llistaProductesCSV.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$mPropietatsPeriodesLocals=db_getPropietatsPeriodesLocals($db);
$mPropietatsPeriodeLocal=@$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']];
$mPeriodesLocalsInfo=db_getPeriodesLocalsInfo($db);
if($mPars['selLlistaId']!=0)
{
	if(!isset($mPars['sel_periode_comanda_local']) || $mPars['sel_periode_comanda_local']=='')
	{
		$mPropietatsPeriodesLocals_=array_keys($mPropietatsPeriodesLocals);
		$periodeLocal=array_shift($mPropietatsPeriodesLocals_);
		$mPars['periode_comanda_local']=$periodeLocal;
		$mPars['sel_periode_comanda_local']=$mPars['periode_comanda_local'];
	}
	$mDatesReservesLocals=getDatesReservesLocals();
}
else
{
	$mPars['periode_comanda_local']='';
	$mPars['sel_periode_comanda_local']='';
	$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['comandesLocalsTancades']=1;
}
$mPars['vPeriodeLocalIncd']=@$mPars['sel_periode_comanda_local'];	//per getIncidencies();

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat (login)</p>
	";
	exit();
	$login=false;
}

	$mCampsProductes=array();
	$mGrup=getGrup($db);
	$mPropietatsGrup=@getPropietatsGrup($mGrup['propietats']);
if
(
	$mPars['selLlistaId']==0
	||
	(
		$mPars['selLlistaId']!=0
		&&
		$mPropietatsPeriodeLocal['comandesLocalsTancades']!=1
	)
)
{
	
	if($mPars['selLlistaId']==0 && $mPars['grup_id']==0)
	{
		$mProductes_=db_getProductes2($db);
		
		$mProductes=array();
		$i=0;
		while(list($key,$mVal)=each($mProductes_))
		{
			$mProductes[$i]=$mVal;
			$i++;
		}
		reset($mProductes_);
		$mPars['numItemsF']=count($mProductes);
		$mPars['numPags']=CEIL($mPars['numItemsF']/$mPars['numItemsPag']);
	}
	else
	{
		$mProductes=db_getProductes($db);
	}
}
else
{
	$mProductes=csv_getProductesLocal($db);
}
$parsChain=makeParsChain($mPars);
//------------------------------------------------------------------------

function db_getLlistaProductesCSV($db)
{
	global $mPars,$mProductes;
	$mColumnesProductesLlista=array('id','producte','productor','categoria0','categoria10','tipus','unitat_facturacio','pes','preu','ms','format');
	
	
	$mProductesLlista=array();
	$i=0;

	While(list($key,$mProducte)=each($mProductes))
	{
			$mProducte_['id']=urldecode($mProducte['id']);
			$mProducte_['producte']=urldecode($mProducte['producte']);
			$mProducte_['productor']=urldecode($mProducte['productor']);
			$mProducte_['categoria0']=$mProducte['categoria0'];
			$mProducte_['categoria10']=$mProducte['categoria10'];
			if(substr($mProducte['tipus'],0,1)==',')
			{
				$mProducte_['tipus']=substr($mProducte['tipus'],1);
			}
			$mProducte_['tipus']=str_replace(',','
',@$mProducte_['tipus']);
			$mProducte_['tipus']=str_replace(',','',$mProducte_['tipus']);
			$mProducte_['unitat_facturacio']=urldecode($mProducte['unitat_facturacio']);
			$mProducte_['pes']=str_replace('.',',',(number_format($mProducte['pes'],2)));
			$mProducte_['preu']=str_replace('.',',',(number_format($mProducte['preu'],2)));
			$mProducte_['ms']=$mProducte['ms'];
			$mProducte_['format']=$mProducte['format'];
			$mProductesLlista[$i]=$mProducte_;
			$i++;
	}
	reset($mProductes);
	@unlink("llistaProductes/pre_llistaProductes_".$mPars['usuari_id'].".csv");

	if(!$fp=fopen("llistaProductes/pre_llistaProductes_".$mPars['usuari_id'].".csv",'w'))
	{
		return false;
	}

	if($mPars['selLlistaId']==0)
	{
		$mH1[0]='Llista CAC actual de productes';
		fputcsv($fp, $mH1,',','"');
		$mH1[0]='ruta: '.$mPars['selRutaSufix'];
		fputcsv($fp, $mH1,',','"');
	}
	else
	{
		$mH1[0]='Llista LOCAL actual de productes';
		fputcsv($fp, $mH1,',','"');
	}
	$mH1[0]=date('d-m-Y h:m');
	fputcsv($fp, $mH1,',','"');
	$mH1[0]=' ';
	fputcsv($fp, $mH1,',','"');
	$mH1[0]='Filtre:';
	fputcsv($fp, $mH1,',','"');

	if($mPars['veureProductesDisponibles']==1)
	{
		$mH1[0]='- nom�s es mostren els productes actius';
	}
	else
	{
		$mH1[0]='- es mostren els productes actius i els inactius';
	}
	fputcsv($fp, $mH1,',','"');
	if($mPars['excloureProductesJaEntregats']==1)
	{
		$mH1[0]="- s'exclouen els productes ja entregats";
	}
	else
	{
		$mH1[0]="- NO s'exclouen els productes ja entregats";
	}
	fputcsv($fp, $mH1,',','"');
	
	$mH1[0]='- productors: '.$mPars['vProductor'];
	fputcsv($fp, $mH1,',','"');
	$mH1[0]='- categoria: '.$mPars['vCategoria'];
	fputcsv($fp, $mH1,',','"');
	$mH1[0]='- sub-categoria: '.$mPars['vSubCategoria'];
	fputcsv($fp, $mH1,',','"');
	
	if($mPars['etiqueta']!='TOTS')
	{
		$mH1[0]='- nom�s es mostren els productes amb etiqueta: '.$mPars['etiqueta'];
		fputcsv($fp, $mH1,',','"');
	}
	
	if($mPars['etiqueta2']!='CAP')
	{
		$mH1[0]='- NO es mostren els productes amb etiqueta: '.$mPars['etiqueta2'];
		fputcsv($fp, $mH1,',','"');
	}
	
	$mH1[0]='- odre llista: '.$mPars['sortBy'].' - '.$mPars['ascdesc'];
	fputcsv($fp, $mH1,',','"');
	$mH1[0]=' ';
	fputcsv($fp, $mH1,',','"');
	

	
	fputcsv($fp, $mColumnesProductesLlista,',','"');
	
	while(list($key,$mProducteLlista)=each($mProductesLlista))
	{
    	if(!fputcsv($fp, $mProducteLlista,',','"'))
		{
			return false;
		}
	}	
	fclose($fp);
	return;
}
//------------------------------------------------------------------------

if($mPars['selLlistaId']==0){$llistaText='CAC';}else{$llistaText='LOCAL';}
db_getLlistaProductesCSV($db);


$mLlistaText[1]='LOCAL';
echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<script type='text/javascript'  language='JavaScript'>
function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
		
	return;
}
</script>
<head>
<title>Llista ".$llistaText." actual de productes</title>
</head>
<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('db_llistaProductesCSV.php?');
echo "
	<table align='center' style='width:90%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</p>
			</td>
		</tr>
		<tr>
			<td style='width:100%;' align='center'>
			<br><br><br>
			<a href='llistaProductes/pre_llistaProductes_".$mPars['usuari_id'].".csv'>llista ".$llistaText." de productes</a>
			<br>
			<br>
			<p class='nota'>* No apareixen els productes actius amb estoc_previst=0</p>
			</td>
		</tr>
	</table>
";
html_helpRecipient();

echo "
<form id='f_pars' name='f_pars' method='post' action='db_llistaProductesCSV.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>

</body>
</html>

";

?>

		