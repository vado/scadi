<?php
//-----------------------------------------------------------------------
function db_getInventariPerProductors($db)
{
	global $mProductes,$mPars,$mParametres,$mMagatzems;

	$mInventariPerProductors=array();
	$mInventariTMP=array();
	$mInventaris=array();

	
/*
			$mDataFiPrecomandes=explode('-',$mParametres['dataFiPrecomandes']['valor']);
			$dataMax = new DateTime();
			$dataMax->setDate('20'.$mDataFiPrecomandes[2],$mDataFiPrecomandes[1],$mDataFiPrecomandes[0]);
			$dataMax->setTime(21,0,0);
			$tMax=$dataMax->getTimestamp();
*/
		while(list($key,$mMagatzem)=each($mMagatzems))
		{
			$mRow=array();
			$mPeriodes[$mMagatzem['ref']]=array();
			//obtenir periodes de precomanda DESC:
			//echo "<br>select periode_comanda,id,num_inventari from inventaris_".(substr($mPars['selRutaSufix'],0,2))." WHERE magatzem_ref='".$mMagatzem['ref']."' AND periode_comanda='".$mParametres['periodeComanda']['valor']."' AND magatzem_ref='".$mMagatzem['ref']."' ORDER BY id DESC limit 0,1";
			if(!$result=mysql_query("select periode_comanda,id,num_inventari from inventaris_".(substr($mPars['selRutaSufix'],0,2))." WHERE periode_comanda='".$mParametres['periodeComanda']['valor']."' AND magatzem_ref='".$mMagatzem['ref']."' ORDER BY id DESC limit 0,1",$db))
			{
				//echo "<br> 264 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
			
   				return false;
			}
			else
			{
				$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
				//echo "<br>select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." WHERE  id='".$mRow['id']."'";
				$result=@mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." WHERE  id='".$mRow['id']."'",$db);
				//echo "<br> 264 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
			
				$mRow_=mysql_fetch_array($result,MYSQL_ASSOC);
				if($mRow_){$mRow=$mRow_;}
				/*
				while($mRow=mysql_fetch_array($result,MYSQL_NUM))
				{
					$mRuta0=explode(':',$mRow[0]);
					$mRuta1=explode('-',$mRuta0[0]);
					$mPeriodes[$mMagatzem['ref']][$mRow[2]]=$mRow[1];
				}
				ksort($mPeriodes[$mMagatzem['ref']]);
				*/
			}
/*
			$mPeriodes_=$mPeriodes[$mMagatzem['ref']];
			$cont=count($mPeriodes[$mMagatzem['ref']]);
			$mRow=false;
				$idFinal=0;
			while(list($data,$val)=each($mPeriodes_))
			{
				$data_ = new DateTime();
				$data_->setDate(1*(substr($data,0,4)),1*(substr($data,4,2)),1*(substr($data,6,2)));
				$data_->setTime(1*(substr($data,8,2)),1*(substr($data,10,2)),1*(substr($data,12,2)));

				$t=$data_->getTimestamp();
				if($t<$tMax)
				{
					$idFinal=$val;
				}
			}
*/
			
			$mInventaris[$mMagatzem['ref']]=$mRow;
		}
		reset($mMagatzems);

		while(list($magatzemRef,$mInventari)=each($mInventaris))			
		{
		//vd($mInventari);
			$mProductesInventari=explode(';',$mInventari['resum']);

			for($i=0;$i<count($mProductesInventari);$i++)
			{
				$mIndexQuantitat=explode(':',$mProductesInventari[$i]);
		
				$id=str_replace('producte_','',$mIndexQuantitat[0]);
				$quantitat=@$mIndexQuantitat[1];
				if($id!='' && $id!=0 && $quantitat>0)
				{
					if(!isset($mInventariTMP[$magatzemRef]))
					{
						$mInventariTMP[$magatzemRef]=array();
					}
					$mInventariTMP[$magatzemRef][$id]=$quantitat;
				}
			}
		}
		reset($mInventaris);

				
	// ordenar per productor i afegir TMP i rebosts
	if($mPars['veureProductesDisponibles']==1)
	{
		if($mPars['paginacio']==1)
		{
			while(list($index,$mProducte)=each($mProductes))
			{
				if($mProducte['actiu']==1 && $mProducte['visible']==1)
				{
					while(list($magatzemRef,$mInventariMagatzem)=each($mInventariTMP))
					{
						while(list($id,$quantitat)=each($mInventariMagatzem))
						{
							if($id==$mProducte['id'])
							{
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id][$magatzemRef]=$quantitat;
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['producte']=$mProducte['producte'];
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['id']=$mProducte['id'];
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['tipus']=$mProducte['tipus'];
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['actiu']=$mProducte['actiu'];
							}
						}
						reset($mInventariMagatzem);
					}
					reset($mInventariTMP);
				}
			}
			reset($mProductes);
		}
		else
		{
			while(list($index,$mProducte)=each($mProductes))
			{
				if($mProducte['actiu']==1)
				{
					while(list($magatzemRef,$mInventariMagatzem)=each($mInventariTMP))
					{
						while(list($id,$quantitat)=each($mInventariMagatzem))
						{
							if($id==$mProducte['id'])
							{
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id][$magatzemRef]=$quantitat;
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['producte']=$mProducte['producte'];
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['id']=$mProducte['id'];
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['tipus']=$mProducte['tipus'];
								$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['actiu']=$mProducte['actiu'];
							}
						}
						reset($mInventariMagatzem);
					}
					reset($mInventariTMP);
				}
			}
			reset($mProductes);
		}
		
	}
	else
	{
		if($mPars['paginacio']==1)
		{

		while(list($index,$mProducte)=each($mProductes))
		{
			if($mProducte['visible']==1)
			{
				while(list($magatzemRef,$mInventariMagatzem)=each($mInventariTMP))
				{
					while(list($id,$quantitat)=each($mInventariMagatzem))
					{
						if($id==$mProducte['id'])
						{
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id][$magatzemRef]=$quantitat;
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['producte']=$mProducte['producte'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['id']=$mProducte['id'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['tipus']=$mProducte['tipus'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['actiu']=$mProducte['actiu'];
						}
					}
					reset($mInventariMagatzem);
				}
				reset($mInventariTMP);
			}
		}
		reset($mProductes);
		}
		else
		{

			while(list($index,$mProducte)=each($mProductes))
			{
				while(list($magatzemRef,$mInventariMagatzem)=each($mInventariTMP))
				{
					while(list($id,$quantitat)=each($mInventariMagatzem))
					{
						if($id==$mProducte['id'])
						{
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id][$magatzemRef]=$quantitat;
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['producte']=$mProducte['producte'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['id']=$mProducte['id'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['tipus']=$mProducte['tipus'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['actiu']=$mProducte['actiu'];
						}
					}
					reset($mInventariMagatzem);
				}
				reset($mInventariTMP);
			
			}
			reset($mProductes);
		}
		
	}
	//vd($mInventariPerProductors);
	return $mInventariPerProductors; 
}
?>

		