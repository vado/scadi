<?php
include "db.php";
include "eines.php";

//------------------------------------------------------------------------
function recepcioFitxer()
{
    global $mPars;

    $missatgeError='';

	$dir0=getcwd();
	
	$dirChain='';
	$dir=@$_POST['sel_directori'];
	if(!@chdir('old'))
	{
		mkdir('old');
	}
	else
	{
		chdir($dir0);
	}

	if($dir!='')
	{
		$dirChain=$dir.'/';
		if(!@chdir($dir))
		{
			mkdir($dir);
		}
		else
		{
			chdir($dir0);
		}
	}
	
    if($_FILES['f_fitxer']['name']!='')
    {
        if (is_uploaded_file($_FILES['f_fitxer']['tmp_name']))
        {
            //mirar si l'extensio �s correcta:
            $size=@pathinfo($_FILES['f_fitxer']['name']);

            $nomFitxer=$_FILES['f_fitxer']['name'];
			if($dir=='')
			{
				@rename('old/old_'.$nomFitxer,'old_'.$nomFitxer);
			}
    
	        if(!copy($_FILES['f_fitxer']['tmp_name'],$dirChain.$nomFitxer))
            {
                $missatgeError.="<br> Atenci�: Error en copiar (".$dirChain.$nomFitxer.").";
				if($dir=='')
				{
					rename('old/old_'.$nomFitxer,$nomFitxer);
				}
            }
		}
    }

    if(strlen($missatgeError)==0)
	{
        $missatgeError.="<br> Ok: El fitxer s'ha guardat correctament.";
    }

return $missatgeError;
}
//--------------------------------------------------------------------


$missatgeAlerta='';


$mMissatgeAlerta=array();

$op=@$_POST['i_op'];
if(isset($op))
{
	if($op=='pujarFitxer')
	{
		$missatgeAlerta=recepcioFitxer();
	}
	else if($op=='crearDir')
	{
		$nouDir=@$_POST['i_dir'];
		if($nouDir!='')
		{
			if(!mkDir($nouDir))
			{
				$missatgeAlerta="Atenci�. no s'ha pogut crear el nou directori";
			}
			else
			{
				$missatgeAlerta="ok, s'ha creat el nou directori correctament";
			}
		
		}
	}
}

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>pujar fitxers</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
</SCRIPT>
</head>
<body onLoad=\"\">

	<table align='center' style='width:80%'>
		<tr>
			<th style='width:100%;' align='center'>
		";
		//	<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
//			".$mContinguts['index']['titol1']."</p>
echo "
			</td>
		</tr>
	</table>

	<table align='center' style='width:80%'>
		<tr>
			<th style='width:100%;' align='center'>
			".$missatgeAlerta."
			</td>
		</tr>
	</table>
";

if(count($mMissatgeAlerta)>0)
{
	echo "
	<table align='center' border='1' width='60%'>
		<tr>
			<td  bgcolor='#eeeeee'  align='left'  width='30%' >
			<br>
			<p>pujades:</p>
			<br>
	";
	while(list($key,$mVal)=each($mMissatgeAlerta))
	{
		echo "
			<p>".$mVal['fitxer']." >  ".$mVal['result']."</p>
			";
	}
	reset($mMissatgeAlerta);
	echo "
			</td>
		</tr>
	</table>
	";
}
echo "
	<table align='center' bgcolor='#dddddd' width='70%' align='center'>
		<tr>
			<td  bgcolor='#eeeeee'  width='100%' >
			<table width='80%' border='0' align='center'>
				<tr>
					<td width='90%' align='center'>
					<table width='100%'>
					<FORM name='f_pujarFitxer' METHOD='post' action='uploadF3.php' target='_self'  ENCTYPE='multipart/form-data'>
					<INPUT TYPE='file' size='8' NAME='f_fitxer' accept='.php'>
					";
					//<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
					echo "
					<input type='hidden' id='i_op' name='i_op' value='pujarFitxer'>
					<input type='submit' value='pujar fitxer'>
					<p>sub-directori:<br>
					<select id='sel_directori' name='sel_directori'>
					";
					$dir=getcwd();
					$mScan=scandir($dir);
					$mDirs=array();
					$mFiles=array();
					
					while(list($key,$val)=each($mScan))
					{
						if($val!='.' && $val!='..')
						{
							if(@chdir($val))
							{
								chdir($dir);
								array_push($mDirs,$val);
							}
							else
							{
								array_push($mFiles,$val);
							}
						}
					}
					reset($mScan);
					sort($mDirs);
					sort($mFiles);
					
					while(list($key,$val)=each($mDirs))
					{
						echo "
					<option value='".$val."'>".$val."</option>						
						";
					}
					echo "
					<option selected value=''></option>						
					</select>
					</p>
					</form>
					<p>Arxius:<br>
					<select>
					";
					
					while(list($key,$val)=each($mFiles))
					{
						echo "
					<option value='".$val."'>".$val."</option>						
						";
					}
					echo "
					</select>
					</p>
					</table>
					<br>
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>

	<table align='center' bgcolor='#dddddd' width='70%' align='center'>
		<tr>
			<td  bgcolor='#eeeeee'  width='100%' >
			<p>crear directori:</p>
			<table width='80%' border='0' align='center'>
				<tr>
					<td width='90%' align='center'>
					<table width='100%'>
					<FORM name='f_crearDirectori' METHOD='post' action='uploadF3.php' target='_self'>
					";
					//<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
					echo "
					<input type='hidden' id='i_op' name='i_op' value='crearDir'>
					<p>nou directori:<br>
					<input type='text' id='i_dir' name='i_dir' value=''>
					<input type='submit' value='crear directori'>
					</p>
					</form>
					</table>
					<br>
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
</body>
</html>
";

?>

			