isValid=false;
missatgeAlerta='';
allowedCharsMobil='0123456789';
allowedCharsFloat='0123456789.';


function checkAfegirIncidencia()
{

	tipusIncidencia=document.getElementById('i_tipusIncidencia').value;
	if(tipusIncidencia=='producte' || tipusIncidencia=='jaEntregat' || tipusIncidencia=='pendentEntrega')
	{
		value=document.getElementById('i_producteId').value;
		if(value=='')
		{
			missatgeAlerta+="\nAtenci�: El camp 'id' ha de contenir l'identificador d'un producte.";			
		}
		else if(value!='')
		{
       		allowedChars=allowedCharsMobil;
			$_ok=0;
			//check for prohibited characters
       		for(i=0;i<value.length;i++)
        	{
            	for(j=0;j<allowedChars.length;j++)
            	{
	                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
    	        }
			}
        	if($_ok<value.length)
        	{
				missatgeAlerta+="\nEl camp 'id' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        	}
		}
	}
	else
	{
		valor=document.getElementById('ta_comentaris').value;
		if(valor=='')
		{
			missatgeAlerta+="\nAtenci�: El camp 'comentari' ha de contenir la descripci� de la incid�ncia.";			
		}
	}

	value=document.getElementById('i_producteRebut').value;
	if(value!='')
	{
       	allowedChars=allowedCharsMobil;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<value.length)
        {
			missatgeAlerta+="\nEl camp 'rebut' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        }
		else
		{
			demanat=document.getElementById('i_producteDemanat').value;
			if(tipusIncidencia=='jaEntregat')
			{
				if(value*1>demanat*1 || value*1<=0)
        		{
					missatgeAlerta+="\nEl camp 'rebut' ha de ser major que 0 i no pot ser major al demanat. Si es major heu d'introduir un altre tipus d'incid�ncia";			
        		}
			}
			else if(tipusIncidencia=='pendentEntrega')
			{
				if(value*1>=demanat*1)
        		{
					missatgeAlerta+="\nEl camp 'rebut' no pot ser igual o major al demanat";			
        		}
			}
		}
	}
	else if(tipusIncidencia=='jaEntregat')
	{
		missatgeAlerta+="\nEl camp 'rebut' ha de contenir un valor positiu i enter";			
	}
	
	
	if (missatgeAlerta.length==0)
	{
		return true;
	}

	return false;
}


function afegirIncidencia(idProducte,usuariId,albara) //dades provenen del boto del producte
{
	//document.getElementById('sel_tipusIncidencia').value='producte';
	//document.getElementById('i_tipusIncidencia').value='producte';

	resetCampsIncidencies()
	
	document.getElementById('i_producteId').value=idProducte;
	valor=document.getElementById('p_nomProducte_'+idProducte).innerHTML;
	document.getElementById('p_producteNom').innerHTML=valor;
	if(albara=='GD')
	{
		valor=document.getElementById('p_demanat_'+idProducte+'_'+usuariId).innerHTML;
	}
	else
	{
		valor=document.getElementById('p_demanat_'+idProducte).innerHTML;
	}
	document.getElementById('i_producteDemanat').value=valor;

	document.getElementById('tr_producteId').style.visibility='inherit';
	document.getElementById('tr_producteId').style.position='relative';
	document.getElementById('tr_producteNom').style.visibility='inherit';
	document.getElementById('tr_producteNom').style.position='relative';
		
	document.getElementById('tr_producteDemanat').style.visibility='inherit';
	document.getElementById('tr_producteDemanat').style.position='relative';
	document.getElementById('tr_producteRebut').style.visibility='inherit';
	document.getElementById('tr_producteRebut').style.position='relative';

	//document.getElementById('i_producteId').readOnly=true;
	document.getElementById('i_producteDemanat').readOnly=true;

	document.getElementById('t_afegirIncidencia').style.position='relative';
	document.getElementById('t_afegirIncidencia').style.visibility='inherit';

	return;
}

function eliminarIncidencia(incidenciaId)
{
	adressAndGet='vistaAlbara.php?gRef='+gRef+'&op='+op+'&opt=eliminar&inId='+incidenciaId;
	enviarFpars(adressAndGet,'_self');
	return;
}

editarIncidenciaIdOld='';
numBotoEditarOld='';
function editarIncidencia(numBotoEditar,incidenciaId)
{
	if(editarIncidenciaIdOld!='')
	{
		document.getElementById('ta_comentaris_'+numBotoEditarOld).style.visibility='hidden';
		document.getElementById('ta_comentaris_'+numBotoEditarOld).style.position='absolute';
		document.getElementById('i_botoEditarG_'+numBotoEditarOld).style.visibility='hidden';

		document.getElementById('p_comentaris_'+numBotoEditarOld).style.visibility='inherit';
		document.getElementById('p_comentaris_'+numBotoEditarOld).style.position='relative';
		document.getElementById('i_botoEditarE_'+numBotoEditarOld).style.visibility='inherit';
		document.getElementById('i_botoEditarR_'+numBotoEditarOld).style.visibility='inherit';
	}
	document.getElementById('p_comentaris_'+numBotoEditar).style.visibility='hidden';
	document.getElementById('i_botoEditarE_'+numBotoEditar).style.visibility='hidden';
	document.getElementById('i_botoEditarR_'+numBotoEditar).style.visibility='hidden';
	document.getElementById('p_comentaris_'+numBotoEditar).style.position='absolute';

	document.getElementById('ta_comentaris_'+numBotoEditar).style.visibility='inherit';
	document.getElementById('ta_comentaris_'+numBotoEditar).style.position='relative';
	document.getElementById('i_botoEditarG_'+numBotoEditar).style.visibility='inherit';
	val1745220815=document.getElementById('p_comentaris_'+numBotoEditar).innerHTML;
	document.getElementById('ta_comentaris_'+numBotoEditar).innerHTML=val1745220815;

	editarIncidenciaIdOld=incidenciaId;
	numBotoEditarOld=numBotoEditar;
	return;
}

function guardarIncidencia(numBotoEditar,incidenciaId)
{
	adressAndGet='vistaAlbara.php?gRef='+gRef+'&op='+op+'&opt=guardar&inId='+incidenciaId+'&numB='+numBotoEditar;
	val1745220814=document.getElementById('i_pars').value;
	document.getElementById('i_pars_'+numBotoEditar).value=val1745220814;
	document.getElementById('f_guardarIncidencia_'+numBotoEditar).action=adressAndGet;
	document.getElementById('f_guardarIncidencia_'+numBotoEditar).submit();
	if(llista_id==0)
	{
		document.getElementById('f_guardarIncidencia_'+numBotoEditar).action='vistaAlbara.php';
	}
	else
	{
		document.getElementById('f_guardarIncidencia_'+numBotoEditar).action='vistaAlbaraLocal.php';
	}
	
	return;
}

function seleccionarTipusIncidencia() //dades provenen del selector de tipus incidencia
{
	val23120055=document.getElementById('t_afegirIncidencia').style.visibility;
	if(val23120055=='hidden')
	{
		resetCampsIncidencies();
	}
	
	valor=document.getElementById('sel_tipusIncidencia').value;
	document.getElementById('i_tipusIncidencia').value=valor;
	if(valor=='producte' || valor=='jaEntregat' || valor=='pendentEntrega')
	{
		document.getElementById('tr_producteId').style.visibility='inherit';
		document.getElementById('tr_producteId').style.position='relative';
		document.getElementById('tr_producteNom').style.visibility='inherit';
		document.getElementById('tr_producteNom').style.position='relative';
		
		document.getElementById('tr_producteDemanat').style.visibility='inherit';
		document.getElementById('tr_producteDemanat').style.position='relative';
		document.getElementById('tr_producteRebut').style.visibility='inherit';
		document.getElementById('tr_producteRebut').style.position='relative';

		//treure 'readonly'
		//document.getElementById('i_producteId').readOnly=false;
		document.getElementById('i_producteDemanat').readOnly=false;
	}
	document.getElementById('t_afegirIncidencia').style.position='relative'
	document.getElementById('t_afegirIncidencia').style.visibility='inherit'

	return;
}

function seleccionarAbonament()
{
	resetCampsIncidencies();
	
	document.getElementById('t_afegirIncidencia').style.position='absolute';
	document.getElementById('t_afegirIncidencia').style.visibility='hidden';
	document.getElementById('t_aplicarAbonament').style.position='relative';
	document.getElementById('t_aplicarAbonament').style.visibility='inherit';
	return;
}

function seleccionarCarrec()
{
	resetCampsIncidencies();
	
	document.getElementById('t_afegirIncidencia').style.position='absolute';
	document.getElementById('t_afegirIncidencia').style.visibility='hidden';
	document.getElementById('t_aplicarCarrec').style.position='relative';
	document.getElementById('t_aplicarCarrec').style.visibility='inherit';

	return;
}

function seleccionarAnularReserva()
{
	resetCampsIncidencies();
	
	document.getElementById('t_afegirIncidencia').style.position='absolute';
	document.getElementById('t_afegirIncidencia').style.visibility='hidden';
	document.getElementById('t_anularReserva').style.position='relative';
	document.getElementById('t_anularReserva').style.visibility='inherit';

	return;
}

function resetCampsIncidencies()
{
	//t_afegirIncidencia:
	
	document.getElementById('tr_producteId').style.visibility='hidden';
	document.getElementById('tr_producteId').style.position='absolute';
	document.getElementById('tr_producteNom').style.visibility='hidden';
	document.getElementById('tr_producteNom').style.position='absolute';
	document.getElementById('tr_producteDemanat').style.visibility='hidden';
	document.getElementById('tr_producteDemanat').style.position='absolute';
	document.getElementById('tr_producteRebut').style.visibility='hidden';
	document.getElementById('tr_producteRebut').style.position='absolute';

	document.getElementById('i_producteId').value='';
	document.getElementById('i_producteDemanat').value='';
	document.getElementById('i_producteRebut').value='';
	document.getElementById('ta_comentaris').value='';
	//document.getElementById('sel_ruta').value=ruta;

	//*v36.2-assignacio
	document.getElementById('sel_ruta').value=rutaPeriode;
	
	//document.getElementById('i_producteId').readOnly=true;
	document.getElementById('i_producteDemanat').readOnly=true;
	
	document.getElementById('t_afegirIncidencia').style.position='absolute';
	document.getElementById('t_afegirIncidencia').style.visibility='hidden';

	document.getElementById('t_aplicarAbonament').style.position='absolute'
	document.getElementById('t_aplicarAbonament').style.visibility='hidden'
	
	document.getElementById('t_aplicarCarrec').style.position='absolute'
	document.getElementById('t_aplicarCarrec').style.visibility='hidden'

	document.getElementById('t_anularReserva').style.visibility='hidden';
	document.getElementById('t_anularReserva').style.position='absolute';
	
	
	return;
}

function vistaImpressio2()
{
	
	valor=document.getElementById('cb_vistaImpressio2').value;
	if(valor==0)
	{
		for(i=0;i<numBotonsEditar;i++)
		{
			document.getElementById('i_botoEditarE_'+i).className='i_botoEditarNo';
			document.getElementById('i_botoEditarG_'+i).className='i_botoEditarNo';
			document.getElementById('i_botoEditarR_'+i).className='i_botoEditarNo';
			document.getElementById('ta_comentaris_'+i).style.visibility='hidden';
			document.getElementById('p_comentaris_'+i).style.visibility='inherit';
	}

		for(i=0;i<numBotonsEditar2;i++)
		{
			document.getElementById('i_botoEditar2_'+i).type='text';
			document.getElementById('i_botoEditar2_'+i).size='5';
			document.getElementById('i_botoEditar2_'+i).readonly=true;
		}
		
		
		if(mostrarIncidencies==1)
		{
			document.getElementById('t_seleccionarTipusIncidencia').style.visibility='hidden';
			document.getElementById('t_seleccionarTipusIncidencia').style.position='absolute';
			document.getElementById('t_seleccionarTipusIncidencia').style.top=0;
		}

		document.getElementById('cb_vistaImpressio2').value=1;
		document.getElementById('cb_vistaImpressio2').checked=true;
	}
	else
	{
		for(i=0;i<numBotonsEditar;i++)
		{
			document.getElementById('i_botoEditarE_'+i).className='i_botoEditarOk';
			document.getElementById('i_botoEditarR_'+i).className='i_botoEditarOk';
		}

		for(i=0;i<numBotonsEditar2;i++)
		{
			document.getElementById('i_botoEditar2_'+i).type='button';
			document.getElementById('i_botoEditar2_'+i).readonly=false;
		}

		if(mostrarIncidencies==1)
		{
			document.getElementById('t_seleccionarTipusIncidencia').style.position='relative';
			document.getElementById('t_seleccionarTipusIncidencia').style.visibility='inherit';
		}

 		document.getElementById('cb_vistaImpressio2').value=0;
		document.getElementById('cb_vistaImpressio2').checked=false;
	}

	return;
}	

function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	if(llista_id==0)
	{
		document.getElementById('f_pars').action='vistaAlbara.php';
	}
	document.getElementById('f_pars').target='_self';
	{
		document.getElementById('f_pars').action='vistaAlbaraLocal.php';
	}
		
	return;
}

function checkAplicarAbonament()
{
	missatgeAlerta='';
	
//*v36-15-12-15 1 declaracio + 1 condicio
	valor=document.getElementById('sel_usuariIncidenciaAa').value;
	if(valor=='')
	{
		missatgeAlerta+="\nAtenci�: Cal seleccionar l'usuari a qui s'aplicar� el c�rrec.";			
	}

	valor=document.getElementById('ta_comentarisAa').value;
	if(valor=='')
	{
		missatgeAlerta+="\nAtenci�: El camp 'comentari' ha de contenir el concepte d'abonament.";			
	}

	valueEcosAa=document.getElementById('i_ecosAa').value;
	if(valueEcosAa!='')
	{
       	allowedChars=allowedCharsFloat;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<valueEcosAa.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(valueEcosAa.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<valueEcosAa.length)
        {
			missatgeAlerta+="\nEl camp 'ecos' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        }
	}

	valueEbAa=document.getElementById('i_ebAa').value;
	if(valueEbAa!='')
	{
       	allowedChars=allowedCharsFloat;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<valueEbAa.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(valueEbAa.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<valueEbAa.length)
        {
			missatgeAlerta+="\nEl camp '"+parametresMoneda3Nom+"' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        }
	}

	valueEurosAa=document.getElementById('i_eurosAa').value;
	if(valueEurosAa!='')
	{
       	allowedChars=allowedCharsFloat;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<valueEurosAa.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(valueEurosAa.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<valueEurosAa.length)
        {
			missatgeAlerta+="\nEl camp 'euros' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        }
	}

	
	valueUmsAa=valueEcosAa+valueEbAa+valueEurosAa;
	if(valueUmsAa<=0)
    {
		missatgeAlerta+="\nLa suma dels imports de les diferents monedes ha de ser positiu.";			
    }
	
	
	if (missatgeAlerta.length==0)
	{
		return true;
	}

	return false;
}

function checkAplicarCarrec()
{
	missatgeAlerta='';

//*v36-15-12-15 1 declaracio + 1 condicio
	valor=document.getElementById('sel_usuariIncidenciaAc').value;
	if(valor=='')
	{
		missatgeAlerta+="\nAtenci�: Cals eleccionar l'usuari a qui s'aplicar� el c�rrec.";			
	}

	valor=document.getElementById('ta_comentarisAc').value;
	if(valor=='')
	{
		missatgeAlerta+="\nAtenci�: El camp 'comentari' ha de contenir el concepte del c�rrec.";			
	}

	valueEcosAc=document.getElementById('i_ecosAc').value;
	if(valueEcosAc!='')
	{
       	allowedChars=allowedCharsFloat;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<valueEcosAc.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(valueEcosAc.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<valueEcosAc.length)
        {
			missatgeAlerta+="\nEl camp 'ecos' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        }
	}

	valueEbAc=document.getElementById('i_ebAc').value;
	if(valueEbAc!='')
	{
       	allowedChars=allowedCharsFloat;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<valueEbAc.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(valueEbAc.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<valueEbAc.length)
        {
			missatgeAlerta+="\nEl camp '"+parametresMoneda3Nom+"' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        }
	}

	valueEurosAc=document.getElementById('i_eurosAc').value;
	if(valueEurosAc!='')
	{
       	allowedChars=allowedCharsFloat;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<valueEurosAc.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(valueEurosAc.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<valueEurosAc.length)
        {
			missatgeAlerta+="\nEl camp 'euros' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        }
	}

	
	valueUmsAc=valueEcosAc+valueEbAc+valueEurosAc;
	if(valueUmsAc<=0)
    {
		missatgeAlerta+="\nLa suma dels imports de les diferents monedes ha de ser positiu.";			
    }
	
	
	if (missatgeAlerta.length==0)
	{
		return true;
	}

	return false;
}

function checkAnularReserva()
{
	missatgeAlerta='';

	valueAnularId=document.getElementById('i_anularId').value;
	if(valueAnularId!='')
	{
       	allowedChars=allowedCharsMobil;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<valueAnularId.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(valueAnularId.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<valueAnularId.length)
        {
			missatgeAlerta+="\nEl camp 'id producte' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        }
	}
	else
    {
		missatgeAlerta+="\nEl camp 'id producte' no pot estar buit";			
    }
	

	valueAnularUts=document.getElementById('i_anularUts').value;
	if(valueAnularUts!='')
	{
       	allowedChars=allowedCharsMobil;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<valueAnularUts.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(valueAnularUts.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<valueAnularUts.length)
        {
			missatgeAlerta+="\nEl camp 'unitat a anul.lar' cont� un valor incorrecte. \nCaracters permesos: "+allowedChars;			
        }
	}
	else
    {
		missatgeAlerta+="\nEl camp 'unitata a anul.lar' no pot estar buit";			
    }
	
	
	if (missatgeAlerta.length==0)
	{
		return true;
	}

	return false;
}

function actualitzarFpagament()
{
	val=document.getElementById('p_fPagamentEcos1').innerHTML;
	if(selUsuariId!=0)
	{
		document.getElementById('p_fPagamentEcos').innerHTML=val
		if(val!=0)
		{
			document.getElementById('tr_fPagamentEcos').style.visibility='inherit';
			document.getElementById('tr_fPagamentEcos').style.position='relative';
		}	
	}

	if(moneda3Activa==1)
	{
		val=document.getElementById('p_fPagamentEb1').innerHTML;
		if(selUsuariId!=0)
		{
			document.getElementById('p_fPagamentEb').innerHTML=val
			if(val!=0)
			{
				document.getElementById('tr_fPagamentEb').style.visibility='inherit';
				document.getElementById('tr_fPagamentEb').style.position='relative';
			}	
		}
	}

	val=document.getElementById('p_fPagamentEu1').innerHTML;
	if(selUsuariId!=0)
	{
		document.getElementById('p_fPagamentEu').innerHTML=val
		if(val!=0)
		{
			document.getElementById('tr_fPagamentEu').style.visibility='inherit';
			document.getElementById('tr_fPagamentEu').style.position='relative';
		}	
	}

	return;
}


