<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "eines.php";
include "db_comptes.php";
include "db_incidencies.php";
include "db_distribucioGrups.php";
include "db_gestioMagatzems.php";
include "html_ajuda1.php";
include "db_ajuda.php";

//--------------------------------------------------------------------

function html_nomsColumnes()
{
	echo "
		<tr>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><p><b>Grup:</b></p></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"FP-Import distribuci� CAC al Grup segons forma pagament grup. Inclou preu total productes, cost transport, fons liquidesa CAC i abonaments i c�rrecs \"><p class='albara'>(FP)<br><b>DISTRIB:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"GE-Import distribuci� CAC al grup segons % ms productors Inclou preu total productes, cost transport, fons liquidesa CAC i abonaments i c�rrecs \n\n \"><p class='albara'>(GE)<br><b>DISTRIB:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"GE- Import Abastiment CAC del perfil de productor vinculat a aquest grup, segons % ms del productor. Inclou nom�s preu de compra dels productes al productor\"><p class='albara'>(GE)<br><b>ABAST:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"GE - Balan� monetari: Import Distribuci� CAC (segons %MS productors)- Import Abastiment CAC d'aquest productor.Saldos a favor de la CAC\"><p class='albara'>(GE)<br><b>DISTR-ABST:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"GE - Intercanvi: Import Distribuci� CAC (segons Forma Pagament grup)- Import Abastiment CAC d'aquest productor. Saldos a favor de la CAC\"><p class='albara'>(Intercanvi)<br><b>DISTR-ABST:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"EC - Estoc CAC: Import dels productes d'aquest productor  a l'inventari actual de la CAC\"><p class='albara'>(GE)<br><b>Estoc CAC <br>dip�sit:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"EC - Estoc CAC Reservat: Import dels productes d'aquest productor, amb estoc positiu a l'inventari actual de la CAC i que s'han reservat en aquest periode\"><p class='albara'>(GE)<br><b>Estoc CAC Reservat<br>dip�sit:</b></p></a></td>
			<td  bgcolor='#FFDB58' width='10%' valign='bottom'><a title=\"RC - Recompte<br>LIQUIDACI�: [GE-Intercanvi] - [EC-Estoc CAC], del balan� d'intercanvi es descompta el que la CAC ja t� en estoc iper tant que el productor no entrega (ja est� pagat per la CAC anteriorment)\"><p class='albara'><br><b>RECOMPTE:</b></p></a></td>
		</tr>
	";

	return;
}
//--------------------------------------------------------------------

function html_nomsColumnes2()
{
	echo "
		<tr>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><p><b>Perfil de Productor:</b></p></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"FP-Import distribuci� CAC al Grup segons forma pagament grup. Inclou preu total productes, cost transport, fons liquidesa CAC i abonaments i c�rrecs \"><p class='albara'>(FP)<br><b>DISTRIB:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"GE-Import distribuci� CAC al grup segons % ms productors Inclou preu total productes, cost transport, fons liquidesa CAC i abonaments i c�rrecs \n\n \"><p class='albara'>(GE)<br><b>DISTRIB:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"GE- Import Abastiment CAC del perfil de productor vinculat a aquest grup, segons % ms del productor. Inclou nom�s preu de compra dels productes al productor\"><p class='albara'>(GE)<br><b>ABAST:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"GE - Balan� monetari: Import Distribuci� CAC (segons %MS productors)- Import Abastiment CAC d'aquest productor.Saldos a favor de la CAC\"><p class='albara'>(GE)<br><b>DISTR-ABST:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"GE - Balan� monetari: Import Distribuci� CAC (segons Forma Pagament grup)- Import Abastiment CAC d'aquest productor. Saldos a favor de la CAC\"><p class='albara'>(Intercanvi)<br><b>DISTR-ABST:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"EC - Estoc CAC: Import dels productes d'aquest productor  a l'inventari actual de la CAC\"><p class='albara'>(GE)<br><b>Estoc CAC <br>dip�sit:</b></p></a></td>
			<td  bgcolor='#eeeeee' width='10%' valign='bottom'><a title=\"EC - Estoc CAC Reservat: Import dels productes d'aquest productor, amb estoc positiu a l'inventari actual de la CAC i que s'han reservat en aquest periode\"><p class='albara'>(GE)<br><b>Estoc CAC Reservat<br>dip�sit:</b></p></a></td>
			<td  bgcolor='#FFDB58' width='10%' valign='bottom'><a title=\"RC - Recompte<br>LIQUIDACI�: [GE-Intercanvi] - [EC-Estoc CAC], del balan� d'intercanvi es descompta el que la CAC ja t� en estoc iper tant que el productor no entrega (ja est� pagat per la CAC anteriorment)\"><p class='albara'><br><b>RECOMPTE:</b></p></a></td>
		</tr>
	";

	return;
}

//--------------------------------------------------------------------


$mParsCAC=array();
$quantitatTotalCac=0;
$quantitatTotalRebosts=0;
$mCr=array();
$missatgeAlerta='';
$mMissatgeAlerta['missatge']='';
$mMissatgeAlerta['result']='';

$recolzament=0;
$recolzamentIntercanvi=0;
$recolzamentT=0;
$recolzamentIntercanviT=0;

$recolzamentTotalSuportatT=0;
$recolzamentTotalRebutT=0;
$recolzamentIntercanviSuportatT=0;
$recolzamentIntercanviRebutT=0;

$mProblemaAmb=array();

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['excloureProductesJaEntregats']='0';
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';
	$mPars['vProducte']='TOTS';

$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

unset($mPars['compte_ecos']);
unset($mPars['compte_cieb']);

$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}

$mRuta=explode('_',$mPars['selRutaSufix']);
if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

	$mPars['taulaProductes']='productes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandes']='comandes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandesSeg']='comandes_seg';
	$mPars['taulaIncidencies']='incidencies_'.$mPars['selRutaSufix'];
	if(count($mRuta)==2)//periode especial
	{
		$mPars['taulaProductors']='productors_'.$mPars['selRutaSufix']; //productes especials
	}
	else
	{
		$mPars['taulaProductors']='productors';
	}


$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];

getConfig($db); //inicialitza variables anteriors;

post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['comptes.php']=db_getAjuda('comptes.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=0;
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';

	$mPars['fPagamentUms']=0;
	$mPars['fPagamentEcos']=0;
	$mPars['fPagamentEb']=0;
	$mPars['fPagamentEu']=0;
	$mPars['fPagamentEuMetalic']=0;
	$mPars['fPagamentEuTransf']=0;
	$mPars['fPagamentEcosIntegralCES']=0;

	$mPars['fPagamentEcosPerc']=0;
	$mPars['fPagamentEbPerc']=0;
	$mPars['fPagamentEuPerc']=0;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['paginacio']=-1;
	if(!isset($mPars['pagS'])){$mPars['pagS']=0;}
	if(!isset($mPars['pagFiltre'])){$mPars['pagFiltre']=$mPars['sortBy'].$mPars['veureProductesDisponibles'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vProductor'].$mPars['vSubCategoria'].$mPars['vCategoria'];}
	if(!isset($mPars['numProdsPag'])){$mPars['numProdsPag']=10;}
	if(!isset($mPars['numPags'])){$mPars['numPags']=0;} 

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}

$mComptesGrupsRef=db_getComptesGrupsRef($db);

$mUsuarisGrupRef=array();
$mProductes=db_getProductes2($db);
$mUsuarisRef=db_getUsuarisRef($db);
$mGrupsRef=db_getGrupsRef($db);
$mRebostsAmbComandaResum=db_getRebostsAmbComandaResum($db);
//vd($mRebostsAmbComandaResum);
if($mPars['nivell']=='sadmin')
{
	$afp_grupId=@$_POST['i_afp_grupId'];
	if(isset($afp_grupId) && $afp_grupId!='')
	{
		$mMissatgeAlerta=db_actualitzarFormesPagamentGrups($afp_grupId,1,$db);
		$mRebostsAmbComandaResum=db_getRebostsAmbComandaResum($db);
	}
	else
	{
		$afp_grupSid=@$_POST['i_afp_grupSid'];
		if(isset($afp_grupSid) && $afp_grupSid=='TOTS')
		{
			$mMissatgeAlerta=db_actualitzarFormesPagamentGrups('',1,$db);
			$mRebostsAmbComandaResum=db_getRebostsAmbComandaResum($db);
		}
	}
}

$mRutesSufixes=getRutesSufixes($db);

//*v36 5-1-16 assignacio
$mPeriodesInfo=db_getPeriodesInfo($db);


$mPerfilsRef=db_getPerfilsRef($db);
$mComandaPerProductors=db_getComandaPerProductors($db);
$mPeticionsGrup=getPeticionsGrup();



echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>"; echo $htmlTitolPags; echo " - Comptes</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_comptes.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
//*v36.2-declaracio
ruta='".$mPars['selRutaSufix']."';
//*v36.2-Declaracio
rutaPeriode='".$mPars['selRutaSufixPeriode']."';
calGuardar=false;
</SCRIPT>
</head>
<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('comptes.php?');
echo "
	<table align='center' style='width:80%' bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			</td>
		</tr>
	</table>

<center><p style='font-size:16px;'>[ Ruta <b>".$mPars['selRutaSufix']."</b>: Resum Comandes ]</p></center>
";
//*v36.5-funcio call
mostrarSelectorRuta(1,'comptes.php');
echo "
<table width='80%' align='center' bgcolor='".$mColors['table']."'>
	<tr>
		<td id='td_missatgeAlerta' style=' width:100%;' align='center'  valign='bottom'>
		".$mMissatgeAlerta['missatge']."
		</td>
	</tr>
</table>
<center><p class='p_nota'>* No es comptabilitzen els productes especials</p></center>
<table align='center' bgcolor='#dddddd' width=90%' bgcolor='".$mColors['table']."'>
";

html_nomsColumnes();

$contLinies=0;

$kgT=0;
$umsTT=0;
$euTT=0;
$ebTT=0;
$ecosTT=0;
$umsTTfp=0;

$cAp_umsT=0;

$cAp_umsTT=0;

$cAp_umsCtT=0;
$cAp_ecosCtT=0;
$cAp_eurosCtT=0;

$cAp_umsFdT=0;
$cAp_ecosFdT=0;
$cAp_eurosFdT=0;

$sumaAbonamentsCarrecsUmsTT=0;
$sumaAbonamentsCarrecsEcosTT=0;
$sumaAbonamentsCarrecsEurosTT=0;

$cont=0;

$sumaAbonamentsCarrecsEcosT=0;
$sumaAbonamentsCarrecsEbT=0;
$sumaAbonamentsCarrecsEuT=0;
$sumaAbonamentsCarrecsUmsT=0;
		
$mColorLinia['haProduit']='#CEF6FB';
$mColorLinia['haProduit2']='#77ffff';
$mColorLinia['haConsumit']='#DCFBCE';
$mColorLinia['haProduitIconsumit']='#F1CEFB';
$colorLinia='#ffffff';

$cpUms=0; //import distribucio cac a grups segons forma pagament dels grups, inclou abonaments i c�rrecs (ums)
$cpEcos=0;
$cpEb=0;
$cpEu=0;

$cpUmsT=0; //Total imports distribucio cac a grups segons forma pagament dels grups, inclou abonaments i c�rrecs (ums)
$cpEcosT=0;
$cpEbT=0;
$cpEuT=0;

$GEdistribUmsT=0; //GE- Total imports distribucio cac a grups segons % ms productes (ums)
$GEdistribEcosT=0;
$GEdistribEbT=0;
$GEdistribEuT=0;

$cDp_umsT=0;
$cDp_ecosT=0;
$cDp_eurosT=0;
$cDp_umsCtT=0;
$cDp_ecosCtT=0;
$cDp_eurosCtT=0;
$cDp_umsFdT=0;
$cDp_ecosFdT=0;
$cDp_eurosFdT=0;

$cAp_umsT=0;
$cAp_ecosT=0;
$cAp_eurosT=0;

$invUms=0.0;
$invEcos=0.0;
$invEuros=0.0;

$invUmsR=0.0;
$invEcosR=0.0;
$invEurosR=0.0;
	
$invUmsT=0.0;
$invEcosT=0.0;
$invEurosT=0.0;

$invUmsRT=0.0;
$invEcosRT=0.0;
$invEurosRT=0.0;
	

$mMagatzems=db_getMagatzems($db);

$mInventariGT=db_getInventari('0',$db);

while(list($grupRef,$mGrup)=each($mGrupsRef))
{
	$haProduit=0;
	$haConsumit=0;

	if(!isset($mComptesGrupsRef[$grupRef])){$mComptesGrupsRef[$grupRef]=array();$mComptesGrupsRef[$grupRef]['saldo_ecos_r']=0;}

	$sumaAbonamentsCarrecsUmsT=0;	

	if($grupRef!=0) //si es cac no es suma
	{
		//consum
		if(!isset($mRebostsAmbComandaResum[$grupRef]))
		{
			$mRebostsAmbComandaResum[$grupRef]['kgT']=0;
			$mFormaPagament=array();
			$mFormaPagamentMembres=array();
			$mPars['fPagamentEcos']=0;
			$mPars['fPagamentEb']=0;
			$mPars['fPagamentEu']=0;
			$mPars['fPagamentUms']=0;
			$mPars['fPagamentEcosPerc']=100;
			$mPars['fPagamentEbPerc']=0;
			$mPars['fPagamentEuPerc']=0;

			$cDp_ums=0;
			$cDp_ecos=0;
			$cDp_euros=0;
			$cDp_umsCt=0;
			$cDp_ecosCt=0;
			$cDp_eurosCt=0;
			$cDp_umsFd=0;
			$cDp_ecosFd=0;
			$cDp_eurosFd=0;

			$mRebostsAmbComandaResum[$grupRef]['umsT']=0;
			$mRebostsAmbComandaResum[$grupRef]['ecosT']=0;
			$mRebostsAmbComandaResum[$grupRef]['eurosT']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctkTums']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctrTums']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctkTecos']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctrTecos']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctrTeuros']=0;
			$mRebostsAmbComandaResum[$grupRef]['umstFDC']=0;
			$mRebostsAmbComandaResum[$grupRef]['ecostFDC']=0;
			$mRebostsAmbComandaResum[$grupRef]['eurostFDC']=0;
			$mRebostsAmbComandaResum[$grupRef]['compte_ecos']='';
			$mRebostsAmbComandaResum[$grupRef]['compte_cieb']='';
			$mRebostsAmbComandaResum[$grupRef]['propietats']='';
		}
		else
		{
			$cDp_ums=$mRebostsAmbComandaResum[$grupRef]['umsT'];
			$cDp_ecos=$mRebostsAmbComandaResum[$grupRef]['ecosT'];
			$cDp_euros=$mRebostsAmbComandaResum[$grupRef]['eurosT'];
			$cDp_umsCt=$mRebostsAmbComandaResum[$grupRef]['ctkTums']+$mRebostsAmbComandaResum[$grupRef]['ctrTums'];
			$cDp_ecosCt=$mRebostsAmbComandaResum[$grupRef]['ctkTecos']+$mRebostsAmbComandaResum[$grupRef]['ctrTecos'];
			$cDp_eurosCt=$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']+$mRebostsAmbComandaResum[$grupRef]['ctrTeuros'];
			$cDp_umsFd=$mRebostsAmbComandaResum[$grupRef]['umstFDC'];
			$cDp_ecosFd=$mRebostsAmbComandaResum[$grupRef]['ecostFDC'];
			$cDp_eurosFd=$mRebostsAmbComandaResum[$grupRef]['eurostFDC'];

			$cDp_umsT+=$mRebostsAmbComandaResum[$grupRef]['umsT'];
			$cDp_ecosT+=$mRebostsAmbComandaResum[$grupRef]['ecosT'];
			$cDp_eurosT+=$mRebostsAmbComandaResum[$grupRef]['eurosT'];
			$cDp_umsCtT+=$mRebostsAmbComandaResum[$grupRef]['ctkTums']+$mRebostsAmbComandaResum[$grupRef]['ctrTums'];
			$cDp_ecosCtT+=$mRebostsAmbComandaResum[$grupRef]['ctkTecos']+$mRebostsAmbComandaResum[$grupRef]['ctrTecos'];
			$cDp_eurosCtT+=$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']+$mRebostsAmbComandaResum[$grupRef]['ctrTeuros'];
			$cDp_umsFdT+=$mRebostsAmbComandaResum[$grupRef]['umstFDC'];
			$cDp_ecosFdT+=$mRebostsAmbComandaResum[$grupRef]['ecostFDC'];
			$cDp_eurosFdT+=$mRebostsAmbComandaResum[$grupRef]['eurostFDC'];

			$haConsumit=1;
		}
		
		$mPars['grup_id']=$grupRef;
		$mRebost=db_getRebost($db);
		$mFormaPagament=db_getFormaPagament($db); // afegeix info a $mPars
		$mFormaPagamentMembres=db_getFormaPagamentMembres($grupRef,$db);
		$mAbonamentsCarrecs=db_getAbonamentsCarrecs($mRebost['id'],$db); // nom�s de la cac a� grup
		$mPars['vRutaIncd']=$mPars['selRutaSufix']; // per compatibilitzar amb db_getIncidencies();
		$mPars['vEstatIncd']='pendent'; // per compatibilitzar amb db_getIncidencies();
		$mPars['vGrupIncd']=$grupRef;
		$mIncidenciesIncd=db_getIncidencies(true,'incd',' * ',$db);		
		$mPars['vEstatIncd']='aplicat'; // per compatibilitzar amb db_getIncidencies();
		$mIncidenciesAbca=db_getIncidencies(true,'abCa',' * ',$db);	
		$incidenciesIncdText='';
		$incidenciesAbcaText='';
		$productesEspecialsText='';
		if(count($mIncidenciesIncd)>0){	$incidenciesIncdText="<font style='color:#ff0000;'><b>".(count($mIncidenciesIncd))."</b></font>";}
		else {	$incidenciesIncdText="<font style='color:#000000;'><b>".(count($mIncidenciesIncd))."</b></font>";}
		if(count($mIncidenciesAbca)>0){	$incidenciesAbcaText="<font style='color:#0000aa;'><b>".(count($mIncidenciesAbca))."</b></font>";}
		else {	$incidenciesAbcaText="<font style='color:#000000;'><b>".(count($mIncidenciesAbca))."</b></font>";}

		if(@$mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials']*1>0)
		{
			$productesEspecialsText="
			<p onClick=\"enviarFpars('resumProductesEspecials.php?sR=".$ruta."&gRef=".$mRebost['id']."','_blank');\" style='cursor:pointer;'><u>comandes&nbsp;productes&nbsp;especials</u>&nbsp;&nbsp;[<font style='color:#ff0000;'><b>".$mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials']."</b></font>]</p>
			";
		}
	
		
		//produccio
		$mPerfilVinculat=db_getPerfilVinculatAgrup($grupRef,$db);
		$mPerfilsAssociats=explode(',',trim($mGrupsRef[$grupRef]['productors_associats'],','));
		//if($grupRef==95){vd($mPerfilVinculat);}
		$cAp_ums=0;
		$cAp_ecos=0;
		$cAp_euros=0;
		$cAp_umsCt=0;
		$cAp_ecosCt=0;
		$cAp_eurosCt=0;
		$cAp_umsFd=0;
		$cAp_ecosFd=0;
		$cAp_eurosFd=0;

		$invUms=0.0;
		$invEcos=0.0;
		$invEuros=0.0;
		
		$invUmsR=0.0;
		$invEcosR=0.0;
		$invEurosR=0.0;

		while(list($producteId,$mComandaAproductor)=@each($mComandaPerProductors[$mPerfilVinculat['id']]))
		{
			if(substr_count($mProductes[$producteId]['tipus'],'especial')==0)
			{
				$cAp_ums-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
				$cAp_ecos-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
				$cAp_euros-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
				$cAp_umsCt-=$mComandaAproductor['quantitatT']*$mComandaAproductor['pes']*$mComandaAproductor['cost_transport_intern_kg'];
				$cAp_ecosCt-=$mComandaAproductor['quantitatT']*$mComandaAproductor['pes']*$mComandaAproductor['cost_transport_intern_kg']*$mComandaAproductor['ms_ctik']/100;
				$cAp_eurosCt-=$mComandaAproductor['quantitatT']*$mComandaAproductor['pes']*$mComandaAproductor['cost_transport_intern_kg']*(100-$mComandaAproductor['ms_ctik'])/100;
				$cAp_umsFd-=$mComandaAproductor['preu']*$mParametres['FDCpp']['valor']/100;
				$cAp_ecosFd-=$mComandaAproductor['preu']*($mParametres['FDCpp']['valor']/100)*$mParametres['msFDCpp']['valor']/100;
				$cAp_eurosFd-=$mComandaAproductor['preu']*($mParametres['FDCpp']['valor']/100)*(100-$mParametres['msFDCpp']['valor'])/100;

				//productes 'dip�sit' en estoc
//				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0 && $mPerfilVinculat['ref']!='63')
				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0)
				{
					if(!isset($mInventariGT['inventariRef'][$producteId])){$mInventariGT['inventariRef'][$producteId]=0;}
					$invUms+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu'];
					$invEcos+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
					$invEuros+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
				}

				//productes 'dip�sit' en estoc que s'han reservat
//				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0 && $mPerfilVinculat['ref']!='63')
				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0)
				{
					if($mComandaAproductor['quantitatT']<$mInventariGT['inventariRef'][$producteId])
					{
						$invUmsR+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
						$invEcosR+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEurosR+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
					else
					{
						$invUmsR+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu'];
						$invEcosR+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEurosR+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
				}
			}
		}
		if($cAp_ums<0){$haProduit=1;}
		
		$colorLinia='#ffffff';
		if($haProduit && $haConsumit){$colorLinia=$mColorLinia['haProduitIconsumit'];}
		else
		if($haConsumit){$colorLinia=$mColorLinia['haConsumit'];}
		else
		if($haProduit){$colorLinia=$mColorLinia['haProduit'];}

		//nom�s mostrar linies amb algun valor diferent de zero

			// suma abonaments/carrecs:
			$sumaAbonamentsCarrecsEcos=0;
			$sumaAbonamentsCarrecsEb=0;
			$sumaAbonamentsCarrecsEu=0;
			$sumaAbonamentsCarrecsUms=0;
			
			while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
			{
				if($mAbonamentCarrec['tipus']=='abonamentCAC')
				{
					$sumaAbonamentsCarrecsEcos-=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb-=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu-=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms-=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
				else if($mAbonamentCarrec['tipus']=='carrecCAC')
				{
					$sumaAbonamentsCarrecsEcos+=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb+=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu+=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms+=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
			}
			reset($mAbonamentsCarrecs['grup']);
			
			$sumaAbonamentsCarrecsEcosT+=$sumaAbonamentsCarrecsEcos;
			$sumaAbonamentsCarrecsEbT+=$sumaAbonamentsCarrecsEb;
			$sumaAbonamentsCarrecsEuT+=$sumaAbonamentsCarrecsEu;
			$sumaAbonamentsCarrecsUmsT+=$sumaAbonamentsCarrecsUms;
			
			
			$cpUms=$mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu']+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
			$cpEcos=$mPars['fPagamentEcos']+$sumaAbonamentsCarrecsEcos;
			$cpEb=$mPars['fPagamentEb']+$sumaAbonamentsCarrecsEb;
			$cpEu=$mPars['fPagamentEu']+$sumaAbonamentsCarrecsEu;

			$GEdistribUms=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
			$GEdistribEcos=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$sumaAbonamentsCarrecsEcos;
			$GEdistribEb=0+$sumaAbonamentsCarrecsEb;
			$GEdistribEu=$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEu;


	if($sumaAbonamentsCarrecsUms!=0 || $cpUms!=0 || $GEdistribUms!=0 || $cAp_ums!=0)
	{
		if($contLinies==1)
		{	
			html_nomsColumnes();
			$contLinies=0;
		}
		echo "
		<tr bgcolor='".$colorLinia."'>

";
//columna 1.1
echo "			
			<td valign='top'>
			<p>[".$cont."]<br></p>
			<b>".(urldecode($mRebost['nom']))."</b>
			<p class='p_micro'>
			Responsable: <b>".(urldecode($mUsuarisRef[$mRebost['usuari_id']]['usuari']))."</b>
			<br>
			email: ".(urldecode($mRebost['email_rebost']))."
			<br>
			mobil: ".$mRebost['mobil']."
			<br>
			adre�a: ".(urldecode($mRebost['adressa']))."
			</p>
			<table style='width:100%;'>
				<tr>
					<td  style='width:60%;' align='left' valign='top'>
					".$productesEspecialsText."
					<p class='p_micro2' onClick=\"enviarFpars('vistaAlbara.php?sR=".$mPars['selRutaSufix']."&gRef=".$mRebost['id']."&op=totals','_blank');\" style='cursor:pointer;'><u>albar�</u></p>
					<p class='p_micro2'>&nbsp;&nbsp;[ Incidencies: ".$incidenciesIncdText."]
					<br>&nbsp;&nbsp;[ Abonaments/c�rrecs: ".$incidenciesAbcaText."]
					";
					$mGrup=$mRebost;
					$mPeticionsGrup=getPeticionsGrup();
					if(count($mPeticionsGrup)>0){$peticionsText="<font style='color:red;'><b>".(count($mPeticionsGrup))."</b></font>";}else{$peticionsText='<b>0</b>';}
					echo "
					<br>&nbsp;&nbsp;[ Peticions pendents: ".$peticionsText."]
					<br>&nbsp;&nbsp;<a style='font-size:10px;' title=\"".@$mRebostsAmbComandaResum[$grupRef]['historial']."\">historial comanda</a></p>
					</td>
				</tr>
			</table>
			<p class='p_micro' >Productor vinculat al grup:
			<br>
			<font color='blue'>
			";
			if(isset($mPerfilVinculat['id']))
			{
				echo "
			<b>
			".(urldecode($mPerfilVinculat['projecte']))."
			</b>
				";
			}
			else
			{
				echo "
			- cap -
				";
			}
			echo "
			</font>
			</p>
			
			<p class='p_micro' >Productors associats al grup:</p>
			<div style='width:200px; height:75px; overflow-y:scroll;'>
			<p class='p_micro' >
			<font color='blue'>
			";
			if(count($mPerfilsAssociats)>0)
			{
				while(list($key,$perfilRef)=each($mPerfilsAssociats))
				{
//*v36.2-condicio
					if($perfilRef!='' && isset($mPerfilsRef[$perfilRef]))
					{
						echo "
			".(urldecode($mPerfilsRef[$perfilRef]['projecte']))."
			<br>
						";
					}
				}
				reset($mPerfilsAssociats);
			}
			else
			{
				echo "- cap -";
			}
			
			echo "
			</font></p>
			</div>


			";
			if($grupRef==62)
			{
				echo "
				<p class='p_micro2' style='color:white; background-color:red;'>* les compres d'aquest grup son despeses internes de la cac, i les vendes son de productes recolzats o transformats que cal pagar al corresponent productor</p>
				";
			}
			echo "
			</td>

";
//columna 1.2
echo "			
			<td valign='top'>
			";
			
			$styleTotalUms='';
			$alertaTotalUms='';
			$botoActualitzarFormesPagamentUsuaris=false;
			if(number_format($mPars['fPagamentUms'],2,'.','')!=number_format($mFormaPagamentMembres['fPagamentUms'],2,'.',''))
			{
				$styleTotalUms=" style='color:#ff0000; font-weight: bold;' ";
				$alertaTotalUms="Atenci�: El Grup no ha actualitzat la forma de pagament";
				$botoActualitzarFormesPagamentUsuaris=true;
			}
			
			echo "
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cpUms,2,'.',''))."</b> ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p".$styleTotalUms.">".(number_format($cpEcos,2,'.',''))." ecos  ".$mRebostsAmbComandaResum[$grupRef]['compte_ecos']; if($mPars['fPagamentEcosIntegralCES']=='1'){echo "&nbsp;<font size='1'><i><a title='INTEGRALCES'>I-CES</a></i></font> ";}else{echo "&nbsp;<font size='1'><i><a title='CES'>CES</a></i></font> ";} echo "
					</td>
				</tr>
				<tr>
					<td>
					<p ".$styleTotalUms.">".(number_format($cpEb,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']." ".$mRebostsAmbComandaResum[$grupRef]['compte_cieb']."&nbsp;<font size='1'><i><a title='INTEGRALCES'>I-CES</a></i></font></p>
					</td>
				</tr>
				<tr>
					<td>
					<p ".$styleTotalUms.">".(number_format($cpEu,2,'.',''))." euros "; if($mPars['fPagamentEuMetalic']==1){echo " <i>en met�l.lic</i> ";}else{echo " <i>transfer�ncia</i> ";} echo "
					</td>
				</tr>
			</table>
			<p class='compacte' ><a  title='total ums segons comandes dels usuaris del grup.\n No inclou abonaments i c�rrecs' ".$styleTotalUms.">[".(number_format($mFormaPagamentMembres['fPagamentUms'],2,'.',''))." ums cg]</a>/<a  title=\"total ums segons forma de pagament del grup.\n No inclou abonaments i c�rrecs.\n".$alertaTotalUms."\"  ".$styleTotalUms.">[".(number_format($mPars['fPagamentUms'],2,'.',''))." ums fp]</a>
			";
			
			if($botoActualitzarFormesPagamentUsuaris)
			{
				$mMissatgeAlerta=db_actualitzarFormesPagamentGrups($grupRef,0,$db);
				if
				(
					(
						number_format($mFormaPagamentMembres['fPagamentUms'],2,'.','')!=number_format($mPars['importsGrups_'.$grupRef],2,'.','')
						||
						number_format($mFormaPagamentMembres['fPagamentUms'],2,'.','')!=number_format($mPars['fPagamentUms'],2,'.','')
					)
					&&
					$mPars['nivell']=='sadmin'
					&&
					$mPars['selRutaSufixPeriode']>1508
				)
				{
					echo "
				<a title=\"actualitzar� els imports de les comandes de cada usuari del grup pero no la del grup. Mant� els % monetaris actuals. Les diferencies solen ser per canvis de preus dels productes o perqu� el responsable de grup no ha actualitzat la forma de pagament del grup.\">
				<input class='i_micro' type='button' onClick=\"javascript:actualitzarFormesPagamentUsuaris('".$grupRef."')\" value='[".number_format($mPars['importsGrups_'.$grupRef],2,'.','')."] actualitzar imports usuaris grup'>
				[sadmin]</a>
					";
				}
			}
			echo "
			</p>
			";
			$styleAC='';
			if($sumaAbonamentsCarrecsUms!=0){$styleAC=" style='color:#0000aa;' ";}else{$styleAC='';}
			echo "
			<a title=\"Suma d'abonaments i c�rrecs de la CAC al Grup per aquesta comanda\"><p class='compacte'>A/C:<font  ".$styleAC.">".(number_format($sumaAbonamentsCarrecsUms,2,'.',''))."</font> ums<p></a>
			<p class='compacte'>".(number_format($mRebostsAmbComandaResum[$grupRef]['kgT'],2,'.',''))." kg<p>
			<p class='compacte'>&nbsp;</p>
			";
			if($cAp_ums>0)
			{
				echo "<p class='albara'>".(formatarData(getDataComanda($db,$mRebost['id'])))."</p>";
			}
					if($mFormaPagamentMembres['fPagamentUms']!=0)
					{
						echo "
					<p>
					Punt d'entrega:
					<br>";
						$puntEntrega=db_getPuntEntrega($grupRef,$db);
						if($puntEntrega=='no_assignat')
						{
							echo "<b><font color='red'>".$puntEntrega."</font></b>";
						}
						else
						{
							echo "<b>".(urldecode($mGrupsRef[$puntEntrega]['nom']))."</b>";
							if(substr_count($mGrupsRef[$puntEntrega]['recepcions'],','.$grupRef.',')>0)
							{
								echo " (<font color='green'>ACCEPTAT</font>)";
							}
							else
							{
								echo " (<font color='red'>PENDENT</font>)";
							}
						}
						echo "
					</p>
						";	
					}
					echo "
			</td>

";
//columna 1.3
echo "			
			
			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format(($GEdistribUms),2,'.',''))."</b> ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($GEdistribEcos),2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($GEdistribEb),2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($GEdistribEu),2,'.',''))." euros</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>
					";
					if($GEdistribUms>0)
					{
						echo "
						<br><br>".(number_format((($GEdistribEcos)/$GEdistribUms)*100,2,'.',''))." % MS productores
						";
					}
					echo "
					</p>
					</td>
				</tr>
			</table>
			</td>

";
//columna 1.4
echo "			
			
			
			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p>".(number_format($cAp_ums,2,'.',''))." ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cAp_ecos),2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cAp_euros),2,'.',''))." euros</p>
					</td>
				</tr>
				<tr>
					<td>
					";
					if($haProduit)
					{
						echo "
					<p onClick=\"enviarFpars('gestioMagatzems.php?sR=".$mPars['selRutaSufix']."&mRef=0&vPr=".$mPerfilVinculat['id']."','_blank');\" style='cursor:pointer;'><u>Total Reservat</u></p>
					<p> ".(number_format($mPerfilVinculat['acord1'],2,'.',''))." % ms Acord<br>
						";
						if(number_format($mPerfilVinculat['acord1'],2,'.','')!=number_format(($cAp_ecos/$cAp_ums)*100,2,'.',''))
						{
							echo "
						<a style='color:#ff0000;' title=\"el % ms promig productes reservats NO coincideix amb l'acord amb el productor\">(<b>".(number_format(($cAp_ecos/$cAp_ums)*100,2,'.',''))."</b> % ms reserves)</a>
							";
						}
						else
						{
							echo "
						<a title=\"el % ms promig productes reservats coincideix amb l'acord amb el productor\">(".(number_format(($cAp_ecos/$cAp_ums)*100,2,'.',''))." % ms reserves)</a>
							";
						}
						echo "
					</p>
						";	
					}			
					echo "
					</td>
				</tr>
			</table>
			</td>

";
//columna 1.5
echo "			
			

			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($GEdistribUms+$cAp_ums,2,'.',''))."</b> ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($GEdistribEcos+$cAp_ecos,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($GEdistribEb,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($GEdistribEu+$cAp_euros,2,'.',''))." euros</p>
					</td>
				</tr>
			</table>
			</td>

";


//columna 1.6
echo "			

			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cpUms+$cAp_ums,2,'.',''))."</b> ums</p>
					</td>
				</tr>

";
if($mPars['selRutaSufixPeriode']*1<$mParametres['periodeIniciRecolzamentSelectiu']['valor'])
{
	if(($cpUms+$cAp_ums)<0) //la CAC ha de pagar i s'aplica l'acord de compra. Si l'acord es menor que el %ms dels prods oferits, la cac aplica el %ms dels prods oferitys 
	//if(($cpUms+$cAp_ums+$invUms)<0) //la CAC ha de pagar i s'aplica l'acord de compra. Si l'acord es menor que el %ms dels prods oferits, la cac aplica el %ms dels prods oferitys 
	{
		echo "
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*($cAp_ecos/$cAp_ums),2,'.',''))." ecos
					";//<p>".(number_format(($cpUms+$cAp_ums+$invUms)*($cAp_ecos/$cAp_ums),2,'.',''))." ecos
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*(100-($cAp_ecos/$cAp_ums)*100)/100,2,'.',''))." euros</p>
					";//<p>".(number_format(($cpUms+$cAp_ums+$invUms)*(100-($cAp_ecos/$cAp_ums)*100)/100,2,'.',''))." euros</p>
					echo "
					<br>
					<p class='p_micro2'>
					* CAC paga<br>	aplicat: ".(number_format(($cAp_ecos/$cAp_ums)*100,2,'.',''))."% ms segons productes</p>
					</td>
				</tr>
		";
	}
	else  //la CAC ha de cobrar i s'aplica la forma de pagament del grup
	{
		echo "
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*$mPars['fPagamentEcosPerc']/100,2,'.',''))." ecos</p>
					";//<p>".(number_format(($cpUms+$cAp_ums+$invUms)*$mPars['fPagamentEcosPerc']/100,2,'.',''))." ecos</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*$mPars['fPagamentEbPerc']/100,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					";//<p>".(number_format(($cpUms+$cAp_ums+$invUms)*$mPars['fPagamentEbPerc']/100,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*$mPars['fPagamentEuPerc']/100,2,'.',''))." euros</p>
					";//<p>".(number_format(($cpUms+$cAp_ums+$invUms)*$mPars['fPagamentEuPerc']/100,2,'.',''))." euros</p>
					echo "
					<br>
					<p class='p_micro2'>
					* CAC cobra<br> %ms i %".$mParametres['moneda3Abrev']['valor']." segons forma pagament del grup
					</p>
					</td>
				</tr>
		";
	}	
}	
else
{
	echo "
				<tr>
					<td>
					<p>".(number_format(($cpEcos+$cAp_ecos),2,'.',''))." ecos
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpEu+$cAp_euros),2,'.',''))." euros</p>
					</td>
				</tr>
	";
}
echo "
			</table>
			</td>
";

//columna 1.7
echo "			
			
			<td valign='top'  bgcolor='#eeeeee'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(@number_format($invUms,2,'.',''))."</b> ums</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(@number_format(($invEcos),2,'.',''))." ecos
					</td>
				</tr>

				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(@number_format(($invEuros),2,'.',''))." euros</p>
					</td>
				</tr>
			</table>
			";
			if(isset($mPerfilVinculat['id']) && $mPerfilVinculat['id']!='' && $invUms!=0)
			{
				echo "
			<p onClick=\"enviarFpars('inventariPerProductors.php?sR=".$mPars['selRutaSufix']."&mRef=0&vPr=".$mPerfilVinculat['id']."','_blank');\" style='cursor:pointer;'><u>Estoc CAC</u></p>
				";
			}
			echo "
			</td>
";
//columna 1.8


echo "			
			
			<td valign='top'  bgcolor='#eeeeee'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(@number_format($invUmsR,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(@number_format($invEcosR,2,'.',''))." ecos
					</td>
				</tr>

				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(number_format($invEurosR,2,'.',''))." euros</p>
					</td>
				</tr>
			</table>
			</td>
";
//columna 1.9
echo "			
			
			<td valign='top'  bgcolor='#FFDB58'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(@number_format($cpUms+$cAp_ums+$invUmsR,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(@number_format(($cpEcos+$cAp_ecos+$invEcosR),2,'.',''))." ecos
					</td>
				</tr>

				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(number_format(($cpEu+$cAp_euros+$invEurosR),2,'.',''))." euros</p>
					</td>
				</tr>
				<tr>
					<td>
					<p class='p_micro2'>
";
//*v36-18-12-15 1 assignacio
	$recolzament=-1*($GEdistribEcos-$cpEcos);

$recolzamentT+=$recolzament;
/*
if($cpUms>=-1*$cAp_ums+$invUmsR){$umsIntercanvi=-1*$cAp_ums+$invUmsR;}else{$umsIntercanvi=$cpUms;}
if($cAp_ums!=0 && $cpUms!=0)
{
	$recolzamentIntercanvi=$umsIntercanvi*(($GEdistribUms-$GEdistribEcos)/$GEdistribUms-(-1*$cAp_ums+$invUms+$cAp_ecos)/(-1*$cAp_ums+$invUms));
}
$recolzamentIntercanviT+=$recolzamentIntercanvi;
*/
if($recolzament>=0)
{
	$recolzamentTotalSuportatT+=$recolzament;
}
else
{
	$recolzamentTotalRebutT+=-1*$recolzament;
}

/*
if($recolzamentIntercanvi>=0)
{
	$recolzamentIntercanviSuportatT+=$recolzamentIntercanvi;
}
else
{
	$recolzamentIntercanviRebutT+=-1*$recolzamentIntercanvi;
}
*/
echo "
					Recolzament:
					<br><b>".(number_format($recolzament,2,'.',''))."</b> ecos
					<br>
					[max: ".$mComptesGrupsRef[$grupRef]['saldo_ecos_r']." ecos]
";
/*
echo "
					Recolzament intercanvi:
					<br><b>".(number_format($recolzamentIntercanvi,2,'.',''))."</b> ecos
";
*/
echo "
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		";
		$recolzament=0;
		$recolzamentIntercanvi=0;
		
		$contLinies++;
		$cont++;

		//if($grupRef!=62) //si es CAC -despeses-ruta, la distribuci� no es comptabilitza  
		//{
			//totals kg
			$kgT+=$mRebostsAmbComandaResum[$grupRef]['kgT'];
			
		
			// totals FP distribucio
			$sumaAbonamentsCarrecsUmsTT+=$sumaAbonamentsCarrecsUmsT;

			$cpUmsT+=$cpUms;
			$cpEcosT+=$cpEcos;
			$cpEbT+=$cpEb;
			$cpEuT+=$cpEu;

			// totals GE distribucio
			$GEdistribUmsT+=$GEdistribUms;
			$GEdistribEcosT+=$GEdistribEcos;
			$GEdistribEbT+=$GEdistribEb;
			$GEdistribEuT+=$GEdistribEu;

			//totals GE abastiment
			$cAp_umsT+=$cAp_ums;
			$cAp_ecosT+=$cAp_ecos;
			$cAp_eurosT+=$cAp_euros;
			$cAp_umsCtT+=$cAp_umsCt;
			$cAp_ecosCtT+=$cAp_ecosCt;
			$cAp_eurosCtT+=$cAp_eurosCt;
			$cAp_umsFdT+=$cAp_umsFd;
			$cAp_ecosFdT+=$cAp_ecosFd;
			$cAp_eurosFdT+=$cAp_eurosFd;
			
			//totals  productes 'dip�sit' en estoc
			$invUmsT+=$invUms;
			$invEcosT+=$invEcos;
			$invEurosT+=$invEuros;

			$invUmsRT+=$invUmsR;
			$invEcosRT+=$invEcosR;
			$invEurosRT+=$invEurosR;
			
		//}
	    //unset($mComandaPerProductors[62]); //grup CAC-despeses-ruta
		
		//extraiem de la matriu el productors amb grup vinculat
		unset($mComandaPerProductors[$mPerfilVinculat['id']]);
		
	}
	}


}
reset($mRebostsAmbComandaResum);


		

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

// repetim pels productors sense grup vinculat
reset($mComandaPerProductors);

while(list($perfilRef,$mComandaProductor)=each($mComandaPerProductors))
{
	$haProduit=0;
	$haConsumit=0;

	$bUms=0;

		//consum
			$mRebostsAmbComandaResum[$perfilRef]['kgT']=0;
			$mFormaPagament=array();
			$mFormaPagamentMembres=array();
			$mPars['fPagamentEcos']=0;
			$mPars['fPagamentEb']=0;
			$mPars['fPagamentEu']=0;
			$mPars['fPagamentUms']=0;
			$mPars['fPagamentEcosPerc']=100;
			$mPars['fPagamentEbPerc']=0;
			$mPars['fPagamentEuPerc']=0;
			//$mAbonamentsCarrecs=array();
		
		//produccio
		$cAp_ums=0;
		$cAp_ecos=0;
		$cAp_euros=0;

		$invUms=0.0;
		$invEcos=0.0;
		$invEuros=0.0;

		$invUmsR=0.0;
		$invEcosR=0.0;
		$invEurosR=0.0;

		while(list($producteId,$mComandaAproductor)=@each($mComandaPerProductors[$perfilRef]))
		{
			if(substr_count($mProductes[$producteId]['tipus'],'especial')==0)
			{
				$cAp_ums-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
				$cAp_ecos-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
				$cAp_euros-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;;

		
				//productes 'dip�sit' en estoc
//				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0 &&  $mPerfilVinculat['ref']!='63')
				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0)
				{
					if(!isset($mInventariGT['inventariRef'][$producteId])){$mInventariGT['inventariRef'][$producteId]=0;}
					$invUms+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu'];
					$invEcos+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
					$invEuros+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
				}

				//productes 'dip�sit' en estoc que s'han reservat
//				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0 && $mPerfilVinculat['ref']!='63')
				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0)
				{
					if($mComandaAproductor['quantitatT']<$mInventariGT['inventariRef'][$producteId])
					{
						$invUmsR+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
						$invEcosR+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEurosR+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
					else
					{
						$invUmsR+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu'];
						$invEcosR+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEurosR+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
				}
			}
		}
		if($cAp_ums<0){$haProduit=1;}
		
		$colorLinia='#ffffff';
		$colorLinia=$mColorLinia['haProduit2'];

	
	if( $cAp_ums!=0)
	{
		if($contLinies==1)
		{	
			html_nomsColumnes2();
			$contLinies=0;
		}

		echo "
		<tr bgcolor='".$colorLinia."'>

";
//columna 2.1
echo "
			<td valign='top'>
			<p>[".$cont."]<br></p>
			<b>".(urldecode($mPerfilsRef[$perfilRef]['projecte']))."</b> 	
			<p class='p_micro2'>
			Responsable: <b>".(urldecode(@$mUsuarisRef[$mPerfilsRef[$perfilRef]['usuari_id']]['nom']))."</b>
			<br>
			email: ".@$mUsuarisRef[$mPerfilsRef[$perfilRef]['usuari_id']]['email']."
			<br>
			mobil: ".@$mUsuarisRef[$mPerfilsRef[$perfilRef]['usuari_id']]['mobil']."
			<br>
			adre�a: ".(urldecode($mPerfilsRef[$perfilRef]['adressa']))."
			</p>
			</td>
			";
			
			//$cpUms=$mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu']+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
//columna 2.2
			
			
			echo "
			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>0.00</b> ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 euros</p>
					</td>
				</tr>
			</table>
			</td>

";
//columna 2.3
echo "
			
			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>0.00</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 euros</p>
					</td>
				</tr>
			</table>
			</td>

";
//columna 2.4
echo "
			
			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cAp_ums,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cAp_ecos,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cAp_euros,2,'.',''))." euros</p>
					</td>
				</tr>
				<tr>
					<td>
					<p onClick=\"enviarFpars('gestioMagatzems.php?sR=".$mPars['selRutaSufix']."&mRef=0&vPr=".$perfilRef."','_blank');\" style='cursor:pointer;'><u>Total Reservat</u></p>
					<p> ".(number_format($mPerfilsRef[$perfilRef]['acord1'],2,'.',''))." % ms Acord<br>
					";
					if(number_format($mPerfilsRef[$perfilRef]['acord1'],2,'.','')!=number_format(($cAp_ecos/$cAp_ums)*100,2,'.',''))
					{
						echo "
						<a style='color:#ff0000;' title=\"el % ms promig productes reservats NO coincideix amb l'acord amb el productor\">(<b>".(number_format(($cAp_ecos/$cAp_ums)*100,2,'.',''))."</b> % ms reserves)</a>
						";
					}
					else
					{
						echo "
						<a title=\"el % ms promig productes reservats coincideix amb l'acord amb el productor\">(".(number_format(($cAp_ecos/$cAp_ums)*100,2,'.',''))." % ms reserves)</a>
						";
					}
					echo "
					</p>
					</td>
				</tr>
			</table>
			</td>

";
//columna 2.5
echo "
	
			
			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cAp_ums,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cAp_ecos),2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cAp_euros,2,'.',''))." euros</p>
					</td>
				</tr>
			</table>
			</td>
";
//columna 2.6
echo "

			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cAp_ums,2,'.',''))."</b>&nbsp;ums</p>
					";//<p><b>".(number_format($cAp_ums+$invUms,2,'.',''))."</b> ums</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cAp_ums)*$mPerfilsRef[$perfilRef]['acord1']/100,2,'.',''))." ecos</p>
					";//<p>".(number_format(($cAp_ums+$invUms)*$mPerfilsRef[$perfilRef]['acord1']/100,2,'.',''))." ecos</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cAp_ums)*(100-$mPerfilsRef[$perfilRef]['acord1'])/100,2,'.',''))." euros</p>
					";//<p>".(number_format(($cAp_ums+$invUms)*(100-$mPerfilsRef[$perfilRef]['acord1'])/100,2,'.',''))." euros</p>
					echo "
					</td>
				</tr>
			</table>
			</td>
			";

//columna 2.7

			echo "
			<td valign='top'  bgcolor='#eeeeee'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($invUms,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($invEcos,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($invEuros,2,'.',''))." euros</p>
					</td>
				</tr>
			</table>
			";
			if($perfilRef!='' && $invUms!=0)
			{
				echo "
			<p onClick=\"enviarFpars('inventariPerProductors.php?sR=".$mPars['selRutaSufix']."&mRef=0&vPr=".$perfilRef."','_blank');\" style='cursor:pointer;'><u>Estoc CAC</u></p>
				";
			}
			echo "
			</td>
			";

//columna 2.8

			echo "
			<td valign='top'  bgcolor='#eeeeee'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($invUmsR,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($invEcosR,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($invEurosR,2,'.',''))." euros</p>
					</td>
				</tr>
			</table>
			</td>
";
//columna 2.9

			echo "
			<td valign='top'  bgcolor='#FFDB58'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cAp_ums+$invUmsR,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format((($cAp_ums)*$mPerfilsRef[$perfilRef]['acord1']/100)+$invEcosR,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format((($cAp_ums)*(100-$mPerfilsRef[$perfilRef]['acord1'])/100)+$invEurosR,2,'.',''))." euros</p>
					</td>
				</tr>
			</table>
			</td>
		";
		$recolzament=0;
		$recolzamentIntercanvi=0;


		//FP totals distribucio
		// 0.00

		//GE totals distribucio
		// 0.00

		//totals Abastiment
		$cAp_umsT+=$cAp_ums;
		$cAp_ecosT+=$cAp_ecos;
		$cAp_eurosT+=$cAp_euros;
		
		$invUmsT+=$invUms;
		$invEcosT+=$invEcos;
		$invEurosT+=$invEuros;

		$cont++;
		
		$contLinies++;
	}
}
reset($mRebostsAmbComandaResum);



echo "
		<tr>
			<td  bgcolor='#FFFFFF' width='15%' valign='top'><br><p style='text-align:center;'>
			I
			</p><p><b></b></p></td>
			
			<td  bgcolor='#FFFFFF' width='15%' valign='top'><br><p style='text-align:center;'>
			II
			</p><a title=\"totals FP-Import distribuci� CAC al Grup segons forma pagament grup. Inclou preu total productes, cost transport i fons liquidesa CAC.\n\n Inclou abonaments i c�rrecs \">
			<p class='albara'>Total <b>Ingressos CAC</b> procedents del que s'ha distribuit als grups segons al seva forma de pagament. Inclou cost transport, fons despeses CAC i abonaments i c�rrecs</p></a></td>
			
			<td  bgcolor='#FFFFFF' width='15%' valign='top'><br><p style='text-align:center;'>
			III
			</p><a title=\"totals GE-Import distribuci� CAC al grup segons % ms productors.\n\n \">
			<p class='albara'>Total <b>ingressos CAC</b> procedents del que s'ha distribuit als grups, segons el % de ms dels productors. Inclou cost transport, fons despeses CAC i abonaments i c�rrecs</p></a></td>
			
			<td  bgcolor='#FFFFFF' width='15%' valign='top'><br><p style='text-align:center;'>
			IV
			</p><a title=\"totals GE- Import Abastiment CAC de tots els productors, segons % ms del productor. Inclou nom�s preu de compra dels productes als productors. \">
			<p class='albara'>Total <b>despeses CAC</b> productors, segons % ms dels seus productes.</p></a></td>
			
			<td  bgcolor='#FFFFFF' width='15%' valign='top'><br><p style='text-align:center;'>
			V
			</p><a title=\"totals GE - Balan� monetari: Import Distribuci� CAC - Import Abastiment CAC d'aquest productor\">
			<p class='albara'>Total [<b>ingressos CAC(III) - despeses CAC(IV)</b> productors]<br>(segons el % de ms dels productors)<br> = [ingressos cost transport + fons despeses + abonaments i c�rrecs]</p></a></td>
			
			<td  bgcolor='#FFFFFF' width='15%' valign='top'><br><p style='text-align:center;'>
			VI
			</p><a title=\"totals Saldos a favor de la CAC\">
			<p class='albara'>Total [<b>ingressos CAC(II) - despeses CAC(IV)</b> productors</b>]<br>(segons la forma de pagament dels grups despr�s d'aplicar l'intercanvi  UMS_consum - UMS_Producci� )<br> = [ingressos cost transport + fons despeses + abonaments i c�rrecs]</p></a></td>
			
			<td  bgcolor='#eeeeee' width='15%' valign='top'><br><p style='text-align:center;'>
			VII
			</p><a title=\"Total valor Estoc CAC: Import total dels productes 'dip�sit'  a l'inventari actual de la CAC
* les dades procedeixen dels �ltims inventaris de tots els magatzems locals i potser no estan actualitzades.\">
			<p class='albara'>(GE)<br><b>Valor Total<br>Estoc CAC<br>productes<br>'dip�sit':</b></p></a>
			<div style='height: 150px; overflow-y:auto;'>
			<p class='p_micro2'>
			<b>Inventaris</b>:<br>";
			
			$mInventariData=@explode('}',$mInventariGT['data']);
			
			$inventariData='';
			while(list($key,$inventariData_)=each($mInventariData))
			{
				if($inventariData_!='')
				{
					$inventariData_=str_replace('{','',$inventariData_);
					$mInventariData_2=explode(';',$inventariData_);
					//vd(substr($mInventariData_[0],0,strpos($mInventariData_[0],'-')));
					$inventariData.=(urldecode(@$mGrupsRef[$mInventariData_2[0]]['nom'])).'<br>'.$mInventariData_2[1].'<br>'.$mInventariData_2[2].'<br><br>';
				}
			}
			
			echo $inventariData."
			</p>			
			</div>
			</td>

			<td  bgcolor='#eeeeee' width='15%' valign='top'><br><p style='text-align:center;'>
			VIII
			</p><a title=\"Total valor Estoc CAC Reservat: Import total dels productes 'dip�sit' amb estoc positiu  a l'inventari actual de la CAC.\">
			<p class='albara'>(GE)<br><b>Valor Total<br>Estoc CAC Reservat<br>productes<br>'dip�sit':</b></p></a>
			</td>

			<td  bgcolor='#eeeeee' width='15%' valign='top'><br><p style='text-align:center;'>
			IX
			</p><a title=\"Total Balan� Intercanvi + valor Estoc CAC Reservat.\">
			<p class='albara'>(GE)<br><b>Total [Balan� Intercanvi(VI) + valor Estoc CAC Reservat(VIII)]:</b></p></a>
			</td>
		</tr>

		<tr>
";
//columna 3.1
echo "
			<td   bgcolor='#ffffff' valign='bottom'>
			</td>

";
//columna 3.2
echo "
			
			<td   valign='top'  bgcolor='#ffffff'>
			";/*--- TOTALS FP-DISTRIBUCIO  --- */echo "
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cpUmsT,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cpEcosT,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cpEbT,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cpEuT,2,'.',''))." euros</p>
					</td>
				</tr>

				<tr>
					<td>
					<p style='color:#999999;'><i>
					<b>Total Abonaments<br>i C�rrecs:</b><br>
					<b>".(number_format($sumaAbonamentsCarrecsUmsTT,2,'.',''))."</b> ums<br>
					<b>Total kg:</b><br>
					".$kgT." kg 
					</i>
					</p>
					</td>
				</tr>
			</table>
			</td>
			
";
//columna 3.3
echo "

			
			<td   valign='top'  bgcolor='#ffffff'>
			";/*--- TOTALS GE-DISTRIBUCIO  --- */echo "
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($GEdistribUmsT,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($GEdistribEcosT,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($GEdistribEbT,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($GEdistribEuT,2,'.',''))." euros</p>
					</td>
				</tr>
			</table>
			</td>

";
//columna 3.4
echo "


			<td   valign='top'  bgcolor='#ffffff'>
			";/*--- TOTALS GE-ABASTIMENT  --- */echo "
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cAp_umsT,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format($cAp_ecosT,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format($cAp_eurosT,2,'.',''))." euros</p>
					</td>
				</tr>
			</table>
			</td>

";
//columna 3.5
echo "
			

			<td   valign='top'  bgcolor='#ffffff'>
			";/*--- TOTALS GE-DISTRIBUCIO-ABASTIMENT  --- */echo "
			<table width='100%'>
				<tr>
					<td>
					<p ><b>".(number_format(($cAp_umsT+$GEdistribUmsT),2,'.',''))."</b>&nbsp;ums</p>
					";//<p ><b>".(number_format(($cAp_umsT+$invUmsT+$GEdistribUmsT),2,'.',''))."</b> ums</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format(($cAp_ecosT+$GEdistribEcosT),2,'.',''))." ecos</p>
					";//<p >".(number_format(($cAp_ecosT+$invEcosT+$GEdistribEcosT),2,'.',''))." ecos</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format(($GEdistribEbT),2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					";//<p >".(number_format(($GEdistribEbT),2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format($cAp_eurosT+$GEdistribEuT,2,'.',''))." euros</p>
					";//<p >".(number_format($cAp_eurosT+$invEurosT+$GEdistribEuT,2,'.',''))." euros</p>
					echo "
					</td>
				</tr>
			</table>
			</td>


";
//columna 3.6
echo "
			
			
			<td   valign='top'  bgcolor='#ffffff'>
			";/*--- TOTALS Intercanvi DISTRIBUCIO  --- */echo "
			<table width='100%'>
				<tr>
					<td>
					<p ><b>".(number_format($cpUmsT+$cAp_umsT,2,'.',''))."</b>&nbsp;ums</p>
					";//<p ><b>".(number_format($cpUmsT+$cAp_umsT+$invUms,2,'.',''))."</b> ums</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format($cpEcosT+$cAp_ecosT,2,'.',''))." ecos</p>
					";//<p >".(number_format($cpEcosT+$cAp_ecosT+$invEcos,2,'.',''))." ecos</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format($cpEbT,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					";//<p >".(number_format($cpEbT,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					echo "
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cpEuT+$cAp_eurosT,2,'.',''))."</b> euros</p>
					";//<p>".(number_format($cpEuT+$cAp_eurosT+$invEuros,2,'.',''))."</b> euros</p>
					echo "
					</td>
				</tr>
			</table>
			</td>
";
//columna 3.7
echo "
			
			
			<td   valign='top'  bgcolor='#eeeeee'>
			";/*--- TOTALS Estoc CAC  --- */echo "
			<table width='100%'>
				<tr>
					<td>
					<p ><b>".(number_format($invUmsT,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format($invEcosT,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format(0,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($invEurosT,2,'.',''))."</b> euros</p>
					</td>
				</tr>
			</table>
			</td>

";
//columna 3.8
echo "
			
			<td   valign='top'  bgcolor='#eeeeee'>
			";/*--- TOTALS Estoc CAC Reservat  --- */echo "
			<table width='100%'>
				<tr>
					<td>
					<p ><b>".(number_format($invUmsRT,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format($invEcosRT,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format(0,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($invEurosRT,2,'.',''))."</b> euros</p>
					</td>
				</tr>
			</table>
			</td>
";
//columna 3.9
echo "
			
			<td   valign='top'  bgcolor='#eeeeee'>
			";/*--- TOTALS Balan� + Estoc CAC Reservat  --- */echo "
			<table width='100%'>
				<tr>
					<td>
					<p ><b>".(number_format($cpUmsT+$cAp_umsT+$invUmsRT,2,'.',''))."</b>&nbsp;ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format($cpEcosT+$cAp_ecosT+$invEcosRT,2,'.',''))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p >".(number_format(0,2,'.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cpEuT+$cAp_eurosT+$invEurosRT,2,'.',''))."</b> euros</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>

		<tr>

";
//columna 4.1
echo "
			<td  bgcolor='#FFFFaa' width='15%' valign='top'>
			</td>
";
//columna 4.2
echo "

			<td  bgcolor='#FFFFaa' width='15%' valign='top'>
			";
			$mMissatgeAlerta=db_actualitzarFormesPagamentGrups('',0,$db);
			echo "
			<p>
			Desfa�ament entre<br>les comandes dels usuaris<br>i la columna II:<br>
			".(number_format($cpUmsT,2,'.',''))." - ".(number_format($mPars['importsGrups'],2,'.',''))."=<b>".(number_format($cpUmsT-$mPars['importsGrups'],2,'.',''))."</b>
			</p>
			<p class='p_micro'>
			* La difer�ncia ha de coincidir amb la suma d'abonaments i c�rrecs.
			</p>
			";
			if($mPars['nivell']=='sadmin' && $mPars['selRutaSufixPeriode']>1508)
			{
				echo "
			<a title=\"Actualitzar� els imports de les comandes de cada usuari de cada grup i tamb� l'import de cada grup segons els imports dels usuaris. Mant� els % monetaris actuals dels usuaris i del grup. Les diferencies solen ser per canvis de preus dels productes o perqu� el responsable de grup no ha actualitzat la forma de pagament del grup.
-Per a la propagacio de les formes de pagament correctes pot ser necessari premer aquest boto dues vegades
-Tamb� eliminar� comandes actives sense cap producte reservat\">
			<input class='i_micro' type='button' onClick=\"javascript:actualitzarFormesPagamentGrups();\" value='[".number_format($mPars['importsGrups'],2,'.','')."] actualitzar imports usuaris i grups'>
			[sadmin]</a> 
				";
			}
			echo "
			</td>
";
//columna 4.3
echo "

			<td  bgcolor='#FFFFaa' width='15%' valign='top'>
			<p>
			Desfa�ament entre<br> les comandes dels usuaris<br>i la columna III:<br>
			".(number_format($GEdistribUmsT,2,'.',''))." - ".(number_format($mPars['importsGrups'],2,'.',''))."=<b>".(number_format($GEdistribUmsT-$mPars['importsGrups'],2,'.',''))."</b>
			</p>
			<p class='p_micro'>
			* La difer�ncia ha de coincidir amb la suma d'abonaments i c�rrecs.
			</p>
			</td>
";
//columna 4.4
echo "

			<td  bgcolor='#FFFFaa' width='15%' valign='top'><p></p></td>
";
//columna 4.5
echo "
			<td  bgcolor='#FFFFaa' width='15%' valign='top'>
			<a title=\"total difer�ncies a favor de la CAC entre els ingressos segons els % de ms dels productes i els ingressos segons les formes de pagament dels grups\"><p><b>Desbalan� monetari:</b><br> (VI-V)<br>
			(difer�ncies a favor de la CAC)</p></a>
			</td>
";
//columna 4.6
echo "
			<td  bgcolor='#FFFFaa' width='15%' valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p>ecos</p>
					</td>
					<td>
					<p>".(number_format($cpEcosT-$GEdistribEcosT,2,'.',''))."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".$mParametres['moneda3Nom']['valor']."</p>
					</td>
					<td>
					<p>".(number_format($cpEbT-$GEdistribEbT,2,'.',''))."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>euros</p>
					</td>
					<td>
					<p><b>".(number_format($cpEuT-$GEdistribEuT,2,'.',''))."</b></p>
					</td>
				</tr>
			</table>
			</p>
			</td>
";
//columna 4.7
echo "

			<td  bgcolor='#FFFFaa' width='15%' valign='top'>
			</td>

";
//columna 4.8
echo "
			<td  bgcolor='#FFFFaa' width='15%' valign='top'>
			<a title=\"total difer�ncies a favor de la CAC entre els ingressos segons els % de ms dels productes i els ingressos segons les formes de pagament dels grups, en el recompte, sumant els productes amb estoc positiu que s'han reservat\"><p><b>Desbalan� monetari en el Recompte:</b><br> (IX-V)<br>
			(difer�ncies a favor de la CAC)</p></a>
			</td>
";
//columna 4.9
echo "
			<td  bgcolor='#FFFFaa' width='15%' valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p>ecos</p>
					</td>
					<td>
					<p>".(number_format($cpEcosT-$GEdistribEcosT+$invEcosRT,2,'.',''))."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".$mParametres['moneda3Nom']['valor']."</p>
					</td>
					<td>
					<p>".(number_format($cpEbT-$GEdistribEbT,2,'.',''))."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>euros</p>
					</td>
					<td>
					<p><b>".(number_format($cpEuT-$GEdistribEuT+$invEurosRT,2,'.',''))."</b></p>
					</td>
				</tr>
			</table>
			</p>
			</td>
		</tr>

		<tr>
			<td><p></p></td>
			<td><p></p></td>
			<td><p></p></td>
			<td><p></p></td>
			<td><p></p></td>
			<td><p></p></td>
			<td><p></p></td>
			<td><p></p></td>
			<td>
			<p style='text-align:right; color:#000077; font-size:15px;'>
			Recolzament total (ecobasics i ecos):<br><b>".(number_format($recolzamentT,2,'.',''))."</b>
			</p>
			
			<p style='text-align:right; color:#000077; font-size:11px;'>
			Recolzament Total suportat:<br><b>".(number_format($recolzamentTotalSuportatT,2,'.',''))."</b>
			</p>
			
			<p style='text-align:right; color:#000077; font-size:11px;'>
			Recolzament Total Rebut:<br><b>".(number_format(-$recolzamentTotalRebutT,2,'.',''))."</b>
			</p>
			
";
/*
echo "		<p style='text-align:right; color:#000077; font-size:13px;'>
			Recolzament Total d'Intercanvi:<br><b>".(number_format($recolzamentIntercanviT,2,'.',''))."</b>
			</p>

			<p style='text-align:right; color:#000077; font-size:11px;'>
			Recolzament Total Suportat d'Intercanvi:<br><b>".(number_format($recolzamentIntercanviSuportatT,2,'.',''))."</b>
			</p>
			
			<p style='text-align:right; color:#000077; font-size:11px;'>
			Recolzament Total Rebut d'Intercanvi:<br><b>".(number_format($recolzamentIntercanviRebutT,2,'.',''))."</b>
			</p>
";
*/
echo "			
			</td>
		</tr>
		
	</table>

<br>&nbsp;
<center>

<div style='width:80%;'>
<p class='nota' style='text-align:justify;'>
</p> 
</div>

<div style='width:80%;'>
<p style='text-align:left;'>Marques visuals:</p>
<table style='width:100%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haConsumit']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que el Grup ha fet comanda per� no ha produ�t per la CAC en aquesta ruta.
		<td>
	</tr>
</table>
<table style='width:100%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haProduit']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que el Grup ha produit per la CAC per� no ha fet comanda en aquesta ruta.
		<td>
	</tr>
</table>
<table style='width:100%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haProduitIconsumit']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que el Grup ha fet comanda i ha produ�t per la CAC en aquesta ruta, entrant en l'intercanvi directe.
		<td>
	</tr>
</table>
<table style='width:100%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haProduit2']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que aquest Productor no t� Grup al Gestor, per� ha produit per la CAC.
		<td>
	</tr>
</table>
</div>

</center>

";
if(count($mProblemaAmb)>0)
{
	$problemaAmb=implode('<br>',$mProblemaAmb);
	echo "
	<br>
	<table style='border:1px solid red;' align='center'>
		<tr>
			<td align='center'>
			<p style='color:red;'>ATENCIO, detectat problema amb els productes:
			<br>
			<b>".$problemaAmb."</b>
			<br>
			Es possible que aquests productes hagin estat reservats, pero no estan actius i no s'estan oferint
			</p>
			</td>
		</tr>
	</table>
	";
}
	
$mProductesFiltre=db_getProductesFiltre('actiu',1,'pes',0,$db); //actius, pes=0;
if(count($mProductesFiltre)>0)
{
	echo "
	<br>
	<table style='border:1px solid red;' align='center' width='60%'>
		<tr>
			<td align='left' width='100%'>
			<p style='color:red;'>ATENCIO, detectats ".(count($mProductesFiltre))." productes <b>actius</b> amb <b>pes=0</b>:
			<br>
			<br>
			<table width='100%'>
				<tr>
					<th width='10%' align='center'>
					<p style='color:red;'>id</p>
					</th>
					
					<th width='40%' align='center'>
					<p style='color:red;'>producte</p>
					</th>
					
					<th width='40%' align='center'>
					<p style='color:red;'>productor</p>
					</th>
					
					<th width='10%' align='center'>
					<p style='color:red;'>tipus</p>
					</th>
				</tr>
			";
			while(list($key,$mProducteFiltre)=each($mProductesFiltre))
			{
				echo "
				<tr>
					<td align='center'>
					<p style='color:red;'><b>".$mProducteFiltre['id']."</b>&nbsp;</p>
					</td>
					
					<td align='left'>
					<p style='color:red;'>".(urldecode($mProducteFiltre['producte']))."</p>
					</td>
					
					<td align='left'>
					<p style='color:red;'>".(urldecode($mProducteFiltre['productor']))."</p>
					</td>
					
					<td align='left'>
					<p style='color:red;'>".trim($mProducteFiltre['tipus'],',')."</p>
					</td>
				</tr>
				";
			}
			reset($mProductesFiltre);
			echo "
			</table>
			</td>
		</tr>
	</table>
	";
}
$mProductesFiltre=db_getProductesFiltre('actiu',1,'preu',0,$db); //actius, pes=0;
if(count($mProductesFiltre)>0)
{
	echo "
	<br>
	<table style='border:1px solid red;' align='center' width='60%'>
		<tr>
			<td align='left' width='100%'>
			<p style='color:red;'>ATENCIO, detectats ".(count($mProductesFiltre))." productes <b>actius</b> amb <b>preu=0</b>:
			<br>
			<br>
			<table width='100%'>
				<tr>
					<th width='10%' align='center'>
					<p style='color:red;'>id</p>
					</th>
					
					<th width='40%' align='center'>
					<p style='color:red;'>producte</p>
					</th>
					
					<th width='40%' align='center'>
					<p style='color:red;'>productor</p>
					</th>
					
					<th width='10%' align='center'>
					<p style='color:red;'>tipus</p>
					</th>
				</tr>
			";
			while(list($key,$mProducteFiltre)=each($mProductesFiltre))
			{
				echo "
				<tr>
					<td align='center'>
					<p style='color:red;'><b>".$mProducteFiltre['id']."</b>&nbsp;</p>
					</td>
					
					<td align='left'>
					<p style='color:red;'>".(urldecode($mProducteFiltre['producte']))."</p>
					</td>
					
					<td align='left'>
					<p style='color:red;'>".(urldecode($mProducteFiltre['productor']))."</p>
					</td>
					
					<td align='left'>
					<p style='color:red;'>".trim($mProducteFiltre['tipus'],',')."</p>
					</td>
				</tr>
				";
			}
			reset($mProductesFiltre);
			echo "
			</table>
			</td>
		</tr>
	</table>
	";
}

$mGrupsNoZona=db_getGrupsSenseZona($db); //actius, pes=0;
if(count($mGrupsNoZona)>0)
{
	echo "
	<br>
	<table style='border:1px solid red;' align='center' width='30%'>
		<tr>
			<td align='left' width='100%'>
			<p style='color:red;'>ATENCIO, detectats ".(count($mGrupsNoZona))." grups <b>actius</b> sense <b>zona assignada</b>:
			<br>
			<br>
			<table width='100%'>
				<tr>
					<th width='100%' align='center'>
					<p style='color:red;'>productor</p>
					</th>
				</tr>
			";
			while(list($key,$grupId)=each($mGrupsNoZona))
			{
				echo "
				<tr>
					<td align='left'>
					<p style='color:red;'>".(urldecode($mGrupsRef[$grupId]['nom']))."</p>
					</td>
				</tr>
				";
			}
			reset($mGrupsNoZona);
			echo "
			</table>
			</td>
		</tr>
	</table>
	";
}

$mPerfilsNoZona=db_getPerfilsActiusSenseZona($db); //actius, pes=0;
if(count($mPerfilsNoZona)>0)
{
	echo "
	<br>
	<table style='border:1px solid red;' align='center' width='30%'>
		<tr>
			<td align='left' width='100%'>
			<p style='color:red;'>ATENCIO, detectats ".(count($mPerfilsNoZona))." <b>productores </b> sense <b>zona assignada</b>:
			<br>
			<br>
			<table width='100%'>
				<tr>
					<th width='100%' align='center'>
					<p style='color:red;'>productora</p>
					</th>
				</tr>
			";
			while(list($key,$perfilId)=each($mPerfilsNoZona))
			{
				echo "
				<tr>
					<td align='left'>
					<p style='color:red;'>".(urldecode($mPerfilsRef[$perfilId]['projecte']))."</p>
					</td>
				</tr>
				";
			}
			reset($mPerfilsNoZona);
			echo "
			</table>
			</td>
		</tr>
	</table>
	";
}

$mGrupsNoZona=db_getGrupsActiusSenseZona($db); //actius, pes=0;
if(count($mGrupsNoZona)>0)
{
	echo "
	<br>
	<table style='border:1px solid red;' align='center' width='30%'>
		<tr>
			<td align='left' width='100%'>
			<p style='color:red;'>ATENCIO, detectats ".(count($mGrupsNoZona))." <b>grups </b> sense <b>zona assignada</b>:
			<br>
			<br>
			<table width='100%'>
				<tr>
					<th width='100%' align='center'>
					<p style='color:red;'>grup</p>
					</th>
				</tr>
			";
			while(list($key,$grupId)=each($mGrupsNoZona))
			{
				echo "
				<tr>
					<td align='left'>
					<p style='color:red;'>".(urldecode($mGrupsRef[$grupId]['nom']))."</p>
					</td>
				</tr>
				";
			}
			reset($mGrupsNoZona);
			echo "
			</table>
			</td>
		</tr>
	</table>
	";
}

$mPesos=db_getPesos($db);
$mFormats=db_getFormats($db);

	echo "
	<br>
	<center>
	<div style='width:40%; height:200px; overflow-y:scroll; '>
	<table bgcolor='#eeeeee' align='center' width='100%'>
		<tr>
			<td align='left' width='50%'  valign='top'>
			<p><b>Gamma de pesos</b><br>(kg):
			<br>
			<table width='100%'>
			";
			while(list($key,$pes)=each($mPesos))
			{
				echo "
				<tr>
					<td align='left'>
					<p>".$pes."</p>
					</td>
				</tr>
				";
			}
			reset($mPesos);
			echo "
			</table>
			</td>
			
			<td align='left' width='50%' valign='top'>
			<p><b>Gamma de formats</b><br>(unitats de facturaci�):
			<br>
			<table width='100%'>
			";
			while(list($key,$format)=each($mFormats))
			{
				echo "
				<tr>
					<td align='left'>
					<p>".$format."</p>
					</td>
				</tr>
				";
			}
			reset($mFormats);
			echo "
			</table>
			</td>
		</tr>
	</table>
	</div>
	</center>
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
";

$parsChain=makeParsChain($mPars);
html_helpRecipient();

echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='i_afp_grupId' name='i_afp_grupId' value=''>
<input type='hidden' id='i_afp_grupSid' name='i_afp_grupSid' value=''>
</form>
</body>
</html>
";
?>

		