<?php



function html_mostrarProductes($db)
{
	global $mProductes_,$mPars,$mRebostsRef,$mPerfilsRef;
		echo "
	<table width='100%' align='left'>
		<tr>
			<td width='100%' >
			<p style='text-align:left; font-size:14px;'><b>FULL D'INVENTARIS</b></p>
			</td>
		</tr>
	</table>
	<br>
		";
	if($mPars['grup_id']=='0')
	{
		$mNomsColumnes=array('id'=>'id','producte'=>'producte','productor'=>'productor','format'=>'format','unitat_facturacio'=>'unitat<br>facturaci�','tipus'=>'tipus','quantitatInvGlobal'=>'INV<br>Global','estoc_disponible'=>'estoc<br>TE�RIC','estoc_real'=>'estoc&nbsp;REAL');
		echo "
	<table width='100%' align='left'>
		<tr>
			<td width='100%' >
			<p style='text-align:left;'>Magatzem-CAC <b>GLOBAL</b></p>
			<p style='text-align:left;'>".date('d-m-Y  H:i:s')."</p>
			</td>
		</tr>
	</table>
	<br>
		";
	}
	else
	{
		$mNomsColumnes=array('id'=>'id','producte'=>'producte','productor'=>'productor','format'=>'format','unitat_facturacio'=>'unitat<br>facturaci�','tipus'=>'tipus','quantitatInvGlobal'=>'INV<br>Global','estoc_disponible'=>'estoc_disponible<br>(estoc_previst-reservat)','estoc_real'=>'estoc&nbsp;REAL','quantitatInvLocal'=>'INV<br>Local');
		echo "
	<table width='100%' align='left'>
		<tr>
			<td width='100%' >
			<p style='text-align:left;'>Magatzem-CAC <b>LOCAL</b> del grup: <b>".(urldecode($mRebostsRef[$mPars['grup_id']]['nom']))."</b></p>
			<p style='text-align:left;'>".date('d-m-Y  H:i:s')."</p>
			</td>
		</tr>
	</table>
	<br>
		";
	}	
	echo "
	<table width='100%' align='left'>
		<tr>
			<td width='100%' >
			<p style='text-align:left;'>* nom�s es mostren els productes dels productors <b>actius</b></p>
			</td>
		</tr>
	</table>
	<br>
	";
	if($mPars['veureProductesDisponibles']=='1')
	{
		echo "
	<table width='100%' align='left'>
		<tr>
			<td width='100%' >
			<p style='text-align:left;'>* nom�s es mostren els productes <b>actius</b></p>
			</td>
		</tr>
	</table>
	<br>
		";
	}
	else
	{
		echo "
	<table width='100%' align='left'>
		<tr>
			<td width='100%' >
			<p style='text-align:left;'>* es mostren els productes <b>actius</b> i els <b>inactius</b></p>
			</td>
		</tr>
	</table>
	<br>
		";
	}
	echo "
	<table width='100%' align='left' border='1'>
		<tr>
	";
	while(list($key,$val)=each($mNomsColumnes))
	{
		echo "
			<th align='left' valign='top'>
			<p>".$val."</p>
			</th>
		";
	}
	reset($mNomsColumnes);
	echo "	
		</tr>
	";
	while(list($projecte,$mProductes)=each($mProductes_))
	{
	while(list($index,$mProducte)=each($mProductes))
	{
		$perfilId=substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'));
		if
		(
			isset($mPerfilsRef[$perfilId])
			&&
			$mPerfilsRef[$perfilId]['estat']=='1'
			&&
			substr_count(strtolower(urldecode($mProducte['producte'])),'plantilla')==0
		)
		{
		
		if($mProducte['estat']==0){$color='grey';}else{$color='black';}	
		echo "	
		<tr>
		";
		while(list($key,$val)=each($mNomsColumnes))
		{
			$valor='';
			if($key=='producte'){$valor=urldecode($mProducte[$key]);}
			else if($key=='productor' || $key=='unitat_facturacio'){$valor=urldecode($mProducte[$key]);}
			else if($key=='tipus')
			{
				if(substr_count($mProducte[$key],',dip�sit')>0)
				{
					$valor='dip�sit<br>';
				}
						
				if(substr_count($mProducte[$key],',semiPerible')>0)
				{
					$valor.='semiPerible<br>';
				}
			}
			else if($key=='estoc_disponible')
			{
				if(substr_count($mProducte['tipus'],',dip�sit')>0 && substr_count($mProducte['tipus'],',semiPerible')==0)
				{
					$valor='<b>0</b>';
				}
				else
				{
					$valor='<b>'.$mProducte[$key].'</b>';						
				}
			}
			else {$valor=$mProducte[$key];}

			echo "
			<td align='left' valign='top'>
			<p style='color:".$color.";'>".$valor."</p>
			</td>
			";
		}
		}
		reset($mNomsColumnes);
		echo "	
		</tr>
		";
	}
	reset($mProductes);		
	}
	reset($mProductes_);		
	echo "
	</table>
	";
	
	return;
}
?>

		