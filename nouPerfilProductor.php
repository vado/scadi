<?php

/*
formulari grup de nou grup
*/

include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "html_ajuda1.php";
include "html_segells.php";
include "eines.php";
//v37-include
include "db_nouPerfilProductor.php";
include "db_gestioGrup.php";
include "db_ajuda.php";

$mMissatgeAlerta=array();
$mMissatgeAlerta['missatge']='';

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$ruta_=@$_GET['sR'];
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	if(!(isset($mPars['selRutaSufix'])))
	{
		$mPars['selRutaSufix']=$ruta;
	}
}

//*v36-declaracio
$mRuta=explode('_',$mPars['selRutaSufix']);
//*v36-condicio
if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db);
$mGrupsRef=db_getGrupsRef($db);

$mPerfil=db_getPerfil($db);

	$mUsuari=db_getUsuari($mPars['usuari_id'],$db);

	$mGrup=getGrup($db);
if
(
	!checkLogin($db) 
	|| 
	(
		$mPars['nivell']!='sadmin'
		&&
		$mPars['nivell']!='admin'
		&&
		$mPars['nivell']!='coord'
		&&
		$mPars['usuari_id']!=$mGrupsRef[$mPars['grup_id']]['usuari_id']
		&&
		$mPars['usuari_id']!=$mPerfilsRef[$mPars['perfil_id']]['usuari_id']
	)
)
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}


$opcio1='';
$opcio2='';

$mGrup=array();

if(isset($mPars['grup_id']) && $mPars['grup_id']!='' && $mPars['grup_id']!='0')
{
	$mGrup=getGrup($db);
	$mPropietatsGrup=getPropietatsGrup($mGrup['propietats']);
}


$mAjuda['nouPerfilProductor.php']=db_getAjuda('nouPerfilProductor.php',$db);
$mAjuda['html.php']=db_getAjuda('html.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$mRutesSufixes=getRutesSufixes($db);
//v36 5-1-16 assignacio
$mPeriodesInfo=db_getPeriodesInfo($db);

	$mPars['taulaProductes']='productes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandes']='comandes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandesSeg']='comandes_seg';
	$mPars['taulaIncidencies']='incidencies_'.$mPars['selRutaSufix'];
	if(count($mRuta)==2)//periode especial
	{
		$mPars['taulaProductors']='productors_'.$mPars['selRutaSufix'];
	}
	else
	{
		$mPars['taulaProductors']='productors';
	}

if
(
	$mPars['nivell']!='sadmin' 
	&& 
	$mPars['nivell']!='admin' 
	&& 
	$mPars['nivell']!='coord' 
)
{
	$mPerfilsRef=array();
	$mUsuarisRef=array();
}
else
{
	$mUsuarisRef=db_getUsuarisRef($db);
	$mPerfilsRef=db_getPerfilsRefTots($db);
}


if
(
	isset($mPerfil['id'])
	&&
	$mPerfil['id']!='' 
)
{
	$opcio='editarPerfil';
}
else
{
	$opcio='';
}

$mMunicipis=db_getMunicipis2Id($db);
$opcio1=@$_GET['op'];
$opcio2=@$_POST['i_opcio'];
if($opcio1!=''){$opcio=$opcio1;}
else if($opcio2!=''){$opcio=$opcio2;}
//altres accions
//guardar segell perfil

		$mCategoria0perfil=db_getCategoria0Perfil($db);
		$mPropietatsSegellPerfil=db_getPropietatsSegellPerfil($db);	
		$maxValorSegellPerfil=getValorMaxSegell($mPropietatsSegellPerfil,$mCategoria0);

if($guardarSegellPerfilId=@$_POST['i_gSp'])
{
	$mPars['selPerfilRef']=$guardarSegellPerfilId;
	
		$mPropietatsSegellPerfilGuardar=$mPropietatsSegellPerfilConfig;
		while(list($id,$mSegellPerfil)=each($mPropietatsSegellPerfilConfig))
		{
			$mPropietatsSegellPerfilGuardar[$id]=$mPropietatsSegellPerfil[$id];
		}
		reset($mPropietatsSegellPerfilConfig);
		
		while(list($id,$mSegellPerfil)=each($mPropietatsSegellPerfilConfig))
		{
			$propietat=@$_POST['sel_segells_'.$id];
			if(isset($propietat))
			{
				$mPropietatsSegellPerfilGuardar[$id]['estat']=$propietat;
			}
		}
		reset($mPropietatsSegellPerfilConfig);
		$propietatsSegellPerfilChain=makePropietatsSegellPerfil($mPropietatsSegellPerfilGuardar);
		$valorSegellPerfil=getValorSegell($mPropietatsSegellPerfilGuardar,$mCategoria0perfil);
		$mMissatgeAlerta=db_guardarPropietatsSegellPerfil($valorSegellPerfil,$propietatsSegellPerfilChain,$db);

		$opcio='editarPerfil';
		$mPropietatsSegellPerfil=db_getPropietatsSegellPerfil($db);	
	

}

if($perfilTransferir=@$_GET['pId'])
{
	$periodeDesti=$_GET['sP'];
	if(isset($perfilTransferir) && $perfilTransferir!='' && isset($periodeDesti) && $periodeDesti!='')
	{
		if(!db_transferirPerfilAperiode($perfilTransferir,$periodeDesti,$db))
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo4'>Atenci�: No s'ha pogut transferir el perfil al periode seleccionat</p>";
		}
		else
		{
			$mMissatgeAlerta['missatge']="<p  class='pAlertaOk4'>El perfil s'ha transferit correctament al periode seleccionat</p>";
		}		
		$opcio='editarPerfil';
	}
}

if($opcio=='nouPerfil')
{
	$mPars['selPerfilRef']='';
	unset($mPerfil);
	$mPerfil['id']='';
	$mPerfil['projecte']='';
	$mPerfil['grup_vinculat']='';
	$mPerfil['acord1']='';
	$mPerfil['usuari_id']='';
	$mPerfil['municipi_id']='';
	$mPerfil['adressa']='';
	$mPerfil['notes']='';
	$mPerfil['web']='';
	$mPerfil['ram']='';
	$mPerfil['avals']='';
	$mPerfil['descripcio']='';
	$mPerfil['zona']='';
	$mPerfil['compte_ecos']='';
}
else if ($opcio=='crearPerfil')
{
	$mPerfil['projecte']=urlencode(@$_POST['i_projecte']);
	$mPerfil['grup_vinculat']=@$_POST['sel_grupVinculat'];
	$mPerfil['acord1']=@$_POST['i_acord1'];
	$mPerfil['usuari_id']=urlencode(@$_POST['sel_usuariId']);
	$mPerfil['municipi_id']=@$_POST['sel_municipi'];
	$mPerfil['adressa']=urlencode(@$_POST['i_adre�a']);
	$mPerfil['notes']=urlencode(@$_POST['ta_notes']);
	$mPerfil['web']=@$_POST['i_web'];
	$mPerfil['ram']=urlencode(@$_POST['i_ram']);
	$mPerfil['avals']=urlencode(@$_POST['i_avals']);
	$mPerfil['descripcio']=urlencode(@$_POST['ta_descripcio']);
	$mPerfil['zona']=urlencode(@$_POST['sel_zona']);
	$mPerfil['compte_ecos']=strtoupper(@$_POST['i_compteEcos']);
	$mMissatgeAlerta=putNouPerfil($opcio,$mPerfil,$db);
	$mGrup=getGrup($db);

	if(!$mMissatgeAlerta['result'])
	{
		$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>Atenci�: no s'ha pogut crear el nou Perfil</p>";
		//error_log ( date('d:m:Y')." Error DB - putNouPerfil() - nouPerfilProductor.php - l:77" ,0,'errors.php');
	}
	else
	{
		$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>S'ha creat el nou Perfil correctament. <font color='DarkOrange'><b>Recorda editar-lo per activar-lo</b></font></p>";
		if
		(
			$mPars['nivell']!='sadmin'
			&&
			$mPars['nivell']!='admin'
			&&
			$mPars['nivell']!='coord'
		)
		{
			$mPerfilsRef=db_getPerfilsRefGrup($db);
			$mUsuarisRef=db_getUsuarisGrupRef($db);
		}
		else
		{
			$mPerfilsRef=db_getPerfilsRefTots($db);
			if(isset($mPars['grup_id']) && $mPars['grup_id']!='')
			{
				$mUsuarisRef=db_getUsuarisGrupRef($db);
			}
			else
			{
				$mUsuarisRef=db_getUsuarisRef($db);
			}
		}
	}
	$opcio='';
	unset($mPerfil);
}
else  if($opcio=='guardarPerfil')
{
	$mPerfil['id']=$mPars['selPerfilRef'];
	$mPerfil['projecte']=urlencode(@$_POST['i_projecte']);
	$mPerfil['grup_vinculat']=@$_POST['sel_grupVinculat'];
	$mPerfil['acord1']=@$_POST['i_acord1'];
	$mPerfil['usuari_id']=urlencode(@$_POST['sel_usuariId']);
	$mPerfil['municipi_id']=@$_POST['sel_municipi'];
	$mPerfil['adressa']=urlencode(@$_POST['i_adre�a']);
	$mPerfil['notes']=urlencode(@$_POST['ta_notes']);
	$mPerfil['web']=@$_POST['i_web'];
	$mPerfil['ram']=urlencode(@$_POST['i_ram']);
	$mPerfil['avals']=urlencode(@$_POST['i_avals']);
	$mPerfil['descripcio']=urlencode(@$_POST['ta_descripcio']);
	$mPerfil['zona']=urlencode(@$_POST['sel_zona']);
	$mPerfil['compte_ecos']=strtoupper(@$_POST['i_compteEcos']);
	
	$mMissatgeAlerta=putNouPerfil($opcio,$mPerfil,$db);
	if($mPars['grup_id']!='0' && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin')
	{
		$mPerfilsRef=db_getPerfilsRefGrup($db);
		$mGrup=getGrup($db);
		$mPropietatsGrup=getPropietatsGrup($mGrup['propietats']);
	}
	else
	{
		$mPerfilsRef=db_getPerfilsRefTots($db);
	}
	$opcio='editarPerfil';
	//$mPerfil['ref']='';
	//$mPars['selPerfilRef']='';
}
else if($opcio=='desactivarPerfil')
{
	$mMissatgeAlerta=db_activarPerfil(0,$db);

	if($mPars['grup_id']!='0')
	{
		$mPerfilsRef=db_getPerfilsRefGrup($db);
		$mGrup=getGrup($db);
		$mPropietatsGrup=getPropietatsGrup($mGrup['propietats']);
	}
	else
	{
		$mPerfilsRef=db_getPerfilsRefTots($db);
	}
	$opcio='editarPerfil';
	
}
else if($opcio=='activarPerfil')
{
	$mMissatgeAlerta=db_activarPerfil(1,$db);

	if($mPars['grup_id']!='0')
	{
		$mPerfilsRef=db_getPerfilsRefGrup($db);
		$mGrup=getGrup($db);
		$mPropietatsGrup=getPropietatsGrup($mGrup['propietats']);
	}
	else
	{
		$mPerfilsRef=db_getPerfilsRefTots($db);
	}
	$opcio='editarPerfil';
}


if($opcio=='editarPerfil')
{
	if($selPerfilId=@$_GET['iPed'])
	{
		$mPars['selPerfilRef']=$selPerfilId;
	}
	$mPerfil=db_getPerfil($db);
	$mRespPerfil=db_getUsuari($mPerfil['usuari_id'],$db);
	$mUsuarisRef[$mRespPerfil['id']]=$mRespPerfil;
}


$mPropietatsSegellPerfil=db_getPropietatsSegellPerfil($db);	

	$parsChain=makeParsChain($mPars);

$mGrupsZones=getGrupsZones($mParametres['agrupamentZones']['valor']);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>"; echo $htmlTitolPags; echo " - Perfils Productors</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_nouPerfilProductor.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_segells.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>

<script type='text/javascript'  language='JavaScript'>
//v37-2 assignacions
perfilId='".@$mPerfil['id']."';
nivell='".$mPars['nivell']."';

navegador();
mSegellPerfilGrups=new Array();
";
while(list($propietatId,$mPropietat)=@each($mPropietatsSegellPerfil))
{
	echo "
//grup:
mSegellPerfilGrups[".$propietatId."]=new Array('".(implode("','",$mPropietat['grup']))."');
	";
}
@reset($mPropietatsSegellPerfil);
echo "
mSegellProducteGrups=new Array();
";
while(list($propietatId,$mPropietat)=@each($mPropietatsSegellProducte))
{
	echo "
//grup:
mSegellProducteGrups[".$propietatId."]=new Array('".(implode("','",$mPropietat['grup']))."');
	";
}
@reset($mPropietatsSegellProducte);
echo "
</script>

</head>

<body bgcolor='".$mColors['body']."'>
";

html_demo('nouPerfilProductor.php?');
html_helpRecipient();
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			<p>(".$mContinguts['form']['titol'].")</p>
			</td>
		</tr>
	</table>
";
if($mPars['nivell']=='sadmin')
{
//v36.5-funcio call	
	mostrarSelectorRuta(1,'nouPerfilProductor.php');
}
echo "
	</center>
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			".$mMissatgeAlerta['missatge']."
			</td>
		</tr>
	</table>


	<table  width='80%' align='center'  bgcolor='".$mColors['table']."'>
";

if(isset($mPerfil) && $mPars['usuari_id']==$mPerfil['usuari_id']) //responsable perfil
{
	echo "		<tr>
			<td valign='middle' align='left'>
			<p class='compacte' >
			Usuaria:<b> ".(urldecode($mPars['usuari']))."</b> (Responsable del perfil)
			<br><br>
			Perfil:<b> ".(urldecode($mPerfil['projecte']))."</b>
			<br>&nbsp;
			</p>
			</td>
		</tr>
	";
}
else //sadmins
{
	echo "		<tr>
			<td valign='middle' align='left'>
			<p class='compacte' >
			Usuaria:<b> ".(urldecode($mPars['usuari']))."</b> [".$mPars['nivell']."]
			<br>&nbsp;
			</p>
			</td>
		</tr>
	";
}

echo "
	</table>
	<table width='80%' border='0' align='center' bgcolor='green'>
		<tr>
";
	if
	(
		isset($mPerfil) && @$mPerfil['id']!=''
		&&
		(
			$mPars['nivell']=='sadmin'
			||
			$mPars['nivell']=='admin'
		)
	)
	{
		echo "
			<td align='left' width='15%'>
			<p style='cursor:pointer; font-size:10px;  color:white;' onClick=\"javascript: cursor=getMouse(event);mostrarFormAccio(cursor.x,cursor.y,'segellEcocicPerfil');\" >&nbsp;&nbsp;Segell ECOCIC del perfil&nbsp;&nbsp;</p>
			</td>
			";
	}
echo "
			<td align='left'>
			</td>
		</tr>
	</table>
";
if($mPars['usuari_id']!=@$mPerfil['usuari_id'] || $mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' )
{
	echo "
	<table border='1' align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td align='center' style='width:100%'>
			<table width='100%'>
				<tr>
					<td align='center' width='33%'  valign='top'>
					";
					if($mPars['grup_id']!='0')
					{
						echo "
					<p>Selecciona un Perfil Associat al Grup per editar-lo:<br>
						";
					}
					else
					{
						echo "
					<p>Selecciona un Perfil per editar-lo:<br>
						";
					}
					echo "
					<select id='sel_perfilId' name='sel_perfilId' onChange=\"enviarFparsPerfilEditar('nouPerfilProductor.php?sR=".$mPars['selRutaSufix']."&iPed='+this.value+'&op=editarPerfil','_self');\">
					";
					$selected1='';
					$selected2='selected';
					while(list($ref,$mPerfil_)=each($mPerfilsRef))
					{
							if($ref==@$mPerfil['id'])
							{
								$selected1='selected';$selected2='';
								$opcio='editarPerfil';
							}
							else
							{
								$selected1='';
							}
						
						if($mPars['grup_id']!='0')
						{
							if(@substr_count($mPropietatsGrup['idsPerfilsActiusLocal'],','.$ref.',')>0)
							{
								$class='oGrupActiu';
							}
							else
							{
								$class='oGrupInactiu';
							}
						}
						else
						{
							if(@$mPerfil_['estat']==1)
							{
								$class='oGrupActiu';
							}
							else
							{
								$class='oGrupInactiu';
							}
						}
						echo "
					<option   class='".$class."' ".$selected1." value='".$ref."'>".(urldecode($mPerfil_['projecte']))."-".$ref."</option>
						";					
					}
					reset($mPerfilsRef);
					echo "
					<option ".$selected2." value=''></option>
					</select>
					<br>
					</p>
					<p class='p_micro' style='text-align:center;'>* en verd els perfils actius</p>
					</td>
					
					<td align='center' width='33%' valign='middle'>
					<table>
						<tr>
							<td>
							<input type='button' value='Crear nou Perfil' onClick=\"enviarFparsPerfilEditar('nouPerfilProductor.php?sR=".$mPars['selRutaSufix']."&op=nouPerfil','_self');\">
							</td>
						
							<td>
					";
					echo html_ajuda1('nouPerfilProductor.php',1);
					echo "
							</td>
						</tr>
					</table>
					</td>
					
					<td align='center' width='33%' valign='middle'>
					";
					if($mPars['nivell']=='sadmin' && isset($mPerfil['id']) && $mPerfil['id']!='')
					{
						echo "
					<p>Transferir el perfil editat al periode seleccionat: <font color='DarkOrange'>[".$mPars['nivell']."]</font></p>
						";
						mostrarPerfilsTransferir(0,'nouPerfilProductor.php');
						echo "
					<input type='button' onClick=\"javascript:transferirPerfilAperiode();\" value='transferir'>
						";
					}					
					echo "
					</td>
				</tr>
			</table>					
			</td>
		</tr>
	</table>
";
}
$grupsAmbVincleChain=db_getIdsGrupsAmbVincle($db);


if ($opcio=='nouPerfil')
{
	$disabledProjecte='';
	$readonlyGrupVinculat='DISABLED';
	$disabledAcordCAC='DISABLED';
	$readonlyMunicipi='';
	$disabledAdressa='';
	$disabledNotes='DISABLED';
	$disabledWeb='';
	$disabledRam='';
	$disabledDescripcio='';
	$disabledAvals='';
	$readonlyZona='DISABLED';
	$readonlyResponsable='';
	$zonaPerfil='';
	

	if($mPars['nivell']=='sadmin')
	{
		$disabledAcordCAC='';
		$readonlyZona='';
		$readonlyGrupVinculat='';
	}
	else
	{
		if($mPars['grup_id']!='0'){$zonaPerfil=db_getZonaGrup($mPars['grup_id'],$db);}
	}
	
	echo "
	<table border='1' align='center' style='width:80%' bgcolor='".$mColors['table']."'>
		<tr>
			<td align='left' style='width:100%'>
			<br>
			<center><p>[Crear Perfil]</p></center>
			<table  align='center'  style='width:90%'>
				<tr>
					<td style='width:100%' align='left'>
					<form id='f_nouPerfil' name='f_nouPerfil' method='POST' action='nouPerfilProductor.php?sR=".$mPars['selRutaSufix']."' target='_self' >
					<input type='hidden' name='i_opcio' value='crearPerfil'>
					<table align='center'  style='width:100%'>
						<tr>
							<th align='left' valign='top'>
							<p>Projecte </p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_projecte' ".$disabledProjecte." name='i_projecte' size='30' value=''>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Compte ecos </p>
							</th>
							<td align='left' valign='top'>
							<p class='p_micro'>
							<input type='text' ".$disabledProjecte." id='i_compteEcos' name='i_compteEcos' size='8' value=''>
							<br>
							* S'utilitza per al balan� d'intercanvi amb la productora
							<br> pels mesos que  no fa comanda al Grup; en els altres<br>
							casos s'utilitza el nombre de compte de la seva comanda a la CAP.
							</p>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Grup Vinculat</p>
							</th>
							<td align='left' valign='top'>
							<table>
								<tr>
									<td>
									<select ".$readonlyGrupVinculat." id='sel_grupVinculat' name='sel_grupVinculat'>
					";
					while(list($id,$mGrup_)=each($mGrupsRef))
					{
						if(substr_count($grupsAmbVincleChain,','.$id.',')==0)
						{
							$disabledGrupAvincular='';
						}
						else
						{
							$disabledGrupAvincular='DISABLED';
						}
						
						echo "
									<option  ".$disabledGrupAvincular." value='".@$mGrup_['id']."'>".(urldecode($mGrup_['nom']))."</option>
						";
					}
					reset($mGrupsRef);
					echo "
									<option selected value=''></option>
									</select>
									</td>
									<td>
									<p class='p_micro'>(Balan� comptable CAC)</p>
									</td>
								</tr>
							</table>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Acord de compra (CAC)</p>
							</th>
							<td align='left' valign='top'>
							<p><input ".$disabledAcordCAC." type='text' id='i_acord1' name='i_acord1' size='4' value=\"".(@number_format($mPerfil['acord1'],2))."\"> %</p>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Municipi</p>
							</th>
							<td align='left' valign='top'>
							<select ".$readonlyMunicipi." id='sel_municipi' name='sel_municipi'>
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mMunicipi)=each($mMunicipis))
					{
						echo "
							<option ".$selected1." value='".$id."'>".(urldecode($mMunicipi['municipi']))."</option>
						";
					}
					reset($mMunicipis);
					echo "
							<option selected value=''></option>
							</select>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Adre�a:
							</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' ".$disabledAdressa." id='i_adre�a' name='i_adre�a' size='30' value=\"".(urldecode($mPerfil['adressa']))."\">
							</td>
						</tr>

						<tr style='visibility:hidden; z-index:0; position:absolute; top:0px; '>
							<th align='left' valign='top'>
							<p>Notes</p>
							</th>
							<td align='left' valign='top'>
							<textArea ".$disabledNotes."  id='ta_notes'  name='ta_notes' cols='50' rows='7'>".(urldecode($mPerfil['notes']))."</textArea>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>web</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' ".$disabledWeb." id='i_web' name='i_web' size='50' value=\"".$mPerfil['web']."\">
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Ram</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' ".$disabledRam." id='i_ram' name='i_ram' size='50' value=\"".(urldecode($mPerfil['ram']))."\">
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Avals</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' ".$disabledAvals." id='i_avals' name='i_avals' size='50' value=\"".(urldecode($mPerfil['avals']))."\">
							<p class='nota'>* avals separats per comes</p>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Descripci�</p>
							";
							html_notesHTML();
							echo "
							</th>
							<td align='left' valign='top'>
							<textArea  ".$disabledDescripcio." id='ta_descripcio' name='ta_descripcio' cols='50' rows='20'>".(urldecode($mPerfil['descripcio']))."</textArea>
							</td>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<br>
							<p>Zona</p>
							<br>
							</th>
							<td align='left' valign='top'>
							<br>
					";
					$selected2='selected';

					echo "
							<select ".$readonlyZona." id='sel_zona' name='sel_zona'>
					";					
					while(list($sz_,$mZones_)=each($mGrupsZones))
					{
						while(list($key,$zona_)=each($mZones_))
						{
							if($zona_==$zonaPerfil){$selected='selected';$selected2='';}else{$selected='';}
							
							echo "
							<option ".$selected." value='".$zona_."'>".$zona_." (".$sz_.")</option>
							";
						}
						reset($mZones_);
					}
					reset($mGrupsZones);
					echo "
							<option ".$selected2." value=''></option>
							</select>
							<br>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Responsable del perfil</p>
							</th>
							<td align='left' valign='top'>
							<select ".$readonlyResponsable." id='sel_usuariId' name='sel_usuariId'>
					";
					while(list($id,$mUsuari_)=each($mUsuarisRef))
					{
						echo "
							<option value='".$id."'>".(urldecode($mUsuari_['usuari']))." (".$mUsuari_['email'].")</option>
						";
					}
					reset($mUsuarisRef);
					echo "
							<option selected value=''></option>
							</select>
					";
					if($mPars['nivell']!='sadmin')
					{
						echo "
							<p class='p_micro'>* el responsable del perfil ha de ser membre del grup</p>
						";
					}							
					echo "
							</td>
						</tr>
						
						<tr>
							<th align='left' valign='top'>
							<p>&nbsp;</p>
							</th>
							<td align='left' valign='top'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							<input type='button' onClick=\"javascript: checkFormPerfil();\" value='crear nou Perfil'>
							</td>
						</tr>
					</table>
					<input type='hidden'  name='i_pars' value='".$parsChain."'>
					</form>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	";
}
else if($opcio=='editarPerfil')
{
	$disabledProjecte='DISABLED';
	$readonlyGrupVinculat='DISABLED';
	$disabledAcordCAC='DISABLED';
	$readonlyMunicipi='DISABLED';
	$readonlyAdressa='DISABLED';
	$disabledNotes='DISABLED';
	$disabledWeb='DISABLED';
	$disabledRam='DISABLED';
	$disabledDescripcio='DISABLED';
	$disabledAvals='DISABLED';
	$readonlyZona='DISABLED';
	$readonlyResponsable='DISABLED';

	if($mPars['nivell']=='sadmin')
	{
		$disabledProjecte='';
		$readonlyGrupVinculat='';
		$disabledAcordCAC='';
		$readonlyMunicipi='';
		$readonlyAdressa='';
		$disabledWeb='';
		$disabledRam='';
		$disabledDescripcio='';
		$disabledAvals='';
		$readonlyZona='';
		$readonlyResponsable='';
	}
	else if($mPars['usuari_id']==$mPerfil['usuari_id']) //resp. perfil
	{
		$readonlyMunicipi='';
		$readonlyAdressa='';
		$disabledWeb='';
		$disabledRam='';
		$disabledAvals='';
		$disabledDescripcio='';
	}
	else
	{
		if($mPars['grup_id']!='0'){$zonaPerfil=db_getZonaGrup($mPars['grup_id'],$db);}
	}
	echo "
	<table border='1' align='center' style='width:80%' bgcolor='".$mColors['table']."'>
		<tr>
			<td align='left' style='width:60%'>
			<br>
			<center><p>[Editar Perfil]</p></center>
			<table width='100%' align='center'>
				<tr>
					<td width='100%' align='center' valign='top'>
				";

				if
				( 
					$mPars['nivell']=='sadmin'
					||
					$mPars['nivell']=='admin'
					||
					$mPars['nivell']=='coord'
				)
				{
					echo "
					<table border='0' width='80%' align='center'>
						<tr>
					";
					$desactivarperfilText1='desactivar perfil';
					$activarperfilText1='desactivar perfil';
					$desactivarperfilTextNota1="Nom�s si no hi ha cap producte actiu d'aquest perfil de productor en la <b>llista global</b> i en la ruta actual";
					$GET_iPed="&iPed=".$mPerfil['id'];
					if($mPars['selLlistaId']!=0)
					{
						$desactivarperfilText1='desactivar perfil a la llista local';
						$activarperfilText1='activar perfil a la llista local';
						$desactivarperfilTextNota1="Nom�s si no hi ha cap producte actiu d'aquest perfil de productor en la <b>llista local</b> i en la ruta actual";
						$GET_iPed='';
						if(substr_count(' '.$mPropietatsGrup['idsPerfilsActiusLocal'],','.$mPerfil['id'].',')>0)
						{
							echo "
							<td  width='20%' align='center'>
							<p class='pGrupActiu'>PERFIL ACTIU</p>	
							</td>

							<td width='20%' align='center'>
							&nbsp;&nbsp;<input type='button' onClick=\"javascript:enviarFparsPerfilEditar('nouPerfilProductor.php?sR=".$mPars['selRutaSufix'].$GET_iPed."&op=desactivarPerfil','_self');\" value='".$desactivarperfilText1."'>&nbsp;&nbsp;
							</td>

							<td width='20%' align='center'>
							<p class='p_micro2'>* ".$desactivarperfilTextNota1."</p>
							</td>
							";
						}
						else
						{
							echo "
							<td width='40%'align='center'>
							<p class='pGrupInactiu'>PERFIL INACTIU</p>	
							</td>

							<td width='60%' align='center'>
							<input type='button' onClick=\"javascript:enviarFparsPerfilEditar('nouPerfilProductor.php?sR=".$mPars['selRutaSufix'].$GET_iPed."&op=activarPerfil','_self');\" value='".$activarperfilText1."'>
							</td>
							";
						}
					}
					else
					{
						if($mPerfil['estat'])
						{
							echo "
							<td width='20%' align='center'>
							<p class='pGrupActiu'>PERFIL ACTIU</p>	
							</td>

							<td width='20%'  align='center'>
							&nbsp;&nbsp;<input type='button' onClick=\"javascript:enviarFparsPerfilEditar('nouPerfilProductor.php?sR=".$mPars['selRutaSufix'].$GET_iPed."&op=desactivarPerfil','_self');\" value='".$desactivarperfilText1."'>&nbsp;&nbsp;
							</td>

							<td width='60%'>
							<p class='p_micro2'>* ".$desactivarperfilTextNota1."</p>
							</td>
							";
						}
						else if(!$mPerfil['estat'])
						{
							echo "
							<td width='40%' align='center'>
							<p class='pGrupInactiu'>PERFIL INACTIU</p>	
							</td>

							<td width='50%' align='center'>
							<input type='button' onClick=\"javascript:enviarFparsPerfilEditar('nouPerfilProductor.php?sR=".$mPars['selRutaSufix'].$GET_iPed."&op=activarPerfil','_self');\" value='activar perfil'>
							</td>
							";
						}
					}
					
					
					echo "
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			<br>
					";
				}
				echo "
			
			
			<form id='f_nouPerfil' name='f_nouPerfil' method='POST' action='nouPerfilProductor.php' target='_self' >
			<input type='hidden' name='i_opcio' value='guardarPerfil'>
			<table  align='center'  style='width:70%'>
				<tr>
					<td style='width:100%' align='left'>
					<table align='center'  style='width:100%'>
						<tr>
							<th align='left' valign='top'>
							<p>Projecte </p>
							</th>
							<td align='left' valign='top'>
							<p><input type='text' ".$disabledProjecte." id='i_projecte' name='i_projecte' size='30' value=\"".(urldecode($mPerfil['projecte']))."\">&nbsp;[CAC]</p>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Compte Ecos </p>
							</th>
							<td align='left' valign='top'>
							<p class='p_micro'>
							<input type='text' ".$disabledProjecte." id='i_compteEcos' name='i_compteEcos' size='8' value=\"".(@urldecode($mPerfil['compte_ecos']))."\">
							<br>
							* S'utilitza per al balan� d'intercanvi amb la productora
							<br> pels mesos que  no fa comanda a la CAB; en els altres<br>
							casos s'utilitza el nombre de compte de la seva comanda a la CAB.
							</p>
							</td>
						</tr>

						<tr>
							<td align='left' valign='top'>
							<br>
							<p><b>Grup Vinculat</b></p>
							</td>
							<td align='left' valign='top'>
							<br>
							<p class='p_micro'>(Balan� comptable CAB)<br>
							<select ".$readonlyGrupVinculat." id='sel_grupVinculat' name='sel_grupVinculat'>
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mGrup_)=each($mGrupsRef))
					{
						if($id!=0 && $id!='')
						{
							if($id==$mPerfil['grup_vinculat'])
							{
								$selected1='selected';
								$selected2='';
							}
							else
							{
								$selected1='';
							}
						}
						echo "
							<option ".$selected1." value='".$id."'>".(urldecode($mGrup_['nom']))."</option>
						";
					}
					reset($mGrupsRef);
					echo "
							<option ".$selected2." value=''></option>
							</select>&nbsp[CAB]</p>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Acord de compra</p>
							</th>
							<td align='left' valign='top'>
							<p><input type='text' ".$disabledAcordCAC." id='i_acord1' name='i_acord1' size='4' value=\"".(number_format($mPerfil['acord1'],2))."\"> % [CAB]</p>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Municipi</p>
							</th>
							<td align='left' valign='top'>
							<select ".$readonlyMunicipi." id='sel_municipi' name='sel_municipi'>
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mMunicipi)=each($mMunicipis))
					{
						if($id==$mPerfil['municipi_id'])
						{
							$selected1='selected';
							$selected2='';
						}
						else
						{
							$selected1='';
						}
						echo "
							<option ".$selected1." value='".$id."'>".(urldecode($mMunicipi['municipi']))."</option>
						";
					}
					reset($mMunicipis);
					echo "
							<option ".$selected2." value=''></option>
							</select>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Adre�a:
							</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' ".$readonlyAdressa." id='i_adre�a' name='i_adre�a' size='30' value=\"".(urldecode($mPerfil['adressa']))."\">
							</td>
						</tr>

						<tr style='visibility:hidden; z-index:0; position:absolute; top:0px; '>
							<th align='left' valign='top'>
							<p>Notes</p>
							</th>
							<td align='left' valign='top'>
							<textArea id='ta_notes'  name='ta_notes' cols='50' rows='7'>".(urldecode($mPerfil['notes']))."</textArea>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>web</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' ".$disabledWeb." id='i_web' name='i_web' size='50' value=\"".$mPerfil['web']."\">
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Ram</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' ".$disabledRam." id='i_ram' name='i_ram' size='50' value=\"".(urldecode($mPerfil['ram']))."\">
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<br>
							<p>Avals</p>
							</th>
							<td align='left' valign='top'>
							<br>
 							<p class='p_micro'>(introduir grups avaladors separats per comes)<br>
							<input type='text' ".$disabledAvals." id='i_avals' name='i_avals' size='50' value=\"".(urldecode($mPerfil['avals']))."\">
							</p>
 							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Descripci�</p>
					";
					html_notesHTML();
					echo "
							</th>
					
							<td align='left' valign='top'>
							<textArea ".$disabledDescripcio." id='ta_descripcio' name='ta_descripcio' cols='50' rows='20'>".(urldecode($mPerfil['descripcio']))."</textArea>
							</td>
							
						</tr>

						<tr>
							<th align='left' valign='top'>
							<br>
							<p>Zona</p>
							<br>
							</th>
							<td align='left' valign='top'>
							<br>
							<select ".$readonlyZona." id='sel_zona' name='sel_zona'>
					";
					
					$selected2='selected';
					
					
					while(list($sz_,$mZones_)=each($mGrupsZones))
					{
						while(list($key,$zona_)=each($mZones_))
						{
							if($zona_==$mPerfil['zona']){$selected='selected';$selected2='';}else{$selected='';}
							
							echo "
							<option ".$selected." value='".$zona_."'>".$zona_." (".$sz_.")</option>
							";
						}
						reset($mZones_);
					}
					reset($mGrupsZones);
					echo "
							<option ".$selected2." value=''></option>
							</select>
							<br>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							<p>- Dades responsable del perfil -</p>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Usuari</p>
							</th>
							<td align='left' valign='top'>
							<select ".$readonlyResponsable." id='sel_usuariId' name='sel_usuariId'>
					";
					$selected1='';
					$selected2='selected';
					$mResponsablePerfil=array();
					if($readonlyResponsable=='')
					{
						while(list($id,$mUsuari_)=each($mUsuarisRef))
						{
							if($id==$mPerfil['usuari_id'])
							{
								$selected1='selected';
								$selected2='';
								$mResponsablePerfil=$mUsuari_;
							}
							else
							{
								$selected1='';
							}
							echo "
							<option ".$selected1." value='".$id."'>".(urldecode($mUsuarisRef[$id]['usuari']))." (".$mUsuarisRef[$id]['email'].")</option>
							";
						}
						reset($mUsuarisRef);
						echo "
							<option ".$selected2." value=''></option>
						";
					}
					else
					{
						$mResponsablePerfil=db_getUsuari($mPerfil['usuari_id'],$db);
						echo "
							<option selected value='".$id."'>".(urldecode($mResponsablePerfil['usuari']))." (".$mResponsablePerfil['email'].")</option>
						";
					}
					echo "
							</select>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Email</p>
							</th>
							<td align='left' valign='top'>
							<input class='i_readonly3' type='text' id='i_email' READONLY name='i_email' size='30' value='".@$mResponsablePerfil['email']."'>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Mobil</p>
							</th>
							<td align='left' valign='top'>
							<input class='i_readonly3' type='text' id='i_mobil' READONLY name='i_mobil' size='30' value='".@$mResponsablePerfil['mobil']."'>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>&nbsp;</p>
							</th>
							<td align='left' valign='top'>
							</td>
						</tr>
				";
				if
				( 
					$mPars['nivell']=='sadmin'
					||
					$mPars['usuari_id']==$mPerfil['usuari_id']
				)
				{
					echo "
						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							<input type='button' onClick=\"javascript: checkFormPerfil();\" value='guardar Perfil'>
							</td>
						</tr>
					";
				}
				echo "
					</table>
					<input type='hidden'  name='i_pars' value='".$parsChain."'>
					</form>
					</td>
				</tr>
			</table>
			
					</td>
				";
				if
				(
					$mUsuari['id']==$mPerfil['usuari_id']
					||	
					$mPars['nivell']=='sadmin'
					||
					$mPars['nivell']=='admin'
				)
				{
					echo "
					<td width='40%' align='center' valign='top'>
					<br>
					";
					html_segellEcocicPerfil('form','nouPerfilProductor.php');
					echo "
					</td>
					";
				}
				echo "
				</tr>
			</table>
			</td>
		</tr>
	</table>
	";
}

	if
	(
		isset($mPerfil) && @$mPerfil['id']!=''
		&&
		(
			$mPars['nivell']=='sadmin'
			||
			$mPars['nivell']=='admin'
		)
	)
	{
		echo "
		<td width='40%' align='center' valign='top'>
		";
			html_segellEcocicPerfil('form','nouPerfilProductor.php');
		echo "
		</td>
		";
	}
	else
	{
		echo "<div id='t_segellEcocicPerfil'></div>";
	}
	
	

$parsChain=makeParsChain($mPars);

echo "

<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='nouPerfilProductor.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</div>
	
</body>
</html>
";
?>
	




		