<?php
function html_segellEcocicPerfil($opcio,$desti)
{
	global 	$mBlocsPropietatsSegells,
			$mCategoria0perfil,
			$mPars,
			$mPerfil,
			$parsChain,
			$mPropietatsSegellPerfil,
			$mEstatsSegellPerfil;
		
	$mPropietatsSegellPerfil0=array();
	
	while(list($bloc,$mBloc)=each($mBlocsPropietatsSegells))
	{
		while(list($key,$mVal)=each($mPropietatsSegellPerfil))
		{
			if($mVal['bloc']==$bloc)
			{
				$mPropietatsSegellPerfil0[$key]=$mPropietatsSegellPerfil[$key];
			}
		}
		reset($mPropietatsSegellPerfil);
	}
	reset($mBlocsPropietatsSegells);

	$mPropietatsSegellPerfil=$mPropietatsSegellPerfil0;

	if($opcio=='form')
	{
		$form1="<form id='f_guardarSegellPerfil' name='f_guardarSegellPerfil' method='post' action='".$desti."' target='_self'>";
				
		$disabled2='';
		$style="style='z-index:0; top:0px; position:absolute; visibility:hidden;'";
		$style2="";
		
	}
	else
	{
		$form1='';
		$disabled2='DISABLED';
		$style="";
		$style2="style='visibility:hidden;'";
		
	}
	echo "
			<table id='t_segellEcocicPerfil' border='1' borderColor='green' width='50%' align='center' bgcolor='white'   ".$style.">
				<tr>
					<td align='center' width='100%' >
					<br>
						<center><p style='font-size:11px;'><b>Segell ECOCIC del perfil</b></p></center>
						<p style='font-size:30px; color:LightBlue;'><i><b>".(urldecode($mPerfil['projecte']))."</b></i></p>
						
	";
	echo $form1;
	echo "
						<table border='0' width='90%' align='center'>
							<tr>
								<td align='right'>
								<input type='button' value='X' onClick=\"javascript: ocultarFormAccio('t_segellEcocicPerfil');\" ".$style2.">
								</td>
							</tr>
							<tr>
								<td width='100%' >
								<p style='font-size:10px;'>
								<b>categories</b>: <br>
							";
							while(list($key,$val)=each($mCategoria0perfil))
							{
								echo "
								<br>- ".$key."
								";
							}
							echo " 
								</p>
								<table width='100%'>
									<tr>
										<th valign='top'>
										<p>id</p>
										</th>
										<th valign='top'>
										<p>Bloc</p>
										</th>
										<th valign='top'>
										<p>Segell</p>
										</th>
										<th valign='top'>
										<p>Categoria</p>
										</th>
										<th valign='top'>
										<p>Estat</p>
										</th>
										<th valign='top'>
										<p>valor ECOCIC</p>
										</th>
										<th valign='top'>
										<p>edici�</p>
										</th>
									</tr>
								";
								$sumaValor=0;
								$sumaValorT=0;
								$contDisabled=0;
								while(list($id,$mPropietatSegellPerfil)=@each($mPropietatsSegellPerfil))
								{
									if
									(
										array_key_exists($mPropietatSegellPerfil['categoria0'],$mCategoria0perfil)
										||
										$mPropietatSegellPerfil['categoria0']=='tots'
									)
									{
										if($mPropietatSegellPerfil['nivell']=='admin')
										{
											if($mPars['nivell']=='sadmin' ||$mPars['nivell']=='admin' || $mPars['nivell']=='coord')
											{
												$disabled='';
											}
											else if($mPars['usuari_id']==$mPerfil['usuari_id'])
											{
												$disabled='DISABLED';
											}
										}
										else if($mPropietatSegellPerfil['nivell']=='RP')
										{
											if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
											{
												if($mPars['usuari_id']==$mPerfil['usuari_id'])
												{
													$disabled='';
												}
												else
												{
													$disabled='DISABLED';
												}
											}
											else if($mPars['usuari_id']==$mPerfil['usuari_id'])
											{
												$disabled='';
											}
										}
										if($disabled=='DISABLED' || $disabled2=='DISABLED')
										{
											$contDisabled++;
										}
									
										echo "
									<tr bgcolor='".@$mBlocsPropietatsSegells[$mPropietatSegellPerfil['bloc']]['color']."'>
										<td align='center' >
										<p style='font-size:10px;'>
										<b>".$id."</b>
										<br>
										[".(implode(',',$mPropietatSegellPerfil['grup']))."]</p>
										</td>

										<td align='left' >
										<p style='font-size:10px;'>".$mPropietatSegellPerfil['bloc']."</p>
										</td>

										<td align='left' >
										<p style='font-size:10px;'>".$mPropietatSegellPerfil['segell']."</p>
										</td>
										
										<td align='center'>
										<p style='font-size:10px;'>".$mPropietatSegellPerfil['categoria0']."</p>
										</td>

										<td align='center'>
										<select ".$disabled." ".$disabled2." onChange=\"javascript: checkGrupSegellPerfilPropietat(this.value,".$id.");\" style='font-size:10px;' id='sel_segells_".$id."' name='sel_segells_".$id."'>
										";
										$selected1='';
										$selected2='selected';
										while(list($estatId,$estatText)=each($mEstatsSegellPerfil))
										{
											if
											(
												$mPropietatSegellPerfil['estat']==$estatId
											)
											{
												$selected1='selected';
												$selected2='';
												if($estatId==1){$sumaValor+=$mPropietatSegellPerfil['valor'];}
											}
											else
											{
												$selected1='';
											}
											echo "
										<option  ".$selected1." value='".$estatId."'>".$mEstatsSegellPerfil[$estatId]."</option>
											";
										}
										reset($mEstatsSegellPerfil);
										$sumaValorT+=$mPropietatSegellPerfil['valor'];
										echo "
										<option ".$selected2." value=''></option>
										</td>
										
										<td align='center'>
										<p style='font-size:10px;'>".$mPropietatSegellPerfil['valor']."</p>
										</td>

										<td align='center'>
										<p style='font-size:10px;'>".$mPropietatSegellPerfil['nivell']."</p>
										</td>
									</tr>
										";
									}
								}
								reset($mPropietatsSegellPerfil);
								if(count($mPropietatsSegellPerfil)>0 && $contDisabled<count($mPropietatsSegellPerfil))
								{$disabledSubmit='';}else{$disabledSubmit='DISABLED';}
								
								echo "
									<tr>
										<td align='left'>
										</th>
										<td align='left'>
										<p>Total Valor:</p>
										</th>
										<th align='center'>
										<p>".(number_format(($sumaValor*100/($sumaValorT)),2,'.',''))."&nbsp;%</p>
										</th>
										<td align='center'>
										<p>".$sumaValor.'/'.$sumaValorT." </p>
										</td>
										<td align='center'>
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
					<center>
					<p style='font-size:10px;'>
					* RP: Responsable del Perfil de Productora
					<br>
					* s'indiquen entre corxets, a sota l'identificador de la propietat, quines propietats son m�tuament excloents
					</p>
					</center>
	";
	if($opcio=='form')
	{
		echo "
						<input id='i_gSp' name='i_gSp' type='hidden' value='".$mPerfil['id']."'>
						<input id='i_pars' name='i_pars' type='hidden' value='".$parsChain."'>
						<br>
						<center><input type='submit' ".$disabledSubmit." value='guardar'></center>
						</form>";
				
	}
		echo "
						</td>
					</tr>
				</table>
		";
	
	return;
}


//------------------------------------------------------------------------------
function html_segellEcocicProducte($opcio,$desti)
{
	global  $mBlocsPropietatsSegells,
			$mPars,
			$mPropietatsSegellProducte,
			$mEstatsSegellProducte,
			$mPerfil,
			$mProducte,
			$parsChain;

	$mPropietatsSegellProducte0=array();
	
	while(list($bloc,$mBloc)=each($mBlocsPropietatsSegells))
	{
		while(list($key,$mVal)=each($mPropietatsSegellProducte))
		{
			if($mVal['bloc']==$bloc)
			{
				$mPropietatsSegellProducte0[$key]=$mPropietatsSegellProducte[$key];
			}
		}
		reset($mPropietatsSegellProducte);
	}
	reset($mBlocsPropietatsSegells);

	$mPropietatsSegellProducte=$mPropietatsSegellProducte0;
	if($opcio=='form')
	{
		$form1="<form id='f_guardarSegellPerfil' name='f_guardarSegellPerfil' method='post' action='".$desti."' target='_self'>";
		$style="style='z-index:0; top:0px; position:absolute; visibility:hidden;'";
		$style2="";
		$disabled2='';
	}
	else
	{
		$form1='';
		$disabled2='DISABLED';
		$style="";
		$style2="style='visibility:hidden;'";
	}
	echo "
			<table id='t_segellEcocicProducte' border='1' borderColor='green' width='50%' align='center' bgcolor='white'   ".$style.">
				<tr>
					<td align='left' width='100%' >
					<br>
					
			<center>
			<table>
				<tr>
					<td>
					<p style='font-size:11px;'><b>Segell ECOCIC del producte</b></p>					
					</td>
					
					<td valign='bottom'>
					".(html_ajuda1('html_gestioProductes.php',7))."
					</td>
				</tr>
			</table>
			</center>
	";					
	echo $form1;
	echo "
					<table border='0' width='60%' align='center'>
						<tr>
						<td align='right'>
						<input type='button' value='X' onClick=\"javascript: ocultarFormAccio('t_segellEcocicProducte');\" ".$style2.">
						</td>
						</tr>
						<tr>
							<td align='left' width='100%' >
							<p style='font-size:10px;'>
							<b>id:</b>".$mProducte['id']."
							<br>
							<b>categoria</b>: ".$mProducte['categoria0']." 
							<br>
							<b>producte:</b> ".(urldecode($mProducte['producte']))."
							</p>
							
							<table width='100%'>
								<tr>
									<th valign='top'>
									<p>id</p>
									</th>
									<th valign='top'>
									<p>Bloc</p>
									</th>
									<th valign='top'>
									<p>Segell</p>
									</th>
									<th valign='top'>
									<p>Categoria</p>
									</th>
									<th valign='top'>
									<p>Estat</p>
									</th>
									<th valign='top'>
									<p>valor ECOCIC</p>
									</th>
									<th valign='top'>
									<p>edici�</p>
									</th>
								</tr>
							";
							$sumaValor=0;
							$sumaValorT=0;
							$contDisabled=0;
							while(list($id,$mPropietatSegellProducte)=each($mPropietatsSegellProducte))
							{
								if
								(
									@$mPropietatSegellProducte['categoria0']==$mProducte['categoria0']
									||
									@$mPropietatSegellProducte['categoria0']=='tots'
								)
								{
									$disabled='DISABLED';
									if(@$mPropietatSegellProducte['nivell']=='admin')
									{
										if($mPars['nivell']=='sadmin' ||$mPars['nivell']=='admin' || $mPars['nivell']=='coord')
										{
											$disabled='';
										}
										else if($mPars['usuari_id']==$mPerfil['usuari_id'])
										{
											$disabled='DISABLED';
										}
									}
									else if(@$mPropietatSegellProducte['nivell']=='RP')
									{
										if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
										{
											$disabled='DISABLED';
										}
										if($mPars['usuari_id']==$mPerfil['usuari_id'])
										{
											$disabled='';
										}
									}
									if($disabled=='DISABLED' || $disabled2=='DISABLED')
									{
										$contDisabled++;
									}
									echo "
								<tr bgcolor='".$mBlocsPropietatsSegells[$mPropietatSegellProducte['bloc']]['color']."'>
									<td align='center' >
									<p style='font-size:10px;'>
									<b>".$id."</b>
									<br>
									[".(implode(',',$mPropietatSegellProducte['grup']))."]</p>
									</td>

									<td align='left'>
									<p style='font-size:10px;'>".@$mPropietatSegellProducte['bloc']."</p>
									</td>

									<td align='left'>
									<p style='font-size:10px;'>".@$mPropietatSegellProducte['segell']."</p>
									</td>
									
									<td align='center'>
									<p style='font-size:10px;'>".@$mPropietatSegellProducte['categoria0']."</p>
									</td>

									<td align='center'>
									<select ".$disabled." ".$disabled2." onChange=\"javascript: checkGrupSegellProductePropietat(this.value,".$id.");\" style='font-size:10px;' id='sel_segells_".$id."' name='sel_segells_".$id."'>
									";
									$selected1='';
									$selected2='selected';
									while(list($estatId,$estatText)=each($mEstatsSegellProducte))
									{
										if
										(
											$mPropietatSegellProducte['estat']==$estatId
										)
										{
											$selected1='selected';
											$selected2='';
											if($estatId==1){$sumaValor+=$mPropietatSegellProducte['valor'];}
										}
										else
										{
											$selected1='';
										}
										echo "
									<option  ".$selected1." value='".$estatId."'>".$mEstatsSegellProducte[$estatId]."</option>
										";
									}
									reset($mEstatsSegellProducte);
									if(isset($mPropietatSegellProducte['valor']))
									{
										$sumaValorT+=$mPropietatSegellProducte['valor'];
									}
									echo "
									<option ".$selected2." value=''></option>
									</td>
 
 									<td align='center'>
									<p style='font-size:10px;'>".@$mPropietatSegellProducte['valor']."</p>
									</td>

									<td align='center'>
									<p style='font-size:10px;'>".@$mPropietatSegellProducte['nivell']."</p>
									</td>
								</tr>
									";
								}
							}
							reset($mPropietatsSegellProducte);
							if(count($mPropietatsSegellProducte)>0 && $contDisabled<count($mPropietatsSegellProducte))
							{$disabledSubmit='';}else{$disabledSubmit='DISABLED';}
							echo "
								<tr>
									<td align='left'>
									</th>
									<td align='left'>
									</th>
									<td align='left'>
									<p>Total Valor:</p>
									</th>
									<th align='center'>
									<p>".(number_format(($sumaValor*100/$sumaValorT),2,'.',''))."&nbsp;%</p>
									</th>
									<td align='center'>
									<p>".$sumaValor.'/'.$sumaValorT." </p>
									</td>
									<td align='center'>
									</td>
								</tr>
							</table>
							<br>
					<center>
					<p style='font-size:10px;'>
					* RP: Responsable del Perfil de Productora
					<br>
					* s'indiquen entre corxets, a sota l'identificador de la propietat, quines propietats son m�tuament excloents
					</p>
					</center>
	";
	if($opcio=='form')
	{
		echo "
							<center><input type='submit' ".$disabledSubmit." value='guardar el segell'></center>
							<br>
							&nbsp;
							</td>
						</tr>
					</table>
					<br>
					<input id='i_gSp' name='i_gSp' type='hidden' value='".$mPerfil['id']."'>
					<input id='i_pars' name='i_pars' type='hidden' value='".$parsChain."'>
					</form>
		";
		$disabled2='';
	}
	echo "
					</td>
				</tr>
			</table>
	";
	
	return;
}
?>

		