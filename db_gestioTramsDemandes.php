<?php
//----------------------------------------------------------------
function f1($cadena)
{
	$m1=array('="',"='",'= "',"=' ",'=   "',"='  ",'=    "',"='    ");
	
	$cadena=str_replace($m1,'<br><br>',$cadena);
	
	return $cadena;
}

//------------------------------------------------------------------------------
function getCategoriesTrams($db)
{
	global $mPars;
	
	$mCategories=array();
	
	$result=mysql_query("select DISTINCT categoria0 from trams_".$mPars['selRutaSufix']."  order by categoria0 ASC",$db);
	//echo "<br>  2329 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		if($mRow['categoria0']!='')
		{
			$mCategories[$i]=$mRow['categoria0'];
			$i++;
		}
	}

	return $mCategories;
}

//------------------------------------------------------------------------------
function getSubCategoriesTrams($db)
{
	global $mPars;
	
	$mSubCategories=array();

	$result=mysql_query("select DISTINCT categoria0,categoria10 from trams_".$mPars['selRutaSufix']." order by categoria0,categoria10 ASC",$db);
	//echo "<br>  2263 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		if($mRow['categoria0']!='' && $mRow['categoria10']!='')
		{
			$mSubCategories[$i]['categoria0']=$mRow['categoria0'];
			$mSubCategories[$i]['categoria10']=$mRow['categoria10'];
			$i++;
		}
	}

	return $mSubCategories;
}

//------------------------------------------------------------------------------
function getSubCategoriesTrams2($db)
{
	global $mPars;
	
	$mSubCategories=array();

	$result=mysql_query("select DISTINCT(categoria0) from trams_".$mPars['selRutaSufix']."  order by categoria0 ASC",$db);
	//echo "<br>  2292 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=@mysql_fetch_array($result,MYSQL_NUM))
	{
		if($mRow[0]!='')
		{
			$mSubCategories['categoria0'][$i]=$mRow[0];
			$i++;
		}
	}
	$result=mysql_query("select DISTINCT(categoria10) from trams_".$mPars['selRutaSufix']."  order by categoria10 ASC",$db);
	//echo "<br>  2300 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=@mysql_fetch_array($result,MYSQL_NUM))
	{
		if($mRow[0]!='')
		{
			$mSubCategories['categoria10'][$i]=$mRow[0];
			$i++;
		}
	}

	return $mSubCategories;
}

//---------------------------------------------------------------
function db_getTrams($db)
{
	global $mPars;

	$orderBy='';
	$where_=" id!=0 "; //redundant
	$limit='';
	$mTrams=array();
	
	if($mPars['sortBy']!='')
	{
		$orderBy=" order by ".$mPars['sortBy']." ".$mPars['ascdesc'];
	}

	if($mPars['veureTramsActius']==1)
	{
		$where_.=" AND actiu=1 ";
	}

	//aplicacio del filtre
	if($mPars['vTipus']!='TOTS')
	{
		$where_.=" AND  tipus='".$mPars['vTipus']."' ";
	}

	if($mPars['vUsuariId']!='TOTS')
	{
		$where_.=" AND  usuari_id='".$mPars['vUsuariId']."' ";
	}
 
	if($mPars['etiqueta']==$mPars['etiqueta2']){$mPars['etiqueta']='TOTS';}

	if($mPars['etiqueta2']!="CAP"){$mPars['etiqueta']='TOTS';}

	if($mPars['vVehicle']!='TOTS')
	{
		$where_.=" AND  vehicle_id='".$mPars['vVehicle']."' ";
	}

	if($mPars['vCategoria']!='TOTS')
	{
		$where_.=" AND  categoria0='".$mPars['vCategoria']."' ";
		
	}

	if($mPars['vSubCategoria']!='TOTS')
	{
		$categoria0=substr($mPars['vSubCategoria'],0,strpos($mPars['vSubCategoria'],'-'));
		$categoria10=substr($mPars['vSubCategoria'],strpos($mPars['vSubCategoria'],'-')+1);

		$where_.=" AND  categoria0='".$categoria0."' AND categoria10='".$categoria10."' ";
	}
	
	if($mPars['etiqueta']!='TOTS')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta']."',tipus)!=0 ";
	}

	if($mPars['etiqueta2']!='CAP')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta2']."',tipus)=0 ";
	}
	
	$result=mysql_query("select COUNT(*) from trams_".$mPars['selRutaSufix']." where ".$where_." ".$orderBy,$db);
	$mRow=mysql_fetch_array($result,MYSQL_NUM);
	$mPars['numItemsF']=$mRow[0];
	$mPars['numPags']=CEIL($mPars['numItemsF']/$mPars['numItemsPag']);
	
	//echo "select * from trams_".$mPars['selRutaSufix']." where ".$where_." ".$orderBy." ".$limit;
	if(!$result=mysql_query("select * from trams_".$mPars['selRutaSufix']." where ".$where_." ".$orderBy." ".$limit,$db))
	{
		//echo "<br> 19 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
			{
				$mRow['visible']=1;
			}
			$mTrams[$i]=$mRow;
			$mTrams[$i]['quantitat_pes']=0;
			$mTrams[$i]['quantitat_volum']=0;
			$mTrams[$i]['quantitat_places']=0;
			$i++;
		}
	}

	return $mTrams;
}

//----------------------------------------------------------------
function db_guardarTram($mGP,$db)
{
	global $mPars;

	$registre='';

	$mysqlChain='';
	if(isset($mGP['categoria0'])){$mGP['categoria0']=urldecode($mGP['categoria0']);}
	if(isset($mGP['categoria10'])){$mGP['categoria10']=urldecode($mGP['categoria10']);}
	if(isset($mGP['tipus'])){$mGP['tipus']=urldecode($mGP['tipus']);}
	if(isset($mGP['municipis_ruta'])){$mGP['municipis_ruta']=urldecode($mGP['municipis_ruta']);}
//	if(isset($mGP['estoc_disponible'])){$mGP['estoc_disponible']=0;}
	$id=$mGP['id'];

	//obtenim dades tram actual0.
	if(!$result=mysql_query("select * from trams_".$mPars['selRutaSufix']."  where id='".$id."'",$db))
	{
		//echo "<br> 170 db_gestioTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
    }
	else
	{
		$mUnitatsContractades=getUnitatsContractadesTram($db);
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

		// mirar si esta caducada
		$mSortida=array();

		$date = new DateTime();
		$t1=$date->getTimestamp();
		$mSortida['Y']=date('Y',strtotime($mRow['sortida']));
		$mSortida['m']=date('m',strtotime($mRow['sortida']));
		$mSortida['d']=date('d',strtotime($mRow['sortida']));
		$mSortida['h']=date('H',strtotime($mRow['sortida']));
		$mSortida['i']=date('i',strtotime($mRow['sortida']));
		
		$instantSortida = new DateTime();
	
		$instantSortida->setDate($mSortida['Y'], $mSortida['m'], $mSortida['d'] );
		$instantSortida->setTime($mSortida['h'], $mSortida['i'], 0 );

		$t2=$instantSortida->getTimestamp();

		//----------------------------------------------------------------------
		//>>>>>>> de moment no s'aplica perqu� les ofertes/demandes caducades no es poden guardar
		// si ho esta, anular contractes
		if($t2-$t1<=0)//caducada
		{
			$mResult=db_anularContractes($mPars['selTramId'],$db);
			if($mResult['result'])
			{
				$mUnitatsContractades=getUnitatsContractadesTram($db);
			}
		} 	
		//----------------------------------------------------------------------
		
		$mGP['pes_disponible']=$mRow['pes_disponible'];
		$mGP['unitatsContractadesPes']=$mUnitatsContractades['pes'];
		//guardar estoc_previst i estoc disponible
		if($mRow['capacitat_pes']!=$mGP['capacitat_pes'] || $mGP['capacitat_pes']!=$mRow['pes_disponible']+$mUnitatsContractades['pes'])
		{
			$mGP['pes_disponible']=$mRow['pes_disponible']*1;
				
			if($mGP['capacitat_pes']*1<=$mUnitatsContractades['pes'])
			{
				$mGP['pes_disponible']=0;
				$mGP['capacitat_pes']=$mUnitatsContractades['pes'];
			}
			else
			{
				$mGP['pes_disponible']=$mGP['capacitat_pes']-$mUnitatsContractades['pes'];
			}
		}
		else
		{
			unset($mGP['capacitat_pes']);
			unset($mGP['pes_disponible']);
		}

		$mGP['volum_disponible']=$mRow['volum_disponible'];
		$mGP['unitatsContractadesVolum']=$mUnitatsContractades['volum'];
		//guardar estoc_previst i estoc disponible
		if($mRow['capacitat_volum']!=$mGP['capacitat_volum'] || $mGP['capacitat_volum']!=$mRow['volum_disponible']+$mUnitatsContractades['volum'])
		{
			$mGP['volum_disponible']=$mRow['volum_disponible']*1;
				
			if($mGP['capacitat_volum']*1<=$mUnitatsContractades['volum'])
			{
				$mGP['volum_disponible']=0;
				$mGP['capacitat_volum']=$mUnitatsContractades['volum'];
			}
			else
			{
				$mGP['volum_disponible']=$mGP['capacitat_volum']-$mUnitatsContractades['volum'];
			}
		}
		else
		{
			unset($mGP['capacitat_volum']);
			unset($mGP['volum_disponible']);
		}
		
		$mGP['places_disponibles']=$mRow['places_disponibles'];
		$mGP['unitatsContractadesPlaces']=$mUnitatsContractades['places'];
		//guardar estoc_previst i estoc disponible
		if($mRow['capacitat_places']!=$mGP['capacitat_places'] || $mGP['capacitat_places']!=$mRow['places_disponibles']+$mUnitatsContractades['places'])
		{
			$mGP['places_disponibles']=$mRow['places_disponibles']*1;
				
			if($mGP['capacitat_places']*1<=$mUnitatsContractades['places'])
			{
				$mGP['places_disponible']=0;
				$mGP['capacitat_places']=$mUnitatsContractades['places'];
			}
			else
			{
				$mGP['places_disponibles']=$mGP['capacitat_places']-$mUnitatsContractades['places'];
			}
		}
		else
		{
			unset($mGP['capacitat_places']);
			unset($mGP['places_disponibles']);
		}
	}
	
	unset($mGP['id']);
	unset($mGP['propietats']);

	if(guardarValorsIpropietatsTram('historial',$mGP,$db))
	{
		return true;
	}
	
	return true;
}

//-------------------------------------------------------------
function db_anularContractes($tramId,$db)
{
	global $mPars,$mGrupsRef, $mUsuarisRef;
	$missatgeAlerta='';

	$mUnitatsContractades_['pes']=0;
	$mUnitatsContractades_['volum']=0;
	$mUnitatsContractades_['places']=0;

	$mUnitatsContractades['pes']=0;
	$mUnitatsContractades['volum']=0;
	$mUnitatsContractades['places']=0;

	$mResult=array();
	$mResult['result']=false;
	$mResult['missatgeAlerta']='';

	$mUnitatsContractades=getUnitatsContractadesTram($db);
	$mGP['unitatsContractadesPes']=$mUnitatsContractades['pes'];
	$mGP['unitatsContractadesVolum']=$mUnitatsContractades['volum'];
	$mGP['unitatsContractadesPlaces']=$mUnitatsContractades['places'];

	$mGP['grupsContractantsPes']='';
	$mGP['grupsContractantsVolum']='';
	$mGP['grupsContractantsPlaces']='';

	if(!$result=mysql_query("select id,grup_id,usuari_id,SUBSTRING(resum,LOCATE('tram_".$mPars['selTramId'].":',resum)) from contractes_".$mPars['selRutaSufix']." where LOCATE('tram_".$mPars['selTramId'].":',CONCAT(' ',resum))>0 order by id ASC",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mResult['missatgeAlerta']="<br>- error en llegir els trams contractats.";
		$mResult['result']=false;
		return $mResult;;
	}
	else
	{
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			$mContractesModificats[$i]=$mRow;
			$i++;
		}
	
		while(list($key,$mContracteModificat)=@each($mContractesModificats))
		{
			$unitatsContractades_=substr($mContracteModificat[3],strpos($mContracteModificat[3],':')+1);
			$unitatsContractades_=substr($unitatsContractades_,0,strpos($unitatsContractades_,';'));
			$mUrs=explode(',',$unitatsReservades_);
			$mUnitatsReservades['pes']+=$mUrs[0];
			$mUnitatsReservades['volum']+=$mUrs[1];
			$mUnitatsReservades['places']+=$mUrs[2];
			$cadena=substr($mComandaModificada[3],0,strpos($mComandaModificada[3],';')+1);
			$mComandesModificades[$key][4]=$unitatsReservades_;
			if(!$result=mysql_query("update contractes_".$mPars['selRutaSufix']." set resum=REPLACE(resum,'".$cadena."','') where id='".$mComandaModificada[0]."'",$db))
			{
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				$mResult['missatgeAlerta'].="<br>- error en llegir els trams contractats.";
				$mResult['result']=false;
				
				return $mResult;
			}
			else
			{
				$mResult['missatgeAlerta'].="<br>- ok, anul.lat contracte del tram id:".$mComandaModificada[0].",grup:".(urldecode($mComandaModificada[1])).",usuari:".$mComandaModificada[2]."(".$mUsuarisRef[$mComandaModificada[2]]['usuari']."), tram:".$tramId.", contractat: ".$mUnitatsContractades_['pes'].' kg, '.$mUnitatsContractades_['volum'].' '.$mTram['unitat_facturacio'].', '.$mUnitatsContractades_['places'].' places';
				$mGP['grupsContractants'].="<br>grup:".($mComandaModificada[1]).",usuari:".$mComandaModificada[2]."(".$mUsuarisRef[$mComandaModificada[2]]['usuari'].") contractat: ".$mUnitatsContractades_['pes'].' kg, '.$mUnitatsContractades_['volum'].' '.$mTram['unitat_facturacio'].', '.$mUnitatsContractades_['places'].' places';
			}
		}
		@reset($mComandesModificades);
	}
	//actualitzar el tram fins on no hagi fallat:

	$mTram=db_getTram($tramId,$db);

	if(!$result=mysql_query("update trams_".$mPars['selRutaSufix']." set capacitat_pes='0', capacitat_volum='0', capacitat_places='0', pes_disponible='0', volum_disponible='0', places_disponibles='0', actiu='0' where id='".$tramId."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mResult['missatgeAlerta'].="<br>- error actualitzar les capacitats del vehicle per aquest tram (pes, volum i places).";
		$mResult['result']=false;
		
		return $mResult;
	}
	
	$mResult['result']=true;

	//valors del tram a modificar
	$mGP['capacitat_pes']=0;
	$mGP['capacitat_volum']=0;
	$mGP['capacitat_places']=0;
	$mGP['pes_disponible']=0;
	$mGP['volum_disponible']=0;
	$mGP['places_disponibles']=0;
	$mGP['actiu']=0;
	
	if(!guardarValorsIpropietatsTram('anularContractes',$mGP,$db))
	{
		$mResult['missatgeAlerta'].="<br>- error en guardar a historial del tram.";
		$mResult['result']=false;
	}

	// si tot be, enviar mail a usuaris amb comanda modificada
	while(list($key,$mCotracteModificat)=@each($mContractesModificats))
	{
		if(!mail_anulacioContractes($mContracteModificat,$db))
		{
			$mResult['missatgeAlerta'].="<br>- error en enviar notificaci� a ".$mUsuarisRef[$mContracteModificat['2']]['email'].".";
			$mResult['result']=false;
		}
		else
		{
			$mResult['missatgeAlerta'].="<br>- ok, enviada notificaci� a ".$mUsuarisRef[$mContracteModificat['2']]['usuari']." (".$mUsuarisRef[$mComandaModificada['2']]['email'].", comanda a grup ".(urldecode($mContracteModificat['1'])).")";
			$mResult['result']=true;
		}
		
	}	
	@reset($mContractesModificats);
	
	return $mResult;
}

//----------------------------------------------------------------
function guardarValorsIpropietatsTram($tipusRegistre,$mGP,$db)
{
	global $mPars;
	$mysqlChain='';
	$registre='';
	$unitatsContractadesText='';
	$grupsContractantsText='';
	//obtenim dades tram actual.
	
	if(!$result=mysql_query("select * from trams_".$mPars['selRutaSufix']."  where id='".$mPars['selTramId']."'",$db))
	{
		echo "<br> 390 db_gestioTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
    }
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	
		if($tipusRegistre=='anularContractes')
		{
			$mUnitatsContractades['pes']=$mGP['unitatsContractadesPes'];
			$mUnitatsContractades['volum']=$mGP['unitatsContractadesVolum'];
			$mUnitatsContractades['places']=$mGP['unitatsContractadesPlaces'];
			$unitatsContractadesText='(unitats contractades:'.$mUnitatsContractades_['pes'].' kg, '.$mUnitatsContractades_['volum'].' '.$mTram['unitat_facturacio'].', '.$mUnitatsContractades_['places'].' places)';
			$grupsContractantsText=$mGP['grupsContractants'];
			unset($mGP['unitatsContractadesPes']);
			unset($mGP['unitatsContractadesVolum']);
			unset($mGP['unitatsContractadesPlaces']);
			unset($mGP['grupsContractants']);
		}

		while(list($key,$val)=each($mGP))
		{
			if(@isset($mRow[$key]))
			{
				if($key=='sortida' || $key=='arribada')
				{
					if(date('Y-m-d H:i:s',strtotime($mGP[$key]))!=date('Y-m-d H:i:s',strtotime($mRow[$key])))
					{
						$mysqlChain.=",".$key."='".$val."'";
						$registre='{p:'.$tipusRegistre.';us:'.$mPars['usuari_id'].';'.$key.':'.$mRow[$key].'->'.$mGP[$key].';dt:'.date('d/m/Y H:i:s').';}'.$registre;
					}
				}
				else if($mGP[$key]!=$mRow[$key])
				{
					$mysqlChain.=",".$key."='".$val."'";
					if($key=='descripcio'){$val='(...)';;$mRow[$key]='(...)';}
					$registre.='{p:'.$tipusRegistre.';us:'.$mPars['usuari_id'].';'.$key.':'.$mRow[$key].'->'.$mGP[$key].';dt:'.date('d/m/Y H:i:s').';}';
				}
			}
		}
		reset($mGP);
		
		if($tipusRegistre=='anularContractes')
		{
			$registre.='{p:'.$tipusRegistre.';us:'.$mPars['usuari_id'].';tram:'.$tramId.', contractat: '.$unitatsContractadesText.';dt:'.date('d/m/Y H:i:s').$grupsContractantsText.'}';
		}
		
		$mysqlChain=substr($mysqlChain,1);
		
		$coma='';
		
		if($mysqlChain!='' || $registre!='')
		{
			if($mysqlChain!='' && $registre!=''){$coma=',';}
			//echo "update trams_".$mPars['selRutaSufix']." set  ".$mysqlChain.$coma."propietats=CONCAT(propietats,'".$registre."') where id='".$mPars['selTramId']."'";
			if(!$result=mysql_query("update trams_".$mPars['selRutaSufix']." set  ".$mysqlChain.$coma."propietats=CONCAT('".$registre."',propietats) where id='".$mPars['selTramId']."'",$db))
			{
				//echo "<br> 457 db_gestioTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
				return false;
   			}
			else
			{
				//echo "<br> ok 457 db_gestioTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
				return true;
			}
		}
	}

	return false;
}


//----------------------------------------------------------------
function db_crearTram($mGP,$db)
{
	global $mPars,$mVehicle;

	$mysqlChain='';
	$mysqlChainCheck='';

	$mGP['categoria0']=urldecode($mGP['categoria0']);
	//$mGP['categoria10']=urldecode($mGP['categoria10']);
	$mGP['municipis_ruta']=urldecode($mGP['municipis_ruta']);
	$mGP['actiu']=0;
	

	$mCamps=array(
'id'=>'',
'codi'=>'',
'sortida'=>'',
'municipi_origen'=>'',
'arribada'=>'',
'municipi_desti'=>'',
'municipis_ruta'=>'',
'periodicitat'=>'',
'actiu'=>'0',
'vehicle_id'=>'',
'usuari_id'=>$mPars['usuari_id'],
'categoria0'=>'',
'categoria10'=>'',
'tipus'=>'DEMANDES',
'capacitat_pes'=>'0',
'pes_disponible'=>$mGP['capacitat_pes'],
'capacitat_volum'=>'0',
'volum_disponible'=>$mGP['capacitat_volum'],
'capacitat_places'=>'0',
'places_disponibles'=>$mGP['capacitat_places'],
'km' => '0',
'estat'=>'',
'descripcio'=>'',
'acords_explicits'=>'',
'preu_pes'=>'',
'preu_volum'=>'',
'preu_places'=>'',
'preu_combustible'=>'',
'pc_ms'=>'',
'propietats'=>''
);

	
	while(list($key,$val)=each($mGP))
	{
		if($key!='' && $val!='')
		{
			$mCamps[$key]=$val;
		}
	}
	reset($mGP);
		
	while(list($key,$val)=each($mCamps))
	{
		if($key!='')
		{
			$mysqlChain.=",'".$val."'";
			
			if(in_array($key,array('sortida','arribada')))
			{
				if($key=='sortida'  || $key=='arribada')
				{ 
					$mysqlChainCheck.=$key."='".(date('Y-m-d H:i:s',strtotime(urldecode($val))))."' AND ";
				}
				else
				{ 
					$mysqlChainCheck.=$key."='".$val."' AND ";
				}
			}
		}
	}	
	reset($mCamps);

	$mysqlChain=substr($mysqlChain,1);
	$mysqlChainCheck=substr($mysqlChainCheck,0,strlen($mysqlChainCheck)-5);
	
	//echo "insert into trams_".$mPars['selRutaSufix']." values(".$mysqlChain.")";
	
	//--------------------------------------------------------------------------
	// de moment no s'aplica, ja que es permet generar ofertes/demandes noves a partir de plantilles
	// velles, nom�s canvia l'id i es posen els contractes a zero
	
	//evitar reenviament:
	//echo "<br>select * from trams_".$mPars['selRutaSufix']." where ".$mysqlChainCheck;
	if(!$result=mysql_query("select * from trams_".$mPars['selRutaSufix']." where ".$mysqlChainCheck,$db))
	{
		//echo "<br> 535 db_gestioTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
    }
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		//if($mRow){return false;}
	}
	//--------------------------------------------------------------------------
	
	
	//echo "<br>insert into trams_".$mPars['selRutaSufix']." values(".$mysqlChain.")";
	if(!$result=mysql_query("insert into trams_".$mPars['selRutaSufix']." values(".$mysqlChain.")",$db))
	{
		//echo "<br> 543 db_gestioTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
    }
	else
	{
		return true;
	}
	return false;
}

//------------------------------------------------------------------------------
function db_repetirTram($mPeriodeDiaNum,$mPeriodeDiaSetmana,$periodeNumMesos,$selTramId,$db)
{
	global $mPars;
	
	$mCamps=array(
'id'=>'',
'codi'=>'',
'sortida'=>'',
'municipi_origen'=>'',
'arribada'=>'',
'municipi_desti'=>'',
'municipis_ruta'=>'',
'periodicitat'=>'',
'actiu'=>'',
'vehicle_id'=>'',
'usuari_id'=>'',
'categoria0'=>'',
'categoria10'=>'',
'tipus'=>'',
'capacitat_pes'=>'0',
'pes_disponible'=>'',
'capacitat_volum'=>'0',
'volum_disponible'=>'',
'capacitat_places'=>'0',
'places_disponibles'=>'',
'km' => '',
'estat'=>'',
'descripcio'=>'',
'acords_explicits'=>'',
'preu_pes'=>'',
'preu_volum'=>'',
'preu_places'=>'',
'preu_combustible'=>'',
'pc_ms'=>'',
'propietats'=>''
);
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=true;
	$mysqlChain='';

	$mTram=db_getTram($db);

				$mSortida['Y']=date('Y',strtotime($mTram['sortida']));
				$mSortida['m']=date('m',strtotime($mTram['sortida']));
				$mSortida['d']=date('d',strtotime($mTram['sortida']));
				$mSortida['h']=date('H',strtotime($mTram['sortida']));
				$mSortida['i']=date('i',strtotime($mTram['sortida']));

				$mArribada['Y']=date('Y',strtotime($mTram['arribada']));
				$mArribada['m']=date('m',strtotime($mTram['arribada']));
				$mArribada['d']=date('d',strtotime($mTram['arribada']));
				$mArribada['h']=date('H',strtotime($mTram['arribada']));
				$mArribada['i']=date('i',strtotime($mTram['arribada']));

				$t0=time();	
				$t1=mktime($mSortida['h'],$mSortida['i'],0,$mSortida['m'],$mSortida['d'],$mSortida['Y']);
				$t2=mktime($mArribada['h'],$mArribada['i'],0,$mArribada['m'],$mArribada['d'],$mArribada['Y']);
	$i=0;
	if($mPeriodeDiaNum[0]!='')
	{
		while($i<$periodeNumMesos)
		{
			while(list($key,$diaNum)=each($mPeriodeDiaNum))
			{
				$t3=mktime($mSortida['h'],$mSortida['i'],0,$mSortida['m']+$i,$diaNum,$mSortida['Y']);
				
				if($t3-$t0>0)
				{
					$mTram['sortida']=date('Y/m/d H:i:s',$t3);
					$mTram['arribada']=date('Y/m/d H:i:s',$t3+($t2-$t1));
		
					while(list($camp,$val)=each($mTram))
					{
						if($camp=='id'){$val='';}
						else if($camp=='actiu'){$val='0';}
						else if($camp=='pes_disponible'){$val=$mTram['capacitat_pes'];}
						else if($camp=='volum_disponible'){$val=$mTram['capacitat_volum'];}
						else if($camp=='places_disponibles'){$val=$mTram['capacitat_places'];}
					
						$mysqlChain.="'".$val."',";
					}
					reset($mTram);
				
					$mysqlChain=(substr($mysqlChain,0,strlen($mysqlChain)-1));

						//echo "<br>insert into trams_".$mPars['selRutaSufix']." values(".$mysqlChain.")";
						if(!$result=mysql_query("insert into trams_".$mPars['selRutaSufix']." values(".$mysqlChain.")",$db))
						{
							//echo "<br> 796 db_gestioTramsOfertes.php ".mysql_errno() . ": " . mysql_error(). "\n";
							//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
							$mMissatgeAlerta['result']=false;
							$mMissatgeAlerta['missatge']="Atenci�: error en generar la demanda de transport. No s'ha pogut insertar la nova demanda";
									
							return $mMissatgeAlerta;
				    	}

					$mysqlChain='';
					unset($mRow);
				}
			}
			reset($mPeriodeDiaNum);
			$i++;
		}
	}
	else if($mPeriodeDiaSetmana[0]!='')
	{
		$i=0;
		while($i<$periodeNumMesos)
		{
			while(list($key,$diaSetmana)=each($mPeriodeDiaSetmana))
			{
				//cercar quins 'dimarts' queden d'aquest mes i guardar el dia del mes que correspon
				$t3=mktime($mSortida['h'],$mSortida['i'],0,$mSortida['m']+$i,$mSortida['d'],$mSortida['Y']);
				$t3_=$t3;
				$d=0;
				while(date('m',$t3)*1==(date('m',$t3_)*1)*1)
				{
					if(date('N',$t3)*1==$diaSetmana && $t3-$t0>0)
					{
						$mTram['sortida']=date('Y/m/d H:i:s',$t3);
						$mTram['arribada']=date('Y/m/d H:i:s',$t3+($t2-$t1));

						while(list($camp,$val)=each($mTram))
						{
							if($camp=='id'){$val='';}
							else if($camp=='actiu'){$val='0';}
							else if($camp=='pes_disponible'){$val=$mTram['capacitat_pes'];}
							else if($camp=='volum_disponible'){$val=$mTram['capacitat_volum'];}
							else if($camp=='places_disponibles'){$val=$mTram['capacitat_places'];}
					
							$mysqlChain.="'".$val."',";
						}
						reset($mTram);
				
						$mysqlChain=(substr($mysqlChain,0,strlen($mysqlChain)-1));
						
							//echo "<br>insert into trams_".$mPars['selRutaSufix']." values(".$mysqlChain.")";
							if(!$result=mysql_query("insert into trams_".$mPars['selRutaSufix']." values(".$mysqlChain.")",$db))
							{
								//echo "<br> 874 db_gestioTramsOfertes.php ".mysql_errno() . ": " . mysql_error(). "\n";
								//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');

								$mMissatgeAlerta['result']=false;
								$mMissatgeAlerta['missatge']="Atenci�: error en generar l'oferta de transport. No s'ha pogut insertar la nova oferta";
									
								return $mMissatgeAlerta;
						    }
							
						$mysqlChain='';
						unset($mRow);
					}
					$d++;
					$t3=mktime($mSortida['h'],$mSortida['i'],0,$mSortida['m']+$i,$mSortida['d']+$d,$mSortida['Y']);
				}
			}
			reset($mPeriodeDiaSetmana);
			$i++;
		}
	}


	return $mMissatgeAlerta;
}

//----------------------------------------------------------------
function db_eliminarTram($eliminarId,$db)
{
	global $mPars;
	
	if(!$result=mysql_query("select id from trams_".$mPars['selRutaSufix']." where id='".$eliminarId."' AND capacitat_pes=pes_disponible AND capacitat_volum=volum_disponible AND capacitat_places=places_disponibles ",$db))
	{
		//echo "<br> 116 db_gestioTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		if($mRow)
		{
			if($mRow['id']==$eliminarId)
			{
				if(!$result=mysql_query("delete from trams_".$mPars['selRutaSufix']." where id='".$eliminarId."'",$db))
				{
					//echo "<br> 432 db_gestiotrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		
					return false;
				}
				else{return true;}
			}
		}
	}

	return false;
}
//----------------------------------------------------------------
function getUnitatsContractadesTram($db)
{
	global $mPars;
	
	$mUnitatsContractadesTrams=array();
	$mUnitatsContractadesTrams['pes']=0;
	$mUnitatsContractadesTrams['volum']=0;
	$mUnitatsContractadesTrams['places']=0;
	$mUnitatsContractadesTrams['total']=0;
	
	$result=mysql_query("select SUBSTRING(resum,LOCATE('tram_".$mPars['selTramId'].":',resum)) from contractes_".$mPars['selRutaSufix']." where LOCATE('tram_".$mPars['selTramId'].":',CONCAT(' ',resum))>0 order by id ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";

	while($mRow=mysql_fetch_array($result,MYSQL_NUM))
	{
		
		$quantitat=substr($mRow[0],strpos($mRow[0],':')+1);
		$quantitat=substr($quantitat,0,strpos($quantitat,';'));
		$mUnitatsContractades_=explode('|',$quantitat);
		if($mUnitatsContractades_[0]*1>0)
		{
			$mUnitatsContractadesTrams['pes']+=$mUnitatsContractades_[0];
		}
		if($mUnitatsContractades_[1]*1>0)
		{
			$mUnitatsContractadesTrams['volum']+=$mUnitatsContractades_[1];
		}
		if($mUnitatsContractades_[2]*1>0)
		{
			$mUnitatsContractadesTrams['places']+=$mUnitatsContractades_[2];
		}
	}

	$mUnitatsContractadesTrams['total']=$mUnitatsContractadesTrams['pes']+$mUnitatsContractadesTrams['volum']+$mUnitatsContractadesTrams['places'];
	
	return $mUnitatsContractadesTrams;
}



?>

		