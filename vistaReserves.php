<?php
include "config.php";
include "db.php";
include "eines.php";
include "db_gestioProductes.php";
include "db_gestioMagatzems.php";
include "html_vistaReserves.php";
include "db_vistaReserves.php";
include "db_mail.php";
	require_once('phpmailer/class.phpmailer.php');
	include("phpmailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mComanda=array();
$mCr=array();
$form='';
$mMissatgeAlerta=array();
$mMissatgeAlerta['missatge1']='';
$mMissatgeAlerta['missatge11']='';
$mMissatgeAlerta['result1']=false;
$mMissatgeAlerta['result11']=false;
$jaEntregatTkg=0;
$jaEntregatTuts=0;

$mReservesCsv=array();

//------------------------------------------------------------------------------

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);

$ruta_=@$_GET['sR'];
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	if(!(isset($mPars['selRutaSufix'])))
	{
		$mPars['selRutaSufix']=$ruta;
	}
}

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
getConfig($db); //inicialitza variables anteriors;
$mPars['vRutaIncd']=$mPars['selRutaSufix'];

$mRutesSufixes=getRutesSufixes($db);

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='productor';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	//$mPars['excloureProductesJaEntregats']='1';
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	//$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';

	$mPars['fPagamentUms']=0.00;
	$mPars['fPagamentEcos']=0.00;
	$mPars['fPagamentEb']=0.00;
	$mPars['fPagamentEu']=0.00;
	$mPars['fPagamentEuMetalic']=0;
	$mPars['fPagamentEuTransf']=0;
	$mPars['fPagamentEcosIntegralCES']=0;
	

	$mPars['fPagamentEcosPerc']=0.00;
	$mPars['fPagamentEbPerc']=0.00;
	$mPars['fPagamentEuPerc']=0.00;

	$mPars['vRutaIncd']=$mPars['selRutaSufix'];
	
	$mPars['albaraVistaImpressio']=0;
	
if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}

$mReservesComandes=array();

$mReservesComandesRecepcionsConfirmades=array();

$zona_=@$_POST['i_zona'];
if(isset($zona_)){$zona=$zona_;}else{$zona='';}

$pE_=@$_GET['pE']; //punt entrega, resum especific nom�s per grups (link desde html.php
if(isset($pE_)){$pE=$pE_;$mPars['pE']=$pE_;}else{$pE='';$mPars['pE']=$pE_;}

$rc_=@$_GET['rc']; //mostra nom�s productes de recepcions confirmades
if(isset($rc_)){	$rc=$rc_;	$mPars['rc']=$rc;}else{	$rc='1';	$mPars['rc']=$rc;}

$elp_=@$_GET['elp']; //exclou productes amb estoc SZ local positiu suficient
if(isset($elp_)){	$elp=$elp_;	$mPars['elp']=$elp;}else{	$elp='1';	$mPars['elp']=$elp;}

	if($mPars['grup_id']=='0'){$mPars['elp']=-1;}

$sz_=@$_GET['sz']; //mostra nom�s productes de recepcions confirmades
if(isset($sz_)){	$sz=$sz_;	$mPars['sz']=$sz;}else{	$sz='';	$mPars['sz']=$sz;}

$mRebostsRef=db_getRebostsRef($db);
$mPuntsEntrega=db_getPuntsEntrega($db,'ref');
$puntEntregaId=db_getPuntEntrega($mPars['grup_id'],$db);
$mRutesSufixes=getRutesSufixes($db);
$mUsuari=db_getUsuari($mPars['usuari_id'],$db);
$mUsuarisRef=db_getUsuarisRef($db);
$mRecepcions=getRecepcions($db);
$mPerfilsRef=db_getPerfilsRef($db);
$mProductors=db_getProductors($db);

$mProductes=db_getProductes2($db);
$mGrupsZones=getGrupsZones($mParametres['agrupamentZones']['valor']);

//inventaris locals de cada magatzem
$mMagatzems=db_getMagatzems($db);
$mInventarisRef=array();
while(list($key,$mMagatzem)=each($mMagatzems))
{
	$mInventarisRef[$mMagatzem['id']]=db_getInventari($mMagatzem['id'],$db);
}
reset($mMagatzems);


$cadenaRecepcionsAConfirmar=@$_POST['i_cadenaRecepcionsAConfirmar']; //grup_idS  de recepcions a confirmar
if(isset($cadenaRecepcionsAConfirmar) && $cadenaRecepcionsAConfirmar!='')
{
	$mMissatgeAlerta=confirmarRecepcions($cadenaRecepcionsAConfirmar,$mPars['pE'],$db);
	$mRebostsRef=db_getRebostsRef($db);
}
	

if($pE!='')
{
	db_getReservesPuntEntrega($pE,$db);
}
else
{
	db_getReservesZona($zona,$db);
}

$superZona=getSuperZona($zona);
echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>".$zona." - Vista Reserves per Zona</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_vistaReserves.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
ruta=".$mPars['selRutaSufix'].";
</SCRIPT>
</head>
<body >
	<table align='center' style='width:100%;'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:16px;'><b>".$mContinguts['index']['titol0']."  - </b> ".$mContinguts['index']['titol1']."</b></p>
			<p style='font-size:13px;'>".$mContinguts['form']['titol']."</p>
			</td>
		</tr>
	</table>
	<table width=100%'>
		<tr>
			<td id='td_missatgeAlerta' style=' width:100%;' align='center'  valign='bottom'>
			".$mMissatgeAlerta['missatge1']."
			</td>
		</tr>
		<tr>
	</table>
	
	
<table width='90%'>
	<tr>
		<td  width='90%' align='center'>
";
if($pE!='')
{
	html_formPuntEntrega();

}
else
{
	html_formZones();
}
echo "
		</td>
	</tr>
";

echo "
	<tr>
		<td  width='90%'>
	";
html_mostrarProductes($db);
html_mostrarNotes();
db_getReservesCSV($db);
$parsChain=makeParsChain($mPars);
echo "
		</td>
	</tr>
</table>
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_zona' name='i_zona' value='".$zona."'>
<input type='hidden' id='i_cadenaRecepcionsAConfirmar' name='i_cadenaRecepcionsAConfirmar' value=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>
";
	

?>

		