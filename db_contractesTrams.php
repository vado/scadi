<?php
//------------------------------------------------------------------------------
function getCategoriesTrams($db)
{
	global $mPars;
	
	$mCategories=array();
	
	$result=mysql_query("select DISTINCT categoria0 from trams_".$mPars['selRutaSufix']."  order by categoria0 ASC",$db);
	//echo "<br>  2329 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		if($mRow['categoria0']!='')
		{
			$mCategories[$i]=$mRow['categoria0'];
			$i++;
		}
	}

	return $mCategories;
}
//------------------------------------------------------------------------------
function getSubCategoriesTrams($db)
{
	global $mPars;
	
	$mSubCategories=array();

	$result=mysql_query("select DISTINCT categoria0,categoria10 from trams_".$mPars['selRutaSufix']." order by categoria0,categoria10 ASC",$db);
	//echo "<br>  2263 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		if($mRow['categoria0']!='' && $mRow['categoria10']!='')
		{
			$mSubCategories[$i]['categoria0']=$mRow['categoria0'];
			$mSubCategories[$i]['categoria10']=$mRow['categoria10'];
			$i++;
		}
	}

	return $mSubCategories;
}
//---------------------------------------------------------------
function db_getTrams($db)
{
	global $mPars;

	$orderBy='';
	$where_=" id!=0 "; //redundant
	$limit='';
	$mTrams=array();
	
	if($mPars['sortBy']!='')
	{
		$orderBy=" order by ".$mPars['sortBy']." ".$mPars['ascdesc'];
	}

	if($mPars['usuari_id']=='417')
	{
		if($mPars['veureTramsNoCaducats']==1) //usuari 'cac'
		{
			$where_.=" AND 
					(
						( 
							(
								categoria0='CAC' 
								OR 
								categoria0='CAC-personal' 
							)
						)
						OR 
						(
							(
								categoria0!='CAC' 
								AND 
								categoria0!='CAC-personal' 
							) 
							AND 
							UNIX_TIMESTAMP(sortida)>UNIX_TIMESTAMP('".date('Y-m-d H:i:s',mktime(date('H'),date('i'),0,date('m'),date('d'),date('Y')))."')
						) 
					)
					";
		}
	}
	else
	{
		if($mPars['veureTramsNoCaducats']==1) 
		{
			$where_.=" 
					AND 
					(
						( 
							(
								categoria0='CAC' 
								OR 
								categoria0='CAC-personal' 
							)
							AND
							(
								usuari_id='".$mPars['usuari_id']."' 
								OR
								usuari_id='417'
								OR
								UNIX_TIMESTAMP(sortida)>UNIX_TIMESTAMP('".date('Y-m-d H:i:s',mktime(date('H'),date('i'),0,date('m'),date('d'),date('Y')))."')
							)							
						)
						OR 
						(
							(
								categoria0!='CAC' 
								AND 
								categoria0!='CAC-personal' 
							) 
							AND 						
							UNIX_TIMESTAMP(sortida)>UNIX_TIMESTAMP('".date('Y-m-d H:i:s',mktime(date('H'),date('i'),0,date('m'),date('d'),date('Y')))."')
						)
					) 
					";
		}
	}
	
	
	if($mPars['veureTramsActius']==1)
	{
		$where_.=" AND actiu=1 ";
	}

	//aplicacio del filtre
	if($mPars['vTipus']!='TOTS')
	{
		$where_.=" AND  tipus='".$mPars['vTipus']."' ";
	}

	if($mPars['vUsuariId']!='TOTS')
	{
		$where_.=" AND  usuari_id='".$mPars['vUsuariId']."' ";
	}

	if($mPars['etiqueta']==$mPars['etiqueta2']){$mPars['etiqueta']='TOTS';}

	if($mPars['etiqueta2']!="CAP"){$mPars['etiqueta']='TOTS';}

	if($mPars['vVehicle']!='TOTS')
	{
		$where_.=" AND  vehicle_id='".$mPars['vVehicle']."' ";
	}

	if($mPars['vCategoria']!='TOTS')
	{
		$where_.=" AND  categoria0='".$mPars['vCategoria']."' ";
		
	}

	if($mPars['vSubCategoria']!='TOTS')
	{
		$categoria0=substr($mPars['vSubCategoria'],0,strpos($mPars['vSubCategoria'],'-'));
		$categoria10=substr($mPars['vSubCategoria'],strpos($mPars['vSubCategoria'],'-')+1);

		$where_.=" AND  categoria0='".$categoria0."' AND categoria10='".$categoria10."' ";
	}
	
	if($mPars['etiqueta']!='TOTS')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta']."',tipus)!=0 ";
	}

	if($mPars['etiqueta2']!='CAP')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta2']."',tipus)=0 ";
	}
	
	$result=mysql_query("select COUNT(*) from trams_".$mPars['selRutaSufix']." where ".$where_." ".$orderBy,$db);
	$mRow=mysql_fetch_array($result,MYSQL_NUM);
	$mPars['numItemsF']=$mRow[0];
	$mPars['numPags']=CEIL($mPars['numItemsF']/$mPars['numItemsPag']);
	
	//echo "<br>select * from trams_".$mPars['selRutaSufix']." where ".$where_." ".$orderBy." ".$limit;
	if(!$result=mysql_query("select * from trams_".$mPars['selRutaSufix']." where ".$where_." ".$orderBy." ".$limit,$db))
	{
		//echo "<br> 19 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
			{
				$mRow['visible']=1;
			}
			else
			{
				$mRow['visible']=0;
			}
			$mTrams[$i]=$mRow;
			$mTrams[$i]['quantitat_pes']=0;
			$mTrams[$i]['quantitat_volum']=0;
			$mTrams[$i]['quantitat_places']=0;
			$i++;
		}
	}

	return $mTrams;
}

//---------------------------------------------------------------
function db_getTramsUsuariRef($db)
{
	global $mPars;

	$mTramsUsuariRef=array();
	
	if(!$result=mysql_query("select * from trams_".$mPars['selRutaSufix']." where usuari_id='".$mPars['usuari_id']."'",$db))
	{
		//echo "<br> 19 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mTramsUsuariRef[$mRow['id']]=$mRow;
		}
	}

	return $mTramsUsuariRef;
}
//------------------------------------------------------------------------------
function db_getContractes($db)
{
	global $mPars;
	
	$mContractes=array();
	
	//echo "<br>select * from contractes_".$mPars['selRutaSufix']." where usuari_id='".$mPars['usuari_id']."'";
	$result=mysql_query("select * from contractes_".$mPars['selRutaSufix']." where usuari_id='".$mPars['usuari_id']."'",$db);

	if($result)
	{ 
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		$mPars['idContracte']=$mRow['id'];
		$mTramsContracte=explode(';',$mRow['resum']);
		
		$n=0;
		for($i=0;$i<count($mTramsContracte);$i++)
		{
			if($mTramsContracte[$i]!='')
			{
				$mIndexQuantitat=explode(':',$mTramsContracte[$i]);
				$index=str_replace('tram_','',$mIndexQuantitat[0]);
				if($index!='' && $index!=0)
				{
					$mContractes[$n]['id']=$mRow['id'];//index contracte
					$mQuantitat=explode('|',$mIndexQuantitat[1]);
					$mContractes[$n]['quantitat_pes']=$mQuantitat[0];
					$mContractes[$n]['quantitat_volum']=$mQuantitat[1];
					$mContractes[$n]['quantitat_places']=$mQuantitat[2];
					$mContractes[$n]['usuari_id']=$mRow['usuari_id'];//index contracte
					$mContractes[$n]['tipus']=$mQuantitat[4];
					$mContractes[$n]['estat']=$mQuantitat[5];
					$mContractes[$n]['index']=$index;//index tram
					if(isset($mQuantitat[3]))
					{
						$mContractes[$n]['vehicle_id']=$mQuantitat[3];
					}
					else
					{
						$mContractes[$n]['vehicle_id']='';
					}
					$n++;
				}
			}
		}
	}

	return $mContractes;
}

//------------------------------------------------------------------------------
function db_getContractesAltri($db)
{
	global $mPars,$mTramsUsuariRef;
	
	$mContractes=array();
	$mContractes['pendents']=array();
	$mContractes['confirmades']=array();
	$mTramsUsuariRefKeys=array_keys($mTramsUsuariRef);
	$cadenaTramsUsuari=implode(',',$mTramsUsuariRefKeys);
	
	if(count($mTramsUsuariRef)>0)
	{
		$where=' WHERE ';
		
		while(list($key,$val)=each($mTramsUsuariRef))
		{
			$where.=" LOCATE('tram_".$key.":',CONCAT(' ',resum))>0 OR ";
		}
		reset($mTramsUsuariRef);
		$where=substr($where,0,strlen($where)-3);

		//echo "select * from contractes_".$mPars['selRutaSufix']." ".$where;
		$result=mysql_query("select * from contractes_".$mPars['selRutaSufix']." ".$where,$db);
		//echo "<br> 10 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
		
		while ($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPars['idContracte']=$mRow['id'];
			$mTramsContracte=explode(';',$mRow['resum']);
			$n=0;
			for($i=0;$i<count($mTramsContracte);$i++)
			{
				if($mTramsContracte[$i]!='')
				{
					$mIndexQuantitat=explode(':',$mTramsContracte[$i]);
					$index=str_replace('tram_','',$mIndexQuantitat[0]);
					if($index!='' && $index!=0)
					{
						$mQuantitat=explode('|',$mIndexQuantitat[1]);
						$mContractes_['id']=$mRow['id'];
						$mContractes_['quantitat_pes']=$mQuantitat[0];
						$mContractes_['quantitat_volum']=$mQuantitat[1];
						$mContractes_['quantitat_places']=$mQuantitat[2];
						$mContractes_['usuari_id']=$mRow['usuari_id'];
						$mContractes_['tipus']=@$mQuantitat[4];
						$mContractes_['estat']=@$mQuantitat[5];
						if(isset($mQuantitat[3]))
						{
							$mContractes_['vehicle_id']=$mQuantitat[3];
						}
						else
						{
							$mContractes_['vehicle_id']='';
						}
						if($mContractes_['estat']=='pendent')
						{
							if(!isset($mContractes['pendents'])){$mContractes['pendents']=array();}
							if(!isset($mContractes['pendents'][$index])){$mContractes['pendents'][$index]=array();}
							array_push($mContractes['pendents'][$index],$mContractes_);
						}
						if($mContractes_['estat']=='acceptat')
						{
							if(!isset($mContractes['confirmades'])){$mContractes['confirmades']=array();}
							if(!isset($mContractes['confirmades'][$index])){$mContractes['confirmades'][$index]=array();}
							array_push($mContractes['confirmades'][$index],$mContractes_);
						}
						$n++;
					}
				}
			}
		}
	}
	return $mContractes;
}
	
	

//------------------------------------------------------------------------------
function db_getContractesCopiar($db)
{
	global $mPars;
	
	$result=@mysql_query("select * from contractes_".$mPars['selRutaSufix']." where usuari_id='".$mPars['usuari_id']."'",$db);
	//echo "<br> 139 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
	if($result)
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		$mPars['idContractes']=$mRow['id'];
			
		return $mRow;
	}
	

	return false;
}
//------------------------------------------------------------------------------

function db_guardarContractes($guardarContractes,$db)
{
	global $mParametres,$mPars,$mGuardarFormaPagament;
		$mMissatgeAlerta=array();
		$mMissatgeAlerta['result']=false;
		$mMissatgeAlerta['missatge']="";
		
		$mContractesAnterior=db_getContractes($db); //arriben tots els trams demanats amb alguna quantitat>0

		$mContractesAnteriorCopiar=db_getContractesCopiar($db);

		$mContractesCombinada=array();

		//$guardarContractes: nom�s arriben els trams de la seleccio visualitzada, siguin en quantitat=0 o  quantitat>0
		//combinar Contractes anterior amb la comanda enviada ($guardarContractes)
		$mTramsContractes=explode(';',$guardarContractes);
		
		$n=0;
		for($i=0;$i<count($mContractesAnterior);$i++)
		{
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['anterior']['quantitat_pes']=@$mContractesAnterior[$i]['quantitat_pes'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['anterior']['quantitat_volum']=@$mContractesAnterior[$i]['quantitat_volum'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['anterior']['quantitat_places']=@$mContractesAnterior[$i]['quantitat_places'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['anterior']['estat']=@$mContractesAnterior[$i]['estat'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['anterior']['vehicle_id']=@$mContractesAnterior[$i]['vehicle_id'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['anterior']['tipus']=@$mContractesAnterior[$i]['tipus'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['anterior']['tram_id']=@$mContractesAnterior[$i]['tram_id'];

			$mContractesCombinada[$mContractesAnterior[$i]['index']]['actual']['quantitat_pes']=@$mContractesAnterior[$i]['quantitat_pes'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['actual']['quantitat_volum']=@$mContractesAnterior[$i]['quantitat_volum'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['actual']['quantitat_places']=@$mContractesAnterior[$i]['quantitat_places'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['actual']['estat']=@$mContractesAnterior[$i]['estat'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['actual']['vehicle_id']=@$mContractesAnterior[$i]['vehicle_id'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['actual']['tipus']=@$mContractesAnterior[$i]['tipus'];
			$mContractesCombinada[$mContractesAnterior[$i]['index']]['actual']['tram_id']=@$mContractesAnterior[$i]['tram_id'];
		}
		for($i=0;$i<count($mTramsContractes);$i++)
		{
			if($mTramsContractes[$i]!='')
			{
				$mIndexQuantitat=explode(':',$mTramsContractes[$i]);
			
				$index=str_replace('tram_','',$mIndexQuantitat[0]);
				$mQuantitat=explode('|',$mIndexQuantitat[1]);
				if($index!='')
				{
					$mPars['selTramId']=$index;
					$mTram=db_getTram($db);
					//si existia a la comanda anterior guardar-hi la nova quantitat >/=0
					//inicialitzar
					
					if(!isset($mContractesCombinada[$index]['anterior']['quantitat_pes'])){$mContractesCombinada[$index]['anterior']['quantitat_pes']=0;}
					if(!isset($mContractesCombinada[$index]['anterior']['quantitat_volum'])){$mContractesCombinada[$index]['anterior']['quantitat_volum']=0;}
					if(!isset($mContractesCombinada[$index]['anterior']['quantitat_places'])){$mContractesCombinada[$index]['anterior']['quantitat_places']=0;}
					if(!isset($mContractesCombinada[$index]['anterior']['vehicle_id'])){$mContractesCombinada[$index]['anterior']['vehicle_id']='';}
					if(!isset($mContractesCombinada[$index]['anterior']['estat'])){$mContractesCombinada[$index]['anterior']['estat']='';}
					if(!isset($mContractesCombinada[$index]['anterior']['tipus'])){$mContractesCombinada[$index]['anterior']['tipus']=$mPars['vTipus'];}

					$mContractesCombinada[$index]['actual']['quantitat_pes']=0;
					$mContractesCombinada[$index]['actual']['quantitat_volum']=0;
					$mContractesCombinada[$index]['actual']['quantitat_places']=0;
					$mContractesCombinada[$index]['actual']['vehicle_id']=$mQuantitat[3];
					$mContractesCombinada[$index]['actual']['tipus']=$mContractesCombinada[$index]['anterior']['tipus'];
					$mContractesCombinada[$index]['actual']['estat']=$mContractesCombinada[$index]['anterior']['estat'];
					$mContractesCombinada[$index]['actual']['tram_id']=$index;
					
					//evitar solapament de trams per un mateix vehicle
					if($mContractesCombinada[$index]['actual']['tipus']=='DEMANDES')
					{
						if
						(
							!$result=mysql_query("select * from trams_".$mPars['selRutaSufix']." 
							where 
							(
								(
									UNIX_TIMESTAMP(sortida)<=UNIX_TIMESTAMP('".$mTram['sortida']."')
									AND
									UNIX_TIMESTAMP(arribada)>UNIX_TIMESTAMP('".$mTram['sortida']."')
								)
								OR
								(
									UNIX_TIMESTAMP(sortida)<UNIX_TIMESTAMP('".$mTram['arribada']."')
									AND
									UNIX_TIMESTAMP(arribada)>=UNIX_TIMESTAMP('".$mTram['arribada']."')
								)
							)
							AND
							vehicle_id='".$mContractesCombinada[$index]['actual']['vehicle_id']."'
							AND 
							actiu='1'
							",$db)
						)
						{
							//echo "<br> 222 db_gestioTramsOfertes.php ".mysql_errno() . ": " . mysql_error(). "\n";
							$mMissatgeAlerta['result']=false;
							$mMissatgeAlerta['missatge']="Atenci�: error en guardar l'oferta de transport. No s'han pogut comprovar interferencies amb dates d'altres ofertes pel mateix vehicle assignat";
							return $mMissatgeAlerta;
						}
						else
						{
							$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
							if($mRow)
							{
								$mMissatgeAlerta['result']=false;
								$mMissatgeAlerta['missatge']="Atenci�: error en guardar el contracte de la demanda de transport id:".$index.". Les dates de sortida o arribada s'interfereixen amb les d'una altra oferta o demanda activa que t� el mateix vehicle assignat";
								return $mMissatgeAlerta;
							}
						}
					}
					if(array_key_exists($index,$mContractesCombinada))
					{
						if($mContractesCombinada[$index]['anterior']['estat']=='acceptat')
						{
							$mContractesCombinada[$index]['actual']['quantitat_pes']=$mContractesCombinada[$index]['anterior']['quantitat_pes'];
							$mContractesCombinada[$index]['actual']['quantitat_volum']=$mContractesCombinada[$index]['anterior']['quantitat_volum'];
							$mContractesCombinada[$index]['actual']['quantitat_places']=$mContractesCombinada[$index]['anterior']['quantitat_places'];
							$mContractesCombinada[$index]['actual']['vehicle_id']=$mContractesCombinada[$index]['anterior']['vehicle_id'];

							//el tipus no canvia:
							$mContractesCombinada[$index]['actual']['tipus']=$mContractesCombinada[$index]['anterior']['tipus'];
							
							//l'estat nom�s el canvia la part contractada:
							$mContractesCombinada[$index]['actual']['estat']='acceptat';
						
							$mContractesCombinada[$index]['actual']['tram_id']=$mContractesCombinada[$index]['anterior']['tram_id'];
						}
						else
						{
							//vd($mContractesCombinada[$index]);
							$mContractesCombinada[$index]['actual']['quantitat_pes']=$mQuantitat[0];
							$mContractesCombinada[$index]['actual']['quantitat_volum']=$mQuantitat[1];
							$mContractesCombinada[$index]['actual']['quantitat_places']=$mQuantitat[2];

							//si es tipus oferta, el vehicle no canvia:
							if($mContractesCombinada[$index]['actual']['tipus']=='OFERTES' && $mContractesCombinada[$index]['anterior']['vehicle_id']!='')
							{
								$mContractesCombinada[$index]['actual']['vehicle_id']=$mContractesCombinada[$index]['anterior']['vehicle_id'];
							}
							else
							{
								$mContractesCombinada[$index]['actual']['vehicle_id']=$mQuantitat[3];
							}

							//el tipus no canvia:
							$mContractesCombinada[$index]['actual']['tipus']=$mContractesCombinada[$index]['anterior']['tipus'];
							
							//l'estat nom�s el canvia la part contractada:
							$mContractesCombinada[$index]['actual']['estat']='pendent';
						
							$mContractesCombinada[$index]['actual']['tram_id']=$index;
						}
					}
					else	//si no, i es >0, afegir
					{
						if($mQuantitat[0]>0 || $mQuantitat[1]>0 || $mQuantitat[2]>0)
						{
							$mContractesCombinada[$index]['actual']['quantitat_pes']=$mQuantitat[0];
							$mContractesCombinada[$index]['actual']['quantitat_volum']=$mQuantitat[1];
							$mContractesCombinada[$index]['actual']['quantitat_places']=$mQuantitat[2];
							$mContractesCombinada[$index]['actual']['vehicle_id']=$mQuantitat[3];
							$mContractesCombinada[$index]['actual']['tipus']=$mQuantitat[4];
							$mContractesCombinada[$index]['actual']['estat']=$mQuantitat[5];
							$mContractesCombinada[$index]['actual']['tram_id']=$index;
						}
					}
				}
			}
		}
		//refer cadena comanda i passar comanda combinada a comanda
		$chain='';
		$n=0;
		while(list($index2,$mQuantitat)=each($mContractesCombinada))
		{
			if($mQuantitat['actual']['quantitat_pes']>0 || $mQuantitat['actual']['quantitat_volum']>0 || $mQuantitat['actual']['quantitat_places']>0)
			{
				$chain.='tram_'.$index2.':'.$mQuantitat['actual']['quantitat_pes'].'|'.$mQuantitat['actual']['quantitat_volum'].'|'.$mQuantitat['actual']['quantitat_places'].'|'.$mQuantitat['actual']['vehicle_id'].'|'.$mQuantitat['actual']['tipus'].'|'.$mQuantitat['actual']['estat'].';';
			}
		}
		reset($mContractesCombinada);
		$guardarContractes=$chain;
		// 4. guardar a bd la nova comanda
						//---------------------------
						//registre propietats contractament
						$registre='';
						if($guardarContractes!=$mContractesAnteriorCopiar['resum'])
						{
							$registre='{p:guardarContractes;us:'.$mPars['usuari_id'].';dt:'.date('d/m/Y H:i:s').';r:'.$guardarContractes.'}';
						}
				
				//---------------------------
				if(count($mContractesAnterior)>0)	//ja existeix un contracte d'aquest usuari
				{
					if(!$result=@mysql_query("update contractes_".$mPars['selRutaSufix']." set resum='".$guardarContractes."', propietats=CONCAT('".$registre."',propietats) where id='".$mContractesAnteriorCopiar['id']."'",$db))
					{
						//echo "<br> 406 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
						//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
					
						$mMissatgeAlerta['result']=false;
						$mMissatgeAlerta['missatge']="Atenci�: error en guardar el contracte id:".$mContractesAnteriorCopiar['id'];
						return $mMissatgeAlerta;
					}	
					else
					{
						// 3. actualitzar estoc a nou estat:
						if(count($mContractesCombinada)>0)
						{
							while(list($index,$mQuantitat)=each($mContractesCombinada))
							{
								if(!$result=mysql_query("update trams_".$mPars['selRutaSufix']." set pes_disponible=pes_disponible+".$mQuantitat['anterior']['quantitat_pes']."-".$mQuantitat['actual']['quantitat_pes'].", volum_disponible=volum_disponible+".$mQuantitat['anterior']['quantitat_volum']."-".$mQuantitat['actual']['quantitat_volum'].", places_disponibles=places_disponibles+".$mQuantitat['anterior']['quantitat_places']."-".$mQuantitat['actual']['quantitat_places']." where id='".$mQuantitat['actual']['tram_id']."'",$db))
								{
									//echo "<br> 422 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
									$mMissatgeAlerta['result']=false;
									$mMissatgeAlerta['missatge']="Atenci�: error en guardar l'estoc de recursos del tram id:".$mQuantitat['actual']['tram_id'];
									return $mMissatgeAlerta;
								}
							}
							reset($mContractesCombinada);				
						}
					}
				}
				else 
				{
					if(!$result=@mysql_query("insert into contractes_".$mPars['selRutaSufix']." values('','".(date('YmdHis'))."','".$mPars['usuari_id']."','".(date('Y-m-d H:i:s'))."','".$guardarContractes."','pendent','','".$registre."')",$db))
					{
						//echo "<br> 432 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
						//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
				
						$mMissatgeAlerta['result']=false;
						$mMissatgeAlerta['missatge']="Atenci�: error en guardar el nou contracte del tram id:".$index2;
						return $mMissatgeAlerta;
					}
					else
					{
						// 3. actualitzar estoc a nou estat:
						if(count($mContractesCombinada)>0)
						{
							while(list($index,$mQuantitat)=each($mContractesCombinada))
							{
								if(!$result=mysql_query("update trams_".$mPars['selRutaSufix']." set pes_disponible=pes_disponible+".$mQuantitat['anterior']['quantitat_pes']."-".$mQuantitat['actual']['quantitat_pes'].", volum_disponible=volum_disponible+".$mQuantitat['anterior']['quantitat_volum']."-".$mQuantitat['actual']['quantitat_volum'].", places_disponibles=places_disponibles+".$mQuantitat['anterior']['quantitat_places']."-".$mQuantitat['actual']['quantitat_places']." where id='".$mQuantitat['actual']['tram_id']."'",$db))
								{
									//echo "<br> 269 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
									$mMissatgeAlerta['result']=false;
									$mMissatgeAlerta['missatge']="Atenci�: error en guardar l'estoc de recursos del tram id:".$mQuantitat['actual']['tram_id'];
									return $mMissatgeAlerta;
								}
							}
							reset($mContractesCombinada);				
						}
					}
				}

	// enviar mail si hi ha un nou contracte o un canvi en un contracte anterior encara no acceptat

		while(list($index2,$mQuantitat)=each($mContractesCombinada))
		{
			//si el contracte es nou
			if
			(
				$mQuantitat['anterior']['quantitat_pes']==0
				&&
				$mQuantitat['anterior']['quantitat_volum']==0
				&&
				$mQuantitat['anterior']['quantitat_places']==0
				&&
				$mQuantitat['anterior']['vehicle_id']==''
			)
			{
				mail_missatgeNovaResposta('contracteNou',$index2,$mContractesCombinada[$index2],$db);
			}			
			else
			{
				if
				(
					$mQuantitat['anterior']['estat']=='pendent'
					&&
					(
						$mQuantitat['anterior']['quantitat_pes']!=$mQuantitat['actual']['quantitat_pes']
						||
						$mQuantitat['anterior']['quantitat_volum']!=$mQuantitat['actual']['quantitat_volum']
						||
						$mQuantitat['anterior']['quantitat_places']!=$mQuantitat['actual']['quantitat_places']
						||
						$mQuantitat['anterior']['vehicle_id']!=$mQuantitat['actual']['vehicle_id']
					)
				)
				{
					mail_missatgeNovaResposta('contracteModificat',$index2,$mContractesCombinada[$index2],$db);
				}
			}
		}
		reset($mContractesCombinada);
	
	
		$mMissatgeAlerta['result']=true;
		$mMissatgeAlerta['missatge']="Els canvis s'han guardat correctament";
		return $mMissatgeAlerta;

	return true;
}


//------------------------------------------------------------------------------
function confirmarResposta($db)
{
	global $mPars;
	
	$missatgeAlerta='';

	//canviar l'estat de del contracte sobre la demanada de pendent->confirmat
	
	$mConfirmarResposta=explode(',',$mPars['i_confirmarResposta']);
	//$mConfirmarResposta[0]->usuariContractatId
	//$mConfirmarResposta[1]->tramContractatId
	//$mConfirmarResposta[2]->usuariContractantId
	//$mConfirmarResposta[3]->contracteId
	//$mConfirmarResposta[4]->accio
	$estat='';
	
	//obtenir el contracte:
		
	if(!$result=mysql_query("select * from contractes_".$mPars['selRutaSufix']."  where id='".$mConfirmarResposta[3]."'",$db))
	{
		//echo "<br> 470 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="ERROR: No s'ha pogut llegir el contracte";
			
		return $missatgeAlerta;
	}
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			
		$mContractesUsuari=explode(';',$mRow['resum']);
			
		$index=0;
		$mConfirmarResposta['tipus']='';
		while(list($key,$contracteUsuari)=each($mContractesUsuari))
		{
			$mIndexQuantitat=explode(':',$contracteUsuari);
			$index=str_replace('tram_','',$mIndexQuantitat[0]);
			$mQuantitat=explode('|',$mIndexQuantitat[1]);
			if($index==$mConfirmarResposta[1])
			{
				$mConfirmarResposta['pes_reservat']=$mQuantitat[0];
				$mConfirmarResposta['volum_reservat']=$mQuantitat[1];
				$mConfirmarResposta['places_reservades']=$mQuantitat[2];
				$mConfirmarResposta['vehicle_id']=$mQuantitat[3];
				$mConfirmarResposta['tipus']=$mQuantitat[4];
				$mConfirmarResposta['estat']=$mQuantitat[5];
				break;
			}
		}
		reset($mContractesUsuari);
		
		if($mConfirmarResposta['tipus']=='')
		{
			$missatgeAlerta="ERROR: No s'ha trobat el tram de la resposta que vols confirmar.";
			
			return $missatgeAlerta;
		}
		
	}
		
	if($mConfirmarResposta[4]=='c')
	{
		$chain1='tram_'.$index.':'.$mConfirmarResposta['pes_reservat'].'|'.$mConfirmarResposta['volum_reservat'].'|'.$mConfirmarResposta['places_reservades'].'|'.$mConfirmarResposta['vehicle_id'].'|'.$mConfirmarResposta['tipus'].'|pendent;';
		$chain2='tram_'.$index.':'.$mConfirmarResposta['pes_reservat'].'|'.$mConfirmarResposta['volum_reservat'].'|'.$mConfirmarResposta['places_reservades'].'|'.$mConfirmarResposta['vehicle_id'].'|'.$mConfirmarResposta['tipus'].'|acceptat;';
		if(!$result=mysql_query("update contractes_".$mPars['selRutaSufix']." set resum=REPLACE(resum,'".$chain1."','".$chain2."') where id='".$mConfirmarResposta[3]."'",$db))
		{
			//echo "<br> 453 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
			$missatgeAlerta="ERROR: No s'ha pogut CONFIRMAR la resposta de l'usuari a la teva demanda";
			
			return $missatgeAlerta;
		}
		$estat='CONFIRMADA';
	}
	else if($mConfirmarResposta[4]=='d')
	{
		//modificar estoc tram, per deixar-lo lliure d'aquesta reserva i eliminar la reserva
		$chain1='tram_'.$index.':'.$mConfirmarResposta['pes_reservat'].'|'.$mConfirmarResposta['volum_reservat'].'|'.$mConfirmarResposta['places_reservades'].'|'.$mConfirmarResposta['vehicle_id'].'|'.$mConfirmarResposta['tipus'].'|pendent;';

		if(!$result=mysql_query("update trams_".$mPars['selRutaSufix']." set pes_disponible=pes_disponible+".$mConfirmarResposta['pes_reservat'].", volum_disponible=volum_disponible+".$mConfirmarResposta['volum_reservat'].", places_disponibles=places_disponibles+".$mConfirmarResposta['places_reservades']." where id='".$mConfirmarResposta[1]."'",$db))
		{
			echo "<br> 463 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
			$missatgeAlerta="ERROR: No s'ha pogut actualitzar l'estoc de recursos solicitats en la demanda de transport";
			
			return $missatgeAlerta;
		}
		else if(!$result=mysql_query("update contractes_".$mPars['selRutaSufix']." set resum=REPLACE(resum,'".$chain1."','') where id='".$mConfirmarResposta[3]."'",$db))
		{
			//echo "<br> 463 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
			$missatgeAlerta="ERROR: No s'ha pogut DESCARTAR la resposta de l'usuari a la teva demanda";
			
			if(!$result=mysql_query("update trams_".$mPars['selRutaSufix']." set pes_disponible=pes_disponible-".$mConfirmarResposta['pes_reservat'].", volum_disponible=volum_disponible-".$mConfirmarResposta['volum_reservat'].", places_disponibles=places_disponibles-".$mConfirmarResposta['places_reservades']." where id='".$mConfirmarResposta[1]."'",$db))
			{
				//echo "<br> 463 db_contractesTrams.php ".mysql_errno() . ": " . mysql_error(). "\n";
			}

			return $missatgeAlerta;
		}
		$estat='DESCARTADA';
	}
	$mPars['selTramId']=$mConfirmarResposta[1];
	$mTram=db_getTram($db);
	$mPars['selVehicleId']=$mConfirmarResposta['vehicle_id'];
	$mVehicle=db_getVehicle($db);
	if(!mail_missatgeAresposta($mConfirmarResposta,$mTram,$mVehicle))
	{
		$missatgeAlerta="ERROR: No s'ha pogut comunicar a l'usuari que la seva resposta ha estat ".$estat;
			
		return $missatgeAlerta;
	}
	
	

	return $missatgeAlerta;

}

//------------------------------------------------------------------------------
function db_getUsuarisTrams($db)
{
	global $mPars;
	
	$mUsuarisTrams=array();

	$result='';
	
	$i=0;
	$result=mysql_query("select DISTINCT(usuari_id) from trams_".$mPars['selRutaSufix']." order by id ASC",$db);
	//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mUsuarisTrams[$i]=$mRow;
			$i++;
		}
	}
	
	return $mUsuarisTrams;
}

//------------------------------------------------------------------------------
function db_getMesosTrams($db)
{
	global $mPars;
	
	$mMesosTrams=array();
	
	if($result=mysql_query("select DISTINCT(SUBSTRING(sortida,3,5)) from trams_".$mPars['selRutaSufix']." order by id ASC",$db))
	{
		//echo "<br>  714 db_contractesTrams.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			array_push($mMesosTrams,str_replace('-','',$mRow[0]));
		}
	}
	asort($mMesosTrams);
	return $mMesosTrams;
}

//------------------------------------------------------------------------------
function db_anularContractesTram($tramId,$db)
{
	global $mPars;
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['result']='';
	$mMissatgeAlerta['missatge']='';
	
	$continuar=false;
	
	//obtenir dades dels contractes establerts sobre aquest tram:
	$mUnitatsContractades=array();

	if(!$result=mysql_query("select * from contractes_".$mPars['selRutaSufix']." where LOCATE('tram_".$tramId.":',CONCAT(' ',resum))>0 order by id ASC",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mMissatgeAlerta['missatge'].="<p class='p_alertaNo'>No s'han pogut llegir els contractes</p>";
		$mMissatgeAlerta['result']=false;
		
		return $mMissatgeAlerta;
	}
	else
	{

	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mUnitatsContractades[$mRow['id']]=array();
		$mUnitatsContractades[$mRow['id']]['pes']=0;
		$mUnitatsContractades[$mRow['id']]['volum']=0;
		$mUnitatsContractades[$mRow['id']]['places']=0;
		$mUnitatsContractades[$mRow['id']]['total']=0;
		$mUnitatsContractades[$mRow['id']]['vehicle_id']='';
		$mUnitatsContractades[$mRow['id']]['tipus']='';
		$mUnitatsContractades[$mRow['id']]['estat']='';
		$mUnitatsContractades[$mRow['id']]['resum']=$mRow['resum'];
		$mUnitatsContractades[$mRow['id']]['usuari_id']=$mRow['usuari_id'];//part contractant

		$quantitat=substr($mRow['resum'],strpos($mRow['resum'],"tram_".$tramId.":")+strlen("tram_".$tramId.":"));
		$quantitat=substr($quantitat,0,strpos($quantitat,';'));
		$mUnitatsContractades_=explode('|',$quantitat);
		$mUnitatsContractades[$mRow['id']]['pes']=$mUnitatsContractades_[0];
		$mUnitatsContractades[$mRow['id']]['volum']=$mUnitatsContractades_[1];
		$mUnitatsContractades[$mRow['id']]['places']=$mUnitatsContractades_[2];
		$mUnitatsContractades[$mRow['id']]['vehicle_id']=$mUnitatsContractades_[3];
		$mUnitatsContractades[$mRow['id']]['tipus']=$mUnitatsContractades_[4];
		$mUnitatsContractades[$mRow['id']]['estat']=$mUnitatsContractades_[5];
		$mUnitatsContractades[$mRow['id']]['total']=$mUnitatsContractades[$mRow['id']]['pes']+$mUnitatsContractades[$mRow['id']]['volum']+$mUnitatsContractades[$mRow['id']]['places'];
		if($mUnitatsContractades[$mRow['id']]['total']>0){$continuar=true;}
	}

	
	if($continuar)
	{
		//eliminar  els contractes
		while(list($contracteId,$mContracte)=each($mUnitatsContractades))
		{
			$contracteVell='tram_'.$tramId.':'.$mContracte['pes'].'|'.$mContracte['volum'].'|'.$mContracte['places'].'|'.$mContracte['vehicle_id'].'|'.$mContracte['tipus'].'|'.$mContracte['estat'].';';
			if(!$result=mysql_query("update contractes_".$mPars['selRutaSufix']." set resum=REPLACE(resum,'".$contracteVell."','') where id='".$contracteId."'",$db))
			{
				$mMissatgeAlerta['missatge'].="<p class='p_alertaNo'>Atenci�: el contracte ".$contracteId." no s'ha pogut anul.lar</p>";
				$mMissatgeAlerta['result']=false;
			}
		}	
		reset($mUnitatsContractades);

		//corregir estoc recursos tram
		if(!$result=mysql_query("update trams_".$mPars['selRutaSufix']." set pes_disponible=capacitat_pes, volum_disponible=capacitat_volum, places_disponibles=capacitat_places where id='".$tramId."'",$db))
		{
			$mMissatgeAlerta['missatge'].="<p class='p_alertaNo'>Atenci�: el tram ".$tramId." no s'ha modificat a recursos 100%</p>";
			$mMissatgeAlerta['result']=false;
		}
	
		//avisar les parts
		if(!mail_anulacioContractes($tramId,$db))
		{
			$mMissatgeAlerta['missatge'].="<p class='p_alertaNo'>Atenci�: no s'ha pogut notificar l'anul.laci� de contractes a les parts</p>";
			$mMissatgeAlerta['result']=false;
		}
	}
	}

	return $mMissatgeAlerta;
}

?>

		