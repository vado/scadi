<?php
include "config.php";
include "einesConfig.php";
include "html_ajuda1.php";
include "db_ajuda.php";
include "db.php";
include "eines.php";

//------------------------------------------------------------------------
function recepcioFitxer($db)
{
    global $mParametres,$mPars,$mTipusUpload;

    $missatgeError='';

	$dir='';
	$dirChain='';
	$nomFitxerTipus='';
	
	$dir0=getcwd();
	if(!@chdir('old'))
	{
		mkdir('old');
	}
	else
	{
		chdir($dir0);
	}
	
	$tipus=@$_POST['i_tipus'];
	if(isset($tipus) && $tipus!='')
	{
		if(array_key_exists($tipus,$mTipusUpload))
		{
			$dia=$_POST['i_dia'];
			$mes=$_POST['i_mes'];
			$any=$_POST['i_any'];
			
			if($mTipusUpload[$tipus]['periodicitat']=='mensual')
			{
				$nomFitxerTipus=$mTipusUpload[$tipus]['nomFitxer'].'_'.$any.'_'.$mes;
				vd($nomFitxerTipus);
			}
			else if($mTipusUpload[$tipus]['periodicitat']=='anual')
			{
				$nomFitxerTipus=$mTipusUpload[$tipus]['nomFitxer'].'_'.$any;
			}
			else //inclou tipus='variable'
			{
				$nomFitxerTipus=$mTipusUpload[$tipus]['nomFitxer'].'_'.$any.'_'.$mes.'_'.$dia;
			}
			
			$dir=$mTipusUpload[$tipus]['nomFitxer'];
		}
		else
		{
			$nomFitxerTipus='';
		}
	}	
	else
	{
		$dir=@$_POST['sel_directori'];
	}	

	if($dir!='')
	{
		$dirChain=$dir.'/';
		if(!@chdir($dir))
		{
			mkdir($dir);
		}
		else
		{
			chdir($dir0);
		}
	}

    if($_FILES['f_fitxer']['name']!='')
    {
        if (is_uploaded_file($_FILES['f_fitxer']['tmp_name']))
        {
            //mirar si l'extensio �s correcta:
            $size=@pathinfo($_FILES['f_fitxer']['name']);
            
			if($nomFitxerTipus!='')
			{
				$nomFitxer=$nomFitxerTipus.'.'.$size['extension'];
			}
			else
			{
				$nomFitxer=$_FILES['f_fitxer']['name'];
			}
			if($dir=='')
			{
				@rename('old/old_'.$nomFitxer,'old_'.$nomFitxer);
			}
    
	        if(!copy($_FILES['f_fitxer']['tmp_name'],$dirChain.$nomFitxer))
            {
                $missatgeError.="<br> Atenci�: Error en copiar (".$dirChain.$nomFitxer.").";
				if($dir=='')
				{
					rename('old/old_'.$nomFitxer,$nomFitxer);
				}
            }
		}
    }

    if(strlen($missatgeError)==0)
	{
        $missatgeError.="<br> Ok: El fitxer s'ha guardat correctament.";
    }

return $missatgeError;
}
//--------------------------------------------------------------------


$missatgeAlerta='';


$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$parsChain=makeParsChain($mPars);
$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];

$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}
getConfig($db); //inicialitza variables anteriors;

if(!checkLogin($db) || $mPars['nivell']!='sadmin')
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['uploadF.php']=db_getAjuda('uploadF.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$mRutesSufixes=getRutesSufixes($db);
$mMissatgeAlerta=array();

$op=@$_POST['i_op'];
if(isset($op))
{
	if($op=='pujarFitxer')
	{
		$missatgeAlerta=recepcioFitxer($db);
	}
}

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>pujar fitxers</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_upload.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
ruta=".$ruta.";
mTipusUpload=new Array();
";
while(list($id,$mVal)=each($mTipusUpload))
{
	echo "
mTipusUpload['".$id."']=new Array();
	";
	while(list($propietat,$valor)=each($mVal))
	{
		echo "
mTipusUpload['".$id."']['".$propietat."']='".$valor."';
		";
	}
	reset($mVal);
}
reset($mTipusUpload);
echo "
function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='llistat_grups.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}
</script>

</head>
<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('uploadF.php?');
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</p>
			</td>
		</tr>
	</table>

	<table align='center' style='width:80%'>
		<tr>
			<th style='width:100%;' align='center'>
			".$missatgeAlerta."
			</td>
		</tr>
	</table>
<center><p style='font-size:16px;'>[ Ruta <b>".$mPars['selRutaSufix']."</b>: Pujar fitxers ]</p></center>
";

if(count($mMissatgeAlerta)>0)
{
	echo "
	<table align='center' border='1' width='60%'>
		<tr>
			<td  bgcolor='#eeeeee'  align='left'  width='30%' >
			<br>
			<p>pujades:</p>
			<br>
	";
	while(list($key,$mVal)=each($mMissatgeAlerta))
	{
		echo "
			<p>".$mVal['fitxer']." >  ".$mVal['result']."</p>
			";
	}
	reset($mMissatgeAlerta);
	echo "
			</td>
		</tr>
	</table>
	";
}
echo "
	<table align='center' bgcolor='#dddddd' width='70%' align='center'>
		<tr>
			<td  bgcolor='#eeeeee'  width='100%' >
			<table width='80%' border='0' align='center'>
				<tr>
					<td width='90%' align='center'>
					<table width='100%'>
						<tr>
							<td>
							<FORM name='f_pujarFitxer' METHOD='post' action='uploadF.php' target='_self'  ENCTYPE='multipart/form-data'>
							<table>
								<tr>
									<td  valign='top'>
									<INPUT TYPE='file' size='8' NAME='f_fitxer' accept='.php'>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td>
									<p>Selecciona el tipus de fitxer:
									<br>
									<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
									<input type='hidden' id='i_op' name='i_op' value='pujarFitxer'>
									<select id='i_tipus' name='i_tipus' onChange=\"javascript: mostrarSelectorsData(this.value);\">
";
while(list($id,$mVal)=each($mTipusUpload))
{
	echo "
									<option value='".$id."'>".$mVal['nom']." (".$mVal['periodicitat'].")</option>						
	";
}
reset($mTipusUpload);
echo "
									<option selected value=''></option>						
									</select>
									</p>
									</td>

									<td valign='top'>
									
									<div id='d_data' style='visibility:hidden;'>
									<p>Selecciona la data del fitxer:
									<br>
									Dia:	<select id='i_dia' name='i_dia' >
";
for($i=1;$i<32;$i++)
{
	if(date('d')==$i){$selected='selected';}else{$selected='';}
	echo "
									<option ".$selected." value='".$i."'>".$i."</option>						
	";
}
reset($mTipusUpload);
echo "
									</select>
									&nbsp;&nbsp;mes: <select id='i_mes' name='i_mes'>
";
for($i=1;$i<13;$i++)
{
	if(date('m')==$i){$selected='selected';}else{$selected='';}
	echo "
									<option ".$selected." value='".$i."'>".$i."</option>						
	";
}
reset($mTipusUpload);
echo "
									</select>
									&nbsp;&nbsp;Any: 
									<select id='i_any' name='i_any'>
";
for($i=(date('Y')-1);$i<=date('Y');$i++)
{
	if(date('Y')==$i){$selected='selected';}else{$selected='';}
	echo "
									<option ".$selected." value='".$i."'>".$i."</option>						
	";
}
reset($mTipusUpload);
echo "
									</select>
									</p>
									</div>
									</td>
								</tr>
								<tr>
									<td>
									<div id='d_dir' style='visibility:inherit;'>
					<p>selecciona carpeta:<br>
					<select id='sel_directori' name='sel_directori'>
					";
					$dir=getcwd();
					$mScan=scandir($dir);
					$mDirs=array();
					$mFiles=array();
					
					while(list($key,$val)=each($mScan))
					{
						if($val!='.' && $val!='..')
						{
							if(@chdir($val))
							{
								@chdir($dir);
								array_push($mDirs,$val);
							}
							else
							{
								array_push($mFiles,$val);
							}
						}
					}
					reset($mScan);
					if(!in_array("docs".$mPars['selRutaSufix'],$mDirs))
					{
						array_push($mDirs,"docs".$mPars['selRutaSufix']);
					}
					sort($mDirs);
					sort($mFiles);
					
					while(list($key,$val)=each($mDirs))
					{
						echo "
					<option value='".$val."'>".$val."</option>						
						";
					}
					echo "
					<option selected value=''></option>						
					</select>
					</p>
									</div>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td>
									<br>
									<input 
									";
									if($mPars['demo']==-1)
									{
										$disabled='';
									}
									else
									{
										$disabled='DISABLED';
									}
									echo " type='submit' ".$disabled." value='pujar fitxer'>
									</td>
									<td>
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					
					</form>
					<p>Arxius:<br>
					<select>
					";
					
					while(list($key,$val)=each($mFiles))
					{
						echo "
					<option value='".$val."'>".$val."</option>						
						";
					}
					echo "
					</select>
					</p>
					</table>
					<br>
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
";
html_helpRecipient();

echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>
";

?>

		