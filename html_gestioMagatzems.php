<?php
//-------------------------------------------------------
function taulaResumComanda()
{
	global $rutaStr,$mParametres;
	
	echo "
				<table  border='0'   cellspacing='1' cellpadding='1' align='center'   style='width:400px;'>
					<tr>
						<th align='right'  bgcolor='#D1FABB' style='width:50%; '>
						</th>

						<th align='right'  bgcolor='#D1FABB' style='width:10%; '>
						<p></p>
						</th>

						<th align='right'  bgcolor='#D1FABB' style='width:13%; '>
						<p>ums</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' style='width:13%; '>
						<p>Ecos</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' style='width:13%;'>
						<p>Euros</p>
						</th>
					</tr>
			
					<tr>
						<td  bgcolor='#EDF8E6' align='left'>
						<a title=\"Cost Transport Extern per kg sobre el producte\"><p style='font-size:11px;'>&nbsp;(CTEK)</p></a>
						</td>

						<td  bgcolor='#EDF8E6' align='right'>
						</td>

						<td  bgcolor='#EDF8E6' align='right' id='td_totalCtekUnitats'>
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtekEcos' >
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtekEuros' >
						<p>0</p>
						</td>
					</tr>
		
					<tr>
						<td   bgcolor='#EDF8E6' align='left'>
						<a title=\"Cost Transport Extern Repartit\"><p style='font-size:11px;'>&nbsp;(CTER)</p></a>
						</td>

						<td   bgcolor='#EDF8E6' align='right' >
						</td>

						<td   bgcolor='#EDF8E6' align='right' id='td_totalCterUnitats' >
						<p>0</p>
						</td>
						
						<td   bgcolor='#EDF8E6' align='right' id='td_totalCterEcos' >
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCterEuros' >
						<p>0</p>
						</td>
					</tr>
		
		
					<tr>
						<td   bgcolor='#EDF8E6' align='left'>
						<a title=\"Cost Transport Intern per kg sobre el producte\"><p style='font-size:11px;'>&nbsp;(CTIK)</p></a>
						</td>

						<td   bgcolor='#EDF8E6' align='right' style='width:10%;'>
						</td>

						<td   bgcolor='#EDF8E6' align='right' id='td_totalCtikUnitats' >
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtikEcos'>
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtikEuros'>
						<p>0</p>
						</td>
					</tr>
			
			
					<tr>
						<td  bgcolor='#EDF8E6'  align='left'>
						<a title=\"Cost Transport Intern Repartit\"><p style='font-size:11px;'>&nbsp;(CTIR)</p></a>
						</td>

						<td  bgcolor='#EDF8E6'  align='right' style='width:10%;'>
						</td>

						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtirUnitats'>
						<p>0</p>
						</td>
						
						<td   bgcolor='#EDF8E6' align='right' id='td_totalCtirEcos' >
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtirEuros'>
						<p>0</p>
						</td>

					</tr>
			
					<tr>
						<td  bgcolor='#D1FABB' align='left'>
						<a title=\"Total Cost Transport Productes CAC\"><p style='font-size:11px;'>&nbsp;Total Cost Transport (CAC)</p></a>
						</td>

						<td  bgcolor='#D1FABB' align='right' style='width:10%;'>
						</td>

						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportUnitats'>
						<p>0</p>
						</th>
						
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportEcos'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportEuros'>
						<p>0</p>
						</th>
					</tr>

					<tr>
						<td  bgcolor='#EDF8E6' align='left'>
						<a title=\"Total kg\"><p style='font-size:11px;'>&nbsp;Total kg</p></a>
						</td>

						<td  bgcolor='#EDF8E6' align='right' id='td_totalQuantitat' >
						<p>0</p>
						</td>

						<td bgcolor='#EDF8E6' >
						</td>
						
						
						<td  bgcolor='#EDF8E6' align='right'>
						</td>
						
						<td  bgcolor='#EDF8E6' align='right'>
						</td>
					</tr>

					<tr>
						<td  bgcolor='#D1FABB' align='left'>
						<a title=\"Preu productes CAC\"><p style='font-size:11px;'>&nbsp;<b>Preu productes CAC</b></p></a>
						</td>

						<td  bgcolor='#D1FABB' align='right'>
						</td>

						<th  bgcolor='#D1FABB' align='right' id='td_totalUnitats'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalEcos'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalEuros'>
						<p>0</p>
						</th>
					</tr>
			
					<tr>
						<td  bgcolor='#C0FAF8' align='left'>
						<a title=\"Fons Despeses CAC (".$mParametres['FDCpp']['valor']."% sobre preus, ".$mParametres['msFDCpp']['valor']."% MS)\"><p style='font-size:11px;'>&nbsp;<b>Fons despeses CAC</b></p></a>
						</td>

						<td  bgcolor='#C0FAF8' align='right'>
						</td>

						<th  bgcolor='#C0FAF8' align='right' id='td_totalUnitatsFDC'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#C0FAF8' align='right' id='td_totalEcosFDC'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#C0FAF8' align='right' id='td_totalEurosFDC'>
						<p>0</p>
						</th>
					</tr>

					<tr>
						<td  bgcolor='#EDF8E6' align='left'>
						<a title=\"Total kg Intercanvis entre Rebosts\"><p style='font-size:11px;'>&nbsp;<b>kg Inter-Rebosts</b></p></a>
						</td>

						<td  bgcolor='#EDF8E6' align='right' id='td_totalKgIntercanvisRebosts'>
						<p>0</p>
						</td>

						<td  bgcolor='#EDF8E6' align='right' style='width:10%;'>
						</td>

						<td  bgcolor='#EDF8E6' align='right'><p>&nbsp;</p></td>
									
						<td  bgcolor='#EDF8E6' align='right' ><p>&nbsp;</p></td>
					</tr>

					<tr>
						<th  bgcolor='#D1FABB' align='left'>
						<a title=\"Cost transport Intercanvis entre Rebosts\"><p style='font-size:11px;'>&nbsp;<b>Cost Transp. Inter-Rebosts</b></p></a>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' style='width:10%;'>
						</th>

						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportIntercanvisRebostsUnitats'><p>&nbsp;</p></th>
						
									
						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportIntercanvisRebostsEcos'><p>&nbsp;</p></th>
						

						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportIntercanvisRebostsEuros'><p>&nbsp;</p></th>

					</tr>			

					<tr>
						<th bgcolor='#D1FABB'  align='right'>
						<a title=\"Import Total\"><p style='font-size:11px;'>&nbsp;<b>Import Total</b></p></a>
						</th>

						<td bgcolor='#D1FABB'  align='right' ' >
						<p></p>
						</td>

						<th  bgcolor='#D1FABB' align='right' id='td_totalFacturaUnitats' >
						</th>

						<th  bgcolor='#D1FABB' align='right' id='td_totalFacturaEcos' >
						</th>

						
						<th  bgcolor='#D1FABB' align='right' id='td_totalFacturaEuros' >
						</th>

					</tr>

					<tr>
						<td  bgcolor='#D1FABB' align='right' id='td_pcMs'>
						<a title=\"% moneda social de la comanda, segons la moneda social dels productes demanats\"><p style='font-size:11px;'>&nbsp;</p></a>
						</td>

						<td  bgcolor='#D1FABB' align='right' >
						</td>
						
						<td  bgcolor='#D1FABB' align='right' style='width:10%;' >
						</td>
						
						<td bgcolor='#D1FABB' >
						</td>
						
						<td bgcolor='#D1FABB' >
						</td>
					</tr>
				</table>
	";
	return;
}
//------------------------------------------------------------------------------
function taulaEinesGrups()
{
	global $mPars,$mRebost,$mProductors,$mCategories,$mSubCategories,$mEtiquetes;

	echo "
		<table  bgcolor='#9EeC7B' style='width:100%;' align='center'>
			<tr>
				<td align='left' valign='bottom' style='width:20%;'>
				<a title=\"Selecciona un productor per veure nom�s els seus productes\">
				<select id='sel_vProductor' onChange=\"javascript: f_vistaProductor();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mProductor)=each($mProductors))
		{
			if($mPars['vProductor']==$mProductor['id']){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." value='".$mProductor['id']."'>".(urldecode($mProductor['projecte']))."</option>
			";
		}
		echo "
				<option ".$selected2." value='TOTS'>- tots els productors -</option>
		";
		reset($mProductors);
		echo "
				</select>
				</a>		
				</td>

				<td align='left' valign='bottom' style='width:20%;'>
				<a title=\"Selecciona una categoria principal per veure nom�s els productes d'aquesta categoria\">
				<select id='sel_vCategoria' onChange=\"javascript: f_vistaCategoria();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$categoria)=each($mCategories))
		{
			if($mPars['vCategoria']==$categoria){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." value='".$categoria."'>".$categoria."</option>
			";
		}
		echo "
				<option ".$selected2." value='TOTS'>- totes les categories -</option>
		";
		reset($mCategories);
		echo "
				</select>
				</a>		
				</td>

				<td align='left' valign='bottom' style='width:20%;'>
				<a title=\"Selecciona una subcategoria per veure nom�s els productes d'aquesta subcategoria\">
				<select id='sel_vSubCategoria' onChange=\"javascript: f_vistaSubCategoria();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mSubCategoria)=each($mSubCategories))
		{
			if($mPars['vSubCategoria']==$mSubCategoria['categoria0'].'-'.$mSubCategoria['categoria10']){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." value='".$mSubCategoria['categoria0'].'-'.$mSubCategoria['categoria10']."'>".$mSubCategoria['categoria0'].'-'.$mSubCategoria['categoria10']."</option>
			";
		}
		echo "
				<option ".$selected2." value='TOTS'>- totes les SUB-categories -</option>
		";
		reset($mSubCategories);
		echo "
				</select>
				</a>		
				</td>

				<td align='left' valign='bottom' style='width:20%;'>
				<a title=\"Selecciona una etiqueta per veure nom�s els productes amb aquesta etiqueta\">
				<select id='sel_etiqueta' onChange=\"javascript: f_vistaEtiqueta();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mEtiqueta)=each($mEtiquetes))
		{
			if($mPars['etiqueta']==$mEtiqueta['nom']){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." value='".$mEtiqueta['nom']."'>".$mEtiqueta['nom']."</option>
				";
		}
		echo "
				<option ".$selected2." value='TOTS'>- cap etiqueta -</option>
		";
		reset($mEtiquetes);
		echo "
				</select>
				</a>
				</td>

				<td align='left' valign='bottom' style='width:20%;'>
				<a title=\"Selecciona una etiqueta per NO veure els productes amb aquesta etiqueta\">
				<select id='sel_etiqueta2' onChange=\"javascript: f_vistaEtiqueta2();\">
		";
			$selected='';
			$selected2='selected';
		while(list($key,$mEtiqueta)=each($mEtiquetes))
		{
			if($mPars['etiqueta2']==$mEtiqueta['nom']){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." value='".$mEtiqueta['nom']."'>".$mEtiqueta['nom']."</option>
			";
		}
		echo "
				<option ".$selected2." value='CAP'>- cap etiqueta -</option>
		";
		reset($mEtiquetes);
		echo "
				</select>
				</a>
				</td>
			</tr>
		</table>
	";
	return;
}
//------------------------------------------------------------------------------
function selectorMagatzem()
{
	global $mContinguts,$mMagatzems,$mRebostsAmbComanda,$mPars,$mParsCAC;
	
	
	$addComanda='';
	$optionFormat='';

	if($mPars['grup_id']=='0'){$target='_blank';}else{$target='_self';}
	
	// form
	echo "
<form id='f_rebost' name='f_rebost' action='gestioMagatzems.php' target='_self' method='post'>
<table align='center'>
	<tr>
		<td align='left' valign='top'>
		<p>Selecciona un MAGATZEM-CAC:<br>
		<select id='sel_rebost' name='sel_rebost' onchange=\"javascript:enviarFpars('gestioMagatzems.php?mRef='+this.value,'".$target."');\">
	";
	$selected1='';
	$selected2='selected';
	for ($i=0;$i<count($mMagatzems);$i++)
	{	
		if($mPars['grup_id']==$mMagatzems[$i]['id']){$selected1='selected';$selected2='';}else{$selected1='';}
		$zona_=substr($mMagatzems[$i]['categoria'],strpos($mMagatzems[$i]['categoria'],'2-')+2);
		$zona=substr($zona_,0,strpos($zona_,','));
		if($zona=='')	
		{
			$zona='('.$zona_.')';
		}
		else
		{
			$zona='('.$zona.')';
		}

		echo "
		<option ".$selected1." value=\"".$mMagatzems[$i]['id']."\">".$addComanda.(urldecode($mMagatzems[$i]['nom']))."	".($zona)."</option>";		
	}
		
	echo "		
		<option value='0'>CAC</option>
		<option ".$selected2."></option>
		</select>
		</p>
		</td>
		<td valign='bottom'>
		<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>
		</td>
	</tr>
</table>

</form>
	";
	return;
}


//----------------------------------------
function mostrarDadesMagatzem($db)
{
	global 
	$mColors,
	$mGrupsUsuari,
	$mGrup,
	$mRebosts,
	$mPars,
	$mCategoriesGrup,
	$mComandaPerProductors,
	$mUsuarisRef,
	$mUsuarisGrupRef,
	$mParametres,
	$jaEntregatTkg,
	$jaEntregatTuts,
	$zona,
	$superZona;
	
	echo "
		<table border='0' width='100%' bgcolor='".$mColors['table']."'>
			<tr>
				<td width='50px'>
				<a title='magatzems de la CAC'><img src='imatges/magatzem.gif'></a>
				</td>
				<td  valign='bottom'  align='left' width='20%'>
				<p class='compacte' >
				Usuari:<b> ".(urldecode($mPars['usuari']))."</b> (".$mPars['nivell'].")
				</p>
				</td>
				
				<td width='10%' align='left'>
	";
	$mostrar=0;
	
	if
	(
		$mPars['nivell']!='visitant'
		&& 
		$mPars['grup_id']!=0
		&&
		(
			$mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord' || $mPars['usuari_id']==$mGrup['usuari_id']
		)
		&&
		($mPars['selRutaSufix']>=date('ym'))
	)
	{
		$botoText=" Guardar nou inventari\n\n	ATENCI�:\n * seleccioneu a la columna 'inventari' els estocs de cada producte abans de crear-lo.\n * Crear un nou inventari neteja el registre d'Operacions sobre l'Inventari.\n * Les diferencies d'estoc no justificades respecte l'inventari anterior s'enregistraran com a p�rdua/recuperaci� en el nou inventari segons si son negatives/positives.";
	
		echo "	
				<a title=\"".$botoText."\"><img src='imatges/guardarp.gif' style='cursor:pointer;' onClick=\"guardarInventari();\" value='".$botoText."'></a>
		";
	}
	echo "
				</td>
				
				<td width='60%'>
				<table width=100%'>
					<tr>
						<td id='td_missatgeAlerta' style=' width:100%;' align='center'  valign='bottom'>
						</td>
					</tr>
					<tr>
						<td id='td_recordatoriGuardarComanda' align='center' width='100%' valign='bottom'>
						</td>
					</tr>
				</table>
";
selUsuariEsProductorAssociatDelGrup($db);
echo "
				</td>
";
//*v36.5-columna html
	echo "
				<td align='right'>
				";
			if($mParametres['precomandaTancada']['valor']==1)
			{
				$imatgeSemafor='semafor_vermell.gif';
				$semaforText="Aquest periode de reserves est� TANCAT";
			}
			else
			{
				$imatgeSemafor='semafor_verd.gif';
				$semaforText="Aquest periode de reserves est� OBERT";
			}
			echo "
			<a title=\"".$semaforText."\"><img src='imatges/".$imatgeSemafor."'></a>
				</td>	
			</tr>
		</table>

		
		<table   bgcolor='#9CE6E3' bordercolor='#A2CBCA' border='0' width='100%' align='center'>
			<tr>
				<td  width='100%' valign='middle' align='left'>
				<table border='0' align='left' valign='top' style='width:100%;'>
					<tr>
						<td width='25%'  align='left' valign='top'>
						<p class='compacte'  style='font-size:16px;'>
						MAGATZEM-CAC:<b>&nbsp;".(urldecode($mGrup['nom']))." </b></p>
						<table width='100%'>
							<tr>
								<td>
	";
	if(isset($mPars['grup_id']) && $mPars['grup_id']!='0')
	{
		echo "
								<p class='nota'>
								Responsable:&nbsp;".(strtoupper(urldecode($mUsuarisGrupRef[$mGrup['usuari_id']]['usuari']['usuari'])))."
								<br>(".@$mUsuarisGrupRef[$mGrup['usuari_id']]['usuari']['email'].") 
								</p>
		";
	}
	echo "
								</td>
							</tr>
						</table>
						</td>

						<td width='25%' align='left' valign='middle'>
	";
	if($mPars['nivell']=='sadmin' ||$mPars['nivell']=='admin' || $mPars['nivell']=='coord')
	{
		selectorMagatzem();
	}
	echo "
						<p class='compacte' style='font-size:12px;'>
						<input type='checkbox' 	";

		if($mPars['veureProductesDisponibles']==1)
		{
				echo " CHECKED ";
		} 
		echo "
									id='ck_veureProductesDisponibles' onClick=\"f_veureProductesDisponibles();\" value='".$mPars['veureProductesDisponibles']."'> 
						Veure nom�s productes actius
						</p>
						<p style='font-size:12px;'>
						<input type='checkbox' 	";

		if($mPars['excloureProductesJaEntregats']==1)
		{
				echo " CHECKED ";
		} 
		echo "
									id='ck_excloureProductesJaEntregats' onClick=\"f_excloureProductesJaEntregats();\" value='".$mPars['excloureProductesJaEntregats']."'> 
						";
					if($mPars['grup_id']=='0')
					{
						echo "
						Excloure productes ja entregats<br> a <b>totes les Zones</b> (".(number_format($jaEntregatTkg,2))." kg, ".$jaEntregatTuts." uts)
						";
					}
					else
					{
						echo "
						Excloure productes ja entregats<br> a la zona <b>".$mCategoriesGrup[2]."</b> (".(number_format($jaEntregatTkg,2))." kg, ".$jaEntregatTuts." uts)
						";
					}
					
					echo "
						</p>
						<p style='font-size:13px; cursor:pointer;' onClick=\"enviarFpars('inventariPerProductors.php?','_blank');\" >
						<u>Qu� hi ha als magatzems CAC?</u>
						</p>
						</td>

						<td width='35%'>
						<p style='font-size:13px; cursor:pointer;' onClick=\"enviarFpars('vistaReservesSZ.php','_blank');\" >
						<u><b>recepcions</b> i <b>llistats interns d'abastiment</b></u>
						</p>

						<p style='font-size:13px; cursor:pointer;' onClick=\"enviarFpars('vistaInventari.php','_blank');\" >
						<u>full d'inventaris (amb els productes)</u>
						</p>

						<a style='font-size:13px;' href='fullInventarisBlanc.ods' >
						<u>full d'inventaris (en blanc)</u>
						</p>
						</td>
					</tr>
				</table>
				</td>
			</tr>

			<tr>
				<td width='100%'>
				<table   width='100%'>
					<tr>
						<td align='left' valign='bottom' width='30%'>
	";
	if($mPars['nivell']!='visitant')
	{
//*v36.5-funcio call
		mostrarSelectorRuta(0,'gestioMagatzems.php');
	}
	echo "
						</td>
						<td align='left' valign='bottom' width='70%'>
						<table style='width:100%;'>
							<tr>
								<td align='left' width='100%' valign='bottom'>
								<center><p style='font-size:11px; text-align:justify;'>* Es mostren els productes reservats de les comandes que tenen el punt d'entrega a la zona d'aquest magatzem,<br> segons els filtres seleccionats i independentment de la p�gina mostrada.<br>* Les reserves de productes especials es mostren pero no es comptabilitzen.</p></center>
								</td>
							</tr>
						</table>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	";

	return;
}


//-------------------------------------------------------
//-------------------------------------------------------
function html_paginacio($index)
{
	global $mPars,$mProductes,$mParametres,$mGrup;

	echo "
	<table  align='center'>
		<tr>
			<td style='width:50px;' align='center'>
			";
	$botoText="guardar inventari";
	if
	(
		$mPars['nivell']!='visitant'
		&& 
		$mPars['grup_id']!=0
		&&
		(
			$mPars['nivell']=='sadmin' || 	$mPars['nivell']=='admin' || $mPars['nivell']=='coord' || $mPars['usuari_id']==$mGrup['usuari_id']
		)
		&&
		($mPars['selRutaSufix']>=date('ym'))
	)
	{
		
		echo "	
				<table>
					<tr>
						<td align='center'>
						<table>	
							<tr>
								<td align='center'>
								<p>
		";
		if(($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord') && $mParametres['precomandaTancada']['valor'])
		{
			echo "
								<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";
		}
		echo "
								</p>
								</td>
							</tr>
						</table>
						</td>
						<td>
						<table id='t_img_guardar_".$index."'>
							<tr>
								<td align='center'>
								<p id='p_img_guardar_".$index."' style='text-decoration:blink; color:red;'></p>
								<a title='".$botoText."'><img src='imatges/guardarpp.gif' style='cursor:pointer;' onClick=\"guardarInventari();\" value='".$botoText."'></a></p>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
		";
	}
	echo "
			</td>
			<td>
			<p>p�gina:</p>
			</td>
			<td> 
			<img src='imatges/page_downp.gif' alt='seleccionar p�gina anterior' style='cursor:pointer;' onClick=\"javascript:avansarPag(-1);\">
			</td>
			<td>
			<select onChange=\"javascript:selectPag(this.value)\";>
	";
	$selected='';
	for($i=1;$i<=$mPars['numPags'];$i++)
	{
		if($mPars['pagS']*1==$i-1)
		{
			$selected='selected';
		}
		else
		{
			$selected='';
		}
		echo "
			<option ".$selected." value='".($i-1)."'>".$i."</option>
		";
	}	
	
	echo "
			</select>
			</td>
			<td>
			<img src='imatges/page_upp.gif' alt='seleccionar p�gina seg�ent' style='cursor:pointer;' onClick=\"javascript:avansarPag(1);\">
			</td>
			<td>
			<p>items/pag:<input type='text' size='2'  id='i_itemsPag_'  onChange=\"javascript: saveItemsPag(this.value);\" value='".$mPars['numItemsPag']."'></p>
			</td>
		</tr>
	</table>
	";


	return;
}


//-------------------------------------------------------
function html_mostrarFormProductesDistribucio()
{
	global  
			$db,
			$login,
			$valorMaxSegell,
			$mode,
			$thumbCode,
			$zona,

			$mColors,
			$mGrupsRef,
			$mCategoriesGrup,
			$mColsProductes,
			$mComanda,
			$mComandaCopiar,
			$mEtiquetes,
			$mFormesPagament,
			$mIntercanvisRebosts,
			$mIntercanvisRebost,
			$mInventariG,
			$mInventariGT,
			$mNomsColumnes,
			$mRebost,
			$mRutesSufixes,
			$mParametres,
			$mPars,
			$mParsCAC,
			$mProductes,
			$mProductors,
			$mPuntsEntrega,
			$mVocalsAccentSearch,
			$mVocalsAccentReplace,
			$mTotalKgComandaCac,
			$enviamentsPendents,
			$recepcionsPendents,
			$jaEntregatTkg,
			$jaEntregatTuts;

	$mErrors=array();
			
	$rebostTmp=str_replace(array('"',"'",' '),'',urlDecode($mRebost['nom']));
	$rebostTmp=str_replace($mVocalsAccentSearch,$mVocalsAccentReplace,$rebostTmp);
	$periodeComandaTmp=str_replace(array('-',":"),'',$mParametres['periodeComanda']['valor']);
	
	$disabledSelInventari='';
	
	$disabledSelInventari='disabled';
//*v36-3-12-15 call
	$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);
	$ultimPeriodeAccessible=$mRutesSufixes[(count($mRutesSufixes)-1)];
	if($mPars['selRutaSufixPeriode']==$ultimPeriodeAccessible)
	{
		$disabledSelInventari='';
	}
	
	
	//--------------------------------------------------------
	// comanda en curs ---------------------------------------
	// totals

	echo "
	<table  id='t_principal' align='center'  style=' width:100%;' bgcolor='".$mColors['table']."'>
		<tr>
			<td style=' width:95%;'  valign='top'>
			<table  align='center'  style=' width:100%;'>
				<tr>
					<td style='width:100%;'>
	";
	mostrarDadesMagatzem($db);				
	//mostrarDadesMagatzem2($db);				
			
	echo "
					<table bgcolor='#A4D781' border='0' style='width:100%'>
						<tr>
							<td bgcolor='#A4D781' style='width:100%' align='center'>
							<table border='0' width='100%'  >
							<tr>
								<td width='40%' align='center'  valign='top'>
	";
	if($mPars['grup_id']!='0')
	{
	
		echo "	
								<p><b>Operacions sobre l'Inventari<br>(magatzems-CAC):</b></p>
								<table border='1' width='100%' align='left'>
									<tr>
										<td  width='50%' align='left'>
										<p>&#9679; Operacions Internes locals (dins aquest magatzem-CAC):</p>
										<p style='color:#000000; cursor:pointer;' onClick=\"enviarFpars('movimentsEstocsTi.php?OG=TI&mRef=".$mPars['grup_id']."','_blank');\">
										<a title=\"Operaci� interna d'un magatzem CAC: es realitza quan s'ha realitzat un canvi de format sobre un producte. En fer-la es modifiquen els estocs local del producte d'origen i el del producte desti\">
										&nbsp;&nbsp;<u>Canvis de format</u>
										</a>
										</p>
										</td>
										<td>
										";
										if($mPars['usuari_id']==$mRebost['usuari_id'] ||  $mPars['nivell']=='sadmin')
										{
											echo "
										<p>
										<input type='checkBox' value='-1' UNCHECKED onClick=\"recordatoriGuardarComanda();val=document.getElementById('i_resetEstocDisponible').value;val=val*(-1);document.getElementById('i_resetEstocDisponible').value=val;\"> Reset inventari
										</p>
										</p>
										<p class='p_micro2'>* si est� seleccionat, quan guardem l'inventari posa a zero l'estoc de TOTS els productes actius de l'inventari (incloent els que no apareixen en aquesta p�gina), prescindint de la selecci� que hi hagi o la que acabem de fer, i marca com a sortida-distribuci� 'AUTO-INV' cada operaci� a l'historial.</p>
											";
										}
										echo "
										</td>
									</tr>
									<tr>
										<td  width='50%' align='left'>
										<p>&#9679; Operacions Internes globals (entre magatzems-CAC):</p>
										<p style='color:#000000; cursor:pointer;' onClick=\"enviarFpars('movimentsEstocsTe.php?OG=TE&mRef=".$mPars['grup_id']."','_blank');\">
										<a title=\"Operaci� interna, entre magatzems CAC: el magatzem d'on surt el producte es qui enregistra la transfer�ncia. En fer-la, es redueix l'estoc del magatzem local del producte transferit i s'incrementa l'estoc del magatzem receptor. El magatzem receptor ha de val.lidar la recepci� del producte\">
										&nbsp;&nbsp;<u>Transferencies de producte</u>
										</a>
										</p>
										</td>
										<td valign='middle'>
					<p class='p_micro2'> <b><font style='color:";
					if($recepcionsPendents>0){echo "red";}else{echo "#000000";}
					echo "; font-size:14px;'>[".$recepcionsPendents."]</font></b> recepcions que encara no has val.lidat
					</p>
					
					<p class='p_micro2'> <b><font style='color:";
					if($enviamentsPendents>0){echo "red";}else{echo "#000000";}
					echo "; font-size:14px;'>[".$enviamentsPendents."]</font></b> enviaments que encara no t'han val.lidat
					</p>
										</td>
									</tr>
									<tr>
										<td  width='50%' align='left'>
										<p>&#9679; Operacions Externes (no procedents d'un altre magatzem-CAC):</p>
										<p style='color:#000000; cursor:pointer;' onClick=\"enviarFpars('movimentsEstocsTeio.php?OG=TEIO&mRef=".$mPars['grup_id']."','_blank');\">
										<a title=\"Operacions externes dels magatzems CAC; per enregistrar entrades o sortides de producte. El magatzem al que entra o del que surt producte es qui enregistra l'entrada o sortida\">
										&nbsp;&nbsp;<u>Entrades i sortides</u>
										</a>
										</p>
								
										<p class='nota3'>
										*&nbsp;entrades(abastiment-recuperaci�)
										<br>
										&nbsp;&nbsp;sortides(distribuci�-p�rdua)
										</p>
										</td>
										<td>
										</td>
									</tr>
								</table>

		";
	}

	echo "
								</td>
								<td  align='center' valign='top'>
								<p><b>Resum de reserves:</b></p>
	";
	taulaResumComanda();	
	echo "
								</td>
								<td width='20%' align='center' valign='top'>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		<center><div id='d_noEstocNegatiu'></div></center>
		</td>
	</tr>

	<tr>
		<td style=' width:100%;' valign='top'>
		<table  border='0' bgcolor='#9EeC7B' style=' width:100%;'>
			<tr>
				<td width='80%' valign='top' align='center'>
	";
	taulaEinesGrups();
	echo "
				</td>
			</tr>
		</table>
		</td>
	</tr>

	<tr>
		<td style=' width:100%;'>
		<center>
		<div style=' width:100%;'>
		<table style=' width:100%;'>
			<tr>
				<td style=' width:33%;'>
			";
			html_paginacio(1);
			echo "
				</td>
				<td  style='width:33%;'>
				<p style='font-size:11px;'>(* <i>productes ordenats per <b>".$mPars['sortBy']."</b> - ".$mPars['ascdesc']."</i>)</p>
				</td>
				<td  style='width:33%;'>
				</td>
			</tr>
		</table>
		<table border='1'  align='center' valign='top' style=' width:100%;'>
	";

	if(count($mProductes)>0 && $mProductes[0]!=NULL)
	{	
			// nom columnes:--------------------------------
		$mColumnesVisibles=array();
		$mColumnesVisibles=$mColsProductes['visiblesMagatzems']; 
		$classTitolColumnes='titolColumnesCac';

		echo "
			<tr>
		";
		while(list($key,$val)=each($mProductes[0]))
		{	
			if(in_array($key,$mColumnesVisibles))
			{
				echo "
				<td bgcolor='#AEFC8B' valign='bottom'>
				<table  align='center' valign='top'>
					<tr>
						<th  align='center' valign='top'>
						";
						if($key=='id')
						{
							echo " 
						
						<p class='".$classTitolColumnes."'><b>".$mNomsColumnes[$key]."</b></p>
						".(html_ajuda1('html.php',31));
						}
						else if($key=='segell')
						{
							echo " 
						
						<p class='".$classTitolColumnes."'><b>".$mNomsColumnes[$key]."</b></p>
						".(html_ajuda1('html.php',32))."
							";
						}
						else
						{
							echo "
						<p class='".$classTitolColumnes."'>".$mNomsColumnes[$key]."</p>
							";
						}
						echo "
						<table align='center' valign='bottom'>
							<tr>
								<td >
								<p style='cursor:pointer;' onClick=\"sortBy('".$key."','DESC');\"><b>&darr;</b></p>
								</td>
								<td>
								<p style='cursor:pointer;' onClick=\"sortBy('".$key."','ASC');\"><b>&uarr;</b></p>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				";
			}
		}
		reset($mProductes[0]);
		if($mPars['grup_id']!='0')
		{
			echo "		
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:10%;'>
				<p class='".$classTitolColumnes."'>Reservat <font style='color:#ff0000;'>".$zona."</font></p>
				</th>

				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:10%;'>
				<p class='p_micro' style='color:blue;'>".(date('h:i:s d/m/Y',time()))."</p>
				
				<p class='".$classTitolColumnes."'>Inventari:</p>
				<p class='p_micro2'>".(urldecode($mRebost['nom']))."<br>(<font style='color:#ff0000;'>".$zona."</font>)
				<br>[".$mInventariG['data']."]<br><i>".(urldecode($mInventariG['autor']))."</i></p><br>
				</th>

				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:10%;'>
				<p class='".$classTitolColumnes."'>Inventari<br>TOTAL<br>CAC</p><br>
				</th>
			";
		}
		else
		{
			echo "		
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:10%;'>
				<p class='".$classTitolColumnes."'>Reservat <font style='color:#ff0000;'>total zones</font></p>
				</th>
				<td bgcolor='#AEFC8B'  align='center' valign='top' style='width:10%;'>
				<p class='p_micro' style='color:blue;'><b>".(date('h:i:s d/m/Y',time()))."</b></p>
				<p class='".$classTitolColumnes."'><b>Inventari<br> <font style='color:#ff0000;'>magatzems-CAC</font></b></p>
				<div style='height: 150px; overflow-y:auto;'>
				<p class='p_micro2'>
				<b>Inventaris</b>:<br>";
			
			$mInventariData=explode('}',$mInventariG['data']);
			
			$inventariData='';
			while(list($key,$inventariData_)=each($mInventariData))
			{
				if(isset($inventariData_) && $inventariData_!='')
				{
					$inventariData_=str_replace('{','',$inventariData_);
					$mInventariData_2=explode(';',$inventariData_);
					if(isset($mGrupsRef[$mInventariData_2[0]]))
					{
						$inventariData.=(urldecode($mGrupsRef[$mInventariData_2[0]]['nom'])).'<br>'.$mInventariData_2[1].'<br>'.$mInventariData_2[2].'<br><br>';
					}
				}
			}
			
			echo $inventariData."
				</p>			
				</div>
				</td>
			";
		}
			
		echo "
			</tr>
		";		
		
	// productes: -------------------------------------

		$contLinies=0;
		$blocLinies=6;
		$linies=1;
	
		while (list($key0,$val0)=each($mProductes))
		{	
			if($mProductes[$key0]['visible']==1)
			{
				//recordatori de noms de columna:
				if($contLinies==$blocLinies)
				{
					$contLinies=0;	
					echo "
			<tr>
					";
			
					while(list($key,$val)=each($mProductes[0]))
					{	
						if(in_array($key,$mColumnesVisibles))
						{
							echo "
			<th  bgcolor='#AEFC8B' align='center' valign='top'>
			<p class='".$classTitolColumnes."'><i>".$mNomsColumnes[$key]."</i></p>
			</th>
							";
						}
					}
					reset($mProductes[0]);

					echo "
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:10%;'>
				<p class='".$classTitolColumnes."'><i>Quant.<br> reservada <font style='color:#ff0000;'>".$zona."</font></p></i></p>
				</th>
					";
					if($mPars['grup_id']!='0')
					{
						echo "		
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:10%;'>
				<p class='p_micro' style='color:blue;'>".(date('h:i:s d/m/Y',time()))."</p>
				
				<p class='".$classTitolColumnes."'>Inventari:</p>
				<p class='p_micro2'>".(urldecode($mRebost['nom']))."<br>(<font style='color:#ff0000;'>".$zona."</font>)
				<br>[".$mInventariG['data']."]<br><i>".(urldecode($mInventariG['autor']))."</i></p><br>
				</th>
				</th>

				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:10%;'>
				<p class='".$classTitolColumnes."'>Inventari<br>TOTAL<br>CAC</p><br>
				</th>
						";
					}
					else
					{
						echo "		
				<td bgcolor='#AEFC8B'  align='center' valign='top' style='width:10%;'>
				<p class='p_micro' style='color:blue;'><b>".(date('h:i:s d/m/Y',time()))."</b></p>
				<p class='".$classTitolColumnes."'><b>Inventari<br> <font style='color:#ff0000;'>magatzems-CAC</font></b></p>
				</div>
				</td>
						";
					}
					echo "
			</tr>
					";
				}

				if($mPars['grup_id']=='0' && ($mProductes[$key0]['estoc_disponible']>$mProductes[$key0]['estoc_previst'] || $mProductes[$key0]['estoc_disponible']<0))
				{
					$colorAlerta='#ff0000';
				}
				else
				{
					$colorAlerta='#000000';
				}
				echo "
			<tr id='tr_filaProducte_".$key."'>
				";
				while(list($key,$val)=each($mProductes[0]))
				{	
					if(in_array($key,$mColumnesVisibles))
					{
						echo "
				<td  id='td_producte".$key."_".$key0."' style='width:10%;'>
						";
				
						if($key=='segell')
						{
							echo " 
				<table>
					<tr>
						<td align='center'>
						<p class='".$classTitolColumnes."' style='color:".$colorAlerta.";'>
						<b>".(number_format($mProductes[$key0][$key],1,'.',''))."&nbsp;%</b>
						</p>
						</td>
					</tr>
				</table>
							";
						}
						else if($key=='estoc_previst')
						{
							echo " 
				<table>
					<tr>
						<td>
						<p class='".$classTitolColumnes."' style='color:".$colorAlerta.";'>
						".$mProductes[$key0][$key]."
						</p>
						</td>
					</tr>
				</table>
							";
						}
						else if($key=='estoc_disponible')
						{
							echo " 
				<table>
					<tr>
						<td>
						<p class='".$classTitolColumnes."' style='color:".$colorAlerta.";'>
						<b>".$mProductes[$key0][$key]."</b>
						</p>
						</td>
					</tr>
				</table>
							";
						}
						else if($key=='producte')
						{
							echo " 
				<div  style='position:relative; z-index:0; top:0px; left:0px;'>
				<div style='position:relative; z-index:0;'>
				<table style='width:100%;'>
					<tr>
						<td style='width:10%;'>
						<table style='width:100%;'>
							<tr>
								<td align='left' style='width:100%;'>	
							";
								//<p class='".$classTitolColumnes."' style='cursor:pointer; color:#3E4B8D;' onClick=\"javascript:cursor=getMouse(event); mostrarTaulaFlotant(cursor.x,cursor.y,'".$mProductes[$key0]['id']."','".$key."','".$key0."',1);\" >[<u>info</u>]
								
							echo "
								</p>
								<p class='".$classTitolColumnes."'>								
								".(urldecode($mProductes[$key0][$key]))."
								</p>
							";
							if($mProductes[$key0]['imatge']!='')
							{
								echo "
								<img src='productes/".$mProductes[$key0]['id']."/".$thumbCode."_".$mProductes[$key0]['imatge']."'>
								";
							}
							echo "
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</div>
				<div  id='d_producte".$key."_".$key0."'  style='position:absolute; z-index:1; top:-30px; left:150px; width:300px; visibility:hidden;'>
				</div>
				</div>
							";
						} 
						else if($key=='productor' || $key=='categoria0' || $key=='categoria10' )
						{
							echo " 
				<table >
					<tr>
						<td >
						<p   class='".$classTitolColumnes."'>
						".(urldecode($mProductes[$key0][$key]))."
						</p>
						</td>
					</tr>
				</table>
							";
						} 
						else if($key=='tipus' || $key=='unitat_facturacio')
						{
							echo " 
				<table >
					<tr>
						<td >
						<p   class='".$classTitolColumnes."'>
						".(posaColor1(urldecode($mProductes[$key0][$key])))."
						</p>
						</td>
					</tr>
				</table>
							";
						} 
						else if($key=='id' )
						{
							if($mProductes[$key0]['actiu']=='0'){$actiuText="<p class='pVinactiu'>INACTIU</p>";}else{$actiuText='';}
							echo " 
				<table>
					<tr>
						<td>
						<p class='".$classTitolColumnes."'>".(urldecode($mProductes[$key0][$key]))."</p>
						</td>
						<td>
						".$actiuText."
						</td>
					</tr>
				</table>
							";
						}
						else if(in_array($key,$mColsProductes['visiblesMagatzems']))
						{	
							echo " 
				<table>
					<tr>
						<td>
						<p class='".$classTitolColumnes."'>".$mProductes[$key0][$key]."</p>
						</td>
					</tr>
				</table>
							";
						}
						echo "
				</div>
				</td>
						";
					}
				}
				reset($mProductes[0]);  

				if($mProductes[$key0]['estoc_previst']!=0){$limitq=200;}else{$limitq=0;}
		
				if(substr_count($mProductes[$key0]['tipus'], 'especial')>0)
				{
					echo "
				<td id='td_producteQuantitat_".$key0."'>
				<p  class='".$classTitolColumnes."'>".$mProductes[$key0]['quantitat']."</p>
				</td>

				<td   id='td_producteInventari_".$key0."'>
				<p  class='".$classTitolColumnes."'>0</p>
				<font id='f_difR_".$key0."' style='font-size:11px; color:#000000;'></font>
				<font id='f_difD_".$key0."' style='font-size:11px; color:#000000;'></p>
				</td>
					";
				}
				else
				{
					echo "
				<td id='td_producteQuantitat_".$key0."'>
				<p  class='".$classTitolColumnes."'>".$mProductes[$key0]['quantitat']."</p>
				</td>

				<td   id='td_producteInventari_".$key0."'>
				<p  class='".$classTitolColumnes."'>
					";
				
					if(!isset($mInventariG['inventariRef'][$mProductes[$key0]['id']]) || $mInventariG['inventariRef'][$mProductes[$key0]['id']]==''){$mInventariG['inventariRef'][$mProductes[$key0]['id']]=0;}
					
					if($mPars['grup_id']!='0')
					{
						if(!isset($mInventariGT['inventariRef'][$mProductes[$key0]['id']]) || $mInventariGT['inventariRef'][$mProductes[$key0]['id']]==''){$mInventariGT['inventariRef'][$mProductes[$key0]['id']]=0;}
					
						echo "
				<select ".$disabledSelInventari." onChange=\"selectProducte('".$key0."');\" id='sel_quantitat_".$key0."' name='sel_quantitat_".$key0."'>
						";
				
						//aix� es inventari, no comandes ni reserves. Nom�s quantitats positives
					
						for($j=0;$j<=$mParametres['limitUtsSelectorProductes']['valor'];$j+=$mProductes[$key0]['format'])
						{
							if($mInventariG['inventariRef'][$mProductes[$key0]['id']]==$j){$selected='selected';}else{$selected='';}
							echo "
				<option ".$selected." value='".$j."'>".$j."</option>
							";
						}
						echo "
				</select>
						";
						$difR=$mInventariG['inventariRef'][$mProductes[$key0]['id']]-$mProductes[$key0]['quantitat'];
						if($difR<0){$difColor='#ff0000';}else if($difR==0){$difColor='#00aa00';}else{$difColor='#0000ff';}
						echo "
				<br>
				<font id='f_difR_".$key0."' style='font-size:11px; color:#000000;'>difR:</font><font style='color:".$difColor.";'><b>".$difR."</b></font></p>
				</td>
						";
						//inventari Total si es mostra un magatzem
						echo "
				<td   id='td_producteInventariT_".$key0."'>
				<p  class='".$classTitolColumnes."'>".$mInventariGT['inventariRef'][$mProductes[$key0]['id']]."	</p>
				</td>
						";
					}
					else //cac global
					{
						echo "
				<p  class='".$classTitolColumnes."'>".$mInventariG['inventariRef'][$mProductes[$key0]['id']]."
						";
						$difR=$mInventariG['inventariRef'][$mProductes[$key0]['id']]-$mProductes[$key0]['quantitat'];
						if($difR<0){$difColor='#ff0000';}else if($difR==0){$difColor='#00aa00';}else{$difColor='#0000ff';}
						echo "
				<br>
				<font id='f_difR_".$key0."' style='font-size:11px; color:#000000;'>difR:</font><font style='color:".$difColor.";'><b>".$difR."</b></font>
						";
						$difD=$mInventariG['inventariRef'][$mProductes[$key0]['id']]-$mProductes[$key0]['estoc_disponible'];
						if($difD<0){$difColor='#ff0000';}else if($difD==0){$difColor='#00aa00';}else{$difColor='#0000aa';}
						echo "
				<br>
				<font id='f_difD_".$key0."' style='font-size:11px; color:#000000;'>difD:</font><font style='color:".$difColor.";'><b>".$difD."</b></font></p>
						";
						if(($mProductes[$key0]['estoc_previst']-$mProductes[$key0]['estoc_disponible'])!=$mProductes[$key0]['quantitat'])
						{
							$mErrors[$mProductes[$key0]['id']]['producte']=$mProductes[$key0]['producte'];
							$mErrors[$mProductes[$key0]['id']]['estoc_previst']=$mProductes[$key0]['estoc_previst'];
							$mErrors[$mProductes[$key0]['id']]['estoc_disponible']=$mProductes[$key0]['estoc_disponible'];
							$mErrors[$mProductes[$key0]['id']]['reservat']=$mProductes[$key0]['quantitat'];
							$mErrors[$mProductes[$key0]['id']]['error']=$mProductes[$key0]['estoc_previst']-$mProductes[$key0]['estoc_disponible']-$mProductes[$key0]['quantitat'];
						}
					}
					echo "
				</td>
					";
				}
			//-------- forma de calcul copiada a vistaAlbara
			if($mParsCAC['quantitat_total']==0)
			{
				$producteCtUnitats=0;
				$producteCtEcos=0;
				$producteCtEuros=0;
			}
			else
			{
				$producteCtUnitats=$mProductes[$key0]['pes']*$mProductes[$key0]['quantitat']*($mProductes[$key0]['cost_transport_extern_kg']+$mProductes[$key0]['cost_transport_intern_kg']+($mParametres['cost_transport_extern_repartit']['valor']/($mParsCAC['quantitat_total']+$mParsCAC['kg_intercanvisRebosts']))+($mParametres['cost_transport_intern_repartit']['valor']/($mParsCAC['quantitat_total']+$mParsCAC['kg_intercanvisRebosts'])));
				$producteCtEcos=
				$mProductes[$key0]['pes']*$mProductes[$key0]['quantitat']*
				(
					$mProductes[$key0]['cost_transport_extern_kg']	*	$mProductes[$key0]['ms_ctek']/100
					+$mProductes[$key0]['cost_transport_intern_kg']	*	$mProductes[$key0]['ms_ctik']/100
					+($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100)
						/($mParsCAC['quantitat_total']+$mParsCAC['kg_intercanvisRebosts'])
					+($mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)
						/($mParsCAC['quantitat_total']+$mParsCAC['kg_intercanvisRebosts'])
				);
				$producteCtEuros=$producteCtUnitats-$producteCtEcos;
			}
			echo "
			</tr>
			";
		
			$contLinies+=1;
			}
		}
		reset($mProductes);
	}
	else
	{
		echo "
		<center><p>No hi ha productes en aquesta recerca</p></center>
		";
	}

	echo "		
		</table>
		<table style=' width:100%;'>
			<tr>
				<td style=' width:33%;'>
			";
			html_paginacio(2);
			echo "
				</td>
				<td  style='width:33%;'>
				<p style='font-size:11px;'>(* <i>productes ordenats per <b>".$mPars['sortBy']."</b> - ".$mPars['ascdesc']."</i>)</p>
				</td>
				<td  style='width:33%;'>
				</td>
			</tr>
		</table>
		</div>
		</center>
		</td>
	</tr>
</table>
	";
	//si cac: mostrar errors
	if($mPars['grup_id']=='0' && count($mErrors)>0)
	{
		echo "
		
<table style='width:60%;' align='center'>
	<tr>
		<td style='width:100%;' align='center'>
		<p style='color:red;'>Detectat possible error de 'estoc_disponible' en els seg�ents productes:</p>
		</td>
	</tr>
</table>
<table border='1' style=' width:70%;' align='center'>
	<tr>
		<th>
		<p style='color:red;'>id</p>
		</th>
		<th>
		<p style='color:red;'>producte</p>
		</th>
		<th>
		<p style='color:red;'>estoc_previst</p>
		</th>
		<th>
		<p style='color:red;'>estoc_disponible</p>
		</th>
		<th>
		<p style='color:red;'>reservat</p>
		</th>
		<th>
		<p style='color:red;'>difer�ncia error</p>
		</th>
	</tr>
		";
		while(list($idProducte,$difError)=each($mErrors))
		{
			echo "
	<tr>
		<td>
		<p style='color:red;'>".$idProducte."</p>
		</td>
		<td>
		<p style='color:red;'>".(urldecode($mErrors[$idProducte]['producte']))."</p>
		</td>
		<td>
		<p style='color:red;'>".$mErrors[$idProducte]['estoc_previst']."</p>
		</td>
		<td>
		<p style='color:red;'>".$mErrors[$idProducte]['estoc_disponible']."</p>
		</td>
		<td>
		<p style='color:red;'>".$mErrors[$idProducte]['reservat']."</p>
		</td>
		<td>
		<p style='color:red;'>".$mErrors[$idProducte]['error']."</p>
		</td>
	</tr>
			";
		}
		reset($mErrors);
		echo "
</table>
<br>
<table style='width:60%;' align='center'>
	<tr>
		<td style='width:100%;' align='center'>
		<p class='p_micro2' style='color:red;'>* una vegada fetes totes les comprovacions desde la fitxa de cada producte 
		(cal 'guardar' des d'all� si hi ha errors), els productes que quedin en aquesta taula provablement 
		siguin els que estan marcats com a ja entregats, els quals no es mostren a la llista, si est� 
		seleccionada l'opcio 'no mostrar els productes ja entregats'.
		</p>
		</td>
	</tr>
</table>
<br>
		";	
	}
	return;
}

//------------------------------------------------------------------------------
function html_mostrarNotesProductesDistribucio($magatzemRef)
{
	if($magatzemRef!='0')
	{
		echo "
	<table style='width:70%;' align='center'>
		<tr>
			<td style='width:100%;' align='center'>
			<p class='nota2'><u>Notes:</u></p>
			<p class='nota2'>
			* <b>DifR</b>:  (valor mostrat per cada producte a la columna 'Inventari'). 
			Indica la diferencia entre el valor d'inventari(real) i les reserves
			d'aquest producte que s'entregaran en la zona d'aquest magatzem. 
			Es �til en acabar el periode de precomandes, quan les comandes estan
			vinculades a zones d'entrega i volem saber quins productes reservats
			haurien d'estar ubicats a cada magatzem (o zona), si volgu�ssim preparar 
			les comandes all�. 
			<br>Si en el moment de preparar les comandes a cada magatzem, DifR fos 0 
			per cada producte reservat, significaria que ja no cal moure cap 
			producte per fer la distribuci�.</p>
			</td>
		</tr>
	</table>
		";
	}
	else
	{
		echo "
	<table style='width:70%;' align='center'>
		<tr>
			<td style='width:100%;' align='center'>
			<p class='nota2'><u>Notes:</u></p>
			
			<p class='nota2'>* <b>DifD</b>:  (valor mostrat per cada producte a la columna 'Inventari'). 
			Indica la diferencia entre el valor total d'inventari(real) i l'estoc disponible(gestor). 
			Es �til en el tancament de la ruta, despr�s de la distribuci�, per observar la diferencia 
			entre els estocs te�rics (producte no reservat del gestor) i la suma dels estocs reals 
			(productes dels inventaris dels magatzems)
			<br>
			Si DifD<0, significa que hi ha menys producte del que hi hauria d'haver en finalitzar la ruta.
			<br>
			</p>
			
			<p class='nota2'>* <b>DifR</b>:  (valor mostrat per cada producte a la columna 'Inventari'). 
			Indica la diferencia entre el valor d'inventari(real) i les reserves
			d'aquest producte que s'entregaran en la zona d'aquest magatzem. 
			Es �til en acabar el periode de precomandes, quan les comandes estan
			vinculades a zones d'entrega i volem saber quins productes reservats
			haurien d'estar ubicats a cada magatzem (o zona), si volgu�ssim preparar 
			les comandes all�. 
			<br>Si en el moment de preparar les comandes a cada magatzem, DifR fos 0 
			per cada producte reservat, significaria que ja no cal moure cap 
			producte per fer la distribuci�.
			</p>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<br>
			<p class='nota2'>
			Per tant:
			<br>
			1/ si el periode de precomandes esta tancat, i DifD<0, la cel.la apareix aixi:
			</p>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<table align='left'>
				<tr>
					<td width='50px' height='50px'  style='background-color:#FA897A;'>
					<p class='nota2'>
					<font style='background-color:#Ff0000; color:#ffffff;'><b>DifD:</b></font>valor
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<p class='nota2'>
			2/ si el periode de precomandes esta obert, i DifR<0, la cel.la apareix aixi:
			</p>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<table align='left'>
				<tr>
					<td width='50px' height='50px'  style='background-color:#FA897A;'>
					<p class='nota2'>
					<font style='background-color:#Ff0000; color:#ffffff;'><b>DifR:</b></font>valor
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<p class='nota2'>
			* en els casos anteriors, si el producte es de tipus dip�sit, i la CAC no n'ha de tenir estoc, el color de fons de la cel.la es m�s clar:
			</p>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<table align='left'>
				<tr>
					<td width='50px' height='50px'  style='background-color:#F9DAD6;'>
					
					<p class='nota2'>
					<font style='background-color:#Ff0000; color:#ffffff;'><b>DifX:</b></font>valor
					</p>
					</td>
				</tr>
			</table>

			</td>
		</tr>
	</table>
		";
	}

	return;
}


				
				
?>

		