<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "eines.php";
include "html_ajuda1.php";
include "db_ajuda.php";
$mParsCAC=array();
$mMissatgeAlerta['missatge']='';
$mMissatgeAlerta['result']='';

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['excloureProductesJaEntregats']='0';
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';

$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}
$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];

getConfig($db); //inicialitza variables anteriors;

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=0;
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';

	$mPars['fPagamentUms']=0;
	$mPars['fPagamentEcos']=0;
	$mPars['fPagamentEb']=0;
	$mPars['fPagamentEu']=0;
	$mPars['fPagamentEuMetalic']=0;
	$mPars['fPagamentEuTransf']=0;
	$mPars['fPagamentEcosIntegralCES']=0;

	$mPars['fPagamentEcosPerc']=0;
	$mPars['fPagamentEbPerc']=0;
	$mPars['fPagamentEuPerc']=0;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['paginacio']=-1;
	if(!isset($mPars['pagS'])){$mPars['pagS']=0;}
	if(!isset($mPars['pagFiltre'])){$mPars['pagFiltre']=$mPars['sortBy'].$mPars['veureProductesDisponibles'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vProductor'].$mPars['vSubCategoria'].$mPars['vCategoria'];}
	if(!isset($mPars['numProdsPag'])){$mPars['numProdsPag']=10;}
	if(!isset($mPars['numPags'])){$mPars['numPags']=0;} 

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['comandesUsuaris.php']=db_getAjuda('comandesUsuaris.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$propietatUsuari=@$_GET['prop'];
if(!isset($propietatUsuari)){$propietatUsuari=$mPars['propietatUsuari'];}
else{$mPars['propietatUsuari']=$propietatUsuari;}

$mUsuarisPropietatRef=db_getUsuarisPropietat($propietatUsuari,$db);
$mProductes=db_getProductes2($db);
$mUsuarisRef=db_getUsuarisRef($db);
$mGrupsRef=db_getGrupsRef($db);
$mUsuarisAmbComandaResum=db_getUsuarisAmbComandaResum($db);
$mUsuarisAmbComandaEspResum=db_getUsuarisAmbComandaEspResum($db);
$mRutesSufixes=getRutesSufixes($db);
//*v36 5-1-16 assignacio
$mPeriodesInfo=db_getPeriodesInfo($db);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>"; echo $htmlTitolPags; echo " - Comandes usuaries</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
ruta=".$ruta.";
calGuardar=false;
</SCRIPT>
</head>
<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('comandesUsuaris.php?');
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			</td>
		</tr>
	</table>

<center><p style='font-size:16px;'>[ Ruta <b>".$mPars['selRutaSufix']."</b>: Resum Comandes Usuaries]</p></center>
";
//v36.5-funcio call
mostrarSelectorRuta(1,'comandesUsuaris.php');
echo "
<table width='80%' align='center'>
	<tr>
		<td id='td_missatgeAlerta' style=' width:100%;' align='center'  valign='bottom'>
		".$mMissatgeAlerta['missatge']."
		</td>
	</tr>
</table>
<center><p class='p_nota'>* es mostren les comandes de les usuaries amb la propietat: '<b>".$propietatUsuari."</b>'</p></center>
<center><p class='p_nota'>* es comptabilitzen els productes especials</p></center>

<table border='1' style='width:80%;' align='center'  bgcolor='".$mColors['table']."'>
";
ksort($mUsuarisAmbComandaResum);

$rM3=0;; //M3 recolzada;
$rEcos=0;; //ecos recolzats

$umsT=0;
$ecosT=0;
$m3T=0;
$eurosT=0;

$umsEspT=0;
$ecosEspT=0;
$m3EspT=0;
$eurosEspT=0;

$umsTT=0;
$ecosTT=0;
$m3TT=0;
$eurosTT=0;

$umsEspTT=0;
$ecosEspTT=0;
$m3EspTT=0;
$eurosEspTT=0;

$productesUmsT=0;
$productesEcosT=0;
$productesEurosT=0;
$productesCtUmsT=0;
$productesCtEurosT=0;
$productesFdUmsT=0;
$productesFdEurosT=0;

$productesEspUmsT=0;
$productesEspEcosT=0;
$productesEspEurosT=0;
$productesEspCtUmsT=0;
$productesEspCtEurosT=0;
$productesEspFdUmsT=0;
$productesEspFdEurosT=0;

$productesUmsTT=0;
$productesEcosTT=0;
$productesEurosTT=0;
$productesCtUmsTT=0;
$productesCtEurosTT=0;
$productesFdUmsTT=0;
$productesFdEurosTT=0;

$productesEspUmsTT=0;
$productesEspEcosTT=0;
$productesEspEurosTT=0;
$productesEspCtUmsTT=0;
$productesEspCtEurosTT=0;
$productesEspFdUmsTT=0;
$productesEspFdEurosTT=0;

$cont=1;
$cont2=1;
		echo "
	<tr>
		<th valign='top'>
		<p>nombre<br>d'usuaries</p>
		</th>

		<th valign='top'>
		<p>nombre<br>de<br>comandes</p>
		</th>

		<th valign='top'>
		<p>usuari</p>
		</th>

		<th valign='top'>
		<p>comissi�</p>
		</th>

		<th valign='top'>
		<p>grup</p>
		</th>

		<th valign='top'>
		<p>ums<br>(forma pagament)</p>
		</th>
		
		<th valign='top'>
		<p>ecos<br>(forma pagament)</p>
		</th>
		";
		
		if($mParametres['moneda3Activa']['valor']=='1')
		{
			echo "
		<th valign='top'>
		<p>".$mParametres['moneda3Nom']['valor']."<br>(forma pagament)</p>
		</th>";
		}
		
		echo "
		<th valign='top'>
		<p>euros<br>(forma pagament)</p>
		</th>

		<th valign='top'>
		<p>ums<br>(productors)</p>
		</th>

		<th valign='top'>
		<p>ecos<br>(productors)</p>
		</th>

		<th valign='top'>
		<p>euros<br>(productors)</p>
		</th>

		";
		
		if($mParametres['moneda3Activa']['valor']=='1')
		{
			echo "
		<th valign='top'>
		<p>".$mParametres['moneda3Nom']['valor']."<br>recolzat</p>
		</td>
			";
		}

		echo "
		<th valign='top'>
		<p>ecos<br>recolzats</p>
		</th>

	</tr>
			";	
			
	while(list($usuariId,$mComandes)=each($mUsuarisAmbComandaResum))
	{
		while(list($grupId,$mComanda)=each($mComandes))
		{
			$ums=0;
			$ecos=0;
			$m3=0;
			$euros=0;

			$umsEsp=0;
			$ecosEsp=0;
			$m3Esp=0;
			$eurosEsp=0;
			
			$productesUms=0;
			$productesEcos=0;
			$productesEuros=0;
			$productesCtUms=0;
			$productesCtEuros=0;
			$productesFdUms=0;
			$productesFdEuros=0;
			
			$productesEspUms=0;
			$productesEspEcos=0;
			$productesEspEuros=0;
			$productesEspCtUms=0;
			$productesEspCtEuros=0;
			$productesEspFdUms=0;
			$productesEspFdEuros=0;

			$umsT=0;
			$ecosT=0;
			$m3T=0;
			$eurosT=0;

			$umsEspT=0;
			$ecosEspT=0;
			$m3EspT=0;
			$eurosEspT=0;
			
			$productesUmsT=0;
			$productesEcosT=0;
			$productesEurosT=0;
			$productesCtUmsT=0;
			$productesCtEurosT=0;
			$productesFdUmsT=0;
			$productesFdEurosT=0;
			
			$productesEspUmsT=0;
			$productesEspEcosT=0;
			$productesEspEurosT=0;
			$productesEspCtUmsT=0;
			$productesEspCtEurosT=0;
			$productesEspFdUmsT=0;
			$productesEspFdEurosT=0;
            
			
			//productes segons f pagament usuari
			$mFormaPagament=explode(',',$mComanda['f_pagament']);
			if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
			if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
			if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
			if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
			if(!isset($mFormaPagament[4])){$mFormaPagament[4]=1;}
			if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
			if(!isset($mFormaPagament[6])){$mFormaPagament[6]=0;}
			if(!isset($mFormaPagament[7])){$mFormaPagament[7]=0;}
			if(!isset($mFormaPagament[8])){$mFormaPagament[8]=0;}
							
			$totalUms=$mFormaPagament[0]+$mFormaPagament[1]+$mFormaPagament[2];

			$ums+=$totalUms;
			$ecos+=$mFormaPagament[0];
			$m3+=$mFormaPagament[1];
			$euros+=$mFormaPagament[2];

			//productes segons % productor
			$mResum=explode(';',$mComanda['resum']);
			while(list($key,$val)=each($mResum))
			{
				$producteId=substr($val,strpos($val,'_')+1);
				$producteId=substr($producteId,0,strpos($producteId,':'));
				
				$quantitat=substr($val,strpos($val,':')+1);
				
				if(isset($mProductes[$producteId]))
				{
					$productesUms+=$mProductes[$producteId]['preu']*$quantitat;
					$productesEcos+=$mProductes[$producteId]['preu']*$quantitat*$mProductes[$producteId]['ms']/100;
					$productesEuros+=$mProductes[$producteId]['preu']*$quantitat*(100-$mProductes[$producteId]['ms'])/100;
					$productesCtUms+=$mProductes[$producteId]['pes']*$quantitat*$mParametres['varCostTransport']['valor'];
					$productesCtEuros+=$mProductes[$producteId]['pes']*$quantitat*$mParametres['varCostTransport']['valor']*(100-$mProductes[$producteId]['ms'])/100;
					$productesFdUms+=$mProductes[$producteId]['preu']*$quantitat*$mParametres['FDCpp']['valor']/100;
					$productesFdEuros+=$mProductes[$producteId]['preu']*$quantitat*($mParametres['FDCpp']['valor']/100)*(100-$mProductes[$producteId]['ms'])/100;
				}
			}
	
			
			//productes especials
			if(isset($mUsuarisAmbComandaEspResum[$usuariId][$grupId]))
			{
				//productes especials segons f pagament usuari
				$mFormaPagament=explode(',',$mComanda['f_pagament']);
				if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
				if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
				if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
				if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
				if(!isset($mFormaPagament[4])){$mFormaPagament[4]=1;}
				if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
				if(!isset($mFormaPagament[6])){$mFormaPagament[6]=0;}
				if(!isset($mFormaPagament[7])){$mFormaPagament[7]=0;}
				if(!isset($mFormaPagament[8])){$mFormaPagament[8]=0;}
							
				$totalUms=$mFormaPagament[0]+$mFormaPagament[1]+$mFormaPagament[2];
				
				$umsEsp+=$totalUms;
				$ecosEsp+=$mFormaPagament[0];
				$m3Esp+=$mFormaPagament[1];
				$eurosEsp+=$mFormaPagament[2];
				
				$mResum=explode(';',$mUsuarisAmbComandaEspResum[$usuariId][$grupId]['resum']);
				while(list($key,$val)=each($mResum))
				{
					$producteId=substr($val,strpos($val,'_')+1,strpos($val,':'));
					$quantitat=substr($val,strpos($val,':')+1);
					if(isset($mProductes[$producteId]))
					{
						$productesEspUms+=$mProductes[$producteId]['preu']*$quantitat;
						$productesEspEcos+=$mProductes[$producteId]['preu']*$quantitat*$mProductes[$producteId]['ms']/100;
						$productesEspEuros+=$mProductes[$producteId]['preu']*$quantitat*(100-$mProductes[$producteId]['ms'])/100;
						$productesEspFdUms+=$mProductes[$producteId]['preu']*$quantitat*$mParametres['FDCpp']['valor']/100;
						$productesEspFdEuros+=$mProductes[$producteId]['preu']*$quantitat*((100-$mProductes[$producteId]['ms'])/100)*$mParametres['FDCpp']['valor']/100;
					}
				}
			}
		
			$umsT+=$ums;
			$ecosT+=$ecos;
			$m3T+=$m3;
			$eurosT+=$euros;

			$umsEspT+=$umsEsp;
			$ecosEspT+=$ecosEsp;
			$m3EspT+=$m3Esp;
			$eurosEspT+=$eurosEsp;
				
			$productesUmsT+=$productesUms;
			$productesEcosT+=$productesEcos;
			$productesEurosT+=$productesEuros;
			$productesCtUmsT+=$productesCtUms;
			$productesCtEurosT+=$productesCtEuros;
			$productesFdUmsT+=$productesFdUms;
			$productesFdEurosT+=$productesFdEuros;

			$productesEspUmsT+=$productesEspUms;
			$productesEspEcosT+=$productesEspEcos;
			$productesEspEurosT+=$productesEspEuros;
			$productesEspCtUmsT+=$productesEspCtUms;
			$productesEspCtEurosT+=$productesEspCtEuros;
			$productesEspFdUmsT+=$productesEspFdUms;
			$productesEspFdEurosT+=$productesEspFdEuros;
			
			$mPropietats=getPropietats($mUsuarisPropietatRef[$usuariId]['propietats']);
			echo "
	<tr>
		<td>
		<p><b>".$cont2."</b></p>
		</td>

		<td>
		<p><b>".$cont."</b></p>
		</td>

		<td>
		<p>".(urldecode($mUsuarisPropietatRef[$usuariId]['usuari']))."<br>".$mUsuarisPropietatRef[$usuariId]['email']."</p>
		</td>

		<td>
		<p>".@$mPropietatsUsuaris[$propietatUsuari][$mPropietats[$propietatUsuari]]."</p>
		</td>
		
		<td>
		<p>".(urldecode($mGrupsRef[$grupId]['nom']))."</p>
		</td>

		<td>
		<p>".(number_format(($umsT+$umsEspT),'2','.',''))."</p>
		</td>
		
		<td>
		<p>".(number_format(($ecosT+$ecosEspT),'2','.',''))."</p>
		</td>
		";
		
		if($mParametres['moneda3Activa']['valor']=='1')
		{
			echo "
		<td>
		<p>".(number_format(($m3T+$m3EspT),'2','.',''))." ".$mParametres['moneda3Abrev']['valor']."</p>
		</td>";
		}
		
		echo "

		<td>
		<p>".(number_format(($eurosT+$eurosEspT),'2','.',''))."</p>
		</td>

		<td>
		<p>".(number_format(($productesUmsT+$productesEspUmsT+$productesCtUmsT+$productesFdUmsT+$productesEspFdUmsT),'2','.',''))."</p>
		</td>

		<td>
		<p>".(number_format(($productesEcosT+$productesEspEcosT),'2','.',''))."</p>
		</td>

		<td>
		<p>".(number_format(($productesEurosT+$productesCtEurosT+$productesFdEurosT+$productesEspEurosT+$productesEspFdEurosT),'2','.',''))."</p>
		</td>
		";
		
		if($mParametres['moneda3Activa']['valor']=='1')
		{
			$m3Recolzats=$m3T+$m3EspT;
		if($m3Recolzats>0){$color='red';}else {$color='green';}
			echo "
		<td>
		<p style='color:".$color."';>".(number_format(($m3Recolzats),'2','.',''))."</p>
		</td>
			";
		}

		$ecosRecolzats=($ecosT+$ecosEspT)-($productesEcosT+$productesEspEcosT);
		if($ecosRecolzats>0){$color='red';}else{$color='green';}
		
		echo "
		<td>
		<p style='color:".$color."';>".(number_format($ecosRecolzats,'2','.',''))."</p>
		</td>

	</tr>
			";

			$umsTT+=$umsT;
			$ecosTT+=$ecosT;
			$m3TT+=$m3T;
			$eurosT+=$eurosT;

			$umsEspTT+=$umsEspT;
			$ecosEspTT+=$ecosEspT;
			$m3EspTT+=$m3EspT;
			$eurosTT+=$eurosT;
			$eurosEspTT+=$eurosEspT;

			$productesUmsTT+=$productesUmsT;
			$productesEcosTT+=$productesEcosT;
			$productesEurosTT+=$productesEurosT;
			$productesCtUmsTT+=$productesCtUmsT;
			$productesCtEurosTT+=$productesCtEurosT;
			$productesFdUmsTT+=$productesFdUmsT;
			$productesFdEurosTT+=$productesFdEurosT;

			$productesEspUmsTT+=$productesEspUmsT;
			$productesEspEcosTT+=$productesEspEcosT;
			$productesEspEurosTT+=$productesEspEurosT;
			$productesEspCtUmsTT+=$productesEspCtUmsT;
			$productesEspCtEurosTT+=$productesEspCtEurosT;
			$productesEspFdUmsTT+=$productesEspFdUmsT;
			$productesEspFdEurosTT+=$productesEspFdEurosT;

			$rM3+=$m3T+$m3EspT;
			$rEcos+=($ecosT+$ecosEspT)-($productesEcosT+$productesEspEcosT);
			$cont++;
		}
		reset($mComandes);
		
		$cont2++;
	}
	reset($mUsuarisAmbComandaResum);
	echo "
	<tr>
		<td>
		</td>

		<td>
		</td>

		<td>
		</td>

		<td>
		</td>

		<td>
		</td>

		<td>
		<p>".(number_format(($umsTT+$umsEspTT),'2','.',''))."</p>
		</td>
		
		<td>
		<p><b>".(number_format(($ecosTT+$ecosEspTT),'2','.',''))."</b></p>
		</td>
		";
		
		if($mParametres['moneda3Activa']['valor']=='1')
		{
			echo "
		<td>
		<p><b>".(number_format(($m3TT+$m3EspTT),'2','.',''))." ".$mParametres['moneda3Abrev']['valor']."</b></p>
		</td>";
		}
		
		echo "
		<td>
		<p>".(number_format(($eurosTT+$eurosEspTT),'2','.',''))."</p>
		</td>


		<td>
		<p>".(number_format(($productesUmsTT+$productesCtUmsTT+$productesFdUmsTT+$productesEspUmsTT+$productesEspFdUmsTT),'2','.',''))."</p>
		</td>

		<td>
		<p><b>".(number_format(($productesEcosTT+$productesEspEcosTT),'2','.',''))."</b></p>
		</td>

		<td>
		<p>".(number_format(($productesEurosTT+$productesCtEurosTT+$productesFdEurosTT+$productesEspEurosTT+$productesEspFdEurosTT),'2','.',''))."</p>
		</td>
		";
		
		if($mParametres['moneda3Activa']['valor']=='1')
		{
			$m3RecolzatsTT=$rM3;
		if($m3RecolzatsTT>0){$color='red';}else{$color='green';}
			echo "
		<td>
		<p  style='color:".$color."';><b>".(number_format($m3RecolzatsTT,'2','.',''))."</b></p>
		</td>
			";
		}
		
		$ecosRecolzatsT=($ecosTT+$ecosEspTT)-($productesEcosTT+$productesEspEcosTT);
		if($ecosRecolzatsT>0){$color='red';}else{$color='green';}
		echo "
		<td>
		<p style='color:".$color."';><b>".(number_format($ecosRecolzatsT,'2','.',''))."</b></p>
		</td>
	</tr>

	<tr>
		<td>
		</td>

		<td>
		</td>

		<td>
		</td>
		
		<td>
		</td>

		<td>
		</td>
		
		<td>
		</td>
		
		<td>
		</td>
		";
		
		if($mParametres['moneda3Activa']['valor']=='1')
		{
			echo "
		<td>
		</td>";
		}
		
		echo "
		<td>
		</td>


		<td>
		</td>

		<td>
		</td>

		<td>
		<p>Total uts recolzades:</p>
		</td>
		";
		
		if($mParametres['moneda3Activa']['valor']=='1')
		{
			echo "
		<td>
		</td>
			";
		}
		
		$utsRecolzatsT=($rM3+$ecosRecolzatsT);
		if($utsRecolzatsT>0){$color='red';}else{$color='green';}
		echo "
		<td>
		<p style='color:".$color."';><b>".(number_format($utsRecolzatsT,'2','.',''))."</b></p>
		</td>
	</tr>
</table>
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
";

$parsChain=makeParsChain($mPars);
html_helpRecipient();


echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>
";
?>

		