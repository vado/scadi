<?php
include "config.php";
include "db.php";
include "eines.php";
include "eines_productes.php";
include "upload.php";

//POST
$editarProducteId=@$_POST['i_editarProducte_id'];

$parsChain=@$_POST['i_pars'];
$mPars=getPars($parsChain);
//DB
$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db);
$mProducte=db_getProducte($editarProducteId,$db);
//VARS
$guardarProducteId='';
$missatgeAlerta='';
$login=false;

$mColsProductes3=array('producte','productor','actiu','tipus','categoria0','categoria10','unitat_facturacio','format','pes','volum','preu','ms','estoc_previst','estoc_disponible','imatge','cost_transport_extern_kg','cost_transport_intern_kg');

$mNomsColumnes3=array(
'producte'=>'Producte',
'productor'=>'Productor',
'actiu'=>'Actiu',
'tipus'=>'Tipus',
'categoria0'=>'Categoria',
'categoria10'=>'Sub-categoria',
'unitat_facturacio'=>'Ut.fact',
'format'=>'Format(pack uts)',
'pes'=>'Pes ut.fact(kg)',
'volum'=>'Volum ut.fact (l)',
'preu'=>'Preu(uts)',
'ms'=>'% MS',
'estoc_previst'=>'Estoc previst',
'estat'=>'estat',
'estoc_disponible'=>'Estoc dispon.',
'imatge'=>'imatge',
'cost_transport_extern_kg'=>'cost transp extern (uts/kg)',
'ms_ctek'=>'% ms del cost transport extern per kg',
'cost_transport_intern_kg'=>'cost transp intern (uts/kg)',
'ms_ctik'=>'% ms del cost transport intern per kg',
);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	$login=false;
}
else
{
	$login=true;

}
$mGuardarProducte=array();
if($login && $mPars['grup_id']=='0')
{
	if($guardarProducteId=@$_POST['i_guardarProducte_id'])
	{
		$mGuardarProducte['id']=$guardarProducteId;
		$mGuardarProducte['producte']=urlencode(@$_POST['i_guardarProducte_producte']);
		$mGuardarProducte['productor']=urlencode(@$_POST['i_guardarProducte_productor']);
		$mGuardarProducte['actiu']=@$_POST['i_guardarProducte_actiu'];
		$mGuardarProducte['tipus']=@$_POST['i_guardarProducte_tipus'];
		$mGuardarProducte['categoria0']=@$_POST['i_guardarProducte_categoria0'];
		$mGuardarProducte['categoria10']=@$_POST['i_guardarProducte_categoria10'];
		$mGuardarProducte['unitat_facturacio']=@$_POST['i_guardarProducte_unitat_facturacio'];
		$mGuardarProducte['format']=@$_POST['i_guardarProducte_format'];
		$mGuardarProducte['pes']=@$_POST['i_guardarProducte_pes'];
		$mGuardarProducte['volum']=@$_POST['i_guardarProducte_volum'];
		$mGuardarProducte['preu']=@$_POST['i_guardarProducte_preu'];
		$mGuardarProducte['ms']=@$_POST['i_guardarProducte_ms'];
		$mGuardarProducte['estoc_previst']=@$_POST['i_guardarProducte_estoc_previst'];
		$mGuardarProducte['estat']=@$_POST['i_guardarProducte_estat'];
		$mGuardarProducte['estoc_disponible']=@$_POST['i_guardarProducte_estoc_disponible'];
		$mGuardarProducte['cost_transport_extern_kg']=@$_POST['i_guardarProducte_cost_transport_extern_kg'];
		$mGuardarProducte['ms_ctek']=@$_POST['i_guardarProducte_ms_ctek'];
		$mGuardarProducte['cost_transport_intern_kg']=@$_POST['i_guardarProducte_cost_transport_intern_kg'];
		$mGuardarProducte['ms_ctik']=@$_POST['i_guardarProducte_ms_ctik'];
		$mGuardarProducte['notes_rebosts']=urlencode(@$_POST['ta_guardarProducte_descripcio']);

		if(!db_guardarProducte($mGuardarProducte,$db))
		{
			$missatgeAlerta="<p  class='pAlertaNo'><i>Atenci�: error en guardar el producte</i></p>";
		}
		else
		{
			$missatgeAlerta="<p  class='pAlertaOk'><i>El producte s'ha guardat correctament.<br> Cal refrescar la p�gina per veure els canvis.</i></p>";
			$mProducte=db_getProducte($guardarProducteId,$db);
		}
	}
	else if($idProducte=@$_POST['i_imatgeIdProducte'])
	{
		$missatgeAlerta=recepcioImatgeProducte($db,$idProducte);
		$editarProducteId=$idProducte;
		$mProducte=db_getProducte($editarProducteId,$db);
	}
}

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>

<script type='text/javascript'  language='JavaScript'>
missatgeAlerta=\"".$missatgeAlerta."\";


function guardarProducte()
{
	document.getElementById('f_ifrGuardarProducte').submit();
}
</script>




<body bgcolor='#ffffff'  onload=''>
";

	echo "
<table style='width:100%;'>
	<tr>
		<td style='width:100%;' valign='middle' align='center'>
		<table style='width:100%;'>
			<tr>
				<td>
				<p style='font-size:15px; color:#aaaaaa;'><b>Admin</b></p>
				</td>
				
				<td style='width:40%;' align='center'>
				<FORM name='f_pujarImatge' METHOD='post' ACTION='' ENCTYPE='multipart/form-data'>
				<INPUT TYPE='hidden' NAME='i_imatgeIdProducte' value='".$mProducte['id']."'>
				<INPUT TYPE='file' size='8' NAME='i_imatge' ACCEPT='image/jpeg'>
				<input type='hidden' id='i_pars1' name='i_pars' value='".$parsChain."'>
				<input type='submit' value='pujar imatge'>
				</form>
				</td>
				
				<td style='width:50%;'>
				<input type='button' onClick=\"guardarProducte();\" value='guardar'>
				</td>
			</tr>
			<tr>
				<td>
				</td>

				<td  style=' width:80%;' align='center'>
				<p>".$missatgeAlerta."</p>
				</td>
				
				<td>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td  style='width:40%;' valign='top' >
		<form id='f_ifrGuardarProducte' name='f_ifrGuardarProducte' method='post' action='html_editarProducte.php' target='_self' >
		<input type='hidden' id='i_guardarProducte_id' name='i_guardarProducte_id' value='".$editarProducteId."'>
		<input type='hidden' id='i_pars1' name='i_pars' value='".$parsChain."'>
		<table style='width:100%;' >
			<tr>
				<th style='width:33%;' align='left' valign='top' bgcolor='#CCD4FA'>
				<p>Id:</p>
				</th>
				<td style='width:66%;' align='left'  valign='top' bgcolor='#CCD4FA' >
				<p>".$mProducte['id']."</p>
				</td>
			</tr>
	";
while (list($key,$val)=each($mProducte))
{
	if(in_array($key,$mColsProductes3))
	{
		if($key=='estoc_previst' || $key=='estoc_disponible')
		{
			echo "
			<tr>
				<th style='width:33%;' align='left' valign='top' bgcolor='#CCD4FA'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top' bgcolor='#CCD4FA' >
				<input type='text' readonly style='background-color:#dddddd;' size='50' id='i_guardarProducte_".$key."' name='i_guardarProducte_".$key."' value='".$val."'>
				</td>
			</tr>
			";
		}
		else if($key=='imatge')
		{
			echo "
			<tr>
				<th style='width:33%;' align='left' valign='top' bgcolor='#CCD4FA'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top' bgcolor='#CCD4FA' >
				";
				if($val!='')
				{
					echo "
				<img  src='productes/".$mProducte['id']."/".$thumbCode."_".$val."'>
					";
				}
				else
				{
					echo "
				<p>No s'ha pujat cap imatge</p>
					";
				}
				echo "
				
				</td>
			</tr>
			";
		}
		else
		{
			echo "
			<tr>
				<th style='width:33%;' align='left' valign='top' bgcolor='#CCD4FA'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top' bgcolor='#CCD4FA' >
				<input type='text' size='50' id='i_guardarProducte_".$key."' name='i_guardarProducte_".$key."' value=\"".(urldecode($val))."\">
				</td>
			</tr>
			";
		}
	}
}
reset($mProducte);
		echo "
			<tr>
				<th style='width:33%;' align='left' valign='top' bgcolor='#CCD4FA'>
				<p>Descripci�:</p>
				</th>
				<td style='width:66%;' align='left'  valign='top' bgcolor='#CCD4FA' >
				<textArea cols='50' rows='10' type='text' id='ta_guardarProducte_descripcio' name='ta_guardarProducte_descripcio'>".urldecode($mProducte['notes_rebosts'])."</textArea>
				</td>
			</tr>
		";

echo "
		<input type='hidden'  name='rebost' value='".$mPars['rebost']."'>
		<input type='hidden' id='i_guardarProducte_id' name='i_guardarProducte_id' value='".$mProducte['id']."'>
		</form>
		</table>
		</td>

	</tr>
</table>


<form id='f_ifrMostrarProducte' name='f_ifrMostrarProducte' method='post' action='html_productes.php' target='_self' style='visibility:hidden;'>
<input type='hidden' id='i_pars1' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='id' name='id' value='".$mProducte['id']."'>
</form>

<form id='f_ifrEditarProducte' name='f_ifrEditarProducte' method='post' action='html_editarProducte.php' target='_self' style='visibility:hidden;'>
<input type='hidden' id='i_pars2' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='i_editarProducte_id' name='i_editarProducte_id' value='".$mProducte['id']."'>
</form>


</body>
</html>
";

?>

		