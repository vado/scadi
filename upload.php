<?php

function recepcioImatgeProducte($db,$idProducte)
{
    global $mExtensionsPermeses,$mParametres,$thumbCode,$mPars;


    $imageDimensions=array();
    $thumbImageNamePath='';
    $imageResizedWidth=0;
    $imageResizedHeight=0;
    $missatgeError='';
    $newThumbWidth=0;
    $newThumbHeight=0;

	$cDir=getcwd();

	if($mPars['selLlistaId']==0)
	{
		if(!@chdir('productes'))
		{
			mkdir('productes');
			chdir('productes');
		}
		if(!@chdir($idProducte))
		{
			mkdir($idProducte);
			chdir($idProducte);
		}
		$dir='productes/'.$idProducte;
	}
	else
	{
		if(!@chdir('llistes'))
		{
			mkdir('llistes');
			chdir('llistes');
		}

		if(!@chdir($mPars['selLlistaId']))
		{
			mkdir($mPars['selLlistaId']);
			chdir($mPars['selLlistaId']);
			
		}

		if(!@chdir('productes'))
		{
			mkdir('productes');
			chdir('productes');
		}

		if(!@chdir($idProducte))
		{
			mkdir($idProducte);
			chdir($idProducte);
		}

		$dir='llistes/'.$mPars['selLlistaId'].'/productes/'.$idProducte;
	}
	chdir($cDir);	
	$mScanDir=scandir($dir);

	    if($_FILES['i_imatge']['name']!='')
        {
            if (is_uploaded_file($_FILES['i_imatge']['tmp_name']))
            {
                //mirar si l'extensio �s correcta:
                $size=@pathinfo($_FILES['i_imatge']['name']);
                if(in_array(@strtolower($size['extension']),$mExtensionsPermeses))
                {
                    $nomImatge=$idProducte.'.'.$size['extension'];
                    //if(!@chdir('productes/'.$idProducte)){mkdir('productes/'.$idProducte);}else{chdir($cDir);}
                    if(!copy($_FILES['i_imatge']['tmp_name'],$dir."/".$nomImatge))
                    {
                        $missatgeError.="<p class='pAlertaNo'> Atenci�: Error en copiar la imatge al directori.</p>";
                    }
                    else
                    {
						//esborrar imatges velles:
						while(list($key,$file)=each($mScanDir))
						{
							if($file!=$nomImatge && $file!='.' && $file!='..')
							{
								unlink($dir."/".$file);
							}
						}
						reset($mScanDir);
						$imageDimensions=getimagesize($dir."/".$nomImatge);
                        //resize if needed:
                        $imageResizedWidth=$imageDimensions[0];
                        $imageResizedHeight=$imageDimensions[1];
                        if($imageDimensions[0]>$mParametres['patternImageMaxWidth']['valor'])
                        {
                            $imageResizedWidth=floor($mParametres['patternImageMaxWidth']['valor']);
                            $imageResizedHeight=floor($imageResizedWidth*($imageDimensions[1]/$imageDimensions[0]));
                            $im=getImage($dir,$nomImatge,$size['extension'],$idProducte);
                            $imDest=@imagecreatetruecolor($imageResizedWidth,$imageResizedHeight);
                            @imagecopyresampled($imDest,$im,0,0,0,0,$imageResizedWidth,$imageResizedHeight,$imageDimensions[0],$imageDimensions[1]);
                            if(@strtolower($size['extension'])=='gif'){@imagegif($imDest,$dir."/".$nomImatge);}
                            else if(@strtolower($size['extension'])=='png'){@imagepng($imDest,$dir."/".$nomImatge);}
                            else if(@strtolower($size['extension'])=='jpg' || @strtolower($size['extension'])=='jpeg'){@imagejpeg($imDest,$dir."/".$nomImatge);}
                            @imagedestroy($im);
                            @imagedestroy($imDest);
                        }

                        //generate thumbnail
                        $newThumbWidth=$imageResizedWidth;
                        $newThumbHeight=$imageResizedHeight;
                        if($imageResizedWidth>$mParametres['thumbsWidth']['valor'])
                        {
                            $newThumbWidth=$mParametres['thumbsWidth']['valor'];
                            $newThumbHeight=$newThumbWidth*($imageDimensions[1]/$imageDimensions[0]);
                        }

                        if($newThumbHeight>$mParametres['thumbsHeight']['valor'])
                        {
                            $newThumbHeight=$mParametres['thumbsHeight']['valor'];
                            $newThumbWidth=$newThumbHeight*($imageDimensions[0]/$imageDimensions[1]);
                        }

                        $thumbImageNamePath=$dir."/".$thumbCode.'_'.$nomImatge;
                        $im=@getImage($dir,$nomImatge,$size['extension'],$idProducte);
                        $imDest=@imagecreatetruecolor($newThumbWidth,$newThumbHeight);
                        @imagecopyresampled($imDest,$im,0,0,0,0,$newThumbWidth,$newThumbHeight,$imageResizedWidth,$imageResizedHeight);
                        if(@strtolower($size['extension'])=='gif'){@imagegif($imDest,$thumbImageNamePath);}
                        else if(@strtolower($size['extension'])=='png'){@imagepng($imDest,$thumbImageNamePath);}
                        else if(@strtolower($size['extension'])=='jpg' || @strtolower($size['extension'])=='jpeg'){@imagejpeg($imDest,$thumbImageNamePath);}
                        @imagedestroy($im);
                        @imagedestroy($imDest);
						
						//guardar path imatge a bbdd
						//echo "<br>update ".$mPars['taulaProductes']." set imatge='".$nomImatge."' where id='".$idProducte."' AND llista_id='".$mPars['selLlistaId']."'";
						if(!$result=mysql_query("update ".$mPars['taulaProductes']." set imatge='".$nomImatge."' where id='".$idProducte."' AND llista_id='".$mPars['selLlistaId']."'",$db))
						{
							//echo "<br> 136 upload.php ".mysql_errno() . ": " . mysql_error(). "\n";
							$missatgeError.="<p class='pAlertaNo'> Atenci�: No s'ha pogut guardar la imatge a la fitxa del producte</p>";
							return false;
						}
						
                    }
                }
                else
                {
                    $missatgeError.="<p class='pAlertaNo'> Atenci�: El tipus de fitxer ".$size['extension']." no est� perm�s (tipus permesos: gif,jpg,png).</p>";
                }
            }
            else
            {
                $missatgeError.="<p class='pAlertaNo'> Atenci�: Error en enviar el fitxer al servidor.</p>";
            }
        

        $thumbImageNamePath='';
        $imageResizedWidth=0;
        $imageResizedHeight=0;
        $newThumbWidth=0;
        $newThumbHeight=0;
    }

    if(strlen($missatgeError)==0)
	{
        $missatgeError.="<p class='pAlertaOk'> Ok: La imatge s'ha guardat correctament.</p>";
    }


return $missatgeError;
}

//------------------------------------------------------------------------------
function recepcioImatgeVehicle($db,$idVehicle)
{
    global $mExtensionsPermeses,$mParametres,$thumbCode,$mPars;

    $imageDimensions=array();
    $thumbImageNamePath='';
    $imageResizedWidth=0;
    $imageResizedHeight=0;
    $missatgeError='';
    $newThumbWidth=0;
    $newThumbHeight=0;
	$cDir=getcwd();
	
	$dir='vehicles';
	
	if(!@chdir('vehicles'))
	{
		@mkdir('vehicles');
	}
	else
	{
		@chdir($cDir);
	}
	
	$mScanDir=@scandir('vehicles/'.$idVehicle);

	    if($_FILES['i_imatge']['name']!='')
        {
            if (is_uploaded_file($_FILES['i_imatge']['tmp_name']))
            {
                //mirar si l'extensio �s correcta:
                $size=@pathinfo($_FILES['i_imatge']['name']);
                if(in_array(@strtolower($size['extension']),$mExtensionsPermeses))
                {
                    $nomImatge=$_FILES['i_imatge']['name'];
                    if(!@chdir('vehicles/'.$idVehicle)){mkdir('vehicles/'.$idVehicle);}else{chdir($cDir);}
                    if(!copy($_FILES['i_imatge']['tmp_name'],'vehicles/'.$idVehicle."/".$nomImatge))
                    {
                        $missatgeError.="<p class='pAlertaNo'> Atenci�: Error en copiar la imatge al directori.</p>";
                    }
                    else
                    {
						//esborrar imatges velles:
						while(list($key,$file)=@each($mScanDir))
						{
							if($file!=$nomImatge && $file!='.' && $file!='..')
							{
								@unlink('vehicles/'.$idVehicle."/".$file);
							}
						}
						@reset($mScanDir);
						
						$imageDimensions=getimagesize('vehicles/'.$idVehicle."/".$nomImatge);
                        //resize if needed:
                        $imageResizedWidth=$imageDimensions[0];
                        $imageResizedHeight=$imageDimensions[1];
                        if($imageDimensions[0]>$mParametres['patternImageMaxWidth']['valor'])
                        {
                            $imageResizedWidth=floor($mParametres['patternImageMaxWidth']['valor']);
                            $imageResizedHeight=floor($imageResizedWidth*($imageDimensions[1]/$imageDimensions[0]));
                            $im=getImage($dir,$nomImatge,$size['extension'],$idVehicle);
                            $imDest=@imagecreatetruecolor($imageResizedWidth,$imageResizedHeight);
                            @imagecopyresampled($imDest,$im,0,0,0,0,$imageResizedWidth,$imageResizedHeight,$imageDimensions[0],$imageDimensions[1]);
                            if(@strtolower($size['extension'])=='gif'){@imagegif($imDest,'vehicles/'.$idVehicle."/".$nomImatge);}
                            else if(@strtolower($size['extension'])=='png'){@imagepng($imDest,'vehicles/'.$idVehicle."/".$nomImatge);}
                            else if(@strtolower($size['extension'])=='jpg' || @strtolower($size['extension'])=='jpeg'){@imagejpeg($imDest,'vehicles/'.$idVehicle."/".$nomImatge);}
                            @imagedestroy($im);
                            @imagedestroy($imDest);
                        }

                        //generate thumbnail
                        $newThumbWidth=$imageResizedWidth;
                        $newThumbHeight=$imageResizedHeight;
                        if($imageResizedWidth>$mParametres['thumbsWidth']['valor'])
                        {
                            $newThumbWidth=$mParametres['thumbsWidth']['valor'];
                            $newThumbHeight=$newThumbWidth*($imageDimensions[1]/$imageDimensions[0]);
                        }

                        if($newThumbHeight>$mParametres['thumbsHeight']['valor'])
                        {
                            $newThumbHeight=$mParametres['thumbsHeight']['valor'];
                            $newThumbWidth=$newThumbHeight*($imageDimensions[0]/$imageDimensions[1]);
                        }

                        $thumbImageNamePath='vehicles/'.$idVehicle."/".$thumbCode.'_'.$nomImatge;
                        $im=@getImage($dir,$nomImatge,$size['extension'],$idVehicle);
                        $imDest=@imagecreatetruecolor($newThumbWidth,$newThumbHeight);
                        @imagecopyresampled($imDest,$im,0,0,0,0,$newThumbWidth,$newThumbHeight,$imageResizedWidth,$imageResizedHeight);
                        if(@strtolower($size['extension'])=='gif'){@imagegif($imDest,$thumbImageNamePath);}
                        else if(@strtolower($size['extension'])=='png'){@imagepng($imDest,$thumbImageNamePath);}
                        else if(@strtolower($size['extension'])=='jpg' || @strtolower($size['extension'])=='jpeg'){@imagejpeg($imDest,$thumbImageNamePath);}
                        @imagedestroy($im);
                        @imagedestroy($imDest);
						
						//guardar path imatge a bbdd
						if(!$result=mysql_query("update vehicles set imatge='".$nomImatge."' where id='".$idVehicle."'",$db))
						{
							//echo "<br> 77. upload.php ".mysql_errno() . ": " . mysql_error(). "\n";
							$missatgeError.="<p class='pAlertaNo'> Atenci�: No s'ha pogut guardar la imatge a la fitxa del vehicle</p>";
							return false;
						}
						
                    }
                }
                else
                {
                    $missatgeError.="<p class='pAlertaNo'> Atenci�: El tipus de fitxer ".$size['extension']." no est� perm�s (tipus permesos: gif,jpg,png).</p>";
                }
            }
            else
            {
                $missatgeError.="<p class='pAlertaNo'> Atenci�: Error en enviar el fitxer al servidor.</p>";
            }
        

        $thumbImageNamePath='';
        $imageResizedWidth=0;
        $imageResizedHeight=0;
        $newThumbWidth=0;
        $newThumbHeight=0;
    }

    if(strlen($missatgeError)==0)
	{
        $missatgeError.="<p class='pAlertaOk'> Ok: La imatge s'ha guardat correctament.</p>";
    }


return $missatgeError;
}

////////////////////////////////////////////////////////////////////////////////

function getImage($dir,$image,$extension,$idProducte)
{
    $im=0;
    if(strtolower($extension)=='gif'){$im=imagecreatefromgif($dir.'/'.$image);}
    else if(strtolower($extension)=='jpg' || strtolower($extension)=='jpeg'){$im=imagecreatefromjpeg($dir.'/'.$image);}
    else if(strtolower($extension)=='png'){$im=imagecreatefrompng($dir.'/'.$image);}

	return $im;
}



?>



