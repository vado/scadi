<?php
//------------------------------------------------------------------------

function csv_crearLlistaProductesPeriodeLocal($db)
{
	global $mPars,$mProductes;
	$i=0;
	$periode=str_replace(array('-',':'),array('_','__'),$mPars['sel_periode_comanda_local']);
	$dir=getcwd();
	if(!@chdir('rebosts'))
	{
		mkdir('rebosts');
		chdir('rebosts');
	}
	else
	if(!@chdir($mPars['grup_id']))
	{
		mkdir($mPars['grup_id']);
		chdir($mPars['grup_id']);
	}
	chdir($dir);
	@unlink("rebosts/".$mPars['grup_id']."/llistaProductesLocal_".$periode.".csv");

	if(!$fp=fopen("rebosts/".$mPars['grup_id']."/llistaProductesLocal_".$periode.".csv",'w'))
	{
		return false;
	}
	$mH1[0]=date('d/m/Y hh:mm');
	fputcsv($fp, $mH1,',','"');
	$mCamps=array_keys($mProductes);
	fputcsv($fp, $mCamps,',','"');
	
	while(list($key,$mProducteLlista)=each($mProductes))
	{
    	if(!fputcsv($fp, $mProducteLlista,',','"'))
		{
			return false;
		}
	}	
	fclose($fp);
	return true;
}

//------------------------------------------------------------------------------
function csv_getProductesLocal($db)
{
	global $mParams,$mPars,$mCampsProductes,$missatgeAlerta;
	
	$mProductes_=array();
	$mProductes=array();

	
	$periode=str_replace(array('-',':'),array('_','__'),$mPars['sel_periode_comanda_local']);
	if(($fp=@fopen("rebosts/".$mPars['grup_id']."/llistaProductesLocal_".$periode.".csv",'r'))!==false)
	{
		while($linCSV=fgetcsv($fp,0,',','"'))
		{
			array_push($mProductes_,$linCSV);
		}
		if($mProductes_ && count($mProductes_)>0)
		{
			array_shift($mProductes_);
			//$mCamps=$mProductes_[0];
			$i=0;
			$j=0;
			$mCampsProductes=db_getCampsproductes($db);
			while(list($key,$mProducte_)=each($mProductes_))
			{
				while(list($key2,$camp)=each($mCampsProductes))
				{
					$mProductes[$i][$camp]=@$mProducte_[$j];
					$j++;
				}
				reset($mCampsProductes);
				$j=0;
				$i++;
			}
			reset($mProductes_);
			$i=0;
			array_shift($mProductes);
			unset($mProductes_);

			$mProductes=filtrarProductesLocal($mProductes,$db); //filtre + paginació
		}
	}
	else
	{
		$missatgeAlerta.="<p class='pAlertaNo4'>ERROR: no s'ha trobat la taula de productes</p>";
	
	}

	
	return $mProductes;
}

?>

		