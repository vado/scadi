<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include "html_ajuda1.php";
include "db_ajuda.php";
include "db_gestioProductes.php";
include "db_gestioMagatzems.php";
include "html_vistaInventari.php";
include "db_vistaInventari.php";

$mComanda=array();
$mCr=array();
$form='';
$missatgeAlerta='';
$jaEntregatT=0;

//------------------------------------------------------------------------------

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);

$ruta_=@$_GET['sR']; //selector de ruta

if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}


getConfig($db); //inicialitza variables anteriors;
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['vistaInventari.php']=db_getAjuda('vistaInventari.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='productor';
	$mPars['ascdesc']='ASC';
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';

	$mPars['fPagamentUms']=0.00;
	$mPars['fPagamentEcos']=0.00;
	$mPars['fPagamentEb']=0.00;
	$mPars['fPagamentEu']=0.00;
	$mPars['fPagamentEuMetalic']=0;
	$mPars['fPagamentEuTransf']=0;
	$mPars['fPagamentEcosIntegralCES']=0;
	

	$mPars['fPagamentEcosPerc']=0.00;
	$mPars['fPagamentEbPerc']=0.00;
	$mPars['fPagamentEuPerc']=0.00;

	$mPars['vRutaIncd']=$mPars['selRutaSufix'];
	
	$mPars['albaraVistaImpressio']=0;
	
if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}


$mMagatzems=db_getMagatzems($db);

$mRebostsRef=db_getRebostsRef($db);
$mRutesSufixes=getRutesSufixes($db);
$mUsuari=db_getUsuari($mPars['usuari_id'],$db);
$mUsuarisRef=db_getUsuarisRef($db);
$mPerfilsRef=db_getPerfilsRef($db);

$mProductes=db_getProductes2($db);


	$mInventariG=db_getInventari($mPars['grup_id'],$db);
	$mInventariGT=db_getInventari('0',$db); //inventari total, si es mostra un magatzem
	// carregar inventari sobre productes:
	while(list($key,$val)=each($mProductes))
	{
		$mProductes[$key]['estoc_real']='';
		if(isset($mInventariGT['inventariRef'][$key]))
		{
			$mProductes[$key]['quantitatInvGlobal']=$mInventariGT['inventariRef'][$key];
		}
		else
		{
			$mProductes[$key]['quantitatInvGlobal']=0;
		}

		if(isset($mInventariG['inventariRef'][$key]))
		{
			$mProductes[$key]['quantitatInvLocal']=$mInventariG['inventariRef'][$key];
		}
		else
		{
			$mProductes[$key]['quantitatInvLocal']=0;
		}
	}
	reset($mProductes);
//ordenar productes per productor, producte
$mProductes_=array();
while(list($producteId,$mProducte)=each($mProductes))
{
	$projecte=substr($mProducte['productor'],strpos($mProducte['productor'],'-')+1);
	if(!isset($mProductes_[$projecte]))
	{$mProductes_[$projecte]=array();}
	$mProductes_[$projecte][$producteId]=$mProducte;
}
reset($mProductes);
ksort($mProductes_);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>

<head>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<title>Full d'Inventaris</title>
<LINK REL='StyleSheet' HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_vistaInventari.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
ruta=".$mPars['selRutaSufix'].";
</SCRIPT>
</head>
<body  bgcolor='".$mColors['body']."'>
";
html_demo('vistaInventari.php?');
echo "
	<table align='left' style='width:90%;'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:16px;'><b>".$mContinguts['index']['titol0']."  - ".$mContinguts['index']['titol1']."</b></p>
			<p style='font-size:13px;'>".$mContinguts['form']['titol']." <a id='a_info' style='color:#ff7700; cursor:pointer;' onclick=\"enviarFpars('informacio.php','_blank')\">(<u>INFO</u>)</a></p>
			</td>
		</tr>
	</table>
<table width='90%'  bgcolor='".$mColors['table']."'>
	<tr>
		<td  width='100%'>
	";
html_mostrarProductes($db);

$parsChain=makeParsChain($mPars);
echo "
		</td>
	</tr>
</table>
";
html_helpRecipient();

echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>
";
	

?>

		