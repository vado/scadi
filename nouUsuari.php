<?php

/*
formulari grup de nou usuari
*/

include "config.php";
include "einesConfig.php";
include "db.php";
include "html_ajuda1.php";
include "db_ajuda.php";
include "html.php";
include "html_nouUsuari.php";
include "eines.php";
include "db_nouUsuari.php";
include "db_gestioGrup.php";
include "db_mail.php";
	require_once('phpmailer/class.phpmailer.php');
	include("phpmailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);

$mRutesSufixes=getRutesSufixes($db);
if($mPars['nivell']=='sadmin')
{
	$ruta=$mPars['selRutaSufix'];
}
else
{
	array_reverse($mRutesSufixes);
	$ruta=array_pop($mRutesSufixes);
	$mPars['selRutaSufix']=$ruta;
}

$parsChain=makeParsChain($mPars);

getConfig($db);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
	$login=false;
}
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['incidencies.php']=db_getAjuda('nouUsuari.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

	if($mParametres['moneda3Activa']['valor']==1)
	{
		$moneda3ElementsStyle=" style='visibility:inherit; z-index:0; position:relative;' ";
	}
	else
	{
		$moneda3ElementsStyle=" style='visibility:hidden; z-index:1;  position:absolute;' ";
	}

	$mUsuari=array();
	$mUsuari['id']='';
	$mUsuari['usuari']='';
	$mUsuari['nom']='';
	$mUsuari['cognoms']='';
	$mUsuari['email']='';
	$mUsuari['mobil']='';
	$mUsuari['municipi']='';
	$mUsuari['adressa']='';
	$mUsuari['propietats']='';
	$mUsuari['grups']='';
	$mUsuari['contrassenya']='';
	$mUsuari['nivell']='usuari';
	$mUsuari['perfils_productor']='';
	$mUsuari['notes']='';
	$mUsuari['estat']='pendent';
	$mUsuari['data']=date('Y-m-d h:i:s');
	$mUsuari['compte_ecos']='';
	$mUsuari['compte_cieb']='';

$opcio1='';
$opcio2='';

if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
{
	$mGrups=getGrups($db,'tots');
}
else
{
	$mGrups=getGrups($db,'actius');
}

$mMunicipis=db_getMunicipis2Id($db);
$mProductors=db_getProductors($db);

$missatgeAlerta='';
$missatgeAlerta1='';

$opcio1=@$_GET['op'];
$opcio2=@$_POST['i_opcio'];
if($opcio1!=''){$opcio=$opcio1;}
else if($opcio2!=''){$opcio=$opcio2;}
else {$opcio='';}

if($opcio=='altaGrup' || $opcio=='baixaGrup')
{
	$opGi=@$_GET['opGi']; //opcio grup id
	if(!put_peticioGrup($opcio,$opGi,$db))
	{
		$missatgeAlerta1="<p class='pAlertaNo'>Atenci�: no s'ha pogut enviar la teva petici�</p>";
	}
	else
	{
		mails_novaPeticio($opcio,$opGi,$db);
		
		$missatgeAlerta1="<p class='pAlertaOk'>La teva petici� s'ha enviat correctament.\n\n El responsable de grup la revisar�</p>";
	}
}

if ($mPars['demo']==-1 && $opcio=='crearUsuari')
{
	$mUsuari['id']=@$_POST['i_id'];
	$mUsuari['usuari']=urlencode(trim(@$_POST['i_usuari']));
	$mUsuari['nom']=urlencode(@$_POST['i_nom']);
	$mUsuari['cognoms']=urlencode(@$_POST['i_cognoms']);
	$mUsuari['email']=@(trim($_POST['i_email']));
	$mUsuari['mobil']=@(trim($_POST['i_mobil']));
	$mUsuari['municipi']=@$_POST['sel_municipi'];
	$mUsuari['adressa']=@$_POST['i_adre�a'];
	$mUsuari['propietats']=@$_POST['ta_propietats'];
	$mUsuari['grups']=@$_POST['sel_grups'];
	$mUsuari['contrassenya']=@$_POST['i_contrasenya'];
	$mUsuari['nivell']=@$_POST['sel_nivell'];
	$mUsuari['perfils_productor']=@$_POST['sel_perfilsProductor'];
	$mUsuari['notes']=@$_POST['ta_notes'];
	$mUsuari['estat']=@$_POST['sel_estat'];
	$mUsuari['data']=date('Y-m-d h:i:s');
	$mUsuari['compte_ecos']=@$_POST['i_compteEcos'];
	$mUsuari['compte_cieb']=@$_POST['i_compteCieb'];

	if(!putNouUsuari($opcio,$mUsuari,$db))
	{
		if($mPars['nivell']=='visitant')
		{
			$missatgeAlerta1="<p class='pAlertaNo'>Atenci�: no s'ha pogut enviar la teva sol.licitut. Es molt possible que aquest compte de correu ja estigui vinculat a un altre usuari. Poseu-vos en contacte amb el responsable del grup que heu seleccionat o amb l'administrador</p>";
		}
		else
		{
			$missatgeAlerta1="<p class='pAlertaNo'>Atenci�: no s'ha pogut crear el nou usuari. Poseu-vos en contacte amb el responsable del grup que heu seleccionat o amb l'administrador</p>";
		}
		error_log ( date('d:m:Y')." Error DB - putNouUsuari() - nouUsuari.php - l:73" ,0,'errors.php');
		while(list($key,$val)=each($mUsuari))
		{
			$mUsuari[$key]='';
		}
		reset($mUsuari);
		$mUsuari['grups']=array();
	}
	else
	{
		if($mPars['nivell']=='visitant')
		{
			$missatgeAlerta1="<p class='pAlertaOk'>La teva petici� s'ha enviat correctament. \n\nEn breu rebr�s un correu del responsable de grup.</p>";
			mails_novaPeticio('altaGrup',$mUsuari['grups'][0],$db);
		}
		else
		{
			$missatgeAlerta1="<p class='pAlertaOk'>S'ha creat el nou usuari correctament.</p>";
		}
		
		$mGrups=getGrups($db,'tots');
	}
	$opcio='solicitutEnviada';
}

if($opcio=='guardarUsuari')
{
	$mUsuari['id']=@$_POST['i_id'];
	$mUsuari_mem=db_getUsuari($mUsuari['id'],$db);
	$mUsuari['usuari']=urlencode(@$_POST['i_usuari']);
	$mUsuari['nom']=urlencode(@$_POST['i_nom']);
	$mUsuari['cognoms']=urlencode(@$_POST['i_cognoms']);
	if($mPars['demo']==-1 && $mPars['usuari_id']==$mUsuari['id'])
	{
		$mUsuari['email']=@$_POST['i_email'];
		$mUsuari['contrassenya']=@$_POST['i_contrasenya'];
	}
	else
	{
		$mUsuari['email']=$mUsuari_mem['email'];
		$mUsuari['contrassenya']=$mUsuari_mem['contrassenya'];
	}
	$mUsuari['mobil']=@$_POST['i_mobil'];
	$mUsuari['municipi']=@$_POST['sel_municipi'];
	$mUsuari['adressa']=@$_POST['i_adre�a'];

	$mPropietats=array();
	$mComissionsUsuariGuardar=array();
	
	$mUsuari['propietats']='{';
	while(list($propietat,$mOpcions)=each($mPropietatsUsuaris))
	{
		if(!isset($mPropietats[$propietat])){$mPropietats[$propietat]='';}
		if($propietat=='comissio')
		{
			$mComissionsUsuariGuardar=@$_POST['sel_'.$propietat];
			while(list($key,$val)=each($mComissionsUsuariGuardar))
			{
				$mPropietats[$propietat].=$val.',';
			}
			$mPropietats[$propietat]=substr($mPropietats[$propietat],0,strlen($mPropietats[$propietat])-1);
		}
		else
		{
			$mPropietats[$propietat]=@$_POST['sel_'.$propietat];
		}
		$mUsuari['propietats'].=$propietat.':'.$mPropietats[$propietat].';';
	}
	reset($mPropietatsUsuaris);
	$mUsuari['propietats'].='}';
	//$mUsuari['propietats']=@$_POST['ta_propietats'];
	$mUsuari['grups']=@$_POST['sel_grups'];
	$mUsuari['perfils_productor']=@$_POST['sel_perfilsProductor'];
	$mUsuari['notes']=@$_POST['ta_notes'];
	$mUsuari['data']=date('Y-m-d h:i:s');
	$mUsuari['compte_ecos']=@$_POST['i_compteEcos'];
	$mUsuari['compte_cieb']=@$_POST['i_compteCieb'];
	if(!putNouUsuari($opcio,$mUsuari,$db))
	{
		$missatgeAlerta1="<p class='pAlertaNo'>Atenci�: no s'ha pogut guardar l'usuari</p>";
		error_log ( date('d:m:Y')." Error DB - putNouUsuari() - nouUsuari.php - l:103" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta1="<p class='pAlertaOk'>S'ha guardat l'usuari correctament.</p>";
	}
	$mPars['selUsuariId']=$mUsuari['id'];
	$mUsuari=db_getUsuari($mPars['selUsuariId'],$db);
	$opcio='editarUsuari';
}


if($opcio=='activarUsuari')
{
	$mPars['selUsuariId']=@$_GET['iUed'];
	$mUsuari=db_getUsuari($mPars['selUsuariId'],$db);
	if(!db_activarUsuari(1,$db))
	{
		$missatgeAlerta1="<p class='pAlertaNo'>Atenci�: no s'ha pogut activar l'usuari</p>";
		error_log ( date('d:m:Y')." Error DB - putNouUsuari() - nouUsuari.php - l:160" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta1="<p class='pAlertaOk'>S'ha activat l'usuari correctament.</p>";
	}
	$opcio='editarUsuari';
}

if($opcio=='desactivarUsuari')
{
	$mPars['selUsuariId']=@$_GET['iUed'];
	$mUsuari=db_getUsuari($mPars['selUsuariId'],$db);
	if(!db_activarUsuari(0,$db))
	{
		$missatgeAlerta1="<p class='pAlertaNo'>Atenci�: no s'ha pogut desactivar l'usuari</p>";
		error_log ( date('d:m:Y')." Error DB - putNouUsuari() - nouUsuari.php - l:178" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta1="<p class='pAlertaOk'>S'ha desactivat l'usuari correctament.</p>";
	}
	$opcio='editarUsuari';
}

if($opcio=='canviNivellUsuari')	
{
	$mPars['selUsuariId']=@$_GET['iUed'];
	$mPars['selUsuariNivell']=@$_GET['iUn'];
	$mUsuari=db_getUsuari($mPars['selUsuariId'],$db);
	if(!db_canviarNivellUsuari($db))
	{
		$missatgeAlerta1="<p class='pAlertaNo'>Atenci�: no s'ha pogut canviar el nivell d'usuari</p>";
		error_log ( date('d:m:Y')." Error DB - putNouUsuari() - nouUsuari.php - l:194" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta1="<p class='pAlertaOk'>S'ha canviat el nivell d'usuari correctament.</p>";
	}
	$opcio='editarUsuari';
}

if($opcio=='editarUsuari')
{
	if($selUsuariId=@$_GET['iUed'])
	{
		$mPars['selUsuariId']=$selUsuariId;
	}
	$mUsuari=db_getUsuari($mPars['selUsuariId'],$db);
	$parsChain=makeParsChain($mPars);
}

if($opcio=='eliminarUsuari')
{
	if($selUsuariId=@$_GET['iUed'])
	{
		$mPars['selUsuariId']=$selUsuariId;
		if(!db_delUsuari($db))
		{
			$missatgeAlerta1="<p class='pAlertaNo'>ATENCI�. No s'ha pogut eliminar l'usuari.</p>";
		}
		else
		{
			$missatgeAlerta1="<p class='pAlertaOk'>S'ha eliminat l'usuari correctament.</p>";
		}
	}
	$parsChain=makeParsChain($mPars);
}
$mPars['perfils_productor']=$mUsuari['perfils_productor'];
$mPerfilsUsuari=db_getPerfilsUsuari($db);


echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>"; echo $htmlTitolPags; echo " - Usuaris</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js1_nouUsuari.js' CHARSET='ISO-8859-1'></SCRIPT>

<script type='text/javascript'  language='JavaScript'>
navegador();
missatgeAlerta=\"".$missatgeAlerta."\";
moneda3DigitsCodi='".$mParametres['moneda3DigitsCodi']['valor']."';
moneda3Nom='".$mParametres['moneda3Nom']['valor']."';
moneda3Activa='".$mParametres['moneda3Activa']['valor']."';
</script>

</head>

<body onLoad=\"javascript: if(missatgeAlerta!=''){alert(missatgeAlerta);}\" bgcolor='".$mColors['body']."'>
";
html_demo('nouUsuari.php?');
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			</td>
		</tr>
	</table>
	<div align='center' width='100%'>".$missatgeAlerta1."</div>
	";
	html_menuUsuari();
	echo "
	<table border='1' align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
";
if($mPars['nivell']=='usuari' || $mPars['nivell']=='coord' || $mPars['nivell']=='admin' || $mPars['nivell']=='sadmin')
{
	echo "
		<tr>
			<td align='center' style='width:100%'>
			<center><p>[Dades d'usuari/a]</p></center>
			<table width='100%'>
				<tr>
					<td align='center' width='33%'  valign='top'>
					</td>

					<td align='center' width='33%' valign='middle'>
					<input type='button' value=\"Editar perfil d'usuari\" onClick=\"enviarFpars('nouUsuari.php?iUed=".$mPars['usuari_id']."&op=editarUsuari','_self');\">
					</td>

					<td align='center' width='33%' valign='middle'>
	";
	html_solicitaAltaGrup();
	echo "			
					</td>
				</tr>
			</table>					
			</td>
		</tr>
	";
}
$style='';
if ($opcio=='nouUsuari')
{

	if($mPars['nivell']=='visitant' || $mPars['nivell']=='usuari'){$style=" style='visibility:hidden; position:absolute;'";}

	echo "
		<tr>
			<td align='left' style='width:100%'>
			<table  align='center'  style='width:70%'>
				<tr>
					<td style='width:100%' align='left'>
					<form id='f_nouUsuari' name='f_nouUsuari' method='POST' action='nouUsuari.php' target='_self' >
					<input type='hidden' name='i_opcio' value='crearUsuari'>
					<input type='hidden' id='sel_compteSoci' value=''>
					<table align='center'  style='width:100%'>
						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							<p  style='font-size:15px;'><i>- Dades de l'usuari -</i></p>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Usuari (Nick)</p>
							</th>
							<td align='left' valign='top'>
							<p><input type='text' id='i_usuari' name='i_usuari' size='30' value=\"".(urldecode($mUsuari['usuari']))."\">*</p>
							</td>
						</tr>

						<tr ".$style.">
							<th align='left' valign='top'>
							<p>Nivell</p>
							</th>
							<td align='left' valign='top'>
							<select id='sel_nivell' name='sel_nivell'>
							";
							if($mPars['nivell']=='sadmin')
							{
								echo "
							<option "; if($mUsuari['nivell']=='admin'){echo " selected ";} echo " value='admin'>admin</option>
							<option "; if($mUsuari['nivell']=='coord'){echo " selected ";} echo " value='coord'>coord</option>
							<option "; if($mUsuari['nivell']=='usuari'){echo " selected ";} echo "  value='usuari'>usuari</option>
								";
							}
							else if($mPars['nivell']=='admin')
							{
								echo "
							<option "; if($mUsuari['nivell']=='coord'){echo " selected ";} echo " value='coord'>coord</option>
							<option "; if($mUsuari['nivell']=='usuari'){echo " selected ";} echo "  value='usuari'>usuari</option>
								";
							}
							else if($mPars['nivell']=='coord' || $mPars['nivell']=='usuari' || $mPars['nivell']=='visitant')
							{
								echo "
							<option "; if($mUsuari['nivell']=='usuari'){echo " selected ";} echo "  value='usuari'>usuari</option>
								";
							}
							
							echo "
							</td>
						</tr>

						<tr ".$style.">
							<th align='left' valign='top'>
							<p>Estat</p>
							</th>
							<td align='left' valign='top'>
							<select id='sel_estat' name='sel_estat'>
							<option  selected value='pendent'>pendent</option>
							</select>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Nom </p>
							</th>
							<td align='left' valign='top'>
							<p><input type='text' id='i_nom' name='i_nom' size='30' value=\"".(urldecode($mUsuari['nom']))."\">*</p>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Cognoms </p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_cognoms' name='i_cognoms' size='30' value=\"".(urldecode($mUsuari['cognoms']))."\">
							</td>
						</tr>
						
						<tr>
							<th align='left' valign='top'>
							<p>Email</p>
							</th>
							<td align='left' valign='top'>
							<p><input type='text' id='i_email' name='i_email' size='30' value='".$mUsuari['email']."'>*</p>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Mobil</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_mobil' name='i_mobil' size='9' value='".$mUsuari['mobil']."'>
							</td>
						</tr>
					
						<tr>
							<td align='left' valign='top'>
							<p><b>grups</b></p>
							</td>
							<td align='left' valign='top'>
							<table width='100%'>
								<tr>
									<td> 
									<p><select id='sel_grups' name='sel_grups[]' multiple>
					";
					$selected1='';
					$selected2='selected';
					while(list($index,$mGrup)=each($mGrups))
					{
						if(substr_count($mUsuari['grups'],$mGrup['ref'])>0)
						{
							$selected1='selected';$selected2='';}else{$selected1='';
						}
						echo "
									<option ".$selected1." value='".$mGrup['id']."'>".(urldecode($mGrup['nom']))."</option>
						";
					}
					reset($mGrups);
					echo "
									<option ".$selected2." value=''></option>
									</select>
									*</p>
									</td>
									<td>
									<p>&nbsp;&nbsp;</p>
									</td>
									<td valign='top' align='left' width='49%'> 
									<p style='color:#aaaaaa;'>
														";
					while(list($index,$mGrup)=each($mGrups))
					{
						if(substr_count($mUsuari['grups'],','.$mGrup['ref'].',')>0)
						{
						echo "
									<font><img src='imatges/okp.gif'>&nbsp;".(urldecode($mGrup['nom']))."</font><br>
						";
						}
					}
					reset($mGrups);
					echo "
									</p>
									</td>
								</tr>
							</table>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Adre�a</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_adre�a' name='i_adre�a' size='50' value='".$mUsuari['adressa']."'>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Municipi</p>
							</th>
							<td align='left' valign='top'>
							<p><select id='sel_municipi' name='sel_municipi'>
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mMunicipi)=each($mMunicipis))
					{
						if($mMunicipi['municipi']==$mUsuari['municipi'])
						{
							$selected1='selected';
							$selected2='';
						}
						else
						{
							$selected1='';
						}
						echo "
							<option ".$selected1." value='".$mMunicipi['id']."'>".(urldecode($mMunicipi['municipi']))."</option>
						";
					}
					reset($mMunicipis);
					echo "
							<option ".$selected2." value=''></option>
							</select>*</p>
							</td>
						</tr>
						<tr ".$style.">
							<th align='left' valign='top'>
							<p>Notes</p>
							</th>
							<td align='left' valign='top'>
							<textArea id='ta_notes'  name='ta_notes' cols='50' rows='7'>".(urldecode($mUsuari['propietats']))."</textArea>
							</td>
						</tr>

						<tr ".$style.">
							<th align='left' valign='top'>
							<p>Propietats</p>
							</th>
							<td align='left' valign='top'>
							<textArea id='ta_propietats'  name='ta_propietats' cols='50' rows='7'>".(urldecode($mUsuari['propietats']))."</textArea>
							</td>
						</tr>

					"; 
					if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
					{
						echo "
						<tr>
							<th align='left' valign='top'>
							<p>Perfils de Productor</p>
							
							</th>
							<td align='left' valign='top'>
							<select id='sel_perfilsProductor' name='sel_perfilsProductor[]' multiple>
						";
						$selected1='';
						$selected2='selected';
					
						while(list($id,$mProductor)=each($mProductors))
						{
							if(substr_count($mUsuari['perfils_productor'],','.$id.',')>0)
							{
								$selected1='selected';
								$selected2='';
							}
							else
							{
								$selected1='';
							}
							echo "
								<option ".$selected1." value='".$id."'>".(urldecode($mProductor['projecte']))."</option>
							";
						}
						reset($mProductors);
						echo "
							<option ".$selected2." value=''></option>
							</select>
							</td>
							<td>
							<table width='100%' border='0'>
								<tr>
									<td valign='top' align='left' width='49%'> 
									<p style='color:#aaaaaa;'>
														";
					while(list($id,$mProductor)=each($mProductors))
					{
						if(substr_count($mUsuari['perfils_productor'],','.$id.',')>0)
						{
									echo "<font><img src='imatges/okp.gif'>&nbsp;".(urldecode($mProductor['projecte']))."</font><br>";
						}
					}
					reset($mProductors);
					echo "
									</p>
									</td>
									</td>
								</tr>
							</table>
						</tr>
						";
					}
					else
					{
						echo "
						<tr style='visibility:hidden; position:absolute;'>
							<th align='left' valign='top'>
							<p>Perfils de Productor</p>
							</th>
							<td align='left' valign='top'>
							<select  id='sel_perfilsProductor' name='sel_perfilsProductor[]' multiple>
							<option value=''></option>
							</select>
							</td>
						</tr>
						";
					}
					echo "

						<tr>
							<th align='left' valign='top'>
							<p>Compte Ecos</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_compteEcos' name='i_compteEcos' size='8' value='".$mUsuari['compte_ecos']."'>
							</td>
						</tr>

						<tr ".$moneda3ElementsStyle.">
							<th align='left' valign='top'>
							<p>Compte ".$mParametres['moneda3Nom']['valor']."</p>
							</th>
							<td align='left' valign='top'>
							<p><input type='text' id='i_compteCieb' name='i_compteCieb' size='".$mParametres['moneda3DigitsCodi']['valor']."' value='".$mUsuari['compte_cieb']."'></p>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Contrasenya</p>
							</th>
							<td align='left' valign='top'>
							<p><input type='password' id='i_contrasenya' name='i_contrasenya' size='10' value='".$mUsuari['contrassenya']."'>*
							(<input type='checkBox' id='cb_veureContrasenya' onClick=\"veureContrasenyaFormNouUsuari()\" value='0'> Veure la contrasenya)</p>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>&nbsp;</p>
							</th>
							<td align='left' valign='top'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							";
							if($mPars['demo']==-1)
							{
								echo "
							<input type='button'   onClick=\"if(!checkFormNouUsuari()){alert(missatgeAlerta);}else{document.getElementById('f_nouUsuari').submit();}\" value='crear nou usuari'>
							<br>
							<p class='nota'>* els camps marcats amb asterisc son obligatoris</p>
								";	
							}
							else if($mPars['demo']==1)
							{
								echo "
							<input type='button' DISABLED  onClick=\"if(!checkFormNouUsuari()){alert(missatgeAlerta);}else{document.getElementById('f_nouUsuari').submit();}\" value='crear nou usuari'>
							<br>
							<p class='pAlertaNo4'>* en el MODE DE PROVA aquest formulari est� desabilitat</p>
								";	
							}
							echo "
							<br><br><br><br>&nbsp;
							</td>
						</tr>
					</table>
					<input type='hidden'  name='i_pars' value='".$parsChain."'>
					</form>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	";
}
else if($opcio=='editarUsuari')
{

	echo "
		<tr>
			<td align='left' style='width:100%'>
			<center><p>[Editar Usuari]</p></center>
			<form id='f_nouUsuari' name='f_nouUsuari' method='POST' action='nouUsuari.php' target='_self' >
			<input type='hidden' name='i_opcio' value='guardarUsuari'>
			<input type='hidden' name='i_id' value='".$mUsuari['id']."'>
			<table  align='center'  style='width:70%'>
				<tr>
					<td style='width:100%' align='left'>
					<table align='center'  style='width:100%'>
						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							<p  style='font-size:15px;'><i>- Dades de l'Usuari -</i></p>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Usuari (nick) </p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_usuari' name='i_usuari' size='30' value=\"".(urldecode($mUsuari['usuari']))."\">
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Nom </p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_nom' name='i_nom' size='30' value=\"".(urldecode($mUsuari['nom']))."\">
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Cognoms </p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_cognoms' name='i_cognoms' size='30' value=\"".(urldecode($mUsuari['cognoms']))."\">
							</td>
						</tr>
						<tr>
							<td align='left' valign='top'>
							<p><b>propietats</b></p>
							</td>
							<td align='left' valign='top'>
							<table width='100%' border='0'>
								<tr>
	";
	$disabled=" DISABLED ";

	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['usuari_id']==$mUsuari['id']){$disabled='';}
	echo "
									<td width='100%' > 
									<table width='100%' bgcolor='lightGrey'>
	";
	$mPropietatsUsuari=getPropietats($mUsuari['propietats']);
	$selected1='';
	$selected2='selected';
	while(list($propietat,$mOpcions)=each($mPropietatsUsuaris))
	{
		if(is_array($mOpcions)){asort($mOpcions);}
		if($propietat=='compteSoci')
		{
			$notaPropietat="* Compte de soci de la CIC, tipus 'coop'. Es necessari per disposar del saldo en ecos recolzats que ofereix la CIC a membres de comissi�";
		}
		else if($propietat=='comissio')
		{
			$notaPropietat="* Comissi� o comissions de la CIC de les que reps una assignaci�. Per seleccionar m�s d'una comissi� selecciona cada comissi� mentre tens premuda la tecla 'CONTROL'. Es necessari per disposar del saldo en ecos recolzats que ofereix la CIC a membres de comissi�";
		}
		else
		{
			$notaPropietat='';
		}
		if($propietat=='comissio')
		{
			echo "
										<tr>			
											<td align='left' valign='top'>
											<p>".$propietat.":</p>
											</td>
											<td align='left'  valign='top'>
											<select id='sel_".$propietat."' name='sel_".$propietat."[]' ".$disabled." multiple>
			";
			if(isset($mPropietatsUsuari[$propietat]))
			{
				$mComissionsUsuari=explode(',',$mPropietatsUsuari[$propietat]);
			}
			else
			{
				$mComissionsUsuari=array();
			}
			while(list($index,$opcio)=each($mOpcions))
			{
				if(array_key_exists($propietat,$mPropietatsUsuari) && in_array($index,$mComissionsUsuari))
				{
					$selected1='selected';
					$selected2='';
				}
				else
				{
					$selected1='';
				}
				echo "
											<option ".$selected1." value='".$index."'>".$opcio."</option>
				";
			}
			reset($mOpcions);
						
			echo "
											<option ".$selected2." value=''></option>
											</select>
											</td>
											<td align='left' valign='top'>
											<p class='p_micro2'>".$notaPropietat."</p>
											</td>
										</tr>
			";
		}
		else
		{
			echo "
										<tr>			
											<td align='left'  valign='top'>
											<p>".$propietat.":</p>
											</td>
											<td align='left'  valign='top'>
											<input type='text' size='10' id='sel_".$propietat."' name='sel_".$propietat."' ".$disabled." value='".@$mPropietatsUsuari[$propietat]."'>
											</td>
											<td align='left' valign='top'>
											<p class='p_micro2'  valign='top'>".$notaPropietat."</p>
											</td>
										</tr>
			";
		}
	}
	reset($mPropietatsUsuaris);
				
	echo "
									</table>
									</td>
								</tr>
							</table>
							</td>
						</tr>

						<tr>
							<td align='left' valign='top'>
							<p><b>grups</b></p>
							</td>
							<td align='left' valign='top'>
							<table width='100%' border='0'>
								<tr>
";
$style=" style='visibility:hidden; position:absolute;' ";
if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){$style='';}
echo "
									<td valign='top' align='left' width='49%'> 
									<p style='color:#aaaaaa;'>
														";
					while(list($index,$mGrup)=each($mGrups))
					{
						if(substr_count($mUsuari['grups'],','.$mGrup['ref'].',')>0)
						{
							echo "<font><img src='imatges/okp.gif'>&nbsp;".(urldecode($mGrup['nom']));
							if($mGrup['usuari_id']==$mUsuari['id'])
							{
								echo "&nbsp;[responsable]";
							}
							else
							{
								echo "&nbsp;[usuari]";
							}
							echo "</font><br>";
						}
					}
					reset($mGrups);
					echo "
									</p>
									</td>
									<td valign='top' align='left' width='1%'> 
									<p>&nbsp;&nbsp;</p>
									</td>
									<td width='50%' ".$style."> 
									<select id='sel_grups' name='sel_grups[]' multiple >
					";
					$selected1='';
					$selected2='selected';
					while(list($index,$mGrup)=each($mGrups))
					{
						if(substr_count($mUsuari['grups'],','.$mGrup['ref'].',')>0)
						{
							$selected1='selected';
							$selected2='';
						}
						else
						{
							$selected1='';
						}
						echo "
									<option ".$selected1." value='".$mGrup['id']."'>".(urldecode($mGrup['nom']))."</option>
						";
					}
					reset($mGrups);
					echo "
									<option ".$selected2." value=''></option>
									</select>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Perfils de Productor</p>
							</th>
							<td align='left' valign='top'>
							<table width='100%' border='0'>
								<tr>
";
$style=" style='visibility:hidden; position:absolute;' ";
if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){$style='';}
echo "

									<td valign='top' align='left' width='49%'> 
									<p style='color:#aaaaaa;'>
														";
					while(list($id,$mProductor)=each($mPerfilsUsuari))
					{
						if(substr_count($mUsuari['perfils_productor'],','.$id.',')>0)
						{
							echo "<font><img src='imatges/okp.gif'>&nbsp;".(urldecode($mProductor['projecte']));
							if(isset($mPerfilsUsuari[$id]) && $mPerfilsUsuari[$id]['usuari_id']==$mUsuari['id'])
							{
								echo "&nbsp;[responsable]";
							}
							else
							{
								echo "&nbsp;[usuari]";
							}
							echo "</font><br>";
						}
						
					}
					reset($mPerfilsUsuari);
					echo "
									</p>
									</td>

									<td valign='top' align='left' width='1%'> 
									<p>&nbsp;&nbsp;</p>
									</td>

									<td valign='top' align='left' width='50%' ".$style."> 
									<select id='sel_perfilsProductor'  name='sel_perfilsProductor[]' multiple>
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mProductor)=each($mPerfilsUsuari))
					{
						if(substr_count($mUsuari['perfils_productor'],','.$id.',')>0)
						{
							$selected1='selected';
							$selected2='';
						}
						else
						{
							$selected1='';
						}
						echo "
									<option ".$selected1." value='".$id."'>".(urldecode($mProductor['projecte']))."</option>
						";
					}
					reset($mPerfilsUsuari);
					echo "
									<option ".$selected2." value=''></option>
									</select>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						
						<tr>
							<th align='left' valign='top'>
							<p>Adre�a</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_adre�a' name='i_adre�a' size='50' value='".$mUsuari['adressa']."'>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Municipi</p>
							</th>
							<td align='left' valign='top'>
							<select id='sel_municipi' name='sel_municipi'>
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mMunicipi)=each($mMunicipis))
					{
						if($id==$mUsuari['municipi'])
						{
							$selected1='selected';
							$selected2='';
						}
						else
						{
							$selected1='';
						}
						echo "
							<option ".$selected1." value='".$id."'>".(urldecode($mMunicipis[$id]['municipi']))."</option>
						";
					}
					reset($mMunicipis);
					echo "
							<option ".$selected2." value=''></option>
							</select>
							</td>
						</tr>
";
$style=" style='visibility:hidden; position:absolute;' ";
if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){$style='';}
echo "

						<tr ".$style.">
							<th align='left' valign='top'>
							<p>Notes</p>
							</th>
							<td align='left' valign='top'>
							<textArea id='ta_notes'  name='ta_notes' cols='50' rows='7'>".(urldecode($mUsuari['notes']))."</textArea>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Email</p>
							</th>
							<td align='left' valign='top'>

";
$readonly=" READONLY ";
$class=" class='i_readonly3' ";
if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){$readonly=" "; $class='';}
echo "
							<input ".$class." type='text' id='i_email' name='i_email' size='30' ".$readonly." value='".$mUsuari['email']."'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Mobil</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_mobil' name='i_mobil' size='9' value='".$mUsuari['mobil']."'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Compte Ecos</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_compteEcos' name='i_compteEcos' size='8' value='".$mUsuari['compte_ecos']."'>
							</td>
						</tr>
						<tr ".$moneda3ElementsStyle.">
							<th align='left' valign='top' >
							<p>Compte ".$mParametres['moneda3Nom']['valor']."</p>
							</th>
							<td align='left' valign='top'>
							<p><input type='text' id='i_compteCieb' name='i_compteCieb' size='".$mParametres['moneda3DigitsCodi']['valor']."' value='".$mUsuari['compte_cieb']."'></p>
							</td>
						</tr>
							";
							if($mPars['demo']==-1 && $mUsuari['id']==$mPars['usuari_id'])
							{
								echo "
						<tr>
							<th align='left' valign='top'>
							<p>Contrasenya</p>
							</th>
							<td align='left' valign='top'>
							<input type='password' id='i_contrasenya' name='i_contrasenya' size='10' value='".$mUsuari['contrassenya']."'></p>
							(<input type='checkBox' id='cb_veureContrasenya' onClick=\"veureContrasenyaFormNouUsuari()\" value='0'> Veure la contrasenya)
							</td>
						</tr>
								";	
							}
							else
							{
								echo "
						<tr>
							<th align='left' valign='top'>
							<p>Contrasenya</p>
							</th>
							<td align='left' valign='top'>
							<input type='password' READONLY id='i_contrasenya' name='i_contrasenya' size='10' value='contrassenya'></p>
							</td>
						</tr>
								";	
							}
							echo "
						<tr>
							<th align='left' valign='top'>
							<p>&nbsp;</p>
							</th>
							<td align='left' valign='top'>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
						";
						if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
						{
							echo "
							<table width='100%' border='1'>
								<tr>
									<td align='center' valign='top'>
									";
									if($mUsuari['estat']=='inactiu' || $mUsuari['estat']=='' || $mUsuari['estat']=='pendent')
									{
										echo "
										<p class='pAlertaNo4'>Usuari INACTIU</p>
										<input type='button' onClick=\"enviarFpars('nouUsuari.php?iUed='+".$mUsuari['id']."+'&op=activarUsuari','_self')\" value='activar'>
										";
									} 
									else if($mUsuari['estat']=='actiu')
									{
										echo "
										<p class='pAlertaOk4'>USUARI ACTIU</p>
										<input type='button' onClick=\"enviarFpars('nouUsuari.php?iUed='+".$mUsuari['id']."+'&op=desactivarUsuari','_self')\" value='desactivar'>
										";
									} 
									echo "
									</td>
								</tr>
							</table>
							";
							if($mPars['nivell']=='sadmin')
							{
								echo "
							<table width='100%' border='1'>
								<tr>
									<td align='center' valign='top'>
									<br>
									<p>Nivell:&nbsp;
									<select onChange=\"enviarFpars('nouUsuari.php?iUed=".$mUsuari['id']."&op=canviNivellUsuari&iUn='+this.value,'_self');\">
									<option "; if($mUsuari['nivell']=='sadmin'){echo " selected ";} echo " value='admin'>sadmin</option>
									<option "; if($mUsuari['nivell']=='admin'){echo " selected ";} echo " value='admin'>admin</option>
									<option "; if($mUsuari['nivell']=='coord'){echo " selected ";} echo " value='coord'>coord</option>
									<option "; if($mUsuari['nivell']=='usuari'){echo " selected ";} echo " value='usuari'>usuari</option>
									</select>
									</p>
									</td>
								</tr>
								<tr>
									<td align='center' valign='top'>
									<br>
									<input type='button' onClick=\"enviarFpars('nouUsuari.php?iUed='+".$mUsuari['id']."+'&op=eliminarUsuari','_self')\" value='eliminar usuari'>
									<br>(* nom�s si no t� comanda en curs)
									</td>
								</tr>
							</table>
								";
							}
							else if($mPars['nivell']=='admin')
							{
								echo "
							<table width='100%' border='1'>
								<tr>
									<td align='center' valign='top'>
									<p>Nivell:&nbsp;
									<select onChange=\"enviarFpars('nouUsuari.php?iUed=".$mUsuari['id']."&op=canviNivellUsuari&iUn='+this.value,'_self');\">
									<option "; if($mUsuari['nivell']=='coord'){echo " selected ";} echo " value='coord'>coord</option>
									<option "; if($mUsuari['nivell']=='usuari'){echo " selected ";} echo " value='usuari'>usuari</option>
									</select>
									</p>
									</td>
								</tr>
							</table>
								";
							}
							
						}
						echo "
							</th>

							<td align='center' valign='top'>
							<table>
								<tr>
									<td align='center' valign='top'>
									<input type='button' onClick=\"if(!checkFormNouUsuari()){alert(missatgeAlerta);}else{document.getElementById('f_nouUsuari').submit();}\" value='guardar usuari'>
									</td>
									<td align='center'>
									</td>

								</tr>
							</table>
							</td>
						</tr>
					</table>
					<input type='hidden' name='i_pars' value='".$parsChain."'>
					</form>
					<br><br>
					&nbsp;
					</td>
				</tr>
			</table>
			</td>
		</tr>
	";
}
else if ($opcio=='solicitutEnviada')
{
	echo "
		<tr>
			<td align='left' style='width:100%'>
			<br>
			<center><p><b> * Sol.licitut Guardada *</b>
			</p></center>
			<br>
			
			<br>
			<table  align='center'  style='width:70%'>
				<tr>
					<td style='width:100%' align='left'>
					<table align='center'  style='width:100%'>
						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							<p  style='font-size:15px;'><i>- Dades de l'usuari -</i></p>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Usuari (Nick)</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_usuari' name='i_usuari'  READONLY size='30' value=\"".(urldecode($mUsuari['usuari']))."\">
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Nom </p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_nom' name='i_nom'  READONLY size='30' value=\"".(urldecode($mUsuari['nom']))."\">
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Cognoms </p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_cognoms' name='i_cognoms'  READONLY  size='30' value=\"".(urldecode($mUsuari['cognoms']))."\">
							</td>
						</tr>
						
						<tr>
							<th align='left' valign='top'>
							<p>Email</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_email' name='i_email' READONLY  size='30' value='".$mUsuari['email']."'>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Mobil</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_mobil'  READONLY  name='i_mobil' size='9' value='".$mUsuari['mobil']."'>
							</td>
						</tr>
					
						<tr>
							<td align='left' valign='top'>
							<p><b>grups</b></p>
							</td>
							<td align='left' valign='top'>
					";
					while(list($index,$mGrup)=each($mGrups))
					{
						if(in_array($mGrup['id'],$mUsuari['grups'])>0)
						{
							echo "
							<p>".(urldecode($mGrup['nom']))."</p>
							";
						}
					}
					reset($mGrups);
					echo "
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Adre�a</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_adre�a'  READONLY  name='i_adre�a' size='50' value='".$mUsuari['adressa']."'>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Municipi</p>
							</th>
							<td align='left' valign='top'>
							<p>".(@urldecode($mMunicipis[$mUsuari['municipi']]['municipi']))."</p>
							</td>
						</tr>
					";
					if($mPars['nivell']=='visitant' || $mPars['nivell']=='usuari'){$style=" style='visibility:hidden; position:absolute;'";}
					echo "
						<tr ".$style.">
							<th align='left' valign='top'>
							<p>Notes</p>
							</th>
							<td align='left' valign='top'>
							<textArea id='ta_notes' READONLY  name='ta_notes' cols='50' rows='7'>".(urldecode($mUsuari['notes']))."</textArea>
							</td>
						</tr>

						<tr ".$style.">
							<th align='left' valign='top'>
							<p>Propietats</p>
							</th>
							<td align='left' valign='top'>
							<textArea id='ta_propietats' READONLY  name='ta_propietats' cols='50' rows='7'>".(urldecode($mUsuari['propietats']))."</textArea>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Compte Ecos</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' READONLY id='i_compteEcos' name='i_compteEcos' size='8' value='".$mUsuari['compte_ecos']."'>
							</td>
						</tr>
						<tr ".$moneda3ElementsStyle.">
							<th align='left' valign='top'>
							<p>Compte ".$mParametres['moneda3Nom']['valor']."</p>
							</th>
							<td align='left' valign='top'>
							<p><input READONLY type='text' id='i_compteCieb' name='i_compteCieb' size='".$mParametres['moneda3DigitsCodi']['valor']."' value='".$mUsuari['compte_cieb']."'></p>
							<br><br>&nbsp;
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	";
}

echo "
	</table>
";
html_helpRecipient();

echo "
<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='nouUsuari.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</div>
	
</body>
</html>
";

?>
	




		