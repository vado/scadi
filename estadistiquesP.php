<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include 'html_estadistiquesP.php';
include "db_estadistiquesP.php";
include "html_ajuda1.php";
include "db_ajuda.php";

$mSb=array('index','producte','productor','quantitat','unitat_facturacio','pes','ums','ms','ecos','euros');
$mSbd=array('ascendent','descendent');


$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();
$parsChain=makeParsChain($mPars);

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db); //inicialitza variables anteriors;
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['estadistiquesP.php']=db_getAjuda('estadistiquesP',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$mRuta=explode('_',$mPars['selRutaSufix']);
if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}


$mProduccioXcsv=array();

	//$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='id';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vProducte']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';
	$mPars['paginacio']=-1;

	$mRutesSufixes=getRutesSufixes($db);
$mPeriodesInfo=db_getPeriodesInfo($db);
$mPerfilsRef=db_getPerfilsRef($db);
$mGrupsRef=db_getGrupsRef($db);

//*v36 20-1-16  3 assignacions
$mGrupsZones=getGrupsZones($mParametres['agrupamentZones']['valor']);
$mSzones=array_keys($mGrupsZones);
$mZones=db_getZones($db);
$mSelRutes=array();
$mSelRutes[0]=$ruta;
$mSelRutes_=@$_POST['sel_rutes'];
if(isset($mSelRutes_)){$mSelRutes=$mSelRutes_;}
//*v36 20-1-16  2 post (zona i szona)
$sSz_=@$_POST['sel_sZona'];
if(isset($sSz_)){$sSz=$sSz_;}else{$sSz='TOTS';}
$sZ_=@$_POST['sel_zona'];
if(isset($sZ_)){$sZ=$sZ_;}else{$sZ='TOTS';}
$sb_=@$_POST['sel_sb'];
if(isset($sb_)){$sb=$sb_;}else{$sb='euros';}
$mPars['sortBy']=$sb;
$sbd_=@$_POST['sel_sbd'];
if(isset($sbd_)){$sbd=$sbd_;}else{$sbd='descendent';}
$sg_=@$_POST['sel_prod'];
if(isset($sg_)){$sg=$sg_;$mPars['vProductor']=$sg_;}else{$sg='';}
echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>Dades de Producci�</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='comandes.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}
</SCRIPT>
</head>
<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('estadistiques.php?');
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			</td>
		</tr>
		<tr>
			<td style='width:100%;' align='center'>
			<br>
			<p>[ Dades de producci� ]</p>
			<p class='p_micro3'>* Correlaci� amb els albarans d'entrega a la CAC dels perfils de productor *</p>
<br><br>
";
mostrarSelectorRutaC('estadistiquesP.php');

if(count($mSelRutes)>0)
{
	$mProduccioX=db_getProduccio($db);
	html_Produccio();
	db_getEstadistiquesPcsv($db);
}
else
{
	echo "(no s'han seleccionat rutes cac per a generar les dades)";
}
echo "
			<br>
			<br>
			</td>
		</tr>
	</table>
";
//*v36 27-1-16 afegit taula prods no trobats
if(count($mProductesNoTrobats)!=0)
{
	echo "
	<center>
	<p style='color:red;'>
	Atenci�, els seg�ents productes s'han trobat a les comandes d'un periode<br>
	pero no a la taula de productes d'aquest periode:</p>
	<table border='1' bordercolor='red' width='30%'>
	";
	while(list($periode,$mIds)=each($mProductesNoTrobats))
	{
		while(list($key,$id)=each($mIds))
		{
			echo "
		<tr>
			<td>
			<p style='color:red;'>".$periode."</p>
			</td>
			
			<td>
			<p style='color:red;'>".$id."</p>
			</td>
		</tr>
			";	
		}
	}
	reset($mProductesNoTrobats);
	echo "
	</table>
	<p  style='color:red;'>
	* Aix� podria provocar errors en els resums on s'inclou aquest periode
	</p>
	
	</center>
	";
}
html_helpRecipient();
echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>

";

?>

		