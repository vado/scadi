<?php

//------------------------------------------------------------------------------
function db_getReservesZona($zona,$db)
{
	global $mPars,$mProductes,$mRebostsRef,$mReservesComandes,$jaEntregatTkg,$jaEntregatTuts,$rc,$mRecepcions,$sz,$mParametres,$mPerfilsRef,$mGrupsZones;
	
	
	$mReservesProductes=array();
	$puntsEntregaZona='';
	$grupsPuntsEntregaZona='';
	
	if($zona!='')
	{
		//get grups dins zona
		if(!$result=mysql_query("select ref from rebosts_".$mPars['selRutaSufix']." WHERE LOCATE('".$zona.",',categoria)>0",$db))
		{
			//echo "<br> 17 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		}
		else
		{ 
			$i=0;
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$puntsEntregaZona.=','.$mRow['ref'].',';
			}
		}
	}
	else
	{
		$zona='totes';
		//get grups dins zona
		if(!$result=mysql_query("select ref from rebosts_".$mPars['selRutaSufix'],$db))
		{
			//echo "<br> 34 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		}
		else
		{ 
			$i=0;
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$puntsEntregaZona.=','.$mRow['ref'].',';
			}
		}
	}
	$puntsEntregaZona=str_replace(',,',',',$puntsEntregaZona);
	
	//get grups amb punts entrega en els grups anteriors
	if(!$result=mysql_query("select rebost,punt_entrega from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',punt_entrega,','),CONCAT(' ','".$puntsEntregaZona."'))>0",$db))
	{
			//echo "<br> 54 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			$grupsPuntsEntregaZona.=','.$grupId.',';
			$mComandesGrups[$grupId]['punt_entrega']=$mRow['punt_entrega'];
		}
	}
	$grupsPuntsEntregaZona=str_replace(',,',',',$grupsPuntsEntregaZona);
	
	//getComandes dels usuaris dels grups dels puntsentrega dins zona
	$do=true;
	
	if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',SUBSTRING_INDEX(rebost,'-',1),','),CONCAT(' ','".$grupsPuntsEntregaZona."'))>0 AND usuari_id!=0",$db))
	{
			//echo "<br> 70 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			if($rc=='1')
			{
				if($mRecepcions[$grupId]['acceptada']==1)
				{
					$do=true;
				}
				else
				{
					$do=false;
				}
			}
			
			if(!isset($mReservesComandes[$zona]))
			{
				$mReservesComandes[$zona]=array();
			}
			if(!isset($mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']]))
			{
				$mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']]=array();
			}
			if(!isset($mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]))
			{
				$mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]=array();
			}
			$mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]['resum']=$mRow['resum'];

			if($do)				
			{
				$mComanda=explode(';',$mRow['resum']);
				while(list($key,$mVal)=each($mComanda))
				{
					$mIndexQuantitat=explode(':',$mVal);
					
					$index=str_replace('producte_','',$mIndexQuantitat[0]);
					$do2=true;
					if($sz!='')
					{
						if($index!='')
						{
							$productorId=substr($mProductes[$index]['productor'],0,strpos($mProductes[$index]['productor'],'-'));

							$superZonaProductor=getSuperZona($mPerfilsRef[$productorId]['zona']);
							if($superZonaProductor==$sz)
							{
								$do2=true;
							}
							else
							{
								$do2=false;
							}
						}
						else
						{
							$do2=false;
						}
					}
					else
					{
						$do2=true;
					}
					
					
					if($do2)
					{
						$quantitat=@$mIndexQuantitat[1];
						if($index!='')
						{
							if(array_key_exists($index,$mReservesProductes))
							{
								$mReservesProductes[$index]+=$quantitat;
							}
							else
							{
								$mReservesProductes[$index]=$quantitat;
							}
						}
					}
				}
			}
		}
	}
	if($mPars['excloureProductesJaEntregats']=='1')
	{
		$mProductesJaEntregats=db_getProductesJaEntregats($db);
		while(list($index,$mVal)=each($mProductesJaEntregats))
		{
			while(list($grupId,$mVal2)=each($mVal))
			{
						//echo "<br>".$mRebostsRef[$grupId]['nom']." (".$mRebostsRef[$grupId]['categoria'].") ".$mProductes[$index]['producte']."-".$mVal2['rebut'];
				if(substr_count($grupsPuntsEntregaZona,','.$grupId.',')>0)
				{
					if($index!='' && isset($mReservesProductes[$index]))
					{
						$mReservesProductes[$index]-=$mVal2['rebut'];
						$jaEntregatTkg+=$mVal2['rebut']*$mProductes[$index]['pes'];
						$jaEntregatTuts+=$mVal2['rebut'];
					}
				}
			}
		}
		reset($mProductesJaEntregats);
	}
	//vd($jaEntregatTkg);
	//incorporar reserves a productes
	
	while(list($index,$mProducte)=each($mProductes))
	{
		if(isset($mReservesProductes[$index]))
		{
			$mProductes[$index]['quantitat']=$mReservesProductes[$index];
		}
		else
		{
			$mProductes[$index]['quantitat']=0;
		}
	}
	reset($mProductes);


	return;
}

//------------------------------------------------------------------------------
function confirmarRecepcions($cadenaRecepcionsAConfirmar,$grupId,$db)
{
	global $mPars,$mMissatgeAlerta;
	
	
	if(!$result=mysql_query("update rebosts_".$mPars['selRutaSufix']." set recepcions=CONCAT(recepcions,'".$cadenaRecepcionsAConfirmar."') where id='".$grupId."'",$db))
	{
		//echo "<br> 154 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		
		$mMissatgeAlerta['missatge1'].="<p class='pAlertaNo'>Atenci�, no s'han pogut acceptar les recepcions dels grups seleccionats. Posa't en contacte amb l'administrador</p>";
		$mMissatgeAlerta['result1']=false;
	}
	else
	{ 
		$mMissatgeAlerta['missatge1'].="<p class='pAlertaOk'>Les recepcions dels grups seleccionats han estat acceptades</p>";
		if(!mail_recepcioAcceptada($cadenaRecepcionsAConfirmar,$grupId,$db))
		{
			$mMissatgeAlerta['missatge1'].="<p class='pAlertaNo'>Atenci�: no s'ha pogut enviar la corresponent notificaci� als responsables de grup</p>";
			$mMissatgeAlerta['result1']=false;
		}
		else
		{
			$mMissatgeAlerta['missatge1'].="<p class='pAlertaOk'>S'ha enviat una notificaci� per correu als responsables de cada grup</p>";
			$mMissatgeAlerta['result1']=true;
		}
	}

	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function db_getReservesCSV($db)
{
	global $mPars,$mReservesCsv,$superZona,$sz,$zona,$jaEntregatTkg;
	
	$dir0=getcwd();
	$dir="docs".$mPars['selRutaSufix'];
	if(!@chdir($dir))
	{
		mkdir($dir);
	}
	else
	{
		if(!chdir('reserves')){mkdir('reserves');}
		chdir($dir0);
	}
	@unlink("docs".$mPars['selRutaSufix']."/reserves/reserves_".$mPars['usuari_id'].".csv");

	if(!$fp=fopen("docs".$mPars['selRutaSufix']."/reserves/reserves_".$mPars['usuari_id'].".csv",'a'))
	{
		return false;
	}


	$mH1=array('vista reserves de zona: ');
	fputcsv($fp, $mH1,',','"');
	$mH1=array('superZona: '.$superZona);
	fputcsv($fp, $mH1,',','"');
	$mH1=array('zona: '.$zona);
	fputcsv($fp, $mH1,',','"');
	$mH1=array(date('d/m/Y hh:mm'));
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	
	if($mPars['excloureProductesJaEntregats']==1)
	{
		$mH1=array("* Mostrar els productes ja entregats a ".$zona." (".$jaEntregatTkg." kg): NO");
		fputcsv($fp, $mH1,',','"');
	}
	else
	{
		$mH1=array("* Mostrar els productes ja entregats a ".$zona." (".$jaEntregatTkg." kg): SI");
		fputcsv($fp, $mH1,',','"');
	}

	if($mPars['rc']==1)
	{
		$mH1=array("* Mostrar nom�s els productes de les recepcions confirmades per cada punt d'entrega: SI");
		fputcsv($fp, $mH1,',','"');
	}
	else
	{
		$mH1=array("* Mostrar nom�s els productes de les recepcions confirmades per cada punt d'entrega: NO");
		fputcsv($fp, $mH1,',','"');
	}

	if($mPars['elp']==1)
	{
		$mH1=array("* Mostrar els productes que no cal portar perqu� en algun magatzem de la Superzona local hi ha estoc positiu suficient: NO");
		fputcsv($fp, $mH1,',','"');
	}
	else
	{
		$mH1=array("* Mostrar els productes que no cal portar perqu� en algun magatzem de la Superzona local hi ha estoc positiu suficient: SI");
		fputcsv($fp, $mH1,',','"');
	}
	if($sz==''){$sz='(cap superZona seleccionada)';}
	$mH1=array("* Mostrar nom�s els productes dels productors gestionats desde la superzona seleccionada: ".$sz);
	fputcsv($fp, $mH1,',','"');
	
	
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');

	while(list($key,$mReservaCsv)=each($mReservesCsv))
	{
    	if(!fputcsv($fp, $mReservaCsv,',','"'))
		{
			return false;
		}
	}	
	reset($mReservesCsv);
	fclose($fp);
	return;
}

?>

		