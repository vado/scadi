<?php
//------------------------------------------------------------------------------
function mostrarFormRebosts()
{
//*v36 7-1-16 tot global
	global 	$mode,
			$mContinguts,
			$mRebosts,
			$login,
			$mRebostsAmbComanda,
			$mPars,
			$mParsCAC,
			$mGrupsPerSzIzona,
			$mGrupsZones,
			$mSelGrups,
			$mGrupsRef,
			$mGrupsActius,
			$mColors;
	
	$addComanda='';
	$optionFormat='';
		$szMem='';
		$colorIndex=1;
		$szColor[-1]['bGsz']='#8E2602'; //taronja fosc
		$szColor[-1]['bGz']='#EA3F03'; //taronja
		$szColor[1]['bGsz']='#075601'; //verd fosc
		$szColor[1]['bGz']='#0C8E02';//verd
		
	// form
	echo "
<form id='f_rebost' name='f_rebost' action='comandes.php' target='_blank' method='post'>
<table align='left'  bgcolor='".$mColors['table']."'>
	<tr>
		<td align='left' valign='top'>
		<p>Selecciona un GRUP:<br>
		<select id='sel_rebost' onchange=\"javascript:if(this.value!=''){enviarFpars('comandes.php?sUid=0&gId='+this.value,'_blank');}\">
	";
				while(list($szona,$mGrupsPerZona)=each($mGrupsPerSzIzona))
				{
					if($szMem!=$szona)
					{
						$colorIndex*=-1;
						$szMem=$szona;
					}
					echo "
				<option disabled style='color:white; font-size:13px; background-color:".$szColor[$colorIndex]['bGsz'].";' label='".$szona."'>--- ".$szona." ---</option>
					";
					while(list($zona,$mGrup)=each($mGrupsPerZona))
					{
						echo "
				<option disabled style='color:white; font-size:13px; background-color:".$szColor[$colorIndex]['bGz'].";' label='".$zona."'>- ".$zona." -</option>
						";
						while(list($index,$grupId)=each($mGrup))
						{
//*v36 7-1-16 condicional
							if(in_array($grupId,$mGrupsActius))
							{
								if(in_array($grupId,$mRebostsAmbComanda))
								{
									$optionFormat=" background-color:#F9E39F;"; 
									$addComanda="[comanda] ";
								}
								else
								{
									$optionFormat='';
									$addComanda='';
								}
							echo "
				<option  style=\"color:".$szColor[$colorIndex]['bGsz']."; ".$optionFormat."\" value='".$grupId."'>".$addComanda.(urldecode($mGrupsRef[$grupId]['nom']))."</option>
							";
							}
							else
							{
								if(in_array($grupId,$mRebostsAmbComanda))
								{
									$optionFormat=" background-color:#cccccc; color:red; '"; 
									$addComanda="[comanda] INACTIU - ";
								}
								else
								{
									$optionFormat=' color:red; ';
									$addComanda='INACTIU - ';
								}
							echo "
				<option  style=\"color:".$szColor[$colorIndex]['bGsz']."; ".$optionFormat."\" value='".$grupId."'>".$addComanda.(urldecode($mGrupsRef[$grupId]['nom']))."</option>
							";
							}
						}
						reset($mGrup);
					}
					reset($mGrupsPerZona);
				}
				reset($mGrupsPerSzIzona);
				
				echo "
				<option value='0'>CAC</option>
				<option selected value=''></option>
				</select>
		</p>
		</td>
	</tr>
</table>
</form>
	";
	return;
}

//------------------------------------------------------------------------------
function mostrarFormMagatzems()
{
	global $mContinguts,$mMagatzems,$mRebostsAmbComanda,$mPars,$mParsCAC,$mColors;
	
	
	$addComanda='';
	$optionFormat='';

	// form
	echo "
		<form id='f_rebost' name='f_rebost' action='gestioMagatzems.php' target='_self' method='post'>
		<p><b>Inventaris</b><br>
		Selecciona un MAGATZEM-CAC:<br>
		<select id='sel_rebost' name='sel_rebost' onchange=\"javascript:enviarFpars('gestioMagatzems.php?mRef='+this.value,'_blank');\">
	";
	$selected1='';
	$selected2='selected';
	for ($i=0;$i<count($mMagatzems);$i++)
	{	
		if($mPars['grup_id']==$mMagatzems[$i]['ref']){$selected1='selected';$selected2='';}else{$selected1='';}
		$zona_=substr($mMagatzems[$i]['categoria'],strpos($mMagatzems[$i]['categoria'],'2-')+2);
		$zona=substr($zona_,0,strpos($zona_,','));
		if($zona=='')	
		{
			$zona='('.$zona_.')';
		}
		else
		{
			$zona='('.$zona.')';
		}

		echo "
		<option ".$selected1." value=\"".$mMagatzems[$i]['ref']."\">".$addComanda.(urldecode($mMagatzems[$i]['nom']))."	".($zona)."</option>";		
	}
		
	echo "		
		<option value='0'>CAC</option>
		<option ".$selected2."></option>
		</select>
		</p>
		</form>
	";
	return;
}


//------------------------------------------------------------------------------
function html_triaGrup()
{
	global $mGrupsUsuari,$mPars,$parsChain,$mGrupsRef,$mUsuarisRef,$mGrupsAnteriorsAmbComandaUsuari,$mColors;
	echo "
<table align='left' width='100%'  bgcolor='".$mColors['table']."'>
	<tr>
		<td align='left' width='100%'>
		<form id='f_grup' name='f_grup' action='comandes.php?sUid=".$mPars['usuari_id']."' target='_blank' method='post'>
		<table align='left' width='100%'>
			<tr>
				<td align='left' valign='bottom' width='100%'>
				<p><b>Llista de productes</b><br>Selecciona el teu grup per fer comanda:<br>
				<select name='sel_grupUsuari' onChange=\"javascript: if(this.value!=''){document.getElementById('f_grup').submit();}\">								\">
				";
				while(list($grupId,$mGrupUsuari)=each($mGrupsUsuari))
				{
					if(substr_count(' '.$mGrupUsuari['categoria'],'inactiu')==0)
					{
						echo "
				<option   value='".$grupId."'>".(urldecode($mGrupUsuari['nom']))."&nbsp;&nbsp;&nbsp;RG:".(urldecode($mUsuarisRef[$mGrupsRef[$grupId]['usuari_id']]['usuari']))."</option>
						";
					}
					else
					{
						echo "
				<option   value='".$grupId."' style='color:red;'>INACTIU - ".(urldecode($mGrupUsuari['nom']))."&nbsp;&nbsp;&nbsp;RG:".(urldecode($mUsuarisRef[$mGrupsRef[$grupId]['usuari_id']]['usuari']))."</option>
						";
					}
				}
				reset($mGrupsUsuari);
				while(list($grupId,$mGrupUsuari)=each($mGrupsAnteriorsAmbComandaUsuari))
				{
					if(substr_count(' '.$mGrupUsuari['categoria'],'inactiu')==0)
					{
						echo "
				<option   value='".$grupId."'>".(urldecode($mGrupUsuari['nom']))."&nbsp;&nbsp;&nbsp;RG:".(urldecode($mUsuarisRef[$mGrupsRef[$grupId]['usuari_id']]['usuari']))."</option>
						";
					}
					else
					{
						echo "
				<option   value='".$grupId."' style='color:red;'>INACTIU - ".(urldecode($mGrupUsuari['nom']))."&nbsp;&nbsp;&nbsp;RG:".(urldecode($mUsuarisRef[$mGrupsRef[$grupId]['usuari_id']]['usuari']))."</option>
						";
					}
				}
				reset($mGrupsAnteriorsAmbComandaUsuari);
				echo "
				<option selected value=''></option>
				</select>
				".(html_ajuda1('html_index.php',3))."				
				</p>
				</td>
			</tr>
		</table>
		<input type='hidden' name='i_pars' value='".$parsChain."'>
		</form>
		</td>
	</tr>
</table>
	";

	return;
}



//------------------------------------------------------------------------------

function html_funcionsSadmin($opcio)
{
	global $mPars,$mUsuaris,$mUsuari,$mColors;
	
	echo "
	<center><p>[Funcions sAdmin]</p></center>
	<table width='80%' border='1' align='center' bgcolor='".$mColors['table']."'>
		<tr>
			<td width='33%' align='left'>
					<p>Accedir amb identitat de l'usuari:<br>
					<select name='sel_accesUsuariId' onChange=\"javascript:if(this.value!=''){iPars=document.getElementById('i_pars').value;document.getElementById('i_pars').value='';enviarFpars('index.php?'+this.value+'&demo=".$mPars['demo']."','_blank');document.getElementById('i_pars').value=iPars;}\">
					";
					while(list($key,$mUsuari_)=each($mUsuaris))
					{
						if($mUsuari_['id']!='6') //visitant
						{
							echo "
					<option "; 
							if($mUsuari_['estat']=='actiu')
							{
								echo " class='oGrupActiu' ";
							} 	
							else if($mUsuari_['estat']=='inactiu')
							{
								echo " class='oGrupInactiu' ";
							} 
							echo "  value=\"us=".$mUsuari_['id']."&pasw=".(MD5($mUsuari_['contrassenya']))."\">".(urldecode($mUsuari_['usuari']))." - ".$mUsuari_['email']."</option>
							";					
						}
					}
					reset($mUsuaris);
					echo "
					<option selected value=''></option>
					</select>
					<br>
					( * en verd els usuaris actius)
					</p>

			</td>

			<td width='33%' align='left'>
			</td>

			<td width='33%' align='left'>
			";
			if($mPars['demo']==-1)
			{
				echo "
			<p class='compacte' onClick=\"javascript:enviarFpars('uploadF.php','_blank');\"   style='color:#885500; cursor:pointer;'><u>pujar fitxer</u></p>
			<p class='compacte' onClick=\"javascript:enviarFpars('uploadF3.php','_blank');\"   style='color:#885500; cursor:pointer;'><u>pujar fitxer (proves)</u></p>
			<p class='compacte' onClick=\"javascript:enviarFpars('operacionsPuntuals.php','_blank');\"   style='color:#885500; cursor:pointer;'><u>Operacions puntuals</u></p>
				";
			}
			echo "
			</td>
		</tr>
	</table>

	";

	return;
}


//------------------------------------------------------------------------------
function html_funcionsAdmin($opcio)
{
	global $mUsuaris,$mColors;
	
	echo "
	<center><p>[Funcions administrador]</p></center>
	<table width='80%' border='1' align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td width='33%' align='left'>
					<p>Selecciona un Usuari per editar-lo:<br>
					<select id='sel_usuariId' name='sel_usuariId' onChange=\"this.value; enviarFpars('nouUsuari.php?iUed='+this.value+'&op=editarUsuari','_self');\">
					";
					while(list($key,$mUsuari_)=each($mUsuaris))
					{
						if($mUsuari_['id']!='6') //visitant
						{
							echo "
					<option "; 
							if($mUsuari_['estat']=='actiu')
							{
								echo " class='oGrupActiu' ";
							} 	
							else if($mUsuari_['estat']=='inactiu')
							{
								echo " class='oGrupInactiu' ";
							} 
							echo "  value='".$mUsuari_['id']."'>".(urldecode($mUsuari_['usuari']))." - ".$mUsuari_['email']."</option>
							";					
						}
					}
					reset($mUsuaris);
					echo "
					<option selected value=''></option>
					</select>
					<br>
					( * en verd els usuaris actius)
					</p>

					<input type='button' value='Crear nou usuari' onClick=\"enviarFpars('nouUsuari.php?op=nouUsuari','_self');\">

			</td>

			<td width='33%' align='left'>
			<p class='compacte' onClick=\"javascript:enviarFpars('parametres.php','_blank');\"   style='color:#885500; cursor:pointer;'><u>configuraci� par�metres</u></p>
			<p class='compacte' onClick=\"javascript: enviarFpars('nouGrup.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>crear/editar grup</u></p>
			<p class='compacte' onClick=\"javascript: enviarFpars('nouPerfilProductor.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>crear/editar perfil de productor</u></p>
			</td>

			<td width='33%' align='left'>
			<p class='compacte' onClick=\"javascript:enviarFpars('mail.php','_blank');\"   style='color:#885500; cursor:pointer;'><u>Operacions email</u></p>
			</td>
		</tr>
	</table>
	
	";

	return;
}

//------------------------------------------------------------------------------
function html_funcionsCoord($opcio,$db)
{
	global $mPars,$mPerfilsRef,$mVehicles,$mModuls,$mRutesSufixes,$mProductes,$mRuta,$mColors;
	
	echo "
	<center><p>[Funcions Coordinador]</p></center>
	<table width='80%' border='1' align='center' bgcolor='".$mColors['table']."'>
		<tr>
			<td width='33%' align='center'>
			<table align='center' width='100%'>
				<tr>
				<td  align='left' width='90%'>
				<p class='nota3'><b><i>&nbsp;&nbsp;Pre-abastiment:</i></b></p>
					<p  class='nota3' onClick=\"javascript: enviarFpars('comptes.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>resum balan�os intercanvi</u></p>
					<p  class='nota3' onClick=\"javascript: enviarFpars('comptesCes.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>llistat de comptes CES comandes actuals</u></p>

				<p class='nota3'><b><i>&nbsp;&nbsp;Abastiment:</i></b></p>
					<p   class='nota3'  style='cursor:pointer;' onClick=\"enviarFpars('vistaReservesSZ.php','_blank');\" >
					<u>llistats interns d'abastiment</u>
					</p>

				<p class='nota3'><b><i>&nbsp;&nbsp;Distribuci�:</i></b></p>
					";
					if($mPars['selRutaSufix']*1<=1503)
					{
						echo "
					<p  class='nota3' style='color:#000000; cursor:pointer;' onClick=\"enviarFpars('tramsRuta.php','_blank');\"><u>Full de ruta</u></p>
						";
					}
					echo "
					<p  class='nota3' onClick=\"javascript: enviarFpars('resumDeRuta.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>resum comandes (zones)</u></p>
					<p  class='nota3' onClick=\"javascript: enviarFpars('resumProductesJJAA.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>resum comandes productes JJAA</u></p>
					";
					//<p  class='nota3' style='color:#000000; cursor:pointer;' onClick=\"enviarFpars('resumProductesEspecials.php','_blank');\"><u>resum comandes productes especials</u></p>
					echo "
					<p class='nota3'><b><i>&nbsp;&nbsp;Post-distribuci�:</i></b></p>
					<p  class='nota3' onClick=\"javascript: enviarFpars('incidencies.php?opt2=incd','_blank');\" style='color:#000000;  cursor:pointer;'><u>registre d'incid�ncies</u></p>
					<p  class='nota3' onClick=\"javascript: enviarFpars('incidencies.php?opt2=abCa','_blank');\" style='color:#000000;  cursor:pointer;'><u>registre d'abonaments i c�rrecs</u></p>
				
					<p class='nota3'><b><i>&nbsp;&nbsp;Dades:</i></b></p>
		";
//*v36.2-condicio
		if(count($mRuta)==1) //no ruta especial
		{
			echo "
					<p  class='nota3' onClick=\"javascript: enviarFpars('calculsRecolzamentSelectiu.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>configuraci� recolzament selectiu ecos</u></p>
			";
		}
		echo "
					<p  class='nota3' onClick=\"javascript: enviarFpars('comandesUsuaris.php?prop=comissio','_blank');\" style='color:#000000;  cursor:pointer;'><u>resum comandes membres comissions</u></p>
					<a href='resum_recolzament_selectiu.ods' target='_blank'>resum_recolzament_selectiu (.ods)</a>
					<p  class='nota3' onClick=\"javascript: enviarFpars('estadistiques.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>dades de consum</u></p>
					<p  class='nota3' onClick=\"javascript: enviarFpars('estadistiquesP.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>dades de producci�</u></p>
					<p  class='nota3' onClick=\"javascript: enviarFpars('balance_comptes_ecos.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>balan� comptes ecos</u></p>
					<p class='nota3'><b><i>&nbsp;&nbsp;Llistes locals:</i></b></p>
					<p  class='nota3' onClick=\"javascript: enviarFpars('resumLlistesLocals.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>resum llistes locals</u></p>
					<p  class='nota3' onClick=\"javascript: enviarFpars('productesLlistesLocals.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>productes llistes locals</u></p>
					</td>
				</tr>
			</table>
					
			</div>
			</td>
			
			<td width='33%' align='center' valign='top'>
			<p><b>Productes</b></p>
			<p style='text-align:left;'><b>Llista de productes</b></p>
			";
	
	
		mostrarFormRebosts();
	
	echo "
			<table align='center' width='100%'>
				<tr>
					<td align='left' width='100%'>
					<p><b>Gesti� de productes</b>
					<br>Selecciona un perfil de productor: 
					<br>
					<select id='sel_perfilUsuari' onChange=\"javascript:if(this.value!=''){enviarFpars('gestioProductes.php?opt=inici&plId='+this.value,'_blank');}\">
	";
	while(list($perfilId,$mPerfil)=each($mPerfilsRef))
	{
		if($mPerfil['estat']=='0'){$disabled='disabled';}else{$disabled='';}
		echo "
			<option ".$disabled." value='".$perfilId."'>".(urldecode($mPerfil['projecte']))."-".$perfilId."</option>
		";
	}
	reset($mPerfilsRef);
		
	echo "
					<option selected value=''></option>
					</select>
					</p>
	";
//*v36-3-12-15-condicional
	if(count($mRuta)==1)
	{
		mostrarFormMagatzems();
	}
	echo "
					</td>
				</tr>
			</table>
	";
//*v36-3-12-15- afegir condicional
	if(count($mRuta)==1)
	{
		echo "
			<table align='center' width='100%'>
				<tr>
					<td align='left' width='100%'>
					<p><b>Moviments d'un producte</b>
					<br>Selecciona un producte: 
					<br>
					<select id='sel_movIdProducte' onChange=\"javascript:if(this.value!=''){enviarFpars('movimentsProductes.php?pId='+this.value,'_blank');}\">
		";
		asort($mProductes);
		while(list($producteId,$mProducte)=each($mProductes))
		{
			if($mProducte['actiu']=='1'){$o_class='oGrupActiu';}else{$o_class='oGrupInactiu';}
			echo "
					<option class='".$o_class."' value='".$producteId."'>".$producteId." (".(substr((urldecode($mProducte['productor'])),0,10))."...) ".(substr(urldecode($mProducte['producte']),0,30))."...</option>
			";
		}
		reset($mProductes);
		
		echo "
					<option selected value=''></option>
					</select>
					</p>
					</td>
				</tr>
			</table>
		";
	}
	echo "
			<p class='p_micro2'>* els productes actius es mostren en verd</p>
			</td>
			<td width='33%' align='center' valign='top'>
			</td>
		</tr>
	</table>
	
	";

	return;
}

//------------------------------------------------------------------------------
function html_funcionsUsuari($opcio)
{
//*v36.3-global
	global $mPars,$mGrupsUsuari,$mPerfilsUsuari,$mVehiclesUsuari,$mModuls,$mRuta,$mColors;
	echo "
	<center><p>[Usuari]</p></center>
	<table width='80%' border='1' align='center' valign='top' bgcolor='".$mColors['table']."'>
		<tr>
			<td width='33%' align='center'>
			<p style='cursor:pointer;' onClick=\"javascript:enviarFpars('nouUsuari.php','_self');\"><u>p�gina d'usuari@s</u>
			".(html_ajuda1('html_index.php',1))."
			</p>
			<a href='https://intern.cooperativaintegral.cat/ca/form/formulari-dalta-soci-afi' target='_blank' class='a_important'>fer-se socia af� de la CIC</a>
			".(html_ajuda1('index.php',1))."
			</p>
			</td>

			<td width='33%' align='center' valign='top'>
			<p><b>Productes</b></p>
	";
	html_triaGrup();
	//echo "<p>(estem realitzan treballs de manteniment de l'aplicatiu)</p>";
	

	if(count($mPerfilsUsuari)>0)
	{
		//gestio de productes per part del responsable del perfil
		echo "
			<table align='center' width='100%'>
				<tr>
					<td align='left' width='100%'>
					<p><b>Gesti� de productes</b>
					<br>Selecciona el teu perfil de productor: 
					<br>
					<select id='sel_perfilUsuari' onChange=\"javascript:if(this.value!=''){enviarFpars('gestioProductes.php?opt=inici&plId='+this.value,'_blank');}\">
		";
		while(list($perfilId,$mPerfil)=each($mPerfilsUsuari))
		{
			if($mPerfil['estat']=='0'){$disabled='disabled';}else{$disabled='';}
			echo "
					<option ".$disabled." value='".$perfilId."'>".(urldecode($mPerfil['projecte']))."</option>
			";
		}
		reset($mPerfilsUsuari);
		
		echo "
					<option selected value=''></option>
					</select>
					".(html_ajuda1('html_index.php',2))."
					</p>
					</td>
				</tr>
			</table>
		";
	}
	echo "			
			</td>
			<td width='33%' align='center' valign='top'>
";
//*v36.3-condicional
if($mModuls['serveis']==1 && count($mRuta)==1)
{
	echo "
			<p><b>Serveis</b></p>
			<p><b>- Transport - </b>
			<br>
			<p class='p_micro3'>[ servei en periode de proves ]</p>
		<table border='0' align='left' >
			<tr>
				<td  valign='middle' align='left' >
				<a title='P�gina Serveis de Transport'><img src='imatges/cotxep.gif' ALT=\"p�gina d'usuari\" style='cursor:pointer' onClick=\"javascript: enviarFpars('contractesTrams.php?','_blank')\"></a>
				</td>
				<td  valign='middle' align='center' >
				<p class='compacte' onClick=\"javascript: enviarFpars('contractesTrams.php?tip=OFERTES','_blank');\" 	style='color:#885500; cursor:pointer;'><u>OFERTES</u></p>
				</td>
				<td  valign='middle' align='center'>
				<p class='compacte' style='color:#885500;'>|</p>
				</td>
				<td  valign='middle' align='center'>
				<p class='compacte' onClick=\"javascript: enviarFpars('contractesTrams.php?tip=DEMANDES','_blank');\" 	style='color:#885500; cursor:pointer;'><u>DEMANDES</u></p>
				</td>
			</tr>
		</table>
			";
}
		echo "
			</td>
		</tr>
	</table>
	
	";

	return;
}

//------------------------------------------------------------------------------
function html_funcionsVisitant($opcio)
{
	global $mColors;
	
	echo "
	<center><p>[Visitant]</p></center>
	<table width='80%' border='1' align='center' bgcolor='".$mColors['table']."'>
		<tr>
			<td width='33%' align='center'>
			<p style='cursor:pointer;' onClick=\"javascript:enviarFpars('nouUsuari.php?op=nouUsuari','_self');\"><u>solicitar alta usuari</u></p>
			</td>

			<td width='33%' align='center' valign='top' valign='top'>
	";
	html_triaGrup();
	//echo "<p>(estem realitzan treballs de manteniment de l'aplicatiu)</p>";
	
	echo "	
			</td>

			<td width='33%' align='center' valign='top'>
			</td>
		</tr>
	</table>
	
	";

	return;
}
//------------------------------------------------------------------------------
function hmtl_info()
{
	global $mText,$mColors;

	echo "
	<table width='80%' align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td width='100%'>
			<table width='50%' align='center'>
				<tr>
					<td width='100%'>
					<p><b>Informaci�: </b></p>
					".$mText['info'][0]."
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	";

return;
}
?>

		