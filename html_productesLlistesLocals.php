<?php
//------------------------------------------------------------------------------
function mostrarSelectorRutaC($desti)
{
	global 	$mGrupsRef,
			$mLlistesRef,
			$mRutesSufixes,
			//$mSelRutes,
			$mPars,
			$mMesos,
			$parsChain,
			$sb,
			$sbd,
			$mSb,
			$mSbd,
			$sg,
			$mPerfilsRef,
			$perfilsRefActiusChain,
			//$mPeriodesInfo,
			$mSzones,
			$mZones,
			$sZ,
			$sSz,
			$mSl;

	echo "
	<form id='f_selRutes' action='".$desti."' method='POST' target='_self'>
	<table width='70%' align='center'>
		<tr>
			<td align='left' valign='top' width='16%'>
			<p>
			SuperZona:
			<br> 
			<select id='sel_sZona' name='sel_sZona'  onChange=\"javascript:document.getElementById('s_enviar1').style.backgroundColor='orange';document.getElementById('sel_zona').value='TOTS';document.getElementById('sel_llista').value='0';document.getElementById('sel_prod').value='TOTS';\">
		";
			$selected='';
			$selected2='selected';
			while(list($index,$sZona)=each($mSzones))
			{
				if($sSz==$sZona)
				{
					$selected='selected';$selected2='';}else{$selected='';
				}
					
				echo "
			<option ".$selected." value='".$sZona."'>".$sZona."</option>
				";
			}
			reset($mSzones);
			echo "
			<option ".$selected2." value='TOTS'>- totes les SuperZones - </option>
			</select>
			<br>
			</td>
			
			<td align='left' valign='top' width='16%'>
			<p>
			Zona:
			<br> 
			<select id='sel_zona' name='sel_zona'  onChange=\"javascript:document.getElementById('s_enviar1').style.backgroundColor='orange';document.getElementById('sel_sZona').value='TOTS';document.getElementById('sel_llista').value='0';document.getElementById('sel_prod').value='TOTS';\">
		";
			$selected='';
			$selected2='selected';
			while(list($index,$zona)=each($mZones))
			{
				if($sZ==$zona)
				{
					$selected='selected';$selected2='';}else{$selected='';
				}
					
				echo "
			<option ".$selected." value='".$zona."'>".$zona."</option>
				";
			}
			reset($mZones);
			echo "
			<option ".$selected2." value='TOTS'>- totes les Zones - </option>
			</select>
			</p>		
			</td>

";
/*
echo "
			<td  width='16%' align='left' valign='top'>
			<p>
			Periode:
			<br>
			<select class='seleccionada2' name='sel_rutes[]'  multiple>
	";
	$selected='';
//*v36-while

	while(list($index,$ruta_)=each($mRutesSufixes))
	{
		$mRuta_=explode('_',$ruta_);
		if(count($mRuta_)>=2) //ruta especial
		{
			if($mRuta_[1]!='grups')
			{
				if(in_array($ruta_,$mSelRutes)){$selected='selected';}else{$selected='';}
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp=$mRuta_[0];
				$color='blue';
				echo "
			<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[1],2,2))])." ".(substr($mRuta_[1],0,2)).$text."</option>
				";
			}
		}
		else
		{
			if($mRuta_[0]!='grups')
			{
				if(in_array($ruta_,$mSelRutes)){$selected='selected';}else{$selected='';}
				$color='black';
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp='';
				echo "
					<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[0],2,2))])." ".(substr($mRuta_[0],0,2)).$text."</option>
				";
			}
		}
	}
	reset($mRutesSufixes);
	echo "
			</select>
			<p class='p_micro'>* select m�ltiple</p>
			</td>
";
*/
echo "

			<td  width='16%' align='left' valign='top'>
			<p>
			Productora:
			<br>
			<select class='seleccionada2' id='sel_prod' name='sel_prod'  onChange=\"javascript:document.getElementById('s_enviar1').style.backgroundColor='orange';document.getElementById('sel_sZona').value='TOTS';document.getElementById('sel_zona').value='TOTS';\">
	";
	$selected='';
	$selected2='selected';
	while(list($perfilId,$mPerfil)=each($mPerfilsRef))
	{
		if($perfilId!='0')
		{
			if($perfilId==$sg){$selected='selected';$selected2='';}else{$selected='';}
			if(substr_count($perfilsRefActiusChain,','.$perfilId.',')>0)
			{
				$disabled='';
			}
			else
			{
				$disabled=' DISABLED ';
			}
			echo "
			<option ".$selected." ".$disabled." value='".$perfilId."'>".(urldecode($mPerfil['projecte']))."</option>
			";
		}
	}
	reset($mRutesSufixes);
	echo "
			<option ".$selected2." value=''>- totes les productores -</option>
			</select>
			<p class='p_micro'>* nom�s es mostren els productes dels perfils actius</p>
			</td>

			<td  width='16%' align='left' valign='top'>
			<p>
			Llista:
			<br>
			<select class='seleccionada2' id='sel_llista' name='sel_llista[]' multiple onChange=\"javascript:document.getElementById('s_enviar1').style.backgroundColor='orange';document.getElementById('sel_sZona').value='TOTS';document.getElementById('sel_zona').value='TOTS';\">
	";
	$selected='';
	$selected2='selected';
	while(list($index,$llistaId)=each($mLlistesRef))
	{
		if(in_array($llistaId,$mSl)){$selected='selected';$selected2='';}else{$selected='';}
		echo "
			<option ".$selected." value='".$llistaId."'>".(urldecode($mGrupsRef[$llistaId]['nom']))."</option>
		";
	}
	reset($mLlistesRef);
	echo "
			<option value=''></option>
			</select>
			<p class='p_micro'>* select m�ltiple</p>
			</td>

			<td  width='16%' align='left' valign='top'>
			<p> ordenar per:<br>
			<select id='sel_sb' name='sel_sb'>
			";
			$selected='';
			while(list($key,$val)=each($mSb))
			{
				if($val==$sb){$selected='selected';}else{$selected='';}
				echo "
			<option ".$selected." value='".$val."'>".$val."</option>
				";
			}
			reset($mSb);
			echo "
			</select>
			</p>
			</td>

			<td  width='16%' align='left' valign='top'>
			<p>en sentit:<br>
			<select id='sel_sbd' name='sel_sbd'>
			";
			$selected='';
			while(list($key,$val)=each($mSbd))
			{
				if($val==$sbd){$selected='selected';}else{$selected='';}
				echo "
			<option ".$selected." value='".$val."'>".$val."</option>
				";
			}
			reset($mSbd);
			echo "
			</select>
			</p>

			<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
			<input type='submit' id='s_enviar1' value='enviar'\">
			</p>
			</td>
		</tr>
	</table>
	</form>
	";

	return;
}

//------------------------------------------------------------------------------
function html_produccio()
{
	global $mRutesSufixes,$mSelRutes,$mPars,$mProduccioX,$sb,$sbd,$mSb,$mSbd,$mProduccioXcsv,$mPerfilsRef,$sg,$mSl;

		$mTotals=array();
		$mBgColor['-1']='#ffffff';
		$mBgColor['1']='#dddddd';
		$colorIndex=-1;
	echo "
	<table style='width:100%;'>
		<tr>
			<td style='width:100%;'>
			<table border='0' style='width:100%;'>
				<tr>
					<td widht='50%' align='left'>
	";
	if(!isset($mSl) || count($mSl)==0 || $mSl[0]==0)
	{
		echo "
					<p style='text-align:center; color:red;' > Atenci�: no s'ha seleccionat cap llista de productes</b></p>
		";
	}
	if($sg!='')
	{
		echo "
					<p style='text-align:'center;'>* productes del perfil de productora <b>'".(urldecode($mPerfilsRef[$sg]['projecte']))."</b>'</p>
		";
	}
	if($mPars['veureProductesDisponibles']==1)
	{
		echo "
					<p style='text-align:'center;'>* nom�s es mostren els productes <b>actius</b></p>
		";
	}
	echo "
					<p style='text-align:'center;'>* productes ordenats per <b>'".$sb."</b>' en sentit <b>'".$sbd."'</b></p>
					</td>

					<td widht='50%' align='right'>
	";
	/*
	if(count($mSelRutes)>0)
	{
		echo "
					<a href='docs".$mPars['selRutaSufix']."/estadistiquesP_".$mPars['usuari_id'].".csv' target='_blank'>descarregar .csv (vista actual)</a>
		";
	}
	*/
	echo "
					</td>
				</tr>
			</table>
			<table border='1' style='width:100%;'>
				<tr>
					<td></td>
					";
					//$mConsumXcsv[0]=array();
					while(list($key,$val)=each($mSb))
					{
					//echo $val.'<br>';
						if($sb==$val){echo "<td bgcolor='#9CE6E3' align='center'><p style='font-size:15px;'><b>".$val."</b></p></td>";}
						else {echo "<td bgcolor='#9CE6E3' align='center'><p  style='font-size:15px;'>".$val."</p></td>";}
					//	array_push($mConsumXcsv[0],$val);
					}
					reset($mSb);
					echo "
				</tr>			
					";

				$cont=0;
				while(list($quantitat,$mProduccioQt_)=each($mProduccioX))
				{
					while(list($index,$mProduccioProducte)=each($mProduccioQt_))
					{
					
						echo "
				<tr >
					<td>".$cont."</td>
						";
						$mProduccioProducte_=array();
						while(list($key,$val)=each($mSb))
						{
							if($sb==$val)
							{
								if($val=='productor' || $val=='producte' || $val=='unitat_facturacio' || $val=='ms')
								{
									//$mProduccioProducte[$val]=urldecode($mProduccioProducte[$val]);
									echo "
					<td style='background-color:".$mBgColor[$colorIndex].";'><p><b>".(urldecode($mProduccioProducte[$val]))."</b></p></td>
									";
								}
								else
								{
									//if(!isset($mTotals[$val])){$mTotals[$val]=$mProduccioProducte[$val];}
									//else {$mTotals[$val]+=$mProduccioProducte[$val];}
									echo "
					<td style='background-color:".$mBgColor[$colorIndex].";'><p><b>".$mProduccioProducte[$val]."</b></p></td>
									";
									//$mProduccioProducte[$val]=str_replace('.',',',$mProduccioProducte[$val]);
									
								}
							}
							else 
							{
								if($val=='productor' || $val=='producte'  || $val=='unitat_facturacio'  || $val=='ms')
								{
									//$mProduccioProducte[$val]=@urldecode($mProduccioProducte[$val]);
									echo "
					<td style='background-color:".$mBgColor[$colorIndex].";'><p>".(urldecode($mProduccioProducte[$val]))."</p></td>
									";
								}
								else
								{
									//if(!isset($mTotals[$val])){$mTotals[$val]=$mProduccioProducte[$val];}
									//else {$mTotals[$val]+=$mProduccioProducte[$val];}
									echo "
					<td style='background-color:".$mBgColor[$colorIndex].";'><p>".$mProduccioProducte[$val]."</p></td>
									";
									//$mProduccioProducte[$val]=str_replace('.',',',$mProduccioProducte[$val]);
								}
							}
							//$mProduccioProducte_[$val]=$mProduccioProducte[$val];
						}
						reset($mSb);
						//array_push($mProduccioXcsv,$mProduccioProducte_);
						echo "
				<tr>			
						";
						$cont++;
						$colorIndex*=-1;
					}
					reset($mProduccioQt_);
				}
				reset($mProduccioX);
				echo "
				<tr>
					<td></td>
					";
					//$mConsumXcsv[0]=array();
					while(list($key,$val)=each($mSb))
					{
					//echo $val.'<br>';
						if($sb==$val){echo "<td bgcolor='#9CE6E3' align='center'><p style='font-size:15px;'><b>".$val."</b></p></td>";}
						else {echo "<td bgcolor='#9CE6E3' align='center'><p  style='font-size:15px;'>".$val."</p></td>";}
					//	array_push($mConsumXcsv[0],$val);
					}
					reset($mSb);
					echo "
				</tr>			
				<tr>
					";
					/*
					while(list($key,$val)=each($mSb))
					{
						if(array_key_exists($val,$mTotals))
						{
							echo "
					<td ><p style='font-size:15px;'><b>".(number_format($mTotals[$val],2))."</b></p></td>";
						}
						else 
						{
							echo "
					<td ><p  style='font-size:15px;'></p></td>
							";
						}
					}
					reset($mSb);
					*/
					echo "
				</tr>			
			</table>
			</td>
		</tr>
	</table>
	";


	return;
}

//------------------------------------------------------------------------------
function html_resumLlistesLocals()
{
	global $mPars,$mComandesLlistesLocals,$mGrupsRef,$mUsuarisRef,$mPeriodes;
	
	$mColor=array();
	$mColor[1]='#9FdEe0'; //CadetBlue
	$mColor[-1]='#7FbEc0';
	$colorIndex=1;
	$mEstat=array();
	$mEstat[1]="<font color='red'><b>TANCAT</b></font>";
	$mEstat[0]='------';
	$mEstat[-1]="<font color='green'><b>OBERT</b></font>";
	
	echo "
	<table border='1' style='width:100%;'>
		<tr>
			<td>
			<p class=''>Llista</p>
			</td>
			<td>
			<p class=''>periodes locals</p>
			</td>
			<td>
			<p class=''>comandes</p>
			</td>
		</tr>
	";
	while(list($grupId,$mComandesGrup)=each($mComandesLlistesLocals))
	{
		echo "
		<tr>
		";
		$cont1=0;
		while(list($periodeId,$mComandesPeriode)=each($mComandesGrup))
		{
			$cont2=0;
				$mPropietatsPeriodeLocal=getPropietatsPeriodeLocal($mPeriodes[$periodeId]['propietats']);
			while(list($usuariId,$mComanda)=each($mComandesPeriode))
			{

					echo "
		<tr>
			<td>
					";
					if($cont1==0)
					{
						echo "
				<p class=''>".(urldecode($mGrupsRef[$grupId]['nom']))."</p>
						";
					}
					echo "
			</td>
			<td bgcolor='".$mColor[$colorIndex]."'>
					";
					if($cont2==0)
					{
						echo "
				<p class=''><b>".$mPeriodes[$periodeId]['periode_comanda']."</b> (".$periodeId.", estat: ".$mEstat[$mPropietatsPeriodeLocal['comandesLocalsTancades']].") [".(count($mComandesPeriode))." comandes]</p>
						";
					}
					echo "
			</td>
			<td bgcolor='".$mColor[$colorIndex]."'>
			<p class=''>".$mComanda['id']." (".(urldecode($mUsuarisRef[$mComanda['usuari_id']]['usuari'])).")</p>
			</td>
		</tr>
					";
					$cont2++;
					$cont1++;
			}
			reset($mComandesPeriode);
			$colorIndex*=-1;
		}
		reset($mComandesGrup);
	}
	reset($mComandesLlistesLocals);
	echo "
	</table>
	";
	


	return;
}
?>

		