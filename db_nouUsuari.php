<?php
//------------------------------------------------------------------------------
function putNouUsuari($opcio,$mUsuari,$db)
{
	global $mPars;

	$chainGrups=',';
	if(is_array($mUsuari['grups']))
	{
		for($i=0;$i<count($mUsuari['grups']);$i++)
		{
			$chainGrups.=$mUsuari['grups'][$i].',';
		}
	}
	$chainPerfilsProductor=',';
	if(is_array($mUsuari['perfils_productor']))
	{
		for($i=0;$i<count($mUsuari['perfils_productor']);$i++)
		{
			$chainPerfilsProductor.=','.$mUsuari['perfils_productor'][$i].',';
		}
		$chainPerfilsProductor=str_replace(',,',',',$chainPerfilsProductor);
		
	}
	
	if($opcio=='guardarUsuari')
	{
		$result=mysql_query("select * from usuaris where id='".$mUsuari['id']."'",$db);
		
		if($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'  || $mPars['usuari_id']==$mUsuari['id'])
			{
				if(!$result=mysql_query("update usuaris set usuari='".$mUsuari['usuari']."',nom='".$mUsuari['nom']."',cognoms='".$mUsuari['cognoms']."',email='".$mUsuari['email']."',adressa='".$mUsuari['adressa']."',municipi='".$mUsuari['municipi']."',mobil='".$mUsuari['mobil']."',propietats='".$mUsuari['propietats']."',grups='".$chainGrups."',contrassenya='".$mUsuari['contrassenya']."',notes='".$mUsuari['notes']."',perfils_productor='".$chainPerfilsProductor."',compte_ecos='".$mUsuari['compte_ecos']."',compte_cieb='".$mUsuari['compte_cieb']."' where id='".$mUsuari['id']."' ",$db))
				{
					//echo "<br>  2580 db.php".mysql_errno() . ": " . mysql_error(). "\n";
					return false;
				}
				else
				{
					$message='{';
					while(list($key,$val)=each($mRow))
					{
						$message.=$key.':'.$val.';';
					}
					$message.='}\n';
					error_log($message,'3','historial/usuaris/usuari_'.$mUsuari['id'].'.php');
				}
			}
			else if($mPars['nivell']!='visitant')
			{
				if(!$result=mysql_query("update usuaris set usuari='".$mUsuari['usuari']."',nom='".$mUsuari['nom']."',cognoms='".$mUsuari['cognoms']."',adressa='".$mUsuari['adressa']."',municipi='".$mUsuari['municipi']."',mobil='".$mUsuari['mobil']."',contrassenya='".$mUsuari['contrassenya']."',notes='".$mUsuari['notes']."',compte_ecos='".$mUsuari['compte_ecos']."',compte_cieb='".$mUsuari['compte_cieb']."' where id='".$mUsuari['id']."' ",$db))
				{
					//echo "<br>  2580 db.php".mysql_errno() . ": " . mysql_error(). "\n";
					return false;
				}
				else
				{
					$message='{';
					while(list($key,$val)=each($mRow))
					{
						$message.=$key.':'.$val.';';
					}
					$message.='}\n';
					error_log($message,'3','historial/usuaris/usuari_'.$mUsuari['id'].'.php');
				}
			}
		}
	}
	else if($opcio=='crearUsuari')
	{
		//comprovar si ja existeix usuari amb el mateix correu:
		$result=mysql_query("select * from usuaris where email='".$mUsuari['email']."'",$db);
		//echo "<br>  35 db_nouUsuari.php".mysql_errno() . ": " . mysql_error(). "\n";
		
		if(!$mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			if(!$result=mysql_query("insert into usuaris values('','".$mUsuari['usuari']."','".$mUsuari['nom']."','".$mUsuari['cognoms']."','".$mUsuari['email']."','".$mUsuari['adressa']."','".$mUsuari['municipi']."','".$mUsuari['mobil']."','".$mUsuari['propietats']."','".$chainGrups."','','".$mUsuari['contrassenya']."','".$mUsuari['nivell']."','','".$mUsuari['estat']."','".$chainPerfilsProductor."','".$mUsuari['compte_ecos']."','".$mUsuari['compte_cieb']."','".$mUsuari['data']."')",$db))
			{
				//echo "<br>  2580 db.php".mysql_errno() . ": " . mysql_error(). "\n";
				return false;
			}
			else
			{
				$result=mysql_query("select * from usuaris where email='".$mUsuari['email']."'",$db);
				$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
				$message='{';
				while(list($key,$val)=each($mRow))
				{
					$message.=$key.':'.$val.';';
				}
				$message.='}/n';
				error_log($message,'3','historial/usuaris/usuari_'.$mRow['id'].'.php');
				
				// enviar peticions a grups
				$usuariId_=$mPars['usuari_id'];
				$mPars['usuari_id']=$mRow['id'];
				while(list($key,$id)=each($mUsuari['grups']))
				{
					put_peticioGrup('altaGrup',$id,$db);
				}
				$mPars['usuari_id']=$usuariId_;
				
			}
		}
		else
		{
			return false;
		}
	}
	
	return true;
}

//------------------------------------------------------------------------------
function db_activarUsuari($opcio,$db)
{
	global $mPars;
	
	$mOpcio[0]='inactiu';
	$mOpcio[1]='actiu';
	
	if(!$result=mysql_query("update usuaris set estat='".$mOpcio[$opcio]."' where id='".$mPars['selUsuariId']."'",$db))
	{
		//echo "<br>  74 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}	

	return true;
}


//------------------------------------------------------------------------------
function db_canviarNivellUsuari($db)
{
	global $mPars;
	if(!$result=mysql_query("update usuaris set nivell='".$mPars['selUsuariNivell']."' where id='".$mPars['selUsuariId']."'",$db))
	{
		//echo "<br>  107 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}	

	return true;
}

//------------------------------------------------------------------------------
function db_delUsuari($db)
{
	global $mPars;
	
	$result=mysql_query("select from comandes_".$mPars['selRutaSufix']." where usuari_id='".$mPars['selUsuariId']."'",$db);
	//echo "<br>  152 db_nouUsuari.php ".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

	if(!$mRow)
	{
		if(!$result=mysql_query("delete from usuaris where id='".$mPars['selUsuariId']."'",$db))
		{
			//echo "<br>  153 db_nouUsuari.php ".mysql_errno() . ": " . mysql_error(). "\n";
			return false;
		}	
	}	

	return true;
}


?>

		