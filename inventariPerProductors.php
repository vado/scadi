<?php

include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include "html_gestioMagatzems.php";
include "db_gestioMagatzems.php";
include "db_inventariPerProductors.php";
include "html_ajuda1.php";
include "db_ajuda.php";

$mode='magatzem';
$mPars['vProducte']='TOTS';

$parsChain=@$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();
/*
if(!$ruta=@$_POST['i_selRuta2'])
{
	$ruta=$mPars['selRutaSufix'];
}
else
{
	$mPars['selRutaSufix']=$ruta;
}
$rutaStr=$ruta;
settype($rutaStr,'integer');
*/
$mPars['paginacio']=0;


$vPr=@$_GET['vPr']; //de comptes.php
if(isset($vPr)){$mPars['vProductor']=$vPr;}


$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}
//*v36-declaracio
$mRuta=explode('_',$mPars['selRutaSufix']);
//*v36-condicio
if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);

getConfig($db); //inicialitza variables anteriors;
$mRutesSufixes=getRutesSufixes($db);

$parsChain=makeParsChain($mPars);	

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	$login=false;
	exit();
}
else
{
	$login=true;

}
	//gets
	if($mRef=@$_GET['mRef']) // seleccio de productors associats al usuari del gestor
	{
		$mPars['vProductor']=$mRef;
	}

$missatgeAlerta='';

$mMagatzems=db_getMagatzems($db);
	$mRebostsRef=db_getRebostsRef($db);
$mPerfilsRef=db_getPerfilsRef($db);
	$mProductors=db_getProductors($db);
	
	$mProductes=db_getProductes2($db);
	$mInventariPerProductors=db_getInventariPerProductors($db);
	if($mPars['grup_id']!=0)
	{
		$mPerfilsAssociats=@explode(',',trim($mRebostsRef[$mPars['grup_id']]['productors_associats'],','));
	}
	else
	{
		$mPerfilsAssociats=array();
	}
	
$sumaQuantitatTotalCACums=0;
$sumaQuantitatTotalCACecos=0;
$sumaQuantitatTotalCACeuros=0;
$sumaQuantitatTotalCACuts=0;
$sumaQuantitatTotalCACkg=0;
	echo "

<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>"; echo $htmlTitolPags; echo " - Inventari per Productors</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<script type='text/javascript'  language='JavaScript'>

navegador();
</script>
</head>
<body bgcolor='".$mColors['body']."'>
";
html_demo('inventariPerProductors.php?');

echo "
	<table align='center' style='width:90%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:90%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b></p>
			<p style='font-size:14px;'>".$mContinguts['form']['titol']."</p>
			</td>
		</tr>
	</table>
";
//*v36.5-funcio call
mostrarSelectorRuta(1,'inventariPerProductors.php');
echo "
	<table style='width:70%;' align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<th align='center'>
			<p>[ Inventari magatzems CAC per productes ]</p>
			</th>
		</tr>
		<tr>
			<td>
	";
	if($mPars['vCategoria']!='TOTS')
	{
		echo "
			<p style='font-size:11px;'>
			* Nom�s es mostren els productes de Categoria: <b>".$mPars['vCategoria']."</b>
			</p>
		";
	}
	if($mPars['vSubCategoria']!='TOTS')
	{
		echo "
			<p style='font-size:11px;'>
			* Nom�s es mostren els productes de SUB-categoria: <b>".$mPars['vSubCategoria']."</b>
			</p>
		";
	}
	if($mPars['etiqueta']!='TOTS')
	{
		echo "
			<p style='font-size:11px;'>
			* Nom�s es mostren els productes amb etiqueta: <b>".$mPars['etiqueta']."</b>
			</p>
		";
	}
	if($mPars['etiqueta2']!='CAP')
	{
		echo "
			<p style='font-size:11px;'>
			* No es mostren els productes amb etiqueta: <b>".$mPars['etiqueta2']."</b>
			</p>
		";
	}
	if($mPars['veureProductesDisponibles']=='1')
	{
		echo "
			<p style='font-size:11px;'>
			* Nom�s es mostren els productes actius</b>
			</p>
		";
	}
	else
	{
		echo "
			<p style='font-size:11px;'>
			* Es mostren els productes actius i els no actius</b>
			</p>
		";
	}
	if($mPars['vProductor']!='TOTS')
	{
		if(substr_count($mPars['vProductor'],'-')==0)
		{
			echo "
			<p style='font-size:11px;'>
			* Nom�s es mostren els productes del productor: <b>".(urldecode($mPerfilsRef[$mPars['vProductor']]['projecte']))."</b>
			</p>
			";
		}
		else
		{
			$gId=substr($mPars['vProductor'],0,strpos($mPars['vProductor'],'-'));
			echo "
			<table width='70%' align='left'>
				<tr>
					<td  width='50%' valign='top'>
					<p style='font-size:11px;' >
					* Nom�s es mostren els productes dels productors associats al GRUP:
					</td>
					
					<td  width='50%' valign='top'>
					<div style='width:200px; height:75px; overflow-y:scroll;'>
					<p class='p_micro2'>
					";
					if(count($mPerfilsAssociats)>0)
					{
						while(list($key,$perfilRef)=each($mPerfilsAssociats))
						{
							if($perfilRef!='')
							{	
								echo "
					- ".(urldecode($mPerfilsRef[$perfilRef]['projecte']))."
					<br>
								";
							}
						}
						reset($mPerfilsAssociats);
					}
					else
					{
						echo "- cap -";
					}
					echo "
					</p>
					</div>
					</td>
				</tr>
			</table>
			";
		}
	}
		echo "
			</td>
		</tr>
		";
		$cont=0;
	while(list($refProductor,$mProductesProductor)=@each($mInventariPerProductors))
	{
		if($refProductor!=0)
		{
			echo "
		<tr>
			<td  bgcolor='#ffccaa' style='width:100%;'>
			<p><b><u>".(urldecode($mPerfilsRef[$refProductor]['projecte']))."</u></b></p>
			<table  style='width:100%;'>
				<tr>
					<td style='width:30%;'>
					<p><b>productes</b></p>
					</td>
					<td   style='width:15%;'>
					<p><b>tipus</b></p>
					</td>
			";
			if($mPars['veureProductesDisponibles']=='-1')
			{
				echo "
					<td  style='width:5%;'>
					<p><b>estat</b></p>
					</td>
					";
			}
			echo "
					<td  style='width:20%;'>
					<p><b>magatzem</b></p>
					</td>
					<td  style='width:10%;'>
					<p><b>unitats</b></p>
					</td>
					<td style='width:20%;'>
					<p><b>data<br>inventari<br>revisat</b></p>
					</td>
				</tr>
			";
		
			$total=0;
			while(list($refProducte,$mInventarisMagatzems)=each($mProductesProductor))
			{
				echo "
				<tr>
					<td>
					<p>[<b>".$mInventarisMagatzems['id']."</b>] ".(urldecode($mInventarisMagatzems['producte']))."</p>
					";
					if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' && $mPars['nivell']=='coord')
					{
						echo "
					<p style='cursor:pointer; font-size:10px;' onClick=\"javascript: enviarFpars('movimentsProductes.php?pId=".$refProducte."','_blank');\"><u>hist�ric moviments</u> [".$mPars['nivell']."]</p>
						";
					}
					echo "
					</td>
					<td>
					".(posaColor1($mInventarisMagatzems['tipus']))."
					</td>
				";
				$estatText="<font color='#00aa00'>ACTIU</font>";
				if($mPars['veureProductesDisponibles']=='-1')
				{
					if($mInventarisMagatzems['actiu']=='0'){$estatText="<font color='#aa0000'>INACTIU</font>";}
					echo "
					<td  style='width:5%;'>
					<p>".$estatText."</p>
					</td>
					";
				}
				echo "
					<td>
					</td>
					<td>
					</td>
					<td>
					</td>
				</tr>
				";
				//$mP=getDatesInventariProducte($db,$mInventarisMagatzems['id']);

				while(list($magatzemRef,$quantitat)=each($mInventarisMagatzems))
				{
					if($magatzemRef!='actiu' && $magatzemRef!='producte' && $magatzemRef!='id' && $magatzemRef!='tipus'  && $magatzemRef!='num_inventari')
					{
						echo "
				<tr>
					<td>
					</td>
					<td>
					</td>
						";
						if($mPars['veureProductesDisponibles']=='-1')
						{
							echo "
					<td>
					</td>
							";
						}					
						echo "
					<td>
					<p>".(urldecode($mRebostsRef[$magatzemRef]['nom']))."</p>
					</td>
					<td>
					<p>";$total+=$quantitat; $cont++; echo round($quantitat)."</p>
					</td>
					<td>
					</td>
				</tr>
						";			
					}
				}
				reset($mInventarisMagatzems);
				$color2045='#3333aa';
				echo "
				<tr>
					<td>
					</td>
					<td>
					</td>
			";
			if($mPars['veureProductesDisponibles']=='-1')
			{
				echo "
					<td>
					</td>
				";
			}					
				echo "
					<td>
					<p style='color:#3333aa;'><i>ESTOC(CAC-Magatzems)</i></p>
					</td>
					<td>
					<p style='color:".$color2045.";'><b>".round($total)."</b></p>
					</td>
					<td>
					</td>
				</tr>
				";	
				$sumaQuantitatTotalCACums+=@$total*@$mProductes[$refProducte]['preu'];
				$sumaQuantitatTotalCACecos+=@$total*@$mProductes[$refProducte]['preu']*@$mProductes[$refProducte]['ms']/100;
				$sumaQuantitatTotalCACeuros+=@$total*@$mProductes[$refProducte]['preu']*(100-@$mProductes[$refProducte]['ms'])/100;
				$sumaQuantitatTotalCACuts+=$total;
				$sumaQuantitatTotalCACkg+=@$total*@$mProductes[$refProducte]['pes'];
				$total=0;
			}
			reset($mProductesProductor);
			echo "
			</table>
			</td>
		</tr>
			";		
		}
		}
		@reset($mInventariPerProductors);
	
	
	echo "
	</table>
	";
	if($cont==0)
	{
		echo "
		
	<table style='width:70%;' align='center'>
		<tr>
			<td  bgcolor='#ffccaa' style='width:100%;'>
			<br>
			<center><p> Actualment, l'estoc dels vostres productes <br> als inventaris dels magatzems CAC es zero</p></center>
			<br>&nbsp;
			</td>
		</tr>
	</table>
		";
	}
	else
	{
////////////////////////////////////////////////////////////////////////////
//*v36-29-12-15 condici�
	echo "
	<table style='width:70%;' align='center'>
		<tr>
			<td width='100%'>
			<table  width='100%'>
				<tr>
					<td width='33%' valign='top' align='center'>
					<table>
						<tr>
							<td valign='top' align='center'>
							<p ><i>Total valor en estoc:</i></p>
							<p><b>".(number_format($sumaQuantitatTotalCACums,2))." ums </b></p>
							<p><b>".(number_format($sumaQuantitatTotalCACecos,2))." ecos </b></p>
							<p><b>".(number_format($sumaQuantitatTotalCACeuros,2))." euros </b></p>
							</td>
						</tr>
					</table>
					</td>

					<td width='33%' valign='top' align='center'>
					<table>
						<tr>
							<td valign='top' align='center'>
							<p ><i>Total unitats en estoc:</i></p>
							<p><b>".(number_format($sumaQuantitatTotalCACuts,2))." uts </b></p>
							</td>
						</tr>
					</table>
					</td>

					<td width='33%' valign='top' align='center'>
					<table>
						<tr>
							<td valign='top' align='center'>
							<p ><i>Total kg en estoc:</i></p>
							<p><b>".(number_format($sumaQuantitatTotalCACkg,2))." kg </b></p>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
		";	
	}
	
	echo "
	<br>
	<br>
	<br>
	<br>
	<br>
	<p>&nbsp;</p>
	";
html_helpRecipient();
	
	echo "
<form id='f_pars' name='f_pars' method='post' action='index.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>
";

	
?>

		