<?php

//v36 24-1-16 modificadet el document sencer
//------------------------------------------------------------------------
function db_getRebostsAmbComandaResum($db)
{
	global $mPars,$mProductes,$mParametres,$mGrupsRef,$mProblemaAmb;


	$mRebostsAmbComandaResum=array();

	$umsT=0;
	$ecosT=0;
	$eurosT=0;
	$pesT=0;

	$ctkTums=0;
	$ctkTecos=0;
	$ctkTeuros=0;
						
	$ctrTums=0;
	$ctrTecos=0;
	$ctrTeuros=0;
	
	$quantitatTotalCac=0;
	$quantitatTotalRebosts=0;


	if(!$result=mysql_query("select sum(estoc_previst*pes) from productes_".$mPars['selRutaSufix']." where actiu='1'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalCac+=$mRow[0];
	}

	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2)),$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
//*v36.2-comentari
		//return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$quantitatTotalRebosts+=$mRow['quantitat'];
		}
	}

	//echo "select * from comandes_".$mPars['selRutaSufix']." ".$where." order by rebost DESC";
	if(!$result=@mysql_query("select * from ".$mPars['taulaComandes']."  order by rebost DESC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";


		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupRef=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			
			if(!isset($mRebostsAmbComandaResum[$grupRef]))
			{
				$mRebostsAmbComandaResum[$grupRef]=array();
				$mRebostsAmbComandaResum[$grupRef]['kgT']=0;
				$mRebostsAmbComandaResum[$grupRef]['umsT']=0;
				$mRebostsAmbComandaResum[$grupRef]['ecosT']=0;
				$mRebostsAmbComandaResum[$grupRef]['eurosT']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctkTums']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctkTecos']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctrTums']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctrTecos']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctrTeuros']=0;
				$mRebostsAmbComandaResum[$grupRef]['umstFDC']=0;
				$mRebostsAmbComandaResum[$grupRef]['ecostFDC']=0;
				$mRebostsAmbComandaResum[$grupRef]['eurostFDC']=0;
				$mRebostsAmbComandaResum[$grupRef]['totalUms']=0;
				$mRebostsAmbComandaResum[$grupRef]['totalEcos']=0;
				$mRebostsAmbComandaResum[$grupRef]['totalEuros']=0;
			}

			if(!isset($mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials']))
			{
				$mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials']=0;
			}

			if($mRow['rebost']!='0-CAC' && $mRow['usuari_id']!=0)
			{

				$mProductesComanda=explode(';',$mRow['resum']);

				for($i=0;$i<count($mProductesComanda);$i++)
				{
					$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
					$key=str_replace('producte_','',$mIndexQuantitat[0]);
					$quantitat=@$mIndexQuantitat[1];
				
					if(!isset($mProductes[$key]) && $key!='' && $key!=0)
					{
						if(!array_key_exists($key,$mProblemaAmb))
						{
							array_push($mProblemaAmb,$key);
						}
					}
					else if($key!='' && $key!=0)
					{
						if(substr_count(@$mProductes[$key]['tipus'],'especial')>0)
						{
							$mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials']+=1;
						}
						else
						{
							$ums_=$mProductes[$key]['preu']*$quantitat;
							//echo "<br>id:".$key." producte:".$mProductes[$key]['producte']." preu:".$mProductes[$key]['preu']." quanti:".$quantitat;
							$umsT+=$ums_;

							$ecos_=$ums_*$mProductes[$key]['ms']/100;
							$ecosT+=$ecos_;
	
							$eurosT+=$ums_-$ecos_;

							$pesT+=$quantitat*$mProductes[$key]['pes'];
				
							$ctkTums+=$mProductes[$key]['pes']*$quantitat*($mProductes[$key]['cost_transport_extern_kg']+$mProductes[$key]['cost_transport_intern_kg']);
							$ctkTecos+=$mProductes[$key]['pes']*$quantitat*(($mProductes[$key]['cost_transport_extern_kg']*$mProductes[$key]['ms_ctek']/100)+($mProductes[$key]['cost_transport_intern_kg']*$mProductes[$key]['ms_ctik']/100));
							$ctkTeuros+=$mProductes[$key]['pes']*$quantitat*(($mProductes[$key]['cost_transport_extern_kg']*(100-$mProductes[$key]['ms_ctek'])/100)+($mProductes[$key]['cost_transport_intern_kg']*(100-$mProductes[$key]['ms_ctik'])/100));
					
							$ctrTums+=  $mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']+$mParametres['cost_transport_intern_repartit']['valor'])/($quantitatTotalCac+$quantitatTotalRebosts));
							$ctrTecos+= $mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100+$mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)/($quantitatTotalCac+$quantitatTotalRebosts));
							$ctrTeuros+=$mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']*(100-$mParametres['ms_ctear']['valor'])/100+$mParametres['cost_transport_intern_repartit']['valor']*(100-$mParametres['ms_ctiar']['valor'])/100)/($quantitatTotalCac+$quantitatTotalRebosts));
						}
					}
				}
				
				$mRebostsAmbComandaResum[$grupRef]['kgT']+=$pesT;
				$mRebostsAmbComandaResum[$grupRef]['umsT']+=$umsT;
				$mRebostsAmbComandaResum[$grupRef]['ecosT']+=$ecosT;
				$mRebostsAmbComandaResum[$grupRef]['eurosT']+=$eurosT;
				$mRebostsAmbComandaResum[$grupRef]['ctkTums']+=$ctkTums;
				$mRebostsAmbComandaResum[$grupRef]['ctkTecos']+=$ctkTecos;
				$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']+=$ctkTeuros;
				$mRebostsAmbComandaResum[$grupRef]['ctrTums']+=$ctrTums;
				$mRebostsAmbComandaResum[$grupRef]['ctrTecos']+=$ctrTecos;
				$mRebostsAmbComandaResum[$grupRef]['ctrTeuros']+=$ctrTeuros;
				$mRebostsAmbComandaResum[$grupRef]['umstFDC']+=$umsT*$mParametres['FDCpp']['valor']/100;
				$mRebostsAmbComandaResum[$grupRef]['ecostFDC']+=($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
				$mRebostsAmbComandaResum[$grupRef]['eurostFDC']+=($umsT*$mParametres['FDCpp']['valor']/100)-($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
				$mRebostsAmbComandaResum[$grupRef]['totalUms']+=$umsT+$ctkTums+$ctrTums+($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
				$mRebostsAmbComandaResum[$grupRef]['totalEcos']+=$ecosT+$ctkTecos+$ctrTecos+($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
				$mRebostsAmbComandaResum[$grupRef]['totalEuros']+=$eurosT+$ctkTeuros+$ctrTeuros+($umsT*$mParametres['FDCpp']['valor']/100)-($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);

				$umsT=0;
				$ecosT=0;
				$eurosT=0;

				$pesT=0;

				$ctkTums=0;
				$ctkTecos=0;
				$ctkTeuros=0;
						
				$ctrTums=0;
				$ctrTecos=0;
				$ctrTeuros=0;
			}
			else if($mRow['rebost']!='0-CAC' && $mRow['usuari_id']==0)
			{
				if(!isset($mRow['compte_ecos']) || $mRow['compte_ecos']=='')
				{
					$mRebostsAmbComandaResum[$grupRef]['compte_ecos']=$mGrupsRef[$grupRef]['compte_ecos'];
				}
				else
				{
					$mRebostsAmbComandaResum[$grupRef]['compte_ecos']=$mRow['compte_ecos'];
				}

				if(!isset($mRow['compte_cieb']) || $mRow['compte_cieb']=='')
				{
					$mRebostsAmbComandaResum[$grupRef]['compte_cieb']=$mGrupsRef[$grupRef]['compte_cieb'];
				}
				else
				{
					$mRebostsAmbComandaResum[$grupRef]['compte_cieb']=$mRow['compte_cieb'];
				}
				$mRebostsAmbComandaResum[$grupRef]['historial']=str_replace('}{','}
{',$mRow['historial']);
			}			
		}
	}
	return $mRebostsAmbComandaResum;
}

//------------------------------------------------------------------------
function db_getRebostsAmbComandaResum2($db)
{
	global $mPars,$mProductes,$mParametres,$mGrupsRef,$mProblemaAmb;
	
	$mRebostsAmbComandaResum=array();

	$umsT=0;
	$ecosT=0;
	$eurosT=0;
	$pesT=0;

	$ctkTums=0;
	$ctkTecos=0;
	$ctkTeuros=0;
						
	$ctrTums=0;
	$ctrTecos=0;
	$ctrTeuros=0;
	
	$quantitatTotalCac=0;
	$quantitatTotalRebosts=0;


	if(!$result=mysql_query("select sum(estoc_previst*pes) from ".$mPars['taulaProductes']." where actiu='1' AND llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalCac+=$mRow[0];
	}

	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2)),$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
//*v36.2-comentari
		//return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$quantitatTotalRebosts+=$mRow['quantitat'];
		}
	}

	//echo "<br>select * from ".$mPars['taulaComandes']." where SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mPars['grup_id']."' AND usuari_id='0'  order by rebost DESC";
	if(!$result=@mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mPars['grup_id']."' AND usuari_id='0'  AND llista_id='".$mPars['selLlistaId']."'  order by rebost DESC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";


		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
				$grupRef=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));

				if(!isset($mRebostsAmbComandaResum[$grupRef]))
				{
				
				$mRebostsAmbComandaResum[$grupRef]=array();
				$mRebostsAmbComandaResum[$grupRef]['kgT']=0;
				$mRebostsAmbComandaResum[$grupRef]['umsT']=0;
				$mRebostsAmbComandaResum[$grupRef]['ecosT']=0;
				$mRebostsAmbComandaResum[$grupRef]['eurosT']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctkTums']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctkTecos']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctrTums']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctrTecos']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctrTeuros']=0;
				$mRebostsAmbComandaResum[$grupRef]['umstFDC']=0;
				$mRebostsAmbComandaResum[$grupRef]['ecostFDC']=0;
				$mRebostsAmbComandaResum[$grupRef]['eurostFDC']=0;
				$mRebostsAmbComandaResum[$grupRef]['totalUms']=0;
				$mRebostsAmbComandaResum[$grupRef]['totalEcos']=0;
				$mRebostsAmbComandaResum[$grupRef]['totalEuros']=0;
				}
				if(!isset($mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials'])){$mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials']=0;}
			if($mRow['rebost']!='0-CAC' && $mRow['usuari_id']!=0)
			{

				$mProductesComanda=explode(';',$mRow['resum']);

				for($i=0;$i<count($mProductesComanda);$i++)
				{
				
				$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
				$key=str_replace('producte_','',$mIndexQuantitat[0]);
				$quantitat=@$mIndexQuantitat[1];
				
				if(!isset($mProductes[$key]) && $key!='' && $key!=0)
				{
					if(!array_key_exists($key,$mProblemaAmb))
					{
						array_push($mProblemaAmb,$key);
					}
				}
				else if($key!='' && $key!=0)
				{
					if(substr_count(@$mProductes[$key]['tipus'],'especial')>0)
					{
						$mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials']+=1;
					}

					$ums_=$mProductes[$key]['preu']*$quantitat;
					//echo "<br>id:".$key." producte:".$mProductes[$key]['producte']." preu:".$mProductes[$key]['preu']." quanti:".$quantitat;
					$umsT+=$ums_;

					$ecos_=$ums_*$mProductes[$key]['ms']/100;
					$ecosT+=$ecos_;

					$eurosT+=$ums_-$ecos_;

					$pesT+=$quantitat*$mProductes[$key]['pes'];
				
					$ctkTums+=$mProductes[$key]['pes']*$quantitat*($mProductes[$key]['cost_transport_extern_kg']+$mProductes[$key]['cost_transport_intern_kg']);
					$ctkTecos+=$mProductes[$key]['pes']*$quantitat*(($mProductes[$key]['cost_transport_extern_kg']*$mProductes[$key]['ms_ctek']/100)+($mProductes[$key]['cost_transport_intern_kg']*$mProductes[$key]['ms_ctik']/100));
					$ctkTeuros+=$mProductes[$key]['pes']*$quantitat*(($mProductes[$key]['cost_transport_extern_kg']*(100-$mProductes[$key]['ms_ctek'])/100)+($mProductes[$key]['cost_transport_intern_kg']*(100-$mProductes[$key]['ms_ctik'])/100));
					
					$ctrTums+=  $mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']+$mParametres['cost_transport_intern_repartit']['valor'])/($quantitatTotalCac+$quantitatTotalRebosts));
					$ctrTecos+= $mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100+$mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)/($quantitatTotalCac+$quantitatTotalRebosts));
					$ctrTeuros+=$mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']*(100-$mParametres['ms_ctear']['valor'])/100+$mParametres['cost_transport_intern_repartit']['valor']*(100-$mParametres['ms_ctiar']['valor'])/100)/($quantitatTotalCac+$quantitatTotalRebosts));
				}
				}
			$mRebostsAmbComandaResum[$grupRef]['kgT']+=$pesT;
			$mRebostsAmbComandaResum[$grupRef]['umsT']+=$umsT;
			$mRebostsAmbComandaResum[$grupRef]['ecosT']+=$ecosT;
			$mRebostsAmbComandaResum[$grupRef]['eurosT']+=$eurosT;
			$mRebostsAmbComandaResum[$grupRef]['ctkTums']+=$ctkTums;
			$mRebostsAmbComandaResum[$grupRef]['ctkTecos']+=$ctkTecos;
			$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']+=$ctkTeuros;
			$mRebostsAmbComandaResum[$grupRef]['ctrTums']+=$ctrTums;
			$mRebostsAmbComandaResum[$grupRef]['ctrTecos']+=$ctrTecos;
			$mRebostsAmbComandaResum[$grupRef]['ctrTeuros']+=$ctrTeuros;
			$mRebostsAmbComandaResum[$grupRef]['umstFDC']+=$umsT*$mParametres['FDCpp']['valor']/100;
			$mRebostsAmbComandaResum[$grupRef]['ecostFDC']+=($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
			$mRebostsAmbComandaResum[$grupRef]['eurostFDC']+=($umsT*$mParametres['FDCpp']['valor']/100)-($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
			$mRebostsAmbComandaResum[$grupRef]['totalUms']+=$umsT+$ctkTums+$ctrTums+($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
			$mRebostsAmbComandaResum[$grupRef]['totalEcos']+=$ecosT+$ctkTecos+$ctrTecos+($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
			$mRebostsAmbComandaResum[$grupRef]['totalEuros']+=$eurosT+$ctkTeuros+$ctrTeuros+($umsT*$mParametres['FDCpp']['valor']/100)-($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);

				$umsT=0;
				$ecosT=0;
				$eurosT=0;

				$pesT=0;

				$ctkTums=0;
				$ctkTecos=0;
				$ctkTeuros=0;
						
				$ctrTums=0;
				$ctrTecos=0;
				$ctrTeuros=0;
			}
			else if($mRow['rebost']!='0-CAC' && $mRow['usuari_id']==0)
			{
				if(!isset($mRow['compte_ecos']) || $mRow['compte_ecos']=='')
				{
					$mRebostsAmbComandaResum[$grupRef]['compte_ecos']=$mGrupsRef[$grupRef]['compte_ecos'];
				}
				else
				{
					$mRebostsAmbComandaResum[$grupRef]['compte_ecos']=$mRow['compte_ecos'];
				}

				if(!isset($mRow['compte_cieb']) || $mRow['compte_cieb']=='')
				{
					$mRebostsAmbComandaResum[$grupRef]['compte_cieb']=$mGrupsRef[$grupRef]['compte_cieb'];
				}
				else
				{
					$mRebostsAmbComandaResum[$grupRef]['compte_cieb']=$mRow['compte_cieb'];
				}
				$mRebostsAmbComandaResum[$grupRef]['historial']=str_replace('}{','}
{',$mRow['historial']);
			}			
		}
	}
	return $mRebostsAmbComandaResum;
}

//------------------------------------------------------------------------
function db_getRebostsAmbComandaResum2Local($db)
{
	global $mPars,$mProductes,$mParametres,$mGrupsRef,$mProblemaAmb,$mPropietatsPeriodesLocals;
	
	$mRebostsAmbComandaResum=array();

	$umsT=0;
	$ecosT=0;
	$eurosT=0;
	$pesT=0;

	$ctkTums=0;
	$ctkTecos=0;
	$ctkTeuros=0;
						
	$ctrTums=0;
	$ctrTecos=0;
	$ctrTeuros=0;
	
	$quantitatTotalCac=0;
	$quantitatTotalRebosts=0;


	if(!$result=mysql_query("select sum(estoc_previst*pes) from ".$mPars['taulaProductes']." where actiu='1'  AND llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalCac+=$mRow[0];
	}


	//echo "<br>select * from ".$mPars['taulaComandes']." where SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mPars['selLlistaId']."' AND usuari_id!='0'  AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' order by rebost DESC";
	if(!$result=@mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mPars['selLlistaId']."' AND usuari_id!='0'  AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' order by rebost DESC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";


		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupRef=$mPars['selLlistaId'];

				if(!isset($mRebostsAmbComandaResum[$grupRef]))
				{
				
				$mRebostsAmbComandaResum[$grupRef]=array();
				$mRebostsAmbComandaResum[$grupRef]['kgT']=0;
				$mRebostsAmbComandaResum[$grupRef]['umsT']=0;
				$mRebostsAmbComandaResum[$grupRef]['ecosT']=0;
				$mRebostsAmbComandaResum[$grupRef]['eurosT']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctkTums']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctkTecos']=0;
				$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']=0;
				$mRebostsAmbComandaResum[$grupRef]['umstFDC']=0;
				$mRebostsAmbComandaResum[$grupRef]['ecostFDC']=0;
				$mRebostsAmbComandaResum[$grupRef]['eurostFDC']=0;
				$mRebostsAmbComandaResum[$grupRef]['totalUms']=0;
				$mRebostsAmbComandaResum[$grupRef]['totalEcos']=0;
				$mRebostsAmbComandaResum[$grupRef]['totalEuros']=0;
				}

				$mProductesComanda=explode(';',$mRow['resum']);

				for($i=0;$i<count($mProductesComanda);$i++)
				{
				
				$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
				$key=str_replace('producte_','',$mIndexQuantitat[0]);
				$quantitat=@$mIndexQuantitat[1];
				
				if(!isset($mProductes[$key]) && $key!='' && $key!=0)
				{
					if(!array_key_exists($key,$mProblemaAmb))
					{
						array_push($mProblemaAmb,$key);
					}
				}
				else if($key!='' && $key!=0)
				{

					$ums_=$mProductes[$key]['preu']*$quantitat;
					//echo "<br>id:".$key." producte:".$mProductes[$key]['producte']." preu:".$mProductes[$key]['preu']." quanti:".$quantitat;
					$umsT+=$ums_;

					$ecos_=$ums_*$mProductes[$key]['ms']/100;
					$ecosT+=$ecos_;

					$eurosT+=$ums_-$ecos_;

					$pesT+=$quantitat*$mProductes[$key]['pes'];
				
					$ctkTums+=$mProductes[$key]['pes']*$quantitat*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ctikLocal'];
					$ctkTecos+=$mProductes[$key]['pes']*$quantitat*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ctikLocal']*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_ctikLocal']/100);
					$ctkTeuros+=$mProductes[$key]['pes']*$quantitat*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ctikLocal']*(100-$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_ctikLocal'])/100);
					
				}
				}
			$mRebostsAmbComandaResum[$grupRef]['kgT']+=$pesT;
			$mRebostsAmbComandaResum[$grupRef]['umsT']+=$umsT;
			$mRebostsAmbComandaResum[$grupRef]['ecosT']+=$ecosT;
			$mRebostsAmbComandaResum[$grupRef]['eurosT']+=$eurosT;
			$mRebostsAmbComandaResum[$grupRef]['ctkTums']+=$ctkTums;
			$mRebostsAmbComandaResum[$grupRef]['ctkTecos']+=$ctkTecos;
			$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']+=$ctkTeuros;
			$mRebostsAmbComandaResum[$grupRef]['umstFDC']+=$umsT*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100;
			$mRebostsAmbComandaResum[$grupRef]['ecostFDC']+=$umsT*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100)*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_fdLocal']);
			$mRebostsAmbComandaResum[$grupRef]['eurostFDC']+=$umsT*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100)*((100-$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_fdLocal'])/100);
			$mRebostsAmbComandaResum[$grupRef]['totalUms']+=$umsT+$ctkTums+($umsT*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100);
			$mRebostsAmbComandaResum[$grupRef]['totalEcos']+=$ecosT+$ctkTecos+($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100)*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_fdLocal']);
			$mRebostsAmbComandaResum[$grupRef]['totalEuros']+=$eurosT+$ctkTeuros+($umsT*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100)*((100-$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_fdLocal'])/100));

				$umsT=0;
				$ecosT=0;
				$eurosT=0;

				$pesT=0;

				$ctkTums=0;
				$ctkTecos=0;
				$ctkTeuros=0;
						
				$ctrTums=0;
				$ctrTecos=0;
				$ctrTeuros=0;
		}
	}
	
	return $mRebostsAmbComandaResum;
}


//------------------------------------------------------------------------------
function db_actualitzarFormesPagamentGrups($grupId_,$opt,$db)
{
	global $mPars,$mParametres,$mProductes,$mUsuarisRef,$mRebostsAmbComandaResum,$mComptesGrupsRef,$mUsuarisGrupRef,$mGrupsRef;
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']='';

	$costProductes=0;
	$costTransport=0;
	$costFDcac=0;
	
	$costProductesEcos=0;
	$costTransportEcos=0;
	$costFDcacEcos=0;

	$costProductesT=0;
	$costTransportT=0;
	$costFDcacT=0;

	$importUsuari=0;
	$importUsuariEcos=0;
	$importUsuariEb=0;
	$importUsuariEuros=0;

	$importGrup=0;
	$importGrupEcos=0;
	$importGrupEb=0;
	$importGrupEuros=0;
				
	$importGrups=0;
	$importGrupsEcos=0;
	$importGrupsEb=0;
	$importGrupsEuros=0;

	// eliminar comandes actives d'usuaris sense cap producte reservat
	if(!$result=mysql_query("delete from comandes_".$mPars['selRutaSufix']." where usuari_id!='0' AND resum=''",$db))
	{
		//echo "<br> 689 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}

	while(list($grupId,$mVal)=each($mRebostsAmbComandaResum))
	{
		if
		(
			($grupId_!='' && $grupId_==$grupId)
			||
			$grupId_==''
		)
		{
			$cont=0;
		//obtenir imports usuaris grups
		//echo "<br>"."select * from comandes_".$mPars['selRutaSufix']." where usuari_id!='0' AND SUBSTRING_INDEX(rebost,'-',1)='".$grupId."'";
//		if($grupId!=0 && $grupId!='0' && $grupId!=62)
		if($grupId!=0 && $grupId!='0')
		{
				$mPars['grup_id']=$grupId;
			$mUsuarisGrupRef=db_getUsuarisGrupRef($db);
			$numUsuarisGrupNoCom=getUsuarisGrupNoCom();

			if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where usuari_id!='0' AND SUBSTRING_INDEX(rebost,'-',1)='".$grupId."' AND resum!=''",$db))
			{
				//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
			}
			else
			{ 
				while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
				{
					$cont++;
					$mProductesComanda=explode(';',$mRow['resum']);
					for($i=0;$i<count($mProductesComanda);$i++)
					{
						$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
						$index=str_replace('producte_','',$mIndexQuantitat[0]);
						$quantitat=@$mIndexQuantitat[1];
						if($index!='' && $index!=0 && isset($mProductes[$index]) && substr_count($mProductes[$index]['tipus'],'especial')==0)
						{		
							$costProductes+=$quantitat*$mProductes[$index]['preu'];
							$costTransport+=$quantitat*$mProductes[$index]['pes']*$mProductes[$index]['cost_transport_intern_kg'];
							$costFDcac+=$quantitat*$mProductes[$index]['preu']*$mParametres['FDCpp']['valor']/100;

							$costProductesEcos+=$quantitat*$mProductes[$index]['preu']*$mProductes[$index]['ms']/100;
							$costTransportEcos+=$quantitat*$mProductes[$index]['pes']*$mProductes[$index]['cost_transport_intern_kg']*$mProductes[$index]['ms_ctik']/100;
							$costFDcacEcos+=$quantitat*$mProductes[$index]['preu']*($mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
						}	
					} 
			
					$importUsuari=$costProductes+$costTransport+$costFDcac;
					
					
					$importUsuariEcos=$costProductesEcos+$costTransportEcos+$costFDcacEcos;
					$importUsuariEb=0;
					$importUsuariEuros=$importUsuari-$importUsuariEcos;
					
					$costProductes=0;
					$costTransport=0;
					$costFDcac=0;

					$costProductesEcos=0;
					$costTransportEcos=0;
					$costFDcacEcos=0;
					
					$nomRebost=substr($mRow['rebost'],strpos($mRow['rebost'],'-')+1);

					if($opt==1 && $mPars['nivell']=='sadmin')
					{
						//actualitzar comandes
						$mFormaPagament=explode(',',$mRow['f_pagament']);
						if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
						if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
						if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
						if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
						if(!isset($mFormaPagament[4])){$mFormaPagament[4]=0;}
						if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
						if(!isset($mFormaPagament[6])){$mFormaPagament[6]=0;}
						if(!isset($mFormaPagament[7])){$mFormaPagament[7]=0;}
						if(!isset($mFormaPagament[8])){$mFormaPagament[8]=0;}
				
						if(!isset($mFormaPagament[9]))
						{
								//max % ms
							if($importUsuariEcos*1!=0 && $importUsuariEb*1==0 && $importUsuariEuros*1==0)
							{
								$mFormaPagament[9]=7;
							}
							else //max% eb
							if($importUsuariEcos*1==0 && $importUsuariEb*1!=0 && $importUsuariEuros*1==0)
							{
								$mFormaPagament[9]=8;
							}
							else // 100% euros
							if($importUsuariEcos*1==0 && $importUsuariEb*1==0 && $importUsuariEuros*1!=0)
							{
								$mFormaPagament[9]=9;
							}
							else // 50% ecos i 50% eb
							if($importUsuariEcos*1!=0 && $importUsuariEcos*1==$importUsuariEb*1 && $importUsuariEuros*1==0)
							{
								$mFormaPagament[9]=10;
							}
							else // 50% eb i 50% euros
							if($importUsuariEcos*1==0 && $importUsuariEb*1!=0 && $importUsuariEb*1==$importUsuariEuros*1)
							{
								$mFormaPagament[9]=12;
							}
							else // 50% ecos i 50% euros
							if($importUsuariEcos*1!=0 && $importUsuariEb*1==0 && $importUsuariEcos*1==$importUsuariEuros*1)
							{
								$mFormaPagament[9]=11;
							}
							else // %ms productors
							{
								$mFormaPagament[9]=13;
							}
						}
							
						$totalUms=$mFormaPagament[0]*1+$mFormaPagament[1]*1+$mFormaPagament[2]*1;

						if($totalUms>0)
						{
							$mFormaPagament[6]=$mFormaPagament[0]*100/$totalUms;
							$mFormaPagament[7]=$mFormaPagament[1]*100/$totalUms;
							$mFormaPagament[8]=$mFormaPagament[2]*100/$totalUms;
						}
						else if($totalUms>0)
						{
							$mFormaPagament[6]=0;
							$mFormaPagament[7]=0;
							$mFormaPagament[8]=0;
						}

						if($importUsuari>0 || $totalUms>0)
						{
							if($totalUms!=$importUsuari)
							{
								$mFormaPagament[6]=$importUsuariEcos*100/$importUsuari;
								$mFormaPagament[7]=$importUsuariEb*100/$importUsuari;
								$mFormaPagament[8]=$importUsuariEuros*100/$importUsuari;
								$mFormaPagament[0]=$importUsuariEcos;
								$mFormaPagament[1]=$importUsuariEb;
								$mFormaPagament[2]=$importUsuariEuros;
							}
															
							//aplicar filtre max ecos
							if(!isset($mComptesGrupsRef[$grupId]['saldo_ecos_r'])){$mComptesGrupsRef[$grupId]['saldo_ecos_r']=0;}
							$maxEcos=$importUsuariEcos+$mComptesGrupsRef[$grupId]['saldo_ecos_r']/$numUsuarisGrupNoCom;
								
							if($mFormaPagament[9]*1==7) //max % ecos
							{
								if($mFormaPagament[0]*1!=$maxEcos)
								{
									if($mFormaPagament[0]*1>$maxEcos)
									{
										$mFormaPagament[2]=$maxEcos-$mFormaPagament[0]*1;
										$mFormaPagament[0]=$maxEcos;
									}
									else if
									(
										$mFormaPagament[0]*1<$maxEcos 
										&&
										$mFormaPagament[2]*1>0
									)
									{
										if(($mFormaPagament[2]*1+$mFormaPagament[0]*1)<=$maxEcos )
										{
											$mFormaPagament[0]=$mFormaPagament[2]*1+$mFormaPagament[0]*1;
											$mFormaPagament[2]=0;
										}
										else
										{
											$mFormaPagament[2]=($mFormaPagament[2]*1+$mFormaPagament[0]*1)-$maxEcos;
											$mFormaPagament[0]=$maxEcos;
										}												
									}
								}
								
								$mFormaPagament[0]=number_format($mFormaPagament[0],2,'.','');
								$mFormaPagament[2]=number_format($mFormaPagament[2],2,'.','');
							}
							else if($mFormaPagament[9]*1==9) // 100% euros
							{
								$mFormaPagament[0]=0;
								$mFormaPagament[1]=0;
								$mFormaPagament[2]=number_format($importUsuari,2,'.','');
								$mFormaPagament[6]=0;
								$mFormaPagament[7]=0;
								$mFormaPagament[8]=100;
							}
							else if($mFormaPagament[9]*1==11) // 50% ecos 50% euros
							{
								$mFormaPagament[0]=number_format(($importUsuari/2),2,'.','');
								$mFormaPagament[1]=0;
								$mFormaPagament[2]=number_format(($importUsuari/2),2,'.','');
								$mFormaPagament[6]=50;
								$mFormaPagament[7]=0;
								$mFormaPagament[8]=50;
							}
							else if($mFormaPagament[9]*1==13) // %ms productors
							{
								if($importUsuari>0)
								{
									$mFormaPagament[0]=number_format($importUsuariEcos,2,'.','');
									$mFormaPagament[1]=0;
									$mFormaPagament[2]=number_format(($importUsuari-$importUsuariEcos),2,'.','');
									$mFormaPagament[6]=number_format(($importUsuariEcos*100/$importUsuari),2,'.','');
									$mFormaPagament[7]=0;
									$mFormaPagament[8]=number_format((($importUsuari-$importUsuariEcos)*100/$importUsuari),2,'.','');
								}
								else
								{
									$mFormaPagament[0]=number_format($importUsuariEcos,2,'.','');
									$mFormaPagament[1]=0;
									$mFormaPagament[2]=number_format(($importUsuari-$importUsuariEcos),2,'.','');
									$mFormaPagament[6]=number_format(0,2,'.','');
									$mFormaPagament[7]=0;
									$mFormaPagament[8]=number_format(0,2,'.','');
								}
							}
					
							//filtre limit ecos
							if($mFormaPagament[0]*1>$maxEcos)
							{
								$mFormaPagament_0=$mFormaPagament[0];
								$mFormaPagament[2]+=$mFormaPagament_0*1-$maxEcos;
								$mFormaPagament[0]-=$mFormaPagament_0*1-$maxEcos;
							}
					
							$totalUms=$mFormaPagament[0]+$mFormaPagament[1]+$mFormaPagament[2];
							
							//definir percentatges a partir dels valors recalculats
							if($totalUms>=0.01)
							{
								//pp ecos:
								$mFormaPagament[6]=number_format(($mFormaPagament[0]/$totalUms)*100,2,'.','');
								//pp eb:
								$mFormaPagament[7]=number_format(($mFormaPagament[1]/$totalUms)*100,2,'.','');
								//pp euros:
								$mFormaPagament[8]=number_format(($mFormaPagament[2]/$totalUms)*100,2,'.','');
							}
							else
							{
								//pp ecos:
								$mFormaPagament[6]=0;
								//pp eb:
								$mFormaPagament[7]=0;
								//pp euros:
								$mFormaPagament[8]=0;
							}
					
							$mPars['f_pagament']=
							(number_format($mFormaPagament[0],2,'.',''))
							.",".
							(number_format($mFormaPagament[1],2,'.',''))
							.",".
							(number_format($mFormaPagament[2],2,'.',''))
							.",".
							$mFormaPagament[3]
							.",".
							$mFormaPagament[4]
							.",".
							$mFormaPagament[5]
							.",".
							(number_format($mFormaPagament[6],2,'.',''))
							.",".
							(number_format($mFormaPagament[7],2,'.',''))
							.",".
							(number_format($mFormaPagament[8],2,'.',''))
							.','.
							$mFormaPagament[9];
						}
						else
						{
							$mPars['f_pagament']='0,0,0,0,0,0,0,0,7';
						}
	
						if
						(
							$mPars['f_pagament']!=$mRow['f_pagament']
						)
						{
							//---------------------------
							//registre historial comanda
							$registre='';
							$mGC['usuari_id']=$mPars['usuari_id'];
							$mGC['f_pagament']=$mPars['f_pagament'];
							
							while(list($key,$val)=each($mGC))
							{
								$registre.='{p:guardarComanda;us:'.$mGC['usuari_id'].'[AUTO];'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
							}
							reset($mGC);
							//---------------------------

							//guardar la comanda  d'aquest usuari
							$nomRebost=substr($mRow['rebost'],strpos($mRow['rebost'],'-')+1);
							
							if(!$result2=@mysql_query("update comandes_".$mPars['selRutaSufix']." set f_pagament='".$mPars['f_pagament']."',historial=CONCAT(historial,'".$registre."') where id='".$mRow['id']."'",$db))
							{
								//echo "<br> 281 db_comptes.php ".mysql_errno() . ": " . mysql_error(). "\n";
								//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
								$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>Error: actualitzar forma_pagament usuari <b>".$mUsuarisRef[$mRow['usuari_id']]['usuari']."</b> a grup: '".(urldecode($nomRebost))."' :<br>".$mRow['f_pagament'].">><br>".$mPars['f_pagament']."</p>";
							}
							else
							{
								//echo "<br> 281 db_comptes.php ".mysql_errno() . ": " . mysql_error(). "\n";
								$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>Ok: actualitzar forma_pagament usuari <b>".$mUsuarisRef[$mRow['usuari_id']]['usuari']."</b> a grup: '".(urldecode($nomRebost))."' :<br>".$mRow['f_pagament'].">><br>".$mPars['f_pagament']." [max: ".(number_format($maxEcos,2,'.',''))." ecos]</p>";
							}
						}
					}
					else
					if($grupId_!='' )
					{
						echo "<p class='p_micro2'>".urldecode($mUsuarisRef[$mRow['usuari_id']]['usuari'])." : ".$mRow['f_pagament']."</p>";
					}
					
					$importGrup+=$importUsuari;
					$importGrupEcos+=$importUsuariEcos;
					$importGrupEb+=$importUsuariEb;
					$importGrupEuros+=$importUsuariEuros;
				}
			}
			

			// obtenir imports del grup
			//obtenir imports usuaris grups
			if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where usuari_id='0' AND SUBSTRING_INDEX(rebost,'-',1)='".$grupId."' order by id DESC",$db))
			{
				//echo "<br> 428 db_comptes.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
			}
			else
			{ 
				$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	
				if($mRow && $opt==1 && $mPars['nivell']=='sadmin')
				{	
					//---------------------------
					//obtenir import de comanda segons productes
					$mGEdistrib=db_getImportComandaGrupAcac($grupId,$db);
									//actualitzar comandes
					// si el grup no ha decidit de quina forma paga, que es guardi segons % monetaris usuaris
					$mFormaPagamentg=explode(',',$mRow['f_pagament']);

					if(!isset($mFormaPagamentg[0])){$mFormaPagamentg[0]=0;}
					if(!isset($mFormaPagamentg[1])){$mFormaPagamentg[1]=0;}
					if(!isset($mFormaPagamentg[2])){$mFormaPagamentg[2]=0;}
					if(!isset($mFormaPagamentg[3])){$mFormaPagamentg[3]=0;}
					if(!isset($mFormaPagamentg[4])){$mFormaPagamentg[4]=1;}
					if(!isset($mFormaPagamentg[5])){$mFormaPagamentg[5]=0;}
					if(!isset($mFormaPagamentg[6])){$mFormaPagamentg[6]=0;}
					if(!isset($mFormaPagamentg[7])){$mFormaPagamentg[7]=0;}
					if(!isset($mFormaPagamentg[8])){$mFormaPagamentg[8]=0;}
						if(!isset($mFormaPagamentg[9]))
						{
								//max % ms
							if($importGrupEcos*1!=0 && $importGrupEb*1==0 && $importGrupEuros*1==0)
							{
								$mFormaPagamentg[9]=7;
							}
							else //max% eb
							if($importGrupEcos*1==0 && $importGrupEb*1!=0 && $importGrupEuros*1==0)
							{
								$mFormaPagamentg[9]=8;
							}
							else // 100% euros
							if($importGrupEcos*1==0 && $importGrupEb*1==0 && $importGrupEuros*1!=0)
							{
								$mFormaPagamentg[9]=9;
							}
							else // 50% ecos i 50% eb
							if($importGrupEcos*1!=0 && $importGrupEcos*1==$importGrupEb*1 && $importGrupEuros*1==0)
							{
								$mFormaPagamentg[9]=10;
							}
							else // 50% eb i 50% euros
							if($importGrupEcos*1==0 && $importGrupEb*1!=0 && $importGrupEb*1==$importGrupEuros*1)
							{
								$mFormaPagamentg[9]=12;
							}
							else // 50% ecos i 50% euros
							if($importGrupEcos*1!=0 && $importGrupEb*1==0 && $importGrupEcos*1==$importGrupEuros*1)
							{
								$mFormaPagamentg[9]=11;
							}
							else // %ms productors
							{
								$mFormaPagamentg[9]=13;
							}
						}
					
															
						//si no s'ha guardat la forma de pagament i els % no sumen 100
					
						if(!isset($mComptesGrupsRef[$grupId]['saldo_ecos_r'])){$mComptesGrupsRef[$grupId]['saldo_ecos_r']=0;}
						$maxEcos=$mGEdistrib['ecos']+$mComptesGrupsRef[$grupId]['saldo_ecos_r'];

						$totalUms=$mFormaPagamentg[0]*1+$mFormaPagamentg[1]*1+$mFormaPagamentg[2]*1;
						
						if($totalUms>0)
						{
							$mFormaPagamentg[6]=$mFormaPagamentg[0]*100/$totalUms;
							$mFormaPagamentg[7]=$mFormaPagamentg[1]*100/$totalUms;
							$mFormaPagamentg[8]=$mFormaPagamentg[2]*100/$totalUms;
						}
						else if($totalUms>0)
						{
							$mFormaPagamentg[6]=0;
							$mFormaPagamentg[7]=0;
							$mFormaPagamentg[8]=0;
						}

						if($importGrup>0 || $totalUms>0 || $mGEdistrib['ums']>0)
						{

							if($totalUms!=$importGrup)
							{
								$mFormaPagamentg[6]=$importGrupEcos*100/$importGrup;
								$mFormaPagamentg[7]=$importGrupEb*100/$importGrup;
								$mFormaPagamentg[8]=$importGrupEuros*100/$importGrup;
								$mFormaPagamentg[0]=$importGrupEcos;
								$mFormaPagamentg[1]=$importGrupEb;
								$mFormaPagamentg[2]=$importGrupEuros;
							}

							if($importGrup!=$mGEdistrib['ums'])
							{
								$mFormaPagamentg[6]=$mGEdistrib['ecos']*100/$mGEdistrib['ums'];
								$mFormaPagamentg[7]=0;
								$mFormaPagamentg[8]=$mGEdistrib['euros']*100/$mGEdistrib['ums'];
								$mFormaPagamentg[0]=$mGEdistrib['ecos'];
								$mFormaPagamentg[1]=0;
								$mFormaPagamentg[2]=$mGEdistrib['euros'];
							}

							//aplicar filtre max ecos
							if($mFormaPagamentg[9]*1==7) //max % ecos
							{
								if($mFormaPagamentg[0]*1!=$maxEcos)
								{
									if($mFormaPagamentg[0]*1>$maxEcos)
									{
										$mFormaPagamentg[2]=$maxEcos-$mFormaPagamentg[0]*1;
										$mFormaPagamentg[0]=$maxEcos;
									}
									else if
									(
										$mFormaPagamentg[0]*1<$maxEcos 
										&&
										$mFormaPagamentg[2]*1>0
									)
									{
										if(($mFormaPagamentg[2]*1+$mFormaPagamentg[0]*1)<=$maxEcos )
										{
											$mFormaPagamentg[0]=$mFormaPagamentg[2]*1+$mFormaPagamentg[0]*1;
											$mFormaPagamentg[2]=0;
										}
										else
										{
											$mFormaPagamentg[2]=($mFormaPagamentg[2]*1+$mFormaPagamentg[0]*1)-$maxEcos;
											$mFormaPagamentg[0]=$maxEcos;
										}												
									}
								}
								
								$mFormaPagamentg[0]=number_format($mFormaPagamentg[0],2,'.','');
								$mFormaPagamentg[2]=number_format($mFormaPagamentg[2],2,'.','');
							}
						else if($mFormaPagamentg[9]*1==9) // 100% euros
						{
							$mFormaPagamentg[0]=0;
							$mFormaPagamentg[1]=0;
							$mFormaPagamentg[2]=number_format($importGrup,2,'.','');
							$mFormaPagamentg[6]=0;
							$mFormaPagamentg[7]=0;
							$mFormaPagamentg[8]=100;
						}
						else if($mFormaPagamentg[9]*1==11) // 50% ecos 50% euros
						{
							$mFormaPagamentg[0]=number_format(($importGrup/2),2,'.','');
							$mFormaPagamentg[1]=0;
							$mFormaPagamentg[2]=number_format(($importGrup/2),2,'.','');
							$mFormaPagamentg[6]=50;
							$mFormaPagamentg[7]=0;
							$mFormaPagamentg[8]=50;
						}
						else if($mFormaPagamentg[9]*1==13) // %ms productors
						{
							$mFormaPagamentg[0]=number_format($importGrupEcos,2,'.','');
							$mFormaPagamentg[1]=0;
							$mFormaPagamentg[2]=number_format(($importGrup-$importGrupEcos),2,'.','');
							$mFormaPagamentg[6]=number_format(($importGrupEcos*100/$importGrup),2,'.','');
							$mFormaPagamentg[7]=0;
							$mFormaPagamentg[8]=number_format((($importGrup-$importGrupEcos)*100/$importGrup),2,'.','');
						}
					
						//filtre limit ecos
						if($importGrupEcos*1>$maxEcos)
						{
							$mFormaPagamentg[2]=$importGrupEuros+($importGrupEcos*1-$maxEcos);
							$mFormaPagamentg[0]=$importGrupEcos-($importGrupEcos*1-$maxEcos);
						}
					

						$totalUms=$mFormaPagamentg[0]+$mFormaPagamentg[1]+$mFormaPagamentg[2];
							
						if($totalUms>=0.01)
						{
							//pp ecos:
							$mFormaPagamentg[6]=number_format(($mFormaPagamentg[0]/$totalUms)*100,2,'.','');
							//pp eb:
							$mFormaPagamentg[7]=number_format(($mFormaPagamentg[1]/$totalUms)*100,2,'.','');
							//pp euros:
							$mFormaPagamentg[8]=number_format(($mFormaPagamentg[2]/$totalUms)*100,2,'.','');
						}
						else
						{
							//pp ecos:
							$mFormaPagamentg[6]=0;
							//pp eb:
							$mFormaPagamentg[7]=0;
							//pp euros:
							$mFormaPagamentg[8]=0;
						}
	
							$mPars['f_pagament']=
							(number_format($mFormaPagamentg[0],2,'.',''))
							.",".
							(number_format($mFormaPagamentg[1],2,'.',''))
							.",".
							(number_format($mFormaPagamentg[2],2,'.',''))
							.",".
							$mFormaPagamentg[3]
							.",".
							$mFormaPagamentg[4]
							.",".
							$mFormaPagamentg[5]
							.",".
							(number_format($mFormaPagamentg[6],2,'.',''))
							.",".
							(number_format($mFormaPagamentg[7],2,'.',''))
							.",".
							(number_format($mFormaPagamentg[8],2,'.',''))
							.','.
							$mFormaPagamentg[9];
					}
					else
					{
						$mPars['f_pagament']='0,0,0,0,0,0,0,0,7';
					}
					if
					(
						$mPars['f_pagament']!=$mRow['f_pagament']
					)
					{
						//---------------------------
						//registre historial comanda
						$registre='';
						$mGC['usuari_id']=$mPars['usuari_id'];
						$mGC['f_pagament']=$mPars['f_pagament'];
									
						while(list($key,$val)=each($mGC))
						{
							$registre.='{p:guardarComanda;us:'.$mGC['usuari_id'].'[AUTO];'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
						}
						reset($mGC);
						//---------------------------
		
						$nomRebost=substr($mRow['rebost'],strpos($mRow['rebost'],'-')+1);
										
						if(!$result2=@mysql_query("update comandes_".$mPars['selRutaSufix']." set f_pagament='".$mPars['f_pagament']."',historial=CONCAT('".$registre."',historial) where id='".$mRow['id']."'",$db))
						{
							//echo "<br> 281 db_comptes.php ".mysql_errno() . ": " . mysql_error(). "\n";
							//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
							$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>Error: modificar forma_pagament grup: '<b>".(urldecode($nomRebost))."</b>' :<br>".$mRow['f_pagament'].">><br>".$mPars['f_pagament']."</p>";
						}
						else
						{	
							//echo "<br> 281 db_comptes.php ".mysql_errno() . ": " . mysql_error(). "\n";
							$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>Ok: modificar forma_pagament grup: '<b>".(urldecode($nomRebost))."</b>' :<br>".$mRow['f_pagament'].">><br>".$mPars['f_pagament']." [max: ".(number_format($maxEcos,2,'.',''))." ecos]</p>";
						}
					}
				}
				$importGrups+=$importGrup;
				$importGrupsEcos+=$importGrupEcos;
				$importGrupsEb+=$importGrupEb;
				$importGrupsEuros+=$importGrupEuros;

				$importGrup=0;
				$importGrupEcos=0;
				$importGrupEb=0;
				$importGrupEuros=0;
			}
		}
	}
	}
	reset($mRebostsAmbComandaResum);
	if($grupId_=='')
	{
		$mPars['importsGrups']=$importGrups;
	}
	else
	{
		$mPars['importsGrups_'.$grupId_]=$importGrups;
	}
	 
	return $mMissatgeAlerta;
}

//*v36-18-12-15 eliminar funcio
/*
//---------------------------------------------------------------
function db_getPerfilsRef2($db)
{
	global $mPars;
	$mPerfilsRef=array();

	if(!$result=mysql_query("select * from ".$mPars['taulaProductors']." where id='".$mPars['perfil_id']."' order by projecte ASC",$db))
	{
		//echo "<br> 138 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB19.1 ',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mPerfil=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPerfilsRef[$mPerfil['id']]=$mPerfil;
		}
	}
	return $mPerfilsRef;
}
*/
//---------------------------------------------------------------
function db_getGrupsRef2($db)
{
	global $mPars;
	$mGrupsRef=array();
	$mGrupsRef[0]['nom']='CAC';
	
	if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where id='".$mPars['grup_id']."' order by nom ASC",$db))
	{
		//echo "<br> 138 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mGrup=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mGrupsRef[$mGrup['ref']]=$mGrup;
		}
	}
	return $mGrupsRef;
}

//------------------------------------------------------------------------------
function db_getGrupsSenseZona($db)
{
	global $mPars,$mGrupsRef;

	$mGrupsNoZona=array();
	
	while(list($grupId,$val)=each($mGrupsRef))
	{
		$zona=db_getZonaGrup($grupId,$db);
		if(!$zona || $zona=''){array_push($mGrupsNoZona,$grupId);}
	}		
	reset($mGrupsRef);
	
	return $mGrupsNoZona;
}

//------------------------------------------------------------------------------
function db_getPerfilsActiusSenseZona($db)
{
	global $mPars,$mPerfilsRef;

	$mPerfilsNoZona=array();
	
	while(list($perfilId,$mPerfil)=each($mPerfilsRef))
	{
		$zona=db_getZonaPerfil($perfilId,$db);
		if(($mPerfil['estat']=='1' || $mPerfil['estat']==1) && (!$zona || $zona='')){array_push($mPerfilsNoZona,$perfilId);}
	}		
	reset($mPerfilsRef);
	
	return $mPerfilsNoZona;
}

//------------------------------------------------------------------------------
function db_getGrupsActiusSenseZona($db)
{
	global $mPars,$mGrupsRef;

	$mGrupsNoZona=array();
	
	while(list($grupId,$mGrup)=each($mGrupsRef))
	{
	
		$zona=db_getZonaGrup($grupId,$db);
		if(!isset($mGrup['categoria']) || substr_count(' '.$mGrup['categoria'],'inactiu')==0 && (!isset($zona) || $zona=='')){array_push($mGrupsNoZona,$grupId);}
	}		
	reset($mGrupsRef);
	return $mGrupsNoZona;
}

//------------------------------------------------------------------------------
function db_getPesos($db)
{
	global $mPars;

	$mPesos=array();
	
	if(!$result=mysql_query("select DISTINCT(pes) from productes_".$mPars['selRutaSufix']." where actiu='1' order by pes DESC",$db))
	{
		//echo "<br> 138 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			array_push($mPesos,$mRow[0]);
		}
	}
	
	return $mPesos;
}

//------------------------------------------------------------------------------
function db_getFormats($db)
{
	global $mPars;

	$mFormats=array();
	
	if(!$result=mysql_query("select DISTINCT(format) from productes_".$mPars['selRutaSufix']." where actiu='1' order by format DESC",$db))
	{
		//echo "<br> 138 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			array_push($mFormats,$mRow[0]);
		}
	}
	
	return $mFormats;
}
//------------------------------------------------------------------------------
function db_getImportComandaGrupAcac($grupId,$db)
{
	global $mRebostsAmbComandaResum;
	
$cDp_umsT=0;
$cDp_ecosT=0;
$cDp_eurosT=0;
$cDp_umsCtT=0;
$cDp_ecosCtT=0;
$cDp_eurosCtT=0;
$cDp_umsFdT=0;
$cDp_ecosFdT=0;
$cDp_eurosFdT=0;
$sumaAbonamentsCarrecsEcosT=0;
$sumaAbonamentsCarrecsEbT=0;
$sumaAbonamentsCarrecsEuT=0;
$sumaAbonamentsCarrecsUmsT=0;
$mPars=array();

		if(!isset($mRebostsAmbComandaResum[$grupId]))
		{
			$mRebostsAmbComandaResum[$grupId]['kgT']=0;
			$mFormaPagament=array();
			$mFormaPagamentMembres=array();
			$mPars['fPagamentEcos']=0;
			$mPars['fPagamentEb']=0;
			$mPars['fPagamentEu']=0;
			$mPars['fPagamentUms']=0;
			$mPars['fPagamentEcosPerc']=100;
			$mPars['fPagamentEbPerc']=0;
			$mPars['fPagamentEuPerc']=0;

			$cDp_ums=0;
			$cDp_ecos=0;
			$cDp_euros=0;
			$cDp_umsCt=0;
			$cDp_ecosCt=0;
			$cDp_eurosCt=0;
			$cDp_umsFd=0;
			$cDp_ecosFd=0;
			$cDp_eurosFd=0;

			$mRebostsAmbComandaResum[$grupId]['umsT']=0;
			$mRebostsAmbComandaResum[$grupId]['ecosT']=0;
			$mRebostsAmbComandaResum[$grupId]['eurosT']=0;
			$mRebostsAmbComandaResum[$grupId]['ctkTums']=0;
			$mRebostsAmbComandaResum[$grupId]['ctrTums']=0;
			$mRebostsAmbComandaResum[$grupId]['ctkTecos']=0;
			$mRebostsAmbComandaResum[$grupId]['ctrTecos']=0;
			$mRebostsAmbComandaResum[$grupId]['ctkTeuros']=0;
			$mRebostsAmbComandaResum[$grupId]['ctrTeuros']=0;
			$mRebostsAmbComandaResum[$grupId]['umstFDC']=0;
			$mRebostsAmbComandaResum[$grupId]['ecostFDC']=0;
			$mRebostsAmbComandaResum[$grupId]['eurostFDC']=0;
			$mRebostsAmbComandaResum[$grupId]['compte_ecos']='';
			$mRebostsAmbComandaResum[$grupId]['compte_cieb']='';
			$mRebostsAmbComandaResum[$grupId]['historial']='';
		}
		else
		{
			$cDp_ums=$mRebostsAmbComandaResum[$grupId]['umsT'];
			$cDp_ecos=$mRebostsAmbComandaResum[$grupId]['ecosT'];
			$cDp_euros=$mRebostsAmbComandaResum[$grupId]['eurosT'];
			$cDp_umsCt=$mRebostsAmbComandaResum[$grupId]['ctkTums']+$mRebostsAmbComandaResum[$grupId]['ctrTums'];
			$cDp_ecosCt=$mRebostsAmbComandaResum[$grupId]['ctkTecos']+$mRebostsAmbComandaResum[$grupId]['ctrTecos'];
			$cDp_eurosCt=$mRebostsAmbComandaResum[$grupId]['ctkTeuros']+$mRebostsAmbComandaResum[$grupId]['ctrTeuros'];
			$cDp_umsFd=$mRebostsAmbComandaResum[$grupId]['umstFDC'];
			$cDp_ecosFd=$mRebostsAmbComandaResum[$grupId]['ecostFDC'];
			$cDp_eurosFd=$mRebostsAmbComandaResum[$grupId]['eurostFDC'];

			$cDp_umsT+=$mRebostsAmbComandaResum[$grupId]['umsT'];
			$cDp_ecosT+=$mRebostsAmbComandaResum[$grupId]['ecosT'];
			$cDp_eurosT+=$mRebostsAmbComandaResum[$grupId]['eurosT'];
			$cDp_umsCtT+=$mRebostsAmbComandaResum[$grupId]['ctkTums']+$mRebostsAmbComandaResum[$grupId]['ctrTums'];
			$cDp_ecosCtT+=$mRebostsAmbComandaResum[$grupId]['ctkTecos']+$mRebostsAmbComandaResum[$grupId]['ctrTecos'];
			$cDp_eurosCtT+=$mRebostsAmbComandaResum[$grupId]['ctkTeuros']+$mRebostsAmbComandaResum[$grupId]['ctrTeuros'];
			$cDp_umsFdT+=$mRebostsAmbComandaResum[$grupId]['umstFDC'];
			$cDp_ecosFdT+=$mRebostsAmbComandaResum[$grupId]['ecostFDC'];
			$cDp_eurosFdT+=$mRebostsAmbComandaResum[$grupId]['eurostFDC'];

		}
		//--------------------------
		//obtenir abonaments i carrecs
			$mAbonamentsCarrecs=db_getAbonamentsCarrecs($grupId,$db); // nom�s de la cac al grup
			// suma abonaments/carrecs:
			$sumaAbonamentsCarrecsEcos=0;
			$sumaAbonamentsCarrecsEb=0;
			$sumaAbonamentsCarrecsEu=0;
			$sumaAbonamentsCarrecsUms=0;
			while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
			{
				if($mAbonamentCarrec['tipus']=='abonamentCAC')
				{
					$sumaAbonamentsCarrecsEcos-=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb-=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu-=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms-=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
				else if($mAbonamentCarrec['tipus']=='carrecCAC')
				{
					$sumaAbonamentsCarrecsEcos+=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb+=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu+=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms+=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
			}
			
			$sumaAbonamentsCarrecsEcosT+=$sumaAbonamentsCarrecsEcos;
			$sumaAbonamentsCarrecsEbT+=$sumaAbonamentsCarrecsEb;
			$sumaAbonamentsCarrecsEuT+=$sumaAbonamentsCarrecsEu;
			$sumaAbonamentsCarrecsUmsT+=$sumaAbonamentsCarrecsUms;
	//--------------------------
	
	$GEdistribUms=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
	$GEdistribEcos=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$sumaAbonamentsCarrecsEcos;
	$GEdistribEb=0+$sumaAbonamentsCarrecsEb;
	$GEdistribEu=$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEu;
	
	$mGEdistrib=array();
	$mGEdistrib['ums']=number_format($GEdistribUms,2,'.','');
	$mGEdistrib['ecos']=number_format($GEdistribEcos,2,'.','');
	$mGEdistrib['eb']=number_format($GEdistribEb,2,'.','');
	$mGEdistrib['euros']=number_format($GEdistribEu,2,'.','');

	return $mGEdistrib;
}
?>

		