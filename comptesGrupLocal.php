<?php
//pel responsable de grup i ladmin

include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "eines.php";
include "db_comptesGrupLocal.php";
include "db_incidencies.php";
include "db_distribucioGrups.php";
include "db_gestioMagatzems.php";
include "html_ajuda1.php";
include "db_ajuda.php";


//--------------------------------------------------------------------

function html_nomsColumnesHelp()
{
	global $mAjuda;
	echo "
		<tr>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Dades</b></p></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Consum</b>&nbsp;".(html_ajuda1('comptesGrupLocal.php',2))."</p></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Producci�</b>&nbsp;".(html_ajuda1('comptesGrupLocal.php',3))."</p></td></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Balan�&nbsp;".(html_ajuda1('comptesGrupLocal.php',4))."</b></p></td></td>
		</tr>
	";

	return;
}

//--------------------------------------------------------------------

function html_nomsColumnes()
{
	echo "
		<tr>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Dades:</b></p></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Consum:</b></p></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Producci�:</b></p></td></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Balan�:</b></p></td></td>
		</tr>
	";

	return;
}
//--------------------------------------------------------------------


function html_nomsColumnesTotals()
{
	echo "
		<tr>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Dades:</b></p></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Consum:</b></p></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Producci�:</b></p></td></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Balan�:</b></p></td></td>
		</tr>
	";

	return;
}
//--------------------------------------------------------------------

$mParsCAC=array();
$quantitatTotalCac=0;
$quantitatTotalRebosts=0;
$mCr=array();
$missatgeAlerta='';
$mMissatgeAlerta['missatge']='';
$mMissatgeAlerta['result']='';

$mProblemaAmb=array();

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

unset($mPars['compte_ecos']);
unset($mPars['compte_cieb']);



$llistaId=@$_GET['sL']; //selector de ruta
if(isset($llistaId))
{
	$mPars['selLlistaId']=$llistaId;
}
else
{
	$llistaId=@$_POST['i_selLlista'];

	if(isset($llistaId))
	{
		$mPars['selLlistaId']=$llistaId;
	}
}



$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];

getConfig($db); //inicialitza variables anteriors;

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=0;
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';

	$mPars['fPagamentUms']=0;
	$mPars['fPagamentEcos']=0;
	$mPars['fPagamentEb']=0;
	$mPars['fPagamentEu']=0;
	$mPars['fPagamentEuMetalic']=0;
	$mPars['fPagamentEuTransf']=0;
	$mPars['fPagamentEcosIntegralCES']=0;

	$mPars['fPagamentEcosPerc']=0;
	$mPars['fPagamentEbPerc']=0;
	$mPars['fPagamentEuPerc']=0;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['paginacio']=-1;
	if(!isset($mPars['pagS'])){$mPars['pagS']=0;}
	if(!isset($mPars['pagFiltre'])){$mPars['pagFiltre']=$mPars['sortBy'].$mPars['veureProductesDisponibles'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vProductor'].$mPars['vSubCategoria'].$mPars['vCategoria'];}
	if(!isset($mPars['numProdsPag'])){$mPars['numProdsPag']=10;}
	if(!isset($mPars['numPags'])){$mPars['numPags']=0;} 

post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['html.php']=db_getAjuda('html.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);
$mAjuda['comptesGrupLocal.php']=db_getAjuda('comptesGrupLocal.php',$db);

if($mPars['selLlistaId']==0)
{
	$mRuta=explode('_',$mPars['selRutaSufix']);
	if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

	$mPars['taulaProductes']='productes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandes']='comandes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandesSeg']='comandes_seg';
	$mPars['taulaIncidencies']='incidencies_'.$mPars['selRutaSufix'];
	if(count($mRuta)==2)//periode especial
	{
		$mPars['taulaProductors']='productors_'.$mPars['selRutaSufix'];
	}
	else
	{
		$mPars['taulaProductors']='productors';
	}
	
}
else
{
	$mPars['taulaProductes']='productes_grups';
	$mPars['taulaProductors']='productors';
	$mPars['taulaComandes']='comandes_grups';
	$mPars['taulaComandesSeg']='comandes_seg_grups';
	$mPars['taulaIncidencies']='incidencies_grups';

	$mPeriodesLocalsInfo=db_getPeriodesLocalsInfo($db);
	if
	(
		!isset($mPars['sel_periode_comanda_local'])
		||
		$mPars['sel_periode_comanda_local']==''
	)
	{
		if(isset($mPeriodesLocalsInfo))
		{
			$mPars['sel_periode_comanda_local']=key($mPeriodesLocalsInfo);
		}
	}
}

$mUsuari=db_getUsuari($mPars['usuari_id'],$db);

$mPars['grup_id']=$mPars['selLlistaId'];
$mGrup=getGrup($db);
$mRebost=$mGrup;

$mGrupsRef=db_getGrupsRef($db); 
$mUsuarisRef=db_getUsuarisGrupRef2($db);
$mPerfilsRef=db_getPerfilsRef($db);
if
(
	!checkLogin($db)
	||
	(
		(
			$mPars['usuari_id']!=@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'] //resp grup
			&&
			(
				$mPars['selLlistaId']==0
				&&
				isset($mPars['selPerfilRef'])
				&&
				$mPars['selPerfilRef']!=''
				&&
				$mPars['selPerfilRef']!='TOTS'
				&&
				$mPars['usuari_id']!=@$mPerfilsRef[$mPars['selPerfilRef']]['usuari_id'] //resp perfil
			)
			&&
			(
				$mPars['nivell']!='sadmin'
				||
				$mPars['nivell']!='admin'
				|| 
				$mPars['nivell']=='coord'
			)
			
		)
	)
	||
	$mPars['nivell']=='visitant'
)
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}

$mLlistesRef=$mGrupsRef;//db_getLlistesRef($db);
$mPerfil=db_getPerfil($db);
$mPerfilResp=db_getUsuari($mPerfil['usuari_id'],$db);
$mGrupsAssociatsRef=db_getGrupsAssociatsPerfilRef($db);
//if($mPars['usuari_id']==$mPerfilResp['id'] || $mPars['nivell']=='coord')
//{
	if($mPars['selLlistaId']==0)
	{
		$mGrupsRef_=$mGrupsAssociatsRef;
		//$mGrupsRef_[$mPerfil['grup_vinculat']]=$mGrupsRef[$mPerfil['grup_vinculat']];
		$mGrupsRef=$mGrupsRef_;
		$mGrupsRef[0]=array('id'=>0,'nom'=>'CAC');
		//$mLlistesRef[0]=array('id'=>0,'nom'=>'CAC');
		$mLlistesRef=$mGrupsRef;
	}
	else
	{
		$mGrupsRef=$mGrupsAssociatsRef;
		$mGrupsRef[0]=array('id'=>0,'nom'=>'CAC');
		$mLlistesRef=$mGrupsRef;//db_getLlistesRef($db);
		$mPerfilsRef[$mPerfil['id']]=$mPerfil;
	}
//}

$mPerfilResp=db_getUsuari($mPerfil['usuari_id'],$db);



$mProductes=db_getProductes2($db);
//*v36-18-12-15 modificar funcio assignacio



$mPropietatsGrup=@getPropietatsGrup($mGrupsRef[$mPars['selLlistaId']]['propietats']);



	$mPars['paginacio']=0;
$mUsuarisAmbComandaResum=db_getUsuarisAmbComandaResum2($db); 
	$mPropietatsPeriodesLocals=db_getPropietatsPeriodesLocals($db);
	$mPeriodesLocalsInfo=db_getPeriodesLocalsInfo($db);
		$mPeriodeNoTancat=getPeriodeLocalNoTancat();



$mRutesSufixes=getRutesSufixes($db);
//*v36 5-1-16 assignacio
$mPeriodesInfo=db_getPeriodesInfo($db);

$mComandaPerProductors=db_getComandaPerProductors($db);


$mPeticionsGrup=getPeticionsGrup();
$mComptesGrup=db_getComptesGrup($db);


echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>"; echo $htmlTitolPags; echo " - Comptes GRUP (LOCAL)</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_comptes.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
ruta=".$ruta.";
</SCRIPT>
</head>

<body bgcolor='#ffffff'  onload=\"\" bgcolor='".$mColors['body']."'>
";
html_demo('comptesGrupLocal.php?');
echo "
<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
	<tr>
		<td style='width:100%;' align='center'>
		<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
		".$mContinguts['index']['titol1']."</b>
		</td>
	</tr>
</table>

";
$mRutesSufixes=getRutesSufixes($db);
//*v36 5-1-16 assignacio
$mPeriodesInfo=db_getPeriodesInfo($db);

	echo "
<table   width='50%' align='center'  bgcolor='".$mColors['table']."'>
	<tr>
		<td align='center' valign='top' width='100%'>
		<table width='100%' align='center' >
			<tr>
	";
	if(isset($mPars['selPerfilRef']) && $mPars['selPerfilRef']!='' && $mPars['selPerfilRef']!='TOTS')	// entra la productora o admin
	{
		echo "
				<td width='50%' align='right' valign='top'>
		";
		mostrarSelectorLlistaPerProductores('comptesGrup.php','comptesGrupLocal.php',$db);
	}
	else
	{
		echo "
				<td width='50%' align='center' valign='middle'>
				<p style=font-size:14px;'>Lista Local: <b>".(urldecode($mGrup['nom']))."</b></p>
		";
	}
	echo "
				</td>
				<td width='50%' align='left'  valign='top'>
	";
	mostrarSelectorPeriodeLocalInfo(1);
	echo "
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<table width=100%'  bgcolor='".$mColors['table']."'>
	<tr>
		<td id='td_missatgeAlerta' style=' width:100%;' align='center'  valign='bottom'>
		".$mMissatgeAlerta['missatge']."
		</td>
	</tr>
</table>

";

if(isset($mPars['selPerfilRef']) && $mPars['selPerfilRef']!=0 && $mPars['selPerfilRef']!='TOTS')
{
	echo "
<table style='width:80%;' align='center'  bgcolor='".$mColors['table']."'>
	<tr>
		<td style='width:25%;' align='left' valign='top'>
		</td>

		<td style='width:75%;' align='center' valign='bottom'>
		<table width='80%'>
			<tr>
				<td align='left' width='40%' valign='middle'>
				<p>Perfil de productor:&nbsp;</p>
				<p>Responsable: ".(urldecode($mPerfilResp['usuari']))."</b>
				<br>
				(".(urldecode($mPerfilResp['email']))." )
				</p>
				</td>
				
				<td align='center' width='60%' valign='middle'>
				<p style='font-size:30px; color:LightBlue;'><i><b>".(urldecode($mPerfilsRef[$mPars['selPerfilRef']]['projecte']))."</b></i></p>
				</td>
				<td align='center'>
";
if($mPars['selLlistaId']!=0)
{
	if(substr_count($mPropietatsGrup['idsPerfilsActiusLocal'],','.$mPars['selPerfilRef'].',')>0)
	{
		$estatPerfilAllistaLocal="<font style='font-size:10px;' color='green'><b>ACTIU</b></font>";
	}
	else
	{
		$estatPerfilAllistaLocal="<font style='font-size:10px;' color='red'><b>INACTIU</b></font>";
	}
	echo " 
				&nbsp;(".$estatPerfilAllistaLocal.")</p>
	";
}
	echo "
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
	";
}
echo "

";


if(strlen($missatgeAlerta)>0)
{
	echo "
<center>
<div style='z-index:0; position:relative; top:0px; visibility:inherit;  width:70%;'>
	";
}
else
{
	echo "
<center>
<div style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
	";
} 
echo "
<p style='text-align:center;'>".$missatgeAlerta."</p>
</center>
</div>


<table align='center' width='90%'  bgcolor='".$mColors['table']."'>
	<tr>
		<td>
		<p>
		Usuari:<b> ".(urldecode($mPars['usuari']))."</b> (".$mPars['nivell'].")
		";
		if($mUsuari['id']==@$mPerfil['usuari_id'])
		{
			echo "
		<br>
		<font style='font-size:10px;' >Funcions:Responsable del Perfil</font>
			";
		}
		if($mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'])
		{
			echo "
		<br>
		<font style='font-size:10px;' >Funcions:Responsable de Grup</font>
			";
		}
		
		echo "
		</p>
		</td>

		<td valign='bottom' style='width:25%;'>
		</td>
		
		<td valign='middle' align='right' style='width:25%;'>
		<p>".(date('H:i:s d/m/Y'))."<p>
		</td>
	</tr>
</table>



<table align='center' bgcolor='#dddddd' width='90%'  bgcolor='".$mColors['table']."'>
";


$contLinies=0;

$kgT=0;

$cAp_umsT=0;
$cAp_umsCtT=0;
$cAp_ecosCtT=0;
$cAp_eurosCtT=0;

$cAp_umsFdT=0;
$cAp_ecosFdT=0;
$cAp_eurosFdT=0;

$cont=0;

$sumaAbonamentsCarrecsEcosT=0;
$sumaAbonamentsCarrecsEbT=0;
$sumaAbonamentsCarrecsEuT=0;
$sumaAbonamentsCarrecsUmsT=0;
		
$mColorLinia['haProduit']='#CEF6FB';
$mColorLinia['haProduit2']='#77ffff';
$mColorLinia['haConsumit']='#DCFBCE';
$mColorLinia['haProduitIconsumit']='#F1CEFB';
$mColorLinia['haProduitNoMembre']='#aaff44';
$colorLinia='#ffffff';

$cpUms=0; //import distribucio cac a grups segons forma pagament dels grups, inclou abonaments i c�rrecs (ums)
$cpEcos=0;
$cpEb=0;
$cpEu=0;

$cpUmsT=0; //Total imports distribucio cac a grups segons forma pagament dels grups, inclou abonaments i c�rrecs (ums)
$cpEcosT=0;
$cpEbT=0;
$cpEuT=0;

$GEdistribUmsT=0; //GE- Total imports distribucio cac a grups segons % ms productes (ums)
$GEdistribEcosT=0;
$GEdistribEbT=0;
$GEdistribEuT=0;

$cDp_umsT=0;
$cDp_ecosT=0;
$cDp_eurosT=0;
$cDp_umsCtT=0;
$cDp_ecosCtT=0;
$cDp_eurosCtT=0;
$cDp_umsFdT=0;
$cDp_ecosFdT=0;
$cDp_eurosFdT=0;

	$cAp_umsT=0;
	$cAp_ecosT=0;
	$cAp_eurosT=0;


$mProductesJaEntregats=db_getProductesJaEntregats2($db);
$mProductesJaEntregatsTot=getProductesJaEntregatsTOT($mProductesJaEntregats);
$totalJaEntregatUms=0;
	
	$sumaAbonamentsCarrecsUmsT=0;	

while(list($usuariId,$mUsuari)=each($mUsuarisRef))
{

	if
	(
		(
			(		
				isset($mPars['selPerfilRef'])
				&&
				$usuariId==$mPerfilsRef[$mPars['selPerfilRef']]['usuari_id']
			)
			||
			(
				!isset($mPars['selPerfilRef'])
			)
		)
	)
	{
		$haProduit=0;
		$haConsumit=0;
		//consum
			$mPars['selUsuariId']=$usuariId;
		db_getFormaPagamentLocal($db);
		$cpUms=0; 
		$cpEcos=0;
		$cpEb=0;
		$cpEu=0;

		$cDp_ums=0;
 		$cDp_ecos=0;
		$cDp_euros=0;
 		$cDp_umsCt=0;

		$cDp_ecosCt=0;
		$cDp_eurosCt=0;
		$cDp_umsFd=0;
		$cDp_ecosFd=0;

		$cDp_eurosFd=0;

		if(!isset($mUsuarisAmbComandaResum[$usuariId]))
		{
			$mUsuarisAmbComandaResum[$usuariId]['kgT']=0;
			
			$mUsuarisAmbComandaResum[$usuariId]['umsT']=0;
			$mUsuarisAmbComandaResum[$usuariId]['ecosT']=0;
			$mUsuarisAmbComandaResum[$usuariId]['eurosT']=0;
			$mUsuarisAmbComandaResum[$usuariId]['ctkTums']=0;

			$mUsuarisAmbComandaResum[$usuariId]['ctkTecos']=0;
			$mUsuarisAmbComandaResum[$usuariId]['ctkTeuros']=0;
			$mUsuarisAmbComandaResum[$usuariId]['umstFDC']=0;
			$mUsuarisAmbComandaResum[$usuariId]['ecostFDC']=0;

			$mUsuarisAmbComandaResum[$usuariId]['eurostFDC']=0;
			$mUsuarisAmbComandaResum[$usuariId]['compte_ecos']='';
			$mUsuarisAmbComandaResum[$usuariId]['compte_cieb']='';
			$mUsuarisAmbComandaResum[$usuariId]['propietats']='';
		}
		else
		{
			$cDp_ums=-1*$mUsuarisAmbComandaResum[$usuariId]['umsT'];
			$cDp_ecos=-1*$mUsuarisAmbComandaResum[$usuariId]['ecosT'];
			$cDp_euros=-1*$mUsuarisAmbComandaResum[$usuariId]['eurosT'];
			$cDp_umsCt=-1*$mUsuarisAmbComandaResum[$usuariId]['ctkTums'];

			$cDp_ecosCt=-1*$mUsuarisAmbComandaResum[$usuariId]['ctkTecos'];
			$cDp_eurosCt=-1*$mUsuarisAmbComandaResum[$usuariId]['ctkTeuros'];
			$cDp_umsFd=-1*$mUsuarisAmbComandaResum[$usuariId]['umstFDC'];
			$cDp_ecosFd=-1*$mUsuarisAmbComandaResum[$usuariId]['ecostFDC'];

			$cDp_eurosFd=-1*$mUsuarisAmbComandaResum[$usuariId]['eurostFDC'];
	
			$cDp_umsT-=$mUsuarisAmbComandaResum[$usuariId]['umsT'];
			$cDp_ecosT-=$mUsuarisAmbComandaResum[$usuariId]['ecosT'];
			$cDp_eurosT-=$mUsuarisAmbComandaResum[$usuariId]['eurosT'];
			$cDp_umsCtT-=$mUsuarisAmbComandaResum[$usuariId]['ctkTums'];

			$cDp_ecosCtT-=$mUsuarisAmbComandaResum[$usuariId]['ctkTecos'];
			$cDp_eurosCtT-=$mUsuarisAmbComandaResum[$usuariId]['ctkTeuros'];
			$cDp_umsFdT-=$mUsuarisAmbComandaResum[$usuariId]['umstFDC'];
			$cDp_ecosFdT-=$mUsuarisAmbComandaResum[$usuariId]['ecostFDC'];

			$cDp_eurosFdT-=$mUsuarisAmbComandaResum[$usuariId]['eurostFDC'];

			$haConsumit=1;
		}
		
			$mPars['vEstatIncd']='aplicat';
		$mAbonamentsCarrecs=db_getAbonamentsCarrecs($mRebost['id'],$db); // nom�s de la cac a� grup
			$mPars['vPeriodeLocalIncd']=$mPars['sel_periode_comanda_local'];
			$mPars['vUsuariIncd']=$usuariId;
			$mPars['vEstatIncd']='pendent'; // per compatibilitzar amb db_getIncidencies();
			$mPars['vGrupIncd']=$mPars['grup_id'];
		$mIncidenciesIncd=db_getIncidencies(true,'incd',' * ',$db);		
			$mPars['vEstatIncd']='aplicat'; // per compatibilitzar amb db_getIncidencies();
		$mIncidenciesAbca=db_getIncidencies(true,'abCa',' * ',$db);	
		$incidenciesIncdText='';
		$incidenciesAbcaText='';
		$productesEspecialsText='';
		if(count($mIncidenciesIncd)>0){	$incidenciesIncdText="<font style='color:#ff0000;'><b>".(count($mIncidenciesIncd))."</b></font>";}
		else {	$incidenciesIncdText="<font style='color:#000000;'><b>".(count($mIncidenciesIncd))."</b></font>";}
		if(count($mIncidenciesAbca)>0){	$incidenciesAbcaText="<font style='color:#0000aa;'><b>".(count($mIncidenciesAbca))."</b></font>";}
		else {	$incidenciesAbcaText="<font style='color:#000000;'><b>".(count($mIncidenciesAbca))."</b></font>";}

	
		//produccio
		$cAp_ums=0;
		$cAp_ecos=0;
		$cAp_euros=0;
		$cAp_umsCt=0;
		$cAp_ecosCt=0;
		$cAp_eurosCt=0;
		$cAp_umsFd=0;
		$cAp_ecosFd=0;
		$cAp_eurosFd=0;

		unset($mPerfilsRefUsuari);
		$mPerfilsRefUsuari=array();
		while(list($perfilId,$mComandaPerProductor)=@each($mComandaPerProductors))
		{
			if($mPerfilsRef[$perfilId]['usuari_id']==$usuariId) //l'usuari es tb el resp del perfil local
			{
					$selPerfilRef_mem=@$mPars['selPerfilRef'];
					$mPars['selPerfilRef']=$perfilId;
				$mPerfil=db_getPerfil($db);
					$mPars['selPerfilRef']=@$selPerfilRef_mem;
					
				$mPerfilsRefUsuari[$perfilId]=$mPerfil;
				while(list($producteId,$mComandaAproductor)=@each($mComandaPerProductor))
				{
					$cAp_ums+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
					$cAp_ecos+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
					$cAp_euros+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
				}
	
			}
		}
		reset($mComandaPerProductors);
	
		//nom�s mostrar linies amb algun valor diferent de zero

		// suma abonaments/carrecs:
		$sumaAbonamentsCarrecsEcos=0;
		$sumaAbonamentsCarrecsEb=0;
		$sumaAbonamentsCarrecsEu=0;
		$sumaAbonamentsCarrecsUms=0;
		while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
		{
			if($mAbonamentCarrec['sel_usuari_id']==$usuariId)
			{
				if($mAbonamentCarrec['tipus']=='abonamentGrup')
				{
					$sumaAbonamentsCarrecsEcos+=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb+=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu+=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms+=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
				else if($mAbonamentCarrec['tipus']=='carrecGrup')
				{
					$sumaAbonamentsCarrecsEcos-=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb-=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu-=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms-=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
			}
		}
			
		$sumaAbonamentsCarrecsEcosT+=$sumaAbonamentsCarrecsEcos;
		$sumaAbonamentsCarrecsEbT+=$sumaAbonamentsCarrecsEb;
		$sumaAbonamentsCarrecsEuT+=$sumaAbonamentsCarrecsEu;
		$sumaAbonamentsCarrecsUmsT+=$sumaAbonamentsCarrecsUms;

		$cpUms=-1*($mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu'])+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
		$cpEcos=-1*$mPars['fPagamentEcos']+$sumaAbonamentsCarrecsEcos;
		$cpEb=-1*$mPars['fPagamentEb']+$sumaAbonamentsCarrecsEb;
		$cpEu=-1*$mPars['fPagamentEu']+$sumaAbonamentsCarrecsEu;

		$GEdistribUms=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
		$GEdistribEcos=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$sumaAbonamentsCarrecsEcos;
		$GEdistribEb=0+$sumaAbonamentsCarrecsEb;
		$GEdistribEu=$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEu;

		if($cAp_ums>0){$haProduit=1;}
		if($haProduit==1 && $haConsumit==1){$colorLinia=$mColorLinia['haProduitIconsumit'];}
		else
		if($haProduit==1 && $haConsumit==0){$colorLinia=$mColorLinia['haProduit'];}
		else
		if($haProduit==0 && $haConsumit==1){$colorLinia=$mColorLinia['haConsumit'];}
		
		if($sumaAbonamentsCarrecsUms!=0 || $cpUms!=0 || $GEdistribUms!=0 || $cAp_ums!=0)
		{
			if($cont>0){html_nomsColumnes();}else{html_nomsColumnesHelp();}
			echo "
	<tr bgcolor='".$colorLinia."'>
		<td valign='top'>
		<p>[".$cont."]<br></p>
		<p class='p_micro'>
		Usuaria: <b>".(urldecode($mUsuarisRef[$usuariId]['usuari']))."</b>
		<br>
		email: ".(urldecode($mUsuarisRef[$usuariId]['email']))."
		<br>
		mobil: ".$mUsuarisRef[$usuariId]['mobil']."
		<br>
		adre�a: ".(urldecode($mUsuarisRef[$usuariId]['adressa']))."
		<br>
			";
			if(count($mPerfilsRefUsuari)>0)
			{
				echo "
		<br>
		Responsable dels Perfils<br>de productor local:
		<br>
 	 			";
				while(list($perfilId,$mPerfilUsuari)=each($mPerfilsRefUsuari))
				{
					echo "
		<b><font color='blue'>&nbsp;&nbsp;".(urldecode($mPerfilUsuari['projecte']))."</font></b><br>
					";
				}
				reset($mPerfilsRefUsuari);
				$perfilsIdChain=implode(',',array_keys($mPerfilsRefUsuari));
			}
			
			echo "
		</p>
		</td>

			
		<td valign='top'>
		<br>
		<table width='100%'>
			<tr>
				<td>
				<p><b>".(number_format($cpUms,2))."</b> ums</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format($cpEcos,2))." ecos  "; if($cpEcos!=0){echo $mPars['fPagamentCompteEcos'];} if($mPars['fPagamentEcosIntegralCES']=='1'){echo "&nbsp;<font size='1'><i><a title='INTEGRALCES'>I-CES</a></i></font> ";}else{echo "&nbsp;<font size='1'><i><a title='CES'>CES</a></i></font> ";} echo "
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format($cpEb,2))." ".$mParametres['moneda3Abrev']['valor']; if($cpEb!=0){echo $mPars['fPagamentCompteCieb'];} echo "&nbsp;<font size='1'><i><a title='INTEGRALCES'>I-CES</a></i></font></p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format($cpEu,2))." euros "; if($mPars['fPagamentEuMetalic']==1){echo " <i>en met�l.lic</i> ";}else{echo " <i>transfer�ncia</i> ";} echo "
				".$productesEspecialsText."
				<p onClick=\"enviarFpars('vistaAlbaraLocal.php?sR=".$mPars['selRutaSufix']."&gRef=".$mRebost['ref']."&sUid=".$usuariId."','_blank');\" style='cursor:pointer;'><u>albar�</u></p>
				<p>&nbsp;&nbsp;[ Incidencies: ".$incidenciesIncdText."]
				<br>&nbsp;&nbsp;[ Abonaments/c�rrecs: ".$incidenciesAbcaText."]
				<br>
				</p>
 				</td>
			</tr>
		</table>
			";
			$styleAC='';
			if($sumaAbonamentsCarrecsUms!=0){$styleAC=" style='color:#0000aa;' ";}else{$styleAC='';}
			echo "
		<a title=\"Suma d'abonaments i c�rrecs de la CAC al Grup per aquesta comanda\"><p class='compacte'>A/C:<font  ".$styleAC.">".(number_format($sumaAbonamentsCarrecsUms,2))."</font> ums<p></a>
		<p class='compacte'>".(number_format($mUsuarisAmbComandaResum[$usuariId]['kgT'],2))." kg<p>
		<p class='compacte'>&nbsp;</p>
			";
			if($cAp_ums>0)
			{
				$mPars['selUsuariId']=$usuariId;
				echo "<p class='albara'>data comanda:<br>".(formatarData(getDataComanda($db,$mPars['grup_id'])))."</p>";
			}
			echo "
		</td>
		
			
		<td valign='top'>
		<br>
		<table width='100%'>
			<tr>
				<td>
				<p><b>".(number_format($cAp_ums,2))."</b> ums</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cAp_ecos),2))." ecos</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cAp_euros),2))." euros</p>
				</td>
			</tr>
			<tr>
				<td>
			";
			if($haProduit)
			{
				echo "
				<p class='compacte' onClick=\"enviarFpars('distribucioGrupsLocals.php?getPs=".$perfilsIdChain."','_blank');\" style='color:#000000;  cursor:pointer;'><u>Total Reservat</u></p>
				";
				if($cAp_ums>0)
				{
					echo "
				<p>(reserves: <b>".(number_format(($cAp_ecos/$cAp_ums)*100,2))."</b> % ms)</p>
					";	
				}
			}			
			echo "
				</td>
			</tr>
		</table>
		</td>
			

		<td valign='top'>
		<br>
		<table width='100%'>
			<tr>
				<td>
				<p><b>".(number_format(($cpUms+$cAp_ums),2))." </b> ums</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cpEcos+$cAp_ecos),2,'.',''))." ecos
				</td>
			</tr>
			<tr>
				<td>
				<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cpEu+$cAp_euros),2,'.',''))." euros</p>
				</td>
			</tr>
		</table>
		</td>
	</tr>
			";
		}

		$kgT+=$mUsuarisAmbComandaResum[$usuariId]['kgT'];

		$cpUmsT+=$cpUms;
		$cpEcosT+=$cpEcos;
		$cpEbT+=$cpEb;
		$cpEuT+=$cpEu;

		$cAp_umsT+=$cAp_ums;
		$cAp_ecosT+=$cAp_ecos;
		$cAp_eurosT+=$cAp_euros;

		$sumaAbonamentsCarrecsUmsT+=$sumaAbonamentsCarrecsUms;
		$sumaAbonamentsCarrecsEcosT+=$sumaAbonamentsCarrecsEcos;
		$sumaAbonamentsCarrecsEbT+=$sumaAbonamentsCarrecsEb;
		$sumaAbonamentsCarrecsEuT+=$sumaAbonamentsCarrecsEu;

		$contLinies++;
		$cont++;
		
	}
}	

$cont2=0;
////////////////////////////////////////////////////////////////////////////////
// repetim pels perfils associats d'usuaris no membres del grup (i per tant, que no han fet comanda)
///////////////////////////////////////////////////////////////////////////////
while(list($perfilId,$mPerfil)=each($mPerfilsRef))
{
	$mPerfilResp=db_getUsuari($mPerfil['usuari_id'],$db);
	if(!array_key_exists($mPerfil['usuari_id'],$mUsuarisRef))
	{
		$colorLinia='#ffffff';
		$haProduit=0;
		$haConsumit=0;
	
		//consum
		//	$mPars['selUsuariId']=$usuariId;
		//db_getFormaPagamentLocal($db);

		$cpUms=0; 
		$cpEcos=0;
		$cpEb=0;
		$cpEu=0;

		$cDp_ums=0;
 		$cDp_ecos=0;
		$cDp_euros=0;
 		$cDp_umsCt=0;

		$cDp_ecosCt=0;
		$cDp_eurosCt=0;
		$cDp_umsFd=0;
		$cDp_ecosFd=0;

		$cDp_eurosFd=0;
/*
	if(!isset($mUsuarisAmbComandaResum[$usuariId]))
	{
		$mUsuarisAmbComandaResum[$usuariId]['kgT']=0;
			
		$mUsuarisAmbComandaResum[$usuariId]['umsT']=0;
		$mUsuarisAmbComandaResum[$usuariId]['ecosT']=0;
		$mUsuarisAmbComandaResum[$usuariId]['eurosT']=0;
		$mUsuarisAmbComandaResum[$usuariId]['ctkTums']=0;

		$mUsuarisAmbComandaResum[$usuariId]['ctkTecos']=0;
		$mUsuarisAmbComandaResum[$usuariId]['ctkTeuros']=0;
		$mUsuarisAmbComandaResum[$usuariId]['umstFDC']=0;
		$mUsuarisAmbComandaResum[$usuariId]['ecostFDC']=0;

		$mUsuarisAmbComandaResum[$usuariId]['eurostFDC']=0;
		$mUsuarisAmbComandaResum[$usuariId]['compte_ecos']='';
		$mUsuarisAmbComandaResum[$usuariId]['compte_cieb']='';
		$mUsuarisAmbComandaResum[$usuariId]['propietats']='';
	}
	else
	{
		$cDp_ums=-1*$mUsuarisAmbComandaResum[$usuariId]['umsT'];
		$cDp_ecos=-1*$mUsuarisAmbComandaResum[$usuariId]['ecosT'];
		$cDp_euros=-1*$mUsuarisAmbComandaResum[$usuariId]['eurosT'];
		$cDp_umsCt=-1*$mUsuarisAmbComandaResum[$usuariId]['ctkTums'];

		$cDp_ecosCt=-1*$mUsuarisAmbComandaResum[$usuariId]['ctkTecos'];
		$cDp_eurosCt=-1*$mUsuarisAmbComandaResum[$usuariId]['ctkTeuros'];
		$cDp_umsFd=-1*$mUsuarisAmbComandaResum[$usuariId]['umstFDC'];
		$cDp_ecosFd=-1*$mUsuarisAmbComandaResum[$usuariId]['ecostFDC'];

		$cDp_eurosFd=-1*$mUsuarisAmbComandaResum[$usuariId]['eurostFDC'];

		$cDp_umsT-=$mUsuarisAmbComandaResum[$usuariId]['umsT'];
		$cDp_ecosT-=$mUsuarisAmbComandaResum[$usuariId]['ecosT'];
		$cDp_eurosT-=$mUsuarisAmbComandaResum[$usuariId]['eurosT'];
		$cDp_umsCtT-=$mUsuarisAmbComandaResum[$usuariId]['ctkTums'];

		$cDp_ecosCtT-=$mUsuarisAmbComandaResum[$usuariId]['ctkTecos'];
		$cDp_eurosCtT-=$mUsuarisAmbComandaResum[$usuariId]['ctkTeuros'];
		$cDp_umsFdT-=$mUsuarisAmbComandaResum[$usuariId]['umstFDC'];
		$cDp_ecosFdT-=$mUsuarisAmbComandaResum[$usuariId]['ecostFDC'];

		$cDp_eurosFdT-=$mUsuarisAmbComandaResum[$usuariId]['eurostFDC'];

	}
*/
		$haProduit=1;
			$mPars['vEstatIncd']='aplicat';
		$mAbonamentsCarrecs=db_getAbonamentsCarrecs($mRebost['id'],$db); // nom�s de la cac a� grup
			$mPars['vPeriodeLocalIncd']=$mPars['sel_periode_comanda_local'];
			$mPars['vUsuariIncd']=$mPerfilResp['id'];
			$mPars['vEstatIncd']='pendent'; // per compatibilitzar amb db_getIncidencies();
			$mPars['vGrupIncd']=$mPars['grup_id'];
		$mIncidenciesIncd=db_getIncidencies(true,'incd',' * ',$db);		
			$mPars['vEstatIncd']='aplicat'; // per compatibilitzar amb db_getIncidencies();
		$mIncidenciesAbca=db_getIncidencies(true,'abCa',' * ',$db);	
		$incidenciesIncdText='';
		$incidenciesAbcaText='';
		$productesEspecialsText='';
		if(count($mIncidenciesIncd)>0){	$incidenciesIncdText="<font style='color:#ff0000;'><b>".(count($mIncidenciesIncd))."</b></font>";}
		else {	$incidenciesIncdText="<font style='color:#000000;'><b>".(count($mIncidenciesIncd))."</b></font>";}
		if(count($mIncidenciesAbca)>0){	$incidenciesAbcaText="<font style='color:#0000aa;'><b>".(count($mIncidenciesAbca))."</b></font>";}
		else {	$incidenciesAbcaText="<font style='color:#000000;'><b>".(count($mIncidenciesAbca))."</b></font>";}

	
		//produccio
		$cAp_ums=0;
		$cAp_ecos=0;
		$cAp_euros=0;
		$cAp_umsCt=0;
		$cAp_ecosCt=0;
		$cAp_eurosCt=0;
		$cAp_umsFd=0;
		$cAp_ecosFd=0;
		$cAp_eurosFd=0;

		while(list($producteId,$mComandaAproductor)=@each($mComandaPerProductors[$perfilId]))
		{
			$cAp_ums+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
			$cAp_ecos+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
			$cAp_euros+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
		}
		@reset($mComandaPerProductors[$perfilId]);
		if($cAp_ums>0){$haProduit=1;}
							
		$colorLinia='#ffffff';
		if($haProduit){$colorLinia=$mColorLinia['haProduitNoMembre'];}
	
		//nom�s mostrar linies amb algun valor diferent de zero

		// suma abonaments/carrecs:
		$sumaAbonamentsCarrecsEcos=0;
		$sumaAbonamentsCarrecsEb=0;
		$sumaAbonamentsCarrecsEu=0;
		$sumaAbonamentsCarrecsUms=0;
		while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
		{
			if($mAbonamentCarrec['sel_usuari_id']==$mPerfilResp['id'])
			{
				if($mAbonamentCarrec['tipus']=='abonamentGrup')
				{
					$sumaAbonamentsCarrecsEcos+=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb+=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu+=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms+=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
				else if($mAbonamentCarrec['tipus']=='carrecGrup')
				{
					$sumaAbonamentsCarrecsEcos-=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb-=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu-=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms-=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
			}
		}
			
		$sumaAbonamentsCarrecsEcosT+=$sumaAbonamentsCarrecsEcos;
		$sumaAbonamentsCarrecsEbT+=$sumaAbonamentsCarrecsEb;
		$sumaAbonamentsCarrecsEuT+=$sumaAbonamentsCarrecsEu;
		$sumaAbonamentsCarrecsUmsT+=$sumaAbonamentsCarrecsUms;

		$cpUms=$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
		$cpEcos=$sumaAbonamentsCarrecsEcos;
		$cpEb=$sumaAbonamentsCarrecsEb;
		$cpEu=$sumaAbonamentsCarrecsEu;

		$GEdistribUms=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
		$GEdistribEcos=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$sumaAbonamentsCarrecsEcos;
		$GEdistribEb=0+$sumaAbonamentsCarrecsEb;
		$GEdistribEu=$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEu;

		if($sumaAbonamentsCarrecsUms!=0 || $cpUms!=0 || $GEdistribUms!=0 || $cAp_ums!=0)
		{
			if($cont2>0){html_nomsColumnes();}else{html_nomsColumnesHelp();}
			echo "
	<tr bgcolor='".$colorLinia."'>
		<td valign='top'>
		<p>[".$cont."]<br></p>
		<p class='p_micro'>
		Usuaria: <b>".(urldecode($mPerfilResp['usuari']))."</b>
		<br>
		email: ".(urldecode($mPerfilResp['email']))."
		<br>
		mobil: ".$mPerfilResp['mobil']."
		<br>
		adre�a: ".(urldecode($mPerfilResp['adressa']))."
		<br>
		<br>
		Responsable del Perfil<br>de productor associat:
		<br>
		<b><font color='blue'>&nbsp;&nbsp;".(urldecode($mPerfil['projecte']))."</font></b><br>
		</p>
		</td>

			
		<td valign='top'>
		<br>
		<table width='100%'>
			<tr>
				<td>
				<p><b>".(number_format(0,2))."</b> ums</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(0,2))." ecos  </p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(0,2))." ".$mParametres['moneda3Abrev']['valor']." </p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(0,2))." euros  </p>
				<p>&nbsp;&nbsp;[ Incidencies: ".$incidenciesIncdText."]
				<br>&nbsp;&nbsp;[ Abonaments/c�rrecs: ".$incidenciesAbcaText."]
				<br>
				</p>
				</td>
			</tr>
		</table>
			";
			$styleAC='';
			if($sumaAbonamentsCarrecsUms!=0){$styleAC=" style='color:#0000aa;' ";}else{$styleAC='';}
			echo "
		<a title=\"Suma d'abonaments i c�rrecs de la CAC al Grup per aquesta comanda\"><p class='compacte'>A/C:<font  ".$styleAC.">".(number_format($sumaAbonamentsCarrecsUms,2))."</font> ums<p></a>
		<p class='compacte'>&nbsp;</p>
		</td>
		
			
		<td valign='top'>
		<br>
		<table width='100%'>
			<tr>
				<td>
				<p><b>".(number_format($cAp_ums,2))."</b> ums</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cAp_ecos),2))." ecos</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cAp_euros),2))." euros</p>
				</td>
			</tr>
			<tr>
				<td>
			";
			if($haProduit)
			{
				echo "
				<p class='compacte' onClick=\"enviarFpars('distribucioGrupsLocals.php?getPs=".$perfilId."','_blank');\" style='color:#000000;  cursor:pointer;'><u>Total Reservat</u></p>
				";
				if($cAp_ums>0)
				{
					echo "
				<p>(reserves: <b>".(number_format(($cAp_ecos/$cAp_ums)*100,2))."</b> % ms)</p>
					";	
				}
			}			
			echo "
				</td>
			</tr>
		</table>
		</td>
			
			
		<td valign='top'>
		<br>
		<table width='100%'>
			<tr>
				<td>
				<p><b>".(number_format(($cAp_ums),2))." </b> ums</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cpEcos+$cAp_ecos),2,'.',''))." ecos
				</td>
			</tr>
			<tr>
				<td>
				<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cpEu+$cAp_euros),2,'.',''))." euros</p>
				</td>
			</tr>
		</table>
		</td>
	</tr>
			";
		}

		//$kgT+=$mUsuarisAmbComandaResum[$usuariId]['kgT'];

		$cpUmsT+=$cpUms;
		$cpEcosT+=$cpEcos;
		$cpEbT+=$cpEb;
		$cpEuT+=$cpEu;

		$cAp_umsT+=$cAp_ums;
		$cAp_ecosT+=$cAp_ecos;
		$cAp_eurosT+=$cAp_euros;

		$sumaAbonamentsCarrecsUmsT+=$sumaAbonamentsCarrecsUms;
		$sumaAbonamentsCarrecsEcosT+=$sumaAbonamentsCarrecsEcos;
		$sumaAbonamentsCarrecsEbT+=$sumaAbonamentsCarrecsEb;
		$sumaAbonamentsCarrecsEuT+=$sumaAbonamentsCarrecsEu;

		$contLinies++;
		$cont++;
		$cont2++;
	}
}
	




////////////////////////////////////////////////////////////////////////////////
//totals
////////////////////////////////////////////////////////////////////////////////

html_nomsColumnesTotals();

echo "
	<tr bgcolor='#FFFFaa'>
		<td valign='top'>
		<br>
		<p>Total: <b>".$kgT."</b> kg</p>
		<p  class='p_micro2'>
";
if(count($mPerfilsRef)>0)
{
	echo "
		<br>
		Perfils<br>de productor local:
		<br>
	";
	while(list($perfilId_,$mPerfil_)=each($mPerfilsRef))
	{
		echo "
		<b><font color='blue'>&nbsp;&nbsp;".(urldecode($mPerfil_['projecte']))."</font></b><br>
		";
	}
	reset($mPerfilsRef);
}

echo "
		</p>
		</td>

			
		<td valign='top'>
		<br>
		<table width='100%'>
			<tr>
				<td>
				<p><b>".(number_format($cpUmsT,2))."</b> ums</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format($cpEcosT,2))."</b> ecos</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format($cpEbT,2))." ".$mParametres['moneda3Abrev']['valor']."</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format($cpEuT,2))." euros </p>
				</td>
			</tr>
		</table>
";
$styleAC='';
if($sumaAbonamentsCarrecsUmsT!=0){$styleAC=" style='color:#0000aa;' ";}else{$styleAC='';}
echo "
		<a title=\"Suma TOTAL d'abonaments i c�rrecs per aquest periode local\"><p class='compacte'>A/C:<font  ".$styleAC.">".(number_format($sumaAbonamentsCarrecsUmsT,2))."</font> ums<p></a>
		<p class='compacte'>&nbsp;</p>
		</td>
		
			
			
		<td valign='top'>
		<br>
		<table width='100%'>
			<tr>
				<td>
				<p><b>".(number_format($cAp_umsT,2))."</b> ums"."</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cAp_ecosT),2))." ecos</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cAp_eurosT),2))." euros</p>
				</td>
			</tr>
		</table>
		</td>
			
			
		<td valign='top'>
		<br>
		<table width='100%'>
			<tr>
				<td>
				<p><b>".(number_format(($cpUmsT+$cAp_umsT),2))." </b> ums</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cpEcosT+$cAp_ecosT),2,'.',''))." ecos
				</td>
			</tr>
			<tr>
				<td>
				<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
				</td>
			</tr>
			<tr>
				<td>
				<p>".(number_format(($cpEuT+$cAp_eurosT),2,'.',''))." euros</p>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<div style='width:80%;' align='center'>
<BR><BR>
<table style='width:80%;'>
	<tr>
		<td>
		</td>
		<td>
		<p style='text-align:left;'>Marques visuals:</p>
		<td>
	</tr>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haConsumit']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que la usuaria ha fet comanda per� no ha produ�t pel GRUP en aquest periode de reserves local.
		<td>
	</tr>
</table>
<table style='width:80%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haProduit']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que la usuaria ha produit pel GRUP per� no ha fet comanda en aquest periode de reserves local.
		<td>
	</tr>
</table>
<table style='width:80%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haProduitIconsumit']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que la usuaria ha fet comanda i ha produ�t pel GRUP en aquest periode de reserves local, entrant en l'intercanvi directe.
		<td>
	</tr>
</table>
<table style='width:80%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haProduitNoMembre']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que la usuaria no es membre el grup per� ha produit pel GRUP en aquest periode de reserves local, com a productora associada.
		<td>
	</tr>
</table>
</div>

</center>
";

$parsChain=makeParsChain($mPars);

echo "
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
";
html_helpRecipient();
echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>

</body>
</html>
";
?>

		