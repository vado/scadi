<?php
//-------------------------------------------------------
function taulaResumComanda() //llista cac
{
	global $mPars,$rutaStr,$mParametres;
	
	if($mPars['grup_id']==0)
	{
		echo "
				<p style='font-size:10px;'>*  Resum del total reservat segons el % MS dels productors:
				<br>
				(s'apliquen els filtres seleccionats)
				</p>
		";
	}
	else
	{
		echo "
				<p  style='font-size:10px;'>*  Resum del total de la comanda segons el % MS dels productors:
				<br>
				(no s'apliquen els filtres seleccionats)</p>
		";
	}
	
	echo "
				<table  border='0'   cellspacing='1' cellpadding='1' align='center'   style='width:400px;'>
					<tr>
						<th align='right'  bgcolor='#D1FABB' style='width:50%; '>
						</th>

						<th align='right'  bgcolor='#D1FABB' style='width:10%; '>
						<p></p>
						</th>

						<th align='right'  bgcolor='#D1FABB' style='width:13%; '>
						<p>ums</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' style='width:13%; '>
						<p>Ecos</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' style='width:13%;'>
						<p>Euros</p>
						</th>
					</tr>
			
					<tr>
						<td  bgcolor='#EDF8E6' align='left'>
						<a title=\"Cost Transport Extern per kg sobre el producte\"><p style='font-size:11px;'>&nbsp;(CTEK)</p></a>
						</td>

						<td  bgcolor='#EDF8E6' align='right'>
						</td>

						<td  bgcolor='#EDF8E6' align='right' id='td_totalCtekUnitats'>
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtekEcos' >
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtekEuros' >
						<p>0</p>
						</td>
					</tr>
		
					<tr>
						<td   bgcolor='#EDF8E6' align='left'>
						<a title=\"Cost Transport Extern Repartit\"><p style='font-size:11px;'>&nbsp;(CTER)</p></a>
						</td>

						<td   bgcolor='#EDF8E6' align='right' >
						</td>

						<td   bgcolor='#EDF8E6' align='right' id='td_totalCterUnitats' >
						<p>0</p>
						</td>
						
						<td   bgcolor='#EDF8E6' align='right' id='td_totalCterEcos' >
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCterEuros' >
						<p>0</p>
						</td>
					</tr>
		
		
					<tr>
						<td   bgcolor='#EDF8E6' align='left'>
						<a title=\"Cost Transport Intern per kg sobre el producte\"><p style='font-size:11px;'>&nbsp;(CTIK)</p></a>
						</td>

						<td   bgcolor='#EDF8E6' align='right' style='width:10%;'>
						</td>

						<td   bgcolor='#EDF8E6' align='right' id='td_totalCtikUnitats' >
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtikEcos'>
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtikEuros'>
						<p>0</p>
						</td>
					</tr>
			
			
					<tr>
						<td  bgcolor='#EDF8E6'  align='left'>
						<a title=\"Cost Transport Intern Repartit\"><p style='font-size:11px;'>&nbsp;(CTIR)</p></a>
						</td>

						<td  bgcolor='#EDF8E6'  align='right' style='width:10%;'>
						</td>

						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtirUnitats'>
						<p>0</p>
						</td>
						
						<td   bgcolor='#EDF8E6' align='right' id='td_totalCtirEcos' >
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtirEuros'>
						<p>0</p>
						</td>

					</tr>
			
					<tr>
						<td  bgcolor='#D1FABB' align='left'>
						<a title=\"Total Cost Transport Productes CAC\"><p style='font-size:11px;'>&nbsp;Total Cost Transport (CAC)</p></a>
						</td>

						<td  bgcolor='#D1FABB' align='right' style='width:10%;'>
						</td>

						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportUnitats'>
						<p>0</p>
						</th>
						
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportEcos'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportEuros'>
						<p>0</p>
						</th>
					</tr>

					<tr>
						<td  bgcolor='#EDF8E6' align='left'>
						<a title=\"Total kg\"><p style='font-size:11px;'>&nbsp;Total kg</p></a>
						</td>

						<td  bgcolor='#EDF8E6' align='right' id='td_totalQuantitat' >
						<p>0</p>
						</td>

						<td bgcolor='#EDF8E6' >
						</td>
						
						
						<td  bgcolor='#EDF8E6' align='right'>
						</td>
						
						<td  bgcolor='#EDF8E6' align='right'>
						</td>
					</tr>

					<tr>
						<td  bgcolor='#D1FABB' align='left'>
						<a title=\"Preu productes CAC\"><p style='font-size:11px;'>&nbsp;<b>Preu productes CAC</b></p></a>
						</td>

						<td  bgcolor='#D1FABB' align='right'>
						</td>

						<th  bgcolor='#D1FABB' align='right' id='td_totalUnitats'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalEcos'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalEuros'>
						<p>0</p>
						</th>
					</tr>
			
					<tr>
						<td  bgcolor='#C0FAF8' align='left'>
						<a title=\"Fons Despeses CAC (".$mParametres['FDCpp']['valor']."% sobre preus, ".$mParametres['msFDCpp']['valor']."% MS)\"><p style='font-size:11px;'>&nbsp;<b>Fons despeses CAC</b></p></a>
						</td>

						<td  bgcolor='#C0FAF8' align='right'>
						</td>

						<th  bgcolor='#C0FAF8' align='right' id='td_totalUnitatsFDC'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#C0FAF8' align='right' id='td_totalEcosFDC'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#C0FAF8' align='right' id='td_totalEurosFDC'>
						<p>0</p>
						</th>
					</tr>

					<tr>
						<td  bgcolor='#EDF8E6' align='left'>
						<a title=\"Total kg Intercanvis entre Rebosts\"><p style='font-size:11px;'>&nbsp;<b>kg Inter-Rebosts</b></p></a>
						</td>

						<td  bgcolor='#EDF8E6' align='right' id='td_totalKgIntercanvisRebosts'>
						<p>0</p>
						</td>

						<td  bgcolor='#EDF8E6' align='right' style='width:10%;'>
						</td>

						<td  bgcolor='#EDF8E6' align='right'><p>&nbsp;</p></td>
									
						<td  bgcolor='#EDF8E6' align='right' ><p>&nbsp;</p></td>
					</tr>

					<tr>
						<th  bgcolor='#D1FABB' align='left'>
						<a title=\"Cost transport Intercanvis entre Rebosts\"><p style='font-size:11px;'>&nbsp;<b>Cost Transp. Inter-Rebosts</b></p></a>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' style='width:10%;'>
						</th>

						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportIntercanvisRebostsUnitats'><p>&nbsp;</p></th>
						
									
						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportIntercanvisRebostsEcos'><p>&nbsp;</p></th>
						

						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportIntercanvisRebostsEuros'><p>&nbsp;</p></th>

					</tr>			

					<tr>
						<th bgcolor='#D1FABB'  align='right'>
						<a title=\"Import Total\"><p style='font-size:11px;'>&nbsp;<b>Import Total</b></p></a>
						</th>

						<td bgcolor='#D1FABB'  align='right' ' >
						<p></p>
						</td>

						<th  bgcolor='#D1FABB' align='right' id='td_totalFacturaUnitats' >
						</th>

						<th  bgcolor='#D1FABB' align='right' id='td_totalFacturaEcos' >
						</th>

						
						<th  bgcolor='#D1FABB' align='right' id='td_totalFacturaEuros' >
						</th>

					</tr>

					<tr>
						<td  bgcolor='#D1FABB' align='right' id='td_pcMs'>
						<a title=\"% moneda social de la comanda, segons la moneda social dels productes demanats\"><p style='font-size:11px;'>&nbsp;</p></a>
						</td>

						<td  bgcolor='#D1FABB' align='right' >
						</td>
						
						<td  bgcolor='#D1FABB' align='right' style='width:10%;' >
						</td>
						
						<td bgcolor='#D1FABB' >
						</td>
						
						<td bgcolor='#D1FABB' >
						</td>
					</tr>
				</table>
	";
	return;
}

//-------------------------------------------------------
function taulaResumComandaLocal()
{
	global $rutaStr,$mPropietatsPeriodeLocal;
	
	echo "
				<p>  Resum de la comanda segons el % MS dels productors:</p>
				<table  border='0'   cellspacing='1' cellpadding='1' align='center'   style='width:400px;'>
					<tr>
						<th align='right'  bgcolor='#D1FABB' style='width:50%; '>
						</th>

						<th align='right'  bgcolor='#D1FABB' style='width:10%; '>
						<p></p>
						</th>

						<th align='right'  bgcolor='#D1FABB' style='width:13%; '>
						<p>ums</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' style='width:13%; '>
						<p>Ecos</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' style='width:13%;'>
						<p>Euros</p>
						</th>
					</tr>
			
";
/*
echo "
					<tr>
						<td   bgcolor='#EDF8E6' align='left'>
						<a title=\"Cost Transport Intern per kg sobre el producte\"><p style='font-size:11px;'>&nbsp;(CTIK)</p></a>
						</td>

						<td   bgcolor='#EDF8E6' align='right' style='width:10%;'>
						</td>

						<td   bgcolor='#EDF8E6' align='right' id='td_totalCtikUnitats' >
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtikEcos'>
						<p>0</p>
						</td>
						
						<td  bgcolor='#EDF8E6'  align='right' id='td_totalCtikEuros'>
						<p>0</p>
						</td>
					</tr>
";
*/
echo "
					<tr>
						<td  bgcolor='#D1FABB' align='left'>
						<a title=\"Total Cost Transport Productes\"><p style='font-size:11px;'>&nbsp;Total Cost Transport</p></a>
						</td>

						<td  bgcolor='#D1FABB' align='right' style='width:10%;'>
						</td>

						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportUnitats'>
						<p>0</p>
						</th>
						
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportEcos'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalCostTransportEuros'>
						<p>0</p>
						</th>
					</tr>

					<tr>
						<td  bgcolor='#EDF8E6' align='left'>
						<a title=\"Total kg\"><p style='font-size:11px;'>&nbsp;Total kg</p></a>
						</td>

						<td  bgcolor='#EDF8E6' align='right' id='td_totalQuantitat' >
						<p>0</p>
						</td>

						<td bgcolor='#EDF8E6' >
						</td>
						
						
						<td  bgcolor='#EDF8E6' align='right'>
						</td>
						
						<td  bgcolor='#EDF8E6' align='right'>
						</td>
					</tr>

					<tr>
						<td  bgcolor='#D1FABB' align='left'>
						<a title=\"Preu productes\"><p style='font-size:11px;'>&nbsp;<b>Preu productes</b></p></a>
						</td>

						<td  bgcolor='#D1FABB' align='right'>
						</td>

						<th  bgcolor='#D1FABB' align='right' id='td_totalUnitats'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalEcos'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#D1FABB' align='right' id='td_totalEuros'>
						<p>0</p>
						</th>
					</tr>
			
					<tr>
						<td  bgcolor='#C0FAF8' align='left'>
						<p style='font-size:11px;'>&nbsp;Fons despeses Grup:<br>
						(".$mPropietatsPeriodeLocal['fdLocal']."% sobre preus, ".$mPropietatsPeriodeLocal['ms_fdLocal']."% MS)</p>
						</td>

						<td  bgcolor='#C0FAF8' align='right'>
						</td>

						<th  bgcolor='#C0FAF8' align='right' id='td_totalUnitatsFDC'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#C0FAF8' align='right' id='td_totalEcosFDC'>
						<p>0</p>
						</th>
						
						<th  bgcolor='#C0FAF8' align='right' id='td_totalEurosFDC'>
						<p>0</p>
						</th>
					</tr>

					<tr>
						<th bgcolor='#D1FABB'  align='right'>
						<a title=\"Import Total\"><p style='font-size:11px;'>&nbsp;<b>Import Total</b></p></a>
						</th>

						<td bgcolor='#D1FABB'  align='right' ' >
						<p></p>
						</td>

						<th  bgcolor='#D1FABB' align='right' id='td_totalFacturaUnitats' >
						</th>

						<th  bgcolor='#D1FABB' align='right' id='td_totalFacturaEcos' >
						</th>

						
						<th  bgcolor='#D1FABB' align='right' id='td_totalFacturaEuros' >
						</th>

					</tr>

					<tr>
						<td  bgcolor='#D1FABB' align='right' id='td_pcMs'>
						<a title=\"% moneda social de la comanda, segons la moneda social dels productes demanats\"><p style='font-size:11px;'>&nbsp;</p></a>
						</td>

						<td  bgcolor='#D1FABB' align='right' >
						</td>
						
						<td  bgcolor='#D1FABB' align='right' style='width:10%;' >
						</td>
						
						<td bgcolor='#D1FABB' >
						</td>
						
						<td bgcolor='#D1FABB' >
						</td>
					</tr>
				</table>
	";
	return;
}
//------------------------------------------------------------------------------
function taulaEinesGrups()
{
	global 	$mAjuda,
			$mPars,
			$mRebost,
			$mProductors,
			$mCategories,
			$mSubCategories,
			$mEtiquetes,
			$mColors,
			$colorLlista;
			
			
	echo "
		<table   style='width:100%;' align='center'>
			<tr>
				<td align='center' valign='bottom' style='width:16.66%;'>
				<table>
					<tr>
						<td><p>cercador 'id':</b>".(html_ajuda1('html.php',34))."</p>
						<input id='i_cercarProducteId' type='text' size='6' value='".$mPars['crPrId']."'>
						<input type='button' onClick=\"javascript:if(checkCercarProdId()){enviarFpars('comandes.php?crPrId='+cercarProdId,'_self');}\" value='cercar'>
						</td>
					</tr>
				</table>
				</td>

				<td align='center' valign='bottom' style='width:16.66%;'>
				<table>
					<tr>
						<td><p>cercador text:</b>".(html_ajuda1('html.php',35))."</p>
						";
						if($mPars['crPrText']!='')
						{
							$bgcolor='orange';
						}
						else
						{
							$bgcolor='white';
						}
						echo "
						<input id='i_cercarProducteText' type='text' size='10' style='background-color:".$bgcolor."' value='".(urldecode($mPars['crPrText']))."'>
						<input type='button' onClick=\"javascript:if(checkCercarProdText()){enviarFpars('comandes.php?crPrText='+cercarProdText,'_self');}\" value='cercar'>
						</td>
					</tr>
				</table>
				</td>

				
				<td align='center' valign='bottom' style='width:16.66%;'>
				<table>
				<tr>
				<td>
				<select id='sel_vProductor' onChange=\"javascript: f_vistaProductor();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mProductor)=each($mProductors))
		{
			//v37- condicio
			if($mPars['selLlistaId']==0)
			{
				if($mPars['vProductor']==$mProductor['id']){$selected='selected';$selected2='';}else{$selected='';}
				if($mProductor['estat']=='1')
				{
					echo "
				<option ".$selected."  value='".$mProductor['id']."'>".(urldecode($mProductor['projecte']))."</option>
					";
				}
			}
			else
			{
				if(@substr_count($mRebost['productors_associats'],','.$mProductor['id'].',')>0)
				{
					if($mPars['vProductor']==$mProductor['id']){$selected='selected';$selected2='';}else{$selected='';}
					if($mProductor['estat']=='1')
					{
						echo "
				<option ".$selected."  value='".$mProductor['id']."'>".(urldecode($mProductor['projecte']))."</option>
						";
					}
				}
			}
		}
		echo "
				<option ".$selected2." value='TOTS'>- tots els productors -</option>
		";
		reset($mProductors);
		echo "
				</select> 
				</td>
				<td>
				".(html_ajuda1('html.php',1))."	
				</td>
				</tr>
				</table>
		";
/*		
		echo "
				<td align='center' valign='bottom' style='width:17%;'>
				<select id='sel_vCategoria' onChange=\"javascript: f_vistaCategoria();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$categoria)=each($mCategories))
		{
			if($mPars['vCategoria']==$categoria){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." value='".$categoria."'>".$categoria."</option>
			";
		}
		echo "
				<option ".$selected2." value='TOTS'>- totes les categories -</option>
		";
		reset($mCategories);
		echo "
				</select>
				".(html_ajuda1($mAjuda['html.php'][16]))."		
				</td>
		";
*/		
		echo "
				<td align='center' valign='bottom' style='width:16.66;'>
				<table>
				<tr>
				<td>
				<select id='sel_vSubCategoria' onChange=\"javascript: f_vistaSubCategoria();\">
		";
		$disabled='disabled';
		$selected='';
		$selected2='selected';
		while(list($key,$mSubCategoria)=each($mSubCategories))
		{
			if($mPars['vSubCategoria']==$mSubCategoria['categoria0'].'-'.$mSubCategoria['categoria10']){$selected='selected';$selected2='';}else{$selected='';}
			if($mSubCategoria['estoc_previst']>0){$disabled='';}else{$disabled='disabled';}
			echo "
				<option ".$selected." ".$disabled." value='".$mSubCategoria['categoria0'].'-'.$mSubCategoria['categoria10']."'>".$mSubCategoria['categoria0'].'-'.$mSubCategoria['categoria10']."</option>
			";
		}
		echo "
				<option ".$selected2." value='TOTS'>- totes les SUB-categories -</option>
		";
		reset($mSubCategories);
		echo "
				</select> 
				</td>
				<td>
				".(html_ajuda1('html.php',2))."		
				</td>
				</tr>
				</table>
				</td>

				<td align='left' valign='bottom' style='width:16.66;'>
				<table>
				<tr>
				<td>
				<select id='sel_etiqueta' onChange=\"javascript: f_vistaEtiqueta();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mEtiqueta)=each($mEtiquetes))
		{
			if($mPars['etiqueta']==$mEtiqueta['nom']){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option style='color:".$mEtiqueta['color'].";' ".$selected." value='".$mEtiqueta['nom']."'>".$mEtiqueta['nom']."</option>
				";
		}
		echo "
				<option ".$selected2." value='TOTS'>- cap etiqueta -</option>
		";
		reset($mEtiquetes);
		echo "
				</select> 
				</td>
				<td>
				".(html_ajuda1('html.php',3))."		
				</td>
				</tr>
				</table>
				</td>

				<td align='center' valign='bottom' style='width:16.66;'>
				<table>
				<tr>
				<td>
				<select id='sel_etiqueta2' onChange=\"javascript: f_vistaEtiqueta2();\">
		";
			$selected='';
			$selected2='selected';
		while(list($key,$mEtiqueta)=each($mEtiquetes))
		{
			if($mPars['etiqueta2']==$mEtiqueta['nom']){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." style='color:".$mEtiqueta['color'].";' value='".$mEtiqueta['nom']."'>".$mEtiqueta['nom']."</option>
			";
		}
		echo "
				<option ".$selected2." value='CAP'>- cap etiqueta -</option>
		";
		reset($mEtiquetes);
		echo "
				</select> 
				</td>
				<td>
				".(html_ajuda1('html.php',4))."		
				</td>
				</tr>
				</table>
				</td>
			</tr>
		</table>
	";
	return;
}

//------------------------------------------------------------------------------
function html_selectorUsuarisGrup()
{
	global $mPars,$mUsuarisGrupRef,$mUsuarisRef,$mAnticsUsuarisGrupAmbComanda,$mAjuda;

	
	$styleComanda='';
	
	echo "
<table align='left'>
	<tr>
		<td  valign='bottom'>
		<table><tr><td><p>Membres actuals:</p></p></td><td> ".(html_ajuda1('html.php',5))."</td></tr></table>
		<select onChange=\"javascript:enviarFpars('comandes.php?sUid='+this.value,'_self');\">
		";
		$selected0='';
		$selected1='selected';
		
		while(list($key,$mUsuari_)=each($mUsuarisGrupRef))
		{
			if($mUsuari_['usuari']['estat']=='actiu')
			{
				if($mUsuari_['usuari']['id']==$mPars['selUsuariId']){$selected0='selected';$selected1='';}
				
				if(array_key_exists($mPars['grup_id'],$mUsuari_['comanda']))
				{
					$styleComanda=" style='color:#007700;' ";
				}
				else
				{
					$styleComanda='';
				}
				echo "
		<option ".$selected0." ".$styleComanda." value='".$mUsuari_['usuari']['id']."'>".(urldecode($mUsuari_['usuari']['usuari']))." (".$mUsuari_['usuari']['email'].")</option>
				";
				$selected0='';
			}
		}
		reset($mUsuarisGrupRef);
		echo "
		<option ".$selected1." value='0'>- GRUP -</option>
		</select>
		</p>
		</td>
	</tr>
</table>

	";
	if(count($mAnticsUsuarisGrupAmbComanda)>0)
	{
		$styleComanda=" style='color:#007700;' ";
		echo "
<br>
<table align='left'>
	<tr>
		<td  valign='bottom'>
		<p>Membres anteriors amb comanda:
		<select onChange=\"javascript:val=this.value; if(val!=''){enviarFpars('comandes.php?sUid='+this.value,'_self');}\">
		";
		$selected0='';
		$selected1='selected';
		$estat='';
		while(list($key,$mUsuari_)=each($mAnticsUsuarisGrupAmbComanda))
		{
			if($mUsuari_['usuari']['estat']=='inactiu'){$estat='[inactiu]';}else{$estat='';}

			if($mUsuari_['usuari']['id']==$mPars['selUsuariId']){$selected0='selected';$selected1='';}
				
			echo "
		<option ".$selected0." ".$styleComanda." value='".$mUsuari_['usuari']['id']."'>".(urldecode($mUsuari_['usuari']['usuari']))." (".$mUsuari_['usuari']['email'].")".$estat."</option>
			";
			$selected0='';
		}
		reset($mAnticsUsuarisGrupAmbComanda);
		echo "
		<option ".$selected1." value='0'>- GRUP -</option>
		</select>
		</p>
		</td>
	</tr>
</table>
		";
		
	}
	return;
}
//------------------------------------------------------------------------------
function html_triaGrup()
{
	global $mGrupsUsuari,$mPars,$parsChain;
	
	
	echo "
		<form id='f_grup' name='f_grup' action='comandes.php' target='_blank' method='post'>
		<table align='center' width='100%'>
			<tr>
				<td align='left' valign='bottom' width='50%'>
				<p>Selecciona un dels teus grups:<br>
				<select name='sel_grupUsuari' onChange=\"javascript: document.getElementById('f_grup').submit();\">
				";
				$selected1='';
				$selected2='selected';
				$selected3='';
				while(list($grupId,$mGrupUsuari)=each($mGrupsUsuari))
				{
					if($mPars['grup_id']!=0)
					{
						if($grupId==$mPars['grup_id']){$selected1='selected'; $selected2=''; $selected3='';}else{$selected1='';}
					}
					
					echo "
				<option ".$selected1." value='".$grupId."'>".(urldecode($mGrupUsuari['nom']))."</option>
					";
				}
				reset($mGrupsUsuari);
				if($mPars['nivell']=='sadmin' OR $mPars['nivell']=='admin' OR $mPars['nivell']=='coord')
				{
					echo "
				<option ".$selected2." value='0'>CAC</option>
					";
				}
				echo "
				</select>				
				</p>
				</td>
			</tr>
		</table>
		<input type='hidden' name='i_pars' value='".$parsChain."'>
		</form>
	";

	return;
}


//----------------------------------------
function mostrarDadesRebost($db)
{
	global 	
	$periodeBases,
	$colorLlista,
	$mAbonamentsCarrecs,
	$mAjuda,
	$mCategoriesGrup,
	$mColors,
	$mComandaPerProductors,
	$mIncidencies,
	$mGrupsUsuari,
	$mGrup,
	$mPars,
	$mPeticionsGrup,
	$mRebosts,
	$mUsuarisRef,
	$mUsuarisGrupRef,
	$mParametres,
	$mPropietatsUsuari,
	$mPropietatsUsuaris,
	$mReservesComandes,
	$mPropietatsPeriodeLocal,
	$mRuta;
	
	
	echo "
				<table width=100%' bgcolor='".$mColors['table']."'>
					<tr>
						<td id='td_missatgeAlerta' style=' width:100%;' align='center'  valign='bottom'>
						</td>
					</tr>
					<tr>
						<td id='td_recordatoriGuardarComanda' align='center' width='100%' valign='bottom'>
						</td>
					</tr>
				</table>
				
		<table border='0' width='100%' bgcolor='".$mColors['table']."'>
			<tr>
				<td  valign='middle' align='left' width='5%'>
				<table align='left'>
					<tr>
						<td>
						<a title='Inici'><img src='imatges/casap.jpeg' ALT=\"p�gina d'usuari\" style='cursor:pointer' onClick=\"javascript: enviarFpars('index.php?us=".$mPars['usuari_id']."&pasw=".$mPars['pasw']."','_self')\"></a>
						</td>
						<td>
						<a title='Sortir'><img src='imatges/out2p.jpg' ALT=\"sortir\" style='cursor:pointer' onClick=\"javascript: document.getElementById('i_pars').value='';enviarFpars('index.php','_self')\"></a>
						</td>
					</tr>
				</table>
				</td>
				
				<td  valign='middle'  align='left' width='15%'>
				<p class='compacte' >
				Usuari:<b> ".(urldecode($mPars['usuari']))."</b> (".$mPars['nivell'].")
				</p>
		";
		
		if(isset($mPropietatsUsuari['comissio']) && $mPropietatsUsuari['comissio']!='')
		{
			echo "
				<p class='p_micro2' >Comissi�/oficina: ";
				$mComissionsUsuari=explode(',',$mPropietatsUsuari['comissio']);
				while(list($key,$val)=each($mComissionsUsuari))
				{
					echo "<br>".$mPropietatsUsuaris['comissio'][$val];
				}
				echo "</p>
			";
		}
		else
		{
			echo "
				<p class='compacte' >Comissi�/oficina: - CAP -</p>
			";
		}
		echo "
				</td>
				
				<td  align='middle' width='50px'>
	";
	$mostrar=0;
	
	$botoText="guardar pre-comanda";
	if
	(
		$mPars['grup_id']!=0
		&&
		$mPars['nivell']!='visitant'
		&& 
		(
			$mPars['nivell']=='admin' || $mPars['nivell']=='sadmin'
			|| 
			(
				!$mParametres['precomandaTancada']['valor']
				&&
				(
					(
						$mPars['selUsuariId']!='0' 
						&&
						$mPars['selUsuariId']==$mPars['usuari_id']
					)
					||
					(
						$mPars['selUsuariId']!='0' 
						&&
						$mPars['usuari_id']==@$mGrup['usuari_id']
					)
					||
					(
						$mPars['selUsuariId']=='0'
						&&
						$mPars['usuari_id']==@$mGrup['usuari_id']
					)
				)
			)
		)
	)
	{
		if($mPars['selUsuariId']==0)
		{	
			$botoText='guardar forma pagament';
		}
		if(($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin') && $mParametres['precomandaTancada']['valor'])
		{
			echo "<font style='color:#FC7202; font-size:12px;'>[".$mPars['nivell']."]</font><br>";
		}
		echo "	
				<a title='".$botoText."'><img src='imatges/guardarpp.gif' style='cursor:pointer;' onClick=\"guardarComanda();\" value='".$botoText."'></a>
		";
	}
	echo "
				</td>
	";
	
	if(@$mPars['grup_id']!=0 &&	(($mPars['selUsuariId']==0 && (str_replace(',','',$mGrup['productors_associats'])!='')) || selUsuariEsProductorAssociatDelGrup($db)))
	{
		echo "
				<td valign='bottom'  width='50px'>
				<a title=\"productes dels productors associats al grup que es troben a l'inventari de la CAC\">
				<img src='imatges/productesp.gif' style='cursor:pointer;' onClick=\"enviarFpars('inventariPerProductors.php?mRef=".$mGrup['id'].'-'.$mGrup['productors_associats']."&psw=".$mPars['pasw']."','_blank');\">
				</a>
				</td>
		";
	}
//*v36-3-12-15-condicional
	if(
		count($mRuta)==1
		&&
		(
			$mPars['grup_id']=='0' 
			|| 
			(
				substr_count($mGrup['categoria'],'magatzem')>0 
				&& 
				$mPars['selUsuariId']==0
			)
		)
	)
	{
		echo "
				<td  width='5px'>
				<p>&nbsp;</p>
				</td>
								
				<td valign='bottom' width='50px'>
				<a title='gesti� inventaris magatzem CAC local'>
				<img src='imatges/magatzemp.gif' style='cursor:pointer;' onClick=\"javascrit:enviarFpars('gestioMagatzems.php?mRef=".$mPars['grup_id']."','_blank');\">
				</a>
				</td>
								
				<td>
				<p>&nbsp;</p>
				</td>
								
		";
	}

	echo "
	";
	if($mPars['nivell']!='visitant')
	{
		echo "
				<td valign='bottom' align='right' width='60%'>
				<table width='100%'>
					<tr>
						<td align='left'>
		";
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord' || $mPars['usuari_id']==$mGrup['usuari_id'])
	{
		echo "
		";
		if($mPars['grup_id']!='0')
		{
			html_selectorUsuarisGrup();
		}
	}
	echo "
						<td align='left'>
		";
		mostrarSelectorRuta(1,'comandes.php');
		if
		(
			$mPars['grup_id']!=0
			&&
			(
				count($mRuta)==1 //no es llista especial
				||
				$mRuta[0]=='grups'	//es llista local
			)
		)
		{
			echo "
						
				";
				if($mParametres['precomandaTancada']['valor']==1)
				{
					$imatgeSemafor='semafor_vermell.gif';
					$semaforText="Aquest periode de reserves a la llista de CAC est� TANCAT";
				}
				else
				{
					$imatgeSemafor='semafor_verd.gif';
					$semaforText="Aquest periode de reserves a la llista de la CAC est� OBERT";
				}
						
				
			echo "
						</td>
						<td align='left'>
			";
			mostrarSelectorLlista('comandes.php');
			echo "
						</td>
			";
		}
		echo "
					</tr>
				</table>
				</td>
		";
	}
	echo "
	
			</tr>
		</table>

		
		<table    bgcolor='".$mColors['comandes.php'][$colorLlista]['t2_bg']."' bordercolor='#A2CBCA' border='0' width='100%' align='center'>
			<tr>
				<td  width='100%' valign='middle' align='left'>
				<table border='0' align='left' valign='top' style='width:100%;'>
					<tr>
						<td width='25%'  align='left' valign='top'>
						<p class='compacte'  style='font-size:16px;'>
						<b>GRUP: &nbsp;".(urldecode($mGrup['nom']))." </b></p>
						<table width='100%'>
							<tr>
								<td>
								<a title='membres del grup'><img src='imatges/grup2p.gif'></a>
								</td>
								<td>
	";
	if(isset($mPars['grup_id']) && $mPars['grup_id']!='0')
	{
		if(isset($mUsuarisGrupRef[$mGrup['usuari_id']]))
		{
			$mRespGrup=$mUsuarisGrupRef[$mGrup['usuari_id']]['usuari'];
		}
		else
		{
			$mRespGrup=$mUsuarisRef[$mGrup['usuari_id']];
		}
			echo "
								<p class='nota'>
								Responsable:&nbsp;".(strtoupper(urldecode($mRespGrup['usuari'])))."
								<br>(".@$mRespGrup['email'].") 
								</p>
			";
	}
	echo "
								</td>
							</tr>
						</table>
						</td>

						<td width='25%' valign='top'>
	";
	if($mPars['grup_id']==0)
	{
		echo "			
						<p class='compacte' style='font-size:12px;'>
						<input type='checkbox' 	";

		if($mPars['veureProductesDisponibles']==1)
		{
				echo " CHECKED ";
		} 
		echo "
									id='ck_veureProductesDisponibles' onClick=\"f_veureProductesDisponibles();\" value='".$mPars['veureProductesDisponibles']."'> 
						Veure nom�s productes actius&nbsp;".(html_ajuda1('html.php',21))."
						</p>
		";
	}
	else if($mPars['selUsuariId']==0)
	{
		echo "
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('totals');\" style='cursor:pointer;'><u>Vista Albar� (totals)</u></p>
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('desglossat');\" style='cursor:pointer;'><u>Vista Albar� (desglossat)</u>&nbsp;".(html_ajuda1('html.php',9))."</p>
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('resumUsuaris');\" style='cursor:pointer;'><u>Resum Imports Usuaries</u>&nbsp;".(html_ajuda1('html.php',10))."</p>
		";
		if(substr_count($mGrup['categoria'],'rebost')>0 && $mPars['selRutaSufix']*1<=1503)
		{
			echo "
						<p class='compacte' style='color:#000000; cursor:pointer;' onClick=\"enviarFpars('tramsRuta.php','_blank');\"><u>Full de ruta CAC</u></p>
			";
		}
	}
	else
	{
		echo "
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('');\" style='cursor:pointer;'><u>Albar� (usuari)</u>&nbsp;".(html_ajuda1('html.php',11))."</p>
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('desglossat');\" style='cursor:pointer;'><u>Albar� (grup)</u>&nbsp;".(html_ajuda1('html.php',12))."</p>
		";
	}

	echo "
						<p class='compacte' onClick=\"enviarFpars('db_llistaProductesCSV.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>vista productes segons filtre actual (CSV)</u>&nbsp;".(html_ajuda1('html.php',13))."</p>
			";

	$getP=selUsuariEsProductorAssociatDelGrup($db);
	if 
	(
		@$mPars['grup_id']==0 
		|| 
		@$mPars['grup_id']!=0 && 
		(
			(
				$mPars['selUsuariId']==0 
				&& 
				(
					substr_count($mGrup['categoria'],',1-rebost')>0 || substr_count($mGrup['categoria'],',1-productorBasic')>0
				)
			)
			|| 
			(
				$mPars['selUsuariId']!=0 && $getP!=false
			)
		)
	)
	{
		if($getP!=false)
		{
			echo "
						<p class='compacte' onClick=\"enviarFpars('distribucioGrups.php?getP=".$getP."','_blank');\" style='color:#000000;  cursor:pointer;'><u><b>Qu� t'estan demanant els altres GRUPS</b></u></p>
			";
		}
		else
		{
			echo "
						<p class='compacte' onClick=\"enviarFpars('distribucioGrups.php?getP=".$getP."','_blank');\" style='color:#000000;  cursor:pointer;'><u>Qu� estan demanant els altres GRUPS</u></p>
			";
		}
	}
	echo "
						</td>
						
						<td width='25%' align='left'  valign='top'>
	";
	if
	(
		(
			$mPars['nivell']=='sadmin' 
			|| 
			$mPars['nivell']=='admin' 
			|| 
			$mPars['nivell']=='coord' 
			|| 
			$mPars['usuari_id']==$mGrup['usuari_id']
		)
		&&
		$mPars['selUsuariId']==0
	)
	{
		if($mPars['grup_id']==0){$text1='Missatges a usuaries';}else{$text1='Missatges a membres del grup';}
		echo "
						<p class='compacte' onClick=\"javascript:enviarFpars('mail.php','_blank');\"   style='cursor:pointer;'>
						<u>".$text1."</u>&nbsp;".(html_ajuda1('html.php',7))."</p>
		";
		if(substr_count($mGrup['categoria'],'magatzem')>0 || substr_count($mGrup['categoria'],'rebost')>0)
		{
			if(isset($mReservesComandes[$mPars['grup_id']]))
			{
				$recepcionsText="<font style='color:#ff0000;'><b>".(count($mReservesComandes[$mPars['grup_id']]))."</b></font>";
			}
			else 
			{	
				$recepcionsText="<font style='color:#000000;'><b>0</b></font>";
			}
			echo "
						<p class='compacte'  style='cursor:pointer;' onClick=\"enviarFpars('vistaReservesSZ.php','_blank');\" >
						<u>Recepcions d'altres comandes</u> [".$recepcionsText."]&nbsp;".(html_ajuda1('html.php',8))."
						</p>
			";
		}
		if(count($mPeticionsGrup)>0){ $peticionsText="<font style='color:#ff0000;'><b>".(count($mPeticionsGrup))."</b></font>";}
		else {	$peticionsText="<font style='color:#000000;'><b>".(count($mPeticionsGrup))."</b></font>";}
		if(count($mIncidencies)>0){	$incidenciesText="<font style='color:#ff0000;'><b>".(count($mIncidencies))."</b></font>";}
		else {	$incidenciesText="<font style='color:#000000;'><b>".(count($mIncidencies))."</b></font>";}
		if(count($mAbonamentsCarrecs)>0){	$incidenciesAbcaText="<font style='color:#ff0000;'><b>".(count($mAbonamentsCarrecs))."</b></font>";}
		else {	$incidenciesAbcaText="<font style='color:#000000;'><b>".(count($mAbonamentsCarrecs))."</b></font>";}

		//v37-condicio
		if(count($mRuta)==1 || $mPars['grup_id']==0) //no es ruta especial
		{
			echo "
						<p class='compacte' onClick=\"javascript: enviarFpars('gestioGrup.php?gRef=".$mPars['grup_id']."','_blank');\" style='color:#000000;  cursor:pointer;'><u>Peticions d'usuaries</u>&nbsp;[".$peticionsText."]
			";
			if($mPars['selUsuariId']==0){echo "&nbsp;".(html_ajuda1('html.php',6));}
		}
						
						
						
						echo "</p>
						<p class='compacte' onClick=\"javascript: enviarFpars('incidencies.php?opt2=incd&gRef=".$mPars['grup_id']."&vLlista=0','_blank');\" style='color:#000000;  cursor:pointer;'><u>Incid�ncies</u> [".$incidenciesText."]</p>
						<p class='compacte' onClick=\"javascript: enviarFpars('incidencies.php?opt2=abCa&gRef=".$mPars['grup_id']."&vLlista=0','_blank');\" style='color:#000000;  cursor:pointer;'><u>Abonaments/c�rrecs</u> [".$incidenciesAbcaText."]</p>
		";
		
	}
	else if($mPars['grup_id']!='0' && $mPars['selUsuariId']!='0')
	{
		if(count($mPeticionsGrup)>0){ $peticionsText="<font style='color:#ff0000;'><b>".(count($mPeticionsGrup))."</b></font>";}
		else {	$peticionsText="<font style='color:#000000;'><b>".(count($mPeticionsGrup))."</b></font>";}
		if(count($mIncidencies)>0){	$incidenciesText="<font style='color:#ff0000;'><b>".(count($mIncidencies))."</b></font>";}
		else {	$incidenciesText="<font style='color:#000000;'><b>".(count($mIncidencies))."</b></font>";}
		if(count($mAbonamentsCarrecs)>0){	$incidenciesAbcaText="<font style='color:#ff0000;'><b>".(count($mAbonamentsCarrecs))."</b></font>";}
		else {	$incidenciesAbcaText="<font style='color:#000000;'><b>".(count($mAbonamentsCarrecs))."</b></font>";}
		echo "
						<p class='compacte' onClick=\"javascript: enviarFpars('incidencies.php?opt2=incd&gRef=".$mPars['grup_id']."&vLlista=0','_blank');\" style='color:#000000;  cursor:pointer;'><u>incid�ncies</u> [".$incidenciesText."]</p>
						<p class='compacte' onClick=\"javascript: enviarFpars('incidencies.php?opt2=abCa&gRef=".$mPars['grup_id']."&vLlista=0','_blank');\" style='color:#000000;  cursor:pointer;'><u>abonaments/c�rrecs</u> [".$incidenciesAbcaText."]</p>
		";
	}
		
	echo "
						</td>

						<td width='25%' valign='top'>
						";
						//<p class='compacte' onClick=\"javascript: enviarFpars('llistat_grups.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>Llistat de grups i equip cac</u></p>
						//<p class='compacte' onClick=\"javascript: enviarFpars('informacio_gestor.php','_blank');\" style='color:#000000;  cursor:pointer;'><u>Info general sobre el gestor</u></p>
						echo "
						</td>
					</tr>
				</table>
				</td>
			</tr>

			<tr>
				<td width='100%'>
				<table   width='100%'>
					<tr>
						<td align='left' valign='bottom' width='20%'>
						</td>
						<td align='left' valign='bottom' width='70%'>
						<table style='width:100%;'>
							<tr>
								<td align='left' width='100%' valign='bottom'>
	";
	if($mPars['grup_id']!=0)
	{
		echo "
								<p style='font-size:11px;'>* A l'import total s'inclouen tots els productes de la comanda del GRUP o de l'usuari, independentment del filtre aplicat i de la p�gina mostrada.</p>
								<p style='font-size:11px;'>* Les reserves de productes especials es mostren pero no es comptabilitzen</p>
		";
	}
	else
	{
		echo "
								<p style='font-size:11px;'>* A l'import total s'inclouen tots els productes de la comanda CAC seleccionats segons els filtres, independentment de la p�gina mostrada.</p>
								<p style='font-size:11px;'>* Es ressalten en verd tots els productes oferits per la CAC, excepte els productes especials</p>
		";
	}
	echo "
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	";

	return;
}

//----------------------------------------
function mostrarDadesRebostGrup($db)
{
	global 	
	$hiHaUnAltrePeriodeNoTancat,
	$hiHaAlgunPeriodeNoTancat,
	$mIncidencies,
	$mAbonamentsCarrecs,
	$mAjuda,
	$mDatesReservesLocals,
	$mPeticionsGrup,
	$mGrupsUsuari,
	$mGrup,
	$mGrupsRef,
	$mRebosts,
	$mPars,
	$mCategoriesGrup,
	$mColors,
	$colorLlista, 
	$mComandaPerProductors,
	$mUsuarisRef,
	$mUsuarisGrupRef,
	$mParametres,
	$mReservesComandes,
	$periodeBases,
	$mPropietatsUsuari,
	$mPropietatsUsuaris,
	$mPropietatsGrupConfig,
	$mPropietatsGrup,
	$mPropietatsPeriodeLocalConfig,
	$mPropietatsPeriodeLocal,
	$mPropietatsPeriodesLocals,
	$mGrupsEsResponsable;
	
	
	echo "
				<table width=100%' bgcolor='".$mColors['table']."'>
					<tr>
						<td id='td_missatgeAlerta' style=' width:100%;' align='center'  valign='bottom'>
						</td>
					</tr>
					<tr>
						<td id='td_recordatoriGuardarComanda' align='center' width='100%' valign='bottom'>
						</td>
					</tr>
				</table>
				
		<table border='0' width='100%' bgcolor='".$mColors['table']."'>
			<tr>
				<td  valign='middle' align='left' width='5%'>
				<table align='left'>
					<tr>
						<td>
						<a title='Inici'><img src='imatges/casap.jpeg' ALT=\"p�gina d'usuari\" style='cursor:pointer' onClick=\"javascript: enviarFpars('index.php?us=".$mPars['usuari_id']."&pasw=".$mPars['pasw']."','_self')\"></a>
						</td>
						<td>
						<a title='Sortir'><img src='imatges/out2p.jpg' ALT=\"sortir\" style='cursor:pointer' onClick=\"javascript: document.getElementById('i_pars').value='';enviarFpars('index.php','_self')\"></a>
						</td>
					</tr>
				</table>
				</td>
				
				<td  valign='middle'  align='left' width='15%'>
				<p class='compacte' >
				Usuari:<b> ".(urldecode($mPars['usuari']))."</b> (".$mPars['nivell'].")
				</p>
				</td>
				
				<td width='10%' align='middle'>
	";
	$mostrar=0;
	
	echo "
				</td>
				
				
				<td width='70%' valign='bottom'>
				<table  align='right'>
					<tr>
						<td >
		";
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord' || $mPars['usuari_id']==$mGrup['usuari_id'])
	{
		echo "
		";
		if($mPars['grup_id']!='0')
		{
			html_selectorUsuarisGrup();
		}
	}
		echo "
						</td>
						<td>
		";
	if(isset($mPropietatsPeriodesLocals))
	{
		mostrarSelectorPeriodeLocal(1,'comandes.php');
	}
				
		echo "			</td>
		";
		if
		(
			$mPars['usuari_id']!='visitant'
		)
		{
			echo "
						<td>
						
			";						
					if(isset($mPropietatsPeriodeLocal['comandesLocalsTancades']) && $mPropietatsPeriodeLocal['comandesLocalsTancades']==1)
					{
						$imatgeSemafor='semafor_vermell.gif';
						$semaforText="Reserves a la llista LOCAL est� TANCAT";
					}
					else if(isset($mPropietatsPeriodeLocal['comandesLocalsTancades']) && $mPropietatsPeriodeLocal['comandesLocalsTancades']==-1)
					{
						$imatgeSemafor='semafor_verd.gif';
						$semaforText="Reserves a la llista LOCAL est� OBERT";
					}
					else if(isset($mPropietatsPeriodeLocal['comandesLocalsTancades']) && $mPropietatsPeriodeLocal['comandesLocalsTancades']==0)
					{
						$imatgeSemafor='semafor_vermell.gif';
						$semaforText="Reserves a la llista LOCAL est� TANCAT";
					}
					else
					{
						$imatgeSemafor='semafor_vermell.gif';
						$semaforText="No s'ha creat cap periode";
					}
				
			echo "
			<a title=\"".$semaforText."\"><img src='imatges/".$imatgeSemafor."'></a>
						</td>
						
						<td align='left' width='50px' >
						<p>&nbsp;&nbsp;</p>
						</td>
						
						<td align='left'>
			";
			mostrarSelectorLlista('comandes.php');
			echo "
						</td>
					</tr>
			";
		}
		echo "
				</table>
				</td>
			</tr>
		</table>

		
		<table   bgcolor='".$mColors['comandes.php'][$colorLlista]['t2_bg']."' bordercolor='#A2CBCA' border='0' width='100%' align='center'>
			<tr>
				<td  width='100%' valign='middle' align='left'>
				<table border='0' align='left' valign='top' style='width:100%;'>
					<tr>
						<td width='20%'  align='left' valign='top'>
						<p class='compacte'  style='font-size:16px;'>
						<b>GRUP:&nbsp;".(urldecode($mGrup['nom']))." </b></p>
						<table width='100%'>
							<tr>
								<td>
								<a title='membres del grup'><img src='imatges/grup2p.gif'></a>
								</td>
								<td>
	";
	if(isset($mPars['grup_id']) && $mPars['grup_id']!='0')
	{
		echo "
								<p class='nota'>
								Responsable:&nbsp;".(strtoupper(urldecode($mUsuarisGrupRef[$mGrup['usuari_id']]['usuari']['usuari'])))."
								<br>(".@$mUsuarisGrupRef[$mGrup['usuari_id']]['usuari']['email'].") 
								</p>
		";
	}
	echo "
								</td>
							</tr>
						</table>
						</td>

						<td width='20%' valign='top'>
	";
	if($mPars['grup_id']==0)
	{
		echo "			
						<p class='compacte' style='font-size:12px;'>
						<input type='checkbox' 	";

		if($mPars['veureProductesDisponibles']==1)
		{
				echo " CHECKED ";
		} 
		echo "
									id='ck_veureProductesDisponibles' onClick=\"f_veureProductesDisponibles();\" value='".$mPars['veureProductesDisponibles']."'> 
						Veure nom�s productes actius&nbsp;".(html_ajuda1('html.php',21))."
						</p>
						<p id='p_vistaAlbara' onClick=\"vistaAlbara('totals');\" style='cursor:pointer;'><u>Veure Vista Albar�</u></p>
		";
	}
	else if($mPars['selUsuariId']==0)
	{
		if($mPars['selLlistaId']!=0) //local
		{
			echo "			
						<p class='compacte' style='font-size:12px;'>
						<input type='checkbox' 	
			";

			if($mPars['veureProductesDisponibles']==1)
			{
				echo " CHECKED ";
			} 
			echo "
									id='ck_veureProductesDisponibles' onClick=\"f_veureProductesDisponibles();\" value='".$mPars['veureProductesDisponibles']."'> 
						Veure nom�s productes actius&nbsp;".(html_ajuda1('html.php',21))."
						</p>
			";
		}
		echo "
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('totals');\" style='cursor:pointer;'><u>Vista Albar� (totals)</u></p>
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('desglossat');\" style='cursor:pointer;'><u>Vista Albar� (desglossat)</u>&nbsp;".(html_ajuda1('html.php',9))."</p>
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('resumUsuaris');\" style='cursor:pointer;'><u>Resum Imports Usuaries</u>&nbsp;".(html_ajuda1('html.php',10))."</p>
						<p class='compacte' id='p_vistaAlbara' onClick=\"enviarFpars('distribucioGrupsLocals.php?getP=TOTS','_blank');\" style='cursor:pointer;'><u>Resum reserves locals per productora</u>&nbsp".(html_ajuda1('html.php',22))."</p>
						<p class='compacte' id='p_vistaAlbara' onClick=\"enviarFpars('comptesGrupLocal.php?llId=".$mPars['selLlistaId']."','_blank');\" style='cursor:pointer;'><u>Resum balan�os d'intercanvi</u>&nbsp".(html_ajuda1('html.php',23))."</p>
			";
		if(substr_count($mGrup['categoria'],'rebost')>0 && $mPars['selRutaSufix']*1<=1503)
		{
			echo "
						<p class='compacte' style='color:#000000; cursor:pointer;' onClick=\"enviarFpars('tramsRuta.php','_blank');\"><u>Full de ruta CAC</u></p>
			";
		}
	}
	else
	{
		echo "
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('');\" style='cursor:pointer;'><u>Albar� (usuari)</u>".(html_ajuda1('html.php',11))."</p>
						<p class='compacte' id='p_vistaAlbara' onClick=\"vistaAlbara('desglossat');\" style='cursor:pointer;'><u>Albar� (grup)</u>".(html_ajuda1('html.php',12))."</p>
			";
	}

		echo "
						<p class='compacte' onClick=\"enviarFpars('db_llistaProductesCSV.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>vista productes segons filtre actual (CSV)</u>&nbsp;".(html_ajuda1('html.php',13))."</p>
			";
		/*
		echo "
						<p class='compacte' onClick=\"enviarFpars('db_resumPeriodeLocalCSV.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>resum actual periode local seleccionat (CSV)</u></p>
			";
		*/
		echo "
						<br>
			";

	$getP=selUsuariEsProductorAssociatDelGrup($db);

	if
	(
		(
			$mPars['nivell']=='sadmin' 
			|| 
			$mPars['nivell']=='admin' 
			|| 
			$mPars['nivell']=='coord' 
			|| 
			$mPars['usuari_id']==$mGrup['usuari_id']
		)
		&&
		$mPars['selUsuariId']==0
	)
	{
		echo "
						<p class='compacte' onClick=\"javascript:enviarFpars('mail.php','_blank');\"   style='cursor:pointer;'>
						<u>Missatges a membres del grup</u>&nbsp;".(html_ajuda1('html.php',7))."
						</p>
		";
		if(count($mPeticionsGrup)>0){ $peticionsText="<font style='color:#ff0000;'><b>".(count($mPeticionsGrup))."</b></font>";}
		else {	$peticionsText="<font style='color:#000000;'><b>".(count($mPeticionsGrup))."</b></font>";}
		if(count($mIncidencies)>0){	$incidenciesText="<font style='color:#ff0000;'><b>".(count($mIncidencies))."</b></font>";}
		else {	$incidenciesText="<font style='color:#000000;'><b>".(count($mIncidencies))."</b></font>";}
		if(count($mAbonamentsCarrecs)>0){	$incidenciesAbcaText="<font style='color:#ff0000;'><b>".(count($mAbonamentsCarrecs))."</b></font>";}
		else {	$incidenciesAbcaText="<font style='color:#000000;'><b>".(count($mAbonamentsCarrecs))."</b></font>";}

		echo "
						<p class='compacte' onClick=\"javascript: enviarFpars('gestioGrup.php?gRef=".$mPars['grup_id']."','_blank');\" style='color:#000000;  cursor:pointer;'><u>Peticions d'usuaries</u> [".$peticionsText."]&nbsp;".(html_ajuda1('html.php',6))."
						</p>
						<p class='compacte' onClick=\"javascript: enviarFpars('incidencies.php?opt2=incd&gRef=".$mPars['grup_id']."&vLlista=".$mPars['selLlistaId']."','_blank');\" style='color:#000000;  cursor:pointer;'><u>Incid�ncies (llista productes local)</u> [".$incidenciesText."]</p>
						<p class='compacte' onClick=\"javascript: enviarFpars('incidencies.php?opt2=abCa&gRef=".$mPars['grup_id']."&vLlista=".$mPars['selLlistaId']."','_blank');\" style='color:#000000;  cursor:pointer;'><u>Abonaments/c�rrecs (llista productes local)</u> [".$incidenciesAbcaText."]</p>
		";    					
	}
	else if($mPars['grup_id']!='0' && $mPars['selUsuariId']!='0')
	{
		if(count($mPeticionsGrup)>0){ $peticionsText="<font style='color:#ff0000;'><b>".(count($mPeticionsGrup))."</b></font>";}
		else {	$peticionsText="<font style='color:#000000;'><b>".(count($mPeticionsGrup))."</b></font>";}
		if(count($mIncidencies)>0){	$incidenciesText="<font style='color:#ff0000;'><b>".(count($mIncidencies))."</b></font>";}
		else {	$incidenciesText="<font style='color:#000000;'><b>".(count($mIncidencies))."</b></font>";}
		if(count($mAbonamentsCarrecs)>0){	$incidenciesAbcaText="<font style='color:#ff0000;'><b>".(count($mAbonamentsCarrecs))."</b></font>";}
		else {	$incidenciesAbcaText="<font style='color:#000000;'><b>".(count($mAbonamentsCarrecs))."</b></font>";}
		echo "
						<p class='compacte' onClick=\"javascript: enviarFpars('incidencies.php?opt2=incd&gRef=".$mPars['grup_id']."&vLlista=".$mPars['selLlistaId']."','_blank');\" style='color:#000000;  cursor:pointer;'><u>incid�ncies (llista productes local)</u> [".$incidenciesText."]</p>
						<p class='compacte' onClick=\"javascript: enviarFpars('incidencies.php?opt2=abCa&gRef=".$mPars['grup_id']."&vLlista=".$mPars['selLlistaId']."','_blank');\" style='color:#000000;  cursor:pointer;'><u>abonaments/c�rrecs (llista productes local)</u> [".$incidenciesAbcaText."]</p>
		";
	}
		
	echo "
						</td>

	";						

	if
	(
		(
			$mPars['nivell']=='sadmin' 
			|| 
			$mPars['nivell']=='admin' 
			|| 
			$mPars['nivell']=='coord' 
			|| 
			$mPars['usuari_id']==$mGrup['usuari_id']
		)
		&&
		$mPars['selUsuariId']==0
		&& 
		$mPars['selLlistaId']!=0
	)
	{
		echo "
						<td width='20%' valign='top'>
						<table align='left'>
		";
		if
		(
			isset($mPropietatsPeriodeLocal['comandesLocalsTancades']) && $mPropietatsPeriodeLocal['comandesLocalsTancades']==0
			||
			isset($mPropietatsPeriodeLocal['comandesLocalsTancades']) && $mPropietatsPeriodeLocal['comandesLocalsTancades']==-1
		)
		{
			echo "
							<tr>
								<td  valign='top'>
								<p  class='p_micro5' >Comandes Locals Tancades:</p>
								</td>
								<td  valign='top'>
								<p class='p_micro5' ><input type='checkbox' 	
			";

			if
			(
				$mPropietatsPeriodeLocal['comandesLocalsTancades']==0
				||
				$mPropietatsPeriodeLocal['comandesLocalsTancades']==1
			)
			{
				echo " CHECKED ";
			} 
			echo "
									id='ck_comandesLocalsTancades' onClick=\"f_comandesLocalsTancades();\" value='".$mPropietatsPeriodeLocal['comandesLocalsTancades']."'>
									&nbsp;".(html_ajuda1('html.php',14))."
								</p>
								</td>
							</tr>
			";
		
			if
			(
				$mPropietatsPeriodeLocal['comandesLocalsTancades']==0
			)
			{
				echo "
							<tr>
								<td  valign='top'>
								<input  class='i_micro' id='i_tancarPeriode' DISABLED  type='button' onClick=\"tancarPeriodeLocal();\"  value='tancar periode ".$mPars['sel_periode_comanda_local']."'>
								</td>
								<td  valign='top'>
								<p class='p_micro5' ><input type='checkbox' id='ck_activarTancarPeriode' onClick=\"javascript: activarTancarPeriode();\" value='0'>
								&nbsp;".(html_ajuda1('html.php',15))."
								</p>
								</td>
							</tr>
				";
			}
		}

		if
		(
			isset($mPropietatsPeriodeLocal['comandesLocalsTancades'])
			&&
			@$mPropietatsPeriodeLocal['comandesLocalsTancades']!=1
		)
		{
			echo "
							<tr>
								<td  valign='top'>
								<p class='p_micro5'>Cost Transport Local:&nbsp;".(html_ajuda1('html.php',16))."</p>
								</td>
								<td  valign='top'>
								<p  class='p_micro5'><input class='i_micro' size='4' type='text' id='i_local_ctik'  value='".$mPropietatsPeriodeLocal['ctikLocal']."'> 
								<input  class='i_micro'  type='button' onClick=\"f_guardarPropietatPeriodeLocal('ctikLocal','i_local_ctik');\"  value='guardar'>
								<br>(ums/kg)</p>
								</td>
							</tr>
							<tr>
								<td  valign='top'>
								<p class='p_micro5'>% ms cost Transport Local:&nbsp;".(html_ajuda1('html.php',17))."</p>
								</td>
								<td  valign='top'>
								<p  class='p_micro5'><input class='i_micro' size='4' type='text' id='i_ms_local_ctik'  value='".$mPropietatsPeriodeLocal['ms_ctikLocal']."'> 
								<input  class='i_micro'  type='button' onClick=\"f_guardarPropietatPeriodeLocal('ms_ctikLocal','i_ms_local_ctik');\"  value='guardar'>
								</p>
								</td>
							</tr>
							<tr>
								<td valign='top'>
								<p  class='p_micro5' >Fons Despeses Locals:&nbsp;".(html_ajuda1('html.php',18))."</p>
								</td>
								<td  valign='top'>
								<p  class='p_micro5'><input  class='i_micro' size='4' type='text' id='i_local_fd' value='".$mPropietatsPeriodeLocal['fdLocal']."'> 
								<input  class='i_micro'  type='button' onClick=\"f_guardarPropietatPeriodeLocal('fdLocal','i_local_fd');\" value='guardar'>
								<br> % sobre preu productes</p>
								</td>
							</tr>
							<tr>
								<td valign='top'>
								<p  class='p_micro5' >% MS del Fons Despeses Locals:&nbsp;".(html_ajuda1('html.php',19))."</p>
								</td>
								<td  valign='top'>
								<p  class='p_micro5'><input  class='i_micro' size='4' type='text' id='i_local_ms_fd' value='".$mPropietatsPeriodeLocal['ms_fdLocal']."'> 
								<input  class='i_micro'  type='button' onClick=\"f_guardarPropietatPeriodeLocal('ms_fdLocal','i_local_ms_fd');\" value='guardar'>
								<br> % sobre preu productes</p>
								</td>
							</tr>
						</table>
			";
			//gestio de productors locals associats al grup, per part del responsable del grup
			echo "
						<table align='center' width='100%'>
							<tr>
								<td valign='top'>
								<p  class='p_micro5' >Adre�a del Punt d'Entrega:</p>
								<p  class='p_micro5'><input  class='i_micro' size='40' type='text' id='i_local_ape' value='".(urldecode($mPropietatsPeriodeLocal['apeLocal']))."'> 
								<input  class='i_micro'  type='button' onClick=\"f_guardarPropietatPeriodeLocal('apeLocal','i_local_ape');\" value='guardar'>
								</p>
								</td>
							</tr>
						</table>
						<table align='center' width='100%'>
							<tr>
								<td valign='top'>
								<p  class='p_micro5' >Compte grup (euros):</p>
								<p  class='p_micro5'><input  class='i_micro' size='40' type='text' id='i_compte_grup_euros' value='".(urldecode($mPropietatsPeriodeLocal['compte_grup_euros']))."'> 
								<input  class='i_micro'  type='button' onClick=\"f_guardarPropietatPeriodeLocal('compte_grup_euros','i_compte_grup_euros');\" value='guardar'>
								</p>
								</td>
							</tr>
						</table>
						";
			}
			else
			{
				echo "
						<input  class='i_micro' type='hidden' id='i_compte_grup_euros' value=''> 
				";
			}
			
						$disabled='';
						if($hiHaAlgunPeriodeNoTancat==1)
						{
							$disabled='DISABLED';
						}
						else
						{
							$disabled='';
						}
			
						echo "
						<table align='center' width='100%'>
							<tr>
								<td valign='top'>
								<p><b>Nou Periode de reserves:</b><br>
								<table>
									<tr>
										<td>
										</td>
										<td>
										<p>dia:</p>
										</td>
										<td>
										<p>mes:</p>
										</td>
										<td>
										<p>any:</p>
										</td>
									</tr>								
									<tr>
										<td>
										<p>inici</p>
										</td>
										<td>
										<select id='sel_i_dia' ".$disabled.">
										";
										$selected='';
										for($i=1;$i<=31;$i++)
										{
											if($i*1==$mDatesReservesLocals['idia']*1)	{$selected='selected';} else {$selected='';}
											echo "
											<option ".$selected." value='".$i."'>".$i."</option>
											";
										}
										echo "
										</select>
										</td>
										<td>
										";
										$selected='';
										echo "
										<select id='sel_i_mes' ".$disabled.">
										";
										for($i=1;$i<=12;$i++)
										{
											if($i*1==$mDatesReservesLocals['imes']*1)	{$selected='selected';} else {$selected='';}
											echo "
											<option ".$selected." value='".$i."'>".$i."</option>
											";
										}
										echo "
										</select>
										</td>
										<td>
										";
										$selected='';
										echo "
										<select id='sel_i_any' ".$disabled.">
										";
										for($i=15;$i<=50;$i++)
										{
											if($i*1==$mDatesReservesLocals['iany']*1)	{$selected='selected';} else {$selected='';}
											echo "
											<option ".$selected." value='".$i."'>".$i."</option>
											";
										}
										echo "
										</select>
										</td>
									</tr>								
									<tr>
										<td>
										<p>fi</p>
										</td>
										<td>
										";
										$selected='';
										echo "
										<select id='sel_f_dia' ".$disabled.">
										";
										for($i=1;$i<=31;$i++)
										{
											if($i*1==$mDatesReservesLocals['fdia']*1)	{$selected='selected';} else {$selected='';}
											echo "
											<option ".$selected." value='".$i."'>".$i."</option>
											";
										}
										echo "
										</select>
										</td>
										<td>
										";
										$selected='';
										echo "
										<select id='sel_f_mes' ".$disabled.">
										";
										for($i=1;$i<=12;$i++)
										{
											if($i*1==$mDatesReservesLocals['fmes']*1)	{$selected='selected';} else {$selected='';}
											echo "
											<option ".$selected." value='".$i."'>".$i."</option>
											";
										}
										echo "
										</select>
										</td>
										<td>
										";
										$selected='';
										echo "
										<select id='sel_f_any' ".$disabled.">
										";
										for($i=15;$i<=50;$i++)
										{
											if($i*1==$mDatesReservesLocals['fany']*1)	{$selected='selected';} else {$selected='';}
											echo "
											<option ".$selected." value='".$i."'>".$i."</option>
											";
										}
										echo "
										</select>
										</td>
									</tr>								
								</table>								
								<input type='button' onClick=\"javascript:guardarDatesReservesLocals();\" value='crear el nou periode' ".$disabled.">&nbsp;".(html_ajuda1('html.php',20))."
								</p>
								</td>
							</tr>
						</table>
						</td>
						
						<td width='20%' valign='top'>
						<table align='center' width='100%'>
							<tr>
								<td align='left' width='100%'>
								<table><tr><td><p class='compacte' onClick=\"javascript: enviarFpars('nouPerfilProductorLocal.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>crear perfil de productor local</u></p></td><td>&nbsp;".(html_ajuda1('html.php',24))."</td></tr></table>
	
			";
			if(count($mPropietatsPeriodesLocals)==0)
			{			
				echo "
			<p class='pAlertaNo4' >Atenci�: encara no s'ha creat cap periode <br>de reserves en aquesta llista local.<br>Encara no es poden gestionar productes, ni perfils de productor.</p>
				";
			}
			else
			{
				echo "
								<p><b>Gesti� de productes</b>&nbsp;".(html_ajuda1('html.php',25))."</p>
								<p>Selecciona un perfil de productor associat al teu grup: 
								<br>
				";
				$mPerfilsRef=db_getPerfilsRefGrup($db);
				if(count($mPerfilsRef)>0)
				{
					echo "
								<select id='sel_perfilUsuari2' onChange=\"javascript:if(this.value!=''){enviarFpars('gestioProductes.php?opt=inici&plId='+this.value,'_blank');}\">
					";
					while(list($perfilId,$mPerfil)=each($mPerfilsRef))
					{
					
						$numProductes=db_getNumProductesPerfilAllista($perfilId,$mPars['selLlistaId'],$db);

						if(substr_count(' '.$mPropietatsGrup['idsPerfilsActiusLocal'],$perfilId)>0)
						{
							$actiuLocalText='[perfil actiu]';
						}
						else
						{
							$actiuLocalText='[perfil inactiu]';
						}

						if($numProductes>0){$color='DarkGreen';}else{$color='black';}
						echo "
								<option  style='color:".$color.";' value='".$perfilId."'>".(urldecode($mPerfil['projecte']))." [".$numProductes." productes] ".$actiuLocalText."</option>
						";
					}
					reset($mPerfilsRef);
					echo "
								<option selected value=''></option>
								</select>
								</p>
								<p class='p_micro3'>* s'indica en <font color='DarkGreen'><b>verd</b></font> la productora que ofereix productes en aquest grup</p>
					";
				}
				else
				{
					echo "
				<p style='font-size:11px; color:red;'> - no hi ha cap perfil de productor associat al grup</p>
					";
				}
			}
			echo "
								</td>
							</tr>
						</table>
			";
	}
	else
	{
		echo "
						<input  class='i_micro' type='hidden' id='i_compte_grup_euros' value=''> 
		";
	}
	echo "						
						</td>
					</tr>
				</table>
				</td>
			</tr>

			<tr>
				<td width='100%'>
				<table   width='100%' align='left'>
					<tr>
						<td align='left' valign='bottom' width='20%'>
						</td>
						<td align='left' valign='bottom' width='70%'>
						<table style='width:100%;'>
							<tr>
								<td align='left' width='100%' valign='bottom'>
	";
	if($mPars['selLlistaId']==0)
	{
		if($mPars['grup_id']!=0)
		{
			echo "
								<p style='font-size:11px;'>* A l'import total s'inclouen tots els productes de la comanda del GRUP o de l'usuari, independentment del filtre aplicat i de la p�gina mostrada.</p>
								<p style='font-size:1px;'>* Les reserves de productes especials es mostren pero no es comptabilitzen</p>
			";
		}
		else
		{
			echo "
								<p style='font-size:11px;'>* A l'import total s'inclouen tots els productes de la comanda CAC seleccionats segons els filtres, independentment de la p�gina mostrada.</p>
								<p style='font-size:11px;'>* Es ressalten en verd tots els productes oferits per la CAC, excepte els productes especials</p>
			";
		}
	}
	else
	{
		echo "
								<p style='font-size:11px;'>* A l'import total s'inclouen tots els productes de la comanda, independentment del filtre aplicat i de la p�gina mostrada.</p>
		";
	}
	echo "
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	";

	return;
}

//------------------------------------------------------------------------------
function htmlForm_formaPagament()
{
	global 	$mAjuda,
			$mColors,
			$mParametres,
			$mRebost,
			$mPars,
			$mPropietatsGrup,
			$mPuntsEntrega,
			$mFormaPagamentMembres,
			$pendentGuardarFormaPagamentIpuntEntrega,
			$haFetComanda,
			$teBases,
			$periodeBases,
			$mSelUsuari,
			$mUsuarisGrupRef,
			$numUsuarisGrupNoCom,
			$mComptesUsuari,
			$mComptesGrup,
			$totalUtilitzatEcosRusuaris,
			$utilitzatAquiEcosR;

	echo "
	
	<table width='100%' valign='top' >
		<tr>
			<td width='100%' valign='top'>
			<table width='100%' align='center'>
				<tr>
					<td align='left'>
	";
	if //inici aplicaci� recolzament selectiu: 1507
	(
		$mPars['selUsuariId']!='0'
		&& 
		isset($mParametres['periodeIniciRecolzamentSelectiu']['valor'])
		&&
		$mParametres['periodeIniciRecolzamentSelectiu']['valor']!=''
		&&
		1*$mPars['selRutaSufix']>=@$mParametres['periodeIniciRecolzamentSelectiu']['valor']
	) 
	{
		if(!isset($mComptesGrup['saldo_ecos_r'])  || $mComptesGrup['saldo_ecos_r']==''){$mComptesGrup['saldo_ecos_r']=0;}

		if(!isset($mComptesUsuari['saldo_ecos_r']) || $mComptesUsuari['saldo_ecos_r']==''){$mComptesUsuari['saldo_ecos_r']=0;}
		if(!isset($mComptesUsuari['utilitzat_ecos_r']) || $mComptesUsuari['utilitzat_ecos_r']==''){$mComptesUsuari['utilitzat_ecos_r']=0;}
		$mPropietatsUsuari=getPropietats($mSelUsuari['propietats']);
						
		if(
			isset($mPropietatsUsuari['comissio']) 
			&& 
			$mPropietatsUsuari['comissio']!='' 
			&& 
			$mComptesUsuari['saldo_ecos_r']>0
		)
		{
			echo "
						<a style='cursor:pointer;' title=\"total d'ecos recolzats disponibles en aquest i altres grups\"> 
						Total disponible d'ecos recolzats com a membre comissi�: <b><font  id='fo_disponibleAquiEcosR_sessio'></font></b>/<font>".(number_format($mComptesUsuari['saldo_ecos_r'],2,'.',''))."<font> ecos
						</a>
						<br>
						<a style='cursor:pointer;' title=\"total d'ecos recolzats utilitzats en aquesta comanda\"> 
						Total d'ecos recolzats utilitzats en aquesta comanda: <b><font  id='fo_utilitzatAquiEcosR_sessio'>".$utilitzatAquiEcosR."</font></b>/<font>".(number_format($mComptesUsuari['saldo_ecos_r'],2,'.',''))."<font> ecos [guardat:<font  id='fo_utilitzatAquiEcosR'>".$utilitzatAquiEcosR."</font>]
						</a>
			";
		}
		else if
		(
			(
				(
					!isset($mPropietatsUsuari['comissio'])
					||
					$mPropietatsUsuari['comissio']==''
				)
				||
				$mComptesUsuari['saldo_ecos_r']==0
			) 
			&& $mComptesGrup['saldo_ecos_r']>0
		)
		{
			echo "
						<a style='cursor:pointer;' title=\"total ecos recolzats d'aquest grup\"> 
						Total ecos recolzats d'aquest grup: <b><font  id='fo_disponibleEcosR'>".(number_format($mComptesGrup['saldo_ecos_r'],2,'.',''))."</font></b> ecos
						</a>
						<br>
						<a style='cursor:pointer;' title=\"Total disponible d'ecos recolzats com a membre del grup\"> 
						Total ecos recolzats utilitzat com a membre del grup: <b><font  id='fo_utilitzatAquiEcosR'></font></b>/<font>".(number_format(($mComptesGrup['saldo_ecos_r']/$numUsuarisGrupNoCom),2,'.',''))."<font> ecos
						</a>
			";
		}
		else
		{
			echo "
						<font  id='fo_disponibleEcosR' style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
						<font  id='fo_utilitzatAquiEcosR'  style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
			";
		}
	}
	else if //inici aplicaci� recolzament selectiu: 1507
	(
		$mPars['selUsuariId']=='0'
		&& 
		isset($mParametres['periodeIniciRecolzamentSelectiu']['valor'])
		&& 
		$mParametres['periodeIniciRecolzamentSelectiu']['valor']!=''
		&&
		1*$mPars['selRutaSufix']>=@$mParametres['periodeIniciRecolzamentSelectiu']['valor']
		&& 
		$mComptesGrup['saldo_ecos_r']>0
	) 
	{
		echo "
						<a style='cursor:pointer;' title=\"total ecos recolzats d'aquest grup\"> 
						Ecos recolzats utilitzats d'aquest grup: <b><font  id='fo_utilitzatAquiEcosR'>0</font>/".(number_format($mComptesGrup['saldo_ecos_r'],2,'.',''))."</font></b> ecos
						</a>
						<br>
						<font  id='fo_disponibleEcosR' style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
		";
	}
	else
	{
		echo "
						<font  id='fo_disponibleEcosR' style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
						<font  id='fo_utilitzatAquiEcosR'  style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
		";
	}
	echo "
					<table>
						<tr>
							<td valign='middle'>
	";
	if($periodeBases!='')
	{
		echo "
							<p class='p_micro2' style='cursor:pointer;' onClick=\"enviarFpars('basesGrups.php','_blank');\"> <u>Bases Grup - antiguitat: ".$periodeBases."</u></p>
		";
	}
	else
	{
		echo "
							<p class='p_micro2' style='cursor:pointer;' onClick=\"enviarFpars('basesGrups.php','_blank');\"><u>Bases Grup</u> (encara no han estat publicades pel responsable de grup)</p>
		";
	}

	if
	(
		$teBases
		&&
		$mPars['selUsuariId']==0 
		&& 
		(
			(
				$mPars['selRutaSufix']==date('ym') 
				&& 
				$mParametres['precomandaTancada']['valor']!='1'
				&&
				$mPars['usuari_id']==$mRebost['usuari_id'] 
			)
			||
			$mPars['nivell']=='sadmin' 
		)
	) 
	{
		echo "
							<table>
								<tr>
									<td>
									<p>Despeses gesti� grup:</p>
									</td>
									
									<td>
									<p><input type='text' class='i_micro' id='i_dgGrup' name='i_dgGrup' "; if(!$teBases){echo " DISABLED ";}else{echo " onChange=\"javascript:recordatoriGuardarComanda();\";";} echo " size='3' value='".$mPropietatsGrup['despesesGrup']."'> %&nbsp;".(html_ajuda1('html.php',26))."</p>
									</td>
								</tr>

								<tr>
									<td>
									<p>% ms aplicat:</p>
									</td>
									
									<td>
									<p><input type='text'  class='i_micro'  id='i_msDgGrup' name='i_msDgGrup' "; if(!$teBases){echo " DISABLED ";}else{echo " onChange=\"javascript:recordatoriGuardarComanda();\";";} echo " size='3' value='".$mPropietatsGrup['msDespesesGrup']."'>&nbsp;".(html_ajuda1('html.php',27))."</p>
									</td>
								</tr>
							</table>
		";
	}
	else
	{
		echo "
							<table>
								<tr>
									<td>
									<p>Despeses gesti� grup:</p>
									</td>
									
									<td>
									<p><input type='text'   class='i_micro'  id='i_dgGrup' name='i_dgGrup' DISABLED  size='3' value='".$mPropietatsGrup['despesesGrup']."'> % ".(html_ajuda1('html.php',26))."</p>
									</td>
								</tr>

								<tr>
									<td>
									<p>% ms aplicat:</p>
									</td>
									
									<td>
									<p><input type='text'  class='i_micro'  id='i_msDgGrup' name='i_msDgGrup' DISABLED  size='3' value='".$mPropietatsGrup['msDespesesGrup']."'> % ms ".(html_ajuda1('html.php',27))."</p>
									</td>
								</tr>
							</table>
		";
	}
	echo "
							</td>
	
							<td valign='bottom'>
							<p>Punt d'Entrega:<br>
							<select id='sel_puntEntregaRef' name='sel_puntEntregaRef'
	"; 
	if
	(
		(
			$mParametres['precomandaTancada']['valor']
			&&
			$mPars['nivell']!='sadmin'
			&&
			$mPars['nivell']!='admin'
		)
		||
		$mPars['selUsuariId']!='0'
		|| 
		$mPars['nivell']=='visitant'
	)
	{
		echo " disabled ";
	} 
	echo " 
							 onChange=\"document.getElementById('i_puntEntregaRef').value=this.value;recordatoriGuardarComanda();\">
	";
	$selected='';
	$selected0='selected';
				
	if(in_array($mRebost['id'],array('16','17','18','19'))){$mPars['puntEntregaRef']=57;}

	while(list($key,$mVal)=each($mPuntsEntrega))
	{
		//if($mFormaPagamentMembres['punt_entrega']==''){$mPars['puntEntregaRef']=$mRebost['ref'];}
		if($mFormaPagamentMembres['punt_entrega']==$mVal['id']){$selected='selected';$selected0='';}else{$selected='';}
		echo "
							<option id='o_pe_".$mVal['id']."' value='".$mVal['id']."' ".$selected.">".(urldecode($mVal['nom']))." (".(urldecode($mVal['municipi'])).")</option>
		";
	}
	reset($mPuntsEntrega);
	echo "
							<option value='' ".$selected0."></option>
							</select>
							</p>
							</td>
					
							<td align='left'>
	";
	if
	(
		$haFetComanda==1 //comanda de grup
		&&
		$pendentGuardarFormaPagamentIpuntEntrega
		&&
		$mPars['grup_id']!=0 
	)	
	{
		if($mPars['selUsuariId']==0)
		{
			echo "<p class='p_micro2' style='color:red;'><b>Atenci�: No s'ha guardat el punt d'entrega i la forma de pagament<br> del grup. Poden apareixer els valors per defecte pero no estan guardats.<br>Aix� pot provocar problemes en la distribuci� i el pagament de la comanda<br> d'aquest grup</b></p>";
		}
		else
		{
			echo "<p class='p_micro2' style='color:red;'><b>Atenci�: El responsable de grup encara no ha guardat el punt d'entrega<br>(si apareix es el valor per defecte, pero no est� guardat)<br> i la forma de pagament del grup.<br> Aix� pot provocar problemes en la distribuci� i el pagament de la<br> comanda d'aquest grup</b></p>";
		}
	}
					
	echo"						
					
							</td>
						</tr>
					</table>
					</td>
				</tr>

				<tr>
					<td align='left'>
	";
	$text2="individual, al GRUP";
	if($mPars['grup_id']==0)
	{
		$text2="CAC";
	}
	else if($mPars['selUsuariId']==0)
	{
		$text2="col.lectiva, a la CAC";
	}
	
	if($mParametres['moneda3Activa']['valor']==1)
	{
		$moneda3ElementsStyle=" style='visibility:inherit; z-index:0; position:relative;' ";
	}
	else
	{
		$moneda3ElementsStyle=" style='visibility:hidden; z-index:1;  position:absolute;' ";
	}
					
	echo "
					<p>Forma de Pagament (".$text2."):</p>
					</td>
					<td align='right'>
					</td>
				</tr>
			</table>
				
			<table border='0' align='center' style='width:100%'>
				<tr>
					<td valign='top'>
					</td>
					<td valign='top'>
					<p>UMS(%):<br><input type='text' class='i_readonly' readonly id='i_fPagamentUmsPerc_' name='i_fPagamentUmsPerc_' value='100' size='5'></p>
					</td>
					<td valign='top'>
					<p>ecos(%):<br><input type='text' class='i_readonly'  readonly id='i_fPagamentEcosPerc_' name='i_fPagamentEcosPerc_' value='".(number_format($mPars['fPagamentEcosPerc'],2))."' size='5'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p>".$mParametres['moneda3Abrev']['valor']."(%):<br><input type='text' class='i_readonly'  readonly id='i_fPagamentEbPerc_' name='i_fPagamentEbPerc_' value='".(number_format($mPars['fPagamentEbPerc'],2))."' size='5'></p>
					</td>
					<td valign='top'>
					<p>euros(%):<br><input type='text' class='i_readonly'  id='i_fPagamentEuPerc_' readonly name='i_fPagamentEuPerc_' value='".(number_format($mPars['fPagamentEuPerc'],2))."' size='5'></p>
					</td>

					<td valign='bottom'>
	";
	$disabled='';
	if($mParametres['precomandaTancada']['valor'] &&  $mPars['nivell']!='sadmin'  && $mPars['nivell']!='admin')	{$disabled='disabled';}
	echo "
					<p>
					<table><tr><td><p>Convertir(ums):</p></td><td>&nbsp;".(html_ajuda1('html.php',28))."</td></tr></table>
					<a title=\"Definir forma de pagament: Seleccionar una opci� de conversi� per passar unitats, decenes o centenes d'una moneda a l'altra\">
					<select ".$disabled." id='sel_fPagamentConversioMonedes_' onChange=\"fp_convertir(0,this.value);recordatoriGuardarComanda();\" name='sel_fPagamentConversioMonedes_'>
	";
	
//v36-3-12-15-marca1
	// ecos recolzats:
	$disabled7=''; //100% ecos
	$disabled11=''; //50n % ecos i 150% euros
	//si euros comanda>ecos recolzats usuari + ecox recolzats grup.usuari $disabled7='disabled'
	
	//if()si euros comanda>ecos recolzats usuari + ecos recolzats grup.usuari $disabled7='disabled'
	
	if($mParametres['moneda3Activa']['valor']==1)
	{
		$mSelected=array(1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>'',10=>'',11=>'',12=>'',13=>'');
		if(array_key_exists($mPars['fPagamentOpcioConversor'],$mSelected)){$mSelected[$mPars['fPagamentOpcioConversor']]=" selected class='o_selected' ";}else{$mSelected[$mPars['fPagamentOpcioConversor']]='';$mSelected['5']=" selected class='o_selected' ";}
		echo "
					<option ".$mSelected[1]." value='1'>ecos>>".$mParametres['moneda3Nom']['valor']."</option>
					<option ".$mSelected[2]." value='2'>ecos>>euros</option>
					<option ".$mSelected[3]." value='3'>".$mParametres['moneda3Nom']['valor'].">>ecos</option>
					<option ".$mSelected[4]." value='4'>".$mParametres['moneda3Nom']['valor'].">>euros</option>
					<option ".$mSelected[5]." value='5'>euros>>ecos</option>
					<option ".$mSelected[6]." value='6'>euros>>".$mParametres['moneda3Nom']['valor']."</option>
		";
//*v36-condicio
		if($mPars['selRutaSufixPeriode']*1<$mParametres['periodeIniciRecolzamentSelectiu']['valor']*1)
		{
			echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>100 % ecos</option>
			";
		}
		else
		{
			echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>m�xim % ecos</option>
			";
		}
		
		echo "
					<option ".$mSelected[8]." value='8'>100% ".$mParametres['moneda3Nom']['valor']."</option>
					<option ".$mSelected[9]." value='9'>100% euros</option>
					<option ".$mSelected[10]." value='10'>50% ecos i 50% ".$mParametres['moneda3Nom']['valor']."</option>
					<option ".$mSelected[11]." ".$disabled11." value='11'>50% ecos i 50% euros</option>
					<option ".$mSelected[12]." value='12'>50% ".$mParametres['moneda3Nom']['valor']." i 50% euros</option>
					<option ".$mSelected[13]." value='13'>%ecos i % euros segons productors</option>
		";
	}
	else
	{

		$mSelected=array(2=>'',5=>'',7=>'',9=>'',11=>'',13=>'');
		if(array_key_exists($mPars['fPagamentOpcioConversor'],$mSelected)){$mSelected[$mPars['fPagamentOpcioConversor']]=" selected class='o_selected' ";}else{$mSelected[$mPars['fPagamentOpcioConversor']]=''; $mSelected['7']=" selected class='o_selected' ";}
		echo "
					<option ".$mSelected[2]." value='2'>ecos>>euros</option>
					<option ".$mSelected[5]." selected value='5'>euros>>ecos</option>
		";
//*v36-condicio
		if($mPars['selRutaSufixPeriode']*1<@$mParametres['periodeIniciRecolzamentSelectiu']['valor'])
		{
			echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>100 % ecos</option>
			";
		}
		else
		{
			echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>m�xim % ecos</option>
			";
		}
		
		echo "
					<option ".$mSelected[9]." value='9'>100% euros</option>
					<option ".$mSelected[11]." ".$disabled11." value='11'>50% ecos i 50% euros</option>
					<option ".$mSelected[13]." value='13'>%ecos i % euros segons productors</option>
		";
	}
//v36-3-12-15-marca1
	echo "
	
					</select>
					</a>
					</p>
					<table>
						<tr>
							<td>
							<p><a title=\"clicar per convertir 1 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,1);recordatoriGuardarComanda()\" id='i_conversor1' name='i_conversor1' value='+'></a>(1 um)</p>
							<p><a title=\"clicar per convertir 1 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,4);recordatoriGuardarComanda()\" id='i_conversor4' name='i_conversor4' value='+'></a>(0.01 um)</p>
							</td>
							<td>
							<p><a title=\"clicar per convertir 10 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,2);recordatoriGuardarComanda()\" id='i_conversor2' name='i_conversor2' value='+'></a>(10 ums)</p>
							<p><a title=\"clicar per convertir 100 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,3);recordatoriGuardarComanda()\" id='i_conversor3' name='i_conversor3' value='+'></a>(100 ums)</p>
							</td>
						</tr>
					</table>
	";
	if($mPars['selUsuariId']==0)
	{
		echo "
					<table><tr><td><input type='button' ".$disabled." onClick=\"javascrit:resetConversor(0);\" value='pagament=ingressos'></td><td> ".(html_ajuda1('html.php',29))."</td></tr></table>
		";
	}
					
	echo "
					</td>
				</tr>

				<tr>
					<td valign='bottom' align='left'>
	";
	if($mPars['selUsuariId']==0)
	{
		echo "
					<p>Pagament del GRUP a CAC:</p>
		";
	}
	echo "
					</td>
					<td valign='top'>
					<p>UMS:<br><input  class='i_readonly'  readonly type='text' id='i_fPagamentUms_' name='i_fPagamentUms_' value='".(@number_format($mPars['fPagamentUms'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top'>
					<p>ecos:<br><input class='i_readonly'  readonly type='text' id='i_fPagamentEcos_' name='i_fPagamentEcos_' value='".(@number_format($mPars['fPagamentEcos'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p>".$mParametres['moneda3Abrev']['valor'].":<br><input class='i_readonly'  readonly type='text' id='i_fPagamentEb_' name='i_fPagamentEb_' value='".(@number_format($mPars['fPagamentEb'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top'>
					<p>euros:<br><input class='i_readonly'  readonly type='text' id='i_fPagamentEu_' name='i_fPagamentEu_' value='".(@number_format($mPars['fPagamentEu'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top'>
					</td>
				</tr>
	";
	$visibility='hidden';
	$position='absolute';
					
	if($mPars['selUsuariId']==0){$visibility='inherit';$position='relative';}

	echo "
				<tr style='position:".$position."; visibility:".$visibility.";'>
					<td valign='top'>
					<p>Total pagaments membres al GRUP:</p>
					</td>
					<td valign='top'>
					<p><input  class='i_readonly2'  readonly type='text' id='i_fPagamentUmsG_' name='i_fPagamentUmsG_' value='".(@number_format($mFormaPagamentMembres['fPagamentUms'],2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly2'  readonly type='text' id='i_fPagamentEcosG_' name='i_fPagamentEcosG_' value='".(@number_format($mFormaPagamentMembres['fPagamentEcos'],2,'.',''))."' size='8'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p><input class='i_readonly2'  readonly type='text' id='i_fPagamentEbG_' name='i_fPagamentEbG_' value='".(@number_format($mFormaPagamentMembres['fPagamentEb'],2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly2'  readonly type='text' id='i_fPagamentEuG_' name='i_fPagamentEuG_' value='".(@number_format($mFormaPagamentMembres['fPagamentEu'],2,'.',''))."' size='8'></p>
					</td>
				</tr>
				
				<tr style='position:".$position."; visibility:".$visibility.";'>
					<td valign='top'>
					<p>balan� de moneda del GRUP:</p>
					</td>
					<td valign='top'>
					<p><input  class='i_readonly3'  readonly type='text' id='i_fPagamentUmsB_' name='i_fPagamentUmsB_' value='".(number_format(($mPars['fPagamentUms']-$mFormaPagamentMembres['fPagamentUms']),2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly3'  readonly type='text' id='i_fPagamentEcosB_' name='i_fPagamentEcosB_' value='".(number_format(($mPars['fPagamentEcos']-$mFormaPagamentMembres['fPagamentEcos']),2,'.',''))."' size='8'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p><input class='i_readonly3'  readonly type='text' id='i_fPagamentEbB_' name='i_fPagamentEbB_' value='".(number_format(($mPars['fPagamentEb']-$mFormaPagamentMembres['fPagamentEb']),2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly3'  readonly type='text' id='i_fPagamentEuB_' name='i_fPagamentEuB_' value='".(number_format(($mPars['fPagamentEu']-$mFormaPagamentMembres['fPagamentEu']),2,'.',''))."' size='8'></p>
					</td>
					<td valign='bottom'>
					</td>
				</tr>

				<tr>
					<td valign='top'>
					</td>
					<td valign='top'>
					</td>
					<td valign='top'>
					<p>Compte CES: ".(html_ajuda1('html.php',33))."
					<br><input ";if($mParametres['precomandaTancada']['valor']  && $mPars['nivell']!='sadmin'  && $mPars['nivell']!='admin'){echo " READONLY ";} echo " type='text' id='i_fPagamentCompteEcos_' name='i_fPagamentCompteEcos_' value='".$mPars['compte_ecos']."' size='8'  onChange=\"javascript: recordatoriGuardarComanda();\">
					<br>
					[<input ";if($mParametres['precomandaTancada']['valor'] && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin'){echo " DISABLED ";} echo " type='checkBox' ";if ($mPars['fPagamentEcosIntegralCES']=='1'){echo " CHECKED ";} echo " onClick=\"recordatoriGuardarComanda();if(this.value==0){this.value=1;}else if(this.value==1){this.value=0;}\" id='ck_fPagamentEcosIntegralCES_' name='ck_fPagamentEcosIntegralCES_' value='".$mPars['fPagamentEcosIntegralCES']."'>IntegralCES]
					</p>
					</td>
					<td valign='top'  ".$moneda3ElementsStyle.">
					<p>Compte ".$mParametres['moneda3Abrev']['valor'].":<br><input ";if($mParametres['precomandaTancada']['valor']  && $mPars['nivell']!='sadmin'  && $mPars['nivell']!='admin'){echo " READONLY ";} echo " type='text' id='i_fPagamentCompteEb_' name='i_fPagamentCompteEb_' value='".$mPars['compte_cieb']."' size='".$mParametres['moneda3DigitsCodi']['valor']."' onChange=\"javascript: recordatoriGuardarComanda();\"></p>
					</td>
					<td valign='top'>
					</td>
					<td valign='top'>
					<div style='position:".$position."; visibility:".$visibility.";'>
					<p id='p_compte_cab_euros'>(Compte CAC Euros:<br>".$mParametres['compte_bancari_cac_1']['valor'].")</p>
					";
					if($mParametres['compte_bancari_cac_1']['valor']!='')
					{
						echo "
						<p><input ";if($mParametres['precomandaTancada']['valor'] && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin'){echo " DISABLED ";} echo " type='checkBox' ";if ($mPars['fPagamentEuMetalic']=='1'){echo " CHECKED ";} echo " onClick=\"javascript: checkbox_selectEUoTRANSF('ck_fPagamentEuMetalic_');\" id='ck_fPagamentEuMetalic_' name='ck_fPagamentEuMetalic_' value='".$mPars['fPagamentEuMetalic']."'>&#8364; en met�l.lic a l'entrega</p>
						<p><input ";if($mParametres['precomandaTancada']['valor'] && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin'){echo " DISABLED ";} echo " type='checkBox' ";if ($mPars['fPagamentEuTransf']=='1'){echo " CHECKED ";} echo " onClick=\"javascript: checkbox_selectEUoTRANSF('ck_fPagamentEuTransf_');\" id='ck_fPagamentEuTransf_' name='ck_fPagamentEuTransf_' value='".$mPars['fPagamentEuTransf']."'>&#8364; per transferencia</p>
						";
					}
					else
					{
						echo "
						<p><input CHECKED DISABLED id='ck_fPagamentEuMetalic_' name='ck_fPagamentEuMetalic_' value='1'>&#8364; en met�l.lic a l'entrega</p>
						<p><input type='hidden' style='position:absolute; z-index:0; top:0px; visibility:hidden;' DISABLED  id='ck_fPagamentEuTransf_' name='ck_fPagamentEuTransf_' value='".$mPars['fPagamentEuTransf']."'>&#8364; per transferencia</p>
						";
					}
					
					echo "
					</div>
					</td>
				</tr>
			</table>
			<div width='80%' align='center'>
			<p class='nota'>* No s'inclouen abonaments i c�rrecs. Els abonaments i c�rrecs de la CAC al Grup, i els del Grup als membres, nom�s s'inclouen en els respectius albarans</p>
			</div>
			</td>
		</tr>
	</table>
	";

return;
}

//------------------------------------------------------------------------------
function htmlForm_formaPagamentOld()
{
	global 	$mAjuda,
			$mColors,
			$mParametres,
			$mRebost,
			$mPars,
			$mPropietatsGrup,
			$mPuntsEntrega,
			$mFormaPagamentMembres,
			$pendentGuardarFormaPagamentIpuntEntrega,
			$haFetComanda,
			$teBases,
			$periodeBases,
			$mSelUsuari,
			$mUsuarisGrupRef,
			$numUsuarisGrupNoCom,
			$mComptesUsuari,
			$mComptesGrup,
			$totalUtilitzatEcosRusuaris,
			$utilitzatAquiEcosR;

	echo "
	
	<table width='100%' valign='top' >
		<tr>
			<td width='100%' valign='top'>
			<table width='100%' align='center'>
				<tr>
					<td align='left'>
	";
	if //inici aplicaci� recolzament selectiu: 1507
	(
		$mPars['selUsuariId']!='0'
		&& 
		isset($mParametres['periodeIniciRecolzamentSelectiu']['valor'])
		&&
		$mParametres['periodeIniciRecolzamentSelectiu']['valor']!=''
		&&
		1*$mPars['selRutaSufix']>=@$mParametres['periodeIniciRecolzamentSelectiu']['valor']
	) 
	{
		if(!isset($mComptesGrup['saldo_ecos_r'])  || $mComptesGrup['saldo_ecos_r']==''){$mComptesGrup['saldo_ecos_r']=0;}

		if(!isset($mComptesUsuari['saldo_ecos_r']) || $mComptesUsuari['saldo_ecos_r']==''){$mComptesUsuari['saldo_ecos_r']=0;}
		if(!isset($mComptesUsuari['utilitzat_ecos_r']) || $mComptesUsuari['utilitzat_ecos_r']==''){$mComptesUsuari['utilitzat_ecos_r']=0;}
		$mPropietatsUsuari=getPropietats($mSelUsuari['propietats']);
						
		if(
			isset($mPropietatsUsuari['comissio']) 
			&& 
			$mPropietatsUsuari['comissio']!='' 
			&& 
			$mComptesUsuari['saldo_ecos_r']>0
		)
		{
			echo "
						<a style='cursor:pointer;' title=\"total d'ecos recolzats disponibles en aquest i altres grups\"> 
						Total disponible d'ecos recolzats com a membre comissi�: <b><font  id='fo_disponibleAquiEcosR_sessio'></font></b>/<font>".(number_format($mComptesUsuari['saldo_ecos_r'],2,'.',''))."<font> ecos
						</a>
						<br>
						<a style='cursor:pointer;' title=\"total d'ecos recolzats utilitzats en aquesta comanda\"> 
						Total d'ecos recolzats utilitzats en aquesta comanda: <b><font  id='fo_utilitzatAquiEcosR_sessio'>".$utilitzatAquiEcosR."</font></b>/<font>".(number_format($mComptesUsuari['saldo_ecos_r'],2,'.',''))."<font> ecos [guardat:<font  id='fo_utilitzatAquiEcosR'>".$utilitzatAquiEcosR."</font>]
						</a>
			";
		}
		else if
		(
			(
				(
					!isset($mPropietatsUsuari['comissio'])
					||
					$mPropietatsUsuari['comissio']==''
				)
				||
				$mComptesUsuari['saldo_ecos_r']==0
			) 
			&& $mComptesGrup['saldo_ecos_r']>0
		)
		{
			echo "
						<a style='cursor:pointer;' title=\"total ecos recolzats d'aquest grup\"> 
						Total ecos recolzats d'aquest grup: <b><font  id='fo_disponibleEcosR'>".(number_format($mComptesGrup['saldo_ecos_r'],2,'.',''))."</font></b> ecos
						</a>
						<br>
						<a style='cursor:pointer;' title=\"Total disponible d'ecos recolzats com a membre del grup\"> 
						Total ecos recolzats utilitzat com a membre del grup: <b><font  id='fo_utilitzatAquiEcosR'></font></b>/<font>".(number_format(($mComptesGrup['saldo_ecos_r']/$numUsuarisGrupNoCom),2,'.',''))."<font> ecos
						</a>
			";
		}
		else
		{
			echo "
						<font  id='fo_disponibleEcosR' style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
						<font  id='fo_utilitzatAquiEcosR'  style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
			";
		}
	}
	else if //inici aplicaci� recolzament selectiu: 1507
	(
		$mPars['selUsuariId']=='0'
		&& 
		isset($mParametres['periodeIniciRecolzamentSelectiu']['valor'])
		&& 
		$mParametres['periodeIniciRecolzamentSelectiu']['valor']!=''
		&&
		1*$mPars['selRutaSufix']>=@$mParametres['periodeIniciRecolzamentSelectiu']['valor']
		&& 
		$mComptesGrup['saldo_ecos_r']>0
	) 
	{
		echo "
						<a style='cursor:pointer;' title=\"total ecos recolzats d'aquest grup\"> 
						Ecos recolzats utilitzats d'aquest grup: <b><font  id='fo_utilitzatAquiEcosR'>0</font>/".(number_format($mComptesGrup['saldo_ecos_r'],2,'.',''))."</font></b> ecos
						</a>
						<br>
						<font  id='fo_disponibleEcosR' style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
		";
	}
	else
	{
		echo "
						<font  id='fo_disponibleEcosR' style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
						<font  id='fo_utilitzatAquiEcosR'  style='z-index:0; top:0px; position:absolute; visibility:hidden;'></font>
		";
	}
	echo "
					<table>
						<tr>
							<td valign='middle'>
	";
	if($periodeBases!='')
	{
		echo "
							<p class='p_micro2' style='cursor:pointer;' onClick=\"enviarFpars('basesGrups.php','_blank');\"> <u>Bases Grup - antiguitat: ".$periodeBases."</u></p>
		";
	}
	else
	{
		echo "
							<p class='p_micro2' style='cursor:pointer;' onClick=\"enviarFpars('basesGrups.php','_blank');\"><u>Bases Grup</u> (encara no han estat publicades pel responsable de grup)</p>
		";
	}

	if
	(
		$teBases
		&&
		$mPars['selUsuariId']==0 
		&& 
		(
			(
				$mPars['selRutaSufix']==date('ym') 
				&& 
				$mParametres['precomandaTancada']['valor']!='1'
				&&
				$mPars['usuari_id']==$mRebost['usuari_id'] 
			)
			||
			$mPars['nivell']=='sadmin' 
		)
	) 
	{
		echo "
							<table>
								<tr>
									<td>
									<p>Despeses gesti� grup:</p>
									</td>
									
									<td>
									<p><input type='text' class='i_micro' id='i_dgGrup' name='i_dgGrup' "; if(!$teBases){echo " DISABLED ";}else{echo " onChange=\"javascript:recordatoriGuardarComanda();\";";} echo " size='3' value='".$mPropietatsGrup['despesesGrup']."'> %&nbsp;".(html_ajuda1('html.php',26))."</p>
									</td>
								</tr>

								<tr>
									<td>
									<p>% ms aplicat:</p>
									</td>
									
									<td>
									<p><input type='text'  class='i_micro'  id='i_msDgGrup' name='i_msDgGrup' "; if(!$teBases){echo " DISABLED ";}else{echo " onChange=\"javascript:recordatoriGuardarComanda();\";";} echo " size='3' value='".$mPropietatsGrup['msDespesesGrup']."'>&nbsp;".(html_ajuda1('html.php',27))."</p>
									</td>
								</tr>
							</table>
		";
	}
	else
	{
		echo "
							<table>
								<tr>
									<td>
									<p>Despeses gesti� grup:</p>
									</td>
									
									<td>
									<p><input type='text'   class='i_micro'  id='i_dgGrup' name='i_dgGrup' DISABLED  size='3' value='".$mPropietatsGrup['despesesGrup']."'> % ".(html_ajuda1('html.php',26))."</p>
									</td>
								</tr>

								<tr>
									<td>
									<p>% ms aplicat:</p>
									</td>
									
									<td>
									<p><input type='text'  class='i_micro'  id='i_msDgGrup' name='i_msDgGrup' DISABLED  size='3' value='".$mPropietatsGrup['msDespesesGrup']."'> % ms ".(html_ajuda1('html.php',27))."</p>
									</td>
								</tr>
							</table>
		";
	}
	echo "
							</td>
	
							<td valign='bottom'>
							<p>Punt d'Entrega:<br>
							<select id='sel_puntEntregaRef' name='sel_puntEntregaRef'
	"; 
	if
	(
		(
			$mParametres['precomandaTancada']['valor']
			&&
			$mPars['nivell']!='sadmin'
			&&
			$mPars['nivell']!='admin'
		)
		||
		$mPars['selUsuariId']!='0'
		|| 
		$mPars['nivell']=='visitant'
	)
	{
		echo " disabled ";
	} 
	echo " 
							 onChange=\"document.getElementById('i_puntEntregaRef').value=this.value;recordatoriGuardarComanda();\">
	";
	$selected='';
	$selected0='selected';
				
	if(in_array($mRebost['id'],array('16','17','18','19'))){$mPars['puntEntregaRef']=57;}

	while(list($key,$mVal)=each($mPuntsEntrega))
	{
		//if($mFormaPagamentMembres['punt_entrega']==''){$mPars['puntEntregaRef']=$mRebost['ref'];}
		if($mFormaPagamentMembres['punt_entrega']==$mVal['id']){$selected='selected';$selected0='';}else{$selected='';}
		echo "
							<option id='o_pe_".$mVal['id']."' value='".$mVal['id']."' ".$selected.">".(urldecode($mVal['nom']))." (".(urldecode($mVal['municipi'])).")</option>
		";
	}
	reset($mPuntsEntrega);
	echo "
							<option value='' ".$selected0."></option>
							</select>
							</p>
							</td>
					
							<td align='left'>
	";
	if
	(
		$haFetComanda==1 //comanda de grup
		&&
		$pendentGuardarFormaPagamentIpuntEntrega
		&&
		$mPars['grup_id']!=0 
	)	
	{
		if($mPars['selUsuariId']==0)
		{
			echo "<p class='p_micro2' style='color:red;'><b>Atenci�: No s'ha guardat el punt d'entrega i la forma de pagament<br> del grup. Poden apareixer els valors per defecte pero no estan guardats.<br>Aix� pot provocar problemes en la distribuci� i el pagament de la comanda<br> d'aquest grup</b></p>";
		}
		else
		{
			echo "<p class='p_micro2' style='color:red;'><b>Atenci�: El responsable de grup encara no ha guardat el punt d'entrega<br>(si apareix es el valor per defecte, pero no est� guardat)<br> i la forma de pagament del grup.<br> Aix� pot provocar problemes en la distribuci� i el pagament de la<br> comanda d'aquest grup</b></p>";
		}
	}
					
	echo"						
					
							</td>
						</tr>
					</table>
					</td>
				</tr>

				<tr>
					<td align='left'>
	";
	$text2="individual, al GRUP";
	if($mPars['grup_id']==0)
	{
		$text2="CAC";
	}
	else if($mPars['selUsuariId']==0)
	{
		$text2="col.lectiva, a la CAC";
	}
	
	if($mParametres['moneda3Activa']['valor']==1)
	{
		$moneda3ElementsStyle=" style='visibility:inherit; z-index:0; position:relative;' ";
	}
	else
	{
		$moneda3ElementsStyle=" style='visibility:hidden; z-index:1;  position:absolute;' ";
	}
					
	echo "
					<p>Forma de Pagament (".$text2."):</p>
					</td>
					<td align='right'>
					</td>
				</tr>
			</table>
				
			<table border='0' align='center' style='width:100%'>
				<tr>
					<td valign='top'>
					</td>
					<td valign='top'>
					<p>UMS(%):<br><input type='text' class='i_readonly' readonly id='i_fPagamentUmsPerc_' name='i_fPagamentUmsPerc_' value='100' size='5'></p>
					</td>
					<td valign='top'>
					<p>ecos(%):<br><input type='text' class='i_readonly'  readonly id='i_fPagamentEcosPerc_' name='i_fPagamentEcosPerc_' value='".(number_format($mPars['fPagamentEcosPerc'],2))."' size='5'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p>".$mParametres['moneda3Abrev']['valor']."(%):<br><input type='text' class='i_readonly'  readonly id='i_fPagamentEbPerc_' name='i_fPagamentEbPerc_' value='".(number_format($mPars['fPagamentEbPerc'],2))."' size='5'></p>
					</td>
					<td valign='top'>
					<p>euros(%):<br><input type='text' class='i_readonly'  id='i_fPagamentEuPerc_' readonly name='i_fPagamentEuPerc_' value='".(number_format($mPars['fPagamentEuPerc'],2))."' size='5'></p>
					</td>
					<td valign='bottom'>
	";
	$disabled='';
	if($mParametres['precomandaTancada']['valor'] &&  $mPars['nivell']!='sadmin'  && $mPars['nivell']!='admin')	{$disabled='disabled';}
	echo "
					<p>
					<table><tr><td><p>Convertir(ums):</p></td><td>&nbsp;".(html_ajuda1('html.php',28))."</td></tr></table>
					<a title=\"Definir forma de pagament: Seleccionar una opci� de conversi� per passar unitats, decenes o centenes d'una moneda a l'altra\">
					<select ".$disabled." id='sel_fPagamentConversioMonedes_' onChange=\"fp_convertir(0,this.value);recordatoriGuardarComanda();\" name='sel_fPagamentConversioMonedes_'>
	";
	
//v36-3-12-15-marca1
	// ecos recolzats:
	$disabled7=''; //100% ecos
	$disabled11=''; //50n % ecos i 150% euros
	//si euros comanda>ecos recolzats usuari + ecox recolzats grup.usuari $disabled7='disabled'
	
	//if()si euros comanda>ecos recolzats usuari + ecos recolzats grup.usuari $disabled7='disabled'
	
	if($mParametres['moneda3Activa']['valor']==1)
	{
		$mSelected=array(1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>'',10=>'',11=>'',12=>'',13=>'');
		if(array_key_exists($mPars['fPagamentOpcioConversor'],$mSelected)){$mSelected[$mPars['fPagamentOpcioConversor']]=" selected class='o_selected' ";}else{$mSelected[$mPars['fPagamentOpcioConversor']]='';$mSelected['5']=" selected class='o_selected' ";}
		echo "
					<option ".$mSelected[1]." value='1'>ecos>>".$mParametres['moneda3Nom']['valor']."</option>
					<option ".$mSelected[2]." value='2'>ecos>>euros</option>
					<option ".$mSelected[3]." value='3'>".$mParametres['moneda3Nom']['valor'].">>ecos</option>
					<option ".$mSelected[4]." value='4'>".$mParametres['moneda3Nom']['valor'].">>euros</option>
					<option ".$mSelected[5]." value='5'>euros>>ecos</option>
					<option ".$mSelected[6]." value='6'>euros>>".$mParametres['moneda3Nom']['valor']."</option>
		";
//*v36-condicio
		if($mPars['selRutaSufixPeriode']*1<$mParametres['periodeIniciRecolzamentSelectiu']['valor']*1)
		{
			echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>100 % ecos</option>
			";
		}
		else
		{
			echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>m�xim % ecos</option>
			";
		}
		
		echo "
					<option ".$mSelected[8]." value='8'>100% ".$mParametres['moneda3Nom']['valor']."</option>
					<option ".$mSelected[9]." value='9'>100% euros</option>
					<option ".$mSelected[10]." value='10'>50% ecos i 50% ".$mParametres['moneda3Nom']['valor']."</option>
					<option ".$mSelected[11]." ".$disabled11." value='11'>50% ecos i 50% euros</option>
					<option ".$mSelected[12]." value='12'>50% ".$mParametres['moneda3Nom']['valor']." i 50% euros</option>
					<option ".$mSelected[13]." value='13'>%ecos i % euros segons productors</option>
		";
	}
	else
	{

		$mSelected=array(2=>'',5=>'',7=>'',9=>'',11=>'',13=>'');
		if(array_key_exists($mPars['fPagamentOpcioConversor'],$mSelected)){$mSelected[$mPars['fPagamentOpcioConversor']]=" selected class='o_selected' ";}else{$mSelected[$mPars['fPagamentOpcioConversor']]=''; $mSelected['7']=" selected class='o_selected' ";}
		echo "
					<option ".$mSelected[2]." value='2'>ecos>>euros</option>
					<option ".$mSelected[5]." selected value='5'>euros>>ecos</option>
		";
//*v36-condicio
		if($mPars['selRutaSufixPeriode']*1<@$mParametres['periodeIniciRecolzamentSelectiu']['valor'])
		{
			echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>100 % ecos</option>
			";
		}
		else
		{
			echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>m�xim % ecos</option>
			";
		}
		
		echo "
					<option ".$mSelected[9]." value='9'>100% euros</option>
					<option ".$mSelected[11]." ".$disabled11." value='11'>50% ecos i 50% euros</option>
					<option ".$mSelected[13]." value='13'>%ecos i % euros segons productors</option>
		";
	}
//v36-3-12-15-marca1
	echo "
	
					</select>
					</a>
					</p>
					<table>
						<tr>
							<td>
							<p><a title=\"clicar per convertir 1 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,1);recordatoriGuardarComanda()\" id='i_conversor1' name='i_conversor1' value='+'></a>(1 um)</p>
							<p><a title=\"clicar per convertir 1 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,4);recordatoriGuardarComanda()\" id='i_conversor4' name='i_conversor4' value='+'></a>(0.01 um)</p>
							</td>
							<td>
							<p><a title=\"clicar per convertir 10 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,2);recordatoriGuardarComanda()\" id='i_conversor2' name='i_conversor2' value='+'></a>(10 ums)</p>
							<p><a title=\"clicar per convertir 100 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,3);recordatoriGuardarComanda()\" id='i_conversor3' name='i_conversor3' value='+'></a>(100 ums)</p>
							</td>
						</tr>
					</table>
	";
	if($mPars['selUsuariId']==0)
	{
		echo "
					<table><tr><td><input type='button' ".$disabled." onClick=\"javascrit:resetConversor(0);\" value='pagament=ingressos'></td><td> ".(html_ajuda1('html.php',29))."</td></tr></table>
		";
	}
					
	echo "
					</td>
				</tr>

				<tr>
					<td valign='bottom' align='left'>
	";
	if($mPars['selUsuariId']==0)
	{
		echo "
					<p>Pagament del GRUP a CAC:</p>
		";
	}
	echo "
					</td>
					<td valign='top'>
					<p>UMS:<br><input  class='i_readonly'  readonly type='text' id='i_fPagamentUms_' name='i_fPagamentUms_' value='".(@number_format($mPars['fPagamentUms'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top'>
					<p>ecos:<br><input class='i_readonly'  readonly type='text' id='i_fPagamentEcos_' name='i_fPagamentEcos_' value='".(@number_format($mPars['fPagamentEcos'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p>".$mParametres['moneda3Abrev']['valor'].":<br><input class='i_readonly'  readonly type='text' id='i_fPagamentEb_' name='i_fPagamentEb_' value='".(@number_format($mPars['fPagamentEb'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top'>
					<p>euros:<br><input class='i_readonly'  readonly type='text' id='i_fPagamentEu_' name='i_fPagamentEu_' value='".(@number_format($mPars['fPagamentEu'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top'>
					</td>
				</tr>
	";
	$visibility='hidden';
	$position='absolute';
					
	if($mPars['selUsuariId']==0){$visibility='inherit';$position='relative';}

	echo "
				<tr style='position:".$position."; visibility:".$visibility.";'>
					<td valign='top'>
					<p>Total pagaments membres al GRUP:</p>
					</td>
					<td valign='top'>
					<p><input  class='i_readonly2'  readonly type='text' id='i_fPagamentUmsG_' name='i_fPagamentUmsG_' value='".(@number_format($mFormaPagamentMembres['fPagamentUms'],2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly2'  readonly type='text' id='i_fPagamentEcosG_' name='i_fPagamentEcosG_' value='".(@number_format($mFormaPagamentMembres['fPagamentEcos'],2,'.',''))."' size='8'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p><input class='i_readonly2'  readonly type='text' id='i_fPagamentEbG_' name='i_fPagamentEbG_' value='".(@number_format($mFormaPagamentMembres['fPagamentEb'],2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly2'  readonly type='text' id='i_fPagamentEuG_' name='i_fPagamentEuG_' value='".(@number_format($mFormaPagamentMembres['fPagamentEu'],2,'.',''))."' size='8'></p>
					</td>
				</tr>
				
				<tr style='position:".$position."; visibility:".$visibility.";'>
					<td valign='top'>
					<p>balan� de moneda del GRUP:</p>
					</td>
					<td valign='top'>
					<p><input  class='i_readonly3'  readonly type='text' id='i_fPagamentUmsB_' name='i_fPagamentUmsB_' value='".(number_format(($mPars['fPagamentUms']-$mFormaPagamentMembres['fPagamentUms']),2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly3'  readonly type='text' id='i_fPagamentEcosB_' name='i_fPagamentEcosB_' value='".(number_format(($mPars['fPagamentEcos']-$mFormaPagamentMembres['fPagamentEcos']),2,'.',''))."' size='8'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p><input class='i_readonly3'  readonly type='text' id='i_fPagamentEbB_' name='i_fPagamentEbB_' value='".(number_format(($mPars['fPagamentEb']-$mFormaPagamentMembres['fPagamentEb']),2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly3'  readonly type='text' id='i_fPagamentEuB_' name='i_fPagamentEuB_' value='".(number_format(($mPars['fPagamentEu']-$mFormaPagamentMembres['fPagamentEu']),2,'.',''))."' size='8'></p>
					</td>
					<td valign='bottom'>
					</td>
				</tr>

				<tr>
					<td valign='top'>
					</td>
					<td valign='top'>
					</td>
					<td valign='top'>
					<p>Compte CES: ".(html_ajuda1('html.php',33))."
					<br><input ";if($mParametres['precomandaTancada']['valor']  && $mPars['nivell']!='sadmin'  && $mPars['nivell']!='admin'){echo " READONLY ";} echo " type='text' id='i_fPagamentCompteEcos_' name='i_fPagamentCompteEcos_' value='".$mPars['compte_ecos']."' size='8'  onChange=\"javascript: recordatoriGuardarComanda();\">
					<br>
					[<input ";if($mParametres['precomandaTancada']['valor'] && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin'){echo " DISABLED ";} echo " type='checkBox' ";if ($mPars['fPagamentEcosIntegralCES']=='1'){echo " CHECKED ";} echo " onClick=\"recordatoriGuardarComanda();if(this.value==0){this.value=1;}else if(this.value==1){this.value=0;}\" id='ck_fPagamentEcosIntegralCES_' name='ck_fPagamentEcosIntegralCES_' value='".$mPars['fPagamentEcosIntegralCES']."'>IntegralCES]
					</p>
					</td>
					<td valign='top'  ".$moneda3ElementsStyle.">
					<p>Compte ".$mParametres['moneda3Abrev']['valor'].":<br><input ";if($mParametres['precomandaTancada']['valor']  && $mPars['nivell']!='sadmin'  && $mPars['nivell']!='admin'){echo " READONLY ";} echo " type='text' id='i_fPagamentCompteEb_' name='i_fPagamentCompteEb_' value='".$mPars['compte_cieb']."' size='".$mParametres['moneda3DigitsCodi']['valor']."' onChange=\"javascript: recordatoriGuardarComanda();\"></p>
					</td>
					<td valign='top'>
					</td>
					<td valign='top'>
					<div style='position:".$position."; visibility:".$visibility.";'>
					<p id='p_compte_cab_euros'>(Compte CAC Euros:<br>".$mParametres['compte_bancari_cac_1']['valor'].")</p>
					";
					if($mParametres['compte_bancari_cac_1']['valor']!='')
					{
						echo "
						<p><input ";if($mParametres['precomandaTancada']['valor'] && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin'){echo " DISABLED ";} echo " type='checkBox' ";if ($mPars['fPagamentEuMetalic']=='1'){echo " CHECKED ";} echo " onClick=\"javascript: checkbox_selectEUoTRANSF('ck_fPagamentEuMetalic_');\" id='ck_fPagamentEuMetalic_' name='ck_fPagamentEuMetalic_' value='".$mPars['fPagamentEuMetalic']."'>&#8364; en met�l.lic a l'entrega</p>
						<p><input ";if($mParametres['precomandaTancada']['valor'] && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin'){echo " DISABLED ";} echo " type='checkBox' ";if ($mPars['fPagamentEuTransf']=='1'){echo " CHECKED ";} echo " onClick=\"javascript: checkbox_selectEUoTRANSF('ck_fPagamentEuTransf_');\" id='ck_fPagamentEuTransf_' name='ck_fPagamentEuTransf_' value='".$mPars['fPagamentEuTransf']."'>&#8364; per transferencia</p>
						";
					}
					else
					{
						echo "
						<p><input CHECKED DISABLED id='ck_fPagamentEuMetalic_' name='ck_fPagamentEuMetalic_' value='1'>&#8364; en met�l.lic a l'entrega</p>
						<p><input type='hidden' style='position:absolute; z-index:0; top:0px; visibility:hidden;' DISABLED  id='ck_fPagamentEuTransf_' name='ck_fPagamentEuTransf_' value='".$mPars['fPagamentEuTransf']."'>&#8364; per transferencia</p>
						";
					}
					
					echo "
					</div>
					</td>
				</tr>
			</table>
			<div width='80%' align='center'>
			<p class='nota'>* No s'inclouen abonaments i c�rrecs. Els abonaments i c�rrecs de la CAC al Grup, i els del Grup als membres, nom�s s'inclouen en els respectius albarans</p>
			</div>
			</td>
		</tr>
	</table>
	";

return;
}

//------------------------------------------------------------------------------
function htmlForm_formaPagamentLlistaLocal()
{
	global 	$mAjuda,
			$mColors,
			$mComptesGrup,
			$mComptesUsuari,
			$mFormaPagamentMembres,
			$mParametres,
			$mRebost,
			$mPars,
			$mPropietatsGrup,
			$mPropietatsPeriodeLocal,
			$mPuntsEntrega,
			$mSelUsuari,
			$mUsuarisGrupRef,
			
			$pendentGuardarFormaPagamentIpuntEntrega,
			$haFetComanda,
			$teBases,
			$periodeBases,
			$numUsuarisGrupNoCom,
			$totalUtilitzatEcosRusuaris,
			$utilitzatAquiEcosR;



	echo "
	
	<table width='100%' valign='top' align='center' >
		<tr>
			<td width='100%' valign='top'>
			<table width='100%' align='center'>
				<tr>
					<td align='left'>
	";
	$text2="( individual, al GRUP )";
	if($mPars['selUsuariId']==0)
	{
		$text2="";
	}
	
	if($mParametres['moneda3Activa']['valor']==1)
	{
		$moneda3ElementsStyle=" style='visibility:inherit; z-index:0; position:relative;' ";
	}
	else
	{
		$moneda3ElementsStyle=" style='visibility:hidden; z-index:1;  position:absolute;' ";
	}
					
	echo "
					<p style='font-size:11px;'><b>Forma de Pagament:</b>
					<br>
					".$text2."</p>
					<p style='font-size:11px;' >
					Adre�a del punt d'entrega:<input style='font-size:11px;' id='i_apeLocal_' type='text' DISABLED size='40' value='".(urldecode($mPropietatsPeriodeLocal['apeLocal']))."'>
					<select id='sel_puntEntregaRef' name='sel_puntEntregaRef' style='visibility:hidden;' ><option value=''></option></select>
					</p>
					</td>
				</tr>
			</table>
				
			<table border='0' align='center' style='width:100%'>
				<tr>
					<td valign='top'>
					</td>
					<td valign='top'>
					<p>UMS(%):<br><input type='text' class='i_readonly' readonly id='i_fPagamentUmsPerc_' name='i_fPagamentUmsPerc_' value='100' size='5'></p>
					</td>
					<td valign='top'>
					<p>ecos(%):<br><input type='text' class='i_readonly'  readonly id='i_fPagamentEcosPerc_' name='i_fPagamentEcosPerc_' value='".(number_format($mPars['fPagamentEcosPerc'],2))."' size='5'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p>".$mParametres['moneda3Abrev']['valor']."(%):<br><input type='text' class='i_readonly'  readonly id='i_fPagamentEbPerc_' name='i_fPagamentEbPerc_' value='".(number_format($mPars['fPagamentEbPerc'],2))."' size='5'></p>
					</td>
					<td valign='top'>
					<p>euros(%):<br><input type='text' class='i_readonly'  id='i_fPagamentEuPerc_' readonly name='i_fPagamentEuPerc_' value='".(number_format($mPars['fPagamentEuPerc'],2))."' size='5'></p>
					</td>
					<td valign='bottom'>
	";
	if($mPars['selUsuariId']!=0)
	{
		$disabled='disabled';
		if
		(
			(
				$mPropietatsPeriodeLocal['comandesLocalsTancades']=='0' 
				&&  
				(
					$mPars['nivell']=='sadmin'  
					|| 
					$mPars['nivell']=='admin'	
					||
					$mPars['usuari_id']==$mRebost['usuari_id']
				)
			)
			||
			(
				$mPropietatsPeriodeLocal['comandesLocalsTancades']=='-1' 
			)
		)
		{
			$disabled='';
		}
		
	
		echo "
					<p>
					<table><tr><td><p>Convertir(ums):</p></td><td> ".(html_ajuda1('html.php',28))."</td></tr></table>
					<a title=\"Definir forma de pagament: Seleccionar una opci� de conversi� per passar unitats, decenes o centenes d'una moneda a l'altra\">
					<select ".$disabled." id='sel_fPagamentConversioMonedes_' onChange=\"fp_convertir(0,this.value);recordatoriGuardarComanda();\" name='sel_fPagamentConversioMonedes_'>
		";
	
		// ecos recolzats:
		$disabled7=''; //100% ecos
		$disabled11=''; //50n % ecos i 150% euros
		//si euros comanda>ecos recolzats usuari + ecox recolzats grup.usuari $disabled7='disabled'
	
		//if()si euros comanda>ecos recolzats usuari + ecos recolzats grup.usuari $disabled7='disabled'
	
		if($mParametres['moneda3Activa']['valor']==1)
		{
			$mSelected=array(1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>'',10=>'',11=>'',12=>'',13=>'');
			if(array_key_exists($mPars['fPagamentOpcioConversor'],$mSelected)){$mSelected[$mPars['fPagamentOpcioConversor']]=" selected class='o_selected' ";}else{$mSelected[$mPars['fPagamentOpcioConversor']]='';$mSelected['5']=" selected class='o_selected' ";}
			echo "
					<option ".$mSelected[1]." value='1'>ecos>>".$mParametres['moneda3Nom']['valor']."</option>
					<option ".$mSelected[2]." value='2'>ecos>>euros</option>
					<option ".$mSelected[3]." value='3'>".$mParametres['moneda3Nom']['valor'].">>ecos</option>
					<option ".$mSelected[4]." value='4'>".$mParametres['moneda3Nom']['valor'].">>euros</option>
					<option ".$mSelected[5]." value='5'>euros>>ecos</option>
					<option ".$mSelected[6]." value='6'>euros>>".$mParametres['moneda3Nom']['valor']."</option>
			";
			if($mPars['selRutaSufix']*1<$mParametres['periodeIniciRecolzamentSelectiu']['valor'])
			{
				echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>100 % ecos</option>
				";
			}
			else
			{
				echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>m�xim % ecos</option>
				";
			}
		
			echo "
					<option ".$mSelected[8]." value='8'>100% ".$mParametres['moneda3Nom']['valor']."</option>
					<option ".$mSelected[9]." value='9'>100% euros</option>
					<option ".$mSelected[10]." value='10'>50% ecos i 50% ".$mParametres['moneda3Nom']['valor']."</option>
					<option ".$mSelected[11]." ".$disabled11." value='11'>50% ecos i 50% euros</option>
					<option ".$mSelected[12]." value='12'>50% ".$mParametres['moneda3Nom']['valor']." i 50% euros</option>
					<option ".$mSelected[13]." value='13'>%ecos i % euros segons productors</option>
			";
		}
		else
		{
			$mSelected=array(2=>'',5=>'',7=>'',9=>'',11=>'',13=>'');
			if(array_key_exists($mPars['fPagamentOpcioConversor'],$mSelected)){$mSelected[$mPars['fPagamentOpcioConversor']]=" selected class='o_selected' ";}else{$mSelected[$mPars['fPagamentOpcioConversor']]=''; $mSelected['7']=" selected class='o_selected' ";}
			echo "
					<option ".$mSelected[2]." value='2'>ecos>>euros</option>
					<option ".$mSelected[5]." value='5'>euros>>ecos</option>
			";
			if($mPars['selRutaSufix']*1<@$mParametres['periodeIniciRecolzamentSelectiu']['valor'])
			{
				echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>100 % ecos</option>
				";
			}
			else
			{
				echo "
					<option ".$mSelected[7]." ".$disabled7." value='7'>m�xim % ecos</option>
				";
			}
		
			echo "
					<option ".$mSelected[9]." value='9'>100% euros</option>
					<option ".$mSelected[11]." ".$disabled11." value='11'>50% ecos i 50% euros</option>
					<option ".$mSelected[13]." value='13'>%ecos i % euros segons productors</option>
			";
		}
	
		echo "
	
					</select>
					</a>
					</p>
					<table>
						<tr>
							<td>
							<p><a title=\"clicar per convertir 1 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,1);recordatoriGuardarComanda()\" id='i_conversor1' name='i_conversor1' value='+'></a>(1 um)</p>
							<p><a title=\"clicar per convertir 1 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,4);recordatoriGuardarComanda()\" id='i_conversor4' name='i_conversor4' value='+'></a>(0.01 um)</p>
							</td>
							<td>
							<p><a title=\"clicar per convertir 10 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,2);recordatoriGuardarComanda()\" id='i_conversor2' name='i_conversor2' value='+'></a>(10 ums)</p>
							<p><a title=\"clicar per convertir 100 unitat d'una moneda a l'altra, segons la opci� triada m�s amunt\"><input type='button' ".$disabled." onClick=\"fp_convertir(1,3);recordatoriGuardarComanda()\" id='i_conversor3' name='i_conversor3' value='+'></a>(100 ums)</p>
							</td>
						</tr>
					</table>
		";					
	}
	else
	{
		echo "
		<input type='button' id='sel_fPagamentConversioMonedes_' value='' style='visibility:hidden;'>
		";
	}
	if($mPars['selUsuariId']==0)
	{
		$visibility='hidden';$position='absolute';
	}
	else
	{
		$visibility='inherit';$position='relative';
	}
	echo "
					</td>
				</tr>

				<tr  style='position:".$position."; visibility:".$visibility.";'>
					<td valign='bottom' align='left'>
					</td>
					<td valign='top'>
					<p>UMS:<br><input  class='i_readonly'  readonly type='text' id='i_fPagamentUms_' name='i_fPagamentUms_' value='".(@number_format($mPars['fPagamentUms'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top'>
					<p>ecos:<br><input class='i_readonly'  readonly type='text' id='i_fPagamentEcos_' name='i_fPagamentEcos_' value='".(@number_format($mPars['fPagamentEcos'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p>".$mParametres['moneda3Abrev']['valor'].":<br><input class='i_readonly'  readonly type='text' id='i_fPagamentEb_' name='i_fPagamentEb_' value='".(@number_format($mPars['fPagamentEb'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top'>
					<p>euros:<br><input class='i_readonly'  readonly type='text' id='i_fPagamentEu_' name='i_fPagamentEu_' value='".(@number_format($mPars['fPagamentEu'],2,'','.'))."' size='8'></p>
					</td>
					<td valign='top'>
					</td>
				</tr>
		";
	
	$visibility='hidden';
	$position='absolute';
	$visibility2='inherit';
	$position2='relative';
					
	if($mPars['selUsuariId']==0)
	{
		$visibility='inherit';$position='relative';
		$visibility2='hidden';$position2='absolute';
	}

	echo "
				<tr style='position:".$position."; visibility:".$visibility.";'>
					<td valign='top'>
					<p>Total pagaments membres al GRUP:</p>
					</td>
					<td valign='top'>
					<p><input  class='i_readonly2'  readonly type='text' id='i_fPagamentUmsG_' name='i_fPagamentUmsG_' value='".(@number_format($mFormaPagamentMembres['fPagamentUms'],2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly2'  readonly type='text' id='i_fPagamentEcosG_' name='i_fPagamentEcosG_' value='".(@number_format($mFormaPagamentMembres['fPagamentEcos'],2,'.',''))."' size='8'></p>
					</td>
					<td valign='top' ".$moneda3ElementsStyle.">
					<p><input class='i_readonly2'  readonly type='text' id='i_fPagamentEbG_' name='i_fPagamentEbG_' value='".(@number_format($mFormaPagamentMembres['fPagamentEb'],2,'.',''))."' size='8'></p>
					</td>
					<td valign='top'>
					<p><input class='i_readonly2'  readonly type='text' id='i_fPagamentEuG_' name='i_fPagamentEuG_' value='".(@number_format($mFormaPagamentMembres['fPagamentEu'],2,'.',''))."' size='8'></p>
					</td>
				</tr>
				";
				$readonly='';
				$disabled='';
				if
				(
					$mPropietatsPeriodeLocal['comandesLocalsTancades']==1
					&& 
					$mPars['nivell']!='sadmin'
					&& 
					$mPars['nivell']!='admin'
					&& 
					$mPars['usuari_id']!=$mRebost['usuari_id']
				)
				{
					$readonly=' READONLY ';
					$disabled=' DISABLED ';
				}
				
				echo "
				
				<tr style='position:".$position2."; visibility:".$visibility2.";'>
					<td valign='top'>
					</td>
					<td valign='top'>
					</td>
					<td valign='top'>
					<p>Compte CES: ".(html_ajuda1('html.php',33))."
					<br><input ".$readonly." type='text' id='i_fPagamentCompteEcos_' name='i_fPagamentCompteEcos_' value='".$mPars['compte_ecos']."' size='8'  onChange=\"javascript: recordatoriGuardarComanda();\">
					<br>
					[<input ".$disabled." type='checkBox' ";if ($mPars['fPagamentEcosIntegralCES']=='1'){echo " CHECKED ";} echo " onClick=\"recordatoriGuardarComanda();if(this.value==0){this.value=1;}else if(this.value==1){this.value=0;}\" id='ck_fPagamentEcosIntegralCES_' name='ck_fPagamentEcosIntegralCES_' value='".$mPars['fPagamentEcosIntegralCES']."'>IntegralCES]
					</p>
					</td>
					<td valign='top'  ".$moneda3ElementsStyle.">
					<p>Compte ".$mParametres['moneda3Abrev']['valor'].":<br><input ".$readonly." type='text' id='i_fPagamentCompteEb_' name='i_fPagamentCompteEb_' value='".$mPars['compte_cieb']."' size='".$mParametres['moneda3DigitsCodi']['valor']."' onChange=\"javascript: recordatoriGuardarComanda();\"></p>
					</td>
					<td valign='top'>
					</td>
					<td valign='top'>
					";
					if(isset($mPropietatsGrup['compte_grup_euros']) && $mPropietatsGrup['compte_grup_euros']!='' && $mPropietatsGrup['compte_grup_euros']!=0)
					{
						echo "	
					<div>
					<p>(Compte Grup Euros:<br>".$mPropietatsGrup['compte_grup_euros'].")</p>
					<p><input ";if($mPropietatsPeriodeLocal['comandesLocalsTancades']==1 && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin' && $mPars['grup_id']!=$mPars['usuari_id']){echo " DISABLED ";} echo " type='checkBox' ";if ($mPars['fPagamentEuMetalic']=='1'){echo " CHECKED ";} echo " onClick=\"javascript: checkbox_selectEUoTRANSF('ck_fPagamentEuMetalic_');\" id='ck_fPagamentEuMetalic_' name='ck_fPagamentEuMetalic_' value='".$mPars['fPagamentEuMetalic']."'>&#8364; en met�l.lic a l'entrega</p>
					<p><input ";if($mPropietatsPeriodeLocal['comandesLocalsTancades']==1 && $mPars['nivell']!='sadmin' && $mPars['nivell']!='admin' && $mPars['grup_id']!=$mPars['usuari_id']){echo " DISABLED ";} echo " type='checkBox' ";if ($mPars['fPagamentEuTransf']=='1'){echo " CHECKED ";} echo " type='checkBox' ";if ($mPars['fPagamentEuTransf']=='1'){echo " CHECKED ";} echo " onClick=\"javascript: checkbox_selectEUoTRANSF('ck_fPagamentEuTransf_');\" id='ck_fPagamentEuTransf_' name='ck_fPagamentEuTransf_' value='".$mPars['fPagamentEuTransf']."'>&#8364; per transferencia</p>
					</div>
						";
					}
					else
					{
						echo "
						<p><input type='checkbox' DISABLED style='z-index:0; top:0px; position:relative;' CHECKED id='ck_fPagamentEuMetalic_' name='ck_fPagamentEuMetalic_' value='1'>&#8364; en met�l.lic a l'entrega</p>
						<input type='hidden' style='z-index:0; top:0px; position:absolute; visibility:hidden;' id='ck_fPagamentEuTransf_' name='ck_fPagamentEuTransf_' value=''>
						";
					}
					echo "
					</td>
				</tr>

			</table>
			<div width='80%' align='center'>
			<p class='nota'>* Els abonaments i c�rrecs del Grup als membres, el cost de transport local i el marge per a despeses locals nom�s s'inclouen en els respectius albarans</p>
			</div>
			</td>
		</tr>
	</table>
	";

return;
}


//----------------------------------------
function mostrarDadesComandaMinima($db)
{
	global $mColors,$mRebost,$mRebosts,$mPars,$mParsCAC,$mostrarComandaMinima,$comandaMinima;
	
	echo "
<table  align='left' valign='top' style='width:100%;' bgcolor='".$mColors['table']."'>
	<tr>
		<td  valign='top' align='left' style='width:100%;'>
		<p  id='p_comandaMinima' style='color:#bb4400; font-size:11px;'>
		<i>
	";	
	if($mostrarComandaMinima && $mPars['grup_id']!='0' && (substr_count($mRebost['categoria'],'1-rebost,')>0))
	{
		echo "
		Comanda M�nima per incloure
		<br>
		el Rebost a la ruta:<b>".$comandaMinima." kg</b> 
			";
	}
	echo "
		</i>
		</p>
		</td>
	</tr>
</table>
	";

	return;
}

//-------------------------------------------------------
function html_paginacio($index)
{
	global 	$mColors,
			$mAjuda,
			$mPars,
			$mProductes,
			$mParametres,
			$mGrup,
			$mPropietatsGrup,
			$mPropietatsPeriodeLocal;

	echo "
	<table  align='center' bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:50px;' align='center'>
			";
	$botoText="guardar pre-comanda";
	if
	(
		$mPars['grup_id']!=0
		&&
		(
			$mPars['selLlistaId']==0
			&&
			$mPars['nivell']!='visitant'
			&& 
			(
				$mPars['nivell']=='admin' || $mPars['nivell']=='sadmin'
				|| 
				(
					!$mParametres['precomandaTancada']['valor']
					&&
					(
						(
							$mPars['selUsuariId']!='0' 
							&&
							$mPars['selUsuariId']==$mPars['usuari_id']
						)
						||
						(
							$mPars['selUsuariId']!='0' 
							&&
							$mPars['usuari_id']==$mGrup['usuari_id']
						)
						||
						(
							$mPars['selUsuariId']=='0'
							&&
							$mPars['usuari_id']==$mGrup['usuari_id']
						)
					)
				)
			)
		)
		||
		(
			$mPars['selLlistaId']!=0
			&&
			$mPars['nivell']!='visitant'
			&& 
			$mPars['selUsuariId']!='0'
			&&
			(
				$mPars['nivell']=='admin' || $mPars['nivell']=='sadmin'
				||
				$mPars['usuari_id']==$mGrup['usuari_id']
			)
		)
		||
		(
			$mPropietatsPeriodeLocal['comandesLocalsTancades']==-1
		)
	)
	{
		$botoText='guardar comanda';
		if($mPars['grup_id']==0)
		{
			$botoText='guardar comanda';
		}
		else if($mPars['selUsuariId']==0)
		{	
			$botoText='guardar forma de pagament';
		}
		
		echo "	
				<table>
					<tr>
						<td align='center'>
						<table>	
							<tr>
								<td align='center'>
								<p>
		";
		if
		(
			(
				$mPars['selLlistaId']==0
				&&
				(
					$mPars['nivell']=='sadmin' 
					|| 
					$mPars['nivell']=='admin' 
					||
					$mParametres['precomandaTancada']['valor']
				)
			)
			||
			(
				$mPars['selLlistaId']!=0
				&&
				(
					$mPars['nivell']=='sadmin' 
					|| 
					$mPars['nivell']=='admin' 
					||
					$mGrup['usuari_id']=$mPars['usuari_id']
					||
					$mPropietatsPeriodeLocal['comandesLocalsTancades']==-1
				)
			)
		)
		{
			if($mGrup['usuari_id']==$mPars['usuari_id'])
			{
				echo "			<font style='color:#FC7202;'>[RG]</font>";
			}
			else
			{
				echo "			<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";
			}
		}
		echo "
								</p>
								</td>
							</tr>
						</table>
						</td>
						<td>
						<table id='t_img_guardar_".$index."'>
							<tr>
								<td align='center'>
								<p id='p_img_guardar_".$index."' style='text-decoration:blink; color:red;'></p>
								<a title='".$botoText."'><img src='imatges/guardarpp.gif' style='cursor:pointer;' onClick=\"guardarComanda();\" value='".$botoText."'></a></p>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
		";
	}
	else
	{
		echo "
						<table id='t_img_guardar_".$index."' style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
							<tr>
								<td align='center'>
								<p id='p_img_guardar_".$index."'></p>
								</td>
							</tr>
						</table>
		";
	}
	echo "
			</td>
			<td>
			<p>p�gina:</p>
			</td>
			<td> 
			<img src='imatges/page_downp.gif' alt='seleccionar p�gina anterior' style='cursor:pointer;' onClick=\"javascript:avansarPag(-1);\">
			</td>
			<td>
			<select onChange=\"javascript:selectPag(this.value)\";>
	";
	$selected='';
	for($i=1;$i<=$mPars['numPags'];$i++)
	{
		if($mPars['pagS']*1==$i-1)
		{
			$selected='selected';
		}
		else
		{
			$selected='';
		}
		echo "
			<option ".$selected." value='".($i-1)."'>".$i."</option>
		";
	}	
	
	echo "
			</select>
			</td>
			<td>
			<img src='imatges/page_upp.gif' alt='seleccionar p�gina seg�ent' style='cursor:pointer;' onClick=\"javascript:avansarPag(1);\">
			</td>
			<td>
			<p>items/pag:<input type='text' size='2'  id='i_itemsPag_'  onChange=\"javascript: saveItemsPag(this.value);\" value='".$mPars['numItemsPag']."'></p>
			</td>
			<td> 
			".(html_ajuda1('html.php',30))."
			</td>
		</tr>
	</table>
	";


	return;
}

//-------------------------------------------------------
function mostrarFormProductes()
{
	global  $db,
			$login,
			$mode,
			$pendentGuardarFormaPagamentIpuntEntrega,
			$ruta0,
			$strRuta,
			$thumbCode,
			$noReservaNegatiu,
			$valorMaxSegell,
			$mColsProductes,
			$mColors,
			$colorLlista, 
			$mAjuda,
			$mComanda,
			$mComandaCopiar,
			$mEtiquetes,
			$mFormesPagament,
			$mGrup,
			$mIntercanvisRebost,
			$mIntercanvisRebosts,
			$mInventariG,
			$mNomsColumnes,
			$mParametres,
			$mPars, 
			$mParsCAC, 
			$mParsCTAR, 
			$mProductes, 
			$mProductors,
			$mPropietatsPeriodeLocal,
			$mPropietatsPeriodesLocals,
			$mPuntsEntrega,
			$mRebost,
			$mVocalsAccentReplace,
			$mVocalsAccentSearch,
			$mTotalKgComandaCac,
			$mPropietatsGrup,
			$mPerfilsRef;
			
	//--------------------------------------------------------------------------
			
		//Tancament Reserves Productes JJAA
		$date = new DateTime();
		$t1=$date->getTimestamp();

		$mT2['Y']=date('Y',strtotime($mParametres['dataTancamentReservesProductesJJAA']['valor']));
		$mT2['m']=date('m',strtotime($mParametres['dataTancamentReservesProductesJJAA']['valor']));
		$mT2['d']=date('d',strtotime($mParametres['dataTancamentReservesProductesJJAA']['valor']));
		$dataTancamentReservesProductesJJAA = new DateTime();
		$dataTancamentReservesProductesJJAA->setDate($mT2['Y'], $mT2['m'], $mT2['d'] );
		$dataTancamentReservesProductesJJAA->setTime(0, 0, 0 );

		$t2=$dataTancamentReservesProductesJJAA->getTimestamp();
		
	//--------------------------------------------------------------------------

		

	$rebostTmp=str_replace(array('"',"'",' '),'',urlDecode($mRebost['nom']));
	$rebostTmp=str_replace($mVocalsAccentSearch,$mVocalsAccentReplace,$rebostTmp);
	$periodeComandaTmp=str_replace(array('-',":"),'',$mParametres['periodeComanda']['valor']);
	
	//--------------------------------------------------------
	// comanda en curs ---------------------------------------
	// totals

	echo "
	<table id='t_principal' align='center'  style=' width:100%;' bgcolor='".$mColors['table']."'>
		<tr>
			<td style=' width:95%;'  valign='top'>
			<table  align='center'  style=' width:100%;'>
				<tr>
					<td style='width:100%;'>
			";
			if($mPars['selLlistaId']==0)
			{
				mostrarDadesRebost($db);				
			}
			else
			{
				mostrarDadesRebostGrup($db);				
			}
			echo "
					<table bgcolor='".$mColors['comandes.php'][$colorLlista]['t3_bg']."' border='0' style='width:100%'>
						<tr>
			";
			if($mPars['grup_id']!='0')
			{
					echo "
							<td  style='width:70%'>
					";
					if
					(
						$mPars['selLlistaId']==0  
					)
					{
						htmlForm_formaPagament();
						echo "
							</td>
							<td bgcolor='#A4D781' style='width:30%'>
						";
						taulaResumComanda();	
					}
					else
					{
						htmlForm_formaPagamentLlistaLocal();
						echo "
							</td>
							<td bgcolor='#A4D781' style='width:30%'>
						";
						taulaResumComandaLocal();	
					}
					
					echo "
							</td>
					";
			}
			else  //CAC
			{
					echo "
							<td bgcolor='#A4D781' style='width:100%' align='center'>
							<table border='0' width='100%'>
								<tr>
									<td width='25%' align='right' valign='top'>
									</td>
									<td  width='50%' align='center'>
					";
					taulaResumComanda();
					echo "
					<input type='hidden' id='sel_puntEntregaRef' value=''>
					";
					echo "
									</td>
									<td width='25%' align='left' valign='top'>
									</td>
								</tr>
							</table>
							</td>
					";
			}
			echo "
						</tr>
					</table>
					</td>
				</tr>
			</table>
			<center><div id='d_noEstocNegatiu'></div></center>
			</td>
		</tr>

		<tr>
			<td style=' width:100%;' valign='top'>
			<table  border='0' bgcolor='#9EeC7B' style=' width:100%;'>
				<tr>
					<td width='100%' valign='top' align='center'>
				";
				taulaEinesGrups();
				echo "
					</td>
				</tr>
			</table>
			</td>
		</tr>

		<tr>
			<td style=' width:100%;'>
			<center>
		<div style=' width:100%;'>
		<table style=' width:100%;'>
			<tr>
				<td style=' width:33%;'>
			";
			html_paginacio(1);
			echo "
				</td>
				<td  style='width:33%;'>
				<p style='font-size:11px;'>(* <i>productes ordenats per <b>".$mPars['sortBy']."</b> - ".$mPars['ascdesc']."</i>)</p>
				</td>
				<td  style='width:33%;'>
				</td>
			</tr>
		</table>
		<table border='1'  align='center' valign='top' style=' width:100%;'>
	";

	if(count($mProductes)>0 && $mProductes[0]!=NULL)
	{
	
	// nom columnes:--------------------------------

	$mColumnesVisibles=array();
	$classTitolColumnes='titolColumnesCac';
	if($mPars['selLlistaId']==0)
	{
		if($mPars['grup_id']=='0')
		{
			$mColumnesVisibles=$mColsProductes['visiblesCAC']; 
		}
		else
		{
			$mColumnesVisibles=$mColsProductes['visiblesRebosts'];
		}
	}
	else
	{
		$mColumnesVisibles=$mColsProductes['visiblesLocal']; 
	}
	echo "
			<tr>
	";
	while(list($key,$val)=each($mProductes[0]))
	{	
		if(in_array($key,$mColumnesVisibles))
		{
			echo "
				<td bgcolor='#AEFC8B' valign='bottom'>
				<table  align='center' valign='bottom'>
					<tr>
						<th  align='center' valign='bottom'>
						";
						if($key=='id')
						{
							echo " 
						
						<p class='".$classTitolColumnes."'><b>".$mNomsColumnes[$key]."</b></p>
						".(html_ajuda1('html.php',31));
						}
						else if($key=='segell')
						{
							echo " 
						
						<p class='".$classTitolColumnes."'><b>".$mNomsColumnes[$key]."</b></p>
							";
							if($mPars['selLlistaId']==0)
							{
								if($mParametres['aplicarValorTallSegell']['valor']*1==1) //aplicar valortallsegell si est� actiu
								{
									echo "
						<p class='p_micro'>Min:".$mParametres['valorTallSegell']['valor']."%</p>
									";
								}
							}
							echo html_ajuda1('html.php',32);
						}
						else
						{
							echo "
						<p class='".$classTitolColumnes."'>".$mNomsColumnes[$key]."</p>
							";
						}
						echo "

						<table align='center' valign='bottom'>
							<tr>
								<td >
								<p style='cursor:pointer;' onClick=\"sortBy('".$key."','DESC');\"><b>&darr;</b></p>
								</td>
								<td>
								<p style='cursor:pointer;' onClick=\"sortBy('".$key."','ASC');\"><b>&uarr;</b></p>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				";
			}
		}
		reset($mProductes[0]);
		echo "
				<th bgcolor='#AEFC8B'  align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Quant.</p><br>
				</th>
		";
		if($mPars['grup_id']==0)
		{
		echo "
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:5%;'>
				<a title=\"".(str_replace('<br>','
',$mInventariG['data']))."\" style='cursor:pointer;'><p class='".$classTitolColumnes."'>Inv.</p></a>
				</th>
		";
		}
		echo "
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				";
				if($mPars['selLlistaId']==0)
				{
					echo "
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(ums)</i></p>
					";
				}
				else
				{
					echo "
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(".$mPropietatsPeriodeLocal['ctikLocal']." ums/kg)</i></p>
					";
				}
				echo "
				</i></p>
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				";
				if($mPars['selLlistaId']==0)
				{
					echo "
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(ecos)</i></p>
					";
				}
				else
				{
					echo "
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(".$mPropietatsPeriodeLocal['ms_ctikLocal']." % ecos)</i></p>
					";
				}
				echo "
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(euros)</i></p>
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Preu (ums)</p><br>&nbsp;
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Preu (ecos)</p><br>&nbsp;
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Preu (euros)</p><br>&nbsp;
				</th>
			</tr>
		";		
		
	// productes: -------------------------------------

	$contLinies=0;
	$blocLinies=6;
	$linies=1;
	while (list($key0,$val0)=each($mProductes))
	{	
		$perfilId=substr($mProductes[$key0]['productor'],0,strpos($mProductes[$key0]['productor'],'-'));
		if
		(
			(
				$mPars['selLlistaId']==0
				&&
				$mProductes[$key0]['visible']==1
			)
			||
			(
				$mPars['selLlistaId']!=0
				&&
				$mProductes[$key0]['visible']==1
				&&
				(
					$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['comandesLocalsTancades']==1
					||
					substr_count(' '.$mPropietatsGrup['idsPerfilsActiusLocal'],','.$perfilId.',')>0
				)
			)
		)
		{
			//recordatori de noms de columna:
			if($contLinies==$blocLinies)
			{
				$contLinies=0;	
				echo "
			<tr>
				";
			
				while(list($key,$val)=each($mProductes[0]))
				{	
					if(in_array($key,$mColumnesVisibles))
					{
						echo "
				<th  bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>".$mNomsColumnes[$key]."</i></p>
				</th>
						";
					}
				}
				reset($mProductes[0]);

				echo "
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>Quantitat</i></p>
				</th>
				";
				if($mPars['grup_id']==0)
				{
					echo "
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Inv.</p><br>
				</th>
					";
				}
				echo "
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				";
				if($mPars['selLlistaId']!=0)
				{
					echo "
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(ums)</i></p>
					";
				}
				else
				{
					echo "
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(".$mPropietatsPeriodeLocal['ctikLocal']." ums/kg)</i></p>
					";
				}
				echo "
				</i></p>
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				";
				if($mPars['selLlistaId']!=0)
				{
					echo "
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(ecos)</i></p>
					";
				}
				else
				{
					echo "
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(".$mPropietatsPeriodeLocal['ms_ctikLocal']." % ecos)</i></p>
					";
				}
				echo "
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>Total<br>Cost<br>Transp<br>(euros)</i></p>
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>Preu (ums)</i></p>
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>Preu (ecos)</i></p>
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>Preu (euros)</i></p>
				</th>
			</tr>
				";		
			}

			if($mPars['grup_id']=='0' && ($mProductes[$key0]['estoc_disponible']>$mProductes[$key0]['estoc_previst'] || $mProductes[$key0]['estoc_disponible']<0))
			{
				$colorAlerta='#ff0000';
			}
			else
			{
				$colorAlerta='#000000';
			}
			echo "
			<tr id='tr_filaProducte_".$key0."'>
			";
			while(list($key,$val)=each($mProductes[0]))
			{	
				if(in_array($key,$mColumnesVisibles))
				{
					echo "
				<td  id='td_producte".$key."_".$key0."' style='width:5%;'>
					";
				
					if($key=='segell')
					{
					echo " 
				<table>
					<tr>
						<td>
						<p class='".$classTitolColumnes."' style='color:".$colorAlerta.";'>
						<b>".(number_format($mProductes[$key0][$key],1,'.',''))."&nbsp;%</b>
						</p>
						</td>
					</tr>
				</table>
					";
					}
					else if($key=='estoc_previst')
					{
					echo " 
				<table>
					<tr>
						<td>
						<p class='".$classTitolColumnes."' style='color:".$colorAlerta.";'>
						<b>".$mProductes[$key0][$key]."</b>
						</p>
						</td>
					</tr>
				</table>
					";
					}
					else if($key=='estoc_disponible')
					{
						echo " 
				<table>
					<tr>
						<td>
						<p class='".$classTitolColumnes."' style='color:".$colorAlerta.";'>
						".$mProductes[$key0][$key]."
						</p>
						</td>
					</tr>
				</table>
						";
					}
					else if($key=='producte')
					{
						echo " 
				<div  style='position:relative; z-index:0; top:0px; left:0px;'>
				<div style='position:relative; z-index:0;'>
				<table style='width:100%;'>
					<tr>
						<td style='width:10%;'>
						<table style='width:100%;'>
							<tr>
								<td align='left' style='width:100%;'>	
								<p class='".$classTitolColumnes."' style='cursor:pointer; color:#3E4B8D;' onClick=\"javascript:cursor=getMouse(event); mostrarTaulaFlotant(cursor.x,cursor.y,'".$mProductes[$key0]['id']."','".$key."','".$key0."',1);\" >[<u>info</u>]
								</p>
								<p class='".$classTitolColumnes."'>								
								".(urldecode(marcarRecerca($mProductes[$key0][$key])))."
								</p>
						";
						if($mProductes[$key0]['imatge']!='')
						{
							if($mPars['selLlistaId']==0)
							{
								echo "
								<img src='productes/".$mProductes[$key0]['id']."/".$thumbCode."_".$mProductes[$key0]['imatge']."'>
								";
							}
							else
							{
								echo "
								<img src='llistes/".$mPars['selLlistaId']."/productes/".$mProductes[$key0]['id']."/".$thumbCode."_".$mProductes[$key0]['imatge']."'>
								";
							}
						}
						echo "
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</div>
				<div  id='d_producte".$key."_".$key0."'  style='position:absolute; z-index:1; top:-30px; left:150px; width:300px; visibility:hidden;'>
				</div>
				</div>
						";
					} 
//*v36-15-11-26 2 else
					else if($key=='productor')
					{
						$perfilId=substr($mProductes[$key0][$key],0,strpos($mProductes[$key0][$key],'-'));
						
						echo " 
				<table >
					<tr>
						<td >
						<p   class='".$classTitolColumnes."'>
						".(urldecode(marcarRecerca($mPerfilsRef[$perfilId]['projecte'])))."
						</p>
						</td>
					</tr>
				</table>
						";
					} 
					else if($key=='categoria0' || $key=='categoria10' )
					{
						echo " 
				<table >
					<tr>
						<td >
						<p   class='".$classTitolColumnes."'>
						".(urldecode(marcarRecerca($mProductes[$key0][$key])))."
						</p>
						</td>
					</tr>
				</table>
						";
					} 
					else if($key=='tipus')
					{
						echo " 
				<table >
					<tr>
						<td >
						<p   class='".$classTitolColumnes."'>
						".(posaColor1(urldecode($mProductes[$key0][$key])))."
						</p>
						</td>
					</tr>
				</table>
						";
					} 
					else if($key=='cost_transport_extern_kg')
					{
						echo " 
				<table >
					<tr>
						<td >
						<p   class='".$classTitolColumnes."'>
						".$mProductes[$key0][$key]."
						<br>
						(".$mProductes[$key0]['ms_ctek']."% ms)
						</p>
						</td>
					</tr>
				</table>
						";
					} 
					else if($key=='cost_transport_intern_kg' )
					{
						echo " 
				<table >
					<tr>
						<td >
						<p   class='".$classTitolColumnes."'>
						".$mProductes[$key0][$key]."
						<br>
						(".$mProductes[$key0]['ms_ctik']."% ms)
						</p>
						</td>
					</tr>
				</table>
						";
					} 
					else if($key=='id' )
					{
						if($mProductes[$key0]['actiu']=='0'){$actiuText="<p class='pVinactiu'>INACTIU</p>";}else{$actiuText='';}
						echo " 
				<table>
					<tr>
						<td>
						<p class='".$classTitolColumnes."'>".(urldecode($mProductes[$key0][$key]))."</p>
						</td>
						<td>
						".$actiuText."
						</td>
					</tr>
				</table>
						";
					}
					else
					{
						echo " 
				<table>
					<tr>
						<td>
						<p class='".$classTitolColumnes."'>".(urldecode($mProductes[$key0][$key]))."</p>
						</td>
					</tr>
				</table>
						";
					}
					echo "
				</div>
				</td>
					";
				}
			}
			reset($mProductes[0]);  

			if($mProductes[$key0]['estoc_previst']!=0){$limitq=200;}else{$limitq=0;}
		
			if($mPars['veureProductesDisponibles']==1) //veure nom�s els productes disponible
			{	
				if($mPars['grup_id']!='0')
				{
					if
					(
						(
							substr_count($mProductes[$key0]['tipus'], 'especial')==0
						) 
						&&
						(
							$mProductes[$key0]['preu']=='' 
							|| 
							$mProductes[$key0]['actiu']==0
						)
					)
					{
					
						echo "
				<td    id='td_producteQuantitat_".$key0."' align='left'>
				<p  class='".$classTitolColumnes."'><i>No disponible</i></p>
				</td>
						";
					}
					else if(substr_count($mProductes[$key0]['tipus'], 'especial')>0)
					{
						echo "
				<td    id='td_producteQuantitat_".$key0."' align='left'>
						"; 
						if(!$mParametres['precomandaTancada']['valor'] && $mPars['selUsuariId']!=0)
						{
							echo " 
				<p  style='color:#000000; cursor:pointer;' onClick=\"enviarFpars('productesEspecials1.php?pei=".$mProductes[$key0]['id']."','_blank');\"'><u>reservar</u></p>
							";
						}
					echo "
				<br>
				<p class='".$classTitolColumnes."'> (reserves:<b>".$mProductes[$key0]['quantitat']."</b>) </p>
				</td>
						";
					}
					else
					{
						echo "
				<td   id='td_producteQuantitat_".$key0."'>
				<p  class='".$classTitolColumnes."'>
				<select "; 
						if
						(
							(
								$mPars['selLlistaId']==0
								&&
								(
									$mPars['nivell']=='visitant'
									||
									$mPars['selUsuariId']==0
									||
									(
										$mParametres['precomandaTancada']['valor']
										&&
										$mPars['nivell']!='admin'
										&&
										$mPars['nivell']!='sadmin'
						 			)
									||
									(
										!$mParametres['precomandaTancada']['valor']
										&&
										(
											(
												$mPars['selUsuariId']!=$mPars['usuari_id']
												&&
												$mGrup['usuari_id']!=$mPars['usuari_id']
												&&
												$mPars['nivell']!='admin'
												&&
												$mPars['nivell']!='sadmin'
											)
											||
											(
												substr_count($mProductes[$key0]['tipus'], 'jjaa')>0
												&&
												$t1>$t2
											)
										)
									)
								)
							)
							||
							(
								$mPars['selLlistaId']!=0
								&&
								(
									isset($mPropietatsPeriodeLocal['comandesLocalsTancades']) && $mPropietatsPeriodeLocal['comandesLocalsTancades']==1
									||
									$mPars['nivell']=='visitant'
									|| 
									$mPars['selUsuariId']=='0'
									||
									(
										$mPars['nivell']!='admin' && $mPars['nivell']!='sadmin'
										&&
										$mPars['usuari_id']!=$mGrup['usuari_id']
										&&
										(
											$mPropietatsPeriodeLocal['comandesLocalsTancades']==0
										)
									)
								)
							)
						)
						{
							echo " 
						disabled 
							";
						} 
						echo " 
					onChange=\"selectProducte('".$key0."');\" id='sel_quantitat_".$key0."' name='sel_quantitat_".$key0."'>
						";

					//no es poden reservar productes amb estoc_disponible=0 o en negatiu abans de limitTempsReservesNegatiu dies de tancament de precomanda
					
						if
						(
							!$noReservaNegatiu
							&&
							substr_count(urldecode($mProductes[$key0]['tipus']), 'dip�sit')==0
							&&
							substr_count($mProductes[$key0]['tipus'], 'dip�sit')==0
						) //es poden fer reserves en negatiu excepte en els productes en dip�sit
						{
							for($j=0;$j<=($mProductes[$key0]['estoc_previst']*2);$j+=$mProductes[$key0]['format'])
							{
								if($mProductes[$key0]['quantitat']==$j){$selected='selected';}else{$selected='';}
								echo "
				<option ".$selected." value='".$j."'>".$j."</option>
							";
							}
					echo "
				</select>
						";
						}
						else	//temps superat; no es poden fer reserves en negatiu o son productes diposit
						{
							for($j=0;$j<=$mProductes[$key0]['quantitat']+$mProductes[$key0]['estoc_disponible'];$j+=$mProductes[$key0]['format'])
							{
								if($mProductes[$key0]['quantitat']==$j){$selected='selected';}else{$selected='';}
								echo "
				<option ".$selected." value='".$j."'>".$j."</option>
							";
							}
							echo "
				</select>
				<script>noEstocNegatiu=1; diesLimitReservesNegatiu=".$mParametres['diesLimitReservesNegatiu']['valor'].";</script>
							";
						}
						echo "
				<br>(".(urldecode($mProductes[$key0]['unitat_facturacio'])).")</p>
				</td>
						";
					}
				}
				else 			//CAC
				{
					if((substr_count($mProductes[$key0]['tipus'], 'especial')==0) && ($mProductes[$key0]['preu']==''  || $mProductes[$key0]['actiu']==0))
					{
						echo "
				<td    id='td_producteQuantitat_".$key0."' align='left'>
				<p   class='".$classTitolColumnes."'><i>No disponible</i></p>
				</td>
						";
					}
					else if(substr_count($mProductes[$key0]['tipus'], 'especial')>0)
					{
						echo "
				<td    id='td_producteQuantitat_".$key0."' align='left'>
				</td>
						";
					}
					else
					{
						echo "
				<td   id='td_producteQuantitat_".$key0."'>
				<p  class='".$classTitolColumnes."'>
				<input size='6' type='text' DISABLED  id='sel_quantitat_".$key0."' name='sel_quantitat_".$key0."' value='".($mProductes[$key0]['quantitat'])."'>
				<br>(".(urldecode($mProductes[$key0]['unitat_facturacio'])).")</p>
				</td>
						";
				
				//// columna inventari (nom�s cac) ////
				
						if(@substr_count($mProductes[$key0]['tipus'],'especial')==0)
						{
							$difP=@$mInventariG['inventariRef'][$mProductes[$key0]['id']]-@$mProductes[$key0]['estoc_previst'];
							$tdColor='#ffffff';
							$difColor='#000000';

							if($difP<0)
							{
								if((@substr_count($mProductes[$key0]['tipus'],'dip�sit')>0 || @substr_count(urldecode($mProductes[$key0]['tipus'],'dip�sit'))>0)){$tdColor='#F9DAD6';}
								else{$tdColor='#FA897A';}
							}
							else if($difP==0){$difColor='#00aa00';}
							else //$dif>0
							{
								$difColor='#0000aa';
								if(
								@substr_count($mProductes[$key0]['tipus'],'dip�sit')>0
								 || 
								@substr_count(urldecode($mProductes[$key0]['tipus']),'dip�sit')>0
								)
								{$tdColor='#A5E7F6';}
								else{$tdColor='#68DFF9';}
							}

							echo "
				<td   id='td_producteInventari_".$key0."' style='background-color:".$tdColor.";'>
							";
				
							if(!@isset($mInventariG['inventariRef'][$mProductes[$key0]['id']]) || @$mInventariG['inventariRef'][$mProductes[$key0]['id']]=='')
							{
								@$mInventariG['inventariRef'][$mProductes[$key0]['id']]=0;
							}
							echo "
				<p  class='".$classTitolColumnes."'>".@$mInventariG['inventariRef'][$mProductes[$key0]['id']]."
				<br>
				<font id='f_difP_".$key0."' style='font-size:11px; color:#000000;'>difP:</font><font style='color:".$difColor.";'><b>".$difP."</b></font></p>
				</td>
							";
						}
						else //especial
						{
							echo "
						<td   id='td_producteInventari_".$key0."'></td>
							";
						}
					}
				}
			}
			else	// veure tots els productes
			{
				if($mPars['grup_id']!='0')
				{
					if((substr_count($mProductes[$key0]['tipus'], 'especial')==0) && ($mProductes[$key0]['preu']=='' || $mProductes[$key0]['actiu']==0))
					{
						echo "
				<td    id='td_producteQuantitat_".$key0."' align='left'>
				<p  class='".$classTitolColumnes."'><i>No disponible</i></p>
				</td>
						";
					}
					else if(substr_count($mProductes[$key0]['tipus'], 'especial')>0)
					{
						echo "
				<td    id='td_producteQuantitat_".$key0."' align='left'>
						";
						if($mPars['selUsuariId']!=0)
						{
							echo "
				<p  style='color:#000000; cursor:pointer;' onClick=\"enviarFpars('productesEspecials1.php?pei=".$mProductes[$key0]['id']."','_blank');\"'>reservar</p>
				<br>
							";
						}
						echo "
				<p class='".$classTitolColumnes."'> (reserves:<b>".$mProductes[$key0]['quantitat']."</b>) </p>
				</td>
						";
					}
					else
					{
						echo "
				<td   id='td_producteQuantitat_".$key0."'>
				<p  class='".$classTitolColumnes."'>
				<select 
					"; 
						if
						(
							(
								$mPars['selLlistaId']==0
								&&
								(
									$mPars['nivell']=='visitant'
									||
									$mPars['selUsuariId']==0
									||
									(
										$mParametres['precomandaTancada']['valor']
										&&
										$mPars['nivell']!='admin'
										&&
										$mPars['nivell']!='sadmin'
						 			)
									||
									(
										!$mParametres['precomandaTancada']['valor']
										&&
										(
											(
												$mPars['selUsuariId']!=$mPars['usuari_id']
												&&
												$mGrup['usuari_id']!=$mPars['usuari_id']
												&&
												$mPars['nivell']!='admin'
												&&
												$mPars['nivell']!='sadmin'
											)
											||
											(
												substr_count($mProductes[$key0]['tipus'], 'jjaa')>0
												&&
												$t1>$t2
											)
										)
									)
								)
							)
							||
							(
								$mPars['selLlistaId']!=0
								&&
								(
									$mPars['nivell']=='visitant'
									|| 
									$mPars['selUsuariId']=='0'
									||
									(
										$mPars['nivell']!='admin' && $mPars['nivell']!='sadmin'
										&&
										$mPars['usuari_id']!=$mGrup['usuari_id']
										&&
										$mPropietatsPeriodeLocal['comandesLocalsTancades']==1
									)
								)
							)
						)
					{
						echo " 
					disabled 
						";
					} 
						echo " 
					onChange=\"selectProducte('".$key0."');\" id='sel_quantitat_".$key0."' name='sel_quantitat_".$key0."'>
						";

						//no es poden reservar productes amb estoc_disponible=0 o en negatiu abans de limitTempsReservesNegatiu dies de tancament de precomanda
					
						if(!$noReservaNegatiu) //es poden fer reserves en negatiu
						{
							for($j=0;$j<=$mParametres['limitUtsSelectorProductes']['valor'];$j+=$mProductes[$key0]['format'])
							{
							if($mProductes[$key0]['quantitat']==$j){$selected='selected';}else{$selected='';}
							echo "
				<option ".$selected." value='".$j."'>".$j."</option>
							";
							}
						echo "
				</select>
						";
						}
						else	//temps superat; no es poden fer reserves en negatiu
						{
							for($j=0;$j<=$mProductes[$key0]['quantitat']+$mProductes[$key0]['estoc_disponible'];$j+=$mProductes[$key0]['format'])
							{
								if($mProductes[$key0]['quantitat']==$j){$selected='selected';}else{$selected='';}
								echo "
				<option ".$selected." value='".$j."'>".$j."</option>
								";
							}
							echo "
				</select>
							";
						}
					
						echo "
				<br>(".(urldecode($mProductes[$key0]['unitat_facturacio'])).")</p>
				</td>
						";
					}
				}
				else 			//CAC
				{
					if((substr_count($mProductes[$key0]['tipus'], 'especial')==0) && ($mProductes[$key0]['preu']=='' ))
					{
						echo "
				<td    id='td_producteQuantitat_".$key0."' align='left'>
				<p   class='".$classTitolColumnes."'><i>No disponible</i></p>
				</td>
						";
					}
					else if(substr_count($mProductes[$key0]['tipus'], 'especial')>0)
					{
						echo "
				<td    id='td_producteQuantitat_".$key0."' align='left'>
				</td>
						";
					}
					else
					{
						echo "
				<td   id='td_producteQuantitat_".$key0."'>
				<p  class='".$classTitolColumnes."'>
				<input size='6' type='text' DISABLED  id='sel_quantitat_".$key0."' name='sel_quantitat_".$key0."' value='".($mProductes[$key0]['quantitat'])."'>
				<br>(".(urldecode($mProductes[$key0]['unitat_facturacio'])).")</p>
				</td>
						";
				
						//// columna inventari (nom�s cac) ////
				
						if(@substr_count($mProductes[$key0]['tipus'],'especial')==0)
						{
							$difP=@$mInventariG['inventariRef'][$mProductes[$key0]['id']]-@$mProductes[$key0]['estoc_previst'];
							$tdColor='#ffffff';
							$difColor='#000000';

							if($difP<0)
							{
								if((@substr_count($mProductes[$key0]['tipus'],'dip�sit') || @substr_count(urldecode($mProductes[$key0]['tipus']),'dip�sit'))>0){$tdColor='#F9DAD6';}
								else{$tdColor='#FA897A';}
							}
							else if($difP==0){$difColor='#00aa00';}
							else //$dif>0
							{
								$difColor='#0000aa';
								if((@substr_count($mProductes[$key0]['tipus'],'dip�sit') || @substr_count(urldecode($mProductes[$key0]['tipus']),'dip�sit'))>0){$tdColor='#A5E7F6';}
								else{$tdColor='#68DFF9';}
							}

							echo "
				<td   id='td_producteInventari_".$key0."' style='background-color:".$tdColor.";'>
							";
				
							if(!@isset($mInventariG['inventariRef'][$mProductes[$key0]['id']]) || @$mInventariG['inventariRef'][$mProductes[$key0]['id']]=='')
							{
								@$mInventariG['inventariRef'][$mProductes[$key0]['id']]=0;
							}
							echo "
				<p  class='".$classTitolColumnes."'>".@$mInventariG['inventariRef'][$mProductes[$key0]['id']]."
				<br>
				<font id='f_difP_".$key0."' style='font-size:11px; color:#000000;'>difP:</font><font style='color:".$difColor.";'><b>".$difP."</b></font></p>
				</td>
							";
						}
						else //especial
						{
							echo "
						<td   id='td_producteInventari_".$key0."'></td>
							";
						}
					}
				}
			}

//-------- forma de calcul copiada a vistaAlbara
		if($mPars['selLlistaId']==0)
		{
			if(@$mParsCAC['quantitat_total']==0)
			{
				$producteCtUnitats=0;
				$producteCtEcos=0;
				$producteCtEuros=0;
			}
			else
			{
				$producteCtUnitats=$mProductes[$key0]['pes']*$mProductes[$key0]['quantitat']*($mProductes[$key0]['cost_transport_extern_kg']+$mProductes[$key0]['cost_transport_intern_kg']+($mParametres['cost_transport_extern_repartit']['valor']/($mParsCAC['quantitat_total']+$mParsCAC['kg_intercanvisRebosts']))+($mParametres['cost_transport_intern_repartit']['valor']/($mParsCAC['quantitat_total']+$mParsCAC['kg_intercanvisRebosts'])));
				$producteCtEcos=
				$mProductes[$key0]['pes']*$mProductes[$key0]['quantitat']*
				(
					$mProductes[$key0]['cost_transport_extern_kg']	*	$mProductes[$key0]['ms_ctek']/100
					+$mProductes[$key0]['cost_transport_intern_kg']	*	$mProductes[$key0]['ms_ctik']/100
					+($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100)
						/($mParsCAC['quantitat_total']+$mParsCAC['kg_intercanvisRebosts'])
					+($mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)
						/($mParsCAC['quantitat_total']+$mParsCAC['kg_intercanvisRebosts'])
				);
				$producteCtEuros=$producteCtUnitats-$producteCtEcos;
			}
		}
		else
		{
			$producteCtUnitats=$mProductes[$key0]['pes']*$mProductes[$key0]['quantitat']*$mPropietatsPeriodeLocal['ctikLocal'];
			$producteCtEcos=$mProductes[$key0]['pes']*$mProductes[$key0]['quantitat']*$mPropietatsPeriodeLocal['ctikLocal']*($mPropietatsPeriodeLocal['ms_ctikLocal'])/100;
			$producteCtEuros=$producteCtUnitats-$producteCtEcos;
		}
		
//------------------------		
		
			echo "
			
				<td id='td_producteCtUnitats_".$key0."'  align='right' style='width:10%;'>
				<p>".(number_format($producteCtUnitats,2))."</p>
				</td>
				<td id='td_producteCtEcos_".$key0."'  align='right' style='width:10%;'>
				<p>".(number_format($producteCtEcos,2))."</p>
				</td>
				<td id='td_producteCtEuros_".$key0."'  align='right' style='width:10%;'>
				<p>".(number_format($producteCtEuros,2))."</p>
				</td>
				<td id='td_productePreuUnitats_".$key0."'  align='right' style='width:10%;'>
				</td>
				<td id='td_productePreuEcos_".$key0."'  align='right' style='width:10%;'>
				</td>
				<td id='td_productePreuEuros_".$key0."'  align='right' style='width:10%;'>
				</td>
			</tr>
			";
			$contLinies+=1;
		}
	}
	reset($mProductes);
}
else
{
	if($mPars['veureProductesDisponibles']==1)
	{
		echo "
		<center><p>No hi ha productes <b>actius</b> en aquesta recerca</p></center>
		";
	}
	else
	{
		echo "
		<center><p>No hi ha productes en aquesta recerca</p></center>
		";
	}
}

	echo "		
		</table>
		<table style=' width:100%;'>
			<tr>
				<td style=' width:33%;'>
			";
			html_paginacio(2);
			echo "
				</td>
				<td  style='width:33%;'>
				<p style='font-size:11px;'>(* <i>productes ordenats per <b>".$mPars['sortBy']."</b> - ".$mPars['ascdesc']."</i>)</p>
				</td>
				<td  style='width:33%;'>
				</td>
			</tr>
		</table>
		</div>
		</center>
		</td>
	</tr>
</table>
	";

	
	return;
}




//------------------------------------------------------------------------------
function html_mostrarNotesProductes($grupId)
{
	if($grupId=='0')
	{
		echo "
	<table style='width:70%;' align='center'>
		<tr>
			<td style='width:100%;' align='center'>
			<p class='nota2'><u>Notes:</u></p>
			
			<p class='nota2'>* <b>DifP</b>:  (valor mostrat per cada producte a la columna 'Inventari'). 
			Indica la diferencia entre l'estoc_previst i el valor total d'inventari de cada producte 
			(la suma dels inventaris de tots els magatzems CAC). 
			<br>
			Es �til abans d'obrir precomandes, per ajustar l'estoc_previst a l'estoc de l'inventari i, despr�s, 
			per afegir les quantitats que la CAC demana als prove�dors. Aix� l'estoc_previst sempre es pot 
			comparar a l'inventari, i sabem el que hi ha i el que encara ha d'entrar als magatzems.
			<br>
			Si DifP<0, significa que s'ofereix menys producte del que hi ha en estoc, i caldria modificar 
			l'estoc_previst per oferir-lo tot.
			<br>
			Si DifP>0, significa que s'ofereix m�s producte del que hi ha en estoc, i significa que s'ha demanat 
			producte per distribuir en aquesta ruta, pero encara no ha entrat als magatzems o encara no s'ha canviat de format.
			</p>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<br>
			<p class='nota2'>
			Marques de color:
			<br>
			1/si DifP<0, la cel.la apareix aixi:
			</p>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<table align='left'>
				<tr>
					<td width='50px' height='50px'  style='background-color:#FA897A;'>
					<p class='nota2'>
					<font style='background-color:#Ff0000; color:#ffffff;'><b>DifP:</b></font>valor
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<br>
			<p class='nota2'>
			2/si DifP<0, i el producte es de tipus 'dip�sit', la cel.la apareix aixi:
			</p>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<table align='left'>
				<tr>
					<td width='50px' height='50px'  style='background-color:#F9DAD6;'>
					<p class='nota2'>
					<font style='background-color:#Ff0000; color:#ffffff;'><b>DifP:</b></font>valor
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<br>
			<p class='nota2'>
			3/si DifP>0, la cel.la apareix aixi:
			</p>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<table align='left'>
				<tr>
					<td width='50px' height='50px'  style='background-color:#68DFF9;'>
					<p class='nota2'>
					<font style='background-color:#Ff0000; color:#ffffff;'><b>DifP:</b></font>valor
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<br>
			<p class='nota2'>
			4/si DifP>0, i el producte es de tipus 'dip�sit', la cel.la apareix aixi:
			</p>
			</td>
		</tr>
		<tr>
			<td align='left'>	
			<table align='left'>
				<tr>
					<td width='50px' height='50px'  style='background-color:#A5E7F6;'>
					<p class='nota2'>
					<font style='background-color:#Ff0000; color:#ffffff;'><b>DifP:</b></font>valor
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<br>
	<br>
	&nbsp;
		";
	}

	return;
}



?>

		