<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include "html_ajuda1.php";
include "db_ajuda.php";

//hello2

//------------------------------------------------------------------------
function recepcioFitxer($db)
{
    global $mParametres,$mPars;

    $missatgeError='';

	$dir0=getcwd();
	
	$dirChain='basesGrups';
	if(!@chdir($dirChain))
	{
		mkdir($dirChain);
	}
	else
	{
		chdir($dir0);
	}

	$dirChain='basesGrups/'.$mPars['selRutaSufix'];
	if(!@chdir($dirChain))
	{
		mkdir($dirChain);
	}
	else
	{
		chdir($dir0);
	}


    if($_FILES['f_fitxer']['name']!='')
    {
        if (is_uploaded_file($_FILES['f_fitxer']['tmp_name']))
        {
            //mirar si l'extensio �s correcta:
            $mInfo=@pathinfo($_FILES['f_fitxer']['name']);
  			if($mInfo['extension']=='pdf')
	        {
				if(!copy($_FILES['f_fitxer']['tmp_name'],$dirChain.'/'.$mPars['grup_id'].'.pdf'))
            	{
                	$missatgeError.="<p class='pAlertaNo4'> Atenci�: Error en copiar (".$dirChain.'/'.$mPars['grup_id'].'.pdf'.").</p>";
            	}
			}
			else
           	{
               	$missatgeError.="<p class='pAlertaNo4'> Atenci�: Nom�s es permeten fitxers .pdf</p>";
           	}
		}
    }

    if(strlen($missatgeError)==0)
	{
        $missatgeError.="<p class='pAlertaOk4'> Ok: El fitxer s'ha guardat correctament.</p>";
    }

	return $missatgeError;
}


$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
getConfig($db); //inicialitza variables anteriors;

	
if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}
$mUsuari=db_getUsuari($mPars['usuari_id'],$db);
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['basesGrups.php']=db_getAjuda('basesGrups.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$gId=@$_GET['gId'];
if(isset($gId) && $gId!='' && $gId!='0'){$mPars['grup_id']=$gId;}

$mGrup=getGrup($db);
$missatgeAlerta='';
$op=@$_POST['i_op'];
if(isset($op))
{
	if($op=='pujarFitxer')
	{
		$missatgeAlerta=recepcioFitxer($db);
	}
}

$teBases=false;
$periodeBases=dir_getBasesGrup();
if($periodeBases)
{
	$teBases=true;
}



$parsChain=makeParsChain($mPars);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>Bases-CAC del grup ".(urldecode($mGrup['nom']))."</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_basesGrups.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
ruta=".$mPars['selRutaSufix'].";
function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='compteCes.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}
</script>
</head>
<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('basesGrups.php?');
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:16px;'><b>".$mContinguts['index']['titol0']."  - </b> ".$mContinguts['index']['titol1']."</b></p>
			</td>
		</tr>
		<tr>
			<td style='width:100%;' align='center'>
			".$missatgeAlerta."
			</td>
		</tr>
	</table>
	<center><p>GRUP: <b>".(urldecode($mGrup['nom']))."</b></p></center>
	<center><p>Usuaria: <b>".(urldecode($mUsuari['usuari']))."</b> (".$mUsuari['nivell'].")</p></center>
	<br>
";
if($mPars['usuari_id']==$mGrup['usuari_id'] || $mPars['nivell']=='sadmin')
{
	echo "
	<FORM name='f_pujarBases' METHOD='POST' action='basesGrups.php' target='_self'  ENCTYPE='multipart/form-data'>
	<input type='hidden' name='i_pars' value='".$parsChain."'>
	<input type='hidden' id='i_op' name='i_op' value='pujarFitxer'>
	<table width='50%' align='center'>
		<tr>
			<td width='50%' valign='bottom'>
			<p><b>Importar Bases del Grup</b>:</p>
			<INPUT TYPE='file' size='30' NAME='f_fitxer' accept='.pdf'>
			</td>
			<td width='50%' valign='bottom'>
			";
			if($mPars['demo']==-1)
			{
				$disabled='';	
			}
			else
			{
				$disabled='DISABLED';
			}
			echo "
			<input type='submit' ".$disabled." value='importar'>
			</td>
		</tr>
		<tr>
			<td width='50%'  valign='top'>
			<br>
			<p class='p_micro2'>- Nom�s s'admet format .pdf</p>
			<p class='p_micro2'>- Les bases del grup han d'estar aprovades per l'assemblea del grup </p>
			<p class='p_micro2'>- Les assemblees han de tenir un m�nim de 5 persones</p>
			<p class='p_micro2'>- Una vegada importat el document quedar� accessible per totes les usuaries</p>
			</td>
			<td width='50%' valign='top'>
			<br>
			<p class='p_micro2'>- La publicaci� de les bases del grup es necessaria perqu� les usuaries puguin consultar-les abans de sol.licitar entrar en el grup.</p>
			<p class='p_micro2'>- La publicaci� de les bases del grup es un requisit per a poder aplicar el marge per a despeses del grup</p>
			<p class='p_micro2'>- A les Bases del grup ha de constar la decisi� aprovada del % que el grup aplicar� sobre l'import de les comandes dels seus membres per a despeses de gesti� del grup</p>
			<p class='p_micro2'>- Es pot aplicar un m�xim d'un <b>".$mParametres['maxDespesesGestioGrup']['valor']." %</b> sobre l'import de les comandes, per decisi� assemblearia de la CIC (JJAA Mas Mauset, 2015)</p>			</td>
		</tr>
	</table>
	</form>
	
	";
}
else
{
	echo "
	<table width='50%' align='center'>
		<tr>
			<td  width='100%' align='center'>
			<p><b>[ Configuraci� del grup ]</b></p>
			<p>Despeses de gesti� del grup: <b>".$mPars['fPagamentDgGrup']."</b> %</p>
			</td>
		</tr>
	</table>
	";

}

echo "
	<table width='80%' align='center'>
		<tr>
			<th  width='100%' align='left'>
			<br>
			<p>Bases actuals del grup: ";if($teBases){echo "(antiguitat: ".$periodeBases.")";} echo "</p>
			<br>
			</th>
		</tr>
		";
		if($teBases)
		{
			echo "
		<tr>
			<th  width='100%' height='600px' align='left'>
			<object data='basesGrups/".$periodeBases."/".$mGrup['id'].".pdf' type='application/pdf' width='100%' height='100%'>
  			<p><a href='basesGrups/".$periodeBases."/".$mGrup['id'].".pdf'>descarregar</a></p>
			</object>
			</th>
		</tr>
			";
		}
		else
		{
			echo "
		<tr>
			<td>
			<p>Encara no s'han importat les bases del grup</p>
			</td>
		</tr>
			";
		}
		echo "
	</table>
	<br><br><br><br>&nbsp;
";
html_helpRecipient();


echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>
";
	

?>