<?php
//----------------------------------------------------------------
function f1($cadena)
{
	$m1=array('="',"='",'= "',"=' ",'=   "',"='  ",'=    "',"='    ");
	
	$cadena=str_replace($m1,'<br><br>',$cadena);
	
	return $cadena;
}

//----------------------------------------------------------------
function db_guardarComandaCacDiposit($mGP,$db)
{
	global $mPars;
	
	$mComandaCAC=db_getComandaCopiar('0',$db);
	$resumProducteFinal='producte_'.$mGP['id'].':'.$mGP['estoc_previst'].';';

	//actualitzar comanda CAC
	//// si ja existeix comanda:
	if(@substr_count($mComandaCAC['resum'],'producte_'.$mGP['id'].':')>0)
	{
		$resumProducte_=substr($mComandaCAC['resum'],strpos($mComandaCAC['resum'],'producte_'.$mGP['id'].':'));
		$resumProducteInicial=substr($resumProducte_,0,strpos($resumProducte_,';')+1);
		if(!$result=mysql_query("update comandes_".$mPars['selRutaSufix']." set resum=REPLACE(resum,'".$resumProducteInicial."','".$resumProducteFinal."')  where rebost='0-CAC'",$db))
		{
			//echo "<br> 26 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
			return false;
    	}
	}
	else
	{
		if(!$result=mysql_query("update comandes_".$mPars['selRutaSufix']." set resum=CONCAT(resum,'".$resumProducteFinal."')  where rebost='0-CAC'",$db))
		{
			//echo "<br> 26 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
			return false;
    	}
	}
	
	return true;
}

//----------------------------------------------------------------
function db_guardarProducte2($mGP,$db)
{
	global $mPars;

	$registre='';
	$mysqlChain='';
	if(isset($mGP['categoria0'])){$mGP['categoria0']=urldecode($mGP['categoria0']);}
		if(isset($mGP['ncategoria0']) && $mGP['ncategoria0']!=''){$mGP['categoria0']=urldecode($mGP['ncategoria0']);}
	if(isset($mGP['categoria10'])){$mGP['categoria10']=urldecode($mGP['categoria10']);}
		if(isset($mGP['ncategoria10']) && $mGP['ncategoria10']!=''){$mGP['categoria10']=urldecode($mGP['ncategoria10']);}
	if(isset($mGP['tipus'])){$mGP['tipus']=urldecode($mGP['tipus']);}
	unset($mGP['ncategoria0']);
	unset($mGP['ncategoria10']);
//	if(isset($mGP['estoc_disponible'])){$mGP['estoc_disponible']=0;}
	
	$id=$mGP['id'];

	//obtenim dades producte actual0.
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']."  where id='".$id."'",$db))
	{
		//echo "<br> 16 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
    }
	else
	{
		if($mPars['selLlistaId']==0)
		{
			$unitatsReservades=getUnitatsReservades($db);
		}
		else
		{
			$unitatsReservades=db_getUnitatsReservadesLocal($db);
		}
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		$mGP['estoc_disponible']=$mRow['estoc_disponible'];
		$mGP['unitatsReservades']=$unitatsReservades;
		//guardar estoc_previst i estoc disponible
		if($mRow['estoc_previst']!=$mGP['estoc_previst'] || $mGP['estoc_previst']!=$mRow['estoc_disponible']+$unitatsReservades || (@$mGP['actiu']=='1' && $mGP['estoc_previst']>0))
		{
			//tant per productes diposit com no diposit

			$mGP['estoc_disponible']=$mRow['estoc_disponible']*1;
				
			if($mGP['estoc_previst']*1<=$unitatsReservades)
			{
				$mGP['estoc_disponible']=0;
				$mGP['estoc_previst']=$unitatsReservades;
			}
			else
			{
				$mGP['estoc_disponible']=$mGP['estoc_previst']-$unitatsReservades;
			}
	
			if(!db_guardarComandaCacDiposit($mGP,$db))
			{
				return false;
			}
		}
		else
		{
			unset($mGP['estoc_previst']);
			unset($mGP['estoc_disponible']);
		}
		
	}
	unset($mGP['id']);
	unset($mGP['historial']);

	if(guardarValorsIpropietatsProducte('historial',$mGP,$db))
	{
		return true;
	}
	
	return true;
}

//-------------------------------------------------------------
function db_anularReserves($producteId,$db)
{
	global $mPars,$mGrupsRef, $mUsuarisRef,$mParametres;
	
	$missatgeAlerta='';
	$unitatsReservades_=0;
	$unitatsReservades=0;
	$mResult=array();
	$mResult['result']=false;
	$mResult['missatgeAlerta']='';

	$unitatsReservades=getUnitatsReservades($db);
	$mGP['unitatsReservades']=$unitatsReservades;
	$mGP['grupsReservants']='';

	if($mPars['selLlistaId']==0)
	{
		if(!$result=mysql_query("select id,usuari_id,rebost,f_pagament,SUBSTRING(resum,LOCATE('producte_".$mPars['selProducteId'].":',resum)) from ".$mPars['taulaComandes']." where LOCATE('producte_".$mPars['selProducteId'].":',CONCAT(' ',resum))>0 order by id ASC",$db))
		{
			//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			$mResult['missatgeAlerta']="<br>- error en llegir les comandes.";
			$mResult['result']=false;
			return $mResult;;
		}
	}
	else
	{
		if(!$result=mysql_query("select id,usuari_id,rebost,f_pagament,SUBSTRING(resum,LOCATE('producte_".$mPars['selProducteId'].":',resum)) from ".$mPars['taulaComandes']." where usuari_id!=0 AND periode_comanda='".$mPars['sel_periode_comanda_local']."' AND llista_id='".$mPars['selLlistaId']."' AND LOCATE('producte_".$mPars['selProducteId'].":',CONCAT(' ',resum))>0 order by id ASC",$db))
		{
			//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			$mResult['missatgeAlerta']="<br>- error en llegir les comandes.";
			$mResult['result']=false;
			return $mResult;;
		}
	}
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_NUM))
	{
		$mComandesModificades[$i]=$mRow;
		$i++;
	}
	
	while(list($key,$mComandaModificada)=@each($mComandesModificades))
	{
		if(substr($mComandaModificada[2],0,strpos($mComandaModificada[2],'-'))!=0)
		{
		
		$unitatsReservades_=substr($mComandaModificada[4],strpos($mComandaModificada[4],':')+1);
		$unitatsReservades_=substr($unitatsReservades_,0,strpos($unitatsReservades_,';'));
		$unitatsReservades+=$unitatsReservades_;
		$cadena=substr($mComandaModificada[4],0,strpos($mComandaModificada[4],';')+1);
		$mComandesModificades[$key][4]=$unitatsReservades_;
		if(!$result=mysql_query("update ".$mPars['taulaComandes']." set resum=REPLACE(resum,'".$cadena."','') where llista_id='".$mPars['selLlistaId']."' AND id='".$mComandaModificada[0]."'",$db))
		{
			//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			$mResult['missatgeAlerta'].="<br>- error en llegir les comandes(llista_id:'".$mPars['selLlistaId'].", id comanda:'".$mComandaModificada[0]."')";
			$mResult['result']=false;
				
			return $mResult;
		}
		else
		{
			$mResult['missatgeAlerta'].="<br>- ok, anul.lada reserva id:".$mComandaModificada[0].",grup:".(urldecode($mComandaModificada[2])).",usuari:".$mComandaModificada[1]."(".$mUsuarisRef[$mComandaModificada[1]]['usuari']."), producte:".$producteId.", reservat: ".$unitatsReservades_;
			$mGP['grupsReservants'].="<br>grup:".($mComandaModificada[2]).",usuari:".$mComandaModificada[1]."(".$mUsuarisRef[$mComandaModificada[1]]['usuari'].") reservat: ".$unitatsReservades_;
		}
		}
	}
	@reset($mComandesModificades);
	
	//actualitzar el producte fins on no hagi fallat:

	$mProducte=db_getProducte($producteId,$db);

	if(!$result=mysql_query("update ".$mPars['taulaProductes']." set estoc_previst='0', estoc_disponible='0', actiu='0' where llista_id='".$mPars['selLlistaId']."' AND id='".$producteId."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mResult['missatgeAlerta'].="<br>- error actualitzar els estocs del producte.";
		$mResult['result']=false;
		
		return $mResult;
	}
	
	$mResult['result']=true;

	//valors del producte a modificar
	$mGP['estoc_previst']=0;
	$mGP['estoc_disponible']=0;
	$mGP['actiu']=0;
	
	if(!guardarValorsIpropietatsProducte('anularReserves',$mGP,$db))
	{
		$mResult['missatgeAlerta'].="<br>- error en guardar a historial producte.";
		$mResult['result']=false;
	}

	// si tot be: 
	////actualitzar forma de pagament de les usuaries
	while(list($key,$mComandaModificada)=@each($mComandesModificades))
	{
		if(substr($mComandaModificada[2],0,strpos($mComandaModificada[2],'-'))!=0)
		{

		$mFpagament=explode(',',$mComandaModificada[3]);
		if(count($mFpagament)>0)
		{
			$ecosT=$mFpagament[0]*1;
			$ebT=@$mFpagament[1]*1;
			$eurosT=$mFpagament[2]*1;
			$umsT=$ecosT+$ebT+$eurosT;
		}
	
		if($mPars['selLlistaId']==0)
		{
			$ecosProdAr=$mComandaModificada[4]*$mProducte['preu']*$mProducte['ms']/100;
			$ecosCTprodAr=$mComandaModificada[4]*$mProducte['pes']*$mProducte['cost_transport_intern_kg']*$mProducte['ms_ctik']/100;
			$ecosFDprodAr=$mComandaModificada[4]*$mProducte['preu']*($mParametres['FDCpp']['valor']/100)*$mParametres['msFDCpp']['valor']/100;
			$ecosTprodAr=$ecosProdAr+$ecosCTprodAr+$ecosFDprodAr;
		
			$eurosProdAr=$mComandaModificada[4]*$mProducte['preu']*(100-$mProducte['ms'])/100;
			$eurosCTprodAr=$mComandaModificada[4]*$mProducte['pes']*$mProducte['cost_transport_intern_kg']*(100-$mProducte['ms_ctik'])/100;
			$eurosFDprodAr=$mComandaModificada[4]*$mProducte['preu']*($mParametres['FDCpp']['valor']/100)*(100-$mParametres['msFDCpp']['valor'])/100;
			$eurosTprodAr=$eurosProdAr+$eurosCTprodAr+$eurosFDprodAr;

			$umsTar=$ecosTprodAr+$eurosTprodAr;
			
			$ecosT_=$ecosT-$umsTar*$mFpagament[6]/100;
			$ebT_=$ebT;
			$eurosT_=$eurosT-$umsTar*(100-$mFpagament[6])/100;
			$umsT_=$ecosT_+$eurosT_+$ebT_;

			
			
		
			if($umsT_<=0.01)
			{
				$ecosT_=0;
				$eurosT_=0;
		
				$ppEcos=0;
				$ppEb=0;
				$ppEuros=0;
			}
			else
			{
				$ecosT_=number_format($ecosT_,2,'.','');
				$eurosT_=number_format($eurosT_,2,'.','');
		
				$ppEcos=number_format(($ecosT_*100/$umsT_),2,'.','');
				$ppEb=number_format(($ebT_*100/$umsT_),2,'.','');
				$ppEuros=number_format(($eurosT_*100/$umsT_),2,'.','');
			}
		
			$f_pagament=$ecosT_.','.$ebT_.','.$eurosT_.','.$mFpagament[3].','.$mFpagament[4].','.$mFpagament[5].','.$ppEcos.','.$ppEb.','.$ppEuros.','.$mFpagament[9];
		}
		else
		{
			$ecosProdAr=$mComandaModificada[4]*$mProducte['preu']*$mProducte['ms']/100;
			$ecosCTprodAr=$mComandaModificada[4]*$mProducte['pes']*$mPropietatsPeriodeLocal['ctikLocal']*$mPropietatsPeriodeLocal['ms_ctikLocal']/100;
			$ecosFDprodAr=$mComandaModificada[4]*$mProducte['preu']*$mPropietatsPeriodeLocal['fdLocal']*$mPropietatsPeriodeLocal['ms_fdLocal']/100;
			$ecosTprodAr=$ecosProdAr+$ecosCTprodAr+$ecosFDprodAr;
		
			$eurosProdAr=$mComandaModificada[4]*$mProducte['preu']*(100-$mProducte['ms'])/100;
			$eurosCTprodAr=$mComandaModificada[4]*$mProducte['pes']*$mPropietatsPeriodeLocal['ctikLocal']*(100-$mPropietatsPeriodeLocal['ms_ctikLocal'])/100;
			$eurosFDprodAr=$mComandaModificada[4]*$mProducte['preu']*$mPropietatsPeriodeLocal['fdLocal']*(100-$mPropietatsPeriodeLocal['ms_fdLocal'])/100;
			$eurosTprodAr=$eurosProdAr+$eurosCTprodAr+$eurosFDprodAr;

			$umsTar=$ecosTprodAr+$eurosTprodAr;
			
			$ecosT_=$ecosT-$umsTar*$mFpagament[6]/100;
			$ebT_=$ebT;
			$eurosT_=$eurosT-$umsTar*(100-$mFpagament[6])/100;
			$umsT_=$ecosT_+$eurosT_+$ebT_;
		
			if($umsT_<=0.01)
			{
				$ecosT_=0;
				$eurosT_=0;
		
				$ppEcos=0;
				$ppEb=0;
				$ppEuros=0;
			}
			else
			{
				$ecosT_=number_format($ecosT_,2,'.','');
				$eurosT_=number_format($eurosT_,2,'.','');
		
				$ppEcos=number_format(($ecosT_*100/$umsT_),2,'.','');
				$ppEb=number_format(($ebT_*100/$umsT_),2,'.','');
				$ppEuros=number_format(($eurosT_*100/$umsT_),2,'.','');
			}
		
			$f_pagament=$ecosT_.','.$ebT_.','.$eurosT_.','.$mFpagament[3].','.$mFpagament[4].','.$mFpagament[5].','.$ppEcos.','.$ppEb.','.$ppEuros.','.$mFpagament[9];
		}

		////////////////////
		// no es filtra la forma de pagament segons el saldo d'ecos recolzats
		// es notifica a la usuaria perqu� modifiqui la forma de pagament o avisi admin
		// en actualitzar formes pagament desde resum per imports quedar� tot corregit correctament,
		//   encara que l usuria no actualitz�s la fp despr�s d'anular les reserves
		////////////////////
		
		
		if($mPars['selLlistaId']==0)
		{
			//echo "<br>update ".$mPars['taulaComandes']." set f_pagament='".$f_pagament."' WHERE SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".(substr($mComandaModificada[2],0,strpos($mComandaModificada[2],'-')))."' AND usuari_id='".$mComandaModificada[1]."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['periode_comanda']."'";
			if(!$result=mysql_query("update ".$mPars['taulaComandes']." set f_pagament='".$f_pagament."' WHERE SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".(substr($mComandaModificada[2],0,strpos($mComandaModificada[2],'-')))."' AND usuari_id='".$mComandaModificada[1]."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['periode_comanda']."'",$db))
			{
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				$mResult['missatgeAlerta'].="<p  class='pAlertaNo'> error en actualitzar la forma de pagament de la usuaria:".$mComandaModificada[1]."(".$mUsuarisRef[$mComandaModificada[1]]['usuari'].")</p>";
				$mResult['result']=false;
			}
			else
			{
				$mResult['missatgeAlerta'].="<p  class='pAlertaOk4'> Actualitzada la forma de pagament de la usuaria usuari:".$mComandaModificada[1]."(".$mUsuarisRef[$mComandaModificada[1]]['usuari']."): ".$mComandaModificada[3]." > ".$f_pagament."</p>";
			}
		}
		else
		{
			//echo "<br>update ".$mPars['taulaComandes']." set f_pagament='".$f_pagament."' WHERE SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".(substr($mComandaModificada[2],0,strpos($mComandaModificada[2],'-')))."' AND usuari_id='".$mComandaModificada[1]."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."'";
			if(!$result=mysql_query("update ".$mPars['taulaComandes']." set f_pagament='".$f_pagament."' WHERE SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".(substr($mComandaModificada[2],0,strpos($mComandaModificada[2],'-')))."' AND usuari_id='".$mComandaModificada[1]."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."'",$db))
			{
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				$mResult['missatgeAlerta'].="<p  class='pAlertaNo4'> error en actualitzar la forma de pagament de la usuaria usuari:".$mComandaModificada[1]."(".$mUsuarisRef[$mComandaModificada[1]]['usuari']."): ".$mComandaModificada[3]."(fp no modificada)</p>";
				$mResult['result']=false;
			}
			else
			{
				$mResult['missatgeAlerta'].="<p  class='pAlertaOk4'> Actualitzada la forma de pagament de la usuaria usuari:".$mComandaModificada[1]."(".$mUsuarisRef[$mComandaModificada[1]]['usuari']."): ".$mComandaModificada[3]." > ".$f_pagament."</p>";
			}
		}
		//enviar mail a usuaris amb comanda modificada

		if(!mail_anulacioReserves($mComandaModificada,$producteId,$db))
		{
			$mResult['missatgeAlerta'].="<p  class='pAlertaNo4'> error en enviar notificaci� a ".$mUsuarisRef[$mComandaModificada[1]]['usuari']." (".$mUsuarisRef[$mComandaModificada[1]]['email']."), comanda a grup ".(urldecode($mComandaModificada[2])).")</p>";
		}
		else
		{
			$mResult['missatgeAlerta'].="<p  class='pAlertaOk4'> enviada notificaci� a ".$mUsuarisRef[$mComandaModificada[1]]['usuari']." (".$mUsuarisRef[$mComandaModificada[1]]['email'].", comanda a grup ".(urldecode($mComandaModificada[2])).")</p>";
		}
		
		}
	}	
	@reset($mComandesModificades);
	
	return $mResult;
}

//----------------------------------------------------------------
function guardarValorsIpropietatsProducte($tipusRegistre,$mGP,$db)
{
	global $mPars;
	
	$mysqlChain='';
	$registre='';
	$unitatsReservadesText='';
	$grupsReservantsText='';
	//obtenim dades producte actual.
	//echo "<br>select * from ".$mPars['taulaProductes']."  where id='".$mPars['selProducteId']."' AND llista_id='".$mPars['selLlistaId']."'";
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']."  where id='".$mPars['selProducteId']."'  AND llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br> 145 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
    }
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		if($tipusRegistre=='anularReserves')
		{
			$unitatsReservades=$mGP['unitatsReservades'];
			$unitatsReservadesText='(unitats reservades:'.$unitatsReservades.')';
			$grupsReservantsText=$mGP['grupsReservants'];
			unset($mGP['unitatsReservades']);
			unset($mGP['grupsReservants']);
		}
/*
		if($mGP['productor']!=$mRow['productor'])
		{
			$mGP['actiu']=0;
			$mGP['estoc_previst']=0;
			$mGP['estoc_disponible']=0;
		}
*/
		while(list($key,$val)=each($mGP))
		{
			if(isset($mRow[$key]))
			{
				if($mGP[$key]!=$mRow[$key])
				{
					
					$mysqlChain.=",".$key."='".$val."'";
					if($key=='notes_rebosts'){$val='(...)';$mRow[$key]=$val;$mGP[$key]=$val;}
					$registre.='{p:'.$tipusRegistre.';us:'.$mPars['usuari_id'].';'.$key.':'.$mRow[$key].'->'.$mGP[$key].';dt:'.date('d/m/Y H:i:s').';}';
				}
			}
		}
		reset($mGP);
		if($tipusRegistre=='anularReserves')
		{
			$registre.='{p:'.$tipusRegistre.';us:'.$mPars['usuari_id'].';'.$unitatsReservadesText.';dt:'.date('d/m/Y H:i:s').';';
			$registre.=$grupsReservantsText.'}';
		}
		
		$mysqlChain=substr($mysqlChain,1);
		
		$coma='';
		if($mysqlChain!='' || $registre!='')
		{
			if($mysqlChain!='' && $registre!=''){$coma=',';}
			
			//echo "<br>update ".$mPars['taulaProductes']." set  ".$mysqlChain.$coma."historial=CONCAT('".$registre."',historial) where id='".$mPars['selProducteId']."'";
			if(!$result=mysql_query("update ".$mPars['taulaProductes']." set  ".$mysqlChain.$coma."historial=CONCAT('".$registre."',historial) where id='".$mPars['selProducteId']."'",$db))
			{
				//echo "<br> 155 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
				return false;
    		}
			else
			{
				if(@$mGP['productor']!=$mRow['productor'])
				{
					$mPars['selProducteId']=0;
				}
			
				return true;
			}
			
		}
		else
		{
			return true;
		}
	}

	return false;
}


//----------------------------------------------------------------
function db_crearProducte($mGP,$db)
{
	global $mPars;

	$mysqlChain='';
	if(isset($mGP['categoria0'])){$mGP['categoria0']=urldecode($mGP['categoria0']);}
		if(isset($mGP['ncategoria0']) && $mGP['ncategoria0']!=''){$mGP['categoria0']=urldecode($mGP['ncategoria0']);unset($mGP['ncategoria0']);}
	if(isset($mGP['categoria10'])){$mGP['categoria10']=urldecode($mGP['categoria10']);}
		if(isset($mGP['ncategoria10']) && $mGP['ncategoria10']!=''){$mGP['categoria10']=urldecode($mGP['ncategoria10']);unset($mGP['ncategoria10']);}
	$mGP['tipus']=urldecode($mGP['tipus']);
	$mGP['actiu']=0;
	//$mGP['estoc_previst']=0;
	$mGP['estoc_disponible']=0;
//	$mGP['notes_rebosts']=f1(urldecode($mGP['notes_rebosts']));
//	$mGP['notes_rebosts']=urlencode($mGP['notes_rebosts']);
	
	$mCamps=array(
'id'=>'',
'segell'=>0,
'producte'=>'',
'actiu'=>'0',
'productor'=>'',
'categoria0'=>'',
'categoria10'=>'',
'tipus'=>'',
'unitat_facturacio'=>'',
'format'=>'',
'pes'=>'',
'volum'=>'',
'preu'=>'',
'ms'=>'',
'estoc_previst'=>'',
'estat'=>'',
'estoc_disponible'=>'',
'notes_rebosts'=>'',
'imatge'=>'',
'notes'=>'',
'cost_transport_extern_kg'=>'',
'ms_ctek'=>'',
'cost_transport_intern_kg'=>'',
'ms_ctik'=>'',
'historial'=>'{p:crearProducte;us:'.$mPars['usuari_id'].';dt:'.date('d/m/Y H:i:s').';}',
'llista_id'=>$mPars['selLlistaId'],
'propietats'=>''
);
	//registre propietats comanda
	
	
	while(list($key,$val)=each($mGP))
	{
		if($key!='' && $val!='')
		{
			$mCamps[$key]=$val;
		}
	}
	reset($mGP);
		

	while(list($key,$val)=each($mCamps))
	{
		if($key!='')
		{
			$mysqlChain.=",'".$val."'";
		}
	}	
	reset($mCamps);


	$mysqlChain=substr($mysqlChain,1);
	
	//echo "<br>"."insert into ".$mPars['taulaProductes']." values(".$mysqlChain.")";
	if(!$result=mysql_query("insert into ".$mPars['taulaProductes']." values(".$mysqlChain.")",$db))
	{
		//echo "<br> 88 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
    }
    else
	{
		$result=mysql_query("select id from ".$mPars['taulaProductes']." where SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPars['selPerfilRef']."' ORDER BY id DESC",$db);
		//echo "<br> 95 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		
		return $mRow['id'];
	}
	return false;
}

//----------------------------------------------------------------
function db_eliminarProducte($eliminarId,$db)
{
	global $mPars;
	
	if(!$result=mysql_query("select id from ".$mPars['taulaProductes']." where id='".$eliminarId."' AND estoc_previst=estoc_disponible ",$db))
	{
		//echo "<br> 116 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		if($mRow['id']==$eliminarId)
		{
			if(!$result=mysql_query("delete from ".$mPars['taulaProductes']." where id='".$eliminarId."'",$db))
			{
				//echo "<br> 116 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		
				return false;
			}
		}
	}

	return true;
}

//------------------------------------------------------------------------------
function db_actualitzarProductesAR($mVals,$db)
{
	global $mPars;
	$mGrup=getGrup($db);
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=true;
	$mMissatgeAlerta2=array();
	$mMissatgeAlerta2['missatge']='';
	$mMissatgeAlerta2['result']=true;

	//obtenir els productes:
	//echo "<br>select * from ".$mPars['taulaProductes']." WHERE llista_id='".$mPars['selLlistaId']."' AND SUBSTRING(productor,1,LOCATE('-',productor)-1)=".$mPars['selPerfilRef']."'";
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." WHERE llista_id='".$mPars['selLlistaId']."' AND SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPars['selPerfilRef']."'",$db))
	{
		//echo "<br> 436 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');

		$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'><i>obtenir productes de la llista <b>".(urldecode($mGrup['nom']))."</b>: ha fallat</p>";
		$mMissatgeAlerta['result']=false;

	}
	else
	{
		$mProductes_=array();
		$mProductes=array();
		
		while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mProductes_[$mRow['id']]=$mRow;
		}

		$mActivar[1]=1;
		$mActivar[-1]=0;

		while(list($id,$mProducte_)=each($mProductes_))
		{
			if(array_key_exists($id,$mVals))
			{
			
				$mProductes[$id]=array();
					$mPars['selProducteId']=$mProducte_['id'];
				$unitatsReservades_=getUnitatsReservades($db);
				if($mPars['selLlistaId']==0)
				{
					$unitatsEnEstocCac=db_getEstocGlobalProducte($id,$db);
				}
				else
				{
					$unitatsEnEstocCac=0;
				}
				

				if($unitatsReservades_*1+$unitatsEnEstocCac*1==0 && $mVals[$id]['estoc_previst']==0)
				{$mProductes[$id]['actiu']=0;}

				if($unitatsReservades_*1+$unitatsEnEstocCac*1==0 && $mVals[$id]['estoc_previst']>0)
				{
					$mProductes[$id]['estoc_previst']=$mVals[$id]['estoc_previst'];
					if($mVals[$id]['activar']==-1)
					{$mProductes[$id]['actiu']=0;}
					else
					if($mVals[$id]['activar']==1)
					{$mProductes[$id]['actiu']=1;}
					else
					{$mProductes[$id]['actiu']=$mProducte_['actiu'];;}
				}

				if($unitatsReservades_*1>0 || $unitatsEnEstocCac*1>0)
				{$mProductes[$id]['actiu']=1;}
				else
				if($mVals[$id]['activar']==-1)
				{$mProductes[$id]['actiu']=0;}
				else
				{$mProductes[$id]['actiu']=$mProducte_['actiu'];}
				

				if($unitatsReservades_*1>=$unitatsEnEstocCac*1)
				{$estocPrevist=$unitatsReservades_;}
				else
				{$estocPrevist=$unitatsEnEstocCac;}


				if
				(
					(
						$mPars['nivell']!='sadmin'
						&&
						$mPars['nivell']!='admin'
					)
					||
					substr_count($mProducte_['tipus'],'dip�sit')>0
				)
				{
					if($estocPrevist*1<=$mVals[$id]['estoc_previst']*1)
					{$estocPrevist=$mVals[$id]['estoc_previst'];}
					else
					{$estocPrevist=$mProducte_['estoc_previst'];}
				}
				else
				{
					if($mVals[$id]['estoc_previst']>$unitatsReservades_)
					{
						$estocPrevist=$mVals[$id]['estoc_previst'];
						if(isset($mVals[$id]['activar']) && $mVals[$id]['activar']!=0)
						{$mProductes[$id]['actiu']=$mActivar[$mVals[$id]['activar']];}
						else
						{$mProductes[$id]['actiu']=$mProducte_['actiu'];}
					}
					else
					{$estocPrevist=$mProducte_['estoc_previst'];}
				}


				$mProductes[$id]['estoc_previst']=$estocPrevist;
				$mProductes[$id]['estoc_disponible']=$estocPrevist-$unitatsReservades_;
				

				if($mProductes[$id]['estoc_previst']==0)
				{$mProductes[$id]['actiu']=0;}
				else if($mVals[$id]['activar']==1)
				{$mProductes[$id]['actiu']=1;}


				
				if
				(
					$unitatsReservades_!=$mProducte_['estoc_previst']-$mProducte_['estoc_disponible']
					&&
					($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
				)
				{
					$mMissatgeAlerta2['missatge'].="<p class='pAlertaNo4'><i>Atencio: calia corregir un error entre l'estoc previst i l'estoc disponible del producte <b>id:".$id."</b> a la llista <b>".(urldecode($mGrup['nom']))."</b></p>";
				}

				$mProductes[$id]['historial']="{p:activacioRapida;us:".$mPars['usuari_id'].";actiu=".$mProductes[$id]['actiu'].";estoc_previst=".$mProductes[$id]['estoc_previst'].";estoc_disponible=".$mProductes[$id]['estoc_disponible'].";dt:".date('d/m/Y H:i:s').";}".$mProducte_['historial'];
			}
		}
		reset($mProductes_);

		//actualitzar
		while(list($id,$mProducte)=each($mProductes))
		{
				//echo "<br>update ".$mPars['taulaProductes']." set estoc_previst='".$mProducte['estoc_previst']."',estoc_disponible='".$mProducte['estoc_disponible']."',actiu='".$mProducte['actiu']."',historial='".$mProducte['historial']."' WHERE id='".$id."' AND llista_id='".$mPars['selLlistaId']."'";
				if(!$result=mysql_query("update ".$mPars['taulaProductes']." set estoc_previst='".$mProducte['estoc_previst']."',estoc_disponible='".$mProducte['estoc_disponible']."',actiu='".$mProducte['actiu']."',historial='".$mProducte['historial']."' WHERE id='".$id."' AND llista_id='".$mPars['selLlistaId']."'",$db))
				{
					//echo "<br> 437 db_gestioProductes.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');

					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'><i>actualitzar el producte <b>id:".$id."</b> a la llista <b>".(urldecode($mGrup['nom']))."</b>: ha fallat</p>";
					$mMissatgeAlerta['result']=false;

				}
				else
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'><i>actualitzar el producte <b>id:".$id."</b> a la llista <b>".(urldecode($mGrup['nom']))."</b>: ok</p>";
				}
		}
		reset($mProductes);
	}
	

	return $mMissatgeAlerta;
}

?>

		