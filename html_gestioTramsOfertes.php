<?php
//------------------------------------------------------------------------------

function html_menuTrams()
{
	global $mPars,$parsChain,$mTrams,$mTram,$mMunicipis,$mDies,$db,$mColors;
	echo "
	<table width='80%' border='1' align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td width='33%' align='center' valign='top'>
			<br>
			<p>Editar OFERTA de transport:<br>
			<select name='sel_tramId' onChange=\"javascript:if(this.value!=''){enviarFpars('gestioTramsOfertes.php?vId=".$mPars['selVehicleId']."&tId='+this.value,'_self');}\">
	";
	$selected1='';
	$selected2='selected';
	$mSortida=array();

	$date = new DateTime();
	$t1=$date->getTimestamp();

			$selTramId_=@$mPars['selTramId'];
			
	while(list($key,$mTram_)=each($mTrams))
	{
			$mPars['selTramId']=@$mTram_['id'];
		$mUnitatsContractades=getUnitatsContractadesTram($db);
		if($mUnitatsContractades['total']>0){$marcaReservat='[R]';}else{$marcaReservat='';}
		
		$mSortida['Y']=date('Y',strtotime($mTram_['sortida']));
		$mSortida['m']=date('m',strtotime($mTram_['sortida']));
		$mSortida['d']=date('d',strtotime($mTram_['sortida']));
		$mSortida['h']=date('H',strtotime($mTram_['sortida']));
		$mSortida['i']=date('i',strtotime($mTram_['sortida']));
		$instantSortida = new DateTime();
		$instantSortida->setDate($mSortida['Y'], $mSortida['m'], $mSortida['d'] );
		$instantSortida->setTime($mSortida['h'], $mSortida['i'], 0 );

		$t2=$instantSortida->getTimestamp();

		if(isset($mTram['id']) && $mTram_['id']==$mTram['id'])
		{
			$selected1='selected';$selected2='';
		}
		else
		{
			$selected1='';
		}
		echo "
			<option "; 
		if($mTram_['actiu']==1 && ($t2-$t1)>0)
		{
			echo " style='color:#00aa00;' ";
		} 	
		else if($mTram_['actiu']==0)
		{
			echo " style='color:#000000;' ";
		} 
		else if(($t2-$t1)<=0)
		{
			echo " style='color:#ff0000;' ";
		} 
		echo $selected1."  value=\"".$mTram_['id']."\">
		[".$mTram_['id']."] ".$marcaReservat." sortida: "
		.(date('H:i d-m-Y',strtotime($mTram_['sortida'])))
		."&nbsp;&nbsp;&nbsp;".(urldecode(substr($mMunicipis[$mTram_['municipi_origen']]['municipi'],0,30)."...>>"
		.(substr($mMunicipis[$mTram_['municipi_desti']]['municipi'],0,30))))."...</option>
		";	
		unset($mUnitatsContractades);				
	}
	reset($mTrams);
		
		$mPars['selTramId']=$selTramId_;
	
	echo "
			<option ".$selected2." value=''></option>
			</select>
			<p class='p_micro'>
			* en verd les ".$mPars['vTipus']." actives
			<br>
			* en vermell les ".$mPars['vTipus']." caducades
			<br>
			* en negre les ".$mPars['vTipus']." inactives
			</p>
			</td>

			<td width='33%' align='center' valign='top'>
			<br>
			<p class='compacte' onClick=\"javascript: enviarFpars('gestioTramsOfertes.php?opt=nou','_self');\" 	style='color:#885500; cursor:pointer;'><u>crear nova OFERTA de transport</u></p>
			</td>

			<td width='33%' align='center' valign='top'>
			</td>
		</tr>
	</table>

	";
	return;
}

//------------------------------------------------------------------------------
function html_mostrarPropietatsTram($db)
{
	global $mPars,$mTram;

	$propietats=substr($mTram['propietats'],1);
	$propietats=substr($propietats,0,strlen($propietats)-1);
	$mHistorial=array();
	$mHistorial_=explode('}{',$propietats);
	while(list($key,$val)=each($mHistorial_))
	{
		$mVal=explode(';',$val);
		$mVal[0]=str_replace('{','',$mVal[0]);
		if($mVal[0]!='')
		{
			if(!isset($mHistorial[$mVal[0]])){$mHistorial[$mVal[0]]=array();}
			array_push($mHistorial[$mVal[0]],$mVal);
		}
	}
	echo "
	<p>[sadmin]</p>
	<div style='width:100%; height:100px; overflow-y:scroll; overflow-x:hidden;'>
	<table border='1' style='width:100%;'>
	";
	while(list($tipus,$mVal)=each($mHistorial))
	{
		echo "
				<tr>
					<td valign='top'>
					<p style='font-size:10px;'>".$tipus."</p>
					</td>
					<td>
					<p style='font-size:10px;'>
					";
					while(list($index,$mVal2)=each($mVal))
					{
						unset($mVal2[0]);
						$registre_=implode('|',$mVal2);
						if(substr($mVal2[1],0,strpos($mVal2[1],':'))=='us')
						{
							$usId=substr($mVal2[1],strpos($mVal2[1],':')+1);
							$mUsuariRegistre=db_getUsuari($usId,$db);
							$registre_=str_replace($mVal2[1],"<font style='cursor:pointer; size:11px;' onClick=\"enviarFpars('nouUsuari.php?iUed=".$usId."&op=editarUsuari','_blank');\"><u>".$mUsuariRegistre['usuari']."</u></font>",$registre_);
						}
						echo "
						".urldecode($registre_)."<br>
						";
					}
					echo "
					</p>
					</td>
				</tr>
	";
	}
	reset($mHistorial);
	echo "
	</table>
	</div>
	
	";	




	return;
}

//------------------------------------------------------------------------------
function html_introduirInstant($key)
{
	global $mPars,$mTram,$mVehicle,$mNomsColumnes3,$campsGuardar,$mUsuari,$mUnitatsContractades;
	
	if(!isset($mTram[$key]))
	{
		$mM[0]=0;
		$mM[1]=0;
		$mM[2]=0;
		$mM[3]=0;
		$mM[4]=0;
	}
	else
	{

		$mM[0]=date('H',strtotime($mTram[$key]));
		$mM[1]=date('i',strtotime($mTram[$key]));
		$mM[2]=date('d',strtotime($mTram[$key]));
		$mM[3]=date('m',strtotime($mTram[$key]));
		$mM[4]=date('Y',strtotime($mTram[$key]));
	}
	$disabled='';
	$nota='';
	
				if
				(
					(
						$mPars['nivell']=='sadmin'
						||
						(
							$mPars['selRutaSufix']==date('ym')
							&&
							$mPars['usuari_id']==$mTram['usuari_id']
						)
					)
				)
				{
					$campsGuardar.=','.$key;
					$onChange=" onChange=\"javascript:caducada=false;recordatoriGuardar();\" ";
				}
				else
				{
					$disabled=" DISABLED style='background-color:#eeeeee;' ";
					$onChange="";
				}

	echo "
<tr>
	<th valign='top' align='left'>
	<p>".$mNomsColumnes3[$key]."</p>
	</th>

	<td>
	<table>
		<tr>
			<td>
			<p>hora:<br>
			<select ".$disabled." id='i_hora_".$key."' ".$onChange.">
			";
			$selected='';
			$selected2='selected';
			for($i=0;$i<24;$i++)
			{
				if($mM[0]==$i){$selected='selected';$selected2='';}else{$selected='';}
				echo "
				<option ".$selected." value='".$i."'>".$i."</option>
				";
			}
			echo "
			<option ".$selected2." value=''></option>
			</select>
			</p>
			</td>

			<td>
			<p>minuts:<br>
			<select ".$disabled." id='i_minuts_".$key."' ".$onChange.">
			";
			$selected='';
			$selected2='selected';
			for($i=0;$i<=60;$i++)
			{
				if(@$mM[1]==$i){$selected='selected';$selected2='';}else{$selected='';}
				echo "
				<option ".$selected." value='".$i."'>".$i."</option>
				";
			}
			echo "
			<option ".$selected2." value=''></option>
			</select>
			</p>
			</td>

			<td>
			<p>dia:<br>
			<select ".$disabled." id='i_dia_".$key."' ".$onChange.">
			";
			$selected='';
			$selected2='selected';
			for($i=1;$i<=31;$i++)
			{
				if(@$mM[2]==$i){$selected='selected';$selected2='';}else{$selected='';}
				echo "
				<option ".$selected." value='".$i."'>".$i."</option>
				";
			}
			echo "
			<option ".$selected2." value=''></option>
			</select>
			</p>
			</td>

			<td>
			<p>mes:<br>
			<select ".$disabled." id='i_mes_".$key."' ".$onChange.">
			";
			$selected='';
			$selected2='selected';
			for($i=1;$i<=12;$i++)
			{
				if(@$mM[3]==$i){$selected='selected';$selected2='';}else{$selected='';}
				echo "
				<option ".$selected." value='".$i."'>".$i."</option>
				";
			}
			echo "
			<option ".$selected2." value=''></option>
			</select>
			</p>
			</td>

			<td>
			<p>any:<br>
			<select ".$disabled." id='i_any_".$key."' ".$onChange.">
			";
			$selected='';
			$selected2='selected';
			for($i=date('Y');$i<=(date('Y')+1);$i++)
			{
				if(@$mM[4]==$i){$selected='selected';$selected2='';}else{$selected='';}
				echo "
				<option ".$selected." value='".$i."'>".$i."</option>
				";
			}
			echo "
			<option ".$selected2." value=''></option>
			</select>
			".$nota."
			</p>
			</td>
		</tr>
	</table>
	<input type='hidden' id='i_".$key."' name='i_".$key."' value='".$mTram[$key]."'>
	</td>
</tr>
	";

	return;
}

function html_notesApropietarisTramsOfertes()
{
	global $mParametres,$mPars;
	
	
	echo "
	<br>
	<table align='center' width='80%'>
		<tr>
			<td width='100%' align='left'>
			<p class='nota'>Notes:</p>
			<p class='nota'><b>Edici� de les ".$mPars['vTipus'].":</b></p>
			<p class='nota'>
			* Les ".$mPars['vTipus']." de transport que han estat contractades totalment o parcial
			 no poden ser modificades pel seu propietari.
			<br>
			<br>
			<p class='nota'><b>Caducitat de les ".$mPars['vTipus'].":</b></p>
			<p class='nota'>
			* Les ".$mPars['vTipus']." de transport amb data de sortida menor o igual 
			a la data actual es consideren '<b>caducades</b>' i ja no es podran editar.
			<br>
			<br>
			* Les ".$mPars['vTipus']." de transport amb data de sortida menor o igual 
			a la data actual menys <b>".$mParametres['mesosVidaTramsCaducats']['valor']." mesos</b>
			s'esborraran de l'historial actual.
			<br>
			<br>
			<p class='nota'><b>".$mPars['vTipus']." amb etiquetes CAC</b></p>
			<p class='nota'>
			* Les ".$mPars['vTipus']." de transport amb etiqueta 'CAC' son especials. Per motius de coordinaci� 
			es poden crear encara que la data de sortida ja hagi passat, per tal que la CAC pugui contractar-la 
			i ambdues parts puguin incloure-la als respectius resums despr�s de revisar els detalls que calgui.
			<br>
			Es l'usuari 'cac' qui contracta les OFERTES i DEMANDES dels usuaris que s'hagin acordat previament en 
			relacio a la ruta, per ambdues parts.
			<br>
			En general, donat que la CAC no disposa de vehicles propis per oferir transport, l'usuari 'cac' nom�s
			contractar�,  i no generar�, OFERTES.
			<br>
			<br>
			Com totes les ".$mPars['vTipus']." caducades, les etiquetades com a 'CAC' no apareixeran a la llista de
			".$mPars['vTipus']." per les usuaries, excepte pel seu propietari i per l'usuari 'cac', per tal que pugui contractar les caducades.
			<br>
			&nbsp;
			</td>
		</tr>
	</table>			
	";
	
	return;
	
}
?>

		