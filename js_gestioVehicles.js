allowedCharsMatricula='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-';
allowedCharsEmail='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.@';
allowedCharsCompteEcos_lletres='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
emailNeededCharacters='@.';
allowedCharsEnter='0123456789';
allowedCharsFloat='0123456789.';
allowedCharsCategories='A��BCDE��FGHI��JKLMNO��PQRSTU��VWXYZa��bcde��fghi��jklmno��pqrstu��vwxyz0123456789- ';
allowedCharsRuta="A��BCDE��FGHI��JKLMNO��PQRSTU��VWXYZa��bcde��fghi��jklmno��pqrstu��vwxyz0123456789-':, ()/";

function hihaCaractersNoPermesos(caracters,cadena,camp)
{
   	allowedChars=caracters;
	missatgeAlerta1='';
	$_ok=0;
	
	//check for prohibited characters
    for(i=0;i<cadena.length;i++)
    {
       for(j=0;j<allowedChars.length;j++)
       {
           if(cadena.charAt(i)==allowedChars.charAt(j)){$_ok++;}
       }
	}
	if($_ok<cadena.length)
    {
		missatgeAlerta1="\nEl camp "+camp+" cont� un valor incorrecte \n\n(caracters permesos: "+allowedChars+")\n";			
    }
	
	return missatgeAlerta1;
}
		
function checkFormGuardarVehicles()
{
	isValid=false;
	missatgeAlerta='';

	value=document.getElementById('i_marca').value;
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'marca' no pot estar buit\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsRuta,value,'marca');
	}

	value=document.getElementById('i_model').value;
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'model' no pot estar buit\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsRuta,value,'model');
	}
	
	value=document.getElementById('i_any').value;
	if(value<=1950 && value>anyActual)
	{
		missatgeAlerta+="\nEl camp 'any' ha de ser un nombre enter de 4 digits, major de 1950 i menor o igual que "+anyActual+"\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsEnter,value,'any');
	}

	value=document.getElementById('i_consum').value;
	if(value==0)
	{
		missatgeAlerta+="\nel camp 'consum' no pot estar buit\n";			
	}
	else if(value>20)
	{
		missatgeAlerta+="\nel camp 'consum' cont� un valor massa alt perqu� el col.lectiu pugui fer un �s sostenible d'aquest vehicle\n";			
	}
	else if(value<0)
	{
		missatgeAlerta+="\nel camp 'consum' ha de ser un valor positiu\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'consum');
	}

	value=document.getElementById('i_combustible').value;
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'combustible' no pot estar buit\n";			
	}

	value=document.getElementById('i_categoria0').value;
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'categoria0' no pot estar buit\n";			
	}


	value=document.getElementById('i_municipi_id').value;
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'municipi' no pot estar buit\n";			
	}

	value=document.getElementById('i_capacitat_pes').value;
	if(value.length>100000)
	{
		missatgeAlerta+="\nEl camp 'capacitat_pes' cont� un nombre massa gran\n";			
	}
	else if(value.length!=0)
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsEnter,value,'capacitat_pes');
	}

	value=document.getElementById('i_capacitat_volum').value;
	if(value.length>100)
	{
		missatgeAlerta+="\nEl camp 'capacitat_volum' cont� un nombre massa gran\n";			
	}
	else if(value.length!=0)
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'capacitat_volum');
	}

	value=document.getElementById('i_capacitat_places').value;
	if(value.length>100)
	{
		missatgeAlerta+="\nEl camp 'places' cont� un nombre massa gran\n";			
	}
	else if(value.length!=0)
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsEnter,value,'places');
	}

	value=document.getElementById('i_matricula').value;
	if(value.length!=0)
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsMatricula,value,'matricula');
	}

		
	if (missatgeAlerta.length==0)
	{
		return true;
	}

	alert(missatgeAlerta);
	
  return false;
}	

function guardarVehicle()
{
	document.getElementById('f_guardarVehicle').submit();
	return;
}

function guardarNouVehicle()
{
	document.getElementById('i_opcio').value='crear';
	document.getElementById('f_guardarVehicle').submit();
	return;
}

function eliminarVehicle(vehicleId)
{
	document.getElementById('i_opcio').value='eliminar';
	document.getElementById('f_guardarVehicle').submit();

	return;
}


function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
		
	return;
}

		