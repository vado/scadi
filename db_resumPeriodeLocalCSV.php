<?php
include "config.php";
include "db.php";
include "eines.php";

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$parsChain=makeParsChain($mPars);

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db); //inicialitza variables anteriors;

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat (login)</p>
	";
	exit();
	$login=false;
}

$mPropietatsPeriodesLocals=db_getPropietatsPeriodesLocals($db);
		$mDatesReservesLocals=getDatesReservesLocals();

if($mPars['grup_id']==0)
{
	$mProductes_=db_getProductes2($db);
	$mProductes=array();
	$i=0;
	while(list($key,$mVal)=each($mProductes_))
	{
		$mProductes[$i]=$mVal;
		$i++;
	}
	reset($mProductes_);
	$mPars['numItemsF']=count($mProductes);
	$mPars['numPags']=CEIL($mPars['numItemsF']/$mPars['numItemsPag']);
}
else
{
	$mProductes=db_getProductes($db);
}

//------------------------------------------------------------------------

function db_getLlistaProductesCSV($db)
{
	global $mPars,$mProductes;
	$mColumnesProductesLlista=array('id','producte','productor','categoria0','categoria10','tipus','unitat_facturacio','pes','preu','ms','format');
	
	$mProductesLlista=array();
	$i=0;

	While(list($key,$mProducte)=each($mProductes))
	{
			$mProducte_['id']=urldecode($mProducte['id']);
			$mProducte_['producte']=urldecode($mProducte['producte']);
			$mProducte_['productor']=urldecode($mProducte['productor']);
			$mProducte_['categoria0']=$mProducte['categoria0'];
			$mProducte_['categoria10']=$mProducte['categoria10'];
			if(substr($mProducte['tipus'],0,1)==',')
			{
				$mProducte_['tipus']=substr($mProducte['tipus'],1);
			}
			$mProducte_['tipus']=str_replace(',','
',$mProducte_['tipus']);
			$mProducte_['tipus']=str_replace(',','',$mProducte_['tipus']);
			$mProducte_['unitat_facturacio']=urldecode($mProducte['unitat_facturacio']);
			$mProducte_['pes']=str_replace('.',',',(number_format($mProducte['pes'],2)));
			$mProducte_['preu']=str_replace('.',',',(number_format($mProducte['preu'],2)));
			$mProducte_['ms']=$mProducte['ms'];
			$mProducte_['format']=$mProducte['format'];
			$mProductesLlista[$i]=$mProducte_;
			$i++;
	}
	reset($mProductes);
	@unlink("llistaProductes/pre_llistaProductes_".$mPars['usuari_id'].".csv");

	if(!$fp=fopen("llistaProductes/pre_llistaProductes_".$mPars['usuari_id'].".csv",'w'))
	{
		return false;
	}

	$mH1[0]='ruta: '.$mPars['selRutaSufix'];
	fputcsv($fp, $mH1,',','"');
	$mH1[0]=date('d/m/Y hh:mm');
	fputcsv($fp, $mH1,',','"');
	$mH1[0]=' ';
	fputcsv($fp, $mH1,',','"');
	$mH1[0]='Filtre:';
	fputcsv($fp, $mH1,',','"');

	if($mPars['veureProductesDisponibles']==1)
	{
		$mH1[0]='- nom�s es mostren els productes actius';
	}
	else
	{
		$mH1[0]='- es mostren els productes actius i els inactius';
	}
	fputcsv($fp, $mH1,',','"');
	if($mPars['excloureProductesJaEntregats']==1)
	{
		$mH1[0]="- s'exclouen els productes ja entregats";
	}
	else
	{
		$mH1[0]="- NO s'exclouen els productes ja entregats";
	}
	fputcsv($fp, $mH1,',','"');
	
	$mH1[0]='- productors: '.$mPars['vProductor'];
	fputcsv($fp, $mH1,',','"');
	$mH1[0]='- categoria: '.$mPars['vCategoria'];
	fputcsv($fp, $mH1,',','"');
	$mH1[0]='- sub-categoria: '.$mPars['vSubCategoria'];
	fputcsv($fp, $mH1,',','"');
	
	if($mPars['etiqueta']!='TOTS')
	{
		$mH1[0]='- nom�s es mostren els productes amb etiqueta: '.$mPars['etiqueta'];
		fputcsv($fp, $mH1,',','"');
	}
	
	if($mPars['etiqueta2']!='CAP')
	{
		$mH1[0]='- NO es mostren els productes amb etiqueta: '.$mPars['etiqueta2'];
		fputcsv($fp, $mH1,',','"');
	}
	
	$mH1[0]='- odre llista: '.$mPars['sortBy'].' - '.$mPars['ascdesc'];
	fputcsv($fp, $mH1,',','"');
	$mH1[0]=' ';
	fputcsv($fp, $mH1,',','"');
	

	
	fputcsv($fp, $mColumnesProductesLlista,',','"');
	
	while(list($key,$mProducteLlista)=each($mProductesLlista))
	{
    	if(!fputcsv($fp, $mProducteLlista,',','"'))
		{
			return false;
		}
	}	
	fclose($fp);
	return;
}
//------------------------------------------------------------------------

db_getLlistaProductesCSV($db);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>Genera el resum actualitzat del periode local seleccionat</title>
</head>
<body>
	<table align='center' style='width:80%'>
		<tr>
			<th style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b></p>
			</th>
		</tr>
	</table>
	<br>
	<br>
	<table align='center' style='width:80%'>
		<tr>
			<td style='width:50%;' align='center'>
			";
			if($mDatesReservesLocals['itimestamp']*1<time()){$iColor='red';}else{$iColor='blue';}
			if($mDatesReservesLocals['ftimestamp']*1<time()){$fColor='red';}else{$fColor='blue';}
			if($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['comandesLocalsTancades']!=1)
			{
				echo "
				<p style='font-size:13px;'>Periode de reserves locals: de <font color='".$iColor."'><b>".(number_format($mDatesReservesLocals['idia']*1,0))."/".(number_format($mDatesReservesLocals['imes']*1,0))."/".(number_format($mDatesReservesLocals['iany']*1,0))."</b></font> a <font color='".$fColor."'><b>".(number_format($mDatesReservesLocals['fdia']*1,0))."/".(number_format($mDatesReservesLocals['fmes']*1,0))."/".(number_format($mDatesReservesLocals['fany']*1,0))."</b></font></p>
				";
			}
			echo "
			</td>
		</tr>
	</table>
	<br>
	<table align='center' style='width:80%'>
		<tr>
			<td style='width:100%;' align='center'>
			<a href='llistaProductes/pre_llistaProductes_".$mPars['usuari_id'].".csv'>resum actualitzat periode local ".$mPars['sel_periode_comanda_local']."</a>
			<br>
			<br>
			";
			if($mPars['veureProductesDisponibles']==1)
			{
				echo "
				<p class='nota'>* Nom�s es mostres els productes <b>actius</b></p>
				";
			}
			else
			{
				echo "
				<p class='nota'>* Nom�s es mostres els productes <b>actius</b> i els productes <b>inactius</b></p>
				";
			}
			echo "
			</td>
		</tr>
	</table>

</body>
</html>

";

?>




		