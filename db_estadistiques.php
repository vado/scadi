<?php
//*v36 20-1-16   modificada funcio

function db_getProductesEstadistiques($selRuta,$db)
{
	global $mPars;

	$orderBy='';
	$where_=" id!=0 "; //redundant
	
//*v36-15-11-26 condicio
	if($mPars['sortBy']!='')
	{
		$orderBy=$mPars['sortBy'];
		
		if($mPars['sortBy']=='productor')
		{
			$orderBy=" (SUBSTRING(productor,LOCATE('-',productor))) ";
		}
		
		$orderBy=" order by ".$orderBy." ".$mPars['ascdesc'];
	}

	if($mPars['veureProductesDisponibles']==1)
	{
		$where_.=" AND actiu=1 ";
	}

	//aplicacio del filtre
	if($mPars['etiqueta']==$mPars['etiqueta2']){$mPars['etiqueta']='TOTS';}

	if($mPars['etiqueta2']!="CAP"){$mPars['etiqueta']='TOTS';}

	if($mPars['vProductor']!='TOTS')
	{
		if(str_replace(',','',$mPars['vProductor'])!=''){$where_productors=' AND (';$where_productors_fi=') ';}else{$where_productors='';$where_productors_fi='';}
		
		$mProductorsId=explode(',',$mPars['vProductor']);
		while(list($key,$productorId)=each($mProductorsId))
		{
			if($productorId!='')
			{
				$where_productors.=" SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$productorId."' OR ";
			}
		}
		if($where_productors!='')
		{
			$where_productors=substr($where_productors,0,strlen($where_productors)-3);
		}
		$where_.=$where_productors.$where_productors_fi;
	}

	if($mPars['vCategoria']!='TOTS')
	{
		$where_.=" AND  categoria0='".$mPars['vCategoria']."' ";
		
	}

	if($mPars['vSubCategoria']!='TOTS')
	{
		$categoria0=substr($mPars['vSubCategoria'],0,strpos($mPars['vSubCategoria'],'-'));
		$categoria10=substr($mPars['vSubCategoria'],strpos($mPars['vSubCategoria'],'-')+1);

		$where_.=" AND  categoria0='".$categoria0."' AND categoria10='".$categoria10."' ";
	}
	
	if($mPars['etiqueta']!='TOTS')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta']."',tipus)!=0 ";
	}

	if($mPars['etiqueta2']!='CAP')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta2']."',tipus)=0 ";
	}

	$where_.=" AND llista_id=".$mPars['selLlistaId']." ";
	
	$mProductes=array();
	
	//echo "<br>select * from productes_".$selRuta." where ".$where_." ".$orderBy;
	if(!$result=mysql_query("select * from productes_".$selRuta." where ".$where_." ".$orderBy,$db))
	{
		//echo "<br> 19 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mPars['paginacio']==1)
			{
				if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
				{
					$mRow['visible']=1;
					$mProductes[$mRow['id']]=$mRow;
				}
				else
				{
					$mRow['visible']=0;
					$mProductes[$mRow['id']]=$mRow;
				}
			}
			else
			{
				$mRow['visible']=1;
				$mProductes[$mRow['id']]=$mRow;
			}
			
			$i++;
		}
	}
	return $mProductes;
}
//------------------------------------------------------------------------------
function db_getConsum($db)
{
	global 	$mPars,
			$mSelRutes,
			$mProductes,
			$sb,
			$sbd,
			$sg,
			$sZ,
			$sSz,
			$mProductesNoTrobats;	
	$mConsum=array();
	$rutaActual=$mPars['selRutaSufix'];
	$cont=0;
	$condicioRebosts=" rebost!='0-CAC' ";
	while(list($key,$selRuta)=each($mSelRutes))
	{
		//$mPars['selRutaSufix']=$selRuta;
		if(isset($sSz) && $sSz!='' && $sSz!='TOTS')
		{
			$grupsSelSzChain=db_getGrupsSzona($sSz,$db);
			$condicioRebosts.=" AND LOCATE(CONCAT(',',(SUBSTRING(rebost,1,LOCATE('-',rebost)-1)),','),' ".$grupsSelSzChain."')>0 ";
		}
		else if(isset($sZ) && $sZ!='' && $sZ!='TOTS')
		{
			$grupsSelZchain=db_getGrupsZona($sZ,$db);
			$condicioRebosts.=" AND LOCATE(CONCAT(',',(SUBSTRING(rebost,1,LOCATE('-',rebost)-1)),','),' ".$grupsSelZchain."')>0 ";
		}
		else if(isset($sg) && $sg!='' && $sg!='0' && $sg!='TOTS')
		{
			$condicioRebosts.=" AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$sg."' ";
		}
		
		//echo "<br>select * from comandes_".$selRuta." where ".$condicioRebosts." AND usuari_id!='0' order by id ASC";
		if(!$result=mysql_query("select * from comandes_".$selRuta." where ".$condicioRebosts." AND usuari_id!='0' order by id ASC",$db))
		{
			//echo "<br> 28 estadistiques.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		
			return false;
		}
		else
		{
			$mProductes=db_getProductesEstadistiques($selRuta,$db);

			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))		
			{
				$mProductesComanda=explode(';',$mRow['resum']);
				for($i=0;$i<count($mProductesComanda);$i++)
				{
					$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
					$index=1*str_replace('producte_','',$mIndexQuantitat[0]);
					$quantitat=@$mIndexQuantitat[1];
					
					if($index!='' && $index!=0)
					{
						if(!isset($mProductes[$index]))
						{
							if(!isset($mProductesNoTrobats[$selRuta])){$mProductesNoTrobats[$selRuta]=array();}
							array_push($mProductesNoTrobats[$selRuta],$index);
						}
						else
						{
							if(!isset($mConsum[$index]))
							{
							$mConsum[$index]=array();
							$mConsum[$index]['index']=$index;
							$mConsum[$index]['ms']=number_format($mProductes[$index]['ms'],2);
							$mConsum[$index]['producte']=$mProductes[$index]['producte'];
							$mConsum[$index]['productor']=$mProductes[$index]['productor'];
							$mConsum[$index]['quantitat']=$quantitat;
							$mConsum[$index]['unitat_facturacio']=$mProductes[$index]['unitat_facturacio'];
							$mConsum[$index]['pes']=number_format($quantitat*$mProductes[$index]['pes'],2);
							$mConsum[$index]['ums']=number_format($quantitat*$mProductes[$index]['preu'],2);
							$mConsum[$index]['ecos']=number_format($quantitat*$mProductes[$index]['preu']*$mProductes[$index]['ms']/100,2);
							$mConsum[$index]['euros']=number_format($quantitat*$mProductes[$index]['preu']*(100-$mProductes[$index]['ms'])/100,2);
							}
							else
							{
							$mConsum[$index]['quantitat']+=$quantitat;
							$mConsum[$index]['pes']+=number_format($quantitat*$mProductes[$index]['pes'],2);
							$mConsum[$index]['ums']+=number_format($quantitat*$mProductes[$index]['preu'],2);
							$mConsum[$index]['ecos']+=number_format($quantitat*$mProductes[$index]['preu']*$mProductes[$index]['ms']/100,2);
							$mConsum[$index]['euros']+=number_format($quantitat*$mProductes[$index]['preu']*(100-$mProductes[$index]['ms'])/100,2);
							}
						}
					}
				}
			}
		}

		$mPars['selRutaSufix']=$rutaActual;
		
		//generar matrius ordenades per clau
		$mConsumX=array();
		
		while(list($index,$mVal)=each($mConsum))
		{
			if(!isset($mConsumX[$mVal[$sb]])){$mConsumX[$mVal[$sb]]=array();}
			$mConsumX[$mVal[$sb]][$mVal['index']]=$mVal;
		}
		reset($mConsum);

		if(in_array($sb,array('producte','productor','unitat_facturacio')))
		{
			ksort($mConsumX,SORT_STRING);
		}
		else
		{
			ksort($mConsumX,SORT_NUMERIC);
		}

		if($sbd=='descendent')
		{
			$mConsumX=array_reverse($mConsumX,true);
		}
	}
	reset($mSelRutes);

	
	return $mConsumX;
}

//------------------------------------------------------------------------

function db_getEstadistiquesCSV($db)
{
	global $mPars,$mConsumXcsv,$mSb,$mSelRutes,$sb,$sbd,$sg,$mGrupsRef,
//*v36 20-1-16 2 decls globals
	$sSz,$sZ;
	
	$dir0=getcwd();
	$dir="docs".$mPars['selRutaSufix'];
	if(!@chdir($dir))
	{
		mkdir($dir);
	}
	else
	{
		chdir($dir0);
	}
	@unlink("docs".$mPars['selRutaSufix']."/estadistiques_".$mPars['usuari_id'].".csv");

	if(!$fp=fopen("docs".$mPars['selRutaSufix']."/estadistiques_".$mPars['usuari_id'].".csv",'w'))
	{
		return false;
	}

	$mH1=array('','resum sobre rutes: '.(implode(',',$mSelRutes)));
	fputcsv($fp, $mH1,',','"');
//*v36 20-1-16 2 linies csv
	if($sSz!='' && $sSz!='TOTS' )
	{
		$mH1=array('','SuperZona: '.$sSz);
		fputcsv($fp, $mH1,',','"');
	}
	if($sZ!='' && $sZ!='TOTS' )
	{
		$mH1=array('','Zona: '.$sZ);
		fputcsv($fp, $mH1,',','"');
	}
	if($sg!='')
	{
		$mH1=array('',(urldecode($mGrupsRef[$sg]['nom'])));
		fputcsv($fp, $mH1,',','"');
	}
	$mH1=array('','ordenat per: '.$sb);
	fputcsv($fp, $mH1,',','"');
	$mH1=array('','en sentit: '.$sbd);
	fputcsv($fp, $mH1,',','"');
	$mH1=array('',date('d/m/Y hh:mm'));
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	fputcsv($fp, $mSb,',','"');
	while(list($key,$mProducteLlista)=each($mConsumXcsv))
	{
    	if(!fputcsv($fp, $mProducteLlista,',','"'))
		{
			return false;
		}
	}	
	reset($mConsumXcsv);
	fclose($fp);
	return;
}

?>

		