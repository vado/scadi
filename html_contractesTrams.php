<?php

//------------------------------------------------------------------------------
function taulaFiltres()
{
	global $mPars,$mVehicles,$mCategories,$mCategoria10,$mCategoria0,$mUsuarisTrams,$mUsuarisRef;

	echo "
		<table  bgcolor='#9EeC7B' >
			<tr>
				<td align='left' valign='bottom' style='width:20%;'>
				<a title=\"Selecciona un usuari per veure nom�s els seus trams\">
				<select id='sel_vUsuari' onChange=\"javascript: f_vistaUsuari();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mUsuariId)=each($mUsuarisTrams))
		{
			if($mPars['vUsuariId']==$mUsuariId['usuari_id']){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." value='".$mUsuariId['usuari_id']."'>".(urldecode($mUsuarisRef[$mUsuariId['usuari_id']]['usuari']))."</option>
			";
		}
		echo "
				<option ".$selected2." value='TOTS'>- tots usuaris actius -</option>
		";
		reset($mUsuarisTrams);
		echo "
				</select>
				</a>		
				</td>

				<td align='left' valign='bottom' style='width:20%;'>
				<a title=\"Selecciona un vehicle per veure nom�s els seus trams\">
				<select id='sel_vVehicle' onChange=\"javascript: f_vistaVehicle();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$mVehicle)=each($mVehicles))
		{
			if($mPars['vVehicle']==$mVehicle['id']){$selected='selected';$selected2='';}else{$selected='';}
			if($mVehicle['marca']=='' && $mVehicle['model']=='')
			{
				$vehicleText=$mVehicle['vehicle'];
			}
			else
			{
				$vehicleText=$mVehicle['marca']." ".$mVehicle['model']." (".$mUsuarisRef[$mVehicle['usuari_id']]['usuari'].")";
			}
			echo "
				<option ".$selected." value='".$mVehicle['id']."'>".(urldecode($vehicleText))."</option>
			";
		}
		echo "
				<option ".$selected2." value='TOTS'>- tots els Vehicles -</option>
		";
		reset($mVehicles);
		echo "
				</select>
				</a>		
				</td>

				<td align='left' valign='bottom' style='width:20%;'>
				<a title=\"Selecciona una categoria principal per veure nom�s els trams d'aquesta categoria\">
				<select id='sel_vCategoria' onChange=\"javascript: f_vistaCategoria();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$categoria)=each($mCategoria0[$mPars['nivell']]))
		{
			if($mPars['vCategoria']==$categoria){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." value='".$categoria."'>".$categoria."</option>
			";
		}
		echo "
				<option ".$selected2." value='TOTS'>- totes les categories -</option>
		";
		reset($mCategoria0[$mPars['nivell']]);
		echo "
				</select>
				</a>		
				</td>

				<td align='left' valign='bottom' style='width:20%;'>
				<a title=\"Selecciona una categoria10 per veure nom�s els trams d'aquesta categoria\">
				<p>
				<select id='sel_vSubCategoria' onChange=\"javascript: f_vistaSubCategoria();\">
		";
		$selected='';
		$selected2='selected';
		while(list($key,$subCategoria)=each($mCategoria10[$mPars['nivell']]))
		{
			if($mPars['vSubCategoria']==$subCategoria){$selected='selected';$selected2='';}else{$selected='';}
			echo "
				<option ".$selected." value='".$subCategoria."'>".$subCategoria."</option>
			";
		}
		echo "
				<option ".$selected2." value='TOTS'>- totes les SUB-categories -</option>
		";
		reset($mCategoria10[$mPars['nivell']]);
		echo "
				</select>
				</p>
				</a>		
				</td>

				<td align='left' valign='bottom' style='width:20%;'>
				</td>

				<td align='left' valign='bottom' style='width:20%;'>
				</td>
			</tr>
		</table>
	";
	return;
}



//----------------------------------------
function mostrarDades($db)
{
	global 	$mPars,$mUsuarisRef,$mParametres,$mVehiclesUsuari,$mColors;
	
	echo "
		<table border='0' width='100%' bgcolor='".$mColors['table']."'>
			<tr>
				<td  valign='middle' align='left' width='5%'>
				<a title='P�gina Serveis de Transport'><img src='imatges/cotxep.gif' ALT=\"p�gina d'usuari\" style='cursor:pointer' onClick=\"javascript: enviarFpars('contractesTrams.php?tip=".$mPars['vTipus']."','_self')\"></a>
				</td>
				
				<td  valign='middle'  align='left' width='15%'>
				<p class='compacte' >
				Usuari:<b> ".(urldecode($mPars['usuari']))."</b> (".$mPars['nivell'].")
				</p>
				</td>
				
				<td width='10%' align='middle''>
				</td>
				<td width='30%'>
				<table width=100%'>
					<tr>
						<td id='td_missatgeAlerta' style=' width:100%;' align='center'  valign='bottom'>
						</td>
					</tr>
					<tr>
						<td id='td_recordatoriGuardarContractes' align='center' width='100%' valign='bottom'>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>

		<table border='0' width='100%' bgcolor='".$mColors['table']."'>
			<tr>
				<td  valign='middle' align='left' width='30%'>
				</td>
				
				<td  valign='middle' align='left' width='30%'>
				</td>
				
				<td  valign='middle' align='right' width='40%'>
				<table border='0' align='right' >
					<tr>
						<td  valign='bottom' align='center' >
						<p class='compacte' onClick=\"javascript: enviarFpars('contractesTrams.php?tip=OFERTES','_self');\" 	style='color:#885500; cursor:pointer;'><u>OFERTES</u></p>
						</td>
						<td  valign='bottom' align='center' >
						<p class='compacte' style='color:#885500;'>|</p>
						</td>
						<td  valign='bottom' align='center' >
						<p class='compacte' onClick=\"javascript: enviarFpars('contractesTrams.php?tip=DEMANDES','_self');\" 	style='color:#885500; cursor:pointer;'><u>DEMANDES</u></p>
						</td>
						";
						if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
						{
							echo "
						<td  valign='bottom' align='center' >
						<p class='compacte' style='color:#885500;'>|</p>
						</td>
						<td  valign='bottom' align='left' >
						<p class='compacte' onClick=\"javascript: enviarFpars('resumOfertesDemandesCAC.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>[CAC] Resum d'ofertes i demandes contractades </u></p>
						<p class='compacte' onClick=\"javascript: enviarFpars('resumOfertesDemandes.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>Resum d'ofertes i demandes contractades </u></p>
						</td>
							";
						}
						else if ($mPars['usuari_id']=='417')//usuari 'cac'
						{
							echo "
						<td  valign='bottom' align='center' >
						<p class='compacte' style='color:#885500;'>|</p>
						</td>
						<td  valign='bottom' align='left' >
						<p class='compacte' onClick=\"javascript: enviarFpars('resumOfertesDemandesCAC.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>Resum d'ofertes i demandes contractades </u></p>
						</td>
							";
						}
						else
						{
							echo "
						<td  valign='bottom' align='center' >
						<p class='compacte' style='color:#885500;'>|</p>
						</td>
						<td  valign='bottom' align='left' >
						<p class='compacte' onClick=\"javascript: enviarFpars('resumOfertesDemandes.php','_blank');\" 	style='color:#885500; cursor:pointer;'><u>Resum d'ofertes i demandes contractades </u></p>
						</td>
							";
						}
						echo "
					</tr>
				</table>
				</td>
			</tr>
		</table>
		
		<table   bgcolor='#9CE6E3' bordercolor='#A2CBCA' border='0' width='100%' align='center' bgcolor='".$mColors['table']."'>
			<tr>
				<td  width='100%' valign='middle' align='left'>
				<table align='center' width='100%'>
					<tr>
						<td align='left' width='100%'>
						<table style='width:100%;'>
		";
		if($mPars['vTipus']=='DEMANDES')
		{
			echo "
							<tr>
								<td  style='width:20%;' valign='bottom' align='left'>
								<table style='width:100%;'>
									<tr>
										<td style='width:100%;' align='left'>
										<p class='p_transp_titol'>
										<b><i>Necessites un vehicle?
										<br>
										Has de trasportar alguna cosa?
										<br>
										Vols organitzar un viatge?</i></b>
										</p>
										</td>
									</tr>
									<tr>
										<td>
										<p>Introdueix una <b>DEMANDA de transport</b>:</p>
										<p onClick=\"javascript: enviarFpars('gestioTramsDemandes.php','_self');\" 	style='color:#885500; cursor:pointer;'><u>generar/editar demandes</u></p>
										</td>
									</tr>
								</table>
								</td>

								<td  style='width:30%;'  align='left' valign='top'>
								<table style='width:100%;'>
									<tr>
										<td style='width:50%;' valign='top'>
										<p  class='p_transp_titol'><b><i>Disposes d'un vehicle? </b></i></p>
										</td>
									</tr>
									<tr>
										<td valign='top'>
										<p>Dona'l d'alta i podr�s respondre a les demandes de transport oferint el teu vehicle:</p>
										<p onClick=\"javascript: enviarFpars('gestioVehicles.php?vUs=".$mPars['usuari_id']."','_self');\" 	style='color:#885500; cursor:pointer;'><u>donar d'alta/editar un vehicle</u>
										</p>
										</td>
									</tr>
								</table>
								</td>

								<td  style='width:50%;' valign='middle' align='left'>
								<p  class='p_transp_titol'><b>
								* Per respondre a les ".$mPars['vTipus']." d'una altra usuaria, 
								selecciona les unitats que li ofereixes transportar 
								- persones(places), pes(kg), volum(litres) i clica 'guardar'.
								<br>
								<br>
								El propietari de la demanda de transport a la que responguis rebr� 
								un av�s via email perqu� accepti o descarti el teu oferiment. 
								<br>
								Quan ho faci rebr�s tamb� un av�s al teu correu.
								</b></p>
								</td>

								<td  style='width:30%;' valign='middle' align='left'>
								</td>
			";
		}
		else if($mPars['vTipus']=='OFERTES')
		{
			echo "
							<tr>
								<td  style='width:30%;' valign='top' align='left'>
								<table style='width:100%;'>
									<tr>
										<td style='width:50%;' align='left' valign='top'>
										<p class='p_transp_titol'><b><i>Preveus fer algun transport?<br>Fas algun trajecte regularment?<br>Vols compartir despeses?</i></b></p>
										</td>
									</tr>
									<tr>
										<td valign='top'>
										<p>Introdueix una <b>OFERTA de transport</b>:
			";
			if(count($mVehiclesUsuari)>0)
			{
				echo "
										<p class='p_micro'>
										<select id='sel_vehicleUsuari' onChange=\"javascript:if(this.value!=''){enviarFpars('gestioTramsOfertes.php?&vId='+this.value+'&vUs=".$mPars['usuari_id']."','_self');}\">
				";
				while(list($vehicleId,$mVehicle)=each($mVehiclesUsuari))
				{
					if($mVehicle['actiu']==1)
					{
						echo "
										<option value='".$mVehicle['id']."'>".(urldecode($mVehicle['marca']))." ".(urldecode($mVehicle['model']))."</option>
						";
					}
				}
				reset($mVehiclesUsuari);
		
				echo "
										<option selected value=''></option>
										</select>
										<br>
										* selecciona el teu vehicle per generar/editar ofertes
										</p>
				";
			}
			else
			{
				echo "
										<p class='pAlertaNo4'>[ Encara no tens cap vehicle donat d'alta i<br> <b>ACTIU</b> per generar ofertes de transport ]</p>
				";
			}
			echo "
										</td>
									</tr>
								</table>
								</td>

								<td  style='width:30%;'  align='left' valign='top'>
								<table style='width:100%;'>
									<tr>
										<td style='width:50%;' valign='top'>
										<p  class='p_transp_titol'><b><i>Disposes d'un vehicle? </b></i></p>
										</td>
									</tr>
									<tr>
										<td valign='top'>
										<p>Dona'l d'alta i podr�s afegir ofertes de transport:</p>
										<p onClick=\"javascript: enviarFpars('gestioVehicles.php?vUs=".$mPars['usuari_id']."','_self');\" 	style='color:#885500; cursor:pointer;'><u>donar d'alta/editar un vehicle</u>
										</p>
										</td>
									</tr>
								</table>
								</td>

								<td  style='width:50%;' valign='middle' align='left'>
								<p  class='p_transp_titol'><b>
								* Per respondre a les ".$mPars['vTipus']." d'una altra usuaria, 
								selecciona les unitats que necessites transportar 
								- persones(places), pes(kg), volum(litres) i clica 'guardar'.
								<br>
								<br>
								El propietari de l'oferta de transport a la que responguis rebr� 
								un av�s via email perqu� accepti o descarti el teu oferiment. 
								<br>
								Quan ho faci rebr�s tamb� un av�s al teu correu.
								</b></p>
								</td>
			";
		}
		echo "
							</tr>
						</table>
						</td>
					</tr>
				</table>
					
				</td>
			</tr>

		</table>
	";

	return;
}







//-------------------------------------------------------
function html_paginacio($index)
{
	global $mPars,$mTrams,$mParametres;

	echo "
	<table  align='center'>
		<tr>
			<td style='width:50px;' align='center'>
			";
	$botoText="guardar contractes";
	if
	(
		$mPars['nivell']!='visitant'
		&& 
		(
			$mPars['nivell']=='admin' || $mPars['nivell']=='sadmin'
			|| 
			(
				!$mParametres['contractamentTransportTancat']['valor']
			)
		)
	)
	{
		
		echo "	
				<table>
					<tr>
						<td align='center'>
						<table>	
							<tr>
								<td align='center'>
								<p>
		";
		if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' && $mParametres['contractamentTransportTancat']['valor'])
		{
			echo "
								<font style='color:#FC7202;'>[".$mPars['nivell']."]</font>";
		}
		echo "
								</p>
								</td>
							</tr>
						</table>
						</td>
						<td>
						<table id='t_img_guardar_".$index."'>
							<tr>
								<td align='center'>
								<p id='p_img_guardar_".$index."' style='text-decoration:blink; color:red;'></p>
								<a title='".$botoText."'><img src='imatges/guardarpp.gif' style='cursor:pointer;' onClick=\"guardarContractes();\" value='".$botoText."'></a></p>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
		";
	}
	echo "
			</td>
			<td>
			<p>p�gina:</p>
			</td>
			<td> 
			<img src='imatges/page_downp.gif' alt='seleccionar p�gina anterior' style='cursor:pointer;' onClick=\"javascript:avansarPag(-1);\">
			</td>
			<td>
			<select onChange=\"javascript:selectPag(this.value)\";>
	";
	$selected='';
	for($i=1;$i<=$mPars['numPags'];$i++)
	{
		if($mPars['pagS']*1==$i-1)
		{
			$selected='selected';
		}
		else
		{
			$selected='';
		}
		echo "
			<option ".$selected." value='".($i-1)."'>".$i."</option>
		";
	}	
	
	echo "
			</select>
			</td>
			<td>
			<img src='imatges/page_upp.gif' alt='seleccionar p�gina seg�ent' style='cursor:pointer;' onClick=\"javascript:avansarPag(1);\">
			</td>
			<td>
			<p>items/pag:<input type='text' size='2'  id='i_itemsPag'  onChange=\"javascript: saveItemsPag(this.value);\" value='".$mPars['numItemsPag']."'></p>
			</td>
		</tr>
	</table>
	";


	return;
}

//-------------------------------------------------------
function mostrarFormTrams()
{
	global  $db,
			$login,
			$ruta0,
			$strRuta,
			$thumbCode,
			$mColors,
			$mContractes,
			$mContractesCopiar,
			$mContracteTrams,
			$mContracteTramsAltri,
			$mEtiquetes,
			$mFormesPagament,
			$mNomsColumnes,
			$mParametres,
			$mPars, 
			$mTrams, 
			$mUsuarisRef,
			$mVehicles,
			$mVehiclesUsuari,
			$mMunicipis;
			
	//--------------------------------------------------------
	// contractament en curs ---------------------------------------
	// totals
	echo "
	<table id='t_principal' align='center'  style=' width:100%;' bgcolor='".$mColors['table']."'>
		<tr>
			<td style=' width:100%;' valign='top'>
			<table  border='0' bgcolor='#9EeC7B' style=' width:100%;'>
				<tr>
					<td width='100%' valign='top' align='left'>
	";
	taulaFiltres();
	echo "
					</td>
				</tr>
			</table>
			</td>
		</tr>

		<tr>
			<td style=' width:100%;'>
			<center>
		<div style=' width:100%;'>
		<table style=' width:100%;'>
			<tr>
				<td style=' width:33%;'>
	";
	html_paginacio(1);
	echo "
				</td>
				<td  style='width:33%;'>
				<p style='font-size:11px;'>(* <i>trams ordenats per <b>".$mPars['sortBy']."</b> - ".$mPars['ascdesc']."</i>)</p>
				</td>
				<td  style='width:33%;'>
				</td>
			</tr>
		</table>

		<table border='1'  align='center' valign='top' style=' width:100%;'>
	";
  	$classTitolColumnes='titolColumnesNoCAC';

	if(count($mTrams)>0 && $mTrams[0]!=NULL)
	{
	
		// nom columnes:--------------------------------

		echo "
			<tr>
		";
		while(list($key,$val)=each($mTrams[0]))
		{	
			if(array_key_exists($key,$mNomsColumnes))
			{
			
				echo "
				<td bgcolor='#AEFC8B' valign='bottom'>
				<table  align='center' valign='bottom'>
					<tr>
						<th  align='center' valign='bottom'>
						<p class='".$classTitolColumnes."'>".$mNomsColumnes[$key]."</p>

						<table align='center' valign='bottom'>
							<tr>
								<td >
								<p style='cursor:pointer;' onClick=\"sortBy('".$key."','DESC');\"><b>&darr;</b></p>
								</td>
								<td>
								<p style='cursor:pointer;' onClick=\"sortBy('".$key."','ASC');\"><b>&uarr;</b></p>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				";
			}
		}
		reset($mTrams[0]);
		echo "
				<th bgcolor='#AEFC8B'  align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Quant.<br>Pes</p><br>
				</th>
				
				<th bgcolor='#AEFC8B'  align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Quant.<br>Volum</p><br>
				</th>
				
				<th bgcolor='#AEFC8B'  align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Quant.<br>Places</p><br>
				</th>
				
				<th bgcolor='#AEFC8B' align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Preu<br>(ums)</p><br>&nbsp;
				</th>
				
				<th bgcolor='#AEFC8B' align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Preu<br>(ecos)</p><br>&nbsp;
				</th>
				
				<th bgcolor='#AEFC8B' align='center' valign='bottom' style='width:5%;'>
				<p class='".$classTitolColumnes."'>Preu<br>(euros)</p><br>&nbsp;
				</th>
			</tr>
		";		
	
		
		// Trams: -------------------------------------

		$contLinies=0;
		$blocLinies=6;
		$linies=1;
		reset($mTrams);
		if(count($mTrams)>0)
		{
		while (list($key0,$val0)=each($mTrams))
		{	
			$mSortida=array();

			$date = new DateTime();
			$t1=$date->getTimestamp();
			$mSortida['Y']=date('Y',strtotime($mTrams[$key0]['sortida']));
			$mSortida['m']=date('m',strtotime($mTrams[$key0]['sortida']));
			$mSortida['d']=date('d',strtotime($mTrams[$key0]['sortida']));
			$mSortida['h']=date('H',strtotime($mTrams[$key0]['sortida']));
			$mSortida['i']=date('i',strtotime($mTrams[$key0]['sortida']));

			$instantSortida = new DateTime();
			$instantSortida->setDate($mSortida['Y'], $mSortida['m'], $mSortida['d'] );
			$instantSortida->setTime($mSortida['h'], $mSortida['i'], 0 );
			$t2=$instantSortida->getTimestamp();

			if($t2-$t1<0){$mTrams[$key0]['caducada']=1;}else{$mTrams[$key0]['caducada']=0;}
			

			//recordatori de noms de columna:
			if($contLinies==$blocLinies)
			{
				$contLinies=0;	
				echo "
			<tr>
				";
			
				while(list($key,$val)=each($mTrams[0]))
				{	
						echo "
				<th  bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p  class='".$classTitolColumnes."'><i>".$key."</i></p>
				</th>
						";
				}
				reset($mTrams[0]);

				echo "
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:5%;'>
				<p  class='".$classTitolColumnes."'><i>Quantitat<br>pes<br>(kg)</i></p>
				</th>
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:5%;'>
				<p  class='".$classTitolColumnes."'><i>Quantitat<br>volum(l.)</i></p>
				</th>
				<th bgcolor='#AEFC8B'  align='center' valign='top' style='width:5%;'>
				<p  class='".$classTitolColumnes."'><i>Quantitat<br>places</i></p>
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>Preu<br>(ums)</i></p>
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>Preu<br>(ecos)</i></p>
				</th>
				<th bgcolor='#AEFC8B' align='center' valign='top' style='width:5%;'>
				<p class='".$classTitolColumnes."'><i>Preu<br>(euros)</i></p>
				</th>
			</tr>
				";		
			}

			
			if($mTrams[$key0]['visible']==1)
			{
				if($mTrams[$key0]['tipus']=='DEMANDES')
				{
					$classTitolColumnes='demandes';
					$classMicro='microDemandes';
					$textTipus='demanda';
				}
				else
				{
					$classTitolColumnes='ofertes';
					$classMicro='microOfertes';
					$textTipus='oferta';
				}
				echo "
			<tr id='tr_filaTram_".$key0."'>
				";
				while(list($key,$val)=each($mTrams[0]))
				{	
					if(array_key_exists($key,$mNomsColumnes))
					{
						echo "
				<td  id='td_tram".$key."_".$key0."' style='width:5%;' valign='top'>
						";
				
						 if($key=='categoria0' || $key=='categoria10' )
						{
							echo " 
				<table >
					<tr>
						<td >
						<p   class='".$classTitolColumnes."'
						";
						if($mTrams[$key0]['categoria0']=='CAC' || $mTrams[$key0]['categoria0']=='CAC-personal')
						{
							echo " style='background-color:orange; color:white; text-align:center;' ";
						}
						echo "
						>
						".(urldecode($mTrams[$key0][$key]))."
						</p>
						";
						if($mTrams[$key0]['caducada']==1)
						{
							echo "
							<p style='color:red;'>[<b><i>caducada</b>]</p>
							";
						}
						echo "
						</td>
					</tr>
				</table>
							";
						} 
						else if($key=='tipus')
						{
							echo " 
				<table >
					<tr>
						<td >
						<p  class='".$classTitolColumnes."'>
						<b>".(posaColor1(urldecode($mTrams[$key0][$key])))."</b>
						</p>
						</td>
					</tr>
				</table>
							";
						} 
						else if($key=='vehicle_id')
						{
							$classEstat=$classTitolColumnes;
							if($mPars['usuari_id']!=$mTrams[$key0]['usuari_id']) // no es el propietari del tram
							{
								while(list($key2,$mContracteTram)=each($mContracteTrams))
								{
									if($mContracteTram['index']==$mTrams[$key0]['id'])			// i es part contractant del tram
									{
										if($mContracteTram['estat']=='pendent')
										{
											$classEstat='pPeticioPendentGST';
											$textConfirmacio="<p  class='".$classEstat."'><b>".(urldecode($mUsuarisRef[$mTrams[$key0]['usuari_id']]['usuari']))."</b> encara <b><i>no ha acceptat ni descartat</i></b> la teva resposta a la seva <b>".$textTipus."</b></p>";
										}
										else if($mContracteTram['estat']=='acceptat')
										{
											$classEstat='pPeticioConfirmatGST';
											$textConfirmacio="<p  class='".$classEstat."'><b>".(urldecode($mUsuarisRef[$mTrams[$key0]['usuari_id']]['usuari']))."</b> <b><i>ha acceptat</i></b> la teva resposta a la seva <b>".$textTipus."</b></p>";
										}
										else
										{
											$classEstat='';
											$textConfirmacio="ni pendent ni acceptada";
										
										}
										echo " 
				<table width='250px;'>
					<tr>
						<td width='100%'>
						".$textConfirmacio."
						</td>
					</tr>
				</table>
										";
									}
								}
								reset($mContracteTrams);
							}
							else //es el propietari del tram, cal que gestioni les peticions
							{
								$classEstat=$classTitolColumnes;
								$classEstat='pPeticioPendentGST';
								$tStyleEstat=" style='background-color:orange; width:100%;' ";									
								if(@count($mContracteTramsAltri['pendents'][$mTrams[$key0]['id']])>0)
								{
									echo "
				<p class='p_micro'>Respostes a la teva <b>".$textTipus."</b> que encara no has acceptat <b>".count($mContracteTramsAltri['pendents'][$mTrams[$key0]['id']])."</b>:</p>
				<table ".$tStyleEstat." width='250px;' border='1'>
									";
									while(list($indexContracte,$mContracteTramAltri)=each($mContracteTramsAltri['pendents'][$mTrams[$key0]['id']]))
									{
										echo "
					<tr>
						<td width='100%'>
						<table  width='100%'>
							<tr>
								<td width='25%' align='left'>
								<p  class='".$classEstat."' style='text-align:left;'>
								".(urldecode($mUsuarisRef[$mContracteTramAltri['usuari_id']]['usuari']))."
								</p>
								</td>

								<td width='25%' align='left'>
								<p  class='".$classEstat."' style='text-align:left;'>
								".$mContracteTramAltri['quantitat_pes']." kg
								</p>
								</td>
							
								<td width='25%' align='left'>
								<p  class='".$classEstat."' style='text-align:left;'>
								".$mContracteTramAltri['quantitat_volum']." litres
								</p>
								</td>
							
								<td width='25%' align='left'>
								<p  class='".$classEstat."' style='text-align:left;'>
								".$mContracteTramAltri['quantitat_places']." places
								</p>
								</td>
							</tr>

							<tr>
								<td align='left'>
								</td>

								<td align='left'>
								<p class='p_micro2'><input type='checkbox' id='cb_respostes_p_".$mPars['usuari_id']."_".$mTrams[$key0]['id']."_".$mContracteTramAltri['usuari_id']."_c' onClick=\"javascript:cb_confirmarResposta('".$mPars['usuari_id']."','".$mTrams[$key0]['id']."','".$mContracteTramAltri['usuari_id']."','c');\" value='0'>&nbsp;confirmar</p>
								</td>
							
								<td align='left'>
								<p class='p_micro2'><input type='checkbox' id='cb_respostes_p_".$mPars['usuari_id']."_".$mTrams[$key0]['id']."_".$mContracteTramAltri['usuari_id']."_d' onClick=\"javascript:cb_confirmarResposta('".$mPars['usuari_id']."','".$mTrams[$key0]['id']."','".$mContracteTramAltri['usuari_id']."','d');\" value='0'>&nbsp;descartar</p>
								</td>
							
								<td align='left'>
								<input class='i_micro' type='button' onClick=\"javascript: guardarConfirmacioResposta('".$mPars['usuari_id']."','".$mTrams[$key0]['id']."','".$mContracteTramAltri['usuari_id']."','".$mContracteTramAltri['id']."');\" value='enviar'>
								</td>
							
							</tr>
						</table>
						</td>
					</tr>
										";
									}
									reset($mContracteTramsAltri['pendents'][$mTrams[$key0]['id']]);
									echo "
				</table>
									";
								}

								$classEstat='pPeticioConfirmatGST';
								$tStyleEstat=" style='background-color:green; width:100%;' ";
								if(@count($mContracteTramsAltri['confirmades'][$mTrams[$key0]['id']])>0)
								{
								echo "<p class='p_micro'>Respostes a la teva ".$textTipus." que has acceptat <b>".count($mContracteTramsAltri['confirmades'][$mTrams[$key0]['id']])."</b>:</p>";
									echo " 
				<table ".$tStyleEstat." width='250px;'  border='1'>
									";
									while(list($indexContracte,$mContracteTramAltri)=each($mContracteTramsAltri['confirmades'][$mTrams[$key0]['id']]))
									{
										echo "
					<tr>
						<td>
						<table>
							<tr>
								<td align='left'>
								<p  class='".$classEstat."' style='text-align:left;'>
								".(urldecode($mUsuarisRef[$mContracteTramAltri['usuari_id']]['usuari']))."
								</p>
								</td>

								<td align='left'>
								<p  class='".$classEstat."' style='text-align:left;'>
								".$mContracteTramAltri['quantitat_pes']." kg
								</p>
								</td>
							
								<td align='left'>
								<p  class='".$classEstat."' style='text-align:left;'>
								".$mContracteTramAltri['quantitat_volum']." litres
								</p>
								</td>
							
								<td align='left'>
								<p  class='".$classEstat."' style='text-align:left;'>
								".$mContracteTramAltri['quantitat_places']." places
								</p>
								</td>
							</tr>
						</table>
						</td>
					</tr>
										";
									}
									reset($mContracteTramsAltri['confirmades'][$mTrams[$key0]['id']]);
									echo "
				</table>
									";
								}
							}
						
							if($mTrams[$key0]['tipus']=='OFERTES')
							{
								echo " 
				<table id='t_vehicle_".$key0."' width='250px;'>
					<tr>
						<td >
								";
								if($mTrams[$key0][$key]!='')
								{
									echo "
									<table style='width:100%;'>
										<tr>
											<td valign='middle'>
											<img src='vehicles/".$mVehicles[$mTrams[$key0][$key]]['id']."/th_".$mVehicles[$mTrams[$key0][$key]]['imatge']."'>
											</td>
											
											<td valign='middle'>
											<p  class='".$classTitolColumnes."'><b>".urldecode($mVehicles[$mTrams[$key0][$key]]['marca'])." - ".(urldecode($mVehicles[$mTrams[$key0][$key]]['model']." (".$mUsuarisRef[$mVehicles[$mTrams[$key0][$key]]['usuari_id']]['usuari'])).")</b></p>
											</td>
										</tr>
									</table>
									";
								}
								echo "
						</p>
						</td>
					</tr>
				</table>
					";
							}						
							else if($mTrams[$key0]['tipus']=='DEMANDES')
							{
							
								if
								(
									$mPars['nivell']=='visitant'
									||
									(
										$mParametres['contractamentTransportTancat']['valor']=='1'
										&&
										$mPars['nivell']!='admin'
										&&
										$mPars['nivell']!='sadmin'
									)
									||
									(
										$mPars['usuari_id']==$mTrams[$key0]['usuari_id']
									)
								)
								{
									$disabled='DISABLED';
								} 
								else
								{
									if($mTrams[$key0]['places_disponibles']==0 && $mTrams[$key0]['pes_disponible']==0 && $mTrams[$key0]['volum_disponible']==0)
									{
										$disabled='DISABLED';
									} 
									else
									{
										$disabled='';
									}
								}
								echo " 
				<table id='t_vehicle_".$key0."' style='width:250px;' >
					<tr>
						<td style='width:100%;' >
						<p  class='".$classMicro."'>
						";
						if($mTrams[$key0]['usuari_id']==$mPars['usuari_id'])
						{ 
							$nota="";
						}
						else if(count($mVehiclesUsuari)==0)
						{ 
							$disabled='DISABLED';
							$nota="<i><b> No has afegit cap vehicle ></b></i> <a title=\"clica per donar d'alta un vehicle\" style='cursor:pointer; font-size:10px;' onClick=\"javascript: enviarFpars('gestioVehicles.php?vUs=".$mPars['usuari_id']."','_self');\">afegir</a>";
						}
						else
						{
							$nota='';
						}
						
						if($mTrams[$key0]['usuari_id']!=$mPars['usuari_id'])
						{
							echo "
						<select ".$disabled." id='sel_vehicle_".$key0."' 	name='sel_vehicle_".$key0."' size='1' onChange=\"javascript:selectVehicle('".$key0."');\">
							";
							$selected1='';
							$selected2='selected';
							while(list($index,$mVehicle)=each($mVehiclesUsuari))
							{
								if($mVehicle['id']==$mTrams[$key0]['vehicle_id']){$selected1='selected';$selected2='';}else{$selected1='';}
								echo "
						<option ".$selected1." value='".$mVehicle['id']."'>".(urldecode($mVehicle['marca']." ".$mVehicle['model']))." (".(urldecode($mUsuarisRef[$mVehicle['usuari_id']]['usuari'])).")</option>
								";
								
								//$mVehicles[$mTrams[$key0][$key]]['marca']."<br>(".$mVehicles[$mTrams[$key0][$key]]['model'].",".$mUsuarisRef[$mVehicles[$mTrams[$key0][$key]]['usuari_id']]['usuari'].")";
							}
							reset($mVehiclesUsuari);
							echo "
						<option ".$selected2." value=''></option>
						</select>".$nota."
							";
						}
						echo "
						</p>
						</td>
					</tr>
				</table>
						";
					}
				echo "
				<table width='250px;'>
					<tr>
						<td width='100%;'>
						<table  width='100%;'>
							<tr>
								<td align='left'>
								<img border='1' style='cursor:pointer;' src='imatges/lupap.gif' onClick=\"javascript:mostrarAcords(".$key0.");\">
								<div id='d_acords_explicits_".$key0."' style='z-index:0; background-color:#ffffff; position:absolute; visibility:hidden;'>
								<table>
									<tr>
										<td>
										<p class='p_micro'>".(urldecode($mTrams[$key0]['acords_explicits']))."</p>
										</td>
									</tr>
								</table>
								</div>
								</td>
							</tr>
						</table>
						<table  width='100%;'>
							<tr>
								<td align='left'>
								<p  class='".$classMicro."'>
								places no assignades:
								</p>
								</td>
								<td align='left'>
								";
								if($mTrams[$key0]['places_disponibles']>0){$class='tramDisponible';}else{$class='tramNoDisponible';}
								echo "
								<p  class='".$class."'>
								<b>".$mTrams[$key0]['places_disponibles']."</b>/".$mTrams[$key0]['capacitat_places']." places
								</p>
								</td>
							</tr>

							<tr>
								<td align='left'>
								<p  class='".$classMicro."'>
								pes no assignat:
								</p>
								</td>
								<td align='left'>
								";
								if($mTrams[$key0]['pes_disponible']>0){$class='tramDisponible';}else{$class='tramNoDisponible';}
								echo "
								<p  class='".$class."'>
								<b>".$mTrams[$key0]['pes_disponible']."</b>/".$mTrams[$key0]['capacitat_pes']." kg
								</p>
								</td>
							</tr>
						
							<tr>
								<td align='left'>
								<p  class='".$classMicro."'>
								volum no assignat:
								</p>
								</td>
								<td align='left'>
								";
								if($mTrams[$key0]['volum_disponible']>0){$class='tramDisponible';}else{$class='tramNoDisponible';}
								echo "
								<p  class='".$class."'>
								<b>".$mTrams[$key0]['volum_disponible']."</b>/".$mTrams[$key0]['capacitat_volum']." l.
								</p>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				";

						} 
						else if($key=='municipi_origen' || $key=='municipi_desti')
						{
							echo " 
				<table>
					<tr>
						<td>
						<p  class='".$classTitolColumnes."'>".(urldecode($mMunicipis[$mTrams[$key0][$key]]))."</p>
						</td>
					</tr>
				</table>
							";
						}
						else if($key=='municipis_ruta')
						{
							$mMunicipisRuta=explode(',',$mTrams[$key0][$key]);
							echo " 
				<table>
					<tr>
						<td>
							";
							while(list($key,$val)=each($mMunicipisRuta))
							{
								if($val!='' && $val!='undefined')
								{
									echo "
							<p  class='".$classTitolColumnes."' style='font-size:10px;'>".(substr(urldecode($mMunicipis[$val]),0,20))."</p>
									";
								}
							}
							reset($mMunicipisRuta);
							echo "
						</td>
					</tr>
				</table>
							";
						}
						else if($key=='usuari_id')
						{
							if($mTrams[$key0][$key]==$mPars['usuari_id'])
							{
								echo " 
				<table>
					<tr>
						<td>
						<p  class='".$classTitolColumnes."' style='color:red;'><b><i>".(urldecode($mUsuarisRef[$mTrams[$key0][$key]]['usuari']))."</b></i></p>
						</td>
					</tr>
				</table>
								";
							}
							else
							{
								echo " 
				<table>
					<tr>
						<td>
						<p  class='".$classTitolColumnes."'>".(urldecode($mUsuarisRef[$mTrams[$key0][$key]]['usuari']))."</p>
						</td>
					</tr>
				</table>
								";
							}
						}
						else if($key=='id')
						{
							echo " 
				<table>
					<tr>
						<td>
						<p  class='".$classTitolColumnes."'a>".(urldecode($mTrams[$key0][$key]))."</p>
						";
						if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
						{
							if
							(
								$mTrams[$key0]['pes_disponible']!=$mTrams[$key0]['capacitat_pes']
								||
								$mTrams[$key0]['volum_disponible']!=$mTrams[$key0]['capacitat_volum']
								||
								$mTrams[$key0]['places_disponibles']!=$mTrams[$key0]['capacitat_places']
							)
							echo "
							<p class='p_micro2'>[".$mPars['nivell']."]</p>
							<a title='Anul.lar contractes'><input type='button' style='font-size:10px;' onClick=\"javascript:enviarFpars('contractesTrams.php?tip='+vTipus+'&opt=delt&tId=".$mTrams[$key0]['id']."','_self');\" value='xContractes'></a>
							";
						}
						echo "
						</td>
					</tr>
				</table>
							";
						}
						else
						{
							echo " 
				<table>
					<tr>
						<td>
						<p  class='".$classTitolColumnes."'a>".(urldecode($mTrams[$key0][$key]))."</p>
						</td>
					</tr>
				</table>
							";
						}
					echo "
				</td>
					";
					}
				}
				reset($mTrams[0]);  
				
	
				if
				(
					$mPars['nivell']=='visitant'
					||
					(
						$mParametres['contractamentTransportTancat']['valor']
						&&
						$mPars['nivell']!='admin'
						&&
						$mPars['nivell']!='sadmin'
					)
					||
					(
						$mPars['usuari_id']==$mTrams[$key0]['usuari_id']
					)
				)
				{
					$disabled='disabled';
				} 
				else
				{
					$disabled='';
				}

				//el tram esta solicitat per l'usuari i acceptat per la part contractada?						
				$contractatIacceptat=false;
				$quantitatInici=0;
				while(list($key,$mContracteTram)=each($mContracteTrams))
				{
					if($mContracteTram['index']==$mTrams[$key0]['id'])
					{
						if($mContracteTram['estat']=='acceptat')
						{
							$contractatIacceptat=true;
						}
						else
						{
							$contractatIacceptat=false;
						}
					}
				}
				reset($mContracteTrams);

				echo "
				<td   id='td_tramQuantitatPes_".$key0."' valign='top'>
				<p  class='".$classTitolColumnes."'>
				<select ".$disabled." onChange=\"selectTram('".$key0."','pes');\" id='sel_quantitat_pes_".$key0."' name='sel_quantitat_pes_".$key0."'>
				";
				if($contractatIacceptat)
				{
					$quantitatInicial=$mTrams[$key0]['quantitat_pes'];
					$quantitatFinal=$quantitatInicial;
				}
				else
				{
					$quantitatInicial=0;
					$quantitatFinal=$mTrams[$key0]['pes_disponible']+$mTrams[$key0]['quantitat_pes'];
				}
				
				for($j=$quantitatInicial;$j<=$quantitatFinal;$j++)
				{
					if(@$mTrams[$key0]['quantitat_pes']==$j){$selected='selected';}else{$selected='';}
					echo "
				<option ".$selected." value='".$j."'>".$j."</option>
					";
				}
				echo "
				</select>
				<br>(kg)</p>
				<p  class='".$classMicro."'>
				".$mTrams[$key0]['preu_pes']."<br>ums/kg
				</p>
				</td>
				
				<td   id='td_tramQuantitatVolum_".$key0."' valign='top'>
				<p  class='".$classTitolColumnes."'>
				<select ".$disabled." onChange=\"selectTram('".$key0."','volum');\" id='sel_quantitat_volum_".$key0."' name='sel_quantitat_volum_".$key0."'>
				";
				if($contractatIacceptat)
				{
					$quantitatInicial=$mTrams[$key0]['quantitat_volum'];
					$quantitatFinal=$quantitatInicial;
				}
				else
				{
					$quantitatInicial=0;
					$quantitatFinal=@$mTrams[$key0]['volum_disponible']+@$mTrams[$key0]['quantitat_volum'];
				}
				
				for($j=$quantitatInicial;$j<=$quantitatFinal;$j++)
				{
					if(@$mTrams[$key0]['quantitat_volum']==$j){$selected='selected';}else{$selected='';}
					echo "
				<option ".$selected." value='".$j."'>".$j."</option>
					";
				}
				echo "
				</select>
				<br>(litres)</p>
				<p  class='".$classMicro."'>
				".$mTrams[$key0]['preu_volum']."<br>ums/l.
				</p>
				</td>
				
				<td   id='td_tramQuantitatPlaces_".$key0."' valign='top'>
				<p  class='".$classTitolColumnes."'>
				<select ".$disabled." onChange=\"selectTram('".$key0."','places');\" id='sel_quantitat_places_".$key0."' name='sel_quantitat_places_".$key0."'>
				";
				if($contractatIacceptat)
				{
					$quantitatInicial=$mTrams[$key0]['quantitat_places'];
					$quantitatFinal=$quantitatInicial;

				}
				else
				{
					$quantitatInicial=0;
					$quantitatFinal=$mTrams[$key0]['places_disponibles']+$mTrams[$key0]['quantitat_places'];
				}
				
				for($j=$quantitatInicial;$j<=$quantitatFinal;$j++)
				{
					if($mTrams[$key0]['quantitat_places']==$j){$selected='selected';}else{$selected='';}
					echo "
				<option ".$selected." value='".$j."'>".$j."</option>
					";
				}
				echo "
				</select>
				<br>(places)</p>
				<p  class='".$classMicro."'>
				".$mTrams[$key0]['preu_places']."<br>ums/pla�a
				</p>
				</td>

				<td id='td_tramPreuUnitats_".$key0."'  align='right' style='width:10%;'  valign='top'>
				<p>0</p>
				</td>
				
				<td id='td_tramPreuEcos_".$key0."'  align='right' style='width:10%;'  valign='top'>
				<p>0</p>
				</td>
				
				<td id='td_tramPreuEuros_".$key0."'  align='right' style='width:10%;'  valign='top'>
				<p>0</p>
				</td>
				";
			}
		}
		reset($mTrams);
		}
		else
		{
			echo "
		<center><p>No hi ha trams en aquesta recerca</p></center>
			";
		}
	}
	
	echo "		
		</table>
		<table style=' width:100%;'>
			<tr>
				<td style=' width:33%;'>
			";
			html_paginacio(2);
			echo "
				</td>
				<td  style='width:33%;'>
				<p style='font-size:11px;'>(* <i>trams ordenats per <b>".$mPars['sortBy']."</b> - ".$mPars['ascdesc']."</i>)</p>
				</td>
				<td  style='width:33%;'>
				</td>
			</tr>
		</table>
		</div>
		</center>
		</td>
	</tr>
</table>
	";

	
	return;
}





//------------------------------------------------------------------------------
function html_mostrarNotesTrams()
{
	global $vTipus,$mPars;
	
		echo "
	<table style='width:70%;' align='center'>
		<tr>
			<td style='width:100%;' align='center'>
			<p class='nota2'>
<b>Funcionament:</b>
<br><br>
- Els usuaris no poden contractar les seves propies ".$vTipus.";
<br><br>
- Els usuaris poden contractar ".$vTipus." de categories 'CAC', 'CAC-personal' i 'CAC-ruta' que, tot i ser especifiques per la CAC, encara poden oferir recursos a la resta d'usuaries. 
En aquest cas, es la part contractada qui ha de confirmar cada resposta que rebi prioritzant l'usuari 'cac'.
<br><br>
- Quan es contracti una oferta o demanda de transport, la part contractada rebr� un avis per correu i un av�s a trav�s del gestor de serveis de transport (GST), amb la informaci� de contacte de la part contractant
<br><br>
- La part contractada haur� de confirmar o rebutjar la petici� de la part contractant des del GST
<br><br>
- Mentre la part contractada no confirmi o rebutgi una petici�, els recursos solicitats (places, kg i volum a transportar) per la part contractant quedaran reservats per aquesta i no estaran disponibles per a d'altres usuaris.
<br><br>
- La part contractant veur� l'estat(pendent o confirmat) de la seva petici� des del GST, al tram corresponent
<br><br>
- La part contractant podr� anul.lar la seva petici� mentre la part contractada no l'hagi confirmat.
<br><br>
- Quan la part contractada confirmi la peticio es considerar� realitzat el contracte, i la part contractant rebr� un missatge de confirmaci�.
<br><br>
- Si s'ha realitzat un contracte entre les dues parts i una de les parts el vol anul.lar, caldr� que es posi d'acord amb l'altra part per solicitar a l'administrador que anul.li el contracte.
<br><br>
- Si no hi ha acord, es considerar� que es la part contractant qui comet la falta de comprom�s. De moment no hi ha altres conseq��ncies.
</p>
<br>
<p class='nota2'>
<b>Ofertes i demandes <b>caducades</b>:</b>
<br>
<br>
- Si la data i hora de sortida d'una oferta o demanda de transport es anterior a l'instant actual, aquesta ja no es mostrar�, 
excepte en el cas que sigui de categoria 'CAC', 'CAC-personal' o 'CAC-ruta'; Per temes de registre comptable de despeses de 
ruta l'usuari CAC i la resta d'usuaries poden contractar ofertes i demandes caducades que siguin d'alguna d'aquestes categories. 
Se seguir� utilitzant en aquests casos el sistema de confirmaci� de la part contractada i notificacions via mail a ambdues parts.
<br>
</p>
<br><br><br>&nbsp;

			</td>
		</tr>
		";
	return;
}

?>

		