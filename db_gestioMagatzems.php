<?php

//------------------------------------------------------------------------------
function getRebostsAmbComandaDinsZona($mRebostsAmbComanda,$db)
{
	global $mPars,$mCategoriesGrup;
	
	$mRebostsDinsZona=array();
	if(!$result=mysql_query("select ref from rebosts_".$mPars['selRutaSufix']." where LOCATE('".$mCategoriesGrup[2]."',categoria)>0",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mRebostsDinsZona[$i]=$mRow['ref'];
			$i++;
		}
	}
	
	$i=0;
	while(list($key,$grupId)=each($mRebostsAmbComanda))
	{
		if(in_array($grupId,$mRebostsDinsZona))
		{
			$mRebostsAmbComanda2[$i]=$grupId;
			$i++;
		}
	}
	reset($mRebostsAmbComanda);	
	
	return $mRebostsAmbComanda2;
}


//------------------------------------------------------------------------------
function db_getDistribucioSegonsReserves($db)
{
	global  $mPars,
			$mRebost,
			$mRebostsRef,
			$mUsuarisRef,
			$mPuntsEntrega,
			$mCategoriesGrup,
			$mProductes_,
			$jaEntregatTkg,
			$jaEntregatTuts,
			$mProductesJaEntregats;	//aqui, $mRebosts son els usuaris magatzem

	$mRebostsAmbComanda=db_getRebostsAmbComanda($db);
	$mComandaTotalReserves=array();
	$mComandaTotalReserves2=array();
	if($mPars['grup_id']=='0') //CAC - Total reserves
	{
			$mComanda=array();
				if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where usuari_id!='0' AND rebost!='0-CAC'",$db))
				{
					//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
				}
				else
				{ 
					while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
					{
						$mProductesComanda=explode(';',$mRow['resum']);
						for($i=0;$i<count($mProductesComanda);$i++)
						{
							$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
							$index=str_replace('producte_','',$mIndexQuantitat[0]);
							$quantitat=@$mIndexQuantitat[1];
							if($index!='' && $index!=0)
							{	
							/*	if($index=='333')
								{
									echo "<br>comandaId:".$mRow['id'];
									echo "<br>producteId:".$index;
									echo "<br>grup:".$mRebostsRef[$grupId]['nom'];
									echo "<br>usuari:".$mUsuarisRef[$mRow['usuari_id']]['usuari'];
									echo "<br>quantitat:<b>".$quantitat."</b>";
								}
							*/
								if(!isset($mComandaTotalReserves[$index])){$mComandaTotalReserves[$index]=0;}
								$mComandaTotalReserves[$index]+=$quantitat;
							}
						}
					}
					//$mComandes[$mRow['rebost']]=$mComanda;
				
			
			}
	}
	else //magatzems - distribuci� te�rica segons comandes actuals, per zones vinculades als usuaris magatzem ST-
	{
		$mGrupsAmbComandaPerZonesPuntsEntrega=db_getGrupsAmbComandaPerZonesPuntsEntrega($db);
		//obtenir la zona coberta pel magatzems actual
		while(list($zona,$mGrupsAmbComandaPerPuntsEntrega)=each($mGrupsAmbComandaPerZonesPuntsEntrega))
		{
			if($zona==$mCategoriesGrup[2])
			{
				while(list($refPuntEntrega,$mGrupsAmbComanda)=each($mGrupsAmbComandaPerPuntsEntrega))
				{
					while(list($refgrup,$mGrupAmbComanda)=each($mGrupsAmbComanda))
					{
						$mComanda=db_getComanda($refgrup,$db);
						if(count($mComanda)>0)
						{
							while(list($key,$mComandaProducte)=each($mComanda))
							{			
								if($mComandaProducte['index']!='' && $mComandaProducte['index']!=0)
								{	
									if(!isset($mComandaTotalReserves[$mComandaProducte['index']])){$mComandaTotalReserves[$mComandaProducte['index']]=0;}
									$mComandaTotalReserves[$mComandaProducte['index']]+=$mComandaProducte['quantitat'];
								}
							}
						}
					}
					reset($mGrupsAmbComanda);
				}
				reset($mGrupsAmbComandaPerPuntsEntrega);
			}
		}
		reset($mGrupsAmbComandaPerZonesPuntsEntrega);
	}

	//conversio a format $mComanda:
	$n=0;
	while(list($index,$quantitat)=each($mComandaTotalReserves))
	{
			if(array_key_exists($index,$mProductesJaEntregats) && array_key_exists($index,$mProductes_))
			{
				while(list($puntEntrega,$mProductesJaEntregatsPE)=each($mProductesJaEntregats[$index]))
				{
					while(list($grupId,$mProductesJaEntregatsGrup)=each($mProductesJaEntregatsPE))
					{
						while(list($incId,$mVal)=each($mProductesJaEntregatsGrup))
						{
							if($mPars['grup_id']=='0')
							{
								if($mPars['excloureProductesJaEntregats']=='1')
								{
									$quantitat-=$mVal['rebut'];
								}
								$jaEntregatTkg+=$mVal['rebut']*$mProductes_[$index]['pes'];
								$jaEntregatTuts+=$mVal['rebut'];
				    		}
							else
							{
								if($mCategoriesGrup[2]==db_getZonaGrup($puntEntrega,$db))
								{
									if($mPars['excloureProductesJaEntregats']=='1')
									{
										$quantitat-=$mVal['rebut'];
									}
									$jaEntregatTkg+=$mVal['rebut']*$mProductes_[$index]['pes'];
									$jaEntregatTuts+=$mVal['rebut'];
								}
							}
						}
						reset($mProductesJaEntregatsGrup);
					}
					reset($mProductesJaEntregatsPE);
				}
				reset($mProductesJaEntregats[$index]);
			}
		$mComandaTotalReserves2[$n]['index']=$index;
		$mComandaTotalReserves2[$n]['quantitat']=$quantitat;
		$n++;
	}
	reset($mComandaTotalReserves);
	
	return $mComandaTotalReserves2;
}

//---------------------------------------------------------------
function db_getInventariCopiar($grupRef,$db)
{
	global $mPars;

	if(!$result=@mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." where magatzem_ref='".$grupRef."' order by id DESC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		
		return false;
	}
	else
	{ 
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		return $mRow;
	}
	
	return false;
}

//---------------------------------------------------------------
function db_getEstocGlobalProducte($id,$db)
{
	global $mPars;

	$mMagatzems=array();
	
	//obtenir magatzems
	if(!$result=mysql_query("select magatzem_ref from inventaris_".(substr($mPars['selRutaSufix'],0,2))." where periode_comanda='".$mPars['periode_comanda']."' AND LOCATE('producte_".$id.":',CONCAT(' ',resum))>0",$db))
	{
		//echo "<br> 214 db_gestioMagatzems ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			if(!in_array($mRow[0],$mMagatzems)){array_push($mMagatzems,$mRow[0]);}
		}
	}
	$val=0;
	while(list($key,$magatzem_ref)=each($mMagatzems))
	{
		$result=mysql_query("select SUBSTRING(resum,LOCATE('producte_".$id.":',resum)) from inventaris_".(substr($mPars['selRutaSufix'],0,2))." WHERE magatzem_ref='".$magatzem_ref."' AND periode_comanda='".$mPars['periode_comanda']."' AND LOCATE('producte_".$id.":',CONCAT(' ',resum))>0 order by id DESC limit 1",$db);
		$mRow=mysql_fetch_array($result,MYSQL_NUM);
		$val_=str_replace('producte_'.$id.':','',$mRow[0]);
		$val_=substr($val_,0,strpos($val_,';'));
		$val+=$val_*1;
	}
	reset($mMagatzems);
	return $val;

}

//---------------------------------------------------------------
function db_guardarInventari($guardarInventari,$db) //nom�s magatzems, no CAC
{
	global $mParametres,$mProductes,$mPars,$mRebost;

		$mInventariAnteriorG=db_getInventari($mPars['grup_id'],$db); //arriben tots els productes demanats amb quantitat>0
		$mInventariAnteriorCopiar=db_getInventariCopiar($mPars['grup_id'],$db);
		$mInventariCombinat=array();
		//$guardarcomanda: nom�s arriben els productes de la seleccio visualitzada, siguin en quantitat=0 o  quantitat>0

		//combinar l'inventari anterior amb l'inventari enviat ($guardarInventari)
		$mProductesInventari=explode(';',$guardarInventari);
		$n=0;
		for($i=0;$i<count($mInventariAnteriorG['inventari']);$i++)
		{
			$mInventariCombinat[$mInventariAnteriorG['inventari'][$i]['index']]['anterior']=$mInventariAnteriorG['inventari'][$i]['quantitat'];
			$mInventariCombinat[$mInventariAnteriorG['inventari'][$i]['index']]['actual']=$mInventariAnteriorG['inventari'][$i]['quantitat'];
		}
		for($i=0;$i<count($mProductesInventari);$i++)
		{
			$mIndexQuantitat=explode(':',$mProductesInventari[$i]);
		
			$index=str_replace('producte_','',$mIndexQuantitat[0]);
			$quantitat=@$mIndexQuantitat[1];
			if($index!='')
			{
				//si existia a Inventari anterior guardar-hi la nova quantitat >/=0
				if(array_key_exists($index,$mInventariCombinat))
				{
					$mInventariCombinat[$index]['actual']=$quantitat;
				}
				else	//si no, i es >0, afegir
				{
					if($quantitat>0)
					{
						$mInventariCombinat[$index]['anterior']=0;
						$mInventariCombinat[$index]['actual']=$quantitat;
					}
				}
			}
		}
		//refer cadena Inventari i passar Inventari combinat a Inventari
		$chain='';
		$n=0;
		while(list($index,$mQuantitat)=each($mInventariCombinat))
		{
			if($mQuantitat['actual']>0)
			{
				$chain.='producte_'.$index.':'.$mQuantitat['actual'].';';
			}
		}
		reset($mInventariCombinat);
		
		$guardarInventari=$chain;
		if($mPars['grup_id']!='0')
		{
			// 1. guardar a bd l'Inventar, nou o vell
			// si n'existeix un, guardar alla (l'�ltim)
			if(isset($mInventariAnteriorCopiar['id']) && date('m-y')==substr($mInventariAnteriorCopiar['periode_comanda'],3,5))
			{
				if(!$result=mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set data='".(date('dmY'))."',resum='".$guardarInventari."',autor='".$mPars['usuari']."' where id='".$mInventariAnteriorCopiar['id']."'",$db))
				{
					//echo "<br> 172 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
			
    				return false;
				}
			}
			else //si no, crea'l, nou (ex: quan un magatzem s'acaba de crear o quan cambiem de mes)
			{
				if(!$result=mysql_query("insert into inventaris_".(substr($mPars['selRutaSufix'],0,2))." values('','".(date('YmdHis'))."','".$mParametres['periodeComanda']['valor']."','".$mRebost['ref']."','".$mRebost['nom']."','".(date('dmY'))."','".$guardarInventari."','".$mPars['usuari']."','')",$db))
				{
					//echo "<br> 172 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
			
    				return false;
				}
			}
		}
	
		$mInventariG=db_getInventari($mPars['grup_id'],$db);

		$revisio='';
		//guardar les diferencies no justificades en el registre d'aquest inventari
		while(list($index,$mQuantitat)=each($mInventariCombinat))
		{
			if($mQuantitat['actual']>$mQuantitat['anterior'])
			{
				$revisio.="[TEIO;".(time()).";".$mPars['usuari'].";".$mPars['grup_id'].";".$index.";".($mQuantitat['actual']-$mQuantitat['anterior']).";".$index.";(AUTO-INV)(Abastiment);EA]";
			}
			else if($mQuantitat['actual']<$mQuantitat['anterior'])
			{
				$revisio.="[TEIO;".(time()).";".$mPars['usuari'].";".$mPars['grup_id'].";".$index.";".($mQuantitat['anterior']-$mQuantitat['actual']).";".$index.";(AUTO-INV)(Distribuci�);SD]";
			}
		}
		reset($mInventariCombinat);
		$result=@mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set revisions=CONCAT('".$revisio."',revisions) where id='".$mInventariG['id']."'",$db);
		//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
		
	return true;
}

//*v36-6-12-15 funcio sencera
//---------------------------------------------------------------
function db_getInventari($magatzemRef,$db) //nom�s magatzems, no CAC
{
	global $mProductes,$mPars,$mMagatzems;
	
	$mInventariGL=array();
	$mInventariGL['data']='';
	$mInventariGL['autor']='';
	$mInventari_=array();
	$mInventari=array();
	$mInventariRef=array();
	$mPeriodes=array();
	
	$mParametres=getParametres($db);
	
	if($magatzemRef!='0')
	{
		//obtenir periodes de precomanda DESC:
		//echo "<br>select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." WHERE magatzem_ref='".$magatzemRef."' AND periode_comanda='".$mParametres['periodeComanda']['valor']."' ORDER BY id DESC limit 0,1";
		if(!$result=@mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." WHERE magatzem_ref='".$magatzemRef."' AND periode_comanda='".$mParametres['periodeComanda']['valor']."' ORDER BY id DESC limit 0,1",$db))
		{
			//echo "<br> 264 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
		}
		else
		{
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

			if(isset($mRow))
			{
				$mProductesInventari=explode(';',$mRow['resum']);
				$n=0;
				for($i=0;$i<count($mProductesInventari);$i++)
				{
					$mIndexQuantitat=explode(':',$mProductesInventari[$i]);
					$index=str_replace('producte_','',$mIndexQuantitat[0]);
					$quantitat=@$mIndexQuantitat[1];
					if($index!='' && $index!=0)
					{
						$mInventari[$n]['index']=$index;
						$mInventari[$n]['quantitat']=$quantitat;
						$mInventariRef[$index]=$quantitat;
						$n++;
					}
				}
		
				$mInventariGL['id']=$mRow['id'];
				$mInventariGL['resum']=$mRow['resum'];
				$mInventariGL['data']='{'.$mRow['data'].';'.$mRow['periode_comanda'].'}';
				$mInventariGL['autor']=$mRow['autor'];
				$mInventariGL['inventari']=$mInventari;
				$mInventariGL['revisions']=$mRow['revisions'];
				$mInventariGL['magatzem_ref']=$mRow['magatzem_ref'];
				$mInventariGL['magatzem_nom']=$mRow['magatzem_nom'];
				ksort($mInventariRef);
				$mInventariGL['inventariRef']=$mInventariRef;
			}
		}
	}
	else // inventari CAC, tots els magatzems
	{
		while(list($key,$mMagatzem)=each($mMagatzems))
		{
			$mPeriodes[$mMagatzem['ref']]=array();
			//obtenir periodes de precomanda DESC:
			if(!$result=@mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." WHERE magatzem_ref='".$mMagatzem['ref']."'  AND periode_comanda='".$mParametres['periodeComanda']['valor']."' ORDER BY id DESC limit 0,1",$db))
			{
				//echo "<br> 264 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
			}
			else
			{
				$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

				if(isset($mRow))
				{
					$mProductesInventari=explode(';',$mRow['resum']);
					for($i=0;$i<count($mProductesInventari);$i++)
					{
						$mIndexQuantitat=explode(':',$mProductesInventari[$i]);
						$index=str_replace('producte_','',$mIndexQuantitat[0]);
						$quantitat=@$mIndexQuantitat[1];
						if($index!='' && $index!=0)
						{	
							if(!isset($mInventari_[$index])){$mInventari_[$index]=0;}
							$mInventari_[$index]+=$quantitat;
						}
					}
					$mInventariGL['data'].='{'.$mRow['magatzem_ref'].';'.$mRow['data'].';'.$mRow['periode_comanda'].'}';
				}
			}
		}
		reset($mMagatzems);
		
		
		//canviar format matriu:
		$n=0;
		while(list($index,$quantitat_)=each($mInventari_))
		{
			$mInventari[$n]['index']=$index;
			$mInventari[$n]['quantitat']=$quantitat_;
			$mInventariRef[$index]=$quantitat_;
			$n++;
		}
		reset($mInventari_);
		$mInventariGL['inventari']=$mInventari;
		ksort($mInventariRef);
		$mInventariGL['inventariRef']=$mInventariRef;
	}
	return 	$mInventariGL;
	
}



//------------------------------------------------------------------------------
function db_getProductesJaEntregats($db) //HTML_gestioMagatzems.php
{
	global $mPars;
	
	$mProductesJaEntregats=array();
	
	if(!$result=@mysql_query("select * from incidencies_".$mPars['selRutaSufix']." WHERE  tipus='jaEntregat' AND sel_usuari_id='0' AND estat='resolta'",$db))
	{
		//echo "<br> 415 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
		
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$puntEntregaId=db_getPuntEntrega($mRow['grup_id'],$db);
				$grupId_=$mPars['grup_id'];
							$mPars['grup_id']=$puntEntregaId;
						$mGrup=getGrup($db);
							$mPars['grup_id']=$grupId_;
			$mCategoriesGrup=@getCategoriesGrup($mGrup,$db);
				
			$sz_=getSuperZona($mCategoriesGrup[2]);
			if(($mPars['sz']!='' && $sz_==$mPars['sz']) || $mPars['sz']=='')
			{
				if(!isset($mProductesJaEntregats[$mRow['producte_id']])){$mProductesJaEntregats[$mRow['producte_id']]=array();}
				if(!isset($mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId]))
				{
					$mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId]=array();
				}
				if(!isset($mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId][$mRow['grup_id']]))
				{
					$mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId][$mRow['grup_id']]=array();
				}
				$mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId][$mRow['grup_id']][$mRow['id']]=$mRow;
			}
		}
	}
	return $mProductesJaEntregats;
}

//------------------------------------------------------------------------------
function db_getProductesJaEntregats2($db) //HTML_vistaReservesSZ.php
{
	global $mPars;
	
	$mProductesJaEntregats=array();
	
	if(!$result=@mysql_query("select * from incidencies_".$mPars['selRutaSufix']." WHERE  tipus='jaEntregat' and sel_usuari_id='0'",$db))
	{
		//echo "<br> 415 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
		
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$puntEntregaId=db_getPuntEntrega($mRow['grup_id'],$db);

			if(!isset($mProductesJaEntregats[$mRow['producte_id']])){$mProductesJaEntregats[$mRow['producte_id']]=array();}
			if(!isset($mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId]))
			{
				$mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId]=array();
			}
			if(!isset($mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId][$mRow['grup_id']]))
			{
				$mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId][$mRow['grup_id']]=array();
			}
			
			$mProductesJaEntregats[$mRow['producte_id']][$puntEntregaId][$mRow['grup_id']][$mRow['id']]=$mRow;
		}
	}
	return $mProductesJaEntregats;
}

//------------------------------------------------------------------------------
function db_getProductesJaEntregatsGrup($db) //HTML_vistaAlbara.php
{
	global $mPars;
	
	$mProductesJaEntregats=array();
	
	if(!$result=@mysql_query("select * from incidencies_".$mPars['selRutaSufix']." WHERE  tipus='jaEntregat' AND grup_id='".$mPars['grup_id']."' AND sel_usuari_id='0'",$db))
	{
		//echo "<br> 415 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
		
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if(!isset($mProductesJaEntregats[$mRow['producte_id']])){$mProductesJaEntregats[$mRow['producte_id']]=array();}
			if(!isset($mProductesJaEntregats[$mRow['producte_id']]))
			{
				$mProductesJaEntregats[$mRow['producte_id']]=array();
			}
			$mProductesJaEntregats[$mRow['producte_id']][$mRow['id']]=$mRow;
		}
	}
	return $mProductesJaEntregats;
}




?>

		