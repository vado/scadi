<?php

function html_formSZones($db)
{
	global 	$mPars,
			$zona,
			$superZona,
			$mGrupsZones,
			$mRebostsRef,
			$jaEntregatTkg,
			$jaEntregatTuts,
			$mRecepcions,
			$mParametres,
			$mGrupsPerSzIzona,
			$mReservesComandesDesti,
			$mReservesComandesOrigen,
			$mReservesCSV,
//*v36-28-11-15 4 declaracions
			$mInfoRutaDistribCsv,
			$mParametres,
			$mUsuarisRef,
			$mMunicipis,
			$mPuntsEntrega,
			$mProductesTots,
			$mInventarisRef,
			$parsChain;
			
			$colorNoSzona='';
			$colorNoZona='';
						
	echo "
	<div>
	<br>
	<p><b> Recepcions a cada punt d'entrega: </b></p>	
	<table  align='center' style='width:60%;' bgcolor='#dddddd'>
		<tr style='background-color:#ffffff;'>
			<th align='left' valign='top'>
			<p class='p_micro2'>super zona</p>
			</th>
			<th align='left' valign='top'>
			<p class='p_micro2'>zona</p>
			</th>
			<th align='left' valign='top'>
			<p class='p_micro2'>punt entrega</p>
			</th>
			<th align='left' valign='top'>
			<p class='p_micro2'>grup</p>
			</th>
			<th  align='left' valign='top'>
			<p  class='p_micro2'>Estat</p>
			</th>
			<th align='left' valign='top'>
			<p class='p_micro2'>pes total (kg)</p>
			</th>
			<th align='left' valign='top'>
			<p class='p_micro2'>albarans</p>
			</th>

		</tr>
			";
			$pesCT=0;
//*v36-28-11-15 4 assignacions
			$pesPE=0;
			$i=0;
			$mInfoRutaDistribCsv[0]=
			array(
				'superZona',
				'zona',
				'punt entrega',
				'data',
				'hora',
				'grup',
				'recepci� acceptada',
				'kg grup',
				'kg punt entrega',
				'adre�a',
				'municipi',
				'email',
				'mobil'
				);
			$i++;
			$mInfoRutaDistribCsv[$i]=array('');
			$i++;
			ksort($mReservesComandesDesti);
			while(list($superZona_,$mReservesZona)=each($mReservesComandesDesti))
			{
				ksort($mReservesZona);
				while(list($zona_,$mReservesPE)=each($mReservesZona))
				{

					ksort($mReservesPE);
					while(list($peId_,$mReservesGrups)=each($mReservesPE))
					{
//*v36-28-11-15 1 assignacio
						$mInfoRutaDistribCsv[$i]=array(
														$superZona_,
														$zona_,
														(urldecode($mRebostsRef[$peId_]['nom'])),
														$mParametres['dataIniciRuta']['valor'],
														'',
														'',
														'',
														'',
														'',
														urldecode($mUsuarisRef[$mRebostsRef[$peId_]['usuari_id']]['adressa']),
														urldecode($mRebostsRef[$peId_]['municipi']),
														$mUsuarisRef[$mRebostsRef[$peId_]['usuari_id']]['email'],
														$mUsuarisRef[$mRebostsRef[$peId_]['usuari_id']]['mobil']
												);
												$i++;
						ksort($mReservesGrups);
						while(list($grupId_,$mReservesGrup)=each($mReservesGrups))
						{
							while(list($sz_,$mSz_)=each($mGrupsZones))
							{
								if(!array_key_exists($superZona_,$mGrupsZones)){$colorNoSzona="style='background-color:red;'";}else{$colorNoSzona='';}

								if
								(
									(
										$superZona==$sz_ 
										&&
										!in_array($zona_,$mSz_)
									)
									|| 
									!array_key_exists($superZona_,$mGrupsZones)
								)
								{
									$colorNoZona="style='background-color:red;'";
								}
								else
								{
									$colorNoZona='';
								}
							}
							reset($mGrupsZones);

							ksort($mReservesGrup);
							echo "
		<tr style='background-color:#ffffff;'>
			<td align='left' valign='top' ".$colorNoSzona.">
			<p class='p_micro2' >".$superZona_."</p>
			</td>
			<td align='left' valign='top' ".$colorNoZona.">
			<p class='p_micro2' >".$zona_."</p>
			</td>
			<td align='left' valign='top'>
			<p class='p_micro2'>".(@urldecode($mRebostsRef[$peId_]['nom']))."</p>
			</td>
			<td align='left' valign='top'>
			";
			if(!isset($mRebostsRef[$grupId_]['nom'])){$color='red';$text='FALTA PUNT ENTREGA';}else{$color='black';$text=(urldecode($mRebostsRef[$grupId_]['nom']));}
			echo "
			<p class='p_micro2' style='color:".$color.";'>".$text."</p>
			</td>
			<td  align='left' valign='top'>
							";
							if(isset($mRecepcions[$grupId_]) && $mRecepcions[$grupId_]['acceptada']=='1')
							{
								echo "<p class='pAlertaOk4'>ACCEPTADA</p>";
							}
							else
							{
								if($peId_=='no_assignat')
								{
									echo "<p class='pAlertaNo4'><i>NO ASSIGNAT</i></p>";
								}
								else if($mPars['usuari_id']==$mRebostsRef[$peId_]['usuari_id'] || $mPars['nivell']=='sadmin')
								{	
									echo "
					<p class='p_micro2'>
					<input type='button' class='i_micro' onClick=\"javascript:enviarFpars('vistaReservesSZ.php?pE=".$peId_."&cRpE=".$grupId_."','_self');\" value='ACCEPTAR'>
									";
									if($mPars['nivell']=='sadmin' && $mPars['usuari_id']!=$mRebostsRef[$peId_]['usuari_id'])
									{
										echo "[SADMIN]";
									}
									echo "
					</p>
									";
								}
								else
								{
									echo "<p class='pAlertaNo4'>PENDENT</p>";
								}
							}
							echo "
			</td>
							";
							$pesC=0;
							while(list($usuariId_,$mReservesUsuari)=each($mReservesGrup))
							{
								if(isset($mReservesUsuari['pesC']))
								{
									$pesC+=$mReservesUsuari['pesC'];
								}
							}
									
							echo "
			<td align='right' valign='top'>
			<p class='p_micro2'>".(number_format($pesC,2,'.',''))."</p>
			</td>
			
			<td align='right' valign='top'>
			<p class='p_micro2' onClick=\"enviarFpars('vistaAlbara.php?sR=".$mPars['selRutaSufix']."&gRef=".$grupId_."&op=totals','_blank');\" style='cursor:pointer;'><u>albar�</u></p>
			</td>
			
		</tr>
							";
							$pesCT+=$pesC;
//*v36-28-11-15 2 assignacions
							$mInfoRutaDistribCsv[$i]=
							array(
								$superZona_,
								$zona_,
								(urldecode($mRebostsRef[$peId_]['nom'])),
								'',
								'',
								$text,
								$mRecepcions[$grupId_]['acceptada'],
								number_format($pesC,2,',','.'),
								'',
								urldecode($mUsuarisRef[$mRebostsRef[$peId_]['usuari_id']]['adressa']),
								urldecode($mRebostsRef[$peId_]['municipi']),
								@$mUsuarisRef[$mRebostsRef[$grupId_]['usuari_id']]['email'],
								@$mUsuarisRef[$mRebostsRef[$grupId_]['usuari_id']]['mobil']
								);
								$i++;
								$pesPE+=$pesC;
						}
						reset($mReservesGrups);	
//*v36-28-11-15 2 assignacions
						$mInfoRutaDistribCsv[$i]=array('','','','','','','','',number_format($pesPE,2,',','.'));
						$pesPE=0;
						$i++;
						$mInfoRutaDistribCsv[$i]=array('');
						$i++;
					}
					reset($mReservesPE);	
				}
				reset($mReservesZona);	
				
				$mInfoRutaDistribCsv[$i]=array('');
				$i++;
				$mInfoRutaDistribCsv[$i]=array('');
				$i++;
				$mInfoRutaDistribCsv[$i]=array('');
				$i++;
				
			}
			reset($mReservesComandesDesti);	
			//$mReservesComandesDesti=array();	
			echo "
		<tr style='background-color:#ffffff;'>
			<th align='left' valign='top'>
			</th>
			<th align='left' valign='top'>
			</th>
			<th align='left' valign='top'>
			</th>
			<th align='left' valign='top'>
			</th>
			<th align='left' valign='top'>
			</th>
			<th align='left' valign='top'>
			<p class='p_micro2'>".(number_format($pesCT,2,'.',''))."</p>
			</th>
			<th align='left' valign='top'>
			</th>
		</tr>
	</table>

	<table  align='center' style='width:30%;' bgcolor='#dddddd'>
	";
	while (list($sz_,$mSz)=each($mGrupsPerSzIzona))
	{
		while (list($z_,$mZ)=each($mSz))
		{
			while (list($key,$grupRef)=each($mZ))
			{
				while (list($puntEntrega,$mReservesComanda)=@each($mReservesComandesDesti))
				{
					if($grupRef==$puntEntrega)
					{
						echo "
		<tr style='background-color:#ffffff;'>
			<th align='left' valign='top' style='width:33%;'>
						";
						if($sz_==$superZona){$class='pAlertaOk4';}else{$class='p_micro2';}
						echo "
			<p  class='".$class."'>".$sz_."</p>
			</th>

			
			<th align='left' valign='top' style='width:33%;'>
						";
						if($z_==$zona){$class='pAlertaOk4';}else{$class='p_micro2';}
						echo "
			<p  class='".$class."'>".$z_."</p>
			</th>

			
			<th align='left' valign='top' style='width:33%;'>
						";
						if($mPars['grup_id']==$grupRef){$class='pAlertaOk4';}else{$class='p_micro2';}
						echo "
			<p  class='".$class."'>".(urldecode($mRebostsRef[$grupRef]['nom']))."</p>
			</th>

			<th align='left' valign='top' style='width:33%;'>
			<p  class='".$class."'>".(urldecode($mRebostsRef[$grupRef]['nom']))."</p>
			</th>
		</tr>
						";
					}
				}
				@reset($mReservesComandesDesti);
			}
			reset($mZ);
		}
		reset($mSz);
	}
	reset($mGrupsPerSzIzona);

	$disabledSelectsDiO='';
	if($mPars['nivell']!='sadmin' && $mPars['nivell']!='admin' && $mPars['nivell']!='coord')
	{
		$disabledSelectsDiO='DISABLED';
	}
	echo "
	</table>
	<a href='docs".$mPars['selRutaSufix']."/reserves/infoFullRutaDistribucio_".$mPars['usuari_id'].".csv'>descarregar info per full ruta distribuci�</a>

	<div>
	<p><b>Selecciona Desti, o Desti i Origen, per veure els productes
	<br> que cal portar de Origen a Desti en la ruta d'abastiment:</b></p>
	<form id='f_origenDesti' method='POST' action='vistaReservesSZ.php' target='_self'>
	<p>- DESTI -</p>
	<table  align='center' style='width:30%;' bgcolor='#dddddd'>
		<tr>
			<td>
			<p>SuperZona:</p>
			<select id='sel_szDesti' name='sel_szDesti'  ".$disabledSelectsDiO." onChange=\"javascript:seleccionarDesti('sel_szDesti');\">
			";
			$selected1='';
			$selected2='selected';
			while (list($sz_,$mSz)=each($mGrupsPerSzIzona))
			{
				if($sz_==$mPars['szDesti']){$selected1='selected';$selected2='';}else{$selected1='';}
				echo "
				<option ".$selected1." value='".$sz_."'>".$sz_."</option>
				";
			}
			reset($mGrupsPerSzIzona);
			echo "
				<option ".$selected2." value=''>- cap superZona -</option>
			</select>
			</td>

			<td>
			<p>Zona:</p>
			<select id='sel_zDesti'  name='sel_zDesti'  ".$disabledSelectsDiO." onChange=\"javascript:seleccionarDesti('sel_zDesti');\">
			";
			$selected1='';
			$selected2='selected';
			while (list($sz_,$mSz)=each($mGrupsPerSzIzona))
			{
				while (list($z_,$mZ)=each($mSz))
				{
					if(@$z_==$mPars['zDesti']){$selected1='selected';$selected2='';}else{$selected1='';}
					echo "
				<option ".$selected1." value='".$z_."'>".$z_."</option>
					";
				}
				reset($mSz);
			}
			reset($mGrupsPerSzIzona);
			echo "
				<option ".$selected2." value=''>- cap Zona -</option>
			</select>
			</td>

			<td>
			<p>Grup:</p>
			<select id='sel_grupDesti' name='sel_grupDesti' ".$disabledSelectsDiO." onChange=\"javascript:seleccionarDesti('sel_grupDesti');\">
			";
			$selected1='';
			$selected2='selected';
			while (list($sz_,$mSz)=each($mGrupsPerSzIzona))
			{
				while (list($z_,$mZ)=each($mSz))
				{
					while (list($key,$grupRef)=each($mZ))
					{
						if(array_key_exists($grupRef,$mRebostsRef))
						{
							if(@$mPars['grupDesti']==$grupRef){$selected1='selected';$selected2='';}else{$selected1='';}
							echo "
				<option ".$selected1." value='".$grupRef."'>".$z_." - ".(urldecode($mRebostsRef[$grupRef]['nom']))."</option>
							";
						}
					}
					reset($mZ);
				}
				reset($mSz);
			}
			reset($mGrupsPerSzIzona);
			echo "
				<option ".$selected2." value=''>- cap Grup -</option>
			</select>
			<td>
			
		</tr>
	</table>
	<br>

	<div id='d_origen'>
	<p>- ORIGEN -</p>
	<table   align='center' style='width:30%;' bgcolor='#dddddd'>
		<tr>
			<td>
			<p>SuperZona:</p>
			<select id='sel_szOrigen' name='sel_szOrigen' ".$disabledSelectsDiO." onChange=\"javascript:seleccionarOrigen('sel_szOrigen');\">
			";
			$selected1='';
			$selected2='selected';
			while (list($sz_,$mSz)=each($mGrupsPerSzIzona))
			{
				if($sz_==$mPars['szOrigen']){$selected1='selected';$selected2='';}else{$selected1='';}
				echo "
				<option ".$selected1." value='".$sz_."'>".$sz_."</option>
				";
			}
			reset($mGrupsPerSzIzona);
			echo "
				<option ".$selected2." value=''>- cap superZona -</option>
			</select>
			</td>

			<td>
			<p>Zona:</p>
			<select id='sel_zOrigen' name='sel_zOrigen' ".$disabledSelectsDiO." onChange=\"javascript:seleccionarOrigen('sel_zOrigen');\">
			";
			$selected1='';
			$selected2='selected';
			while (list($sz_,$mSz)=each($mGrupsPerSzIzona))
			{
				while (list($z_,$mZ)=each($mSz))
				{
					if($z_==$mPars['zOrigen']){$selected1='selected';$selected2='';}else{$selected1='';}
					echo "
				<option ".$selected1." value='".$z_."'>".$z_."</option>
					";
				}
				reset($mSz);
			}
			reset($mGrupsPerSzIzona);
			echo "
				<option ".$selected2." value=''>- cap Zona -</option>
			</select>
			</td>

			<td>
			<p>Grup:</p>
			<select id='sel_grupOrigen' name='sel_grupOrigen' ".$disabledSelectsDiO." onChange=\"javascript:seleccionarOrigen('sel_grupOrigen');\">
			";
			$selected1='';
			$selected2='selected';
			while (list($sz_,$mSz)=each($mGrupsPerSzIzona))
			{
				while (list($z_,$mZ)=each($mSz))
				{
					while (list($key,$grupRef)=each($mZ))
					{
						if($mPars['grupOrigen']==$grupRef){$selected1='selected';$selected2='';}else{$selected1='';}
						echo "
				<option ".$selected1." value='".$grupRef."'>".$z_." - ".(urldecode($mRebostsRef[$grupRef]['nom']))."</option>
						";
					}
					reset($mZ);
				}
				reset($mSz);
			}
			reset($mGrupsPerSzIzona);
			echo "
				<option ".$selected2." value=''>- cap Grup -</option>
			</select>
			<td>
			
		</tr>
	</table>


			<table align='center' width='100%'>
				<tr>
					<td align='center' width='100%'>
					<br>
					<p>Mostrar un sol producte: </p>
					<select id='sel_producte' name='sel_producte'>
	";
	asort($mProductesTots);
	$selected='';
	$selected2='selected';
	while(list($producteId_,$mProducte_)=each($mProductesTots))
	{
		if($producteId_==$mPars['vProducte']){$selected='selected';$selected2='';}else{$selected='';}
		if($mProducte_['actiu']=='1'){$o_class='oGrupActiu';}else{$o_class='oGrupInactiu';}
		echo "
					<option class='".$o_class."' ".$selected." value='".$producteId_."'>".$producteId_." (".(urldecode($mProducte_['productor'])).") ".(urldecode($mProducte_['producte']))."</option>
		";
	}
	reset($mProductesTots);
		
	echo "
					<option ".$selected2." value='TOTS'>- TOTS -</option>
					</select>
					</p>
					";
					if($mPars['vProducte']!='TOTS')
					{
						echo "
					<table bgcolor='#eeeeee'>
						<tr>
							<td valign='top'>
							<p>
							<br>
							Tipus:
							</p>
							</td>
							
							<td valign='top'>
							<p>
							".(posaColor1($mProductesTots[$mPars['vProducte']]['tipus']))."
							</p>
							</td>
						</tr>
					</table>
						";	
						}
					echo "
					<p class='p_nota'>* els productes actius es mostren en verd</p>
					</td>
				</tr>
			</table>
			
				</div>
			<input type='hidden' name='i_pars' value='".$parsChain."'>
			<br>
			<input id='b_enviar' ".$disabledSelectsDiO." type='submit' value='enviar'>
	</form>
	</div>	
	";
	return $pesCT;
}




function html_mostrarProductes($db)
{
	global 	$mPars,
			$mProductes,
			$mProductesOrigen,
			$mPerfilsRef,
			$rc,
			$pE,
			$sz,
			$jaEntregatTkg,
			$jaEntregatTuts,
			$zona,
			$mRebostsRef,
			$mGrupsZones,
			$mInventarisRef,
			$superZona,
			$mReservesCsv,
			$mInfoRutaAbastCsv,
			$origenDestiText,
			$mProductesJaEntregatsZones,
			$destiText,
			$pesSegonsReserves;

			
	$mNomsColumnes=array('id','producte','productor','format','pes','pesT');
	$pesT=0;
	$pesTT=0;
	$pesTTcalPortar=0;
	$pesTTpendentAbastiment=0;
	
	$var='';
	
	
		if($rc=='1'){$checkedRc='checked';}else{$checkedRc='';}
		if($mPars['elp']=='1'){$checkedElp='checked';}else{$checkedElp='';}
		
		echo "
	<br>
	<center>
	<br>
	Llistat/albar� dels productes ".$origenDestiText.":</p>
	<p class='p_micro' style='text-align:center;'>
	<input ".$checkedRc." type='checkbox' value='".$rc."' onClick=\"javascript:val=this.value;val*=-1;this.value=val;enviarFpars('vistaReservesSZ.php?pE=".$pE."&rc='+this.value+'&elp=".$mPars['elp']."&szD=".$mPars['szDesti']."&zD=".$mPars['zDesti']."&grupD=".$mPars['grupDesti']."&szO=".$mPars['szOrigen']."&zO=".$mPars['zOrigen']."&grupO=".$mPars['grupOrigen']."','_self');
	\"> mostrar nom�s els productes de les recepcions confirmades per cada punt d'entrega
	</p>

	";
	if($mPars['szOrigen']!='' || $mPars['zOrigen']!='' || $mPars['grupOrigen']!='')
	{
		echo "
	<p class='p_micro' style='text-align:center;'>
	<input ".$checkedElp." type='checkbox' value='".$mPars['elp']."' onClick=\"
	javascript:
	val=this.value;
	val*=-1;
	this.value=val;
	enviarFpars('vistaReservesSZ.php?pId=".$mPars['vProducte']."&pE=".$pE."&elp='+this.value+'&szD=".$mPars['szDesti']."&zD=".$mPars['zDesti']."&grupD=".$mPars['grupDesti']."&szO=".$mPars['szOrigen']."&zO=".$mPars['zOrigen']."&grupO=".$mPars['grupOrigen']."','_self');
	\"> no mostrar els productes que no cal portar perqu� al Desti hi ha estoc positiu suficient,<br> o perqu� el producte est� pendent d'entrar i entra per la zona de dest�
	</p>
		";
	}
	
	/*
	if($mPars['grup_id']!='0')
	{
		echo "
	<p class='p_micro' style='text-align:center;'>
	Selecciona la SuperZona per mostrar nom�s els productes dels productors gestionats desde la superzona seleccionada:
	<br>
	<select	id='sel_sz' onChange=\"javascript:enviarFpars('vistaReservesSZ.php?sz='+this.value,'_self');\">
		";
		$selected2='selected';
		reset($mGrupsZones);
		while(list($sz_,$mZones_)=each($mGrupsZones))
		{
			if($sz_==$sz){$selected='selected';$selected2='';}else{$selected='';}
			echo "
		<option ".$selected." value='".$sz_."'>".$sz_." (".(implode(',',$mZones_)).")</option>
			";
		}
		reset($mGrupsZones);
		echo "
		<option ".$selected2." value=''>- cap -</option>
	</select>
	</p>
		";
	}
	*/
	


	$mTaula=array();
	$mMagatzemMostrar=array();

	$jaEntregatTkg=0;
	$jaEntregatTuts=0;

	while(list($key,$val)=each($mNomsColumnes))
	{
		if($val=='pes'){$val_=$val.' (kg)';}else{$val_=$val;}
		$mTaula[0][$val]=$val;
	}
	reset($mNomsColumnes);

	while(list($magatzemRef,$mInventariRef)=each($mInventarisRef))
	{
		$mTaula[0][$magatzemRef]=$magatzemRef;
	}
	reset($mInventarisRef);

	$mTaula[0]['reservat']="<p style='color:green;'>reservat<br><b>".$destiText."</b></p><p class='p_micro2'>[*distribuci�]</p>";
	$mTaula[0]['cal_portar']="<font style='color:DarkOrange;'>cal portar</font><p class='p_micro2'>[*intercanvi<br>superZones]</p>";
	$mTaula[0]['pendent_abastiment']='pendent abastiment';
	$mTaula[0]['unitat_facturacio']='unitat facturaci�';
				
	$mProductesJaEntregatsDesti=array();
	$mProductesJaEntregatsOrigen=array();
	
	$mCategoriesGrupOrigen=@getCategoriesGrup($mRebostsRef[$mPars['grupOrigen']]);
	while(list($index,$mProducte)=each($mProductes))
	{
		if($mProducte['quantitat']*1>0 && substr_count($mProducte['tipus'],',jjaa,')==0) //reservat al desti
		{
			while(list($key,$val)=each($mNomsColumnes))
			{
				if($val=='producte'){$valor=urldecode($mProducte[$val]);}
				else if($val=='productor' || $val=='unitat_facturacio'){$valor=urldecode($mProducte[$val]);}
				//else if($val=='quantitat'){$valor='<b>'.$mProducte[$val].'</b>';}
				else if($val=='pesT')
				{
					$valor=$mProducte['pes']*$mProducte['quantitat']; 
				}
				else {$valor=$mProducte[$val];}

				$mTaula[$index][$val]=$valor;
			}
			reset($mNomsColumnes);

			$inventariRefIndexQuantitatLocal=0; //local: indica Desti
			$inventariRefIndexQuantitatOrigen=0;

			//------------------------------------------------------------------
			//inventaris
			//------------------------------------------------------------------
			while(list($magatzemRef,$mInventariRef)=each($mInventarisRef))
			{
				$zonaMagatzem=db_getZonaGrup($magatzemRef,$db);
				$sZmagatzem=getSuperZona($zonaMagatzem);
				if(isset($mInventariRef['inventariRef'][$index]))
				{
					$inventariRefIndexQuantitat=$mInventariRef['inventariRef'][$index];
				}
				else
				{
					$inventariRefIndexQuantitat=0;
				}
								
				if($inventariRefIndexQuantitat*1>0)
				{
					if(!in_array($magatzemRef,$mMagatzemMostrar)){array_push($mMagatzemMostrar,$magatzemRef);}
					if
					(
						(
							isset($mPars['szDesti'])
							&&
							$mPars['szDesti']!=''
							&&
							$mPars['szDesti']==$sZmagatzem
						)
						||
						(
							isset($mPars['zDesti'])
							&&
							$mPars['zDesti']!=''
							&&
							$mPars['zDesti']==$zonaMagatzem
						)
						||
						(
							isset($mPars['grupDesti'])
							&&
							$mPars['grupDesti']!=''
							&&
							$mPars['grupDesti']==$magatzemRef
						)
						||
						(
							(
								!isset($mPars['szDesti'])
								||
								$mPars['szDesti']==''							
							)
							&&
							(
								!isset($mPars['zDesti'])
								||
								$mPars['zDesti']==''							
							)
							&&
							(
								!isset($mPars['grupDesti'])
								||
								$mPars['grupDesti']==''							
							)
						)
					)
					{
						$inventariRefIndexQuantitatLocal+=$inventariRefIndexQuantitat*1;
					}
					//inventaris de l'origen
					if
					(
						(
							isset($mPars['szOrigen'])
							&&
							$mPars['szOrigen']!=''
							&&
							$mPars['szOrigen']==$sZmagatzem
						)
						||
						(
							isset($mPars['zOrigen'])
							&&
							$mPars['zOrigen']!=''
							&&
							$mPars['zOrigen']==$zonaMagatzem
						)
						||
						(
							isset($mPars['grupOrigen'])
							&&
							$mPars['grupOrigen']!=''
							&&
							$mPars['grupOrigen']==$magatzemRef
						)
						||
						(
							(
								!isset($mPars['szOrigen'])
								||
								$mPars['szOrigen']==''							
							)
							&&
							(
								!isset($mPars['zOrigen'])
								||
								$mPars['zOrigen']==''							
							)
							&&
							(
								!isset($mPars['grupOrigen'])
								||
								$mPars['grupOrigen']==''							
							)
						)
					)
					{
						$inventariRefIndexQuantitatOrigen+=$inventariRefIndexQuantitat*1;
					}
				}

				$mTaula[$index][$magatzemRef]=$inventariRefIndexQuantitat;
			}
			reset($mInventarisRef);

			//------------------------------------------------------------------
			// productes ja entregats
			//------------------------------------------------------------------
			$mProductesJaEntregatsDesti[$index]=0;
			$mProductesJaEntregatsOrigen[$index]=0;

			
			$destiBool=false;
			$origenBool=false;
			if
			(
				isset($mPars['szDesti']) &&	$mPars['szDesti']!=''
				||
				isset($mPars['zDesti']) &&	$mPars['zDesti']!=''
				||
				isset($mPars['grupDesti']) &&	$mPars['grupDesti']!=''
			)
			{$destiBool=true;}

			if
			(
				isset($mPars['szOrigen']) &&	$mPars['szOrigen']!=''
				||
				isset($mPars['zOrigen']) &&	$mPars['zOrigen']!=''
				||
				isset($mPars['grupOrigen']) &&	$mPars['grupOrigen']!=''
			)
			{$origenBool=true;}
			
			
			while(list($sz_,$mProductesJaEntregatsZona)=each($mProductesJaEntregatsZones))
			{
				while(list($z_,$mProductesJaEntregatsPEs)=each($mProductesJaEntregatsZona))
				{
					while(list($puntEntrega,$mProductesJaEntregatsPE)=each($mProductesJaEntregatsPEs))
					{
						if(isset($mProductesJaEntregatsPE[$index]))
						{
							if($destiBool)
							{
								if
								(
									(
										isset($mPars['szDesti'])
										&&
										$mPars['szDesti']!=''
										&&
										$mPars['szDesti']==$sz_
									)
									||
									(
										isset($mPars['zDesti'])
										&&
										$mPars['zDesti']!=''
										&&
										$mPars['zDesti']==$z_
									)
									||
									(
										isset($mPars['grupDesti'])
										&&
										$mPars['grupDesti']!=''
										&&
										$mPars['grupDesti']==$puntEntrega
									)
								)
								{
									if($mProductesJaEntregatsPE[$index]*1>0)
									{
										if(!isset($mProductesJaEntregatsDesti[$index]))
										{
											$mProductesJaEntregatsDesti[$index]=$mProductesJaEntregatsPE[$index];
										}
										else
										{
											$mProductesJaEntregatsDesti[$index]+=$mProductesJaEntregatsPE[$index]*1;
										}
										$mTaula[$index]['pesT']-=$mProductes[$index]['pes']*$mProductesJaEntregatsPE[$index];
										$jaEntregatTkg+=$mProductes[$index]['pes']*$mProductesJaEntregatsPE[$index];
										$jaEntregatTuts+=$mProductesJaEntregatsPE[$index];
									}
								}
							}
							else if ($origenBool)
							{
								if
								(
									(
										isset($mPars['szOrigen'])
										&&
										$mPars['szOrigen']!=''
										&&
										$mPars['szOrigen']==$sz_
									)
									||
									(
										isset($mPars['zOrigen'])
										&&
										$mPars['zOrigen']!=''
										&&
										$mPars['zOrigen']==$z_
									)
									||
									(
										isset($mPars['grupOrigen'])
										&&
										$mPars['grupOrigen']!=''
										&&
										$mPars['grupOrigen']==$puntEntrega
									)
								)
								{
									if($mProductesJaEntregatsPE[$index]*1>0)
									{
										if(!isset($mProductesJaEntregatsOrigen[$index]))
										{
											$mProductesJaEntregatsOrigen[$index]=$mProductesJaEntregatsPE[$index];
										}
										else
										{
											$mProductesJaEntregatsOrigen[$index]+=$mProductesJaEntregatsPE[$index]*1;
										}
										$mTaula[$index]['pesT']-=$mProductes[$index]['pes']*$mProductesJaEntregatsPE[$index];
										$jaEntregatTkg+=$mProductes[$index]['pes']*$mProductesJaEntregatsPE[$index];
										$jaEntregatTuts+=$mProductesJaEntregatsPE[$index];
									}
								}
							}
							else //no s'ha definit origen ni desti
							{
								if($mProductesJaEntregatsPE[$index]*1>0)
								{
									if(!isset($mProductesJaEntregatsDesti[$index]))
									{	
										$mProductesJaEntregatsDesti[$index]=$mProductesJaEntregatsPE[$index];
									}
									else
									{
										$mProductesJaEntregatsDesti[$index]+=$mProductesJaEntregatsPE[$index]*1;
									}
									$mTaula[$index]['pesT']-=$mProductes[$index]['pes']*$mProductesJaEntregatsPE[$index];
									$jaEntregatTkg+=$mProductes[$index]['pes']*$mProductesJaEntregatsPE[$index];
									$jaEntregatTuts+=$mProductesJaEntregatsPE[$index];
								}
							}
						}
					}
					@reset($mProductesJaEntregatsPEs);
				}
				@reset($mProductesJaEntregatsZona);
			}
			@reset($mProductesJaEntregatsZones);
			
			//quantitats:
			$calPortar=0;
			$pendentAbastiment=0;

			$mTaula[$index]['reservat']=$mProducte['quantitat'];

			// si falta producte a desti

			if($origenBool			)
			{
				if($mProducte['quantitat']-$mProductesJaEntregatsDesti[$index]>$inventariRefIndexQuantitatLocal)
				{
					$productorId=substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'));
					if
					(
						(
							isset($mPars['szOrigen']) && $mPars['szOrigen']!=''
							&&
							getSuperZona($mPerfilsRef[$productorId]['zona'])==$mPars['szOrigen']
						)
						||
						(
							isset($mPars['zOrigen']) && $mPars['zOrigen']!=''
							&&
							$mPerfilsRef[$productorId]['zona']==$mPars['zOrigen']
						)
						||
						(
							isset($mPars['grupOrigen']) && $mPars['grupOrigen']!=''
							&&
							$mPerfilsRef[$productorId]['zona']==$mCategoriesGrupOrigen['2']
						)
					)
					{
						$calPortar=$mProducte['quantitat']-$mProductesJaEntregatsDesti[$index]-$inventariRefIndexQuantitatLocal;
					}
					
//els productes ja entregats s'han de treure de l'inventari
					//disminuir $calPortar segons inventari en origen positiu i major que les reserves en origen
					if(!isset($mProductesOrigen[$index]['quantitat'])){$mProductesOrigen[$index]['quantitat']=0;}
					if($inventariRefIndexQuantitatOrigen-$mProductesOrigen[$index]['quantitat']+$mProductesJaEntregatsOrigen[$mProducte['id']]<0)
					{
						$pendentAbastiment=$calPortar;
						$perfilId=substr($mProductes[$index]['productor'],0,strpos($mProductes[$index]['productor'],'-'));
						if($mPars['grupOrigen']!='')
						{
							$zonaPerfil=substr($mRebostsRef[$mPars['grupOrigen']]['categoria'],strpos($mRebostsRef[$mPars['grupOrigen']]['categoria'],',2-')+3);
							$zonaPerfil=substr($zonaPerfil,0,strpos($zonaPerfil,','));
						}
						else
						{
							$zonaPerfil='';
						}
					
						if
						(
							getSuperZona($mPerfilsRef[$perfilId]['zona'])!=$mPars['szOrigen']
							&&
							$mPerfilsRef[$perfilId]['zona']!=$mPars['zOrigen']
							&&
							$mPerfilsRef[$perfilId]['zona']!=$zonaPerfil
						)
						{
							$calPortar=0;
						}
					}
					else
					{
						if(($inventariRefIndexQuantitatOrigen-$mProductesOrigen[$index]['quantitat']+$mProductesJaEntregatsOrigen[$mProducte['id']])>$calPortar)
						{
							$pendentAbastiment=0;
						}
						else
						{
							$pendentAbastiment=$calPortar-($inventariRefIndexQuantitatOrigen-$mProductesOrigen[$index]['quantitat']-$mProductesJaEntregatsOrigen[$mProducte['id']]);
							//$calPortar=$inventariRefIndexQuantitatOrigen-$mProductesOrigen[$index]['quantitat']+$mProductesJaEntregatsOrigen[$mProducte['id']];
						}
					
						if($calPortar<=0){$calPortar=0;$pendentAbastiment=0;}
					}
				}
			}
			else // no s'ha elegit desti -> es mostren totes les reserves
			{
				$calPortar=$mProducte['quantitat']-$mProductesJaEntregatsDesti[$index]-$inventariRefIndexQuantitatLocal;
				
				if($calPortar<0)
				{
					$calPortar=0;
				}
				$pendentAbastiment=$calPortar;

				/*
				if($index=='783')
				{
					$var="reservat: ".$mProducte['quantitat']."<br>ja entregat: ".$mProductesJaEntregatsDesti[$index]."<br>estoc: ".$inventariRefIndexQuantitatLocal."<br>pendentAbastiment: ".$pendentAbastiment;
				}
				*/
			}
		

			//$calPortar=$mProducte['quantitat']-$pendentAbastiment;
			$mTaula[$index]['cal_portar']=$calPortar;
			$mTaula[$index]['pendent_abastiment']=$pendentAbastiment;

			if($mPars['elp']=='1')
			{
				$pesT+=$mProducte['pes']*$calPortar;
			}
			else
			{
				$pesT+=$mProducte['pes']*$mProducte['quantitat'];
			}
			$pesTTcalPortar+=$mProducte['pes']*$calPortar;
			$pesTTpendentAbastiment+=$mProducte['pes']*$pendentAbastiment;
			
			$mTaula[$index]['unitat_facturacio']=$mProducte['unitat_facturacio'];

		}
	}
	reset($mProductes);	


	//--------------------------------------------------------------------------
	$mCols=array_merge($mNomsColumnes,$mMagatzemMostrar,array('reservat','cal_portar','pendent_abastiment','unitat_facturacio'));
	reset($mMagatzemMostrar);
	if($mPars['excloureProductesJaEntregats']=='1')
	{
		if($destiText==''){$destiText='a totes les zones';}
		echo "
		<p class='pAlerta4' style='color:orange; text-align:center;'>* s'exclouen els productes ja entregats (<b>".(number_format($jaEntregatTkg,2,'.',''))."</b> kg, <b>".(number_format($jaEntregatTuts,2,'.',''))."</b> uts.) a <b>".$destiText."</b></p>
		";
	}
	else
	{
		if($destiText==''){$destiText='a totes les zones';}
		echo "
		<p class='pAlerta4' style='color:orange; text-align:center;'>* <b>NO</b> s'exclouen els productes ja entregats (<b>".(number_format($jaEntregatTkg,2,'.',''))."</b> kg, ".(number_format($jaEntregatTuts,2,'.',''))." uts.) a <b>".$destiText."</b></p>
		";
	}

	if(isset($mPars['vProducte']) && $mPars['vProducte']!='TOTS')
	{
		echo "
		<p class='pAlerta4' style='text-align:center; color:red;'>* nom�s es mostra el producte <b>id:".$mPars['vProducte']." - ".(urldecode($mProductes[$mPars['vProducte']]['producte']))."</b> (del/ls productors seleccionats)</p>
		";
	}
	else if(isset($mPars['vProductor']) && $mPars['vProductor']!='TOTS')
	{
		echo "
		<p class='pAlerta4' style='text-align:center; color:red;'>* nom�s es mostren els productes del productor <b>".(urldecode($mPerfilsRef[$mPars['vProductor']]['projecte']))."</b></p>
		";
	}
	if(isset($mPars['vCategoria']) && $mPars['vCategoria']!='TOTS')
	{
		echo "
		<p class='pAlerta4' style='text-align:center; color:red;'>* nom�s es mostren els productes de categoria <b>".$mPars['vCategoria']."</b></p>
		";
	}
	if(isset($mPars['vSubCategoria']) && $mPars['vSubCategoria']!='TOTS')
	{
		echo "
		<p class='pAlerta4' style='text-align:center; color:red;'>* nom�s es mostren els productes de sub-categoria <b>".$mPars['vSubCategoria']."</b></p>
		";
	}
	if(isset($mPars['etiqueta']) && $mPars['etiqueta']!='TOTS')
	{
		echo "
		<p class='pAlerta4' style='text-align:center; color:red;'>* nom�s es mostren els productes de tipus <b>".$mPars['etiqueta']."</b></p>
		";
	}
	if(isset($mPars['etiqueta2']) && $mPars['etiqueta2']!='CAP')
	{
		echo "
		<p class='pAlerta4' style='text-align:center; color:red;'>* no es mostren els productes de tipus <b>".$mPars['etiqueta2']."</b></p>
		";
	}
	
	echo "
	</center>
	";

	echo "
	<table border='0' width='70%' align='center'>
		<tr>
			<td align='right' valign='bottom' >
			<a href='docs".$mPars['selRutaSufix']."/reserves/reserves_".$mPars['usuari_id'].".csv' target='_blank'>Descarregar aquesta vista de reserves de zona (.csv)</a>
			</td>
		</tr>
	</table>
	<table border='1' width='70%' align='center'>
	";
	
	
	$linCsv=1;
	$linCsv2=1;
	while(list($idProducte,$mLinia)=each($mTaula))		
	{
		if($idProducte==0)
		{
			echo "
		<tr>
			";
			while(list($col,$val)=each($mLinia))		
			{
				if(in_array($col,$mCols))
				{
					$zonaMagatzem=db_getZonaGrup($col,$db);
					$sZmagatzem=getSuperZona($zonaMagatzem);
					if(in_array($col,$mMagatzemMostrar))
					{
						$color='black';

						echo "
			<th align='left' width='100' valign='top'>
						";
						if($mPars['szDesti']==$sZmagatzem || $mPars['zDesti']==$zonaMagatzem){$color='green';}
						else if($mPars['szOrigen']==$sZmagatzem || $mPars['zOrigen']==$zonaMagatzem){$color='blue';}
			
						$mData=explode(';',$mInventarisRef[$col]['data']);
						echo "
			<p class='p_micro2' style='color:".$color.";'>-inventari-<br>(".$zonaMagatzem.")</p>
			<p class='p_micro2'>".(str_replace(array('{','}'),'',$mData[0]))."</p>
			<p class='p_micro2'>".(str_replace(array('{','}'),'',$mData[1]))."</p>
			<p>".(urldecode($mRebostsRef[$col]['nom']))."</p>
			</th>
						";
						$mReservesCsv[0][$col]=urldecode($mRebostsRef[$col]['nom']);
					}
					else if($col=='cal_portar')
					{
						if
						(
							$origenBool && $destiBool)																	
						{
							echo "
			<th align='center' valign='top' >
			<p >".$mTaula[0][$col]."</p>
			</th>
							";
							$mReservesCsv[0][$col]=$col;
						}
					}
					else  if($col=='reservat')
					{
						echo "
			<th align='center' valign='top' >
			<p >".$mTaula[0][$col]."</p>
						";
						if($mPars['excloureProductesJaEntregats']=='1')
						{
							echo "
			<p style='color:orange; font-size:10px;'>pendent entrega</p>
							";
						}
						echo "
			</th>
						";
						$mReservesCsv[0][$col]=$col;
					}
					else
					{
						echo "
			<th align='center' valign='top' >
			<p >".$mTaula[0][$col]."</p>
			</th>
						";
						$mReservesCsv[0][$col]=$col;
					}
				}
			}
			reset($mLinia);

			echo "
		</tr>
			";
		}
		else
		{

			echo "
		<tr>
			";
			while(list($col,$val)=each($mLinia))		
			{
				/*if($idProducte==526)
				{
					//$var='reservat desti:'.$mProductes[$mProducte['id']]['quantitat'].'<br> - cal portar: '.$calPortar.'<br> - estocDisponibleEnOrigen: '.$estocDisponibleEnOrigen.'<br> - inventariRefIndexQuantitatLocal(desti):'.$inventariRefIndexQuantitatLocal.'<br>- pendentAbastiment: '.$pendentAbastiment;
					$var='elp: '.$mPars['elp'].'==1 && cal_portar: '.$mLinia['cal_portar'].'>0  && pendent_abastiment: '.$mLinia['pendent_abastiment'].'>0';
				}
				*/
				
				if($mPars['elp']=='-1' || ($mPars['elp']=='1' && ($mLinia['cal_portar']>0  || $mLinia['pendent_abastiment']>0)))
				{
					if(in_array($col,$mCols))
					{
						$zonaMagatzem=db_getZonaGrup($col,$db);
						$sZmagatzem=getSuperZona($zonaMagatzem);
						//pels magatzems
						if(in_array($col,$mMagatzemMostrar))
						{
							$color='lightGrey';
							if($mPars['szDesti']==$sZmagatzem || $mPars['zDesti']==$zonaMagatzem){$color='lightGreen';}
							else if($mPars['szOrigen']==$sZmagatzem || $mPars['zOrigen']==$zonaMagatzem){$color='lightBlue';}

							if($val>0)
							{
								echo "
			<th align='center' valign='middle' style='background-color:".$color.";'>
			<p >".$val."</p>
			</th>
								";
								$mReservesCsv[$linCsv][$col]=$val;
							}
							else
							{
								echo "
			<td align='center' valign='middle' style='background-color:".$color.";'>
			<p >".$val."</p>
			</td>
								";
								$mReservesCsv[$linCsv][$col]=$val;
							}
						}
						else if($col=='producte' || $col=='productor' || $col=='unitat_facturacio')
						{
							echo "
			<td>
			<p>".(urldecode($val))."</p>
			</td>
							";
							$mReservesCsv[$linCsv][$col]=urldecode($val);
							
						}
						else if($col=='pes')
						{
							echo "
			<td>
			<p>".(number_format($val,2))."</p>
			</td>
							";
							$mReservesCsv[$linCsv][$col]=str_replace('.',',',(number_format($val,2)));
						}
						else if($col=='pesT')
						{
							$pesTT+=$val;
							echo "
			<td>
			<p>".(number_format($val,2))."</p>
			</td>
							";
							$mReservesCsv[$linCsv][$col]=str_replace('.',',',(number_format($val,2)));
						}
						else if($col=='cal_portar')
						{
							if($origenBool && $destiBool)
							{
								echo "
			<td align='middle'>
			<p><font style='color:DarkOrange;'><b>".$val."</font></b></p>
			</td>
								";
								$mReservesCsv[$linCsv][$col]=$val;
							}
						}
						else if($col=='pendent_abastiment')
						{
							if($val>0)
							{
								if(substr_count($mProductes[$mLinia['id']]['tipus'],'dip�sit')>0)
								{
									$colorPendentAbastiment='#F9DAD6';
								}
								else
								{
									$colorPendentAbastiment='#FA897A';
								}
							}
							else
							{
								$colorPendentAbastiment='white';
							}
								echo "
			<td style='background-color:".$colorPendentAbastiment.";' align='middle'>
			<p>".$val."</p>
			</td>
							";
							$mReservesCsv[$linCsv][$col]=$val;
						}
						else
						{
							echo "
			<td align='middle' >
			<p>".$val."</p>
			</td>
							";
							$mReservesCsv[$linCsv][$col]=$val;
						}
					}
				}
			}
			reset($mLinia);
			echo "
		</tr>
			";
			
			$linCsv+=1;
		}
	}
	reset($mTaula);
	echo "
	</table>
";
echo "
	<table border='0' width='40%' align='center'>
		</tr>

	";
	if($origenBool && $destiBool)
	{
		echo "	
		<tr>
			<td width='50%' align='right'>
			<p>Total cal portar:</p>
			</td>
			<td width='50%' align='left'>
			<p><b>".(number_format($pesTTcalPortar,2))."</b> kg</p>
			</td>
		</tr>
		";
		$mReservesCsv[$linCsv]['Total cal portar']='Total cal Portar: '.(number_format($pesTTcalPortar,2));
		$linCsv+=1;
		if($mPars['elp']=='-1')
		{
			echo "
		<tr>
			<td width='50%' align='right'>
			<p>Total pre-distribu�t:</p>
			</td>
			<td width='50%' align='left'>
			<p><b>".(number_format($pesTT-$pesTTcalPortar-$pesTTpendentAbastiment,2))."</b> kg</p>
			</td>
		</tr>
			";
			$mReservesCsv[$linCsv]['Total pre-distribuit']='Total pre-distribu�t: '.(number_format($pesTT-$pesTTcalPortar-$pesTTpendentAbastiment,2));
			$linCsv+=1;
		}
	}

	if($mPars['excloureProductesJaEntregats']=='1')
	{
		if($mPars['elp']=='-1')
		{
	echo "

		<tr>
			<td width='50%' align='right'>
			<p>[ Pendent Abastiment:</p>
			</td>
			<td width='50%' align='left'>
			<p><b>".(number_format($pesTTpendentAbastiment,2))."</b> kg ]</p>
			</td>
		</tr>
		<tr>
			<td width='50%' align='right'>
			<p><b>Ja entregat:</b></p>
			</td>
			<td width='50%' align='left'>
			<p><b>".(number_format($jaEntregatTkg,2))."</b> kg</p>
			</td>
		</tr>
		<tr>
			<td width='50%' align='right'>
			<p><b>Pendent entrega:</b></p>
			</td>
			<td width='50%' align='left'>
			<p><b>".(number_format($pesTT,2))."</b> kg</p>
			</td>
		</tr>
		<tr>
			<td width='50%' align='right'>
			<p><b>suma de control:</b></p>
			</td>
			<td width='50%' align='left'>
			";
			if(number_format($pesTT+$jaEntregatTkg,2)!=number_format($pesSegonsReserves,2))
			{
				$color='red';
				$dif='('.number_format($pesTT+$jaEntregatTkg-$pesSegonsReserves,2).' kg)';
			}
			else
			{
				$color='';
				$dif='';
			}
			echo "
			<p style='color:".$color.";'><b>".(number_format($pesTT+$jaEntregatTkg,2))."</b> kg ".$dif."</p>
			</td>
		</tr>
			";
			$mReservesCsv[$linCsv]['Pes Total']='Total pendent abastiment: '.(number_format($pesTTpendentAbastiment,2));
			$linCsv+=1;
			$mReservesCsv[$linCsv]['Pes Total']='SUMA DE CONTROL: '.(number_format($pesTT+$jaEntregatTkg,2));
			$linCsv+=1;
		}
	}
	else
	{
		echo "
		<tr>
			<td width='50%' align='right'>
			<p>( Total ja entregat:</p>
			</td>
			<td width='50%' align='left'>
			<p><b>".(number_format($jaEntregatTkg,2))."</b> kg)</p>
			</td>
		</tr>
		";
		$mReservesCsv[$linCsv]['Total jaEntregat']='Total jaEntregat: '.(number_format($jaEntregatTkg,2));
		$linCsv+=1;
		if($mPars['elp']=='-1')
		{
			echo "			
		<tr>
			<td width='50%' align='right'>
			<p>Total pendent Abastiment:</p>
			</td>
			<td width='50%' align='left'>
			<p><b>".(number_format($pesTTpendentAbastiment,2))."</b> kg </p>
			</td>
		</tr>
		<tr>
			<td width='50%' align='right'>
			<p><b>suma de control:</b></p>
			</td>
			<td width='50%' align='left'>
			";
			if(number_format($pesTT+$jaEntregatTkg,2)!=number_format($pesSegonsReserves,2))
			{
				$color='red';
				$dif='('.number_format($pesTT+$jaEntregatTkg-$pesSegonsReserves,2).' kg)';
			}
			else
			{
				$color='';
				$dif='';
			}
			echo "
			<p style='color:".$color.";'><b>".(number_format($pesTT+$jaEntregatTkg,2))."</b> kg ".$dif."</p>
			</td>
		</tr>
			";
			$mReservesCsv[$linCsv]['Total pendent Abastiment']='Total pendent Abastiment: '.(number_format($pesTTpendentAbastiment,2));
			$linCsv+=1;
			$mReservesCsv[$linCsv]['suma control']='SUMA DE CONTROL: '.(number_format($pesTT,2));
			$linCsv+=1;
		}
	}
	echo "
	</table>
	<br>&nbsp;
	";
	echo '<br>'.$var;

	
	return;
}

//------------------------------------------------------------------------------
function html_mostrarNotes()
{
	echo "
	<table style='width:70%;' align='center'>
		<tr>
			<td style='width:100%;' align='center'>
			<p class='nota2'><u>Notes:</u></p>
			<p class='nota2'>
			1- es mostren tots els magatzems amb estoc positiu d'algun dels productes visualitzats 
			<br>
			2- els magatzems que pertanyen al <b>desti</b> seleccionat (superzona, zona o grup) apareixen en <font style='background-color:lightGreen;'>verd</font>
			<br>
			3- els magatzems que pertanyen a l'<b>origen</b> seleccionat (superzona, zona o grup) apareixen en <font style='background-color:lightBlue;'>blau</font>
			<br>
			4- si un producte t� estoc positiu al magatzem Origen per� s'ha reservat en l'origen, no es pot portar a Desti, i cal_portar es mant� en positiu.
			<br>
			5- si d'un producte ja s'ha entregat alguna unitat al magatzem Origen i no figura a l'estoc, pero es necessita al desti, i el productor es de la zona d'origen, es comptar� com a 'cal_portar' i com a 'pendent_abastiment' en l'origen.
			<br>
			6- els productes que <b>cal portar</b> apareixen en <font style='color:darkOrange;'><b>taronja</b></font>.
			<br>
			7- els productes  <b>pendents d'abastiment</b> (d'entrar) apareixen en <font style='background-color:#FA897A;'>vermell</font> si son 'no dip�sit', i en <font style='background-color:#F9DAD6;'>vermell clar </font> si son 'dip�sit'.
			<br>
			8- els productes  <b>pendents d'abastiment</b> (d'entrar) constaran com a tals tant en l'origen com en el desti si els dos son grups d'una mateixa zona; ja que es considera que l'abastiment pendent est� vinculat a la zona i no al grup.
			<br>
			9- els productes  de tipus '<b>JJAA</b>' no s'inclouen a les llistes d'intercanvi intern, ja que es consideren jaEntregats a les JJAA.
			</p>
			</td>
		</tr>
	</table>
		";

	return;
}

?>

		