<?php

///////////////////////////////
$source='0'; // 1: online
$demo=-1;
$mailIntern='cac';

	$mModuls['serveis']=1; //habilita l'acc�s a serveis transport
	$mModuls['modeProves']=1; //habilita entrar/sortir del mode de proves
	
///////////////////////////////	
$mDEMO_Params=array();
if($source=='0')//local
{
	$mDEMO_Params['host']                    	='localhost';
    $mDEMO_Params['user']                       ='root';
    $mDEMO_Params['password']                   ='';
	$mDEMO_Params['bd']                         ="cic_gc39_demo";
}
else if ($source=='1')	//online
{
	$mDEMO_Params['host']                       ='localhost';
    $mDEMO_Params['user']                       ='c46ciccac';
	$mDEMO_Params['password']                   ='RKnp88YdkQWdVK';
 	$mDEMO_Params['bd']                         ="c46ciccac3_demo";
}

$htmlTitolPags='CAC'; //CAC2


$ruta=date('ym'); 
$mode='normal';


$mParametres=array();

$oDataFiPrecomandes = new DateTime("2014-10-19"); // <<< nom�s inicialitzar

$mProductesCostTransportZero=array('34','65','66');
$mRutes=array('abril 2014','maig 2014','juny 2014','juliol 2014','agost 2014');

$mCategoriesUsuaris=array('sadmin','admin','coord','usuari','visitant');

$mIO=array();
$mIO['EA']['ref']='EA';
$mIO['EA']['nom']='Abastiment'; //productes procedents d'una compra de la CAC
$mIO['EA']['accio']='introduir producte'; //boto introduir operacio
$mIO['EA']['fletxa']='&uarr;'; 

$mIO['ER']['ref']='ER';
$mIO['ER']['nom']='Recuperaci�'; //productes procedents d'una recuperaci� al magatzem de producte perdut
$mIO['ER']['accio']='recuperar producte'; //productes procedents d'una compra de la CAC
$mIO['ER']['fletxa']='&uarr;'; 

$mIO['SD']['ref']='SD';
$mIO['SD']['nom']='Distribuci�'; //productes destinats a una venda col.lectiva de la CAC
$mIO['SD']['accio']='treure producte'; //productes procedents d'una compra de la CAC
$mIO['SD']['fletxa']='&darr;'; 

$mIO['SP']['ref']='SP';
$mIO['SP']['nom']='P�rdua'; //productes que s'han perdut
$mIO['SP']['accio']='perdre producte'; //productes procedents d'una compra de la CAC
$mIO['SP']['fletxa']='&darr;'; 

$mM=array();

$mMesos['01']='gener';
$mMesos['02']='febrer';
$mMesos['03']='mar�';
$mMesos['04']='abril';
$mMesos['05']='maig';
$mMesos['06']='juny';
$mMesos['07']='juliol';
$mMesos['08']='agost';
$mMesos['09']='setembre';
$mMesos['10']='octubre';
$mMesos['11']='novembre';
$mMesos['12']='desembre';




$mDies['01']='dilluns';
$mDies['02']='dimarts';
$mDies['03']='dimecres';
$mDies['04']='dijous';
$mDies['05']='divendres';
$mDies['06']='dissabte';
$mDies['07']='diumenge';

$mDiesSetmana=array('dilluns','dimarts','dimecres','dijous','divendres','dissabte','diumenge');

$mContinguts=array();
$mText=array();
$mRebosts=array();
$mProductes=array();

//*v36.6-elements matriu
$mColsProductes['visiblesLocal']=array('id','segell','producte','productor','preu','pes', 'ms', 'estoc_previst', 'tipus','categoria0','categoria10','format','estoc_disponible');
$mColsProductes['visiblesRebosts']=array('id','segell','producte','productor','preu','pes', 'ms', 'estoc_previst', 'tipus','categoria0','categoria10','format','estoc_disponible');
$mColsProductes['visiblesCAC']=array('id','segell','producte','productor','tipus','categoria0','categoria10','unitat_facturacio','format','pes','volum','preu','ms','estoc_previst','estoc_disponible','cost_transport_extern_kg','cost_transport_intern_kg');
$mColsProductes['visiblesMagatzems']=array('id','segell','producte','productor','tipus','categoria0','categoria10','unitat_facturacio','format','pes','ms','estoc_previst','estoc_disponible');
$classTitolColumnes='titolColumnesCac';

$mEtiquetes=array('');
$mEtiquetes[0]['nom']='b�sic';
$mEtiquetes[0]['color']='brown';
$mEtiquetes[1]['nom']='dip�sit';  //intern
$mEtiquetes[1]['color']='DarkViolet';
$mEtiquetes[2]['nom']='semiPerible';
$mEtiquetes[2]['color']='DarkGreen';
$mEtiquetes[3]['nom']='alcohol';  //intern
$mEtiquetes[3]['color']='DarkViolet';
$mEtiquetes[4]['nom']='jjaa';	 //intern
$mEtiquetes[4]['color']='DarkViolet';
$mEtiquetes[5]['nom']='recolzat-100%';
$mEtiquetes[5]['color']='DarkViolet';	 //intern
$mEtiquetes[6]['nom']='gratu�t';  
$mEtiquetes[6]['color']='blue';
$mEtiquetes[7]['nom']='canviFormat'; //intern
$mEtiquetes[7]['color']='DarkViolet';

$mEtiquetesTrams=array('');
$mEtiquetesTrams[0]['nom']='OFERTES';
$mEtiquetesTrams[0]['color']='#05B8FA';
$mEtiquetesTrams[1]['nom']='DEMANDES';
$mEtiquetesTrams[1]['color']='#ff0000';

$mStTipusPluralAsingular['OFERTES']='OFERTA';
$mStTipusPluralAsingular['DEMANDES']='DEMANDES';

$mCategoria0['sadmin']=array('CIC','JJAA-CIC','CAC','CAC-personal','REBOSTS','G-CONVIVENCIES','VIATGE','FESTA','CONCERT','altres'); 
$mCategoria0['admin']=$mCategoria0['sadmin'];
$mCategoria0['coord']=$mCategoria0['admin'];
$mCategoria0['usuari']=array('CIC','JJAA-CIC','CAC','REBOSTS','G-CONVIVENCIES','VIATGE','FESTA','CONCERT','altres'); 
$mCategoria0['visitant']=$mCategoria0['usuari'];

$mCategoria10['sadmin']=array(); 
$mCategoria10['admin']=$mCategoria10['sadmin'];
$mCategoria10['coord']=$mCategoria10['sadmin'];
$mCategoria10['usuari']=$mCategoria10['sadmin'];
$mCategoria10['visitant']=$mCategoria10['sadmin'];

$mMesosResumContractes=array($ruta);

$mNomsColumnes=array(
'id'=>'Id',
'segell'=>'Segell ECOCIC',
'producte'=>'Producte',
'productor'=>'Productor',
'actiu'=>'Actiu',
'tipus'=>'Tipus',
'categoria0'=>'Categoria',
'categoria10'=>'Sub-<br>categoria',
'unitat_facturacio'=>'Ut<br>fact',
'format'=>'Format<br>(pack uts)',
'pes'=>'Pes<br>ut.<br>fact<br>(kg)',
'volum'=>'Volum<br>ut.<br>fact<br>(l)',
'preu'=>'Preu<br>(uts)',
'ms'=>'% MS',
'estoc_previst'=>'Estoc<br>previst',
'estat'=>'estat',
'estoc_disponible'=>'Estoc<br>dispon.',
'cost_transport_extern_kg'=>'cost<br>transp<br>extern<br>(uts/kg)',
'ms_ctek'=>'% ms del cost transport extern per kg',
'cost_transport_intern_kg'=>'cost<br>transp<br>intern<br>(uts/kg)',
'ms_ctik'=>'% ms del cost transport intern per kg'
);

$mColsProductes['visiblesUsuaris']=array();
$mColsProductes['notesRebosts']=array();
$mColsProductes['notesUsuaris']=array();
$mColsProductes['notesAdmin']=array();

//periodes normals
$mTaulesBDmensual=array(
'comandes',
'parametres',
'incidencies',
'productes',
'rebosts',
'trams',
'contractes'
);

$mTaulesBDuniques=array(
'municipis2',
'usuaris',
'productors',
'comandes_seg',
'comandes_especials'
);

$mTaulesBDanual=array(
'inventaris',
'intercanvis_rebosts',
'comptes'
);

$mTaulesMensualsAntigues=array(
'trams_ruta'
);

//*v36.2-var
//periodes especials
$mTaulesBDmensualESP=array(
'comandes',
'comandes_seg',
'incidencies',
'parametres',
'productes',
'productors',
'rebosts',
);

$mEstatsIncd0=array ('pendent','resolta');
$mEstatsAbcA0=array ('aplicat','noAplicat');




$mVocalsAccentSearch=array(
'�','�','�','�','�',
'�','�','�','�','�',
'�','�','�','�','�',
'�','�','�','�','�',
'�','�','�','�','�',
'�','�','�','�','�',
'�','�','�','�','�',
'�','�','�','�','�',
'�','�'
);

$mVocalsAccentReplace=array(
'a','e','i','o','u',
'A','E','I','O','U',
'a','e','i','o','u',
'A','E','I','O','U',
'a','e','i','o','u',
'A','E','I','O','U',
'a','e','i','o','u',
'A','E','I','O','U',
'c','C'
);

// upload.php

$mExtensionsPermeses=array('gif','png','jpg','jpeg');
$thumbCode='th';

//totes les p�gines
$mColors['body']='white';
$mColors['table']='white';
$mColors['bodyDemo']='LightSteelBlue ';
$mColors['tableDemo']='white';

//p�gines concretes
$mColors['comandes.php'][0]['t1_bg']='#BDC7F9';
$mColors['comandes.php'][1]['t1_bg']='#BDC7F9';

$mColors['comandes.php'][0]['t2_bg']='#9CE6E3';
$mColors['comandes.php'][1]['t2_bg']='#F9E396';

$mColors['comandes.php'][0]['t3_bg']='#A4D781';
$mColors['comandes.php'][1]['t3_bg']='#A4D781';

$mColors['incidencies.php'][0]['t1_bg']='LightBlue';
$mColors['incidencies.php'][1]['t1_bg']='#F9E396';

$mColor['distribucioGrups.php'][0]['t1_bG']='#ffccaa';


$mTipusIncidencies=array('pendentEntrega','jaEntregat','producte','formaPagament','SERVEI INTER-REBOST','altres');

//v*37.2- 2 valors
$getParsCode1='kT832nj';
$getParsCode2='lP6m3rO';

$htmlTitolPags='CAC';

$mExcConfiables=array(
'ANOI'=>array('CES'),
'COOP'=>array('I-CES'),
'EBRE'=>array('CES'),
'ECOP'=>array('CES'),
'ECOS'=>array('I-CES'),
'EGIR'=>array('I-CES'),
'EXBV'=>array('CES'),
'EXEM'=>array('I-CES'),
'GOTA'=>array('I-CES'),
'GRTX'=>array('I-CES'),
'HORA'=>array('I-CES'),
'LAIE'=>array('I-CES'),
'PLES'=>array('CES'),
'RIPA'=>array('CES'),
'SELV'=>array('CES'),
'SENY'=>array('CES'),
'VALL'=>array('I-CES')
);

$mExcNoConfiables=array(
'BION'=>array('CES'),
'EVDT'=>array('CES'),
'TRAP'=>array('CES')
);

$mPropietatsUsuaris=array(
'comissio'=>array(
	0=>'Acollida (CA)',
	1=>'Borsa de Treball (BT)',
	2=>"Central d'Abastiment Catalana (CAC)",
	3=>'Comunicaci� (CCom)',
	4=>'Coordinaci� (CCord)',
	5=>'Educaci� (OE)',
	6=>'Gesti� Econ�mica (GE)',
	7=>'Inform�tica (CI)',
	8=>'Jur�dica (CJ)',
	9=>'Processos Documentals (PD)',
	10=>'Xarxa Territorial (XT)',
	11=>"Comissi� d'Intercanvi i moneda Social (CIMS)",
	12=>"Habitatge (OH)",
	13=>"Suport Hum� (OSH)",
	14=>"Projectes Productius (CPP)",
	15=>"XCTIT"
	),
'compteSoci'=>''
);

$mPropietatsGrups=array(
'CAL'=>array(
	0=>'no',
	1=>'si'
	)
);

//----------------------------------------
//valors bbdd. Per defecte:
$mPropietatsGrupConfig=array(
'idsPerfilsActiusLocal'=>'',
'despesesGrup'=>0,
'msDespesesGrup'=>0
);

$mPropietatsPeriodeLocalConfig=array(
'comandesLocalsTancades'=>0,
'ctikLocal'=>0,
'ms_ctikLocal'=>0,
'fdLocal'=>0,
'ms_fdLocal'=>0,
'apeLocal'=>'',
'compte_grup_euros'=>0
);

$mParametresConfig=array(
'limitNombrePeriodesLocalsHistorial'=>array('parametre'=>'limitNombrePeriodesLocalsHistorial', 'valor'=>10,'id'=>51,'propietats'=>'','notes'=>''),
'limitNombreProductesLocals'=>array('parametre'=>'limitNombreProductesLocals', 'valor'=>1000,'id'=>50,'propietats'=>'','notes'=>''),
'diesFiRecepcioIncidencies'=>array('parametre'=>'diesFiRecepcioIncidencies', 'valor'=>15,'id'=>52,'propietats'=>'','notes'=>''),
'manteniment'=>array('parametre'=>'manteniment', 'valor'=>0,'id'=>49,'propietats'=>'','notes'=>''),
'aplicarValorTallSegell'=>array('parametre'=>'aplicarValorTallSegell', 'valor'=>0,'propietats'=>'','id'=>53,'notes'=>''),
'valorTallSegell'=>array('parametre'=>'valorTallSegell', 'valor'=>'10','propietats'=>'','id'=>54,'notes'=>''),
'numProductesGrupsMes'=>array('parametre'=>'numProductesGrupsMes', 'valor'=>'100','propietats'=>'','id'=>55,'notes'=>'')
);

$idioma='text0'; //catal�
//$idioma='text1'; 
//$idioma='text2'; 
//$idioma='text3'; 

$mBlocsPropietatsSegells=array(
'ecologia'=>array('color'=>'#80ff80'),
'�tica'=>array('color'=>'#ffff80'),
'salut'=>array('color'=>'#80ccff'),
'log�stica'=>array('color'=>'#ff8080'),
'suport m�tu'=>array('color'=>'#DB9EFA'),
'SCADI'=>array('color'=>'orange')
);


//no es pot alterar l'ordre, afegir a la c�a, es mostren ordenades per bloc
$mPropietatsSegellPerfilConfig=array(
'0'=>array('segell'=>'Materia primera 100% ECOL�GICA',  'grup'=>array(0,1),	'bloc'=>'ecologia'	,'categoria0'=>'tots',		'valor'=>100,	'estat'=>0,'nivell'=>'RP'),
'1'=>array('segell'=>'Materia primera 90% ECOL�GICA',   'grup'=>array(0,1),	'bloc'=>'ecologia'	,'categoria0'=>'tots',		'valor'=>90,	'estat'=>0,'nivell'=>'RP'),
'2'=>array('segell'=>'Materia primera 100% BIOREGIONAL','grup'=>array(2,3),	'bloc'=>'ecologia'	,'categoria0'=>'tots',		'valor'=>100,	'estat'=>0,'nivell'=>'RP'),
'3'=>array('segell'=>'Materia primera 90% BIOREGIONAL', 'grup'=>array(2,3),	'bloc'=>'ecologia'	,'categoria0'=>'tots',		'valor'=>90,	'estat'=>0,'nivell'=>'RP'),
'4'=>array('segell'=>'Producci� �tica',					'grup'=>array(4),	'bloc'=>'�tica'		,'categoria0'=>'tots',		'valor'=>100,	'estat'=>0,'nivell'=>'RP'),
'5'=>array('segell'=>"Higiene altament cuidada",		'grup'=>array(5),	'bloc'=>'salut'		,'categoria0'=>'alimentaci�','valor'=>50,	'estat'=>0,'nivell'=>'RP'),
'6'=>array('segell'=>'Etiquetatge correcte',			'grup'=>array(6),	'bloc'=>'SCADI'		,'categoria0'=>'tots',		'valor'=>50,	'estat'=>0,'nivell'=>'admin'),
'7'=>array('segell'=>'Preparaci� correcte de comandes CAB','grup'=>array(7),	'bloc'=>'SCADI'		,'categoria0'=>'tots',		'valor'=>50,'estat'=>0,'nivell'=>'admin'),
'8'=>array('segell'=>'Presentaci� correcte productes al SCADI',	'grup'=>array(8),	'bloc'=>'SCADI'		,'categoria0'=>'tots',	'valor'=>50,'estat'=>0,'nivell'=>'admin'),
'9'=>array('segell'=>'Mostra els prove�dors de la seva mat�ria primera a la fitxa del producte', 'grup'=>array(9), 'bloc'=>'SCADI','categoria0'=>'tots', 'valor'=>200,'estat'=>0,'nivell'=>'admin'),
'10'=>array('segell'=>'Materia primera 100% de productores CAC','grup'=>array(10,11),	'bloc'=>'suport m�tu', 'categoria0'=>'tots',	'valor'=>100,'estat'=>0,'nivell'=>'RP'),
'11'=>array('segell'=>'Materia primera 50% de productores CAC', 'grup'=>array(10,11),	'bloc'=>'suport m�tu', 'categoria0'=>'tots',	'valor'=>50, 'estat'=>0,'nivell'=>'RP'),
'12'=>array('segell'=>'�s PRIORITARI per la CAC', 'grup'=>array(12),	'bloc'=>'SCADI', 'categoria0'=>'tots',	'valor'=>150, 'estat'=>0,'nivell'=>'admin'),
);
$mEstatsSegellPerfil=array(
0=>'NO',
1=>'SI',
);

//no es pot alterar l'ordre, afegir a la c�a, es mostren ordenades per bloc
$mPropietatsSegellProducteConfig=array(
'0'=>array('segell'=>'Materia primera 100% ECOL�GICA',	'grup'=>array(0,1), 'bloc'=>'ecologia',		'categoria0'=>'tots',		'valor'=>100,'estat'=>0,'nivell'=>'RP'),
'1'=>array('segell'=>'Materia primera 90% ECOL�GICA',	'grup'=>array(0,1), 'bloc'=>'ecologia',		'categoria0'=>'tots',		'valor'=>90,'estat'=>0,'nivell'=>'RP'),
'2'=>array('segell'=>'Materia primera 100% BIOREGIONAL','grup'=>array(2,3), 'bloc'=>'ecologia',		'categoria0'=>'tots',		'valor'=>100,'estat'=>0,'nivell'=>'RP'),
'3'=>array('segell'=>'Materia primera 90% BIOREGIONAL', 'grup'=>array(2,3), 'bloc'=>'ecologia',		'categoria0'=>'tots',		'valor'=>90,'estat'=>0,'nivell'=>'RP'),
'4'=>array('segell'=>'Materia primera 100% de productores CAC','grup'=>array(4,5),	'bloc'=>'suport m�tu', 'categoria0'=>'tots',	'valor'=>100,'estat'=>0,'nivell'=>'RP'),
'5'=>array('segell'=>'Materia primera 50% de productores CAC', 'grup'=>array(4,5),	'bloc'=>'suport m�tu', 'categoria0'=>'tots',	'valor'=>50, 'estat'=>0,'nivell'=>'RP')
);
$mEstatsSegellProducte=array(
0=>'NO',
1=>'SI',
);


//es pot modificar l'odre i l'identificador
$mTipusUpload=array(
'1'=>array('nom'=>'ACTA reuni� CAC','nomFitxer'=>'acta_reunio_cac','periodicitat'=>'variable','nivells'=>'sadmin,admin,coord,usuari'),
'2'=>array('nom'=>'Comandes a prove�dors','nomFitxer'=>'comandes_a_proveidors','periodicitat'=>'mensual','nivells'=>'sadmin,admin,coord'),
'3'=>array('nom'=>'Canvis de Format prevists','nomFitxer'=>'canvis_de_format','periodicitat'=>'mensual','nivells'=>'sadmin,admin,coord'),
'4'=>array('nom'=>'Transformacions previstes','nomFitxer'=>'transformacions','periodicitat'=>'mensual','nivells'=>'sadmin,admin,coord'),
'5'=>array('nom'=>'Full Ruta Abastiment','nomFitxer'=>'full_ruta_abastiment','periodicitat'=>'mensual','nivells'=>'sadmin,admin,coord'),
'6'=>array('nom'=>'Full Ruta Distribuci�','nomFitxer'=>'full_ruta_distribucio','periodicitat'=>'mensual','nivells'=>'sadmin,admin,coord'),
'7'=>array('nom'=>'Butllet� CAC','nomFitxer'=>'butlleti_cac','periodicitat'=>'mensual','nivells'=>'sadmin,admin,coord,usuari,visitant'),
//'8'=>array('nom'=>'Resum Recolzament Selectiu','nomFitxer'=>'resum_recolzament_selectiu','periodicitat'=>'anual','nivells'=>'sadmin,admin,coord'),
'8'=>array('nom'=>'ACTA BIOREGI� NORD','nomFitxer'=>'acta_bioregio_nord','periodicitat'=>'variable','nivells'=>'sadmin,admin,coord,usuari'),
'9'=>array('nom'=>'ACTA BIOREGI� SUD','nomFitxer'=>'acta_bioregio_sud','periodicitat'=>'variable','nivells'=>'sadmin,admin,coord,usuari')
);
?>
		