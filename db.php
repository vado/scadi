<?php
//..............................................................................
function db_conect($mParams)
{
    // conexio a mysql:
    if(!$db=@mysql_connect($mParams['host'],$mParams['user'],$mParams['password']))
	{
		//echo "<br> 3".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*3*/',mysql_errno().'--'.mysql_error(),'2','dbAdmin.php');
	}
    //seleccionar base de dades:
    if(!$err=mysql_select_db($mParams['bd'],$db))
	{
		//echo "<br> 3".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*3*/',mysql_errno().'--'.mysql_error(),'2','dbAdmin.php');
	}
	
	return $db;
}

//..............................................................................
function db_conect_($mParams)
{
    // conexio a mysql:
    if(!$db=mysqli_connect($mParams['host'],$mParams['user'],$mParams['password'],$mParams['bd']))
	{
		mysqli_error($db);
		//err__('DB/*3*/',mysqli_error($db).'--'.mysql_error(),'2','dbAdmin.php');
	}
	mysqli_error($db);
	return $db;
	
	//////////////////////////////////////////////////////////////////////////////////
	//																				//
	//	ATENCIO: pel canvi a misqli, canviar ordre parametres a mysql_select_db		//
	//	I modificar totes les funcions mysql a mysqli								//
	//																				//
	//																				//
	//////////////////////////////////////////////////////////////////////////////////
}

//..............................................................................
function selectDb($dataBase,$db)
{
	if(!$err=mysql_select_db($dataBase,$db))
	{
		echo "<br> 3".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*3*/',mysql_errno().'--'.mysql_error(),'2','dbAdmin.php');
	}

	return;
}
//..............................................................................
function closeDb($db)
{
	if(!$err=mysql_close($db))
	{
		echo "<br> 50".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*3*/',mysql_errno().'--'.mysql_error(),'2','dbAdmin.php');
	}

	return;
}

//---------------------------------------------------------------
function db_getRebosts($db)
{
	global $mPars;
	$mRebosts=array();
	$i=0;
	
	if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where locate('1-puntEntrega,',categoria)=0 and locate('inactiu',categoria)=0 order by nom ASC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			while(list($key,$val)=each($mRow))
			{
				$mRebosts[$i][$key]=$mRow[$key];
			}
			$i++;
		}
	}
	return $mRebosts;
}

//---------------------------------------------------------------
function db_getGrupsUsuari($db)
{
	global $mPars;
	
	$mGrupsUsuari=array();
	$cadenaGrups='';
	$mGrups_=explode(',',$mPars['grups_usuari']);
	$or='OR';
	for($i=0;$i<count($mGrups_);$i++)
	{
		if($i==count($mGrups_)-1){$or='';}
		$cadenaGrups.=" id='".$mGrups_[$i]."' ".$or;
	}
	//echo "<br>select id,nom,categoria from rebosts_".$mPars['selRutaSufix']." where ".$cadenaGrups."  order by id DESC";
	if(!$result=mysql_query("select id,nom,categoria from rebosts_".$mPars['selRutaSufix']." where ".$cadenaGrups."  order by id DESC",$db))
	{
		//echo "<br> 75 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if
			(
				(
					(
						$mPars['nivell']!='sadmin' && $mPars['nivell']!='admin' 
					)
					&&
					substr_count(' '.$mRow['categoria'],'inactiu')==0
				)
				||
				(
					$mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' 
				)
			)
			{
				$mGrupsUsuari[$mRow['id']]['nom']=$mRow['nom'];
				$mGrupsUsuari[$mRow['id']]['categoria']=$mRow['categoria'];
			}
		}
	}
	return $mGrupsUsuari;
}

//---------------------------------------------------------------
function db_getGrupsRef($db)
{
	global $mPars;
	$mGrupsRef=array();
	$mGrupsRef[0]['nom']='CAC';
	$mGrupsRef[0]['adressa']='-';
	$mGrupsRef[0]['municipi']='-';
	$mGrupsRef[0]['propietats']='-';
	
	if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']."  order by nom ASC",$db))
	{
		//echo "<br> 138 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mGrup=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mGrupsRef[$mGrup['id']]=$mGrup;
		}
	}
	return $mGrupsRef;
}

//---------------------------------------------------------------
function db_getGrupsCALref($db)
{
	global $mPars;
	$mGrupsRef=array();
	
	if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where LOCATE('CAL:1;',CONCAT(' ',c_ecos))>0 AND LOCATE(CONCAT(',','inactiu',','),categoria)=0 order by nom ASC",$db))
	{
		//echo "<br> 138 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mGrup=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mGrupsRef[$mGrup['id']]=$mGrup;
		}
	}
	return $mGrupsRef;
}

//---------------------------------------------------------------
function db_getGrupsProductorsRef($db)
{
	global $mPars,$perfilsGrupIdChain;
	$mGrupsProductorsRef=array();
	
	if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',id,','),' ".$perfilsGrupIdChain."')>0 AND LOCATE(CONCAT(',','inactiu',','),categoria)=0 order by nom ASC",$db))
	{
		//echo "<br> 164 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mGrup=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mGrup['id']!='62') //cac-despeses-ruta
			{
				$mGrupsProductorsRef[$mGrup['id']]=$mGrup;
			}
		}
	}
	return $mGrupsProductorsRef;
}

//---------------------------------------------------------------
function db_getGrupsNoProductorsRef($db)
{
	global $mPars,$perfilsGrupIdChain;
	
	$mGrupsNoProductorsRef=array();
	
	if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',id,','),' ".$perfilsGrupIdChain."')=0 AND LOCATE(CONCAT(',','inactiu',','),categoria)=0 order by nom ASC",$db))
	{
		echo "<br> 186 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mGrup=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mGrupsNoProductorsRef[$mGrup['id']]=$mGrup;
		}
	}
	return $mGrupsNoProductorsRef;
}

//---------------------------------------------------------------
function getGrup($db)
{
//v37-global
	global $mPars,$mParams;
	$mGrup=array();

	if($mPars['grup_id']=='0')
	{
		$mGrup['id']='0';
		$mGrup['nom']='CAC';
		$mGrup['categoria']='';
		$mGrup['productors']='';
		$mGrup['usuari_id']=$mParams['sadminId'];
	}
	else
	{
		//echo "<br>select * from rebosts_".$mPars['selRutaSufix']." where ref='".$mPars['grup_id']."'";
		if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where ref='".$mPars['grup_id']."'",$db))
		{
			//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
   		}
		else
    	{
			$mGrup=mysql_fetch_array($result,MYSQL_ASSOC);
			if(substr_count($mGrup['categoria'],'inactiu')>0)
			{
				$mGrup['estat']='inactiu';
			}
			else if(substr_count($mGrup['categoria'],',actiu')>0)
			{
				$mGrup['estat']='actiu';
			}
			else
			{
				$mGrup['estat']='';
			}
		}
	}
	return $mGrup;
}

//------------------------------------------------------------------------------

function db_getPerfilsRef($db)//nom�s els que tenen productes
{
	global $mPars;
	
	$mPerfilsRef=array();
	$mPerfilsRef_=array();

		//echo "<br>select SUBSTRING(productor,1,LOCATE('-',productor)-1) from ".$mPars['taulaProductes']." WHERE llista_id='".$mPars['selLlistaId']."'";
		$result=mysql_query("select SUBSTRING(productor,1,LOCATE('-',productor)-1) from ".$mPars['taulaProductes']." WHERE llista_id='".$mPars['selLlistaId']."'",$db);
		//echo "<br> 270 db.php  ".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=@mysql_fetch_array($result,MYSQL_NUM))
		{
			$mPerfilsRef_[$mRow[0]]=array();
		}
		$productorsIdChain=','.(implode(',',array_keys($mPerfilsRef_))).',';
		
		//echo "<br>select * from ".$mPars['taulaProductors']." WHERE LOCATE(CONCAT(',',id,','),CONCAT(' ','".$productorsIdChain."'))>0 order by projecte ASC";
		$result=mysql_query("select * from ".$mPars['taulaProductors']." WHERE LOCATE(CONCAT(',',id,','),CONCAT(' ','".$productorsIdChain."'))>0 order by projecte ASC",$db);
		//echo "<br> 279 db.php  ".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPerfilsRef[$mRow['id']]=$mRow;
		}

	return $mPerfilsRef;
}

//------------------------------------------------------------------------------
 function db_getPerfilsRefTots($db)
 {
 	global $mPars;
 	
 	$mPerfilsRefTots=array();
 
 		
	$result=mysql_query("select * from ".$mPars['taulaProductors']." order by projecte ASC",$db);
	//echo "<br> 279 db.php  ".mysql_errno() . ": " . mysql_error(). "\n";
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mPerfilsRefTots[$mRow['id']]=$mRow;
	}
 	return $mPerfilsRefTots;
 }
 

//---------------------------------------------------------------
function db_getPerfil($db)
{
	global $mPars;

	$mPerfil=array();

//*v36 24-1-16 modificat query
	if(!$result=mysql_query("select * from ".$mPars['taulaProductors']." where id='".@$mPars['selPerfilRef']."'",$db))
	{
		//echo "<br> 276 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		$mPerfil=mysql_fetch_array($result,MYSQL_ASSOC);
	}

	return $mPerfil;
}

//---------------------------------------------------------------
function db_getPerfilsUsuari($db)
{
	global $mPars;
	$mPerfilsUsuari=array();
	$cadenaPerfils='';
	$mPerfils_=@explode(',',$mPars['perfils_productor']);
	$or='OR';
	for($i=0;$i<count($mPerfils_);$i++)
	{
		if($i==count($mPerfils_)-1){$or='';}
		$cadenaPerfils.=" id='".$mPerfils_[$i]."' ".$or;
	}
	if(!$result=mysql_query("select * from ".$mPars['taulaProductors']." where ".$cadenaPerfils."  order by id DESC",$db))
	{
		//echo "<br> 207  db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPerfilsUsuari[$mRow['id']]=$mRow;
		}
	}
	return $mPerfilsUsuari;
}

//---------------------------------------------------------------
function db_getPerfilsUsuariAgrup($db)
{
	global $mPars;

	$mPerfilsUsuariAgrup=array();
	$cadenaPerfils='';
	$mPerfils_=@explode(',',$mPars['perfils_productor']);
	$or='OR';
	for($i=0;$i<count($mPerfils_);$i++)
	{
		if($i==count($mPerfils_)-1){$or='';}
		$cadenaPerfils.=" id='".$mPerfils_[$i]."' ".$or;
	}
	if(!$result=mysql_query("select * from ".$mPars['taulaProductors']." where ".$cadenaPerfils."  AND usuari_id='".$mPars['usuari_id']."' order by id DESC",$db))
	{
		echo "<br> 384  db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPerfilsUsuariAgrup[$mRow['id']]=$mRow;
		}
	}
	return $mPerfilsUsuariAgrup;
}

//---------------------------------------------------------------
function db_getPerfilVinculatAgrup($grupRef,$db)
{
	global $mPars;
	
	
	if(!$result=mysql_query("select * from ".$mPars['taulaProductors']." where grup_vinculat='".$grupRef."'",$db))
	{
		//echo "<br> 228 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		$i=0;
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	}

	return $mRow;
}



//---------------------------------------------------------------
function getPeticionsGrup()
{
	global $mGrup;
	
	$mPeticionsGrup=array();
	if(@$mGrup['notes']!='')
	{	
		$mPeticionsGrup=explode('}{',$mGrup['notes']);
	}
	
	return $mPeticionsGrup;
}

//---------------------------------------------------------------
function db_getPeticionsGrup()
{
	global $mGrup;
	
	$mPeticionsGrup=array();
	if(@$mGrup['notes']!='')
	{	
		$mPeticionsGrup=explode('}{',$mGrup['notes']);
	}
	
	return $mPeticionsGrup;
}

//---------------------------------------------------------------
function db_associarPerfilAgrup($perfilId,$grupId,$db)
{
	global $mPars,$mParams,$mGrup;
	
	if($grupId!=0)
	{
		$productorsAssociats=@str_replace(','.$perfilId.',','',$mGrup['productors_associats']);
		$productorsAssociats=str_replace(',,',',',$productorsAssociats);
		$productorsAssociats.=','.$perfilId.',';
	
		//actualitzar historial
		
		if(!$result=mysql_query("update rebosts_".$mPars['selRutaSufix']." set productors_associats='".$productorsAssociats."' where id='".$grupId."'",$db))
		{
			//echo "<br> 337 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
			return false;
		}
	}
	return true;
}
//---------------------------------------------------------------
function db_getPerfilsRefGrup($db)
{
	global $mPars,$mGrup;
	
	$mPerfilsRefGrup=array();
	//echo "<br>"."select * from ".$mPars['taulaProductors']." where LOCATE(CONCAT(',',id,','),' ".$mGrup['productors_associats']."')>0";
	if(!$result=mysql_query("select * from ".$mPars['taulaProductors']." where LOCATE(CONCAT(',',id,','),' ".@$mGrup['productors_associats']."')>0",$db))
	{
		//echo "<br> 373 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
 	 	//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPerfilsRefGrup[$mRow['id']]=$mRow;
		}
	}
	

	return $mPerfilsRefGrup;
}

//---------------------------------------------------------------
function db_getNumProductesPerfilAllista($perfilId,$llistaId,$db)
{
	global $mPars,$mGrup;
	
	$numProductes=0;
	
	if($llistaId==0)
	{
		$taulaProductes='productes_'.$mPars['selRutaSufix'];
	}
	else
	{
		$taulaProductes='productes_grups';
	}
	
	//echo "<br>select COUNT(*) from ".$taulaProductes." where llista_id='".$llistaId."' AND SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$perfilId."'";
	if(!$result=mysql_query("select COUNT(*) from ".$taulaProductes." where llista_id='".$llistaId."' AND SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$perfilId."'",$db))
	{
		//echo "<br> 407 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		$mRow=mysql_fetch_array($result,MYSQL_NUM);
		return $mRow[0];
	}

	return $numProductes;
}

//---------------------------------------------------------------
function db_getNumProductesllista($llistaId,$db)
{
	global $mPars;
	
	$numProductes=0;
	
	
	if(!$result=mysql_query("select COUNT(*) from ".$mPars['taulaProductes']." where llista_id='".$llistaId."'",$db))
	{
		//echo "<br> 467 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		$mRow=mysql_fetch_array($result,MYSQL_NUM);
		
		return $mRow[0];
	}

	return $numProductes;
}
//---------------------------------------------------------------
function db_getLlistesRef($db)
{
	global $mPars;
	
	$mLlistesRef=array();

	if(!$result=mysql_query("select DISTINCT llista_id FROM productes_grups",$db))
	{
		//echo "<br> 429 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			$mLlistesRef[$i]=$mRow[0];
			$i++;
		}
	}
	return $mLlistesRef;
}

//---------------------------------------------------------------
function db_getGrupsAssociatsPerfil($db)
{
	global $mPars;
	
	$mGrupsAssociats=array();

	//echo "<br>select * FROM rebosts_".$mPars['selRutaSufix']." WHERE LOCATE(',".$mPars['selPerfilRef'].",',CONCAT(' ',productors_associats)>0)";
	if(!$result=mysql_query("select * FROM rebosts_".$mPars['selRutaSufix']." WHERE LOCATE(',".$mPars['selPerfilRef'].",',CONCAT(' ',productors_associats))>0",$db))
	{
		//echo "<br> 452 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mGrupsAssociats[$i]=$mRow['id'];
			$i++;
		}
	}
	return $mGrupsAssociats;
}

//---------------------------------------------------------------
function db_getGrupsAssociatsPerfilRef($db)
{
	global $mPars;
	
	$mGrupsAssociatsRef=array();

	if(isset($mPars['selPerfilRef']))
	{
		//echo "<br>select * FROM rebosts_".$mPars['selRutaSufix']." WHERE LOCATE(',".$mPars['selPerfilRef'].",',CONCAT(' ',productors_associats)>0)";
		if(!$result=mysql_query("select * FROM rebosts_".$mPars['selRutaSufix']." WHERE LOCATE(',".$mPars['selPerfilRef'].",',CONCAT(' ',productors_associats))>0",$db))
		{
			//echo "<br> 452 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
		}
		else
  		{
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mGrupsAssociatsRef[$mRow['id']]=$mRow;
			}
		}
	}
	return $mGrupsAssociatsRef;
}

//---------------------------------------------------------------
function db_getGrupsAmbComandaPerZonesPuntsEntrega($db)
{
	global $mPars,$mRebostsRef,$mPuntsEntrega,$cG2;

	$mComandesPerPuntsEntrega=array();
	$mGrupsAmbComandaPerZonesPuntsEntrega=array();
	$i=0;
	//obtenir punts entrega grups
	if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where rebost!='0-CAC' AND usuari_id='0'",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
   	}
	   
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
/*		if(!isset($mComandesPerPuntsEntrega[$mRow['punt_entrega']]))
		{
			$mComandesPerPuntsEntrega[$mRow['punt_entrega']]=array();
		}
*/
		if(isset($mRebostsRef[$mRow['punt_entrega']]))
		{
			$mCategoriesGrup=@getCategoriesGrup($mRebostsRef[$mRow['punt_entrega']],$db);
		}
		else
		{
			$mCategoriesGrup=@getCategoriesGrup($mPuntsEntrega[$mRow['punt_entrega']],$db);
		}
		if(@isset($cG2) && $cG2!='')
		{
			if($mCategoriesGrup[2]==$cG2)
			{
				if(!@isset($mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]]))
				{
					@$mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]]=array();
				}
		
				if(!@isset($mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']]))
				{
					@$mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']]=array();
				}

				$refRebost=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
		
				if(!@isset($mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']][$refRebost]))
				{
					@$mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']][$refRebost]=array();
				}
				$mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']][$refRebost]=$mRebostsRef[$refRebost];
			}
		}
		else
		{
			if(!@isset($mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]]))
			{
				@$mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]]=array();
			}
		
			if(!@isset($mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']]))
			{
				@$mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']]=array();
			}

			$refRebost=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
		
			if(!@isset($mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']][$refRebost]))
			{
				@$mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']][$refRebost]=array();
			}
			@$mGrupsAmbComandaPerZonesPuntsEntrega[$mCategoriesGrup[2]][$mRow['punt_entrega']][$refRebost]=@$mRebostsRef[$refRebost];
		}
	}
	return $mGrupsAmbComandaPerZonesPuntsEntrega;
}

//---------------------------------------------------------------
function getGrups($db,$opcio)
{
	global $mPars;
	$mGrups=array();
	$i=0;
	if($opcio=='tots')
	{
		if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where nom!='visitant' order by nom ASC",$db))
		{
			//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
    	}
    }
	else if($opcio=='actius')
	{
		if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where LOCATE('inactiu',categoria)=0 AND LOCATE('usuariIntern,',categoria)=0 AND  nom!='visitant' order by nom ASC",$db))
		{
			//echo "<br> 226 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
    	}
    }
	
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		while(list($key,$val)=each($mRow))
		{
			$mGrups[$i][$key]=$mRow[$key];
		}
		if(substr_count($mGrups[$i]['categoria'],'inactiu')>0)
		{
			$mGrups[$i]['estat']='inactiu';
		}
		else if(substr_count($mGrups[$i]['categoria'],',actiu')>0)
		{
			$mGrups[$i]['estat']='actiu';
		}
		else
		{
			$mGrups[$i]['estat']='';
		}
		$i++;
	}

	return $mGrups;
}


//---------------------------------------------------------------
function db_getRebostsRef($db)
{
	global $mPars;
	
	$mRebosts=array();
	$mRebosts[0]['nom']='CAC';
	$i=0;
	
	if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where locate('1-puntEntrega,',categoria)=0 and locate('inactiu,',categoria)=0 order by nom DESC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mRebosts[$mRow['id']]=$mRow;
			//echo "<br>".$mRow['id'].'  -  '.$mRebosts[$mRow['ref']]['nom'];
		}
	}
	return $mRebosts;

}

//---------------------------------------------------------------
function db_getMagatzems($db)
{
	global $mPars;
	$mMagatzems=array();
	$i=0;
	
	//echo "<br>select * from rebosts_".$mPars['selRutaSufix']." where locate('magatzem',categoria)>0 and locate('inactiu',categoria)=0 order by nom DESC";
	if(!$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where locate('magatzem',categoria)>0 and locate('inactiu',categoria)=0 order by nom DESC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			while(list($key,$val)=each($mRow))
			{
				$mMagatzems[$i][$key]=$mRow[$key];
			}
			$i++;
		}
	}
	
	return $mMagatzems;
}

//---------------------------------------------------------------
//+paginacio
function db_getProducte($producteId,$db)  //comandes.php, gestioMagatzems.php
{
	global $mPars;
	
	$mProducte=array();

	//echo "<br>"."select * from ".$mPars['taulaProductes']." where id='".$producteId."' AND llista_id='".$mPars['selLlistaId']."'";
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where id='".$producteId."' AND llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br> 19 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$mProducte=mysql_fetch_array($result,MYSQL_ASSOC);
	}
	
	return $mProducte;
}

//---------------------------------------------------------------
//+paginacio
function db_getProductes($db)  //comandes.php, gestioMagatzems.php, db_mail.php
{
	global $mPars,$mCategoriesGrup,$mParametres;

	
	if($mPars['selLlistaId']==0)
	{
		//filtrar segons perfils actius
		$perfilsRefActiusChain=db_getPerfilsRefActiusChain($db);
	}
	
	
	$orderBy='';
	$where_=" id!=0 "; //redundant
	$limit='';
	
//*v36-15-11-26 condicio
	if($mPars['sortBy']!='')
	{
		$orderBy=$mPars['sortBy'];
		
		if($mPars['sortBy']=='productor')
		{
			$orderBy=" (SUBSTRING(productor,LOCATE('-',productor))) ";
		}
		
		
		$orderBy=" order by ".$orderBy." ".$mPars['ascdesc'];
	}

	if($mPars['veureProductesDisponibles']==1)
	{
		$where_.=" AND actiu=1 ";
	}

	$where_.=" AND llista_id=".$mPars['selLlistaId']." ";
	
	if($mPars['selLlistaId']==0)
	{
		$where_.=" AND LOCATE(
							CONCAT(
									',',
									SUBSTRING(productor,1,LOCATE('-',productor)-1),
									','
						          ),
							'".$perfilsRefActiusChain."'
						  )>0 ";
	}
	$mProductes=array();
	$i=0;
	
	//echo "<br>select * from ".$mPars['taulaProductes']." where ".$where_." ".$orderBy." ".$limit;
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where ".$where_." ".$orderBy." ".$limit,$db))
	{
		//echo "<br> 19 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mRow['visible']=0;
			$mProductes[$i]=$mRow;
			$i++;
		}
	}

	//aplicacio del filtre
	if($mPars['etiqueta']==$mPars['etiqueta2']){$mPars['etiqueta']='TOTS';}

	if($mPars['etiqueta2']!="CAP"){$mPars['etiqueta']='TOTS';}


	if($mPars['vProductor']!='TOTS')
	{
		$where_.=" AND  SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPars['vProductor']."' ";
	}

	if($mPars['vCategoria']!='TOTS')
	{
		$where_.=" AND  categoria0='".$mPars['vCategoria']."' ";
		
	}

	if($mPars['vSubCategoria']!='TOTS')
	{
		$categoria0=substr($mPars['vSubCategoria'],0,strpos($mPars['vSubCategoria'],'-'));
		$categoria10=substr($mPars['vSubCategoria'],strpos($mPars['vSubCategoria'],'-')+1);

		$where_.=" AND  categoria0='".$categoria0."' AND categoria10='".$categoria10."' ";
	}
	
	if($mPars['etiqueta']!='TOTS')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta']."',tipus)!=0 ";
	}

	if($mPars['etiqueta2']!='CAP')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta2']."',tipus)=0 ";
	}
	
	if($mPars['selLlistaId']==0 && @$mCategoriesGrup[1]!='rebost')
	{
		$where_.=" AND LOCATE('recolzat-100%',tipus)=0 ";
	}

	if($mPars['selLlistaId']==0 && $mParametres['aplicarValorTallSegell']['valor']*1==1) //aplicar valortallsegell si est� actiu
	{
		$where_.=" AND segell*1>".($mParametres['valorTallSegell']['valor']*1)." ";
	}
	
	
	$result=mysql_query("select COUNT(*) from ".$mPars['taulaProductes']." where ".$where_." ".$orderBy,$db);
	$mRow=@mysql_fetch_array($result,MYSQL_NUM);

//	if($mPars['paginacio']==1)
//	{
		$mPars['numItemsF']=$mRow[0];
		$mPars['numPags']=CEIL($mPars['numItemsF']/$mPars['numItemsPag']);
		
//		$limit=" LIMIT ".$mPars['numProdsPag']." offset ".(($mPars['pagS'])*$mPars['numProdsPag']);
//	}
//	else
//	{
//		$limit='';
//	}
	$mProductesF=array();
	
	//echo "select * from ".$mPars['taulaProductes']." where ".$where_." ".$orderBy." ".$limit;
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where ".$where_." ".$orderBy." ".$limit,$db))
	{
		//echo "<br> 19 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
			{
				$mRow['visible']=1;
				$mProductesF[$mRow['id']]=$mRow;
			}
			$i++;
		}
	}

	while(list($key,$val)=each($mProductes))
	{
		if(array_key_exists($mProductes[$key]['id'],$mProductesF))
		{
			$mProductes[$key]=$mProductesF[$mProductes[$key]['id']];
		}
	}
	reset($mProductes);
	
	return $mProductes;
}

//---------------------------------------------------------------
function db_getProductes2($db)
{
	global $mPars;

	$orderBy='';
	$where_=" id!=0 "; //redundant
	
//*v36-15-11-26 condicio
	if($mPars['sortBy']!='')
	{
		$orderBy=$mPars['sortBy'];
		
		if($mPars['sortBy']=='productor')
		{
			$orderBy=" (SUBSTRING(productor,LOCATE('-',productor))) ";
		}
		
		$orderBy=" order by ".$orderBy." ".$mPars['ascdesc'];
	}

	if($mPars['veureProductesDisponibles']==1)
	{
		$where_.=" AND actiu=1 ";
	}

	//aplicacio del filtre
	if($mPars['etiqueta']==$mPars['etiqueta2']){$mPars['etiqueta']='TOTS';}

	if($mPars['etiqueta2']!="CAP"){$mPars['etiqueta']='TOTS';}

	if($mPars['vProducte']!='TOTS')
	{
		$where_.=" AND id='".$mPars['vProducte']."' ";
	}

	if($mPars['vProductor']!='TOTS')
	{
		if(str_replace(',','',$mPars['vProductor'])!=''){$where_productors=' AND (';$where_productors_fi=') ';}else{$where_productors='';$where_productors_fi='';}
		
		$mProductorsId=explode(',',$mPars['vProductor']);
		while(list($key,$productorId)=each($mProductorsId))
		{
			if($productorId!='')
			{
				$where_productors.=" SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$productorId."' OR ";
			}
		}
		if($where_productors!='')
		{
			$where_productors=substr($where_productors,0,strlen($where_productors)-3);
		}
		$where_.=$where_productors.$where_productors_fi;
	}

	if($mPars['vCategoria']!='TOTS')
	{
		$where_.=" AND  categoria0='".$mPars['vCategoria']."' ";
		
	}

	if($mPars['vSubCategoria']!='TOTS')
	{
		$categoria0=substr($mPars['vSubCategoria'],0,strpos($mPars['vSubCategoria'],'-'));
		$categoria10=substr($mPars['vSubCategoria'],strpos($mPars['vSubCategoria'],'-')+1);

		$where_.=" AND  categoria0='".$categoria0."' AND categoria10='".$categoria10."' ";
	}
	
	if($mPars['etiqueta']!='TOTS')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta']."',tipus)!=0 ";
	}

	if($mPars['etiqueta2']!='CAP')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta2']."',tipus)=0 ";
	}

	$where_.=" AND llista_id=".$mPars['selLlistaId']." ";
	
	$mProductes=array();
	
	//echo "<br>select * from ".$mPars['taulaProductes']." where ".$where_." ".$orderBy;
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where ".$where_." ".$orderBy,$db))
	{
		//echo "<br> 19 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mPars['paginacio']==1)
			{
				if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
				{
					$mRow['visible']=1;
					$mProductes[$mRow['id']]=$mRow;
				}
				else
				{
					$mRow['visible']=0;
					$mProductes[$mRow['id']]=$mRow;
				}
			}
			else
			{
				$mRow['visible']=1;
				$mProductes[$mRow['id']]=$mRow;
			}
			
			$i++;
		}
	}
	
	return $mProductes;
}

//---------------------------------------------------------------
function db_getProductes3($db)
{
	global $mPars;
	
	$mProductes=array();

	$orderBy='';
	$where_=" id!=0 "; //redundant
	$limit='';
	
	if($mPars['sortBy']!='')
	{
		$orderBy=" order by ".$mPars['sortBy']." ".$mPars['ascdesc'];
	}

	if($mPars['veureProductesDisponibles']==1)
	{
		$where_.=" AND actiu=1 ";
	}

	//aplicacio del filtre
	if($mPars['etiqueta']==$mPars['etiqueta2']){$mPars['etiqueta']='TOTS';}

	if($mPars['etiqueta2']!="CAP"){$mPars['etiqueta']='TOTS';}

	if($mPars['vProductor']!='TOTS')
	{
		$where_.=" AND  SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPars['vProductor']."' ";
	}

	if($mPars['vCategoria']!='TOTS')
	{
		$where_.=" AND  categoria0='".$mPars['vCategoria']."' ";
		
	}

	if($mPars['vSubCategoria']!='TOTS')
	{
		$categoria0=substr($mPars['vSubCategoria'],0,strpos($mPars['vSubCategoria'],'-'));
		$categoria10=substr($mPars['vSubCategoria'],strpos($mPars['vSubCategoria'],'-')+1);

		$where_.=" AND  categoria0='".$categoria0."' AND categoria10='".$categoria10."' ";
	}
	
	if($mPars['etiqueta']!='TOTS')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta']."',tipus)!=0 ";
	}

	if($mPars['etiqueta2']!='CAP')
	{
		$where_.=" AND LOCATE('".$mPars['etiqueta2']."',tipus)=0 ";
	}
	

	$where_.=" AND llista_id='".$mPars['selLlistaId']."' ";

	$mProductesF=array();
	
	//echo "<br>select * from ".$mPars['taulaProductes']." where ".$where_." ".$orderBy." ".$limit;
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where ".$where_." ".$orderBy." ".$limit,$db))
	{
		//echo "<br> 987 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mProductes[$mRow['id']]=$mRow;
		}
	}

	return $mProductes;
}



//---------------------------------------------------------------
function db_guardarComanda($guardarComanda,$db)
{
	global 	$mParametres,
			$mRebost,
			$mProductes,
			$mPars,
			$mParsCTAR,
			$mGuardarFormaPagament,
			$mPropietatsGrup,
			$mPropietatsSelUsuari,
			$mComptesUsuari,
			$mComptesGrup,
			$sumaRecolzamentComandesSelUsuari,
			$mRuta;
	
	
		//comprovar que l'usuari pot fer comanda en aquest grup.

		$mComandaAnterior=db_getComanda($mPars['grup_id'],$db); //arriben tots els productes demanats amb quantitat>0
		$mComandaAnteriorCopiar=db_getComandaCopiar($mPars['grup_id'],$db);
			if(!isset($mComandaAnteriorCopiar['compte_ecos'])){$mComandaAnteriorCopiar['compte_ecos']='';}
			if(!isset($mComandaAnteriorCopiar['compte_cieb'])){$mComandaAnteriorCopiar['compte_cieb']='';}

		$mComandaCombinada=array();

		//$guardarcomanda: nom�s arriben els productes de la seleccio visualitzada, siguin en quantitat=0 o  quantitat>0

		//combinar la comanda anterior amb la comanda enviada ($guardarcomanda)
		$mProductesComanda=explode(';',$guardarComanda);
		$n=0;
		for($i=0;$i<count($mComandaAnterior);$i++)
		{
			$mComandaCombinada[$mComandaAnterior[$i]['index']]['anterior']=$mComandaAnterior[$i]['quantitat'];
			$mComandaCombinada[$mComandaAnterior[$i]['index']]['actual']=$mComandaAnterior[$i]['quantitat'];
		}

		for($i=0;$i<count($mProductesComanda);$i++)
		{
			$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
		
			$index=str_replace('producte_','',$mIndexQuantitat[0]);
			$quantitat=@$mIndexQuantitat[1];
			if($index!='')
			{
				//si existia a la comanda anterior guardar-hi la nova quantitat >/=0
				if(array_key_exists($index,$mComandaCombinada))
				{
					$mComandaCombinada[$index]['actual']=$quantitat;
				}
				else	//si no, i es >0, afegir
				{
					if($quantitat*1>0)
					{
						$mComandaCombinada[$index]['anterior']=0;
						$mComandaCombinada[$index]['actual']=$quantitat;
					}
				}
			}
		}
		//refer cadena comanda i passar comanda combinada a comanda
		$chain='';
		$n=0;
		while(list($index,$mQuantitat)=each($mComandaCombinada))
		{
			if($mQuantitat['actual']*1>0)
			{
				$chain.='producte_'.$index.':'.$mQuantitat['actual'].';';
			}
		}
		reset($mComandaCombinada);
		
		$guardarComanda=$chain;
		//veure si el rebost ja ha fet una comanda per aquest periode
	
		//si hi �s:
		
		$mComandesEspecialsRebost=array();
		
		if($mPars['grup_id']!='0' && $mPars['selUsuariId']!='0')
		{
				//v37-condicional
				if(count($mRuta)==1)
				{
				
				//-1. veure taula comandes_especials1 i incorporar a variable 'guardarcomanda'
				$result=@mysql_query("select c7 from comandes_especials1_".$mPars['selRutaSufix']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."'   AND usuari_id='".$mPars['selUsuariId']."'",$db);
				//echo "<br> 1 ".mysql_errno() . ": " . mysql_error(). "\n";
				while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
				{
					if(!array_key_exists($mRow['c7'],$mComandesEspecialsRebost))
					{
						@$mComandesEspecialsRebost[$mRow['c7']]=0;
					}
					$mComandesEspecialsRebost[$mRow['c7']]+=1;
				}
				while(list($key,$val)=each($mComandesEspecialsRebost))
				{
					if(substr_count($guardarComanda,'producte_'.$key.':')>0)
					{
						$chain1831=$guardarComanda;
						//obtenir cadena de producte per a substituir
						$comandaEspecial=substr($chain1831, strpos($chain1831,'producte_'.$key.':'));
						$comandaEspecial=substr($comandaEspecial, 0, strpos($comandaEspecial,';'));
						$guardarComanda=str_replace($comandaEspecial,'producte_'.$key.':'.$val.';',$guardarComanda);
					}
					else
					{
						$guardarComanda=$guardarComanda.'producte_'.$key.':'.$val.';';
					}
				}
				
				}		

				// 0. eliminar copia seg comanda anterior:
				//$result=@mysql_query("delete from comandes_seg where periode_comanda='".$periodeComanda."' AND rebost='".$mPars['rebost']."'",$db);
				//echo "<br> 1 ".mysql_errno() . ": " . mysql_error(). "\n";

				//0- revisar si el grup ja ha fet comanda. Si no, guardar forma de pagament del grup segons dades d'�ltima comanda
				$result=@mysql_query("select * from comandes_".$mPars['selRutaSufix']." where rebost='".$mPars['grup_id'].'-'.$mRebost['nom']."'",$db);
				//echo "<br> 577 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
				$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
				//si el grup no ha fet comanda, guardar forma pagament i punt d'entrega del grup
				if(!$mRow)				
				{
					$mRow=false;
					//v37-condicional
					if(count($mRuta)==1)
					{
					//obtenir comanda ruta anterior d'aquest grup, usuari GRUP
					$ruta0_=date('ym',mktime(0,0,0,date('m')-1,date('d'),date('Y')));
					
					$result=@mysql_query("select * from comandes_".$ruta0_." where rebost='".$mPars['grup_id'].'-'.$mRebost['nom']."' and usuari_id='0'",$db);
					$mRow=@mysql_fetch_array($result,MYSQL_ASSOC);
					}
					if(!$mRow)				
					{
						//si no va fe comanda el mes anterior, guardar opcions pagament de grup per defecte
						//echo "<br> 589 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
						//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');

						//---------------------------
						//registre historial comanda
						$registre='';
						$mGC['usuari_id']='0';
						$mGC['f_pagament']='0,0,0,0,0,0,100,0,0,7';
						$mGC['punt_entrega']='';
	
						while(list($key,$val)=each($mGC))
						{
							$registre.='{p:guardarComanda;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
						}
						reset($mGC);
						//---------------------------
						
						if(!$result=@mysql_query("insert into comandes_".$mPars['selRutaSufix']." values('','".(date('YmdHis'))."','".$mParametres['periodeComanda']['valor']."','".$mPars['grup_id'].'-'.$mRebost['nom']."','0','".(date('dmY'))."','','','','','','0,0,0,0,0,0,100,0,0,7','".$mPars['grup_id']."','".$registre."','','','".$mPars['llista_id']."','')",$db))
						{
							//echo "<br> 599 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
							//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
						}
					}
					else
					{
						// si en va fer, quardar opcions en aquesta ruta (percentatges moneda i punt entrega
						$mFormaPagament=explode(',',$mRow['f_pagament']);
						if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
						if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
						if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
						if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
						if(!isset($mFormaPagament[4])){$mFormaPagament[4]=0;}
						if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
						if(!isset($mFormaPagament[6])){$mFormaPagament[6]=0;}
						if(!isset($mFormaPagament[7])){$mFormaPagament[7]=0;}
						if(!isset($mFormaPagament[8])){$mFormaPagament[8]=0;}
						if(!isset($mFormaPagament[9])){$mFormaPagament[9]=7;}
						
						$totalUms=$mFormaPagament[0]+$mFormaPagament[1]+$mFormaPagament[2];
						if($totalUms==0){$totalUms=1;}
						$ppEcos=number_format($mFormaPagament[0]*100/$totalUms,2);
						$ppEb=number_format($mFormaPagament[1]*100/$totalUms,2);
						$ppEuros=number_format($mFormaPagament[2]*100/$totalUms,2);
						
						$mPars['f_pagament']="0,0,0,0,0,0,".$ppEcos.",".$ppEb.",".$ppEuros.",".$mFormaPagament[9];
						
						//---------------------------
						//registre historial comanda
						$registre='';
						$mGC['usuari_id']=$mPars['usuari_id'];
						$mGC['f_pagament']="0,0,0,0,0,0,".$ppEcos.",".$ppEb.",".$ppEuros.",".$mFormaPagament[9];
						$mGC['punt_entrega']='';
						$mGC['compte_ecos']=$mRow['compte_ecos'];
						$mGC['compte_cieb']=$mRow['compte_cieb'];
							
						while(list($key,$val)=each($mGC))
						{
							$registre.='{p:guardarComanda;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
						}
						reset($mGC);
						//---------------------------

						//guardar la comanda del mes anterior, d'aquest grup, grup 0, nom�s forma pagament i punt entrega
						if(!$result=@mysql_query("insert into comandes_".$mPars['selRutaSufix']." values('','".(date('YmdHis'))."','".$mParametres['periodeComanda']['valor']."','".$mPars['grup_id'].'-'.$mRebost['nom']."','0','".(date('dmY'))."','','','','','','0,0,0,0,0,0,".$ppEcos.",".$ppEb.",".$ppEuros.",".$mFormaPagament[9]."','".$mRow['punt_entrega']."','".$registre."','".$mRow['compte_ecos']."','".$mRow['compte_cieb']."','','')",$db))
						{
							//echo "<br> 599 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
							//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
						}
					}
				}
				
				// 1. guardar a bd-seg la comanda anterior
				//echo "<br>insert into comandes_seg_".$mPars['selRutaSufix']." values('','".$mComandaAnteriorCopiar['num_comanda']."','".$mComandaAnteriorCopiar['periode_comanda']."','".$mComandaAnteriorCopiar['rebost']."','".$mComandaAnteriorCopiar['usuari_id']."','".$mComandaAnteriorCopiar['data']."','".$mComandaAnteriorCopiar['resum']."','','','','','".$mComandaAnteriorCopiar['f_pagament']."','".$mComandaAnteriorCopiar['punt_entrega']."','".$mComandaAnteriorCopiar['propietats']."','".$mComandaAnteriorCopiar['compte_ecos']."','".$mComandaAnteriorCopiar['compte_cieb'];
				if(!$result=@mysql_query("insert into comandes_seg values('','".$mComandaAnteriorCopiar['num_comanda']."','".$mComandaAnteriorCopiar['periode_comanda']."','".$mComandaAnteriorCopiar['rebost']."','".$mComandaAnteriorCopiar['usuari_id']."','".$mComandaAnteriorCopiar['data']."','".$mComandaAnteriorCopiar['resum']."','','','','','".$mComandaAnteriorCopiar['f_pagament']."','".$mComandaAnteriorCopiar['punt_entrega']."','".$mComandaAnteriorCopiar['historial']."','".$mComandaAnteriorCopiar['compte_ecos']."','".$mComandaAnteriorCopiar['compte_cieb']."','".$mPars['selLlistaId']."','')",$db))
				{
					//echo "<br> 609 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
    				return false;
				}

				// 2. eliminar comanda anterior:
				//echo "<br>delete from comandes_".$mPars['selRutaSufix']." where id='".$mPars['idComanda']."'";
				if(!$result=mysql_query("delete from comandes_".$mPars['selRutaSufix']." where id='".$mPars['idComanda']."'",$db))
				{
					//echo "<br> 617 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
					return false;
				}
			
				// 3. actualitzar estoc a estat anterior:
				if(count($mComandaCombinada)>0)
				{
					while(list($index,$mQuantitat)=each($mComandaCombinada))
					{
						if(!$result=mysql_query("update productes_".$mPars['selRutaSufix']." set estoc_disponible=estoc_disponible+".$mQuantitat['anterior']." where id='".$index."'",$db))
						{
							//echo "<br> 628 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
							return false;
						}
					}
					reset($mComandaCombinada);				
				}
			
			
				// 4. guardar a bd la nova comanda
						//---------------------------
						//registre historial comanda
						$registre='';
						$mGC['usuari_id']=$mPars['selUsuariId'];
						$mGC['f_pagament']=@$mPars['f_pagament'];
						$mGC['resum']=$guardarComanda;
						$mGC['compte_ecos']=strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']);
						$mGC['compte_cieb']=strtoupper($mGuardarFormaPagament['fPagamentCompteEb']);
						//$mGC['punt_entrega']='';
	
						while(list($key,$val)=each($mGC))
						{
							$registre.='{p:guardarComanda;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
						}
						reset($mGC);
						//---------------------------
				if(!$result=@mysql_query("insert into comandes_".$mPars['selRutaSufix']." values('','".(date('YmdHis'))."','".$mParametres['periodeComanda']['valor']."','".$mPars['grup_id'].'-'.$mRebost['nom']."','".$mPars['selUsuariId']."','".(date('dmY'))."','".$guardarComanda."','','','','','".$mPars['f_pagament']."','','".$registre."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."','".$mPars['selLlistaId']."','')",$db))
				{
					//echo "<br> 639 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');

					// recuperar copia seg comanda:
					$mComandaSeg=get_comandaSeg($mPars['grup_id'].'-'.$mRebost['nom'],$db);
					$result=@mysql_query("insert into comandes_".$mPars['selRutaSufix']." values('','".$mComandaSeg['num_comanda']."','".$mComandaSeg['periode_comanda']."','".$mComandaSeg['rebost']."','".$mComandaSeg['usuari_id']."','".$mComandaSeg['data']."','".$mComandaSeg['resum']."','".$mComandaAnteriorCopiar['ctiar']."','".$mComandaSeg['ms_ctiar']."','".$mComandaSeg['ctear']."','".$mComandaSeg['ms_ctear']."', '".$mComandaSeg['f_pagament']."','','".$mComandaSeg['historial']."','".$mComandaSeg['compte_ecos']."','".$mComandaSeg['compte_cieb']."','".$mComandaSeg['propietats']."','".$mComandaSeg['llista_id']."','".$mComandaSeg['propietats']."')",$db);
					//echo "<br> 645 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
				
    				return false;
				}
			
				// 5. actualitzar estoc a nou estat:
				if(count($mComandaCombinada)>0)
				{
					while(list($index,$mQuantitat)=each($mComandaCombinada))
					{
						if(!$result=mysql_query("update productes_".$mPars['selRutaSufix']." set estoc_disponible=estoc_disponible-".$mQuantitat['actual']." where id='".$index."'",$db))
						{
							//echo "<br> 657 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
							return false;
						}
					}
					reset($mComandaCombinada);				
				}
			
				//6. guardar forma de pagament: // no es modifica el punt d'entrega
				$totalUms=$mGuardarFormaPagament['fPagamentEcos']+$mGuardarFormaPagament['fPagamentEb']+$mGuardarFormaPagament['fPagamentEu'];
				if($totalUms==0){$totalUms=1;}
				$ppEcos=number_format($mGuardarFormaPagament['fPagamentEcos']*100/$totalUms,2);
				$ppEb=number_format($mGuardarFormaPagament['fPagamentEb']*100/$totalUms,2);
				$ppEuros=number_format($mGuardarFormaPagament['fPagamentEu']*100/$totalUms,2);
				
						//---------------------------
						//registre historial comanda
						$registre='';
						$mGC['usuari_id']=$mPars['usuari_id'];
						$mGC['f_pagament']=$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.".".$mGuardarFormaPagament['fPagamentOpcioConversor'].".";
						$mGC['compte_ecos']=strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']);
						$mGC['compte_cieb']=strtoupper($mGuardarFormaPagament['fPagamentCompteEb']);
						//$mGC['resum']=$mComandaSeg['resum'];
						//$mGC['punt_entrega']='';
	
						while(list($key,$val)=each($mGC))
						{
							$registre.='{p:guardarComanda;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
						}
						reset($mGC);
						//---------------------------
				//echo "<br>update comandes_".$mPars['selRutaSufix']." set compte_ecos='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."',compte_cieb='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."',historial=CONCAT('".$registre."',historial),f_pagament='".$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.",".$mGuardarFormaPagament['fPagamentOpcioConversor']."' where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."'";
				if(!$result=mysql_query("update comandes_".$mPars['selRutaSufix']." set compte_ecos='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."',compte_cieb='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."',historial=CONCAT('".$registre."',historial),f_pagament='".$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.",".$mGuardarFormaPagament['fPagamentOpcioConversor']."' where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."'",$db))
				{
					//echo "<br> 667 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
					return false;
				}
				
				/*
				//7. guardar comptes:
				if(!$result=mysql_query("update usuaris set compte_ecos='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."',compte_cieb='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."' where id='".$mPars['selUsuariId']."'",$db))
				{
					//echo "<br> 9 ".mysql_errno() . ": " . mysql_error(). "\n";
					return false;
				}
				*/
				//7. guardar utilitzatEcosR
				
				if(isset($mPropietatsSelUsuari['comissio']) && $mPropietatsSelUsuari['comissio']!=='')
				{
					$idComandaEliminada=$mPars['idComanda'];
					
						db_getComandaCopiar($mPars['grup_id'],$db);
						
					if(isset($mComptesUsuari['utilitzat_ecos_r_chain']))
					{
						$mRecolzamentComandesUsuari=getRecolzamentComandesUsuari($mComptesUsuari['utilitzat_ecos_r_chain']);
					}
					else
					{
						$mComptesUsuari['utilitzat_ecos_r_chain']='';
						$mRecolzamentComandesUsuari=array();
					}
					unset($mRecolzamentComandesUsuari[$idComandaEliminada]);
					$mRecolzamentComandesUsuari[$mPars['idComanda']]=$mGuardarFormaPagament['utilitzatEcosR'];
					$chain=makeChainRecolzamentComandesUsuari($mRecolzamentComandesUsuari);
					$mComptesUsuari['utilitzat_ecos_r']=$sumaRecolzamentComandesSelUsuari;
					$mComptesUsuari['utilitzat_ecos_r_chain']=$chain;
					//$mPropietats['disponibleEcosR']=$mGuardarFormaPagament['disponibleEcosR'];
					db_guardarComptesUsuari($db);
				}

		}
/*
		else if($mPars['grup_id']=='0')
		{
		
					// 0. eliminar copia seg comanda anterior:
					//if(!$result=@mysql_query("delete from comandes_seg where periode_comanda='".$periodeComanda."' AND rebost='".$mPars['rebost']."'",$db))
					//{
					//	echo "<br> 8 ".mysql_errno() . ": " . mysql_error(). "\n";
					//}
		
					// 1. guardar a bd-seg la comanda anterior
					if(isset($mComandaAnteriorCopiar['num_comanda']))
					{
						if(!$result=mysql_query("insert into comandes_seg values('','".$mComandaAnteriorCopiar['num_comanda']."','".$mComandaAnteriorCopiar['periode_comanda']."','".$mComandaAnteriorCopiar['rebost']."','".$mComandaAnteriorCopiar['usuari_id']."','".$mComandaAnteriorCopiar['data']."','".$mComandaAnteriorCopiar['resum']."','".$mComandaAnteriorCopiar['ctiar']."','".$mComandaAnteriorCopiar['ms_ctiar']."','".$mComandaAnteriorCopiar['ctear']."','".$mComandaAnteriorCopiar['ms_ctear']."','".$mComandaAnteriorCopiar['f_pagament']."','".$mComandaAnteriorCopiar['punt_entrega']."','".$mComandaAnteriorCopiar['historial']."','".$mComandaAnteriorCopiar['compte_ecos']."','".$mComandaAnteriorCopiar['compte_cieb']."','".$mComandaAnteriorCopiar['llista_id']."','".$mComandaAnteriorCopiar['propietats']."')",$db))
						{
							//echo "<br> 690 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		    				return false;
						}
						// 2. eliminar comanda anterior:
						if(!$result=mysql_query("delete from comandes_".$mPars['selRutaSufix']." where id='".$mPars['idComanda']."'",$db))
						{
							//echo "<br> 697 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
							return false;
						}
					}
		
					
					// 32. per a cada producte posar estoc previst i estoc disponible a zero:
					if(!$result=mysql_query("update productes_".$mPars['selRutaSufix']." set estoc_previst=0",$db))
					{
						echo "<br> 705 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
						return false;
					}

					
					// 4. actualitzar estoc a estat anterior:

					if(count($mComandaCombinada)>0)
					{
						while(list($index,$mQuantitat)=each($mComandaCombinada))
						{
							if(!$result=mysql_query("update productes_".$mPars['selRutaSufix']." set estoc_disponible=estoc_disponible-".$mQuantitat['anterior']." where id='".$index."'",$db))
							{
								echo "<br> 717 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
								return false;
							}
						}
						reset($mComandaCombinada);				
					}

					
		
					// 5. guardar a bd la nova comanda
				// 4. guardar a bd la nova comanda
						//---------------------------
						//registre historial comanda
						$registre='';
						$mGC['usuari_id']=$mPars['usuari_id'];
						//GC['f_pagament']=$mPars['f_pagament'];
						$mGC['resum']=$guardarComanda;
						//$mGC['punt_entrega']='';
	
						while(list($key,$val)=each($mGC))
						{
							$registre.='{p:guardarComanda;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
						}
						reset($mGC);
						//---------------------------
					if(!$result=@mysql_query("insert into comandes_".$mPars['selRutaSufix']." values('','".(date('YmdHis'))."','".$mParametres['periodeComanda']['valor']."','0-CAC','".$mPars['usuari_id']."','".(date('dmY'))."','".$guardarComanda."','".$mParsCTAR['ctiar']."','".$mParsCTAR['ms_ctiar']."','".$mParsCTAR['ctear']."','".$mParsCTAR['ms_ctear']."','".$mPars['f_pagament']."','".$mPars['puntEntregaRef']."','".$registre."','','','".$mPars['selLlistaId']."','')",$db))
					{
						echo "<br> 729 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		    		
						// recuperar copia seg comanda:
						$mComandaSeg=get_comandaSeg($mPars['grup_id'].'-'.$mRebost['nom'],$db);
						if(!$result=@mysql_query("insert into comandes_".$mPars['selRutaSufix']." values('','".$mComandaSeg['num_comanda']."','".$mComandaSeg['periode_comanda']."','".$mComandaAnteriorCopiar['rebost']."','".$mComandaSeg['usuari_id']."','".$mComandaSeg['data']."','".$mComandaAnteriorCopiar['resum']."','".$mComandaAnteriorCopiar['ctiar']."','".$mComandaSeg['ms_ctiar']."','".$mComandaSeg['ctear']."','".$mComandaSeg['ms_ctear']."','".$mComandaSeg['f_pagament']."','".$mComandaSeg['punt_entrega']."','".$mComandaAnteriorCopiar['historial']."','".$mComandaAnteriorCopiar['compte_ecos']."','".$mComandaAnteriorCopiar['compte_cieb']."','".$mComandaAnteriorCopiar['llista_id']."','".$mComandaAnteriorCopiar['propietats']."')",$db))
						{
							echo "<br> 736 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
						}
					
						return false;
					}
					
					// 6. actualitzar estoc a nou estat
					$indexProductesCacChain='';
					if(count($mComandaCombinada)>0)
					{
						while(list($index,$mQuantitat)=each($mComandaCombinada))
						{
							$indexProductesCacChain.=','.$index;

							if(!$result=mysql_query("update productes_".$mPars['selRutaSufix']." set estoc_disponible=estoc_disponible+".$mQuantitat['actual']." where id='".$index."'",$db))
							{
								echo "<br> 751 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
								return false;
							}
							if(!$result=mysql_query("update productes_".$mPars['selRutaSufix']." set estoc_previst=".$mQuantitat['actual']." where id='".$index."'",$db))
							{
								echo "<br> 756 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
								return false;
							}

							if(!$result=mysql_query("update productes_".$mPars['selRutaSufix']." set estoc_disponible=estoc_disponible-(estoc_previst-".$mQuantitat['actual']."),estoc_previst=".$mQuantitat['actual']."  where id='".$index."'",$db))
							{
								//echo "<br> 756 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
								return false;
							}
							
						}
						reset($mComandaCombinada);
					}
					
					$indexProductesCacChain.=',';
					
					
					// marcar actiu=1 als prods que la CAC demana i actiu=0 els altres
					if(!$result=mysql_query("update productes_".$mPars['selRutaSufix']." set actiu=0 where (preu<=0) OR locate(concat(',',id,','),' ".$indexProductesCacChain."')=0",$db))
					{
						//echo "<br> 768 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
						return false;
					}
					

					if(!$result=mysql_query("update productes_".$mPars['selRutaSufix']." set actiu=1 where (estoc_previst>0 OR locate('especial',CONCAT(' ',tipus))>0)",$db))
					{
						//echo "<br> 774 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
						return false;
					}
					else
					{
						return true;
					}

	}
*/
	else if ($mPars['selUsuariId']=='0')
	{
	  // guardar forma de pagament i punt d'entrega del Grup
	
		//0. si no hi ha comanda registrada, crearla
		//1. guardar forma de pagament: // no es modifica el punt d'entrega
		$totalUms=$mGuardarFormaPagament['fPagamentEcos']+$mGuardarFormaPagament['fPagamentEb']+$mGuardarFormaPagament['fPagamentEu'];
		if($totalUms==0){$totalUms=1;}
		$ppEcos=number_format($mGuardarFormaPagament['fPagamentEcos']*100/$totalUms,2);
		$ppEb=number_format($mGuardarFormaPagament['fPagamentEb']*100/$totalUms,2);
		$ppEuros=number_format($mGuardarFormaPagament['fPagamentEu']*100/$totalUms,2);
		

		$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']."  where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='0'",$db);
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		if(!$mRow)
		{
			//echo "<br> 730 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//1. guardar forma de pagament:
						//---------------------------
						//registre historial comanda
						$registre='';
						$mGC['usuari_id']=$mPars['usuari_id'];
						$mGC['f_pagament']=$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.",".$mGuardarFormaPagament['fPagamentOpcioConversor'];
						$mGC['resum']=$guardarComanda;
						$mGC['punt_entrega']=$mGuardarFormaPagament['puntEntregaRef'];
						$mGC['compte_ecos']=strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']);
						$mGC['compte_cieb']=strtoupper($mGuardarFormaPagament['fPagamentCompteEb']);
	
						while(list($key,$val)=each($mGC))
						{
							$registre.='{p:guardarComanda;us:'.$mGC['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
						}
						reset($mGC);
						//---------------------------
			//echo "<br>insert into comandes_".$mPars['selRutaSufix']." values('','".(date('YmdHis'))."','".$mParametres['periodeComanda']['valor']."','".$mPars['grup_id'].'-'.$mRebost['nom']."','0','".(date('dmY'))."','','','','','','".$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.",".$mGuardarFormaPagament['fPagamentOpcioConversor']."','".$mGuardarFormaPagament['puntEntregaRef']."','".$registre."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."','".$mPars['selLlistaId']."')";
			if(!$result=mysql_query("insert into comandes_".$mPars['selRutaSufix']." values('','".(date('YmdHis'))."','".$mParametres['periodeComanda']['valor']."','".$mPars['grup_id'].'-'.$mRebost['nom']."','0','".(date('dmY'))."','','','','','','".$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.",".$mGuardarFormaPagament['fPagamentOpcioConversor']."','".$mGuardarFormaPagament['puntEntregaRef']."','".$registre."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."','".$mPars['selLlistaId']."','')",$db))
			{
				//echo "<br> 795 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
				return false;
			}
		}
		else
		{
			//1. guardar forma de pagament: // no es modifica el punt d'entrega
						//---------------------------
						//registre historial comanda
						$registre='';
						$mGC['usuari_id']=$mPars['usuari_id'];
						$GC['f_pagament']=$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.",".$mGuardarFormaPagament['fPagamentOpcioConversor'];
						$mGC['resum']=$guardarComanda;
						$mGC['punt_entrega']=$mGuardarFormaPagament['puntEntregaRef'];
						$mGC['compte_ecos']=strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']);
						$mGC['compte_cieb']=strtoupper($mGuardarFormaPagament['fPagamentCompteEb']);
						
	
						while(list($key,$val)=each($mGC))
						{
							$registre.='{p:guardarComanda;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
						}
						reset($mGC);
						//---------------------------
			if(!$result=mysql_query("update comandes_".$mPars['selRutaSufix']." set historial=CONCAT('".$registre."',historial),f_pagament='".$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.",".$mGuardarFormaPagament['fPagamentOpcioConversor']."',punt_entrega='".$mGuardarFormaPagament['puntEntregaRef']."',compte_ecos='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."',compte_cieb='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."' where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='0'",$db))
			{
				//echo "<br> 729 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
				return false;
			}
		}

				//7. guardar utilitzatEcosR
				if($mComptesGrup['saldo_ecos_r']>0)
				{
					db_guardarComptesGrup($db);
				}

				//7. guardar parametre despeses grup
				$mPropietatsGrup['despesesGrup']=$mGuardarFormaPagament['fPagamentDgGrup'];
//*v36 27-1-16 1 linies php
				$mPropietatsGrup['msDespesesGrup']=$mGuardarFormaPagament['fPagamentMsDgGrup'];
				$propietatsGrup=makePropietatsGrup($mPropietatsGrup);
				db_guardarPropietatsGrup($propietatsGrup,$db);

	}
	return true;
}

//---------------------------------------------------------------
function db_guardarComandaLocal($guardarComanda,$db)
{
	global 	$mParametres,
			$mRebost,
			$mProductes,
			$mPars,
			$mGuardarFormaPagament,
			$mPropietatsGrup,
			$mPropietatsPeriodeLocal,
			$mPropietatsSelUsuari;
	
	
	//comprovar que l'usuari pot fer comanda en aquest grup.

	$mComandaAnterior=db_getComandaLocal($mPars['grup_id'],$db); //arriben tots els productes demanats amb quantitat>0
	$mComandaAnteriorCopiar=db_getComandaCopiar($mPars['grup_id'],$db);
	if(!isset($mComandaAnteriorCopiar['compte_ecos'])){$mComandaAnteriorCopiar['compte_ecos']='';}
	if(!isset($mComandaAnteriorCopiar['compte_cieb'])){$mComandaAnteriorCopiar['compte_cieb']='';}

	$mComandaCombinada=array();

	//$guardarcomanda: nom�s arriben els productes de la seleccio visualitzada, siguin en quantitat=0 o  quantitat>0

	//combinar la comanda anterior amb la comanda enviada ($guardarcomanda)
	$mProductesComanda=explode(';',$guardarComanda);
	$n=0;
	for($i=0;$i<count($mComandaAnterior);$i++)
	{
		$mComandaCombinada[$mComandaAnterior[$i]['index']]['anterior']=$mComandaAnterior[$i]['quantitat'];
		$mComandaCombinada[$mComandaAnterior[$i]['index']]['actual']=$mComandaAnterior[$i]['quantitat'];
	}

	for($i=0;$i<count($mProductesComanda);$i++)
	{
		$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
		
		$index=str_replace('producte_','',$mIndexQuantitat[0]);
		$quantitat=@$mIndexQuantitat[1];
		if($index!='')
		{
			//si existia a la comanda anterior guardar-hi la nova quantitat >/=0
			if(array_key_exists($index,$mComandaCombinada))
			{
				$mComandaCombinada[$index]['actual']=$quantitat;
			}
			else	//si no, i es >0, afegir
			{
				if($quantitat>0)
				{
					$mComandaCombinada[$index]['anterior']=0;
					$mComandaCombinada[$index]['actual']=$quantitat;
				}
			}
		}
	}
	//refer cadena comanda i passar comanda combinada a comanda
	$chain='';
	$n=0;
	while(list($index,$mQuantitat)=each($mComandaCombinada))
	{
		if($mQuantitat['actual']>0)
		{
			$chain.='producte_'.$index.':'.$mQuantitat['actual'].';';
		}
	}
	reset($mComandaCombinada);
		
	$guardarComanda=$chain;
	//veure si el rebost ja ha fet una comanda per aquest periode
	
	//si hi �s:
		
	$mComandesEspecialsRebost=array();
		
	if($mPars['grup_id']!='0' && $mPars['selUsuariId']!='0')
	{
		
		// 1. guardar a bd-seg la comanda anterior
		//echo "<br>insert into ".$mPars['taulaComandesSeg']." values('','".$mComandaAnteriorCopiar['num_comanda']."','".$mComandaAnteriorCopiar['periode_comanda']."','".$mComandaAnteriorCopiar['rebost']."','".$mComandaAnteriorCopiar['usuari_id']."','".$mComandaAnteriorCopiar['data']."','".$mComandaAnteriorCopiar['resum']."','','','','','".$mComandaAnteriorCopiar['f_pagament']."','".$mComandaAnteriorCopiar['punt_entrega']."','".$mComandaAnteriorCopiar['historial']."','".$mComandaAnteriorCopiar['compte_ecos']."','".$mComandaAnteriorCopiar['compte_cieb']."','".$mPars['selLlistaId']."','".$mComandaAnteriorCopiar['propietats']."')";
		if(!$result=@mysql_query("insert into ".$mPars['taulaComandesSeg']." values('','".$mComandaAnteriorCopiar['num_comanda']."','".$mComandaAnteriorCopiar['periode_comanda']."','".$mComandaAnteriorCopiar['rebost']."','".$mComandaAnteriorCopiar['usuari_id']."','".$mComandaAnteriorCopiar['data']."','".$mComandaAnteriorCopiar['resum']."','','','','','".$mComandaAnteriorCopiar['f_pagament']."','".$mComandaAnteriorCopiar['punt_entrega']."','".$mComandaAnteriorCopiar['historial']."','".$mComandaAnteriorCopiar['compte_ecos']."','".$mComandaAnteriorCopiar['compte_cieb']."','".$mPars['selLlistaId']."','".$mComandaAnteriorCopiar['propietats']."')",$db))
		{
			//echo "<br> 1665 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
    		return false;
		}

		// 2. eliminar comanda anterior:
		if(!$result=mysql_query("delete from ".$mPars['taulaComandes']." where id='".$mPars['idComanda']."'",$db))
		{
			//echo "<br> 617 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			return false;
		}
			
		// 3. actualitzar estoc a estat anterior:
		if(count($mComandaCombinada)>0)
		{
			while(list($index,$mQuantitat)=each($mComandaCombinada))
			{
				if(!$result=mysql_query("update ".$mPars['taulaProductes']." set estoc_disponible=estoc_disponible+".$mQuantitat['anterior']." where id='".$index."' AND llista_id='".$mPars['selLlistaId']."'",$db))
				{
					//echo "<br> 628 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
					return false;
				}
			}
			reset($mComandaCombinada);				
		}
			
			
		// 4. guardar a bd la nova comanda
		//---------------------------
		//registre historial comanda
		$registre='';
		$mGC['usuari_id']=$mPars['selUsuariId'];
		$mGC['f_pagament']=@$mPars['f_pagament'];
		$mGC['resum']=$guardarComanda;
		$mGC['compte_ecos']=strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']);
		$mGC['compte_cieb']=strtoupper($mGuardarFormaPagament['fPagamentCompteEb']);
		//$mGC['punt_entrega']='';
		while(list($key,$val)=each($mGC))
		{
			$registre.='{p:guardarComanda;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
		}
		reset($mGC);
		
		//propietatsComanda
		$propietatsComanda=0;
		
		//---------------------------
		//echo "<br>insert into ".$mPars['taulaComandes']." values('','".(date('YmdHis'))."','".$mPars['sel_periode_comanda_local']."','".$mPars['grup_id'].'-'.$mRebost['nom']."','".$mPars['selUsuariId']."','".(date('dmY'))."','".$guardarComanda."','','','','','".@$mPars['f_pagament']."','','".$registre."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."','".$mPars['selLlistaId']."','".$propietatsComanda."')";
		if(!$result=mysql_query("insert into ".$mPars['taulaComandes']." values('','".(date('YmdHis'))."','".$mPars['sel_periode_comanda_local']."','".$mPars['grup_id'].'-'.$mRebost['nom']."','".$mPars['selUsuariId']."','".(date('dmY'))."','".$guardarComanda."','','','','','".@$mPars['f_pagament']."','','".$registre."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."','".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."','".$mPars['selLlistaId']."','".$propietatsComanda."')",$db))
		{
			//echo "<br> 1714 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');

			// recuperar copia seg comanda:
			$mComandaSeg=get_comandaSeg($mPars['grup_id'].'-'.$mRebost['nom'],$db);
			$result=@mysql_query("insert into ".$mPars['taulaComandes']." values('','".$mComandaSeg['num_comanda']."','".$mComandaSeg['sel_periode_comanda']."','".$mComandaSeg['rebost']."','".$mComandaSeg['usuari_id']."','".$mComandaSeg['data']."','".$mComandaSeg['resum']."','".$mComandaAnteriorCopiar['ctiar']."','".$mComandaSeg['ms_ctiar']."','".$mComandaSeg['ctear']."','".$mComandaSeg['ms_ctear']."', '".$mComandaSeg['f_pagament']."','','".$mComandaSeg['historial']."','".$mComandaSeg['compte_ecos']."','".$mComandaSeg['compte_cieb']."','".$mComandaSeg['llista_id']."','".$mComandaSeg['propietats']."')",$db);
			//echo "<br> 1720 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
				
			return false;
		}
			
			
		// 5. actualitzar estoc a nou estat:
		if(count($mComandaCombinada)>0)
		{
			while(list($index,$mQuantitat)=each($mComandaCombinada))
			{
				if(!$result=mysql_query("update ".$mPars['taulaProductes']." set estoc_disponible=estoc_disponible-".$mQuantitat['actual']." where id='".$index."' AND llista_id='".$mPars['selLlistaId']."'",$db))
				{
					//echo "<br> 657 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
					return false;
				}
			}
			reset($mComandaCombinada);				
		}
			
		//6. guardar forma de pagament: // no es modifica el punt d'entrega
		$totalUms=$mGuardarFormaPagament['fPagamentEcos']+$mGuardarFormaPagament['fPagamentEb']+$mGuardarFormaPagament['fPagamentEu'];
		if($totalUms==0){$totalUms=1;}
		$ppEcos=number_format($mGuardarFormaPagament['fPagamentEcos']*100/$totalUms,2);
		$ppEb=number_format($mGuardarFormaPagament['fPagamentEb']*100/$totalUms,2);
		$ppEuros=number_format($mGuardarFormaPagament['fPagamentEu']*100/$totalUms,2);
				
		//---------------------------
		//registre historial comanda
		$registre='';
		$mGC['usuari_id']=$mPars['usuari_id'];
		$mGC['f_pagament']=$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.".".$mGuardarFormaPagament['fPagamentOpcioConversor'].".";
		$mGC['compte_ecos']=strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']);
		$mGC['compte_cieb']=strtoupper($mGuardarFormaPagament['fPagamentCompteEb']);
		//$mGC['resum']=$mComandaSeg['resum'];
		//$mGC['punt_entrega']='';
	
		while(list($key,$val)=each($mGC))
		{
			$registre='{p:guardarComanda;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}'.$registre;
		}
		reset($mGC);

		//---------------------------
		//echo "<br>update ".$mPars['taulaComandes']." set compte_ecos='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."',compte_cieb='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."',historial=CONCAT('".$registre."',historial),f_pagament='".$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.",".$mGuardarFormaPagament['fPagamentOpcioConversor']."' where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."'";
		if(!$result=mysql_query("update ".$mPars['taulaComandes']." set compte_ecos='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEcos']))."',compte_cieb='".(strtoupper($mGuardarFormaPagament['fPagamentCompteEb']))."',historial=CONCAT('".$registre."',historial),f_pagament='".$mGuardarFormaPagament['fPagamentEcos'].",".$mGuardarFormaPagament['fPagamentEb'].",".$mGuardarFormaPagament['fPagamentEu'].",".$mGuardarFormaPagament['fPagamentEuMetalic'].",".$mGuardarFormaPagament['fPagamentEuTransf'].",".$mGuardarFormaPagament['fPagamentEcosIntegralCES'].",".$ppEcos.",".$ppEb.",".$ppEuros.",".$mGuardarFormaPagament['fPagamentOpcioConversor']."' where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."'",$db))
		{
			//echo "<br> 667 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			return false;
		}
	}

	
	return true;
}

//------------------------------------------------------------------------------
function db_getComptesUsuarisRef($db)
{
	global $mPars;
	
	$mComptesUsuarisRef=array();
	
	//echo "<br>select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where sel_usuari_id='".$mPars['sel_usuari_id']."' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'";
	if(!$result=mysql_query("select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where grup_id='0' AND sel_usuari_id!='0' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db))
	{
		//echo "<br> 1349 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		//echo "<br> 134 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mComptesUsuarisRef[$mRow['sel_usuari_id']]=$mRow;
		}
		
	}

	return $mComptesUsuarisRef;
}

//------------------------------------------------------------------------------
function db_getComptesUsuarisGrupRef($db)
{
	global $mPars,$idUsuarisGrupChain;
	
	$mComptesUsuarisGrupRef=array();
	
	//echo "<br>select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where sel_usuari_id!='0' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'";
	if(!$result=mysql_query("select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where LOCATE(CONCAT(',',sel_usuari_id,','),' ".$idUsuarisGrupChain."')>0 AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db))
	{
		//echo "<br> 1349 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		//echo "<br> 134 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mComptesUsuarisGrupRef[$mRow['sel_usuari_id']]=$mRow;
		}
	}

	return $mComptesUsuarisGrupRef;
}

//------------------------------------------------------------------------------
function db_getComptesUsuari($db)
{
	global $mPars;
	
	$mComptesUsuari=array();
	$mComptesUsuari['id']=0;
	$mComptesUsuari['saldo_ecos_r']=0;
	$mComptesUsuari['utilitzat_ecos_r']=0;
	$mComptesUsuari['disponible_ecos_r']=0;
	$mComptesUsuari['utilitzat_ecos_r_chain']='';
	
	//echo "<br>select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where sel_usuari_id='".$mPars['selUsuariId']."' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'";
	if(!$result=mysql_query("select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where sel_usuari_id='".$mPars['selUsuariId']."' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db))
	{
		//echo "<br> 1349 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		//echo "<br> 134 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		if($mComptesUsuari_=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mComptesUsuari=$mComptesUsuari_;
		}
		
	}

	return $mComptesUsuari;
}

//------------------------------------------------------------------------------
function db_getComptesGrupsRef($db)
{
	global $mPars;
	
	$mComptesGrupsRef=array();
	
	//echo "<br>select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where sel_usuari_id='".$mPars['sel_usuari_id']."' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'";
	if(!$result=mysql_query("select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where  grup_id!='0' AND sel_usuari_id='0' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db))
	{
		//echo "<br> 1349 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		//echo "<br> 134 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mComptesGrupsRef[$mRow['grup_id']]=$mRow;
		}
	}
	return $mComptesGrupsRef;
}

//------------------------------------------------------------------------------
function db_getPerfilsGrupIdChain($db)
{
	global $mPars;
	
	$perfilsGrupIdChain='';

	if($result=mysql_query("select * from ".$mPars['taulaProductors']." where  grup_vinculat!='' AND grup_vinculat!='0' AND grup_vinculat!='NULL'",$db))
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$perfilsGrupIdChain.=','.$mRow['grup_vinculat'].',';
		}
		$perfilsGrupIdChain=substr($perfilsGrupIdChain,0,strlen($perfilsGrupIdChain)-1);
	}
	else
	{
		echo "<br> 1461 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}

	return $perfilsGrupIdChain;
}

//------------------------------------------------------------------------------
function db_getComptesGrupsProductorsRef($db)
{
	global $mPars,$perfilsGrupIdChain;
	
	$mComptesGrupsRef=array();
	$perfilsIdChain='';
	//echo "<br>select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where  LOCATE(CONCAT(',',grup_id,',',' ".$perfilsGrupIdChain."')>0 AND sel_usuari_id='0' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'";
	if(!$result=mysql_query("select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where  LOCATE(CONCAT(',',grup_id,','),' ".$perfilsGrupIdChain."')>0 AND sel_usuari_id='0' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db))
	{
		//echo "<br> 1349 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		//echo "<br> 134 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mComptesGrupsRef[$mRow['grup_id']]=$mRow;
		}
	}
	return $mComptesGrupsRef;
}
//------------------------------------------------------------------------------
function db_getComptesGrupsNoProductorsRef($db)
{
	global $mPars,$perfilsGrupIdChain;
	
	$mComptesGrupsRef=array();
	$perfilsIdChain='';
	//echo "<br>"."select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where  LOCATE(CONCAT(',',grup_id,',',' ".$perfilsGrupIdChain."')=0 AND sel_usuari_id='0' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'";
	if(!$result=mysql_query("select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where  LOCATE(CONCAT(',',grup_id,','),' ".$perfilsGrupIdChain."')=0 AND sel_usuari_id='0' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db))
	{
		//echo "<br> 1349 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		//echo "<br> 134 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mComptesGrupsRef[$mRow['grup_id']]=$mRow;
		}
	}
	return $mComptesGrupsRef;
}
//------------------------------------------------------------------------------
function db_getComptesGrup($db)
{
	global $mPars;
	
	$mComptesGrup=array();
	$mComptesGrup['id']=0;
	$mComptesGrup['utilitzat_ecos_r']=0;
	$mComptesGrup['disponible_ecos_r']=0;
	$mComptesGrup['saldo_ecos_r']=0;
	$mComptesGrup['utilitzat_ecos_r_chain']='';
	
	if(!$result=@mysql_query("select * from comptes_".(substr($mPars['selRutaSufix'],0,2))." where grup_id='".$mPars['grup_id']."' AND sel_usuari_id='0' AND ruta='".$mPars['selRutaSufix']."' AND tipus='config'",$db))
	{
		//echo "<br> 1368 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		if($mComptesGrup_=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mComptesGrup=$mComptesGrup_;
		}
	}

	return $mComptesGrup;
}

//------------------------------------------------------------------------------
function db_guardarComptesUsuari($db)
{
	global $mPars,$mComptesUsuari,$mPropietatsSelUsuari;
	$values='';
	
	if(!isset($mComptesUsuari['id']) || $mComptesUsuari['id']==0 || $mComptesUsuari['id']=='')
	{
		if(isset($mPropietatsSelUsuari['comissio']) && $mPropietatsSelUsuari['comissio']!='' && $mComptesUsuari['saldo_ecos_r']>0)
		{
			//echo "<br>1482 insert into comptes_".substr($mPars['selRutaSufix'],0,2)." values('','".(date('Y-m-d h:i:s'))."','".$mPars['selRutaSufix']."','".$mPars['selUsuariId']."','".$mPars['grup_id']."','','config','".$mComptesUsuari_['saldo_ecos_r']."','".($mComptesUsuari_['saldo_ecos_r']-$mComptesUsuari_['utilitzat_ecos_r'])."','".$mComptesUsuari_['utilitzat_ecos_r']."','".$mComptesUsuari_['utilitzat_ecos_r_chain']."','0','0','0','','','','','0','0')";
			if(!$result=mysql_query("insert into comptes_".substr($mPars['selRutaSufix'],0,2)." values('','".(date('Y-m-d h:i:s'))."','".$mPars['selRutaSufix']."','".$mPars['usuari_id']."','".$mPars['grup_id']."','".$mPars['selUsuariId']."','config','".$mComptesUsuari['saldo_ecos_r']."','".($mComptesUsuari['saldo_ecos_r']-$mComptesUsuari['utilitzat_ecos_r'])."','".$mComptesUsuari['utilitzat_ecos_r']."','".$mComptesUsuari['utilitzat_ecos_r_chain']."','0','0','0','','','','','0','0')",$db))
			{
				//echo "<br> 1478 guardarComptesUsuari.php ".mysql_errno() . ": " . mysql_error(). "\n";
				err__(' 1478 guardarComptesUsuari.php  ',mysql_errno().'--'.mysql_error(),'2','db.php');
			}
		}
		else
		{
			//echo "<br>1485 insert into comptes_".substr($mPars['selRutaSufix'],0,2)." values('','".(date('Y-m-d h:i:s'))."','".$mPars['selRutaSufix']."','".$mPars['usuari_id']."','".$mPars['grup_id']."','".$mPars['selUsuariId']."','config','0','0','".$mComptesUsuari['utilitzat_ecos_r']."','".$mComptesUsuari['utilitzat_ecos_r_chain']."','0','0','0','','','','','0','0')";
			if(!$result=mysql_query("insert into comptes_".substr($mPars['selRutaSufix'],0,2)." values('','".(date('Y-m-d h:i:s'))."','".$mPars['selRutaSufix']."','".$mPars['selUsuariId']."','".$mPars['grup_id']."','".$mPars['selUsuariId']."','config','0','0','".$mComptesUsuari['utilitzat_ecos_r']."','".$mComptesUsuari['utilitzat_ecos_r_chain']."','0','0','0','','','','','0','0')",$db))
			{
				//echo "<br> 1486 guardarComptesUsuari.php ".mysql_errno() . ": " . mysql_error(). "\n";
				err__(' 1486 guardarComptesUsuari.php  ',mysql_errno().'--'.mysql_error(),'2','db.php');
			}
		}
	
	}
	else
	{
		while (list($key,$val)=each($mComptesUsuari))
		{
			if($key=='disponible_ecos_r' || $key=='utilitzat_ecos_r' || $key=='utilitzat_ecos_r_chain')
			{
				$values.=$key."='".$val."', ";
			}
		}
		reset($mComptesUsuari);
		$values=substr($values,0,strlen($values)-2);
		//echo "<br>1507 update comptes_".substr($mPars['selRutaSufix'],0,2)." set ".$values." where id='".$mComptesUsuari['id']."'";
		if(!$result=@mysql_query("update comptes_".substr($mPars['selRutaSufix'],0,2)." set ".$values." where id='".$mComptesUsuari['id']."'",$db))
		{
			//echo "<br> 1353 db.php".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		}
	}

	return;
}

//------------------------------------------------------------------------------
function db_guardarComptesGrup($db)
{
	global $mPars,$mComptesGrup;
	$values='';
	
	if(!isset($mComptesGrup['id']) || $mComptesGrup['id']==0 || $mComptesGrup['id']=='')
	{
		if($mComptesGrup['saldo_ecos_r']>0)
		{
			if(!$result=mysql_query("insert into comptes_".substr($mPars['selRutaSufix'],0,2)." values('','".(date('Y-m-d h:i:s'))."','".$mPars['selRutaSufix']."','".$mPars['usuari_id']."','".$mPars['grup_id']."','".$mPars['selUsuariId']."','config','".$mComptesUsuari['saldo_ecos_r']."','".($mComptesGrup['saldo_ecos_r']-$mComptesGrup['utilitzat_ecos_r'])."','".$mCompteGrup['utilitzat_ecos_r']."','','0','0','0','','','','','0','0')",$db))
			{
				//echo "<br> 1478 guardarComptesGrup.php ".mysql_errno() . ": " . mysql_error(). "\n";
				err__(' 1626 guardarComptesGrup.php  ',mysql_errno().'--'.mysql_error(),'2','db.php');
			}
		}
		else
		{
			if(!$result=mysql_query("insert into comptes_".substr($mPars['selRutaSufix'],0,2)." values('','".(date('Y-m-d h:i:s'))."','".$mPars['selRutaSufix']."','".$mPars['usuari_id']."','".$mPars['grup_id']."','".$mPars['selUsuariId']."','config','0','0','".$mComptesGrup['utilitzat_ecos_r']."',','0','0','0','','','','','0','0')",$db))
			{
				//echo "<br> 1486 guardarComptesUsuari.php ".mysql_errno() . ": " . mysql_error(). "\n";
				err__(' 1635 guardarComptesGrup.php  ',mysql_errno().'--'.mysql_error(),'2','db.php');
			}
		}
	
	}
	else
	{
		while (list($key,$val)=each($mComptesGrup))
		{
			if($key=='disponible_ecos_r' || $key=='utilitzat_ecos_r')
			{
				$values.=$key."='".$val."', ";
			}
		}
		reset($mComptesGrup);
		$values=substr($values,0,strlen($values)-2);

		if(!$result=@mysql_query("update comptes_".substr($mPars['selRutaSufix'],0,2)." set ".$values." where id='".$mComptesGrup['id']."'",$db))
		{
			//echo "<br> 1654 db_comptesGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		}
	}

	return;
}
//------------------------------------------------------------------------
function db_getRebostsAmbComanda($db)
{
	global $mPars;
	
	$mRebostsAmbComanda=array();

	if(!$result=@mysql_query("select * from ".$mPars['taulaComandes']." WHERE llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['periode_comanda']."' order by rebost DESC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mRow['usuari_id']!='0')
			{
				array_push($mRebostsAmbComanda,substr($mRow['rebost'],0,strpos($mRow['rebost'],'-')));
			}
		}
	}

	return $mRebostsAmbComanda;
}

//------------------------------------------------------------------------
function db_getComandaLocal($grupRef,$db)
{
	global $mPars;
	
	$mComanda_=array();
	$mComanda=array();
	
	
	if(isset($mPars['selUsuariId']) && @$mPars['selUsuariId']!='0')
	{
		$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id='".$mPars['selUsuariId']."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' ORDER BY id DESC limit 1",$db);
		//echo "<br> 2117 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		if($result)
		{ 
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			
			$mPars['idComanda']=$mRow['id'];
			$mProductesComanda=explode(';',$mRow['resum']);
			$n=0;
			for($i=0;$i<count($mProductesComanda);$i++)
			{
				$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
				$index=str_replace('producte_','',$mIndexQuantitat[0]);
				$quantitat=@$mIndexQuantitat[1];
				if($index!='' && $index!=0)
				{
					$mComanda[$n]['index']=$index;
					$mComanda[$n]['quantitat']=$quantitat;
					$n++;
				}
			}
		}
	}
	else 
	{
		//echo "<br>select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."'  ORDER BY ID DESC limit 1";
		$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."'  ORDER BY ID DESC",$db);
		//echo "<br> 2258 db.php ".mysql_errno() . ": " . mysql_error(). "\n";

		if($result)
		{ 
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mPars['idComanda']=$mRow['id'];
				$mProductesComanda=explode(';',$mRow['resum']);
				for($i=0;$i<count($mProductesComanda);$i++)
				{
					$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
					$index=str_replace('producte_','',$mIndexQuantitat[0]);
					$quantitat=@$mIndexQuantitat[1];
					if($index!='' && $index!=0)
					{
						if(!isset($mComanda_[$index])){$mComanda_[$index]=0;}
						$mComanda_[$index]+=$quantitat;
					}
				}
			}
			//Convertir format:
			$n=0;
			while(list($index,$quantitat)=each($mComanda_))
			{
				$mComanda[$n]['index']=$index;
				$mComanda[$n]['quantitat']=$quantitat;
				$n++;
			}
			reset($mComanda_);
													
		}
	}
	return $mComanda;
}

//------------------------------------------------------------------------
function db_getComanda($grupRef,$db)
{
	global $mPars;
	
	$mComanda_=array();
	$mComanda=array();
	
	
	if(isset($mPars['selUsuariId']) && @$mPars['selUsuariId']!='0')
	{
		$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id='".$mPars['selUsuariId']."' AND llista_id='".$mPars['selLlistaId']."'  ORDER BY id DESC limit 1",$db);
		//echo "<br> 2122 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		if($result)
		{ 
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			
			$mPars['idComanda']=$mRow['id'];
			$mProductesComanda=explode(';',$mRow['resum']);
			$n=0;
			for($i=0;$i<count($mProductesComanda);$i++)
			{
				$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
				$index=str_replace('producte_','',$mIndexQuantitat[0]);
				$quantitat=@$mIndexQuantitat[1];
				if($index!='' && $index!=0)
				{
					$mComanda[$n]['index']=$index;
					$mComanda[$n]['quantitat']=$quantitat;
					$n++;
				}
			}
		}
	}
	else 
	{
		if($grupRef!=0) //grup
		{
			$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."'",$db);
			//echo "<br> 826 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		}
		else	//cac
		{
			$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' ORDER BY ID DESC limit 1",$db);
			//echo "<br> 826 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		}
		if($result)
		{ 
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mPars['idComanda']=$mRow['id'];
				$mProductesComanda=explode(';',$mRow['resum']);
				for($i=0;$i<count($mProductesComanda);$i++)
				{
					$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
					$index=str_replace('producte_','',$mIndexQuantitat[0]);
					$quantitat=@$mIndexQuantitat[1];
					if($index!='' && $index!=0)
					{
						if(!isset($mComanda_[$index])){$mComanda_[$index]=0;}
						$mComanda_[$index]+=$quantitat;
					}
				}
			}
			//Convertir format:
			$n=0;
			while(list($index,$quantitat)=each($mComanda_))
			{
				$mComanda[$n]['index']=$index;
				$mComanda[$n]['quantitat']=$quantitat;
				$n++;
			}
			reset($mComanda_);
													
		}
	}
	return $mComanda;
}
//------------------------------------------------------------------------
function db_getFormaPagamentMembres($grupRef,$db)
{
//*v36-16-12-15 global
	global $mPars,$mGrup,$mRuta;

	$mFormaPagamentMembres=array();

	
	$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id='0'",$db);
	//echo "<br> 826 db.php ".mysql_errno() . ": " . mysql_error(). "\n";

	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

	if(!$mRow)
	{
		//si no hi s'ha guardat agafem valors ruta anterior
		$date = new DateTime();
		$t1=$date->getTimestamp();
		$date->setDate(date('Y',$t1), date('m',$t1)-1, date('d',$t1));
		$t2=$date->getTimestamp();
		$rutaSufixAnterior=date('ym',$t2);
		
		$result=mysql_query("select * from comandes_".$rutaSufixAnterior." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id='0'",$db);
		//echo "<br> 826 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		if($mRow['f_pagament']!='')
		{
			$mFormaPagament=explode(',',$mRow['f_pagament']);
			if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
			if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
			if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
			if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
			if(!isset($mFormaPagament[4])){$mFormaPagament[4]=0;}
			if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
			if(!isset($mFormaPagament[6]))
			{
				$mFormaPagament[6]=100;
				$mFormaPagament[7]=0;
				$mFormaPagament[8]=0;
			}
			$mFormaPagament[9]=0;
			$mFormaPagamentMembres['f_pagament']=$mFormaPagament[0].','.$mFormaPagament[1].','.$mFormaPagament[2].','.$mFormaPagament[3].','.$mFormaPagament[4].','.$mFormaPagament[5].','.$mFormaPagament[6].','.$mFormaPagament[7].','.$mFormaPagament[8].','.$mFormaPagament[9];
			$mFormaPagamentMembres['punt_entrega']=$mRow['punt_entrega'];
		}
		else // si tampoc est� guardada, aplicar valors per defecte
		{
			$mFormaPagamentMembres['f_pagament']='0,0,0,0,0,0,0,100,0,0,7';
			$mFormaPagamentMembres['punt_entrega']=$mGrup['id'];
		}
	}
	else
	{
		$mFormaPagamentMembres=$mRow;
	}
	
	
	//obtenir resum comandes individuals:

	$mFormaPagamentMembres['fPagamentEcos']=0;
	$mFormaPagamentMembres['fPagamentEb']=0;
	$mFormaPagamentMembres['fPagamentEu']=0;
	$mFormaPagamentMembres['fPagamentEuMetalic']=0;
	$mFormaPagamentMembres['fPagamentEuTransf']=0;
	$mFormaPagamentMembres['fPagamentEcosIntegralCES']=0;
	$mFormaPagamentMembres['fPagamentEcosPerc']=100;
	$mFormaPagamentMembres['fPagamentEbPerc']=0;
	$mFormaPagamentMembres['fPagamentEuPerc']=0;
	$mFormaPagamentMembres['fPagamentUms']=0;
	$mFormaPagamentMembres['opcioConversor']=0;
	
	$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id!='0'",$db);
	//echo "<br> 938 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	
	if($result)
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mRow['f_pagament']!='')
			{
				$mFormaPagament=explode(',',$mRow['f_pagament']);
				if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
				if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
				if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
				if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
				if(!isset($mFormaPagament[4])){$mFormaPagament[4]=0;}
				if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
				if(!isset($mFormaPagament[6]))
				{
					$mFormaPagament[6]=100;
					$mFormaPagament[7]=0;
					$mFormaPagament[8]=0;
				}
				$mFormaPagament[9]=7;

				$mFormaPagamentMembres['fPagamentEcos']+=$mFormaPagament[0];
				$mFormaPagamentMembres['fPagamentEb']+=$mFormaPagament[1];
				$mFormaPagamentMembres['fPagamentEu']+=$mFormaPagament[2];
			}
		}

		$mFormaPagamentMembres['fPagamentUms']=$mFormaPagamentMembres['fPagamentEcos']+$mFormaPagamentMembres['fPagamentEb']+$mFormaPagamentMembres['fPagamentEu'];

		@$mFormaPagamentMembres['fPagamentEcosPerc']=$mFormaPagamentMembres['fPagamentEcos']*100/$mFormaPagamentMembres['fPagamentUms'];
		@$mFormaPagamentMembres['fPagamentEbPerc']=$mFormaPagamentMembres['fPagamentEb']*100/$mFormaPagamentMembres['fPagamentUms'];
		@$mFormaPagamentMembres['fPagamentEuPerc']=$mFormaPagamentMembres['fPagamentEu']*100/$mFormaPagamentMembres['fPagamentUms'];

		$mFormaPagamentMembres['fPagamentEuMetalic']=0;			//no util, aqui s'utilitza la mitjana del grup
		$mFormaPagamentMembres['fPagamentEuTransf']=0;			//no util
		$mFormaPagamentMembres['fPagamentEcosIntegralCES']=0;	//no util
	}
	return $mFormaPagamentMembres;
}

//------------------------------------------------------------------------------
function db_getFormaPagamentMembresLocal($grupRef,$db)
{
//*v36-16-12-15 global
	global $mPars,$mGrup,$mRuta;

	$mFormaPagamentMembres=array();

	
	//obtenir resum comandes individuals:

	$mFormaPagamentMembres['fPagamentEcos']=0;
	$mFormaPagamentMembres['fPagamentEb']=0;
	$mFormaPagamentMembres['fPagamentEu']=0;
	$mFormaPagamentMembres['fPagamentEuMetalic']=0;
	$mFormaPagamentMembres['fPagamentEuTransf']=0;
	$mFormaPagamentMembres['fPagamentEcosIntegralCES']=0;
	$mFormaPagamentMembres['fPagamentEcosPerc']=100;
	$mFormaPagamentMembres['fPagamentEbPerc']=0;
	$mFormaPagamentMembres['fPagamentEuPerc']=0;
	$mFormaPagamentMembres['fPagamentUms']=0;
	$mFormaPagamentMembres['opcioConversor']=0;
	
	//echo "<br>select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id!='0' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' AND llista_id='".$mPars['selLlistaId']."'";
	$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id!='0' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' AND llista_id='".$mPars['selLlistaId']."'",$db);
	//echo "<br> 938 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	
	if($result)
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mRow['f_pagament']!='')
			{
				$mFormaPagament=explode(',',$mRow['f_pagament']);
				if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
				if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
				if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
				if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
				if(!isset($mFormaPagament[4])){$mFormaPagament[4]=0;}
				if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
				if(!isset($mFormaPagament[6]))
				{
					$mFormaPagament[6]=100;
					$mFormaPagament[7]=0;
					$mFormaPagament[8]=0;
				}
				$mFormaPagament[9]=7;

				$mFormaPagamentMembres['fPagamentEcos']+=$mFormaPagament[0];
				$mFormaPagamentMembres['fPagamentEb']+=$mFormaPagament[1];
				$mFormaPagamentMembres['fPagamentEu']+=$mFormaPagament[2];
			}
		}

		$mFormaPagamentMembres['fPagamentUms']=$mFormaPagamentMembres['fPagamentEcos']+$mFormaPagamentMembres['fPagamentEb']+$mFormaPagamentMembres['fPagamentEu'];
		@$mFormaPagamentMembres['fPagamentEcosPerc']=$mFormaPagamentMembres['fPagamentEcos']*100/$mFormaPagamentMembres['fPagamentUms'];
		@$mFormaPagamentMembres['fPagamentEbPerc']=$mFormaPagamentMembres['fPagamentEb']*100/$mFormaPagamentMembres['fPagamentUms'];
		@$mFormaPagamentMembres['fPagamentEuPerc']=$mFormaPagamentMembres['fPagamentEu']*100/$mFormaPagamentMembres['fPagamentUms'];

		$mFormaPagamentMembres['fPagamentEuMetalic']=0;			//no util, aqui s'utilitza la mitjana del grup
		$mFormaPagamentMembres['fPagamentEuTransf']=0;			//no util
		$mFormaPagamentMembres['fPagamentEcosIntegralCES']=0;	//no util
	}
	
	return $mFormaPagamentMembres;
}


//------------------------------------------------------------------------------
function db_getFormaPagament($db)
{
	global $mPars;
	
		if($mPars['selUsuariId']!='0')
		{
			$result=mysql_query("select f_pagament from comandes_".$mPars['selRutaSufix']." where  SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."'  ORDER BY id DESC limit 1",$db);
			//echo "<br> 890 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		
			//grup --------------------------------------
			$result2=mysql_query("select f_pagament from comandes_".$mPars['selRutaSufix']." where  SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='0'  ORDER BY id DESC limit 1",$db);
			//echo "<br> 895 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		
			$mRow=mysql_fetch_array($result2,MYSQL_ASSOC);
			if($mRow['f_pagament'])
			{
				$mFormaPagament=explode(',',$mRow['f_pagament']);
				if(!isset($mFormaPagament[9])){$mFormaPagament[9]=7;}
				$mPars['fPagamentDgGrup']=$mFormaPagament[9];
			}
		}
		else
		{
			//echo "<br>select f_pagament from comandes_".$mPars['selRutaSufix']." where  SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='0'";
			$result=mysql_query("select f_pagament from comandes_".$mPars['selRutaSufix']." where  SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='0'",$db);
			//echo "<br> 895 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		}
	
		if($result)
		{ 
		
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			if($mRow)
			{
				$mFormaPagament=explode(',',$mRow['f_pagament']);
				if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
				if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
				if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
				if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
				if(!isset($mFormaPagament[4])){$mFormaPagament[4]=0;}
				if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
				if(!isset($mFormaPagament[6])){$mFormaPagament[6]=0;}
				if(!isset($mFormaPagament[7])){$mFormaPagament[7]=0;}
				if(!isset($mFormaPagament[8])){$mFormaPagament[8]=0;}
				if(!isset($mFormaPagament[9])){$mFormaPagament[9]=7;}
				$mPars['fPagamentEcos']=@$mFormaPagament[0];
				$mPars['fPagamentEb']=@$mFormaPagament[1];
				$mPars['fPagamentEu']=@$mFormaPagament[2];
				$mPars['fPagamentEuMetalic']=@$mFormaPagament[3];
				$mPars['fPagamentEuTransf']=@$mFormaPagament[4];
				$mPars['fPagamentEcosIntegralCES']=@$mFormaPagament[5];
				$mPars['fPagamentEcosPerc']=$mFormaPagament[6];
				$mPars['fPagamentEbPerc']=$mFormaPagament[7];
				$mPars['fPagamentEuPerc']=$mFormaPagament[8];
				$mPars['fPagamentOpcioConversor']=$mFormaPagament[9];
				$mPars['fPagamentUms']=$mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu'];
			}
			else
			{
				// si no hi ha f pagament guardat en aquesta ruta, agafar l'anterior
//*v36-16-12-15 query marca1_i
				$anyRutavella=substr($mPars['selRutaSufixPeriode'],0,2);
				$mesRutavella=substr($mPars['selRutaSufixPeriode'],2);
				$ruta0_=date('ym',mktime(0,0,0,$mesRutavella*1-1,date('d'),$anyRutavella));
//*v36-16-12-15 query marca1_f
			
				if(@$mPars['selUsuariId']!='0')
				{
					$result=mysql_query("select f_pagament from comandes_".$ruta0_." where  SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."'",$db);
					//echo "<br> 890 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
				}
				else
				{
					$result=mysql_query("select f_pagament from comandes_".$ruta0_." where  SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."'AND usuari_id='0'",$db);
					//echo "<br> 895 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
				}
				if($result)
				{ 
					$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
					if($mRow)
					{
						$mFormaPagament=explode(',',$mRow['f_pagament']);
						if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
						if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
						if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
						if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
						if(!isset($mFormaPagament[4])){$mFormaPagament[4]=0;}
						if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
						if(!isset($mFormaPagament[6])){$mFormaPagament[6]=0;}
						if(!isset($mFormaPagament[7])){$mFormaPagament[7]=0;}
						if(!isset($mFormaPagament[8])){$mFormaPagament[8]=0;}
						if(!isset($mFormaPagament[9])){$mFormaPagament[9]=7;}
						$mPars['fPagamentEcos']=0;
						$mPars['fPagamentEb']=0;
						$mPars['fPagamentEu']=0;
						$mPars['fPagamentEuMetalic']=@$mFormaPagament[3];
						$mPars['fPagamentEuTransf']=@$mFormaPagament[4];
						$mPars['fPagamentEcosIntegralCES']=@$mFormaPagament[5];
						$mPars['fPagamentEcosPerc']=$mFormaPagament[6];
						$mPars['fPagamentEbPerc']=$mFormaPagament[7];
						$mPars['fPagamentEuPerc']=$mFormaPagament[8];
						$mPars['fPagamentOpcioConversor']=$mFormaPagament[9];

						$mPars['fPagamentUms']=0;
					}
					else
					{
						// si no hi ha f pagament guardat en ruta anterior s'aplicaran fp per defecte a pagina comandes
					
						$mPars['fPagamentEcos']=0;
						$mPars['fPagamentEb']=0;
						$mPars['fPagamentEu']=0;
						$mPars['fPagamentEuMetalic']=0;
						$mPars['fPagamentEuTransf']=0;
						$mPars['fPagamentEcosIntegralCES']=0;
						$mPars['fPagamentEcosPerc']=0;
						$mPars['fPagamentEbPerc']=0;
						$mPars['fPagamentEuPerc']=0;
						$mPars['fPagamentOpcioConversor']=7;

						$mPars['fPagamentUms']=0;
					}
				}
			}
		}
	

	return;
}

//------------------------------------------------------------------------------
function db_getFormaPagamentLocal($db)
{
	global $mPars;

		$mPars['fPagamentEcos']=0;
		$mPars['fPagamentEb']=0;
		$mPars['fPagamentEu']=0;
		$mPars['fPagamentCompteEcos']='';
		$mPars['fPagamentCompteCieb']='';
		$mPars['fPagamentEuMetalic']=0;			
		$mPars['fPagamentEuTransf']=0;			
		$mPars['fPagamentEcosIntegralCES']=0;	
		$mPars['fPagamentOpcioConversor']=0;	
		$mPars['fPagamentUms']=0;
		$mPars['fPagamentEcosPerc']=0;
		$mPars['fPagamentEbPerc']=0;
		$mPars['fPagamentEuPerc']=0;

		if($mPars['selUsuariId']!='0')
		{
			$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."'  AND periode_comanda='".$mPars['sel_periode_comanda_local']."'  ORDER BY id DESC limit 1",$db);
			//echo "<br> 2508 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		}
		else
		{
			$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."'  AND periode_comanda='".$mPars['sel_periode_comanda_local']."'",$db);
			//echo "<br> 938 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		}

	
	if($result)
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mRow)
			{
				$mFormaPagament=explode(',',$mRow['f_pagament']);
				if(!isset($mFormaPagament[0])){$mFormaPagament[0]=0;}
				if(!isset($mFormaPagament[1])){$mFormaPagament[1]=0;}
				if(!isset($mFormaPagament[2])){$mFormaPagament[2]=0;}
				if(!isset($mFormaPagament[3])){$mFormaPagament[3]=0;}
				if(!isset($mFormaPagament[4])){$mFormaPagament[4]=0;}
				if(!isset($mFormaPagament[5])){$mFormaPagament[5]=0;}
				if(!isset($mFormaPagament[6]))
				{
					$mFormaPagament[6]=100;
					$mFormaPagament[7]=0;
					$mFormaPagament[8]=0;
				}
				if(!isset($mFormaPagament[9])){$mFormaPagament[9]=7;}

				$mPars['fPagamentEcos']=$mFormaPagament[0];
				$mPars['fPagamentEb']=$mFormaPagament[1];
				$mPars['fPagamentEu']=$mFormaPagament[2];
				$mPars['fPagamentCompteEcos']=$mRow['compte_ecos'];
				$mPars['fPagamentCompteCieb']=$mRow['compte_cieb'];
				$mPars['fPagamentEuMetalic']=$mFormaPagament[3];			
				$mPars['fPagamentEuTransf']=$mFormaPagament[4];			
				$mPars['fPagamentEcosIntegralCES']=$mFormaPagament[5];	
				$mPars['fPagamentOpcioConversor']=$mFormaPagament[9];	
			}
			else
			{
				$mPars['fPagamentCompteEcos']='';
				$mPars['fPagamentCompteCieb']='';
			}
				$mPars['fPagamentUms']=$mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu'];

				@$mPars['fPagamentEcosPerc']=$mPars['fPagamentEcos']*100/$mPars['fPagamentUms'];
				@$mPars['fPagamentEbPerc']=$mPars['fPagamentEb']*100/$mPars['fPagamentUms'];
				@$mPars['fPagamentEuPerc']=$mPars['fPagamentEu']*100/$mPars['fPagamentUms'];
		}
	}
		
	return;
}

//------------------------------------------------------------------------
function db_getComandaCopiar($grupRef,$db)
{
	global $mPars;
	$mComanda=array();
	$mRowPropietats=array();
	if($mPars['selUsuariId']!='0')
	{
		if($mPars['selLlistaId']==0)
		{
			//obtenir punt entrega del grup
			//echo "<br>select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' AND usuari_id=0 ORDER BY id DESC limit 1";
			$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' AND usuari_id=0 ORDER BY id DESC limit 1",$db);
			//echo "<br> 2577 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			$mRowGrup=@mysql_fetch_array($result,MYSQL_ASSOC);
			
			
			//echo "<br>select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id='".$mPars['selUsuariId']."' AND llista_id='".$mPars['selLlistaId']."'  ORDER BY id DESC limit 1";
			$result2=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id='".$mPars['selUsuariId']."' AND llista_id='".$mPars['selLlistaId']."'  ORDER BY id DESC limit 1",$db);
			//echo "<br> 2581 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		}
		else
		{
			$result=mysql_query("select propietats from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' AND usuari_id=0 AND id='".$mPars['sel_periode_comanda_local']."'  ORDER BY id DESC limit 1",$db);
			//echo "<br> 2586 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			$mRowGrup['punt_entrega']='';
			$mRow=@mysql_fetch_array($result,MYSQL_ASSOC);
			$mRow_=$mRow;

			//echo "<br>select * from ".$mPars['taulaComandes'] ;
			$result2=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id='".$mPars['selUsuariId']."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."'  ORDER BY id DESC limit 1",$db);
			//echo "<br> 2593 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		}

		if($result2)
		{
			$mRow=mysql_fetch_array($result2,MYSQL_ASSOC);
			//$mPars['idComanda']=@$mRow['id'];
			$mRow['punt_entrega']=$mRowGrup['punt_entrega'];
			$mRow['propietats']=@$mRow_['propietats'];
			return $mRow;
		}
	}
	else
	{ 
		if($grupRef!=0)
		{
			if($mPars['selLlistaId']==0)
			{
				$result=@mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND usuari_id='0' AND llista_id='".$mPars['selLlistaId']."'",$db);
				//echo "<br>  2602 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
				if($result)
				{
					$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
					$mPars['idComanda']=$mRow['id'];
			
					return $mRow;
				}
			}
			else
			{
				//echo "<br>select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' AND id='".$mPars['sel_periode_comanda_local']."'";
				$result=@mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' AND id='".$mPars['sel_periode_comanda_local']."'",$db);
				//echo "<br>  2614 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
				if($result)
				{
					$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
					$mPars['idComanda']=$mRow['id'];
					$mRow['propietatsPeriode']=$mRow['propietats'];
			
					return $mRow;
				}
			}
			
		}
		else
		{
			//echo "<br>select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' ORDER BY ID DESC LIMIT 1";
			$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$grupRef."' AND llista_id='".$mPars['selLlistaId']."' ORDER BY ID DESC LIMIT 1",$db);
			//echo "<br>  2770 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			if($result)
			{
				$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
				$mPars['idComanda']=$mRow['id'];
				$mRow['propietatsPeriode']=$mRow['propietats'];
			
				return $mRow;
			}
		}
	}
	
	return false;
}

//------------------------------------------------------------------------
function checkLogin($db)
{
	global $mPars,$mParams,$mDEMO_Params;

	$mPars['idPerfilsUsuariResponsableChain']='';
	$mPars['idGrupsUsuariResponsableChain']='';
	$mPars['DEMO_idGrupsUsuariResponsableChain']='';
	$mPars['DEMO_idPerfilsUsuariResponsableChain']='';
	
	$mBad=array('"',"'",'\\','/');
	if($pasw=str_replace($mBad,'',@$_POST['i_pasw'])) //probe de login
	{
		$usuari=str_replace($mBad,'',@$_POST['i_usuari']);
		if(!$result=mysql_query("select id,nivell,grups,usuari from usuaris where (LOWER(usuari)='".(strtolower(@$usuari))."' || LOWER(usuari)='".(urlencode(strtolower($usuari)))."') AND estat='actiu' AND md5(contrassenya)='".(MD5(@$pasw))."'",$db))
		{
			//echo "<br> 1031 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//$mPars=f_reset($mPars);
			unset($mPars);
			return false;
		}	
		else
		{
			if(!$mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				unset($mPars);
				return false;
			}
			else
			{		
				$mPars['usuari_id']=$mRow['id'];
				$mPars['pasw']=MD5($pasw);
				$mPars['nivell']=$mRow['nivell'];
				$mPars['grups_usuari']=$mRow['grups'];
				$mPars['usuari']=$mRow['usuari'];
				$mPars['DEMO_nivell']='visitant';
				if($mRow['nivell']!='visitant')
				{
					$mPars['idGrupsUsuariResponsableChain']=db_getIdGrupsUsuariResponsableChain($db);
					$mPars['idPerfilsUsuariResponsableChain']=db_getIdPerfilsUsuariResponsableChain($db);
					
					$DEMO_db=db_conect($mDEMO_Params);
					$mPars['DEMO_idGrupsUsuariResponsableChain']=db_getDEMO_IdGrupsUsuariResponsableChain($DEMO_db);
					$mPars['DEMO_idPerfilsUsuariResponsableChain']=db_getDEMO_IdPerfilsUsuariResponsableChain($DEMO_db);
						$mDEMO_usuari=db_getUsuari($mPars['usuari_id'],$DEMO_db);
					$mPars['DEMO_nivell']=$mDEMO_usuari['nivell'];
					closeDb($DEMO_db);
					$db=db_conect($mParams);
				}
				return true;
			}
		}
	}
	else 
	{
		if($usuari=str_replace($mBad,'',@$_GET['us'])) //prove de seleccio usuari
		{
			$pasw=str_replace($mBad,'',@$_GET['pasw']);
		}
		else
		{
			if(isset($mPars['usuari_id']) && isset($mPars['pasw']))
			{
				$usuari=@$mPars['usuari_id'];
				$pasw=@$mPars['pasw'];
			}
			else
			{
				unset($mPars);
				return false;
			}
		}

		if(!$result=mysql_query("select id,nivell,grups,usuari from usuaris where id='".$usuari."' AND estat='actiu'  AND md5(contrassenya)='".($pasw)."'",$db))
		{
			//echo "<br> 1077 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			unset($mPars);
			return false;
		}
		else
		{
			if(!$mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				unset($mPars);
				return false;
			}
			else
			{
				$mPars['usuari_id']=$usuari;
				$mPars['pasw']=$pasw;
				$mPars['nivell']=$mRow['nivell'];
				$mPars['grups_usuari']=$mRow['grups'];
				$mPars['usuari']=$mRow['usuari'];
				$mPars['DEMO_nivell']='visitant';
				if($mRow['nivell']!='visitant')
				{
					$mPars['idGrupsUsuariResponsableChain']=db_getIdGrupsUsuariResponsableChain($db);
					$mPars['idPerfilsUsuariResponsableChain']=db_getIdPerfilsUsuariResponsableChain($db);
					$DEMO_db=db_conect($mDEMO_Params);
					//selectDb($mDEMO_Params['bd'],$DEMO_db);
					$mPars['DEMO_idGrupsUsuariResponsableChain']=db_getDEMO_IdGrupsUsuariResponsableChain($DEMO_db);
					$mPars['DEMO_idPerfilsUsuariResponsableChain']=db_getDEMO_IdPerfilsUsuariResponsableChain($DEMO_db);
						$mDEMO_usuari=db_getUsuari($mPars['usuari_id'],$DEMO_db);
					$mPars['DEMO_nivell']=$mDEMO_usuari['nivell'];
					closeDb($DEMO_db);
					$db=db_conect($mParams);
					
				}
				return true;
			}
		}
	}
	unset($mPars);
			
	return false;
}

//---------------------------------------------------------------------
function db_getRebost($db)
{
	global $mPars;
	$mRebost=array();
	if($mPars['grup_id']=='0')
	{
		$mRebost['id']='0';
		$mRebost['nom']='CAC';
		$mRebost['categoria']='';
		$mRebost['productors']='';
		$mRebost['compte_ecos']='';
		$mRebost['compte_cieb']='';
	}
	else if($result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where ref='".$mPars['grup_id']."'",$db))
	{
		//echo "<br> 1038".mysql_errno() . ": " . mysql_error(). "\n";
		$mRebost=mysql_fetch_array($result,MYSQL_ASSOC);
	}

	return $mRebost;
}


//------------------------------------------------------------------------------
function db_getQuantitats($db)
{
	global $mPars;
	
	$mQuantitats=array();
	
	if(!$result=@mysql_query("select * from comandes_".$mPars['selRutaSufix'],$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mProductesComanda=explode(';',$mRow['resum']);
			for($i=0;$i<count($mProductesComanda);$i++)
			{
			
				$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
		
				$index=str_replace('producte_','',$mIndexQuantitat[0]);
				$quantitat=@$mIndexQuantitat[1];
				if($index!='' && $index!=0)
				{
					if(!isset($mQuantitats[$index])){$mQuantitats[$index]=0.00;}
					$quantitatMEM=$mQuantitats[$index];
					if($mRow['rebost']=='0-CAC')
					{
						$mQuantitats[$index]+=$quantitat;
						//echo "<br>comanda ".$mRow['rebost'].": id[".$index."]=>+".$quantitat.",(abans: ".$quantitatMEM.", ara: total:".$mQuantitats[$index].")";
					}
					else
					{
						$mQuantitats[$index]-=$quantitat;
						//echo "<br>comanda ".$mRow['rebost'].": id[".$index."]=>-".$quantitat.",(abans: ".$quantitatMEM.", ara: total:".$mQuantitats[$index].")";
					}
				}
			}
		}
	}

	return $mQuantitats;
}

//------------------------------------------------------------------------------
function db_getQuantitatsRebost($db)
{
	global $mPars;
	
	$mQuantitats=array();
	
	if(!$result=@mysql_query("select * from comandes_".$mPars['selRutaSufix']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."'",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mProductesComanda=explode(';',$mRow['resum']);
			for($i=0;$i<count($mProductesComanda);$i++)
			{
			
				$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
		
				$index=str_replace('producte_','',$mIndexQuantitat[0]);
				$quantitat=@$mIndexQuantitat[1];
				if($index!='' && $index!=0)
				{
					$mQuantitats[$index]=$quantitat;
				}
			}
		}
	}

	return $mQuantitats;
}

//------------------------------------------------------------------------------
function db_getTotalKgComandaCAC($db)
{
	global $mPars;
	
	$mQuantitats=array();
	$mTotalKgComandaCac=array();
	$mTotalKgComandaCac['totalkg']=0;
	
	if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where rebost='0-CAC'",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

		$mProductesComanda=explode(';',$mRow['resum']);
		for($i=0;$i<count($mProductesComanda);$i++)
		{
			$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
		
			$index=str_replace('producte_','',$mIndexQuantitat[0]);
			$quantitat=@$mIndexQuantitat[1];
			if($index!='' && $index!=0)
			{
				$mTotalKgComandaCac['totalkg']+=$quantitat;
			}
		}
	}
	return $mTotalKgComandaCac;
}


//------------------------------------------------------------------------------
function db_guardarCostTransportArepartir($db)
{
	global $mPars;

	if(!$result=mysql_query("update comandes_".$mPars['selRutaSufix']." set ctear='".$mPars['ctear']."', ms_ctear='".$mPars['ms_ctear']."',ctiar='".$mPars['ctiar']."', ms_ctiar='".$mPars['ms_ctiar']."' where rebost='0-CAC'",$db))
	{
		//echo "<br> ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}

	return true;
}

//------------------------------------------------------------------------------
function db_getParsCostTransportArepartir($db)
{
	global $mPars;
	
	if(!$result=mysql_query("select ctear,ms_ctear,ctiar,ms_ctiar from comandes_".$mPars['selRutaSufix']." where rebost='0-CAC'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		if(!$mParsCTAR=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			return false;
		}
		else
		{
			$mPars['ctear']=$mParsCTAR['ctear'];
			$mPars['ms_ctear']=$mParsCTAR['ms_ctear'];
			$mPars['ctiar']=$mParsCTAR['ctiar'];
			$mPars['ms_ctiar']=$mParsCTAR['ms_ctiar'];
			
			return $mParsCTAR;
		}
	}
	
	return false;
}

//------------------------------------------------------------------------------
function db_getQuantitatCac($db)
{
	global $mRebost,$mPars;
	
	$mRebostProg=db_getRebost($db);
	
	if(!$result=mysql_query("select sum(estoc_previst*pes) from productes_".$mPars['selRutaSufix'],$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$mParsCAC['quantitat_total']=$mRow[0];
	}

	if(!$result=mysql_query("select ctiar,ms_ctiar,ctear,ms_ctear from comandes_".$mPars['selRutaSufix']." where rebost='0-CAC'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		$mParsCAC['cost_transport_r_unitats']=$mRow['ctiar']+$mRow['ctear'];
		$mParsCAC['cost_transport_r_ecos']=$mRow['ctiar']*$mRow['ms_ctiar']/100+$mRow['ctear']*$mRow['ms_ctear']/100;
		$mParsCAC['cost_transport_r_euros']=$mRow['ctiar']*(100-$mRow['ms_ctiar'])/100+$mRow['ctear']*(100-$mRow['ms_ctear'])/100;
        if($mParsCAC['cost_transport_r_unitats']!=0)
		{
			$mParsCAC['cost_transport_r_ms']=$mParsCAC['cost_transport_r_ecos']*100/$mParsCAC['cost_transport_r_unitats'];
		}
		else
		{
			$mParsCAC['cost_transport_r_ms']=0;
		}
	}

	/*
	if(!$result=mysql_query("select anclatge_ruta,distancia_anclatge from municipis_".$mPars['selRutaSufix']." where municipi='".@$mRebost['municipi']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		$mParsCAC['anclatge_ruta']=$mRow['anclatge_ruta'];
		$mParsCAC['distancia_anclatge']=$mRow['distancia_anclatge'];
	}
	*/
	$mParsCAC['anclatge_ruta']=0;
	$mParsCAC['distancia_anclatge']=0;
	

	return $mParsCAC;
}

//------------------------------------------------------------------------------
function db_getIntercanvisRebosts($db)
{
	global $mParsCAC,$factorParticipacioRutaCac,$mPars;
	
	$mIntercanvisRebosts=array();
	$mParsCAC['kg_intercanvisRebosts']=0;
	
	$i=0;
	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2)),$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mIntercanvisRebosts[$i]=$mRow;
			$mParsCAC['kg_intercanvisRebosts']+=$mRow['quantitat'];
			$i++;
		}
	}
	$mParsCAC['kg_intercanvisRebosts']*=$factorParticipacioRutaCac;
	
	return $mIntercanvisRebosts;

}
//------------------------------------------------------------------------------
function db_getIntercanvisRebost($db)
{
	global $mRebost, $mPars, $mParsCAC,$factorParticipacioRutaCac;
	
	$mIntercanvisRebosts=array();
	$mParsCAC['kg_intercanvisRebost']=0;
	
	$i=0;
	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2))." where SUBSTRING_INDEX(rebost_comprador,'-',1)='".$mPars['grup_id']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mIntercanvisRebosts[$i]=$mRow;
			$mParsCAC['kg_intercanvisRebost']+=$mRow['quantitat'];
			$i++;
		}
	}
	$mParsCAC['kg_intercanvisRebost']*=$factorParticipacioRutaCac;

	return $mIntercanvisRebosts;

}
//------------------------------------------------------------------------------

function putNovaComandaProdEsp($mNovaComandaProdEsp,$i_id, $db)
{
	global $mPars, $mRebost,$mPropietatsSelUsuari,$mComptesUsuari,$sumaRecolzamentComandesSelUsuari;
	
	$mComptesUsuari_=array();
	
	$formaPagament=$mNovaComandaProdEsp['ecos'].",".$mNovaComandaProdEsp['compte_ecos'].",".$mNovaComandaProdEsp['ecobasics'].",".$mNovaComandaProdEsp['compte_cieb'].",".$mNovaComandaProdEsp['euros'].",".$mNovaComandaProdEsp['euMetalic'].",".$mNovaComandaProdEsp['euTransf'];
	$result=mysql_query("select * from comandes_especials1_".$mPars['selRutaSufix']." where num_comanda='".$mNovaComandaProdEsp['formId']."'",$db);
	if(!$mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		//echo "<br>"."insert into comandes_especials1_".$mPars['selRutaSufix']." values('','".$mNovaComandaProdEsp['formId']."','".$mPars['periode_comanda']."','".$mPars['grup_id'].'-'.(urlencode($mRebost['nom']))."','".$mPars['usuari_id']."','".date('d-m-Y h:i:s')."','producte_".$i_id.":1;','','','','','".$formaPagament."','".$mNovaComandaProdEsp['codi']."','".$mNovaComandaProdEsp['nom']."','".$mNovaComandaProdEsp['cognoms']."','".$mNovaComandaProdEsp['email']."','".$mNovaComandaProdEsp['mobil']."','".$mNovaComandaProdEsp['compte_ecos']."','".$mNovaComandaProdEsp['compte_cieb']."','".$i_id."','".$mNovaComandaProdEsp['quantitat']."','')";
		if(!$result=mysql_query("insert into comandes_especials1_".$mPars['selRutaSufix']." values('','".$mNovaComandaProdEsp['formId']."','".$mPars['periode_comanda']."','".$mPars['grup_id'].'-'.(urlencode($mRebost['nom']))."','".$mPars['selUsuariId']."','".date('d-m-Y h:i:s')."','producte_".$i_id.":1;','','','','','".$formaPagament."','".$mNovaComandaProdEsp['segell']."','".$mNovaComandaProdEsp['nom']."','".$mNovaComandaProdEsp['cognoms']."','".$mNovaComandaProdEsp['email']."','".$mNovaComandaProdEsp['mobil']."','".$mNovaComandaProdEsp['compte_ecos']."','".$mNovaComandaProdEsp['compte_cieb']."','".$i_id."','".$mNovaComandaProdEsp['quantitat']."','')",$db))
		{
			//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			return false;
		}
		else
		{
			//actualitzar taula comandes
			$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."'",$db);
			//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			
			if($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				if(substr_count($mRow['resum'],'producte_'.$i_id.':')>0)
				{
					$chain1943=substr($mRow['resum'],strpos($mRow['resum'],'producte_'.$i_id.':')); 
					$chain1943=substr($chain1943,0,strpos($chain1943,';')); 
					$q=substr($chain1943,strpos($chain1943,':')+1);
					$quantitat=$q;
					$mRow['resum']=str_replace('producte_'.$i_id.':'.$quantitat.';','producte_'.$i_id.':'.($quantitat+1).';',$mRow['resum']);

					$result=mysql_query("update comandes_".$mPars['selRutaSufix']." set resum='".$mRow['resum']."' where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."'",$db);
					//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				}
				else
				{
					$result=mysql_query("update comandes_".$mPars['selRutaSufix']." set resum='".$mRow['resum'].'producte_'.$i_id.':'."1;' where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."'  AND usuari_id='".$mPars['selUsuariId']."'",$db);
					//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				}
			}
			else		
			{
				$result=@mysql_query("insert into comandes_".$mPars['selRutaSufix']." values('','".(date('YmdHis'))."','".$mPars['periode_comanda']."','".$mPars['grup_id'].'-'.(urlencode($mRebost['nom']))."','".$mPars['selUsuariId']."','".(date('dmY'))."','producte_".$i_id.":1;','','','','','0,0,0,0,0,0,100,0,0','".$mPars['grup_id']."','')",$db);
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			}

				//7. guardar utilitzatEcosR
				
				if(isset($mComptesUsuari['utilitzat_ecos_r_chain']))
				{
					$mRecolzamentComandesUsuari=getRecolzamentComandesUsuari($mComptesUsuari['utilitzat_ecos_r_chain']);
				}
				else
				{
					$mComptesUsuari['utilitzat_ecos_r_chain']='';
					$mRecolzamentComandesUsuari=array();
				}
				$mRecolzamentComandesUsuari['ESP'.date('d/i/Y')]=$mNovaComandaProdEsp['utilitzatEcosR'];
				$chain=makeChainRecolzamentComandesUsuari($mRecolzamentComandesUsuari);
				//membres comissio amb saldo ecos r positiu
				//$mComptesUsuari['saldo_ecos_r']=$comptesUsuari_saldo_ecos_r-$mNovaComandaProdEsp['utilitzatEcosR'];
				$mComptesUsuari['utilitzat_ecos_r']=$sumaRecolzamentComandesSelUsuari;
				$mComptesUsuari['utilitzat_ecos_r_chain']=$chain;
				db_guardarComptesUsuari($db);

			return true;
		}
	}
	return false;

}
//------------------------------------------------------------------------------
function db_getComandesEspecialsPerProductors($db)
{
	global $mPars;
	
	$mComandesEspecials=array();
	$mProductesEspecials=array();
	$mProductesEspecialsPerProductors=array();
	
	$i=0;
	$idsProductesEspecials=',';

		if($mPars['grup_id']=='0')
	{
		$result=mysql_query("select * from comandes_especials1_".$mPars['selRutaSufix'],$db);
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		$result=mysql_query("select * from comandes_especials1_".$mPars['selRutaSufix']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."'",$db);
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		if(!array_key_exists($mRow['c7'],$mComandesEspecials))
		{
			$mComandesEspecials[$mRow['c7']]=array();
		}
		array_push($mComandesEspecials[$mRow['c7']],$mRow);
		$idsProductesEspecials.='_'.$mRow['c7'].',';
	}
	//obtenir productes especials:
	$result=mysql_query("select * from productes_".$mPars['selRutaSufix']." where  locate(concat('_',id,','),'".$idsProductesEspecials."')>0 order by producte ASC",$db);
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		//ordenar per productor
		if(!array_key_exists(urlencode($mRow['productor']),$mProductesEspecialsPerProductors))
		{
			$mProductesEspecialsPerProductors[urlencode($mRow['productor'])]=array();
		}
		
		if(!array_key_exists($mRow['id'],$mProductesEspecialsPerProductors[urlencode($mRow['productor'])]))
		{
			$mProductesEspecialsPerProductors[urlencode($mRow['productor'])][$mRow['id']]=$mRow;
			$mProductesEspecialsPerProductors[urlencode($mRow['productor'])][$mRow['id']]['comandes']=array();
			$mProductesEspecialsPerProductors[urlencode($mRow['productor'])][$mRow['id']]['comandes']=$mComandesEspecials[$mRow['id']];
		}
		
	}
	//echo "<pre>";var_dump($mComandesEspecials);echo "</pre>";
	//echo "<pre>";var_dump($mProductesEspecialsPerProductors);echo "</pre>";

	return $mProductesEspecialsPerProductors;
}
//------------------------------------------------------------------------------
function getDatesReservaProducte($db,$idProducte)
{
	global $mPars;

	$quantitat=0;

	$mDCRP=array(); //Dates Reserva Producte per cada uauari a per cada producte a cada Rebost, segons registre comandes_seg
	$mDCRP2=array(); //Dates Reserva Producte per cada uauari a per cada producte a cada Rebost, segons registre comandes
	
	//revisions
	//echo "<br>select * from comandes_seg_".$mPars['selRutaSufix']." where LOCATE('_".$idProducte.":',resum)>0";
	$result=mysql_query("select * from ".$mPars['taulaComandesSeg']." where LOCATE('_".$idProducte.":',resum)>0",$db);
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		//if(!in_array($mRow['rebost'],$mDCRP)){$mDCRP[$mRow['rebost']]=array();}

		$quantitat=substr($mRow['resum'],strpos($mRow['resum'],"_".$idProducte.":"));
		$quantitat=substr($quantitat,0,strpos($quantitat,';'));
		$quantitat=substr($quantitat,strpos($quantitat,':')+1);
	//	echo $mRow['rebost']."-".$mRow['num_comanda']."-".$quantitat."<br>";

		$rebostRef=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
		$mDCRP[$rebostRef][$idProducte][$mRow['usuari_id']]['quantitat']=$quantitat;
		$mDCRP[$rebostRef][$idProducte][$mRow['usuari_id']]['num_comanda']=$mRow['num_comanda'];
		$mDCRP[$rebostRef][$idProducte][$mRow['usuari_id']]['compte_ecos']=$mRow['compte_ecos'];
	}
	reset($mDCRP);
	
	//�ltima comanda
	$result=mysql_query("select * from ".$mPars['taulaComandes']." where LOCATE('_".$idProducte.":',resum)>0",$db);
	
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		//if(!in_array($mRow['rebost'],$mDCRP)){$mDCRP[$mRow['rebost']]=array();}
	
		$rebostRef=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
		$quantitat=substr($mRow['resum'],strpos($mRow['resum'],"_".$idProducte.":"));
		$quantitat=substr($quantitat,0,strpos($quantitat,';'));
		$quantitat=substr($quantitat,strpos($quantitat,':')+1);
		//echo $mRow['rebost']."-".$mRow['num_comanda']."-".$quantitat."<br>";
	
		$mDCRP2[$rebostRef][$idProducte][$mRow['usuari_id']]['quantitat']=$quantitat;
		$mDCRP2[$rebostRef][$idProducte][$mRow['usuari_id']]['num_comanda']=$mRow['num_comanda'];
		$mDCRP2[$rebostRef][$idProducte][$mRow['usuari_id']]['compte_ecos']=$mRow['compte_ecos'];
	}
	reset($mDCRP2);
	
	$quantitat2=0;
	$numComanda2=0;

	while(list($rebostRef,$mRebost)=each($mDCRP2))
	{
		if(count($mRebost)>0)
		{
			while(list($idProducte,$mRevisionsUsuaris)=each($mRebost))
			{
				if(is_array($mRevisionsUsuaris))
				{
					//$mRevisio=ksort($mRevisio);
					while(list($usuariId,$mRevisioUsuari)=each($mRevisionsUsuaris))
					{
							if(isset($mDCRP[$rebostRef][$idProducte][$usuariId]['quantitat']))
							{
								if($mRevisioUsuari['quantitat']==$mDCRP[$rebostRef][$idProducte][$usuariId]['quantitat'])
								{
									//echo $numComanda." - ".$quantitat."!=".$numComanda2."-".$quantitat2."<br>";
									$mDCRP2[$rebostRef][$idProducte][$usuariId]['quantitat']=$mDCRP[$rebostRef][$idProducte][$usuariId]['quantitat'];
									$mDCRP2[$rebostRef][$idProducte][$usuariId]['num_comanda']=$mDCRP[$rebostRef][$idProducte][$usuariId]['num_comanda'];
									$mDCRP2[$rebostRef][$idProducte][$usuariId]['compte_ecos']=$mDCRP[$rebostRef][$idProducte][$usuariId]['compte_ecos'];
								}		
							}
					}
				}
			}
			//unset($mDCRP[$rebostRef][$id]);
			//$mDCRP[$rebost]=array();
			//$mDCRP[$rebostRef][$idProducte]['quantitat']=$quantitat2;
			//$mDCRP[$rebostRef][$idProducte]['num_comanda']=$numComanda2;
		}
		//else
		//{
		//	$mDCRP[$rebost][$idProducte]['quantitat']=0;
		//	$mDCRP[$rebost][$idProducte]['num_comanda']=0;
		//}

		//$quantitat2=0;
		//$numComanda2=0;
	}
	reset($mDCRP2);
	
	return $mDCRP2;
}

//------------------------------------------------------------------------------
function getTeam($db)
{
	$mTeam=array();
	$result=mysql_query("select * from usuaris where nivell='sadmin' OR nivell='admin' OR nivell='coord'",$db);
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mTeam[$i]=$mRow;
		$i++;
	}

	return $mTeam;
}
//------------------------------------------------------------------------------
function formatarData($dataNum)
{
	$any=substr($dataNum,0,4);
	$mes=substr($dataNum,4,2);
	$dia=substr($dataNum,6,2);
	$hora=substr($dataNum,8,2);
	$min=substr($dataNum,10,2);
	$seg=substr($dataNum,12,2);
	
	$dataFormatada=$dia."/".$mes."/".$any."-".$hora.":".$min.":".$seg;

	return $dataFormatada;
}

//------------------------------------------------------------------------------
function getDataComanda($db)
{
	global $mPars;
	if($mPars['selUsuariId']=='0') //grup
	{
		$result=mysql_query("select num_comanda from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id!='0' order by id DESC",$db);
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	}
	else
	{
		$result=mysql_query("select num_comanda from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."' order by id DESC",$db);
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	}
	return $mRow['num_comanda'];
}

//------------------------------------------------------------------------------
function db_getPuntsEntrega($db,$opcio)
{
	global $mRebost,$mPars;
	
	$mPuntsEntrega=array();
	
	$result=mysql_query("select id,nom,municipi,adressa,categoria,gestor_magatzem,email_rebost,mobil from rebosts_".$mPars['selRutaSufix']." where (locate('1-rebost,',categoria)>0 OR locate('1-puntEntrega,',categoria)>0) AND locate('inactiu,',categoria)=0 order by nom ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	
	if($opcio=='index')
	{
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPuntsEntrega[$i]=$mRow;
			$i++;
		}
	}
	else if($opcio=='ref') //vistaAlbara.php
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPuntsEntrega[$mRow['id']]=$mRow;
		}
	}
	return $mPuntsEntrega;
}
//------------------------------------------------------------------------------
function db_activarTramRuta($id,$activar,$db)
{
	global $mPars;
	
	$result=mysql_query("update trams_ruta_".$mPars['selRutaSufix']." set actiu='".$activar."' where id='".$id."'",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	return;
}

//------------------------------------------------------------------------------
function db_getTramsActiusRuta($db)
{
	global $mPars;
	
	$mTramsActiusRuta=array();
	
	$result=mysql_query("select * from trams_ruta_".$mPars['selRutaSufix']." where actiu=1 order by ordre ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mTramsActiusRuta[$i]=$mRow;
		$i++;
	}

	return $mTramsActiusRuta;
}
//------------------------------------------------------------------------------
function db_getTramsRuta($db)
{
	global $mPars;
	
	$mTramsRuta=array();
	
	$result=mysql_query("select * from trams_ruta_".$mPars['selRutaSufix']." order by ordre ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mTramsRuta[$i]=$mRow;
		$i++;
	}

	return $mTramsRuta;
}
//------------------------------------------------------------------------------
function db_getTram($db)
{
	global $mPars;
	
	//echo "<br>select * from trams_".$mPars['selRutaSufix']." where id='".$mPars['selTramId']."'";
	$result=mysql_query("select * from trams_".$mPars['selRutaSufix']." where id='".$mPars['selTramId']."'",$db);
	//echo "<br>  2289 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

	return $mRow;
}//------------------------------------------------------------------------------
function db_getTramsRutaPerTram($db)
{
	global $mPars;
	
	$mTramsRuta=array();
	
	$result=mysql_query("select * from trams_ruta_".$mPars['selRutaSufix']." order by municipi_origen,municipi_desti ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mTramsRuta[$i]=$mRow;
		$i++;
	}

	return $mTramsRuta;
}
//------------------------------------------------------------------------------
function db_getTramRuta($editarTramRutaId,$db)
{
	global $mPars;
	
	$mTramRutaEditar=array();
	$result=mysql_query("select * from trams_ruta_".$mPars['selRutaSufix']." where id='".$editarTramRutaId."'",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mRow['municipi_origen']=urlencode($mRow['municipi_origen']);
		$mRow['municipi_desti']=urlencode($mRow['municipi_desti']);
		$mTramRutaEditar=$mRow;
	}

	return $mTramRutaEditar;
}
//------------------------------------------------------------------------------
function db_guardarTramRuta($mGuardarTram,$db)
{
	global $mPars;
	
	if(!$result=mysql_query("update trams_ruta_".$mPars['selRutaSufix']." set ordre='".$mGuardarTram['ordre']."',data_hora='".$mGuardarTram['data_hora']."',municipi_origen='".$mGuardarTram['municipi_origen']."',municipi_desti='".$mGuardarTram['municipi_desti']."',km='".$mGuardarTram['km']."',ruta='".$mGuardarTram['ruta']."',hores='".$mGuardarTram['hores']."',minuts='".$mGuardarTram['minuts']."',temps_parada='".$mGuardarTram['temps_parada']."',actiu='".$mGuardarTram['actiu']."',vehicle_id='".$mGuardarTram['vehicle_id']."',notes='".$mGuardarTram['notes']."' where id='".$mGuardarTram['id']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="Atenci�: no s\\'ha pogut guardar el tram de ruta";
	}
	else
	{
		$missatgeAlerta="el tram de ruta s\\'ha guardat correctament";
	}
	return $missatgeAlerta;
}

//------------------------------------------------------------------------------
function guardarNouTramRuta($mTramRuta,$db)
{
	global $mPars;
	
	$missatgeAlerta='';
	if(!$result=mysql_query("insert into trams_ruta_".$mPars['selRutaSufix']." values('','".$mTramRuta['ordre']."','','".$mTramRuta['municipi_origen']."','".$mTramRuta['municipi_desti']."','".$mTramRuta['km']."','".$mTramRuta['hores']."','".$mTramRuta['minuts']."','".$mTramRuta['temps_parada']."','".$mTramRuta['ruta']."',0,'".$mTramRuta['vehicle_id']."','".$mTramRuta['notes']."')",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="Atenci�: no s\\'ha pogut guardar el nou tram de ruta";
	}
	else
	{
		$missatgeAlerta="el nou tram de ruta s\\'ha guardat correctament";
	}
	return $missatgeAlerta;
}
//------------------------------------------------------------------------------
function esborrarTramRuta($esborrarTramRutaId,$db)
{
	global $mPars;
	
	$missatgeAlerta='';
	if(!$result=mysql_query("delete from trams_ruta_".$mPars['selRutaSufix']." where id='".$esborrarTramRutaId."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="Atenci�: no s\\'ha pogut esborrar el nou tram de ruta";
	}
	else
	{
		$missatgeAlerta="el nou tram de ruta s\\'ha esborrat correctament";
	}
	return $missatgeAlerta;
}
	

//------------------------------------------------------------------------------
function db_getMunicipis2($db)
{
	$mMunicipis2=array();
	
	$result=mysql_query("select * from municipis2 order by municipi ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mRow['municipi']=urlencode($mRow['municipi']);
		$mRow['comarca']=urlencode($mRow['comarca']);
		$mMunicipis2[$i]=$mRow;
		$i++;
	}

	return $mMunicipis2;
}

//------------------------------------------------------------------------------
function db_getMunicipis2Id($db)
{
	$mMunicipis2Id=array();
	
	$result=mysql_query("select * from municipis2 order by municipi ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mRow['municipi']=urlencode($mRow['municipi']);
		$mRow['comarca']=urlencode($mRow['comarca']);
		$mMunicipis2Id[$mRow['id']]=$mRow;
		$i++;
	}

	return $mMunicipis2Id;
}
//------------------------------------------------------------------------------
function db_getMunicipisId($db)
{
	$mMunicipis=array();
	
	$result=mysql_query("select * from municipis2 order by municipi ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mMunicipis[$mRow['id']]=urlencode($mRow['municipi']);
	}

	return $mMunicipis;
}
//------------------------------------------------------------------------------
function db_getParametresRuta($db)
{
	global $mPars;
	
	$mParametresRuta=array();
	
	$result=mysql_query("select * from parametres_ruta_".$mPars['selRutaSufix']." order by id ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mParametresRuta[$i]=$mRow;
		$i++;
	}

	return $mParametresRuta;
}

//------------------------------------------------------------------------------
function db_guardarParametreRuta($mParametre,$db)
{
	global $mPars;
	
	if(!$result=mysql_query("update parametres_ruta_".$mPars['selRutaSufix']." set valor='".$mParametre['valor']."',notes='".$mParametre['notes']."' where id='".$mParametre['id']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="Atenci�: no s\\'ha pogut guardar el par�metre de ruta";
	}
	else
	{
		$missatgeAlerta="el par�metre de ruta s\\'ha guardat correctament";
	}
	return $missatgeAlerta;
}




//------------------------------------------------------------------------------
function makeParsChain($mPars_)
{
	global $getParsCode1,$getParsCode2;

	$parsChain='';
	while(list($key,$val)=each($mPars_))
	{
		$parsChain.=$key.$getParsCode1.$val.$getParsCode2;
	}

	return $parsChain;
}

//------------------------------------------------------------------------------
function getPars($parsChain)
{
	global $getParsCode1,$getParsCode2;

	$mPars_=array();
	
	$mPars_0=explode($getParsCode2,$parsChain);
	while(list($key,$val)=each($mPars_0))
	{
		$var=substr($val,0,strpos($val,$getParsCode1));
		$valor=substr($val,strpos($val,$getParsCode1)+strlen($getParsCode1));
		$mPars_[$var]=$valor;
	}

	return $mPars_;
}

//------------------------------------------------------------------------------
function calculaHoresRuta($hora,$minut)
{
	global $OhoraIniciRuta;

	$OhoraIniciRuta->add(new DateInterval('PT'.$hora.'H'.$minut.'M'));
	
	return $OhoraIniciRuta->format("H:i d/m/Y");
}

//------------------------------------------------------------------------------

function db_getProductors($db)
{
	global $mPars;
	
	$mProductors=array();
	$mProductors_=array();

		$result=mysql_query("select SUBSTRING(productor,1,LOCATE('-',productor)-1) from ".$mPars['taulaProductes']." WHERE llista_id='".$mPars['selLlistaId']."'",$db);
		//echo "<br> 3614 db.php  ".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=@mysql_fetch_array($result,MYSQL_NUM))
		{
			$mProductors_[$mRow[0]]=array();
		}
		$productorsIdChain=','.(implode(',',array_keys($mProductors_))).',';
		
		//echo "<br>select * from ".$mPars['taulaProductors']." WHERE LOCATE(CONCAT(',',id,','),CONCAT(' ','".$productorsIdChain."'))>0 order by projecte ASC";
		$result=mysql_query("select * from ".$mPars['taulaProductors']." WHERE LOCATE(CONCAT(',',id,','),CONCAT(' ','".$productorsIdChain."'))>0 order by projecte ASC",$db);
		//echo "<br> 3622 db.php  ".mysql_errno() . ": " . mysql_error(). "\n";
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mProductors[$i]=$mRow;
			$i++;
		}

	return $mProductors;
}

//------------------------------------------------------------------------------
function getCategories($db)
{
	global $mPars;
	
	$result=mysql_query("select DISTINCT categoria0 from productes_".$mPars['selRutaSufix']."  order by categoria0 ASC",$db);
	//echo "<br>  2329 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mCategories[$i]=$mRow['categoria0'];
		$i++;
	}
	
	if($mPars['selLlistaId']!=0)
	{
		$result=mysql_query("select DISTINCT categoria0 from productes_grups WHERE llista_id='".$mPars['selLlistaId']."' order by categoria0 ASC",$db);
		//echo "<br>  2329 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mCategories[$i]=$mRow['categoria0'];
			$i++;
		}
	}

	$mCategories_=array_values($mCategories);
	return $mCategories_;
}

//------------------------------------------------------------------------------
function getSubCategories($db)
{
	global $mPars;
	
	$mSubCategories_=array();
	$mSubCategories=array();
	
	if($mPars['selLlistaId']==0)
	{
		$result=mysql_query("select categoria0,categoria10,estoc_previst from productes_".$mPars['selRutaSufix']." order by categoria0,categoria10 ASC",$db);
		//echo "<br>  2263 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if(!isset($mSubCategories_[$mRow['categoria0']])){$mSubCategories_[$mRow['categoria0']]=array();}
			if(!isset($mSubCategories_[$mRow['categoria0']][$mRow['categoria10']])){$mSubCategories_[$mRow['categoria0']][$mRow['categoria10']]=0;}
			$mSubCategories_[$mRow['categoria0']][$mRow['categoria10']]+=$mRow['estoc_previst'];
		}
	}
	else if($mPars['selLlistaId']!=0)
	{
		$result=mysql_query("select categoria0,categoria10,estoc_previst from productes_grups WHERE llista_id='".$mPars['selLlistaId']."' order by categoria0 ASC",$db);
		//echo "<br>  2329 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if(!isset($mSubCategories_[$mRow['categoria0']])){$mSubCategories_[$mRow['categoria0']]=array();}
			if(!isset($mSubCategories_[$mRow['categoria0']][$mRow['categoria10']])){$mSubCategories_[$mRow['categoria0']][$mRow['categoria10']]=0;}
			$mSubCategories_[$mRow['categoria0']][$mRow['categoria10']]+=$mRow['estoc_previst'];
		}
	}
	
	$i=0;
	while(list($categoria0,$mVal)=each($mSubCategories_))
	{
		while(list($categoria10,$sumaEstocPrevist)=each($mVal))
		{
			$mSubCategories[$i]['categoria0']=$categoria0;
			$mSubCategories[$i]['categoria10']=$categoria10;
			$mSubCategories[$i]['estoc_previst']=$sumaEstocPrevist;
			$i++;
		}
	}

	return $mSubCategories;
}

//------------------------------------------------------------------------------
function getSubCategories2($db)
{
	global $mPars;
	
	$result=mysql_query("select DISTINCT(categoria0) from productes_".$mPars['selRutaSufix']."  order by categoria0 ASC",$db);
	//echo "<br>  2292 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_NUM))
	{
		$mSubCategories['categoria0'][$i]=$mRow[0];
		$i++;
	}

	if($mPars['selLlistaId']!=0)
	{
		$result=mysql_query("select DISTINCT(categoria0) from productes_grups WHERE llista_id='".$mPars['selLlistaId']."' order by categoria0 ASC",$db);
		//echo "<br>  2329 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			$mSubCategories['categoria0'][$i]=$mRow[0];
			$i++;
		}
	}
	
	$mSubCategories_['categoria0']=array_values($mSubCategories['categoria0']);
	unset($mSubCategories_['categoria0']['']);
	asort($mSubCategories_['categoria0']);
	
	$result=mysql_query("select DISTINCT(categoria10) from productes_".$mPars['selRutaSufix']."  order by categoria10 ASC",$db);
	//echo "<br>  2300 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_NUM))
	{
		$mSubCategories['categoria10'][$i]=$mRow[0];
		$i++;
	}

	if($mPars['selLlistaId']!=0)
	{
		$result=mysql_query("select DISTINCT(categoria10) from productes_grups WHERE llista_id='".$mPars['selLlistaId']."' order by categoria0 ASC",$db);
		//echo "<br>  2329 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			$mSubCategories['categoria10'][$i]=$mRow[0];
			$i++;
		}
	}

	$mSubCategories_['categoria10']=array_values($mSubCategories['categoria10']);
	unset($mSubCategories_['categoria10']['']);
	asort($mSubCategories_['categoria10']);
	


	return $mSubCategories_;
}
//------------------------------------------------------------------------------
function getCategoriesGrups($db)
{
	global $mPars,$mParametres;
	
	$mCategoriesGrups=array();
	$mCategoriesGrup=explode(',',$mParametres['categories_grups']['valor']);
	$i=0;
	while(list($key,$val)=each($mCategoriesGrup))
	{
		if($val!='' && !in_array($val,$mCategoriesGrups) && $val!='visitant'  && $val!='actiu'  && $val!='inactiu')
		{
			$mCategoriesGrups[$i]=$val;
			$i++;
		}
	}
	natsort($mCategoriesGrups);

	return $mCategoriesGrups;
}

//------------------------------------------------------------------------------
function db_getZones($db)
{
	global $mPars,$mParametres;
	
	$mZones=array();
	$mZones_=explode(',',$mParametres['categories_grups']['valor']);
	$i=0;
	while(list($key,$val)=each($mZones_))
	{
		if($val!='' && !in_array($val,$mZones) && $val!='visitant'  && $val!='actiu'  && $val!='inactiu' && substr($val,0,strpos($val,'-'))==2)
		{
			$mZones[$i]=substr($val,strpos($val,'-')+1);
			$i++;
		}
	}
	natsort($mZones);
	//ksort($mZones);
	return $mZones;
}
//------------------------------------------------------------------------------
function getCategoriesGrup($mRebost)
{
	global $mPars;
	$mCategoriesGrup=array();
	$mCategoriesGrup_=explode(',',$mRebost['categoria']);
	while(list($key,$val)=each($mCategoriesGrup_))
	{
		$categoriaIndex=substr($val,0,strpos($val,'-'));
		$categoriaNoIndex=substr($val,strpos($val,'-')+1);
		if($categoriaIndex!='' && $val!='' && $val!='actiu' && $val!='inactiu' && $val!='visitant')
		{
			$mCategoriesGrup[$categoriaIndex]=$categoriaNoIndex;
		}
	}
	natsort($mCategoriesGrup_);

	return $mCategoriesGrup;
}
/*
//------------------------------------------------------------------------------
function getCategoriesGrups($db)
{
	global $mPars;
	
	$mCategoriesGrups=array();
	$result=mysql_query("select DISTINCT categoria from rebosts_".$mPars['selRutaSufix']." order by categoria ASC",$db);
	//echo "<br>  2329 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$i=0;
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mCategoriesGrup=explode(',',$mRow['categoria']);
		while(list($key,$val)=each($mCategoriesGrup))
		{
			if($val!='' && !in_array($val,$mCategoriesGrups) && $val!='visitant'  && $val!='actiu'  && $val!='inactiu')
			{
				$mCategoriesGrups[$i]=$val;
				$i++;
			}
		}
		reset($mCategoriesGrup);
	}
	natsort($mCategoriesGrups);
	return $mCategoriesGrups;
}
*/
//------------------------------------------------------------------------------
function getParametres($db)
{
	global $mPars;
	
	$result=mysql_query("select * from parametres_".$mPars['selRutaSufix']." where valor!='' order by parametre ASC",$db);
	//echo "<br>  41 config.php".mysql_errno() . ": " . mysql_error(). "\n";
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mParametres[$mRow['parametre']]=$mRow;
	}

	return $mParametres;
}

//------------------------------------------------------------------------------
//*v36 5-1-16 modificada funcio
//*v36.2-funcio
function getRutesSufixes($db)
{
	global $mParams;
	
	$result=mysql_query("SHOW TABLES FROM ".$mParams['bd'],$db);
	//echo "<br>  41 config.php".mysql_errno() . ": " . mysql_error(). "\n";
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		if(substr_count($mRow['Tables_in_'.$mParams['bd']],'productes')>0)
		{
			$mCamps=explode('_',$mRow['Tables_in_'.$mParams['bd']]);
			if(count($mCamps)==3) //ruta especial
			{
				if(!isset($mRutesSufixes[$mCamps[2]]))
				{
					$mRutesSufixes[$mCamps[2]]=$mCamps[1].'_'.$mCamps[2];
				}
				else
				{
					$mRutesSufixes[$mCamps[2].'_']=$mCamps[1].'_'.$mCamps[2];
				}
			}
			else
			{
				if(!isset($mRutesSufixes[$mCamps[1]]))
				{
					$mRutesSufixes[$mCamps[1]]=$mCamps[1];
				}
				else
				{
					$mRutesSufixes[$mCamps[1].'_']=$mCamps[1];
				}
			}
		}
	}
	ksort($mRutesSufixes);
	
	return $mRutesSufixes;
}


//------------------------------------------------------------------------------
function getRutesSufixesExportades($db)
{
	global $mParams;
	
	$mRutesSufixesExportades=array();
	
	$mFitxers=scandir('sql');
	while(list($key,$val)=each($mFitxers))
	{
		if(strlen($val)==4)
		{
			array_push($mRutesSufixesExportades,$val);
		}
	}
	natsort($mRutesSufixesExportades);
	
	return $mRutesSufixesExportades;
}
//------------------------------------------------------------------------------
function getConfig($db)
{
	global	$mPars,$mContinguts, $mText,$oDataFiPrecomandes,$mParametresConfig,$mParametres;
	
	
	$mRutesSufixes=getRutesSufixes($db);
	$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);
	while(!$result=mysql_query("select * from parametres_".$mPars['selRutaSufix']." order by propietats ASC",$db))
	{
		if(count($mRutesSufixes>0))
		{
			$mPars['selRutaSufix']=array_pop($mRutesSufixes);
		}
		else
		{
			break;
		}
		//echo "<br>  2595 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	
	$mParametres=$mParametresConfig;
	
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mParametres[$mRow['parametre']]=$mRow;
	}
	//asort($mParametres);
	if(!isset($mParametres['periodeIniciRecolzamentSelectiu']['valor'])){$mParametres['periodeIniciRecolzamentSelectiu']['valor']=1508;}
	$mDataFiRuta=explode('-',$mParametres['dataFiRuta']['valor']);
	$dataFiRecepcioIncidencies = new DateTime();
	$dataFiRecepcioIncidencies->setDate('20'.$mDataFiRuta[2]*1 , $mDataFiRuta[1]*1 , $mDataFiRuta[0]*1+1*$mParametres['diesFiRecepcioIncidencies']['valor'] );
	
	$mContinguts['index']['titol0']="<font color='LightSeaGreen'>S.C.A.D.I.</font>";
	$mContinguts['index']['titol1']="<font color='LightSeaGreen' style='font-size:14px;'> Sistema Cooperatiu d'Abastiment i Distribuci� Integral </font><br><font color='LightSeaGreen' style='font-size:12px;'>Central d'Abastiment Catalana (Cooperativa Integral Catalana)</font>";
	$mContinguts['form']['titol']="Pre-comandes fins el ".$mParametres['dataFiPrecomandes']['valor']."&nbsp;[RUTA:<b>".$mPars['selRutaSufix']."</b>]";
	
	$mText['info'][0]="
	<p>&#149; periode de precomandes: <b>".$mParametres['dataIniciPrecomandes']['valor']."</b> a <b>".$mParametres['dataFiPrecomandes']['valor']."</b></p>
	<p>&#149; Ruta d'Abastiment CAC: <b>".$mParametres['iniciRutaAbastiment']['valor']."</b> (per a productors 'dip�sit')</p>
	<p>&#149; Ruta Distribuci� CAC: <b>".$mParametres['diaIniciRuta']['valor']." ".$mParametres['dataIniciRuta']['valor']."</b> i <b>".$mParametres['diaFiRuta']['valor']." ".$mParametres['dataFiRuta']['valor']."</b></p>
	<p>&#149; Tancament de reserves de productes 'JJAA' (com el vi): ".(date('d/m/Y',strtotime(@$mParametres['dataTancamentReservesProductesJJAA']['valor']))).", en finalitzar el dia (00:00h)</p>
	<p>&#149; Tancament recepci� incid�ncies distribuci�: <font color='red'>".(date('d/m/Y',($dataFiRecepcioIncidencies->getTimestamp())))."</font>, en finalitzar el dia (00:00h)</p>
	";
	$mText['info'][1]="
	<b><i>Les pre-comandes es tancaran el dia ".$mParametres['dataFiPrecomandes']['valor']." i passaran a comandes definitives amb comprom�s de compra</i></b>.";
	
	return $mParametres;
}
//*v36-12-12-15 modificar funcio
//------------------------------------------------------------------------------
function putNouGrup($opcio,$mGrup,$db)
{
	global $mPars,$mCategoriesGrups,$mUsuarisRef;
	
	$categories='';
	
	$mGrup['categoria']=filtrarCategoriesGrupIncompatibles($mGrup['categoria']);

	if(is_array($mGrup['productors_associats']))
	{
		$productorsAssociats='';
		while(list($key,$val)=each($mGrup['productors_associats']))
		{
			$productorsAssociats.=','.$val;
		}
		$productorsAssociats.=',';
	}
	if($opcio=='guardarGrup')
	{
		if(!$result=mysql_query("select categoria from rebosts_".$mPars['selRutaSufix']." where ref='".$mGrup['id']."' ",$db))
		{
			//echo "<br>  2580 db.php".mysql_errno() . ": " . mysql_error(). "\n";

			return false;
		}
		else
		{
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			while(list($key,$val)=each($mCategoriesGrups))
			{
				$mRow['categoria']=str_replace(','.$val.',','',$mRow['categoria']);
				$mRow['categoria']=str_replace($val,'',$mRow['categoria']);
			}
			reset($mCategoriesGrups);
			$categories=$mRow['categoria'];
			while(list($key,$val)=each($mGrup['categoria']))
			{
				$categories.=','.$val.',';
			}
			reset($mGrup['categoria']);
			$categories=str_replace(',,',',',$categories);
		}
		
		//echo "<br>update rebosts_".$mPars['selRutaSufix']." set nom='".$mGrup['nom']."',adressa='".$mGrup['adressa']."',municipi='".$mGrup['municipi']."',bioregio='".$mGrup['bioregio']."',codi_ecoxarxa='".$mGrup['codi_ecoxarxa']."',categoria='".$categories."',gestor_magatzem='".$mUsuarisRef[$mGrup['usuari_id']]['usuari']."',usuari_id='".$mGrup['usuari_id']."',compte_ecos='".$mGrup['compte_ecos']."',compte_cieb='".$mGrup['compte_cieb']."',email_rebost='".$mGrup['email_rebost']."',productors_associats='".$productorsAssociats."',notes='".$mGrup['notes']."',mobil='".$mGrup['mobil']."',c_ecos='".$mGrup['c_ecos']."' where ref='".$mGrup['ref']."' ";
		
		if(!$result=mysql_query("update rebosts_".$mPars['selRutaSufix']." set nom='".$mGrup['nom']."',adressa='".$mGrup['adressa']."',municipi='".$mGrup['municipi']."',bioregio='".$mGrup['bioregio']."',codi_ecoxarxa='".$mGrup['codi_ecoxarxa']."',categoria='".$categories."',gestor_magatzem='".$mUsuarisRef[$mGrup['usuari_id']]['usuari']."',usuari_id='".$mGrup['usuari_id']."',compte_ecos='".$mGrup['compte_ecos']."',compte_cieb='".$mGrup['compte_cieb']."',email_rebost='".$mGrup['email_rebost']."',productors_associats='".$productorsAssociats."',notes='".$mGrup['notes']."',mobil='".$mGrup['mobil']."',c_ecos='".$mGrup['c_ecos']."' where ref='".$mGrup['id']."' ",$db))
		{
			//echo "<br>  2303 db.php".mysql_errno() . ": " . mysql_error(). "\n";

			return false;
		}
		//per si el responsable encara no pertany al grup, afegirlo:
		db_afegirUsuariAgrup($mGrup['usuari_id'],$mGrup['id'],$db);
		
	}
	else if($opcio=='crearGrup')
	{
		//comprovar si ja existeix el nom del grup:
		$result=mysql_query("select * from rebosts_".$mPars['selRutaSufix']." where nom='".$mGrup['nom']."'",$db);
		$mRow=mysql_fetch_array($result,MYSQL_NUM);
		if(!$mRow)
		{
			if(!$result=mysql_query("select MAX(id) from rebosts_".$mPars['selRutaSufix'],$db))
			{
				//echo "<br>  2594 db.php".mysql_errno() . ": " . mysql_error(). "\n";
				return false;
			}		
			else
			{
				$mRow=mysql_fetch_array($result,MYSQL_NUM);
				while(list($key,$val)=each($mGrup['categoria']))
				{
					$categories.=','.$val;
				}
				reset($mGrup['categoria']);
				
//*v36.6-condicio query
				if(!$result=mysql_query("insert into rebosts_".$mPars['selRutaSufix']." values('','".($mRow[0]+1)."','".$mGrup['nom']."','".$mGrup['adressa']."','".$mGrup['municipiId']."','".$mGrup['bioregio']."','".$mGrup['codi_ecoxarxa']."',',inactiu,".$categories."','','','".$mGrup['compte_ecos']."','".$mGrup['compte_cieb']."','','".$mGrup['email_rebost']."','','','".$mGrup['mobil']."','','','','','','','','','','','','')",$db))
				{
					//echo "<br>  2331 db.php".mysql_errno() . ": " . mysql_error(). "\n";
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	
	return true;
}

//------------------------------------------------------------------------------
function filtrarCategoriesGrupIncompatibles($mCategoriesGrupEnviades)
{
	$mCategoriesGrupFiltrades=array();
	//una sola categoria de cada nivell/prefixe
	while(list($key,$categoria)=each($mCategoriesGrupEnviades))
	{
		$prefixe=substr($categoria,0,strpos($categoria,'-'));
		if(!array_key_exists($prefixe,$mCategoriesGrupFiltrades))
		{
			$mCategoriesGrupFiltrades[$prefixe]=$categoria;
		}
	
	}
	reset($mCategoriesGrupEnviades);
	//nom�s els rebosts poden ser magatzem
	if(@$mCategoriesGrupFiltrades[4]=='4-magatzem' && $mCategoriesGrupFiltrades[1]!='1-rebost')
	{
		unset($mCategoriesGrupFiltrades[4]);
	}
	return $mCategoriesGrupFiltrades;
}



//------------------------------------------------------------------------------
function db_afegirCategoriaGrup($db,$novaCategoria)
{
	global $mCategoriesGrups,$mPars;
	
	if(!$result=mysql_query("select * from parametres_".$mPars['selRutaSufix']." where parametre='categories_grups' AND LOCATE('".$novaCategoria."',valor)>0",$db))
	{
		//echo "<br>  2646 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else		
	{
		$mRow=mysql_fetch_array($result,MYSQL_NUM);
		if(!$mRow)
		{
			if(!$result=mysql_query("update parametres_".$mPars['selRutaSufix']." set valor=CONCAT(valor,',','".$novaCategoria."') where parametre='categories_grups'",$db))
			{
				//echo "<br>  2648 db.php".mysql_errno() . ": " . mysql_error(). "\n";
				return false;
			}
		}
	}
	return true;
}

//------------------------------------------------------------------------------
function db_eliminarGrup($db)
{
	global $mPars,$mGrup,$mPropietatsPeriodesLocals;
	//comprovar si hi ha comanda en curs d'aquest grup
	if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."'",$db))
	{
		//echo "<br>  2661 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else		
	{
		$mRow=mysql_fetch_array($result,MYSQL_NUM);
		if(!$mRow)
		{
			//comprocar si el periode de reserves local esta obert
			//$mPropietatsGrup=@getPropietatsGrup($mGrup['propietats']);
			if($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['comandesLocalsTancades']==1)
			{
				if(!$result=mysql_query("delete from rebosts_".$mPars['selRutaSufix']." where ref='".$mPars['selGrupRef']."'",$db))
				{
					//echo "<br>  2671 db.php".mysql_errno() . ": " . mysql_error(). "\n";
					return false;
				}		
			}
		}
		else
		{
			return false;
		}
	}

	return true;
}

//*v36-12-12-15 modificar funcio
//------------------------------------------------------------------------------
function db_activarGrup($opcio,$db)
{
	global $mPars,$mGrup;
	
	$nouCategoria='';

	if(!$result=mysql_query("select categoria from rebosts_".$mPars['selRutaSufix']." where ref='".$mGrup['id']."'",$db))
	{
		//echo "<br>  2701 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else		
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		if($mRow)
		{
			$nouCategoria=$mRow['categoria'];
			$nouCategoria=str_replace(array(',inactiu,',',actiu,'),'',$nouCategoria);
			$nouCategoria=str_replace(',,',',',$nouCategoria);
			if($opcio==0) //desactivar
			{
				$nouCategoria.=',inactiu,';
			}	
			else if($opcio==1) //activar
			{
				$nouCategoria.=',actiu,';
			}
			if(!$result=mysql_query("update rebosts_".$mPars['selRutaSufix']." set categoria='".$nouCategoria."' where ref='".$mGrup['id']."'",$db))
			{
				//echo "<br>  2671 db.php".mysql_errno() . ": " . mysql_error(). "\n";
				return false;
			}	
		}
		else
		{
			return false;
		}
	}

	return true;
}

//------------------------------------------------------------------------------
function putNouPerfil($opcio,$mPerfil,$db)
{
	global $mPars,$mGrup;
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=true;
	
	if($opcio=='guardarPerfil')
	{
		$mysqlchain='';

		if($mPerfil['projecte']!='')
		{
			$mysqlchain.=",projecte='".$mPerfil['projecte']."'";
		}
		$mysqlchain.=",usuari_id='".$mPerfil['usuari_id']."'";
		$mysqlchain.=",adressa='".$mPerfil['adressa']."'";
		$mysqlchain.=",municipi_id='".$mPerfil['municipi_id']."'";
		$mysqlchain.=",web='".$mPerfil['web']."'";
		$mysqlchain.=",ram='".$mPerfil['ram']."'";
		if($mPerfil['notes']!='')
		{
			$mysqlchain.=",notes='".$mPerfil['notes']."'";
		}
		$mysqlchain.=",descripcio='".$mPerfil['descripcio']."'";
		$mysqlchain.=",avals='".$mPerfil['avals']."'";
		$mysqlchain.=",grup_vinculat='".$mPerfil['grup_vinculat']."'";
		$mysqlchain.=",acord1='".$mPerfil['acord1']."'";
		$mysqlchain.=",zona='".$mPerfil['zona']."'";
		$mysqlchain.=",compte_ecos='".$mPerfil['compte_ecos']."'";

		$mysqlchain=substr($mysqlchain,1);

		//echo "<br>update ".$mPars['taulaProductors']." set ".$mysqlchain." where id='".$mPerfil['id']."' ";
		if(!$result=mysql_query("update ".$mPars['taulaProductors']." set ".$mysqlchain." where id='".$mPerfil['id']."' ",$db))
		{
			//echo "<br>  2658 db.php".mysql_errno() . ": " . mysql_error(). "\n";
			$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No s'ha pogut guardar el perfil</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}
		else
		{
			$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>S'ha guardat el perfil</p>";
			$mMissatgeAlerta['result']=true;
		}
		//afegir el perfil de productor al perfil d'usuari del responsable del perfil
		if(!$result=mysql_query("select perfils_productor from usuaris where id='".$mPerfil['usuari_id']."' ",$db))
		{
			//echo "<br>  2666 db.php".mysql_errno() . ": " . mysql_error(). "\n";
			$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No s'ha trobat l'usuaria (responsable perfil)</p>";
		}
		else
		{
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			
			$perfilsProductor=str_replace(','.$mPerfil['id'].',',',',$mRow['perfils_productor']);
			$perfilsProductor.=','.$mPerfil['id'].',';
			//echo "<br>"."update usuaris set perfils_productor='".$perfilsProductor."' where id='".$mPerfil['usuari_id']."' ";
			if(!$result=mysql_query("update usuaris set perfils_productor='".$perfilsProductor."' where id='".$mPerfil['usuari_id']."' ",$db))
			{
				//echo "<br>  2677 db.php".mysql_errno() . ": " . mysql_error(). "\n";
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No s'ha pogut associar el perfil a la usuaria (responsable perfil)</p>";
			}
		}
	}
	else if($opcio=='crearPerfil')
	{
		if($mPars['selLlistaId']==0)
		{
		
			//comprovar si el grup_vinculat es diferent de cap i ja esta vinculat a un altre perfil
			if(isset($mPerfil['grup_vinculat']) && $mPerfil['grup_vinculat']!='')
			{
				$result=mysql_query("select * from ".$mPars['taulaProductors']." where grup_vinculat='".$mPerfil['grup_vinculat']."'",$db);
				//echo "<br>  2690 db.php".mysql_errno() . ": " . mysql_error(). "\n";
				$mRow=mysql_fetch_array($result,MYSQL_NUM);
	
				if($mRow)
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No es pot vincular el perfil al grup seleccionat (el grup ja est� vinculat a un altre perfil)</p>";
					$mMissatgeAlerta['result']=false;
				
					return $mMissatgeAlerta;
				}
			}

			//comprovar si ja existeix el nom del grup:
			$result=mysql_query("select * from ".$mPars['taulaProductors']." where projecte='".$mPerfil['projecte']."'",$db);
			$mRow=mysql_fetch_array($result,MYSQL_NUM);
			if(!$mRow)
			{
				//registre historial perfil
				$registre='';
				$mGC['usuari_id']=$mPars['usuari_id'];
	
				while(list($key,$val)=each($mGC))
				{
					$registre.='{p:crearPerfil;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
				}
				reset($mGC);
				
				$mRow=mysql_fetch_array($result,MYSQL_NUM);
				//echo "<br>insert into ".$mPars['taulaProductors']." values('','".($mRow[0]+1)."','".$mPerfil['projecte']."','".$mPerfil['usuari_id']."','".$mPerfil['adressa']."','".$mPerfil['municipi_id']."','".$mPerfil['web']."','".$mPerfil['ram']."','".$mPerfil['notes']."','".$mPerfil['descripcio']."','".$mPerfil['avals']."','".$mPerfil['grup_vinculat']."','1','".$mPerfil['acord1']."','".$mPerfil['zona']."','".$registre."','".$mPerfil['compte_ecos']."')";
				if(!$result=mysql_query("insert into ".$mPars['taulaProductors']." values('','".($mRow[0]+1)."','".$mPerfil['projecte']."','".$mPerfil['usuari_id']."','".$mPerfil['adressa']."','".$mPerfil['municipi_id']."','".$mPerfil['web']."','".$mPerfil['ram']."','".$mPerfil['notes']."','".$mPerfil['descripcio']."','".$mPerfil['avals']."','".$mPerfil['grup_vinculat']."','0','".$mPerfil['acord1']."','".$mPerfil['zona']."','','".$registre."','".$mPerfil['compte_ecos']."')",$db))
				{
					//echo "<br>  4512 db.php".mysql_errno() . ": " . mysql_error(). "\n";
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No s'ha pogut crear el nou perfil</p>";
					$mMissatgeAlerta['result']=false;
				
					return $mMissatgeAlerta;
				}
			}
			else
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>Ja existeix un altre perfil amb aquest nom</p>";
				$mMissatgeAlerta['result']=false;
			
				return $mMissatgeAlerta;
			}
		}
		else
		{
			//registre historial perfil
			$registre='';
			$mGC['usuari_id']=$mPars['usuari_id'];
	
			while(list($key,$val)=each($mGC))
			{
				$registre.='{p:crearPerfil;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
			}
			reset($mGC);
		
			//comprovar si ja existeix el nom del grup:
			$result=mysql_query("select * from ".$mPars['taulaProductors']." where projecte='".$mPerfil['projecte']."'",$db);
			$mRow=mysql_fetch_array($result,MYSQL_NUM);
			if(!$mRow)
			{
				$mRow=mysql_fetch_array($result,MYSQL_NUM);
				//echo "<br>insert into ".$mPars['taulaProductors']." values('','".($mRow[0]+1)."','".$mPerfil['projecte']."','".$mPerfil['usuari_id']."','".$mPerfil['adressa']."','".$mPerfil['municipi_id']."','".$mPerfil['web']."','".$mPerfil['ram']."','".$mPerfil['notes']."','".$mPerfil['descripcio']."','".$mPerfil['avals']."','".$mPerfil['grup_vinculat']."','1','".$mPerfil['acord1']."','".$mPerfil['zona']."','','".$registre."')";
				if(!$result=mysql_query("insert into ".$mPars['taulaProductors']." values('','".($mRow[0]+1)."','".$mPerfil['projecte']."','".$mPerfil['usuari_id']."','".$mPerfil['adressa']."','".$mPerfil['municipi_id']."','".$mPerfil['web']."','".$mPerfil['ram']."','".$mPerfil['notes']."','".$mPerfil['descripcio']."','".$mPerfil['avals']."','".$mPerfil['grup_vinculat']."','1','".$mPerfil['acord1']."','".$mPerfil['zona']."','','".$registre."','".$mPerfil['compte_ecos']."')",$db))
				{
					//echo "<br>  2714 db.php".mysql_errno() . ": " . mysql_error(). "\n";
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No s'ha pogut crear el nou perfil</p>";
					$mMissatgeAlerta['result']=false;
				
					return $mMissatgeAlerta;
				}
			}
			else
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>Ja existeix un altre perfil amb aquest nom</p>";
				$mMissatgeAlerta['result']=false;
			
				return $mMissatgeAlerta;
			}
		}
	
		//afegir el perfil de productor al perfil d'usuari del responsable del perfil
		if(!$result=mysql_query("select id from ".$mPars['taulaProductors']." where projecte='".$mPerfil['projecte']."' ",$db))
		{
			//echo "<br>  2666 db.php".mysql_errno() . ": " . mysql_error(). "\n";
			$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No s'ha trobat el perfil </p>";
		}
		else
		{
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			
			if(!$result=mysql_query("select perfils_productor from usuaris where id='".$mPerfil['usuari_id']."' ",$db))
			{
				//echo "<br>  2666 db.php".mysql_errno() . ": " . mysql_error(). "\n";
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No s'ha trobat l'usuaria (responsable perfil)</p>";
			}
			else
			{
				$mRow2=mysql_fetch_array($result,MYSQL_ASSOC);
				$perfilsProductor=str_replace(','.$mRow['id'].',',',',$mRow2['perfils_productor']);
				$perfilsProductor.=','.$mRow['id'].',';
				//echo "<br>"."update usuaris set perfils_productor='".$perfilsProductor."' where id='".$mPerfil['usuari_id']."' ";
				if(!$result=mysql_query("update usuaris set perfils_productor='".$perfilsProductor."' where id='".$mPerfil['usuari_id']."' ",$db))
				{
					//echo "<br>  2677 db.php".mysql_errno() . ": " . mysql_error(). "\n";
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No s'ha pogut associar el perfil a la usuaria (responsable perfil)</p>";
				}
			}
		}


		if($mPars['grup_id']!=0)
		{
			//associar el perfil de productor al grup des del que es crea

			$perfilsProductor=str_replace(','.$mRow['id'].',',',',$mGrup['productors_associats']);
			$perfilsProductor.=','.$mRow['id'].',';
			//echo "<br>update rebosts_".$mPars['selRutaSufix']." set productors_associats='".$perfilsProductor."' where id='".$mGrup['id']."' ";
			if(!$result=mysql_query("update rebosts_".$mPars['selRutaSufix']." set productors_associats='".$perfilsProductor."' where id='".$mGrup['id']."' ",$db))
			{
				//echo "<br>  4350 db.php".mysql_errno() . ": " . mysql_error(). "\n";
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>No s'ha pogut associar el perfil al grup</p>";
			}
		}
	}
	
	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function db_activarPerfil($opcio,$db)
{
	global $mPars;
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']="";
	$mMissatgeAlerta['result']=true;

	if($opcio*1==0)
	{
		//comprovar si hi ha algun producte actiu d'aquest perfil de productor
		//echo "<br>select * from ".$mPars['taulaProductes']." where SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPars['selPerfilRef']."' AND actiu='1'";
		$result=mysql_query("select * from ".$mPars['taulaProductes']." where SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPars['selPerfilRef']."' AND actiu='1'",$db);
		//echo "<br>  2575 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		if($mRow)
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo4'>Atenci�: no s'ha pogut desactivar el Perfil perqu� t� algun producte actiu en aquest periode</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}
		else
		{
			//echo "<br>update ".$mPars['taulaProductors']." set estat='0' where id='".$mPars['selPerfilRef']."'";
			if(!$result=mysql_query("update ".$mPars['taulaProductors']." set estat='0' where id='".$mPars['selPerfilRef']."'",$db))
			{
				//echo "<br>  2586 db.php".mysql_errno() . ": " . mysql_error(). "\n";

				$mMissatgeAlerta['missatge']="<p class='pAlertaNo4'>Atenci�: no s'ha pogut desactivar el Perfil (error de connexi� amb bbdd)</p>";
				$mMissatgeAlerta['result']=false;
				
				return $mMissatgeAlerta;
			}
		}
		$mMissatgeAlerta['missatge']="<p class='pAlertaOk4'>S'ha desactivat el Perfil</p>";
		$mMissatgeAlerta['result']=true;
						
		return $mMissatgeAlerta;
		
	}
	else
	{
		if(!$result=mysql_query("update ".$mPars['taulaProductors']." set estat='1' where id='".$mPars['selPerfilRef']."'",$db))
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo4'>Atenci�: no s'ha pogut activar el Perfil (error de connexi� amb bbdd)</p>";
			$mMissatgeAlerta['result']=false;
				
			return $mMissatgeAlerta;
		}

		$mMissatgeAlerta['missatge']="<p class='pAlertaOk4'>S'ha activat el Perfil</p>";
		$mMissatgeAlerta['result']=true;
						
		return $mMissatgeAlerta;
	}

	return true;
}

//------------------------------------------------------------------------------
function db_activarPerfilLocal($opcio,$db)
{
	global $mPars,$mPropietatsGrup;

	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']="";
	$mMissatgeAlerta['result']=true;

	$mPropietatsGrup['idsPerfilsActiusLocal']=str_replace(','.$mPars['selPerfilRef'].',','',$mPropietatsGrup['idsPerfilsActiusLocal']);
	if($opcio*1==0)
	{
		$propietats=makePropietatsGrup($mPropietatsGrup);
		$activarText='desactivat';
	}
	else
	{
		$mPropietatsGrup['idsPerfilsActiusLocal'].=','.$mPars['selPerfilRef'].',';
		$propietats=makePropietatsGrup($mPropietatsGrup);
		$activarText='activat';
	}

	//echo "<br>update rebosts_".$mPars['selRutaSufix']." set propietats='".$propietats."' WHERE id='".$mPars['grup_id']."'";
	if(!$result=mysql_query("update rebosts_".$mPars['selRutaSufix']." set propietats='".$propietats."' WHERE id='".$mPars['grup_id']."'",$db))
	{
		//echo "<br>  2575 db.php".mysql_errno() . ": " . mysql_error(). "\n";

		$mMissatgeAlerta['missatge']="<p class='pAlertaNo4'>Atenci�: No s'ha ".$activarText." el Perfil (error de connexi� bbdd)</p>";
		$mMissatgeAlerta['result']=true;
						
		return $mMissatgeAlerta;
	}

	$mMissatgeAlerta['missatge']="<p class='pAlertaOk4'>S'ha ".$activarText." el Perfil</p>";
	$mMissatgeAlerta['result']=true;
						
	return $mMissatgeAlerta;

}
//------------------------------------------------------------------------------
function db_getUsuari($usuariId,$db)
{
	$result=mysql_query("select * from usuaris where id='".$usuariId."'",$db);
	//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	
	$mUsuari=mysql_fetch_array($result,MYSQL_ASSOC);

	return $mUsuari;
}

//------------------------------------------------------------------------------
function db_getUsuaris($db)
{
	global $mPars;
	
	$mUsuaris=array();
	$result='';
	
	if($mPars['nivell']=='sadmin')
	{
		$result=mysql_query("select * from usuaris order by usuari,email ASC",$db);
		//echo "<br>  4772 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else if($mPars['nivell']=='admin')
	{
		$result=mysql_query("select * from usuaris where nivell='coord' OR  nivell='usuari' order by usuari,email ASC",$db);
		//echo "<br>  4777 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else if($mPars['nivell']=='coord')
	{
		$result=mysql_query("select * from usuaris where nivell='usuari' order by usuari,email ASC",$db);
		//echo "<br>  4782 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	$i=0;
	
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mUsuaris[$i]=$mRow;
			$i++;
		}
	}
	
	return $mUsuaris;
}

//------------------------------------------------------------------------------
function db_getUsuarisRef($db)
{
	global $mPars;
	
	$mUsuarisRef=array();
	$cadenaUsuarisGrupId='';
	
	$result='';
	
	$i=0;
		$result=mysql_query("select * from usuaris order by usuari,email ASC",$db);
		//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mUsuarisRef[$mRow['id']]=$mRow;
			$i++;
			$mUsuarisRef[$mRow['id']]['comanda']=array();
			$cadenaUsuarisGrupId.=','.$mRow['id'].',';
		}
		//$cadenaUsuarisGrupId=str_replace(',,','',$cadenaUsuarisGrupId);

		$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where LOCATE(usuari_id,' ".$cadenaUsuarisGrupId."')!=0 AND usuari_id!='0' order by id ASC",$db);
		//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		if($result)
		{
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
				$mUsuarisRef[$mRow['usuari_id']]['comanda'][$grupId]=$mRow;
			}
		}
	}
	
	return $mUsuarisRef;
}

//------------------------------------------------------------------------------
function db_getUsuarisComissionsRef($db)
{
	global $mPars;
	
	$mUsuarisRef=array();
	$cadenaUsuarisGrupId='';
	
	$result='';
	
	if(!$result=mysql_query("select * from usuaris where LOCATE('comissio',CONCAT(' ',propietats))>0 order by id ASC",$db))
	{
		echo "<br>  3273 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mUsuarisRef[$mRow['id']]=$mRow;
		}
	}
	
	return $mUsuarisRef;
}
//------------------------------------------------------------------------------
function db_getResponsablesGrups($db)
{
	global $mPars;
	
	$mResponsablesGrup=array();
	
	$result=mysql_query("select DISTINCT(usuari_id) from rebosts_".$mPars['selRutaSufix']." order by id ASC",$db);
	//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";

	$i=0;
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mResponsablesGrup[$mRow['usuari_id']]=$mRow;
			$i++;
		}
	}

	return $mResponsablesGrup;
}
//------------------------------------------------------------------------------
function db_getResponsablesPerfils($db)
{
	global $mPars;
	
	$mResponsablesPerfil=array();
	
	$result=mysql_query("select usuari_id from ".$mPars['taulaProductors']." order by id ASC",$db);
	//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";

	$i=0;
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mResponsablesPerfil[$mRow['usuari_id']]=$mRow;
			$i++;
		}
	}
	return $mResponsablesPerfil;
}
//------------------------------------------------------------------------------
function db_getIdGrupsUsuariResponsableChain($db)
{
	global $mPars;
	
	$idGrupsUsuariResponsableChain='';
	
	$result=mysql_query("select id from rebosts_".$mPars['selRutaSufix']." WHERE usuari_id='".$mPars['usuari_id']."' order by id ASC",$db);
	//echo "<br>  4887 db.php".mysql_errno() . ": " . mysql_error(). "\n";

	$i=0;
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$idGrupsUsuariResponsableChain.=$mRow['id'].',';
		}
		$idGrupsUsuariResponsableChain=substr($idGrupsUsuariResponsableChain,0,strlen($idGrupsUsuariResponsableChain)-1);
	}
	return $idGrupsUsuariResponsableChain;
}
//------------------------------------------------------------------------------
function db_getIdPerfilsUsuariResponsableChain($db)
{
	global $mPars;
	
	$idPerfilsUsuariResponsableChain='';
	
	$result=mysql_query("select id from ".$mPars['taulaProductors']." WHERE usuari_id='".$mPars['usuari_id']."' order by id ASC",$db);
	//echo "<br>  4905 db.php".mysql_errno() . ": " . mysql_error(). "\n";

	$i=0;
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$idPerfilsUsuariResponsableChain.=$mRow['id'].',';
		}
		$idPerfilsUsuariResponsableChain=substr($idPerfilsUsuariResponsableChain,0,strlen($idPerfilsUsuariResponsableChain)-1);
	}
	return $idPerfilsUsuariResponsableChain;
}
//------------------------------------------------------------------------------
function db_getDEMO_IdGrupsUsuariResponsableChain($db)
{
	global $mPars;
	
	$DEMO_idGrupsUsuariResponsableChain='';
	
	$result=mysql_query("select id from rebosts_".$mPars['selRutaSufix']." WHERE usuari_id='".$mPars['usuari_id']."' order by id ASC",$db);
	//echo "<br>  4887 db.php".mysql_errno() . ": " . mysql_error(). "\n";

	$i=0;
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$DEMO_idGrupsUsuariResponsableChain.=$mRow['id'].',';
		}
		$DEMO_idGrupsUsuariResponsableChain=substr($DEMO_idGrupsUsuariResponsableChain,0,strlen($DEMO_idGrupsUsuariResponsableChain)-1);
	}
	return $DEMO_idGrupsUsuariResponsableChain;
}
//------------------------------------------------------------------------------
function db_getDEMO_IdPerfilsUsuariResponsableChain($db)
{
	global $mPars;
	
	$DEMO_idPerfilsUsuariResponsableChain='';
	
	$result=mysql_query("select id from ".$mPars['taulaProductors']." WHERE usuari_id='".$mPars['usuari_id']."' order by id ASC",$db);
	//echo "<br>  4905 db.php".mysql_errno() . ": " . mysql_error(). "\n";

	$i=0;
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$DEMO_idPerfilsUsuariResponsableChain.=$mRow['id'].',';
		}
		$DEMO_idPerfilsUsuariResponsableChain=substr($DEMO_idPerfilsUsuariResponsableChain,0,strlen($DEMO_idPerfilsUsuariResponsableChain)-1);
	}
	return $DEMO_idPerfilsUsuariResponsableChain;
}/*
//------------------------------------------------------------------------------
function db_getResponsablesPerfilsActius($db)
{
	global $mPars;
	
	$mResponsablesPerfil=array();
	
	$result=mysql_query("select usuari_id from ".$mPars['taulaProductors']." where estat='1' order by id ASC",$db);
	//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";

	$i=0;
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mResponsablesPerfil[$mRow['usuari_id']]=$mRow;
			$i++;
		}
	}
	return $mResponsablesPerfil;
}
*/
//------------------------------------------------------------------------------
function db_getUsuarisRefGrups($db)
{
	global $mPars,$mSelGrups,$mResponsablesPerfils,$mResponsablesGrups;

	//usat per email.php
	//envia matriu amb usuaris dels grups presents a $mSelGrups;
	$mUsuarisRef=array();
	$cadenaGrups='';
	$cadenaUsuarisRefGrups='';
	$locate='';
	$substring='';
	while(list($index,$grupId)=each($mSelGrups))
	{
		if($grupId!='')
		{
			$locate.=" OR LOCATE(',".$grupId.",',CONCAT(' ',grups))>0";
			$substring.=" OR SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$grupId."'";
		}
	}
	$locate=substr($locate,3);
	$substring=substr($substring,3);
	
	$result='';
	
	$i=0;
	$result=mysql_query("select * from usuaris where ".$locate." order by usuari,email ASC",$db);
	//echo "<br>select * from usuaris where ".$locate." order by usuari,email ASC";
	//echo "<br>  3196  db.php".mysql_errno() . ": " . mysql_error(). "\n";
	
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mUsuarisRef[$mRow['id']]=$mRow;
			$mUsuarisRef[$mRow['id']]['comanda']=array();
				if(array_key_exists($mRow['id'],$mResponsablesPerfils))
				{
					$mUsuarisRef[$mRow['id']]['responsablePerfil']=1;
				}
				else
				{
					$mUsuarisRef[$mRow['id']]['responsablePerfil']=0;
				}
				
				if(array_key_exists($mRow['id'],$mResponsablesGrups))
				{
					$mUsuarisRef[$mRow['id']]['responsableGrup']=1;
				}
				else
				{
					$mUsuarisRef[$mRow['id']]['responsableGrup']=0;
				}
			$cadenaUsuarisRefGrups.=','.$mRow['id'].',';
			$i++;
		}
	}

	$result=mysql_query("select * from ".$mPars['taulaComandes']." where LOCATE(CONCAT(',',usuari_id,','),' ".$cadenaUsuarisRefGrups."')!=0 AND usuari_id!='0' AND (".$substring.") AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='0' AND llista_id='".$mPars['selLlistaId']."' order by id ASC",$db);
	//echo "<br>  3210 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	//echo "<br>select * from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',usuari_id,','),' ".$cadenaUsuarisRefGrups."')!=0 AND usuari_id!='0' AND (".$substring.") AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='0' order by id ASC";
	if($result)
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mRow['usuari_id']!='0')
			{
				$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
				$mUsuarisRef[$mRow['usuari_id']]['comanda'][$grupId]=$mRow;
			}
		}
	}
	return $mUsuarisRef;
}

//------------------------------------------------------------------------------
function db_getUsuarisGrup($db)
{
	global $mPars;

	$mUsuaris=array();
	$result='';
	$i=0;
	if($mPars['grup_id']!=0)
	{
		$result=mysql_query("select * from usuaris where LOCATE(',".$mPars['grup_id'].",',CONCAT(' ',grups))!=0 order by usuari,email ASC",$db);
		//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";

		if($result)
		{
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mUsuaris[$i]=$mRow;
				$i++;
			}
		}
	}
	return $mUsuaris;
}

//------------------------------------------------------------------------------
function db_getUsuarisGrupRef($db)
{
	global $mPars;
	
	$mUsuarisGrupRef=array();
	$cadenaUsuarisGrupId='';
	
	$result='';
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
	{
		$mysqlUsuarisActius="";
	}
	else
	{
		$mysqlUsuarisActius=" AND estat='actiu' ";
	}
	
	if($mPars['grup_id']!=0)
	{
		$result=mysql_query("select * from usuaris where LOCATE(',".$mPars['grup_id'].",',CONCAT(' ',grups))!=0 ".$mysqlUsuarisActius." order by email ASC",$db);
		//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";

		if($result)
		{
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mUsuarisGrupRef[$mRow['id']]['usuari']=$mRow;
				if(!isset($mUsuarisGrupRef[$mRow['id']]['comanda'])){$mUsuarisGrupRef[$mRow['id']]['comanda']=array();}
				$cadenaUsuarisGrupId.=','.$mRow['id'].',';
			}
		}
		//$cadenaUsuarisGrupId=str_replace(',,','',$cadenaUsuarisGrupId);

		if($mPars['selLlistaId']==0)
		{
			$result=mysql_query("select * from ".$mPars['taulaComandes']." where LOCATE(CONCAT(',',usuari_id,','),' ".$cadenaUsuarisGrupId."')!=0 AND usuari_id!='0' AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mPars['grup_id']."' order by id ASC",$db);
			//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		}
		else
		{
			$result=mysql_query("select * from ".$mPars['taulaComandes']." where LOCATE(CONCAT(',',usuari_id,','),' ".$cadenaUsuarisGrupId."')!=0 AND usuari_id!='0' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' AND llista_id='".$mPars['selLlistaId']."' order by id ASC",$db);
			//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		}
		if($result)
		{
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
				$mUsuarisGrupRef[$mRow['usuari_id']]['comanda'][$grupId]=$mRow;
			}
		}

	}

	return $mUsuarisGrupRef;
}

//------------------------------------------------------------------------------
function db_getUsuarisGrupRef2($db)
{
	global $mPars;
	
	$mUsuarisGrupRef=array();
	
	$result='';
	if($mPars['grup_id']!=0)
	{
		$result=mysql_query("select * from usuaris where LOCATE(',".$mPars['grup_id'].",',CONCAT(' ',grups))!=0  order by email ASC",$db);
		//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";

		if($result)
		{
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mUsuarisGrupRef[$mRow['id']]=$mRow;
			}
		}
	}

	return $mUsuarisGrupRef;
}
//------------------------------------------------------------------------------
function db_getAnticsUsuarisGrupAmbComanda($db)
{
	global $mPars,$mUsuarisGrupRef,$mUsuarisRef;
	
	$mAnticsUsuarisGrupAmbComanda=array();
	$mUsuarisAmbComanda=array()	;
	
	$result='';
	if($mPars['grup_id']!=0)
	{

		$result=mysql_query("select usuari_id from ".$mPars['taulaComandes']." where SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mPars['grup_id']."' AND usuari_id!='0' order by id ASC",$db);
		//echo "<br>  2469 db.php".mysql_errno() . ": " . mysql_error(). "\n";
		if($result)
		{
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				if(!array_key_exists($mRow['usuari_id'],$mUsuarisGrupRef))
				{
					$mAnticsUsuarisGrupAmbComanda[$mRow['usuari_id']]=array();
					$mAnticsUsuarisGrupAmbComanda[$mRow['usuari_id']]['comanda']=$mRow;
					$mAnticsUsuarisGrupAmbComanda[$mRow['usuari_id']]['usuari']=$mUsuarisRef[$mRow['usuari_id']];
				}
			}
		}
	}

	return $mAnticsUsuarisGrupAmbComanda;
}
//------------------------------------------------------------------------------
function db_getPuntEntrega($grupId,$db)
{
	global $mPars;
	
	$result=mysql_query("select  punt_entrega from comandes_".$mPars['selRutaSufix']." WHERE SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$grupId."' AND usuari_id='0'",$db);
	//echo "<br>  2478 db.php".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

	if($mRow['punt_entrega']==''){$mRow['punt_entrega']='no_assignat'; }
	
	
	return $mRow['punt_entrega'];
}

//------------------------------------------------------------------------------
function db_afegirUsuariAgrup($usuariId,$grupId,$db)			
{
	//netejar:
	if(!$result=mysql_query("update usuaris set grups=REPLACE(grups,',".$grupId.",',','), grups=REPLACE(grups,',,',',') where id='".$usuariId."' ",$db))
	{
		//echo "<br>  73 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	//afegir al grup:
	if(!$result=mysql_query("update usuaris set grups=CONCAT(grups,',','".$grupId."',','), estat='actiu' WHERE id='".$usuariId."'",$db))
	{
		//echo "<br>  79 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
function db_getAbonamentsCarrecs($grupId,$db)
{
	global $mPars,$cadenaIdsMembresGrup;
	
	$missatgeAlerta='';
	$mAbonamentsCarrecs=array();
	$mAbonamentsCarrecs['cac']=array();
	$mAbonamentsCarrecs['grup']=array();
	$mAbonamentsCarrecs['usuari']=array();
	$i=0;
	if($mPars['selLlistaId']=='0') //CAC
	{
		if($mPars['grup_id']=='0') //CAC
		{
			//echo "<br>select * from ".$mPars['taulaIncidencies']." WHERE ruta='".$mPars['selRutaSufix']."' AND grup_id='0' AND estat='aplicat' order by id ASC";
			$result=mysql_query("select * from ".$mPars['taulaIncidencies']." WHERE ruta='".$mPars['selRutaSufix']."' AND grup_id='0' AND estat='aplicat' order by id ASC",$db);
			//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mAbonamentsCarrecs['cac'][$i]=$mRow;
				$i++;
			}
		}
		else
		{
			//echo "<br>select * from ".$mPars['taulaIncidencies']." WHERE ruta='".$mPars['selRutaSufix']."' AND grup_id='".$mPars['grup_id']."' AND estat=='aplicat' order by id ASC";
			$result=mysql_query("select * from ".$mPars['taulaIncidencies']." WHERE ruta='".$mPars['selRutaSufix']."' AND grup_id='".$mPars['grup_id']."' AND estat='aplicat' order by id ASC",$db);
			//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mAbonamentsCarrecs['cac'][$i]=$mRow;
				$i++;
			}
		}
	}
	else //GRUP
	{
		//echo "<br> select * from ".$mPars['taulaIncidencies']." WHERE ruta='".$mPars['sel_periode_comanda_local']."' AND llista_id='".$mPars['selLlistaId']."'  AND ( tipus='abonamentGrup' || tipus='carrecGrup') and estat='aplicat'  order by id ASC";
		$result=mysql_query("select * from ".$mPars['taulaIncidencies']." WHERE ruta='".$mPars['sel_periode_comanda_local']."' AND llista_id='".$mPars['selLlistaId']."'  AND ( tipus='abonamentGrup' || tipus='carrecGrup') and estat='aplicat'  order by id ASC",$db);
		//echo   " 5174 db.php <br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mAbonamentsCarrecs['grup'][$i]=$mRow;
			$i++;
		}
	}

	return $mAbonamentsCarrecs;
}

//------------------------------------------------------------------------------
function selUsuariEsProductorAssociatDelGrup($db)
{
	global $mPars,$mSelUsuari,$mGrup;

	$perfilsIdChain='';
	
	$mPerfilsProductorUsuari=explode(',',$mSelUsuari['perfils_productor']);
	$mPerfilsProductorGrup=@explode(',',$mGrup['productors_associats']);
	
	while(list($key,$id)=each($mPerfilsProductorUsuari))
	{
		if($id!=',' && $id!='' && $id!=' ' && $id!='0')
		{
			while(list($key,$id2)=each($mPerfilsProductorGrup))
			{
				if($id==$id2)
				{
					$perfilsIdChain.=','.$id;
				}
			}	
			reset($mPerfilsProductorGrup);
		}
	}
	reset($mPerfilsProductorUsuari);

	if($perfilsIdChain!=''){$perfilsIdChain=$perfilsIdChain.',';return $perfilsIdChain;}else{return false;}
	
	return false;
}

//------------------------------------------------------------------------------
function usuariEsRespGProductorDelProducte($db)
{
	global $mPars,$mUsuari,$mGrup,$mProducte;

	$mPerfilsProductorUsuari=explode(',',$mUsuari['perfils_productor']);
	$mPerfilsProductorGrup=@explode(',',$mGrup['productors_associats']);
	while(list($key,$id)=each($mPerfilsProductorUsuari))
	{
		if($id!=',' && $id!='' && $id!=' ' && $id!='0')
		{
			while(list($key,$id2)=each($mPerfilsProductorGrup))
			{
				if($id==$id2)
				{
					//si es el productor del producte
					if(substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))==$id)
					{
						return $id;
					}
				}
			}	
			reset($mPerfilsProductorGrup);
		}
	}
	reset($mPerfilsProductorUsuari);

	return false;
}

//------------------------------------------------------------------------------
function getUnitatsReservades($db)
{
	global $mPars;
	
	$unitatsReservades=0;
	
	//echo "<br>select SUBSTRING(resum,LOCATE('producte_".$mPars['selProducteId'].":',resum)),rebost from ".$mPars['taulaComandes']." where rebost!='0-CAC' AND LOCATE('producte_".$mPars['selProducteId'].":',CONCAT(' ',resum))>0 AND llista_id='".$mPars['selLlistaId']."' order by id ASC";
	$result=mysql_query("select SUBSTRING(resum,LOCATE('producte_".$mPars['selProducteId'].":',resum)),rebost from ".$mPars['taulaComandes']." where rebost!='0-CAC' AND LOCATE('producte_".$mPars['selProducteId'].":',CONCAT(' ',resum))>0 AND llista_id='".$mPars['selLlistaId']."' order by id ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";

	while($mRow=mysql_fetch_array($result,MYSQL_NUM))
	{
		$quantitat=substr($mRow[0],strpos($mRow[0],':')+1);
		$quantitat=substr($quantitat,0,strpos($quantitat,';'));
		if($quantitat*1>0)
		{
			$unitatsReservades+=$quantitat;
		}
	}
	return $unitatsReservades;
}

//------------------------------------------------------------------------------
function db_getUnitatsReservadesLocal($db)
{
	global $mPars;
	
	$unitatsReservades=0;
	
	//echo "<br>select SUBSTRING(resum,LOCATE('producte_".$mPars['selProducteId'].":',resum)),rebost from ".$mPars['taulaComandes']." where rebost!='0-CAC' AND LOCATE('producte_".$mPars['selProducteId'].":',CONCAT(' ',resum))>0 AND llista_id='".$mPars['selLlistaId']."'  AND periode_comanda='".$mPars['sel_periode_comanda_local']."' order by id ASC";
	$result=mysql_query("select SUBSTRING(resum,LOCATE('producte_".$mPars['selProducteId'].":',resum)),rebost from ".$mPars['taulaComandes']." where LOCATE('producte_".$mPars['selProducteId'].":',CONCAT(' ',resum))>0 AND usuari_id!=0 AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' order by id ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";

	while($mRow=mysql_fetch_array($result,MYSQL_NUM))
	{
		$quantitat=substr($mRow[0],strpos($mRow[0],':')+1);
		$quantitat=substr($quantitat,0,strpos($quantitat,';'));
		if($quantitat*1>0)
		{
			$unitatsReservades+=$quantitat;
		}
	}
	return $unitatsReservades;
}

//------------------------------------------------------------------------------
function db_getZonaGrup($grupId,$db)
{
	global $mPars;
	
	$zona='';
	$result=mysql_query("select categoria from rebosts_".$mPars['selRutaSufix']." where id='".$grupId."' order by id ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";

	$mRow=mysql_fetch_array($result,MYSQL_NUM);
	if(substr_count(' '.$mRow[0],',2-')>0)
	{
		$zona=substr($mRow[0],strpos($mRow[0],',2-')+3);
		$zona=substr($zona,0,strpos($zona,','));
	}
	return $zona;
}

//------------------------------------------------------------------------------
function db_getZonaPerfil($perfilId,$db)
{
	global $mPars;
		
	$result=mysql_query("select zona from ".$mPars['taulaProductors']." where id='".$perfilId."' order by id ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";

	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

	return $mRow['zona'];
}
//------------------------------------------------------------------------------
function getGrupsZones($cadenaGrupsZones)
{
	$mGrupsZones=array();
	$mGrups=explode(';',$cadenaGrupsZones);

	while(list($nomSZ,$cadenaZones)=each($mGrups))
	{
		$nomSZ=substr($cadenaZones,0,strpos($cadenaZones,':'));
		$cadenaZones=substr($cadenaZones,strpos($cadenaZones,':')+1);
		$mZones=explode(',',$cadenaZones);
		if($nomSZ!='') 
		{
			$mGrupsZones[$nomSZ]=$mZones;
		}
	}
	reset($mGrups);

	reset($mGrupsZones);
	
	return $mGrupsZones;
}

//------------------------------------------------------------------------------
function getUnitatsReservadesPerZona($producteId,$db)
{
	global $mPars;
	
	$mUnitatsReservades=array();
	
	$result=mysql_query("select rebost,SUBSTRING(resum,LOCATE('producte_".$producteId.":',resum)) from comandes_".$mPars['selRutaSufix']." where rebost!='0-CAC' AND LOCATE('producte_".$producteId.":',CONCAT(' ',resum))>0 order by id ASC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";

	while($mRow=mysql_fetch_array($result,MYSQL_NUM))
	{
		$grupId=substr($mRow[0],0,strpos($mRow[0],'-'));
		$puntEntregaId=db_getPuntEntrega($grupId,$db);
		
		if($puntEntregaId!='no-assignat'){$zona=db_getZonaGrup($puntEntregaId,$db);}else{$zona='no-assignada';}
		
		if(!isset($mUnitatsReservades[$zona])){$mUnitatsReservades[$zona]=0;}

		$quantitat=substr($mRow[1],strpos($mRow[1],':')+1);
		$quantitat=substr($quantitat,0,strpos($quantitat,';'));
		if($quantitat*1>0)
		{
			$mUnitatsReservades[$zona]+=$quantitat;
		}
	}

	return $mUnitatsReservades;
}

//------------------------------------------------------------------------------
function db_getReservesPuntEntrega($puntEntregaId,$db)
{
	global $mPars,$rc,$mProductes,$mReservesComandes,$jaEntregatTuts,$jaEntregatTkg,$mGrupsZones,$mPerfilsRef,$sz;
	
		$grupId_=$mPars['grup_id'];
		$mPars['grup_id']=$puntEntregaId;
	$mGrup=getGrup($db);
		$mPars['grup_id']=$grupId_;
	
	$mReservesProductes=array();
	$grupsEntreguen='';
	
	//get grups amb punts entrega en els grups anteriors
	if(!$result=mysql_query("select rebost,punt_entrega from comandes_".$mPars['selRutaSufix']." where punt_entrega='".$puntEntregaId."'",$db))
	{
		//echo "<br> 159 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			$grupsEntreguen.=','.$grupId.',';
			$mComandesGrups[$grupId]['punt_entrega']=$mRow['punt_entrega'];
		}
	}
	$grupsEntreguen=str_replace(',,',',',$grupsEntreguen);
	
	//getComandes dels usuaris dels grups dels puntsentrega dins zona
	$do=true;
	if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',SUBSTRING_INDEX(rebost,'-',1),','),CONCAT(' ','".$grupsEntreguen."'))>0 AND usuari_id!=0",$db))
	{
		echo "<br> 5551 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			if($rc=='1')
			{
				if(substr_count($mGrup['recepcions'],$grupId)>0)
				{
					$do=true;
				}
				else
				{
					$do=false;
				}
			}

				if(!isset($mReservesComandes[$mComandesGrups[$grupId]['punt_entrega']]))
				{
					$mReservesComandes[$mComandesGrups[$grupId]['punt_entrega']]=array();
				}
				if(!isset($mReservesComandes[$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]))
				{
					$mReservesComandes[$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]=array();
				}
				$mReservesComandes[$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]['resum']=$mRow['resum'];
	
			if($do)				
			{
				$mComanda=explode(';',$mRow['resum']);
				while(list($key,$mVal)=each($mComanda))
				{
					$mIndexQuantitat=explode(':',$mVal);
					$index=str_replace('producte_','',$mIndexQuantitat[0]);
					$do2=true;
					if($sz!='')
					{
						if($index!='')
						{
							$productorId=substr($mProductes[$index]['productor'],0,strpos($mProductes[$index]['productor'],'-'));

							$superZonaProductor=getSuperZona($mPerfilsRef[$productorId]['zona']);
							if($superZonaProductor==$sz)
							{
								$do2=true;
							}
							else
							{
								$do2=false;
							}
						}
						else
						{
							$do2=false;
						}
					}
					else
					{
						$do2=true;
					}
					
					
					if($do2)
					{
						$quantitat=@$mIndexQuantitat[1];
						if($index!='')
						{
							if(array_key_exists($index,$mReservesProductes))
							{
								$mReservesProductes[$index]+=$quantitat;
							}
							else
							{
								$mReservesProductes[$index]=$quantitat;
							}
						}
					}
				}
			}
		}
	
		if($mPars['excloureProductesJaEntregats']=='1')
		{
			$mProductesJaEntregats=db_getProductesJaEntregats($db);
			while(list($index,$mVal)=each($mProductesJaEntregats))
			{
				while(list($grupId,$mVal2)=each($mVal))
				{
					if(substr_count($grupsEntreguen,','.$grupId.',')>0)
					{
						@$mReservesProductes[$index]-=$mVal2['rebut'];
						$jaEntregatTkg+=$mVal2['rebut']*$mProductes[$index]['pes'];
						$jaEntregatTuts+=$mVal2['rebut'];
					}
				}
				reset($mVal);
			}
			reset($mProductesJaEntregats);
		}
	}
	
	//incorporar reserves a productes
	
	while(list($index,$mProducte)=each($mProductes))
	{
		if(isset($mReservesProductes[$index]))
		{
			$mProductes[$index]['quantitat']=$mReservesProductes[$index];
		}
		else
		{
			$mProductes[$index]['quantitat']=0;
		}
	}
	reset($mProductes);


	return;
}

//------------------------------------------------------------------------------
function getRecepcions($db)
{
	global $mPars,$mRebostsRef;
	
	$mRecepcions=array();
	
	if(!$result=mysql_query("select rebost,punt_entrega from comandes_".$mPars['selRutaSufix']." where usuari_id='0' AND rebost!='0-CAC'",$db))
	{
		//echo "<br> 3701 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			$mRecepcions[$grupId]['punt_entrega']=$mRow['punt_entrega'];
			if(@substr_count($mRebostsRef[$mRow['punt_entrega']]['recepcions'],','.$grupId.',')>0)
			{
				$mRecepcions[$grupId]['acceptada']=1;
			}
			else
			{
				$mRecepcions[$grupId]['acceptada']=0;
			}
		}
	}
	

	return $mRecepcions;
}

//------------------------------------------------------------------------------
function getSuperZona($zona)
{
	global $mPars,$mGrupsZones;
	
	@reset($mGrupsZones);
	while(list($sz_,$mZones_)=@each($mGrupsZones))
	{
		if(in_array($zona,$mZones_)){return $sz_;}
	}
	@reset($mGrupsZones);

	return false;;
}
//------------------------------------------------------------------------------
function db_getProductesFiltre($camp1,$valor1,$camp2,$valor2,$db)
{
	global $mPars,$mRebostsRef;
	
	$mProductesFiltre=array();
	
	if(!$result=mysql_query("select * from productes_".$mPars['selRutaSufix']." where ".$camp1."='".$valor1."' AND ".$camp2."='".$valor2."'",$db))
	{
		//echo "<br> 3833 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mProductesFiltre[$i]=$mRow;
			$i++;
		}
	}
	

	return $mProductesFiltre;
}

//------------------------------------------------------------------------------
function db_getComandesComptesCes($db)
{
	global $mPars;
	
	
//*v36 24-1-16 1 assignacio
		$contEuro=0;
	if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." ",$db))
	{
		//echo "<br> 3833 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
//*v36 24-1-16 eliminar 1 condicio
			//if($mRow['compte_ecos']!='')
			//{
//*v36 24-1-16 1 condicio
				if($mRow['compte_ecos']==''){$mRow['compte_ecos']='euro_'.$contEuro;$contEuro++;}
				if(!isset($mComandesComptesCes[$mRow['compte_ecos']]))
				{
					$mComandesComptesCes[$mRow['compte_ecos']]=array();
				}
				$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
				if(!isset($mComandessCompteCes[$mRow['compte_ecos']][$grupId]))
				{
					$mComandesComptesCes[$mRow['compte_ecos']][$grupId]=array();
				}
				if(!isset($mComandesComptesCes[$mRow['compte_ecos']][$grupId][$mRow['usuari_id']]))
				{
					$mComandesComptesCes[$mRow['compte_ecos']][$grupId][$mRow['usuari_id']]=array();
				}
				$mComandesComptesCes[$mRow['compte_ecos']][$grupId][$mRow['usuari_id']]=$mRow;
			//}
		}
	}
	

	return $mComandesComptesCes;
}


//------------------------------------------------------------------------------
function db_grupsPerSzIzona($db)
{
	global $mPars,$mGrupsZones,$mRebostsRef;
	
	$mGrupsPerSzIzona=array();
	while(list($grupRef,$mGrupRef)=each($mRebostsRef))
	{
		$zonaGrup=db_getZonaGrup($grupRef,$db);
		while(list($sz,$mSz)=each($mGrupsZones))
		{
			if(!isset($mGrupsPerSzIzona[$sz])){$mGrupsPerSzIzona[$sz]=array();}
			while(list($key,$z)=each($mSz))
			{
				if(!isset($mGrupsPerSzIzona[$sz][$z])){$mGrupsPerSzIzona[$sz][$z]=array();}
				if($z==$zonaGrup){array_push($mGrupsPerSzIzona[$sz][$z],$grupRef);}
			}
			reset($mSz);
		}
		reset($mGrupsZones);
	}
	reset($mRebostsRef);
	

	return $mGrupsPerSzIzona;
}
//------------------------------------------------------------------------------
function db_grupsPerSzIzonaRef($db)
{
	global $mPars,$mGrupsZones,$mRebostsRef;
	
	$mGrupsPerSzIzona=array();
	while(list($grupRef,$mGrupRef)=each($mRebostsRef))
	{
		$zonaGrup=db_getZonaGrup($grupRef,$db);
		while(list($sz,$mSz)=each($mGrupsZones))
		{
			if(!isset($mGrupsPerSzIzona[$sz])){$mGrupsPerSzIzona[$sz]=array();}
			while(list($key,$z)=each($mSz))
			{
				if(!isset($mGrupsPerSzIzona[$sz][$z])){$mGrupsPerSzIzona[$sz][$z]=array();}
				if($z==$zonaGrup){$mGrupsPerSzIzona[$sz][$z][$grupRef]=array();}
			}
			reset($mSz);
		}
		reset($mGrupsZones);
	}
	reset($mRebostsRef);
	

	return $mGrupsPerSzIzona;
}
//------------------------------------------------------------------------------
function db_getCodisEcoxarxes($db)
{
	global $mPars;
	
	$mCodisEcx=array();
	
	if(!$result=mysql_query("select DISTINCT(SUBSTRING(compte_ecos,1,4)) from comandes_".$mPars['selRutaSufix']." ",$db))
	{
		//echo "<br> 3973 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			if($mRow[0]!='')
			{
				$mCodisEcx[$mRow[0]]['color']=substr(MD5($mRow[0]),0,6);
			}
		}
	}
	

	return $mCodisEcx;
}

//------------------------------------------------------------------------------
function db_getUsuarisPropietat($propietatUsuari,$db)
{
	global $mPropietatsUsuaris;
	
	$mUsuarisPropietatRef=array();
	
	if(!$result=mysql_query("select * from usuaris where LOCATE('{".$propietatUsuari.":',CONCAT(' ',propietats))>0",$db))
	{
		//echo "<br> 3973 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mUsuarisPropietatRef[$mRow['id']]=$mRow;
		}
	}
	return $mUsuarisPropietatRef;
}
//------------------------------------------------------------------------------
function db_getUsuarisAmbComandaResum($db)
{
	global $mPars,$mUsuarisAmbComanda,$mUsuarisPropietatRef;
	
	$usuarisPropietat=implode(',',array_keys($mUsuarisPropietatRef));
	$usuarisPropietat=$usuarisPropietat.',';
	$usuarisPropietat=str_replace(',62,',',',$usuarisPropietat);
		

	//echo "select * from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',usuari_id,','),CONCAT(' ','".$usuarisPropietat."'))>0 AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='62' AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='0' AND usuari_id!='0'";
	if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',usuari_id,','),CONCAT(' ','".$usuarisPropietat."'))>0 AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='62' AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='0' AND usuari_id!='0'",$db))
	{
		//echo "<br> 3973 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			if(!isset($mUsuarisAmbComanda[$mRow['usuari_id']])){$mUsuarisAmbComanda[$mRow['usuari_id']]=array();}
			if(!isset($mUsuarisAmbComanda[$mRow['usuari_id']][$grupId])){$mUsuarisAmbComanda[$mRow['usuari_id']][$grupId]=array();}
			$mUsuarisAmbComanda[$mRow['usuari_id']][$grupId]=$mRow;
		}
	}
	return $mUsuarisAmbComanda;
}
		
//------------------------------------------------------------------------------
function db_getUsuarisAmbComandaEspResum($db)
{
	global $mPars,$mUsuarisAmbComanda,$mUsuarisPropietatRef;
	
	$usuarisPropietat=implode(',',array_keys($mUsuarisPropietatRef));
	$usuarisPropietat=$usuarisPropietat.',';
	$usuarisPropietat=str_replace(',62,',',',$usuarisPropietat);
		
	$mUsuarisAmbComandaEsp=array();
	//echo "select * from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',usuari_id,','),CONCAT(' ','".$usuarisPropietat."'))>0 AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='62' AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='0' AND usuari_id!='0'";
	if(!$result=mysql_query("select * from comandes_especials1_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',usuari_id,','),CONCAT(' ','".$usuarisPropietat."'))>0 AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='62' AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)!='0' AND usuari_id!='0'",$db))
	{
		//echo "<br> 3973 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			if(!isset($mUsuarisAmbComandaEsp[$mRow['usuari_id']])){$mUsuarisAmbComandaEsp[$mRow['usuari_id']]=array();}
			if(!isset($mUsuarisAmbComandaEsp[$mRow['usuari_id']][$grupId])){$mUsuarisAmbComandaEsp[$mRow['usuari_id']][$grupId]=array();}
			$mUsuarisAmbComandaEsp[$mRow['usuari_id']][$grupId]=$mRow;
		}
	}
	
	return $mUsuarisAmbComandaEsp;
}

//------------------------------------------------------------------------------
function db_getRecolzamentEcosComandesEspUsuariComissio($db)
{
	global $mPars,$mParametres;

	$parsChain=makeParsChain($mPars);

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['etiqueta']='especial';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';
	$mProductes=db_getProductes2($db);
	$quantitatTotalCac=0;
	$quantitatTotalRebosts=0;
	
	$ecos=0;
	$ecosR=0;

	if(!$result=mysql_query("select sum(estoc_previst*pes) from productes_".$mPars['selRutaSufix']." where actiu='1'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalCac+=$mRow[0];
	}

	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2)),$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$quantitatTotalRebosts+=$mRow['quantitat'];
		}
	}
	
	//echo "<br>select * from comandes_especials1_".$mPars['selRutaSufix']." where usuari_id='".$mPars['selUsuariId']."'";
	if(!$result=mysql_query("select * from comandes_especials1_".$mPars['selRutaSufix']." where usuari_id='".$mPars['selUsuariId']."'",$db))
	{
		//echo "<br> 4397 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mResum=explode(';',$mRow['resum']);
			$key=str_replace('producte_','',$mResum[0]);
			$key=substr($key,0,strpos($key,':'));
			if($key!='')
			{
				$ecos+=$mProductes[$key]['preu']*$mProductes[$key]['ms']/100;
				$ecos+=$mProductes[$key]['preu']*($mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
				$ecos+=$mProductes[$key]['pes']*(($mProductes[$key]['cost_transport_extern_kg']*$mProductes[$key]['ms_ctek']/100)+($mProductes[$key]['cost_transport_intern_kg']*$mProductes[$key]['ms_ctik']/100));
				$ecos+=$mProductes[$key]['pes']*(($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100+$mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)/($quantitatTotalCac+$quantitatTotalRebosts));
				$mFpagament=explode(',',$mRow['f_pagament']);
				$ecosR+=$mFpagament[0]-$ecos;
			}
			$ecos=0;
		}
	}
	$mPars=getPars($parsChain);
	return $ecosR;
}

//------------------------------------------------------------------------------
function db_getRecolzamentEcosComandesEspUsuariAgrup($db)
{
	global $mPars,$mParametres;
	
	$parsChain=makeParsChain($mPars);

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['etiqueta']='especial';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';
	$mProductes=db_getProductes2($db);
	$ecos=0;
	$ecosR=0;
	$quantitatTotalCac=0;
	$quantitatTotalRebosts=0;
	
	if(!$result=mysql_query("select sum(estoc_previst*pes) from productes_".$mPars['selRutaSufix']." where actiu='1'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalCac+=$mRow[0];
	}

	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2)),$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$quantitatTotalRebosts+=$mRow['quantitat'];
		}
	}

	//echo "<br>select * from comandes_especials1_".$mPars['selRutaSufix']." where usuari_id='".$mPars['selUsuariId']."' AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mPars['grup_id']."'";
	if(!$result=mysql_query("select * from comandes_especials1_".$mPars['selRutaSufix']." where usuari_id='".$mPars['selUsuariId']."' AND SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mPars['grup_id']."'",$db))
	{
		echo "<br> 4508 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mResum=explode(';',$mRow['resum']);
			$key=str_replace('producte_','',$mResum[0]);
			$key=substr($key,0,strpos($key,':'));
			if($key!='')
			{
				$ecos+=$mProductes[$key]['preu']*$mProductes[$key]['ms']/100;
				$ecos+=$mProductes[$key]['preu']*($mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
				$ecos+=$mProductes[$key]['pes']*(($mProductes[$key]['cost_transport_extern_kg']*$mProductes[$key]['ms_ctek']/100)+($mProductes[$key]['cost_transport_intern_kg']*$mProductes[$key]['ms_ctik']/100));
				$ecos+=$mProductes[$key]['pes']*(($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100+$mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)/($quantitatTotalCac+$quantitatTotalRebosts));
				$mFpagament=explode(',',$mRow['f_pagament']);
				$ecosR+=1*$mFpagament[0]-$ecos;
			}
			$ecos=0;
		}
	}
	$mPars=getPars($parsChain);
	
	return $ecosR;
}
//------------------------------------------------------------------------------
function guardarPropietatsUsuari($mPropietats_,$db)
{
	global $mPars,$mSelUsuari;

	$mPropietats=getPropietats($mSelUsuari['propietats']);
	while(list($propietat,$valor)=each($mPropietats_))
	{
		$mPropietats[$propietat]=$valor;
	}
	reset($mPropietats_);
	
	$propietats=makePropietats($mPropietats);
	
	if(!$result=mysql_query("update usuaris set propietats='".$propietats."' where id='".$mSelUsuari['id']."'",$db))
	{
		//echo "<br> 4237 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}

	return;
}

//------------------------------------------------------------------------------
function db_guardarPropietatsGrup($propietatsGrup,$db)
{
	global $mPars;

	if(!$result=mysql_query("update rebosts_".$mPars['selRutaSufix']." set propietats='".$propietatsGrup."' where id='".$mPars['grup_id']."'",$db))
	{
		//echo "<br> 4237 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	
	return true;
}
//------------------------------------------------------------------------------
function db_guardarPropietatsPeriodeLocal($propietatsPeriodeLocal,$db)
{
	global $mPars;

	//echo "<br>update ".$mPars['taulaComandes']." set propietats='".$propietatsPeriodeLocal."' where periode_comanda='".$mPars['sel_periode_comanda_local']."' AND usuari_id='0' AND llista_id='".$mPars['grup_id']."'";
	if(!$result=mysql_query("update ".$mPars['taulaComandes']." set propietats='".$propietatsPeriodeLocal."' where id='".$mPars['sel_periode_comanda_local']."'",$db))
	{
		//echo "<br> 5566 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	
	return true;
}
//------------------------------------------------------------------------------
function db_guardarNovaComandaLocalGrup($mDatesReservesLocals,$db) //nom�s per fer accessible el periode
{
	global $mPars,$mRebost,$mPropietatsPeriodeLocalConfig,$mPeriodesLocalsInfo,$mParametres,$mParams;
	
	//$mPropietatsPeriodeLocal=db_getPropietatsPeriodeLocal($db);
	$propietatsPeriodeLocal=makePropietatsPeriodeLocal($mPropietatsPeriodeLocalConfig);
	$hiHaSobreposicio=false;
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['result']=true;
	$mMissatgeAlerta['missatge']='';
	
	$mPars['sel_periode_comanda_local']=
		$mDatesReservesLocals['idia']
	."-".$mDatesReservesLocals['imes']
	."-".$mDatesReservesLocals['iany']
	.":".$mDatesReservesLocals['fdia']
	."-".$mDatesReservesLocals['fmes']
	."-".$mDatesReservesLocals['fany'];
	
	$mPars['periode_comanda_local']=$mPars['sel_periode_comanda_local'];



	//comprovar que el nou periode no es sobreposa amb algun periode no tancat 
	
	if(!$result=mysql_query("select * from ".$mPars['taulaComandes']." WHERE  SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mPars['grup_id']."' AND llista_id='".$mPars['selLlistaId']."' AND LOCATE('comandesLocalsTancades:1;',CONCAT(' ',propietats))=0 AND usuari_id=0",$db))
	{
		//echo "<br> 5572 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mDates=explode(':',$mRow['periode_comanda']);
		$mDates[0]=str_replace('-',',',$mDates[0]);
		$mDates[1]=str_replace('-',',',$mDates[1]);
		$mDatesReservesLocals_=getDatesReservesLocalsNouPeriode($mDates[0],$mDates[1]);
		if
		(
			(
				$mDatesReservesLocals_['itimestamp']*1>=$mDatesReservesLocals['itimestamp']*1
				&&
				$mDatesReservesLocals_['itimestamp']*1<=$mDatesReservesLocals['ftimestamp']*1
			)
			||
			(
				$mDatesReservesLocals_['ftimestamp']*1>=$mDatesReservesLocals['itimestamp']*1
				&&
				$mDatesReservesLocals_['ftimestamp']*1<=$mDatesReservesLocals['ftimestamp']*1
			)
		)
		{
			$hiHaSobreposicio=true;
		}
	}
	}
	if($hiHaSobreposicio)
	{
		$mMissatgeAlerta['missatge']="<p class='pAlertaNo4'>Atenci�: el periode (".$mPars['sel_periode_comanda_local'].") es sobreposa amb algun periode no TANCAT.<br>Cal escollir unes altres dates. Els periodes NO TANCATS no es poden sobreposar entre ells.</p>";
		$mMissatgeAlerta['result']=false;

		return $mMissatgeAlerta;
	}
	else
	{
		//registre historial comanda
		$registre='';
		$mGC['usuari_id']=$mPars['usuari_id'];
	
		while(list($key,$val)=each($mGC))
		{
			$registre.='{p:guardarComanda;us:'.$mPars['usuari_id'].';'.$key.':'.$val.';dt:'.date('d/m/Y H:i:s').';}';
		}
		reset($mGC);
		//echo "<br>insert into ".$mPars['taulaComandes']." values('','".(date('YmdHis'))."','".$mPars['sel_periode_comanda_local']."','".$mPars['grup_id'].'-'.$mRebost['nom']."','0','".(date('dmY'))."','','','','','','','','".$registre."','','','".$mPars['selLlistaId']."','".$propietatsPeriodeLocal."'";
		if(!$result=mysql_query("insert into ".$mPars['taulaComandes']." values('','".(date('YmdHis'))."','".$mPars['sel_periode_comanda_local']."','".$mPars['grup_id'].'-'.$mRebost['nom']."','0','".(date('dmY'))."','','','','','','','','".$registre."','','','".$mPars['selLlistaId']."','".$propietatsPeriodeLocal."')",$db))
		{
			//echo "<br> 5747 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
			
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo4'>Atenci�: no s'ha pogut guardar el nou periode (".$mPars['sel_periode_comanda_local'].").</p>";
			$mMissatgeAlerta['result']=false;

			return $mMissatgeAlerta;
		}
		else
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaOk4'>S'ha creat el nou periode (".$mPars['sel_periode_comanda_local'].").</p>";
		
			// eliminar info periodes vells
			
			$mPeriodesEliminar=array();
			$mPeriodesLocalsInfo=db_getPeriodesLocalsInfo($db);
			while(count($mPeriodesLocalsInfo)>$mParametres['limitNombrePeriodesLocalsHistorial']['valor']*1)
			{
				$mPeriodeEliminar=array_pop($mPeriodesLocalsInfo);
				array_push($mPeriodesEliminar,$mPeriodeEliminar);
			}

			//eliminar els periodes m�s vells
			while(list($key,$mPeriode)=each($mPeriodesEliminar))
			{
				if(!$result=mysql_query("delete from ".$mPars['taulaComandes']." WHERE id='".$mPeriode['id']."'",$db))
				{
					//echo "<br> 5747 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1',mysql_errno().'--'.mysql_error(),'100','db.php');
			
					$mMissatgeAlerta['missatge']="<p class='pAlertaNo4'>Atenci�: no s'ha pogut eliminar el periode antic: <b>".$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['periode_comanda']." (".$mPars['sel_periode_comanda_local'].")</b></p>";
				}
				else
				{
					//unset($mParams['path']."/rebosts/".$mPars['grup_id']."/llistaProductesLocal_".$id.".csv");
				}
			}
			$mMissatgeAlerta['missatge']="<p class='pAlertaOk4'>S'han eliminat de l'historial els periodes m�s antics</p>";
		}
	}

	return $mMissatgeAlerta;
}


//------------------------------------------------------------------------------
//*v36-5-1-16 nova funcio
function db_getPeriodesInfo($db)
{
	global $mPars,$mRutesSufixes;

	$mPeriodesInfo=array();
	while(list($key,$val)=each($mRutesSufixes))
	{
		$mPeriodesInfo[$val]=array();

		if(!$result=mysql_query("select parametre,valor from parametres_".$val." WHERE parametre='precomandaTancada'",$db))
		{
			//echo "<br> 4237 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			$mPeriodesInfo[$val]['precomandaTancada']='-';
		}
		else
		{
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			
			$mPeriodesInfo[$val][$mRow['parametre']]=$mRow['valor'];
		}
	}
	reset($mRutesSufixes);
	return $mPeriodesInfo;
}

//------------------------------------------------------------------------------
//*v36-5-1-16 nova funcio
function db_getPeriodesLocalsInfo($db)
{
	global $mPars;

	$mPeriodesLocalsInfo=array();
	
	if(!$result=mysql_query("select * from comandes_grups WHERE llista_id='".$mPars['selLlistaId']."' AND usuari_id=0 ORDER BY id DESC",$db))
	{
		//echo "<br> 4237 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPeriodesLocalsInfo[$mRow['id']]=$mRow;
		}
	}

	return $mPeriodesLocalsInfo;
}

//------------------------------------------------------------------------------
//*v36-7-1-16 nova funcio
function db_getGrupsActius($db)
{
	global $mPars;
	
	$mGrupsActius=array();
	
	//echo "<br>select id from rebosts_".$mPars['selRutaSufix']." WHERE LOCATE(',actiu,',CONCAT(' ',categoria))!=0";
	if(!$result=mysql_query("select id from rebosts_".$mPars['selRutaSufix']." WHERE LOCATE(',actiu,',CONCAT(' ',categoria))!=0",$db))
	{
		//echo "<br> 4931 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{			
			array_push($mGrupsActius,$mRow['id']);
		}
	}
	
	return $mGrupsActius;
	
}
//------------------------------------------------------------------------------
//*v36 20-1-16 nova funcio
function db_getGrupsSzona($sZn,$db)
{
	global $mPars,$mGrupsZones;
	
	$grupsSelSzChain='';
	
	$where='(';
	while(list($sZona,$mSzona)=each($mGrupsZones))
	{
		if($sZona==$sZn)
		{
			while(list($key,$zona)=each($mSzona))
			{
				$where.="LOCATE(',2-".$zona.",',CONCAT(' ',categoria))!=0 OR ";
			}
			reset($mSzona);
		}
	}
	reset($mGrupsZones);
	
	$where=substr($where,0,strlen($where)-3);
	$where.=')';
	
	//echo "<br>select id from rebosts_".$mPars['selRutaSufix']." WHERE ".$where."";
	if(!$result=mysql_query("select id from rebosts_".$mPars['selRutaSufix']." WHERE ".$where." ",$db))
	{
		//echo "<br> 4957 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{			
			$grupsSelSzChain.=','.$mRow['id'];
		}
		$grupsSelSzChain.=',';
	}

	return $grupsSelSzChain;
}
//------------------------------------------------------------------------------
//*v36 20-1-16 nova funcio
function db_getGrupsZona($sz,$db)
{
	global $mPars;
	
	$grupsSelZchain='';
	if(!$result=mysql_query("select id from rebosts_".$mPars['selRutaSufix']." WHERE LOCATE(',2-".$sz.",',CONCAT(' ',categoria))!=0",$db))
	{
		//echo "<br> 4980 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{			
			$grupsSelZchain.=','.$mRow['id'];
		}
		$grupsSelZchain.=',';
	}
	return $grupsSelZchain;
}

//------------------------------------------------------------------------------
function db_getProductesSzona($sZn,$db)
{
	global $mPars,$mGrupsZones;
	
	$perfilsSelSzChain='';
	$productesSelSzChain='';
	
	$where='(';
	while(list($sZona,$mSzona)=each($mGrupsZones))
	{
		if($sZona==$sZn)
		{
			while(list($key,$zona)=each($mSzona))
			{
				$where.=" zona='".$zona."' OR ";
			}
			reset($mSzona);
		}
	}
	reset($mGrupsZones);
	
	$where=substr($where,0,strlen($where)-3);
	$where.=')';
	
	//echo "<br>select id from rebosts_".$mPars['selRutaSufix']." WHERE ".$where."";
	if(!$result=mysql_query("select id from productors WHERE ".$where." ",$db))
	{
		//echo "<br> 4957 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{			
			$perfilsSelSzChain.=','.$mRow['id'];
		}
		$perfilsSelSzChain.=',';
	}

	if(!$result=mysql_query("select id from productes_".$mPars['selRutaSufix']." WHERE LOCATE(CONCAT(',',SUBSTRING(productor,1,LOCATE('-',productor)-1),','),CONCAT(' ','".$perfilsSelSzChain."'))!=0 ",$db))
	{
		//echo "<br> 4957 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{			
			$productesSelSzChain.=','.$mRow['id'];
		}
		$productesSelSzChain.=',';
	}
	return $productesSelSzChain;
}
//------------------------------------------------------------------------------
function db_getProductesZona($sz,$db)
{
	global $mPars;
	
	$perfilsSelZchain='';
	$productesSelZchain='';
	
	//echo "<br>select id from productors WHERE zona='".$sz."' ";
	if(!$result=mysql_query("select id from productors WHERE zona='".$sz."' ",$db))
	{
		//echo "<br> 6333 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{			
			$perfilsSelZchain.=','.$mRow['id'];
		}
		$perfilsSelZchain.=',';
	}

	//echo "<br>select id from productes_".$mPars['selRutaSufix']." WHERE LOCATE(CONCAT(',',SUBSTRING(productor,1,LOCATE('-',productor)-1),','),CONCAT(' ','".$perfilsSelZchain."'))!=0 ";
	if(!$result=mysql_query("select id from productes_".$mPars['selRutaSufix']." WHERE LOCATE(CONCAT(',',SUBSTRING(productor,1,LOCATE('-',productor)-1),','),CONCAT(' ','".$perfilsSelZchain."'))!=0 ",$db))
	{
		//echo "<br> 6347 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		//echo "<br> 6347 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{			
			$productesSelZchain.=','.$mRow['id'];
		}
		$productesSelZchain.=',';
	}	
	
	return $productesSelZchain;
}
//------------------------------------------------------------------------------
function db_getIdsGrupsAmbVincle($db)
{
	global $mPars;

	$grupsAmbVincleChain='';
	
	if(!$result=mysql_query("select grup_vinculat from ".$mPars['taulaProductors']." where grup_vinculat!=''",$db))
	{
		//echo "<br> 5599 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupsAmbVincleChain.=','.$mRow['grup_vinculat'];
		}
		$grupsAmbVincleChain=substr($grupsAmbVincleChain,1);
	}

	return $grupsAmbVincleChain;
}

//------------------------------------------------------------------------------
function db_getPerfilsRefActiusChain($db)
{
	global $mPars,$mPropietatsGrup;
	
	$perfilsRefActiusChain='';

	if($mPars['selLlistaId']==0)
	{
		if(!$result=mysql_query("select id from ".$mPars['taulaProductors']."  WHERE estat='1' order by projecte ASC",$db))
		{
			//echo "<br> 5791 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
		}
		else
  		{
			while($mPerfil=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$perfilsRefActiusChain.=','.$mPerfil['id'];
			}
			$perfilsRefActiusChain.=',';
		}
	}
	else
	{
		$perfilsRefActiusChain=$mPropietatsGrup['idsPerfilsActiusLocal'];
	}


	return $perfilsRefActiusChain;
}

//------------------------------------------------------------------------------
function db_getPropietatsPeriodesLocals($db)
{
	global $mPars,$mPropietatsPeriodeLocalConfig;
	
	$mPropietatsPeriodesLocals=array();
	$mPropietatsPeriodesLocals_=array();
	//echo "<br>select * from ".$mPars['taulaComandes']."  WHERE usuari_id=0 AND llista_id='".$mPars['selLlistaId']."'  order by CONCAT(SUBSTRING(periode_comanda,7,2),SUBSTRING(periode_comanda,4,2),SUBSTRING(periode_comanda,1,2))*1 DESC";
	if(!$result=mysql_query("select * from ".$mPars['taulaComandes']."  WHERE usuari_id=0 AND llista_id='".$mPars['selLlistaId']."'  order by CONCAT(SUBSTRING(periode_comanda,7,2),SUBSTRING(periode_comanda,4,2),SUBSTRING(periode_comanda,1,2))*1 DESC",$db))
	{
		//echo "<br> 5818 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mPropietatsPeriodesLocals[$mRow['id']]=$mPropietatsPeriodeLocalConfig;
			
			$mPropietatsPeriodesLocals_=getPropietatsPeriodeLocal($mRow['propietats']);
			while(list($propietat,$valor)=each($mPropietatsPeriodesLocals_))
			{
				$mPropietatsPeriodesLocals[$mRow['id']][$propietat]=$valor;
			}
		}
	}
	

	return $mPropietatsPeriodesLocals;
}

//------------------------------------------------------------------------------
function db_getPropietatsPeriodesLocalsLlistesAssociades($llistesAssociadesChain,$db)
{
	global $mPars,$mPropietatsPeriodeLocalConfig;
	
	$mPropietatsPeriodesLocals=array();
	$mPropietatsPeriodesLocals_=array();

	if(!$result=mysql_query("select * from ".$mPars['taulaComandes']."  WHERE usuari_id=0 AND LOCATE(CONCAT(',',llista_id,','),'".$llistesAssociadesChain."')>0  order by CONCAT(SUBSTRING(periode_comanda,7,2),SUBSTRING(periode_comanda,4,2),SUBSTRING(periode_comanda,1,2))*1 DESC",$db))
	{
		//echo "<br> 5818 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if(!isset($mPropietatsPeriodesLocals[$mRow['llista_id']]))
			{
				$mPropietatsPeriodesLocals[$mRow['llista_id']]=array();
			}
			$mPropietatsPeriodesLocals[$mRow['llista_id']][$mRow['periode_comanda']]=$mPropietatsPeriodeLocalConfig;
			$mPropietatsPeriodesLocals_=getPropietatsPeriodeLocal($mRow['propietats']);
			
			while(list($propietat,$valor)=each($mPropietatsPeriodesLocals_))
			{
				$mPropietatsPeriodesLocals[$mRow['llista_id']][$mRow['periode_comanda']][$propietat]=$valor;
			}
		}
	}

	return $mPropietatsPeriodesLocals;
}
//------------------------------------------------------------------------------
function db_tancarPeriodeLocalGrup($db)
{
	global $mPars,$mProductes,$mPropietatsPeriodeLocal;

	$mPropietatsPeriodeLocal['comandesLocalsTancades']=1;
	$propietatsPeriodeLocal=makePropietatsPeriodeLocal($mPropietatsPeriodeLocal);
	if(db_guardarPropietatsPeriodeLocal($propietatsPeriodeLocal,$db))
	{
		//guardar propietat periode a 'tancat'

		if(!$result=mysql_query("update ".$mPars['taulaComandes']." set propietats='".$propietatsPeriodeLocal."' WHERE id='".$mPars['sel_periode_comanda_local']."'",$db))
		{
			//echo "<br> 6638 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
			
			return false;
		}
		else
		{
			//generar el .txt amb la taula de productes actual
			if(csv_crearLlistaProductesPeriodeLocal($db)) //comandes - local
			{
				//echo "<br>update ".$mPars['taulaProductes']." set estoc_previst=estoc_disponible, actiu='0' WHERE llista_id='".$mPars['selLlistaId']."'";
				if(!$result=mysql_query("update ".$mPars['taulaProductes']." set estoc_previst=estoc_disponible, actiu='0' WHERE llista_id='".$mPars['selLlistaId']."'",$db))
				{
					//echo "<br> 6650 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
			
					return false;
				}
			}
		}
	}
	else
	{
		return false;
	}

	
	
	return true;
}

//------------------------------------------------------------------------------
function db_getCampsProductes($db)
{
	global $mPars;
	
	$result=mysql_query("select * from ".$mPars['taulaProductes']." limit 1",$db);
	//echo "<br>  41 config.php".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	$mCampsProductes=array_keys($mRow);
	
	return $mCampsProductes;
}

//------------------------------------------------------------------------------
function db_getPropietatsSegellPerfil($db)
{
	global $mPars,$mPropietatsSegellPerfilConfig;
	
	$mPropietatsSegellPerfil=array();
	if(!$result=mysql_query("select propietats from ".$mPars['taulaProductors']."  WHERE id='".@$mPars['selPerfilRef']."'",$db))
	{
		//echo "<br> 6254 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			
		$mPropietatsSegellPerfil=getPropietatsSegellPerfil($mRow['propietats']);
			
	}
	
	return $mPropietatsSegellPerfil;
}

//------------------------------------------------------------------------------
function db_guardarPropietatsSegellProducte($valor,$propietats,$db)
{
	global $mPars;

	//echo "<br>update ".$mPars['taulaProductes']." set segell='".$valor."',propietats='".$propietats."' WHERE llista_id='".$mPars['selLlistaId']."' AND id='".$mPars['selProducteId']."'";
	if(!$result=mysql_query("update ".$mPars['taulaProductes']." set segell='".$valor."',propietats='".$propietats."' WHERE llista_id='".$mPars['selLlistaId']."' AND id='".$mPars['selProducteId']."'",$db))
	{
		//echo "<br> 6273 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	
	return true;
}
//------------------------------------------------------------------------------
function db_getPropietatsSegellProducte($db)
{
	global $mPars,$mPropietatsSegellProducteConfig;
	
	$mPropietatsSegellProducte=array();
	if(!$result=mysql_query("select propietats from ".$mPars['taulaProductes']." WHERE  llista_id='".$mPars['selLlistaId']."' AND id='".@$mPars['selProducteId']."'",$db))
	{
		//echo "<br> 6287 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		$mPropietatsSegellProducte=getPropietatsSegellProducte($mRow['propietats']);
			
	}
	
	return $mPropietatsSegellProducte;
}

//------------------------------------------------------------------------------
function db_guardarPropietatsSegellPerfil($valorSegellPerfil,$propietats,$db)
{
	global $mPars;
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=true;

	if(!$result=mysql_query("update ".$mPars['taulaProductors']." set segell='".$valorSegellPerfil."',propietats='".$propietats."' where id='".$mPars['selPerfilRef']."'",$db))
	{
		//echo "<br> 6310 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		
		$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>Actualitzar segell d'aquest perfil de productora: ha fallat</p>";
		$mMissatgeAlerta['result']=false;

		return $mMissatgeAlerta;
	}
	else
	{
		//obtenir productes d'aquest perfil per cada llista on en tingui

		$mCategoria0perfil=db_getCategoria0Perfil($db);
		$mPropietatsSegellPerfil=db_getPropietatsSegellPerfil($db);	
		$maxValorSegellPerfil=getValorMaxSegell($mPropietatsSegellPerfil,$mCategoria0perfil);

		$mLlistesRef=db_getGrupsAssociatsPerfilRef($db);
		$mLlistesRef[0]=array('nom'=>'CAC');
		while(list($llistaId,$mLlista)=each($mLlistesRef))
		{
			$numProductes=db_getNumProductesPerfilAllista($mPars['selPerfilRef'],$llistaId,$db);
			if($numProductes>0)
			{
				if($llistaId==0)
				{
					$taulaProductes='productes_'.$mPars['selRutaSufix'];
				}
				else
				{
					$taulaProductes='productes_grups';
				}

				//obtenir les propietats de cada producte
				if(!$result=mysql_query("select id,propietats,categoria0 from ".$taulaProductes." WHERE SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPars['selPerfilRef']."' AND llista_id='".$llistaId."'",$db))
				{
					//echo "<br> 6336 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>Obtenir segell dels productes d'aquest perfil de productora a la llista del grup <b>".(urldecode($mLlista['nom']))."</b> : ha fallat</p>";
					$mMissatgeAlerta['result']=false;
				}
				else
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>Obtenir segell dels productes d'aquest perfil de productora a la llista del grup <b>".(urldecode($mLlista['nom']))."</b> : ok</p>";
				
					while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
					{
						$mPropietatsSegellProducte=db_getPropietatsSegellProducte($db);	
						$valorSegellProducte=getValorSegell($mPropietatsSegellProducte,array($mRow['categoria0']=>''));
						$maxValorSegellProducte=getValorMaxSegell($mPropietatsSegellProducte,array($mRow['categoria0']=>''));

						$valor=(($valorSegellPerfil+$valorSegellProducte)/($maxValorSegellPerfil+$maxValorSegellProducte))*100;				
						//echo "<br>update ".$taulaProductes." set segell=".(number_format($valor,2,'.',''))." WHERE id='".$mRow['id']."' AND llista_id='".$llistaId."'";
						if(!$result2=mysql_query("update ".$taulaProductes." set segell=".(number_format($valor,2,'.',''))." WHERE id='".$mRow['id']."' AND llista_id='".$llistaId."'",$db))
						{
							//echo "<br> 6347 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
			
							$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>Actualitzar segell del producte id='".$mRow['id']."' d'aquest perfil de productora a la llista del grup <b>".(urldecode($mLlista['nom']))."</b> : ha fallat</p>";
							$mMissatgeAlerta['result']=false;
						}
						else
						{
							$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>Actualitzar segell del producte id='".$mRow['id']."' d'aquest perfil de productora a la llista del grup <b>".(urldecode($mLlista['nom']))."</b> : ok</p>";
						}
					}
				}
			}
		}	
		$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>Actualitzar segell d'aquest perfil de productora: ok</p>";
	}
	
	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function db_getCategoria0Perfil($db)
{
	global $mPars;
	
	$mCategoria0Perfil=array();
	$mCategoria0Perfil1=array();
	$mCategoria0Perfil2=array();
	//propietats llista cab
	//echo "<br>select categoria0 from productes_".$mPars['selRutaSufix']."  WHERE SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPars['selPerfilRef']."'";
	if(!$result=mysql_query("select DISTINCT(categoria0) from productes_".$mPars['selRutaSufix']."  WHERE SUBSTRING(productor,1,LOCATE('-',productor)-1)='".@$mPars['selPerfilRef']."' ORDER BY categoria0",$db))
	{
		//echo "<br> 6416 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mCategoria0Perfil1[$mRow['categoria0']]='';
		}
			
	}

	//propietats llistes locals

	//echo "<br>select DISTINCT(categoria0) from productes_grups  WHERE SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPars['selPerfilRef']."'";
	if(!$result=mysql_query("select DISTINCT(categoria0) from productes_grups  WHERE SUBSTRING(productor,1,LOCATE('-',productor)-1)='".@$mPars['selPerfilRef']."'",$db))
	{
		//echo "<br> 6431 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{	
			$mCategoria0Perfil2[$mRow[0]]='';
		}
	}

	$mCategoria0Perfil=array_merge($mCategoria0Perfil1,$mCategoria0Perfil1);

	return $mCategoria0Perfil;
}

//------------------------------------------------------------------------------
function db_getProductesLlistaLocalGrupRef($db)
{
	global $mPars;

	$mNumProdsLlistesLocals=array();
	
	if(!$result=mysql_query("select id,llista_id from productes_grups WHERE actiu='1' ORDER BY id ASC",$db))
	{
		//echo "<br> 6626 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{	
			if(!isset($mNumProdsLlistesLocals[$mRow[1]])){$mNumProdsLlistesLocals[$mRow[1]]=array();}
			array_push($mNumProdsLlistesLocals[$mRow[1]],$mRow[0]);
		}
	}

	return $mNumProdsLlistesLocals;
}

//------------------------------------------------------------------------------
function db_cercarProductePerId($db)
{
	global $mPars,$mPropietatsPeriodeLocal;

	$producteId=$mPars['crPrId'];

	$mProductes=array();
	
	if($mPars['selLlistaId']==0)
	{
		if($mPars['veureProductesDisponibles']==1)
		{
			if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where id='".$producteId."' AND llista_id='".$mPars['selLlistaId']."' AND actiu=1",$db))
			{
				//echo "<br> 6653 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    		}
			$mProducte=mysql_fetch_array($result,MYSQL_ASSOC);
			if($mProducte)
			{
				$mProductes[0]=$mProducte;
				$mProductes[0]['visible']=1;
			}
		
		}
		else
		{
			if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where id='".$producteId."' AND llista_id='".$mPars['selLlistaId']."'",$db))
			{
				//echo "<br> 6653 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    		}
			$mProducte=mysql_fetch_array($result,MYSQL_ASSOC);
			if($mProducte)
			{
				$mProductes[0]=$mProducte;
				$mProductes[0]['visible']=1;
			}
		}
    }
	else
	{
		if($mPropietatsPeriodeLocal['comandesLocalsTancades']==0 || $mPropietatsPeriodeLocal['comandesLocalsTancades']==-1)
		{
			if($mPars['veureProductesDisponibles']==1)
			{
				if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where id='".$producteId."' AND llista_id='".$mPars['selLlistaId']."' AND actiu='1'",$db))
				{
					//echo "<br> 6660 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    			}
				$mProducte=mysql_fetch_array($result,MYSQL_ASSOC);
				if($mProducte)
				{
					$mProductes[0]=$mProducte;
					$mProductes[0]['visible']=1;
				}
			}
			else
			{
				if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where id='".$producteId."' AND llista_id='".$mPars['selLlistaId']."'",$db))
				{
					//echo "<br> 6660 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    			}
				$mProducte=mysql_fetch_array($result,MYSQL_ASSOC);
				if($mProducte)
				{
					$mProducte['visible']=1;
					$mProductes[0]=$mProducte;
				}
			}
		}	
		else
		{
			$mProductes_=csv_getProductesLocal($db);
			if($mPars['veureProductesDisponibles']==1)
			{
				$i=0;
				while(list($key,$mProducte_)=each($mProductes_))
				{
					if
					(
						$mProducte_['actiu']==1 
						&& 
						$mProducte_['id']==$producteId 
					)												
					{
						if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
						{
							$mProducte_['visible']=1;
						}
						else
						{
							$mProducte_['visible']=0;
						}
						$mProductes[$i]=$mProducte_;
						$i++;
					}
				}
				reset($mProductes);
				$mPars['numPags']=ceil(count($mProductes)/($mPars['numItemsPag']*1));
				
			}
			else
			{
				$i=0;
				while(list($key,$mProducte_)=each($mProductes_))
				{
					if
					(
						$mProducte_['id']==$producteId 
					)												
					{
						if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
						{
							$mProducte_['visible']=1;
						}
						else
						{
							$mProducte_['visible']=0;
						}
						$mProductes[$i]=$mProducte_;
						$i++;
					}
				}
				reset($mProductes);
				$mPars['numPags']=ceil(count($mProductes)/($mPars['numItemsPag']*1));
			}
		}
	}


	return $mProductes;
}
//------------------------------------------------------------------------------
function db_cercarProductePerText($db)
{
	global $mPars,$mPropietatsPeriodeLocal;
	
	$text=$mPars['crPrText'];
	$mysql1="
				AND 
			(
				LOCATE('".$text."',CONCAT(' ',notes_rebosts))>0 
				|| 
				LOCATE(LOWER('".$text."'),CONCAT(' ',LOWER(notes_rebosts)))>0 
				|| 
				LOCATE(UPPER('".$text."'),CONCAT(' ',UPPER(notes_rebosts)))>0 
				|| 
				LOCATE('".$text."',CONCAT(' ',producte))>0 
				|| 
				LOCATE(LOWER('".$text."'),CONCAT(' ',LOWER(producte)))>0 
				|| 
				LOCATE(UPPER('".$text."'),CONCAT(' ',UPPER(producte)))>0 
				|| 
				LOCATE('".$text."',CONCAT(' ',productor))>0 
				|| 
				LOCATE(LOWER('".$text."'),CONCAT(' ',LOWER(productor)))>0 
				|| 
				LOCATE(UPPER('".$text."'),CONCAT(' ',UPPER(productor)))>0 
				|| 
				LOCATE('".$text."',CONCAT(' ',categoria0))>0 
				|| 
				LOCATE(LOWER('".$text."'),CONCAT(' ',LOWER(categoria0)))>0 
				|| 
				LOCATE(UPPER('".$text."'),CONCAT(' ',UPPER(categoria0)))>0 
				|| 
				LOCATE('".$text."',CONCAT(' ',categoria10))>0 
				|| 
				LOCATE(LOWER('".$text."'),CONCAT(' ',LOWER(categoria10)))>0 
				|| 
				LOCATE(UPPER('".$text."'),CONCAT(' ',UPPER(categoria10)))>0 
				|| 
				LOCATE('".$text."',CONCAT(' ',tipus))>0 
			)";
	$mProductes=array();
	
	if($mPars['selLlistaId']==0)
	{
		if($mPars['veureProductesDisponibles']==1)
		{
			/*echo "<br>select * from ".$mPars['taulaProductes']." 
						where 
						llista_id='".$mPars['selLlistaId']."' 
						AND 
						actiu=1
						".$mysql1;
						*/
			if(!$result=mysql_query("
			select * from ".$mPars['taulaProductes']." 
			where 
			llista_id='".$mPars['selLlistaId']."' 
			AND 
			actiu=1
			".$mysql1." 
			",$db))
			{
				//echo "<br> 6653 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    		}
			$i=0;
			while($mProducte=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
				{
					$mProducte['visible']=1;
				}
				else
				{
					$mProducte['visible']=0;
				}
				$mProductes[$i]=$mProducte;
				$i++;
			}
		
			$mPars['numPags']=ceil(count($mProductes)/($mPars['numItemsPag']*1));
		}
		else
		{
			if(!$result=mysql_query("
			select * from ".$mPars['taulaProductes']." 
			where 
			llista_id='".$mPars['selLlistaId']."' 
			AND 
			LOCATE('".$text."',CONCAT(' ',notes_rebosts))>0
			".$mysql1." 
			",$db))
			{
				//echo "<br> 6653 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    		}
			$i=0;
			while($mProducte=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
				{
					$mProducte['visible']=1;
				}
				else
				{
					$mProducte['visible']=0;
				}
				$mProductes[$i]=$mProducte;
				$i++;
			}
			$mPars['numPags']=ceil(count($mProductes)/($mPars['numItemsPag']*1));
			
		}
    }
	else
	{
		if($mPropietatsPeriodeLocal['comandesLocalsTancades']=='0' || $mPropietatsPeriodeLocal['comandesLocalsTancades']=='-1')
		{
			if($mPars['veureProductesDisponibles']=='1')
			{
				if(!$result=mysql_query("
				select * from ".$mPars['taulaProductes']." 
				where 
				llista_id='".$mPars['selLlistaId']."' 
				AND 
				actiu='1' 
				".$mysql1." 
				",$db))
				{
					//echo "<br> 6660 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    			}
				$i=0;
				while($mProducte=mysql_fetch_array($result,MYSQL_ASSOC))
				{
					if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
					{
						$mProducte['visible']=1;
					}
					else
					{
						$mProducte['visible']=0;
					}
					$mProductes[$i]=$mProducte;
					$i++;
				}
				$mPars['numPags']=ceil(count($mProductes)/($mPars['numItemsPag']*1));
				
			}
			else
			{
				if(!$result=mysql_query("
				select * from ".$mPars['taulaProductes']." 
				where  
				llista_id='".$mPars['selLlistaId']."' 
				".$mysql1." 
				",$db))
				{
					//echo "<br> 6660 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    			}
				$i=0;
				while($mProducte=mysql_fetch_array($result,MYSQL_ASSOC))
				{
					if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
					{
						$mProducte['visible']=1;
					}
					else
					{
						$mProducte['visible']=0;
					}
					$mProductes[$i]=$mProducte;
					$i++;
				}
				$mPars['numPags']=ceil(count($mProductes)/($mPars['numItemsPag']*1));
				
			}
		}	
		else
		{
			$mProductes_=csv_getProductesLocal($db);
			if($mPars['veureProductesDisponibles']==1)
			{
				$i=0;
				while(list($key,$mProducte_)=each($mProductes_))
				{
					$cadena=
					$mProducte_['notes_rebosts'].
					(strToLower($mProducte_['notes_rebosts'])).
					(strToUpper($mProducte_['notes_rebosts'])).
					$mProducte_['producte'].
					(strToLower($mProducte_['producte'])).
					(strToUpper($mProducte_['producte'])).
					$mProducte_['categoria0'].
					(strToLower($mProducte_['categoria0'])).
					(strToUpper($mProducte_['categoria0'])).
					$mProducte_['categoria10'].
					(strToLower($mProducte_['categoria10'])).
					(strToUpper($mProducte_['categoria10'])).
					$mProducte_['tipus'].
					(strToLower($mProducte_['tipus'])).
					(strToUpper($mProducte_['tipus']));
					if
					(
						$mProducte_['actiu']==1 
						&& 
						substr_count($cadena,$text)>0
					)												
					{
						if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
						{
							$mProducte_['visible']=1;
						}
						else
						{
							$mProducte_['visible']=0;
						}
						$mProductes[$i]=$mProducte_;
						$i++;
					}
				}
				reset($mProductes);
				$mPars['numPags']=ceil(count($mProductes)/($mPars['numItemsPag']*1));
				
			}
			else
			{
				$i=0;
				while(list($key,$mProducte_)=each($mProductes_))
				{
					$cadena=
					$mProducte_['notes_rebosts'].
					(strToLower($mProducte_['notes_rebosts'])).
					(strToUpper($mProducte_['notes_rebosts'])).
					$mProducte_['producte'].
					(strToLower($mProducte_['producte'])).
					(strToUpper($mProducte_['producte'])).
					$mProducte_['categoria0'].
					(strToLower($mProducte_['categoria0'])).
					(strToUpper($mProducte_['categoria0'])).
					$mProducte_['categoria10'].
					(strToLower($mProducte_['categoria10'])).
					(strToUpper($mProducte_['categoria10'])).
					$mProducte_['tipus'].
					(strToLower($mProducte_['tipus'])).
					(strToUpper($mProducte_['tipus']));
					if
					(
						substr_count($cadena,$text)>0
					)												
					{
						if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
						{
							$mProducte_['visible']=1;
						}
						else
						{
							$mProducte_['visible']=0;
						}
						$mProductes[$i]=$mProducte_;
						$i++;
					}
				}
				reset($mProductes);
				$mPars['numPags']=ceil(count($mProductes)/($mPars['numItemsPag']*1));
			}
		}
	}


	return $mProductes;
}

//------------------------------------------------------------------------------
function db_obtenirEcosCobratsOpagatsPerCoop($db)
{
	global $mPars,$mRebostsAmbComandaResum,$mComandaPerProductors;
	
	$mEcosCobratsPerCoop=array();

	while(list($grupId,$mRebostAmbComandaResum)=each($mRebostsAmbComandaResum))
	{
		if
		(
			isset($mRebostAmbComandaResum['compte_ecos'])
			&&
			$mRebostAmbComandaResum['compte_ecos']!=''
		)
		{
			if
			(
				!isset($mEcosCobratsPerCoop[substr($mRebostAmbComandaResum['compte_ecos'],0,4)])
			)
			{			
				$mEcosCobratsPerCoop[substr($mRebostAmbComandaResum['compte_ecos'],0,4)]=array();
			}

			if
			(
				!isset($mEcosCobratsPerCoop[substr($mRebostAmbComandaResum['compte_ecos'],0,4)][$mRebostAmbComandaResum['compte_ecos']])
			)
			{			
				$mEcosCobratsPerCoop[substr($mRebostAmbComandaResum['compte_ecos'],0,4)][$mRebostAmbComandaResum['compte_ecos']]=array();
			}

			//segons productes
			//$mEcosCobratsPerCoop[substr($mRebostAmbComandaResum['compte_ecos'],0,4)][$mRebostAmbComandaResum['compte_ecos']]['ecosT']=$mRebostAmbComandaResum['ecosT'];
			//segons forma pagament
				$mFormaPagamentMembres=db_getFormaPagamentMembres($grupId,$db); 
			$mFpagament=explode(',',$mFormaPagamentMembres['f_pagament']);
			$mEcosCobratsPerCoop[substr($mRebostAmbComandaResum['compte_ecos'],0,4)][$mRebostAmbComandaResum['compte_ecos']]['ecosT']=$mFpagament[0]*1;
			$mEcosCobratsPerCoop[substr($mRebostAmbComandaResum['compte_ecos'],0,4)][$mRebostAmbComandaResum['compte_ecos']]['grupCompraId']=$grupId;
		}
	}
	reset($mRebostsAmbComandaResum);

	while(list($perfilId,$mComandaPerProductor)=each($mComandaPerProductors))
	{
		if
		(
			isset($mComandaPerProductor['compte_ecos'])
			&&
			$mComandaPerProductor['compte_ecos']!=''
		)
		{
			if
			(
				!isset($mEcosCobratsPerCoop[substr($mComandaPerProductor['compte_ecos'],0,4)])
			)
			{			
				$mEcosCobratsPerCoop[substr($mComandaPerProductor['compte_ecos'],0,4)]=array();
			}

			if
			(
				!isset($mEcosCobratsPerCoop[substr($mComandaPerProductor['compte_ecos'],0,4)][$mComandaPerProductor['compte_ecos']])
			)
			{			
				$mEcosCobratsPerCoop[substr($mComandaPerProductor['compte_ecos'],0,4)][$mComandaPerProductor['compte_ecos']]=array();
			}

			$mEcosCobratsPerCoop[substr($mComandaPerProductor['compte_ecos'],0,4)][$mComandaPerProductor['compte_ecos']]['ecosTp']=$mComandaPerProductor['ecosTp'];
			$mEcosCobratsPerCoop[substr($mComandaPerProductor['compte_ecos'],0,4)][$mComandaPerProductor['compte_ecos']]['perfilVenId']=$perfilId;
		}
	}
	reset($mComandaPerProductors);
	ksort($mEcosCobratsPerCoop);
	return $mEcosCobratsPerCoop;
}

//------------------------------------------------------------------------------
function db_getGrupsAnteriorsAmbComandaUsuari($db)
{
	global $mPars,$mUsuari;

	$mGrupsAnteriorsAmbComandaUsuari=array();
	$cadenaGrups='';

	//grups d'aquest periode CAB als que la usuaria no pertany en aquest periode i on va fer comanda
	//echo "<br>select rebost from comandes_".$mPars['selRutaSufix']." where usuari_id='".$mPars['usuari_id']."'  order by id DESC";
	if(!$result=mysql_query("select rebost from comandes_".$mPars['selRutaSufix']." where usuari_id='".$mPars['usuari_id']."'  order by id DESC",$db))
	{
		//echo "<br> 7231 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if(substr_count($mUsuari['grups'],','.(substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'))).',')==0)
			{
				$cadenaGrups.=','.(substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'))).',';
			}
		}
	}
	if($cadenaGrups!='')
	{
		//echo "<br>select id,nom,categoria from rebosts_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',id,','),' ".$cadenaGrups."')>0  order by id DESC";
		if(!$result=mysql_query("select id,nom,categoria from rebosts_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',id,','),' ".$cadenaGrups."')>0  order by id DESC",$db))
		{
			//echo "<br> 7251 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
    	}
    	else
    	{
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mGrupsAnteriorsAmbComandaUsuari[$mRow['id']]['nom']=$mRow['nom'];
				$mGrupsAnteriorsAmbComandaUsuari[$mRow['id']]['categoria']=$mRow['categoria'];
			}
		}
	}

	return $mGrupsAnteriorsAmbComandaUsuari;
}

//------------------------------------------------------------------------------
function db_getNumProductesNousPeriode($grupId,$db)
{
	global $mPars;

	$mProds0=array();
	$mProds1=array();
	
	
	$mPerfil=db_getPerfilVinculatAgrup($grupId,$db);
	
	if
	(
		!isset($mPerfil)
		||
		count($mPerfil)==0
	)
	{
		return 0;
	}
	//vd($mPerfil);
	
	$rutaAnterior=$mPars['selRutaSufix']*1-1;
	if(substr(''.$rutaAnterior,2)=='00'){$rutaAnterior--;}

	//productes de la ruta anterior
	if(!$result=mysql_query("select id from  productes_".$rutaAnterior." where SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPerfil['id']."'  order by id DESC",$db))
	{
		//echo "<br> 7432 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		return 0;
   	}
   	else
   	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			array_push($mProds0,$mRow['id']);
		}
	}

	//productes de la ruta actual
	if(!$result=mysql_query("select id from productes_".$mPars['selRutaSufix']." where SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPerfil['id']."'  order by id DESC",$db))
	{
		//echo "<br> 7447 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		return 0;
   	}
   	else
   	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			array_push($mProds1,$mRow['id']);
		}
	}
	//obtenim ids que estan a prods1 i no estan a prods0:

	$mProds2=array_diff($mProds1,$mProds0);
	$numProds=count($mProds2);

/*
if($grupId==24)
{
	echo "<br>select id from  productes_".$rutaAnterior." where SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$mPerfil['id']."'  order by id DESC";
	vd($mProds0);
	vd($rutaAnterior);
	echo "<br>select id from productes_".$mPars['selRutaSufix']." where SUBSTRING(productor,1,LOCATE('-',productor)-1)='".$perfilId."'  order by id DESC";
	vd($mProds1);
	vd($mPars['selRutaSufix']);
	vd($mProds2);
}
*/
	
	return $numProds;
}
?>


		