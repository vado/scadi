<?php

include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "eines.php";
include "html_ajuda1.php";
include "db_ajuda.php";


$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();
$parsChain=makeParsChain($mPars);

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db);
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['informacio_gestor.php']=db_getAjuda('informacio_gestor.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}

$mRebosts=db_getRebosts($db);
$mTeam=getTeam($db);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>"; echo $htmlTitolPags; echo " - Informaci�</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<script type='text/javascript'  language='JavaScript'>
navegador();
missatgeAlerta='';
function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='informacio_gestor.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}
</script>

</head>

<body  bgcolor='".$mColors['body']."' onLoad=\"javascript: if(missatgeAlerta!=''){alert(missatgeAlerta);}\">
";
html_demo('informacio_gestor.php?');
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			<p>(".$mContinguts['form']['titol'].")</p>
			</p>
			</td>
		</tr>
	</table>
	

		<table  align='center'  bgcolor='#ffccaa'  style='width:80%;'   bgcolor='".$mColors['table']."'>
			<tr>
				<td  style=' width:100%;'>
				<table  align='center'   style='width:90%;' >
					<tr>
						<td  style='width:100%;' bgcolor='#FAB384'>
						<table style='width:100%;'>
							<tr>
								<td>
								<p>&nbsp;</p>
								</td>
							
								<td>
								<br><br>
								<p class='titol1'><b><i><u>Informaci� important pels Rebosts</b>:</u></i></p>
								".$mText['info'][0]."
								<p>&#149; Els pagaments cal fer-los preferiblement al comptat en el moment de l'entrega.
								<br> <br>
								Si no �s possible, la CAC ofereix finan�ament a rebosts de confian�a: 50% a un mes, 50% a dos mesos
								per a compres de mes de 100 unitats(euros o ecos). En aquest cas els pagaments caldr� 
								fer-los mitjan�ant transfer�ncia al compte de la CAC:<br>
								<b>".$mParametres['compte_bancari_cac_1']['valor']."</b>
								<br><br>
								Si cap de les opcions es viable contacteu amb la CAC a <b>abastiment@cooperativaintegral.cat</b>
								</p>
								<p> Per a convocar els usuaris del vostre Rebost per a la la recepci� de comandes , recomanem que 
								feu difusi�, enviant 
								la convocatoria a la llista de correu de la vostra ecoxarxa o be enviant-la internament des del CES 
								com a notificaci� (via administrador). 
								<br> D'aquesta manera cada usuari podr� escollir el rebost de la seva ecoxarxa on vol fer la comanda.
								<br>&nbsp;
								<br>&nbsp;
								</td>
						
								<td>
								<p>&nbsp;</p>
								</td>
					
							</tr>
						</table>
						</td>
					</tr>
	
					<tr>
						<td >
						<br><br>&nbsp;
						<p><b><i><u> Sobre l'�s del gestor de comandes:</u></b></i></p>
						
						<p>&bull; per fer la vostra comanda a la CAC com a Rebost, o be actualitzar-la afegint o reduint la llista de productes que demaneu, 
						tant sols heu de seleccionar la quantitat que voleu a la columna 'Quantitat'. Cada producte t� el seu selector de quantitat. 
						Nom�s permet seleccionar quantitats corresponents a m�ltiples del format en unitats del producte. Les quantitats que es poden
						seleccionar tamb� depenen de l'estoc disponible del producte, que es veur� reduit cada vegada que un rebost seleccioni 
						una nova quantitat del producte.
						
						<p>&bull; Al llarg del periode de precomanda podeu anar accedint a aquesta p�gina i actualitzar la vostra
						comanda a l'al�a o a la baixa. Recordeu que intentem fer un control d'estocs, de manera que la comanda
						que tingueu enviada estar� afectant el producte disponible pels altres rebosts.
						".$mText['info'][1]."			
						</p>
						<p>&bull; Abans de guardar la comanda heu de seleccionar una opci� de pagament</p>

						<p>&bull; Per tal de facilitar la gesti� de comandes dels rebosts pels seus usuaris, podeu descarregar la llista de productes en format .ods. (link:
						<b>'llistaproductes.ods'</b>).
						<br>
						Es tracta d'una fulla de c�lcul ja preparada perqu� aneu afegint les comandes que us faran els vostres usuaris. Una vegada tingueu el total 
						podeu fer la comanda com a rebost en aquest web. Us recomanem anar actualitzant la comanda on-line per tal d'assegurar-vos que hi ha producte 
						disponible. L'ideal seria que cada rebost fes una compra a partir d'una previsi�, tal com fa la CAC a nivell global, i anar ajustant en les 
						comandes successives les quantitats de producte demanat, aix� com anar mantenint un estoc de productes a nivell local que donaran vida als rebosts.
						</p>
						
						<p>&bull; Us recomanem que aneu accedint a la p�gina <b>'Qu� estan demanant els altres rebosts?'</b>(link), 
						on podeu saber on anir�n els productes. Podeu posar-vos en contacte amb altres rebosts per acordar la
						compra conjunta d'algun producte (ex: garrafes oli oliva de 25l, o sacs de gra de 25 kg)
						</p>
						<p>&bull; Si est� seleccionat el selector <b>'<input type='checkbox' checked value=''>Veure nom�s els productes disponibles'</b> (a sobre del 
						nom de les columnes, a l'esquerra)	es mostraran nom�s els productes que la CAC incorpora en aquesta comanda. Si no est� seleccionat podeu 
						veure tots els productes que actualment la CAC ha introdu�t a la seva base de dades per incorporar en comandes posteriors.
						</p>
						<p>&bull; A sota el nom de cada columna hi ha dues fletxetes que permeten ordenar els productes per ordre ascendent o descendent segons el 
						contingut de cada columna, ja sigui nombres o lletres.
						</p>
						<p>&bull; Al link \"<b>descarregar pre-comanda actual</b>\", a sobre la taula de productes i a la dreta, podeu descarregar la vostra comanda 
						actual en format .csv. (La versi� completa inclou els detalls del cost de transport)
						</p>

						<br><br>&nbsp;
						</td>
					</tr>

					<tr>
						<td >
						<br>&nbsp;
						<p><b><i><u> El preu dels productes i el cost de transport:</u></b></i></p>
						
						<p>&bull; La CAC pot configurar la llista de preus amb quatre par�metres relatius al cost de transport, amb els corresponents 
						par�metres de % de moneda social:
						<p>1/ <u><i>cost de transport extern corresponent a cada producte</i></u>:
						Es el cost de transport no gestionat per la CAC que es pot assumir incl�s en la distribuci� dels productes de cada comanda. 
						Per exemple, el cas d'un productor que ofereix fer l'entrega del producte a la CAC a partir d'un cert volum de producte i 
						que el cobrar� a tant per km o per kg.
						
						</p>
						<p>2/ <u><i>cost de transport intern corresponent a cada producte</i></u>:
						Es el cost de transport gestionat per la CAC que es pot vincular a un conjunt concret de productes que generen aquest cost, 
						i que s'aplicar� a cada producte per unitat de facturaci�. Per exemple, el cas de la recollida a un productor en concret.
						</p>
						<p>3/ <u><i>cost de transport extern repartit sobre el total de productes</i></u>:
						Es el cost del transport no gestionat per la CAC que es reparteix sobre tots els productes per unitat de facturaci�, de forma que queda 
						repartit i permet reduir el cost als rebosts m�s allunyats.
						</p>
						<p>4/ <u><i>cost de transport intern repartit sobre el total de productes</i></u>:
						Es el cost del transport gestionat per la CAC que es reparteix sobre tots els productes per unitat de facturaci�, de forma que queda 
						repartit i permet reduir el cost als rebosts m�s allunyats.
						</p>
						</p>
						<p>5/ <u><i>cost de transport per enviaments entre rebosts</i></u>:
						<br>
						Es el cost del transport que assumeix un rebost per la rebuda de producte d'un altre rebost uttilitzant la ruta de la CAC.
						<br>
						<br>
						El rebost comprador ha de solicitar a la CAC que vol utilitzar la ruta per rebre producte d'un altra rebost 
						(producte no incl�s en la comanda de la CAC), i ha de comunicar a la CAC quin pes t� aquest producte. La CAC introduir� les dades 
						d'aquest transport al gestor de comandes, de manera que per cada rebost que utilitzi la ruta de la CAC per rebre producte d'un altre 
						rebost, disminuir� el cost de transport per la resta de rebosts.
						<br>
						<br>
						Per tant, podeu enviar productes d'un rebost a l'altre utilitzant el transport de la CAC dins aquesta ruta. En aquest cas, la CAC
						cobrar� el cost proporcional de transport en l'entrega i els rebosts gestionaran el pagament del producte que s'envi�n per la seva
						banda. Per tal de comptar amb aquest servei, la CAC ha de confirmar que podr� fer-se c�rrec del transport (per q�esti� d'espai 
						disponible durant el tram en q�esti�).
						<br>
						   La forma per al c�lcul d'aquest transport es la seg�ent:
						<br>
						<br>						
						- donat que el pressupost est� vinculat a la ruta i es distribueix sobre els kg, i que quan el total de kg aumenta 
						el cost repartit sobre els kg disminueix
						<br>
						<br>
						<i>cost transport per kg intercanvi rebost= uts cost transport ruta / (kg total ruta + <b>kg intercanvi rebost)</b>
						<br>
						<br>
						cost transport intercanvi rebost =  <b>kg intercanvi rebost/2</b> * (uts cost transport ruta / (kg total ruta + <b>kg intercanvi rebost/2)</b>)</i>
						<br>
						<br>
						
						ex:	el rebost de vilafranca envia 235 kg de producte a can cal�ada, (ambd�s son rebosts dins ruta):
						<br>
						<br>
						<i>cost transport per kg intercanvi rebost= 388/(1526+<b>235</b>/2)= 0.27 uts/kg al 17.5% ms</i>
						<br>
						<br>
						<i>cost transport intercanvi rebost =  <b>235</b>/2 *388/(1526+<b>235</b>/2)= 27.7 uts al 17.5% ms</i>
						<br>
						<br>
						* per calcular el cost de transport es considera el producte entregat pel rebost prove�dor com a producte incl�s en la comanda total de la cac, de manera que l'�s que fan els rebosts de la ruta de la CAC s'inclou en la colectivitzaci� del cost de transport, i colabora en la reducci� del cost de transport per tots els rebosts que participen en la compra. Es per aix� que quan es tanqui la comanda el cost de transport es pot veure reduit per tots els rebosts. Per tant, us animem a fer us d'aquest servei CAC, ja que es en benefici de tots.
						   Cada rebost haur� de valorar si li surt mes a compte anar-ho a buscar ell mateix al rebost prove�dor.
						</p>
						<br>
						<br>
						<p>En el cas d'aquesta primera compra de la CAC:
						<br>
						- cost transport extern per kg=0
						<br>
						- cost transport intern per kg=0
						<br>
						- cost transport extern repartit = 0
						<br>
						- cost transport intern repartit = 388 unitats al 17.5% ms
						<br><br>
						* podeu veure aquest valors a la part superior a la dreta de la p�gina de comanda i a la fila de cada producte
						<br><br>&nbsp;
						</td>
					</tr>


					<tr>
						<td>
						<p><b><i><u> resum dels camps que es mostren a la taula de productes:</u></b></i></p>
<p><b>id</b>: identificador provisional del producte (encara no tenim a punt el sistema de codificaci�)
</p>
<p><b>segell ECOCIC</b>: valor del segell ECOCIC del producte, calculat segons la valoraci� que fan la CAB i la productora sobre el producte.
</p>
<p><b>producte</b>: Nom del producte, que pot incloure alguna informaci�  m�s sobre format, productors, etc...
</p>
<p><b>productor</b>: Nom del productor
</p>
<p><b>categoria0</b>: categoria principal del producte
</p>
<p><b>categoria10</b>: categoria secundaria del producte
</p>
<p><b>tipus</b>: etiquetes internes ('dip�sit', 'b�sic', 'semiperible', etc...)
</p>
<p><b>unitat_facturacio</b>: unitat de facturaci� del producte (kg, litres, paquets de x unitats, pots de x ml, etc...)
</p>
<p><b>format</b>: format d'entrega. Determina tamb� la compra m�nima. (Sacs de 25 kg, garrafes de 25 l, caixes de 12 unitats, etc...)
</p>
<p><b>preu</b>: el preu del producte en unitats monetaries
</p>
<p><b>ms</b>: el % en monda social del preu del producte en unitats monetaries
</p>
<p><b>estoc_previst</b>: quantitat que compra la CAC
</p>
<p><b>estoc_disponible</b>: quantitat disponible del producte que encara no s'ha reservat per cap rebost
</p>
<p><b>cost transport extern kg</b>: Cost de transport que no gestiona la CAC corresponent a cada producte
</p>
<p><b>cost_transport_intern_kg</b>: Cost del transport que gestiona la CAC, corresponent a cada producte
</p>
						</p>
						<br><br>&nbsp;
						</td>
					</tr>


				</table>
			</td>
		</tr>
	</table>
";
html_helpRecipient();

echo "

<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='nouGrup.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</div>
	
</body>
</html>
";

?>
	




		