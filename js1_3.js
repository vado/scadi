allowedCharsEmail='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.@';
allowedCharsCompteEcos_lletres='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
emailNeededCharacters='@.';
allowedCharsMobil='0123456789';
allowedCharsCategoriesGrups='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-';
		
function checkFormCrearGrup()
{
	isValid=false;
	missatgeAlerta='';
	
	// camps obligatoris
	value=document.getElementById('i_nomGrup').value;
	if(value.length<3)
	{
		missatgeAlerta+="\nEl camp 'Nom' ha de tenir m�s de 2 car�cters";			
	}
			
	value=document.getElementById('i_codiEcoxarxa').value;
	if(value.length!=4)
	{
		missatgeAlerta+="\nEl camp 'Codi Ecoxarxa' ha de tenir 4 car�cters";			
	}
	else
	{
       	allowedChars=allowedCharsCompteEcos_lletres;
		isValid='false';
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){isValid=true;}
            }
		}
	
	}

	value=document.getElementById('i_adre�a').value;
	if(value.length<3)
	{
		missatgeAlerta+="\nEl camp 'Adre�a' ha de tenir m�s de 3 car�cters";			
	}


	value=document.getElementById('i_email').value;
	if(value.length<5)
    {
		missatgeAlerta+="\nEl camp 'Email' ha de tenir m�s de 4 car�cters";
    }
	else
	{
       	allowedChars=allowedCharsEmail;
		isValid='false';
		caractersNecessaris=2;        	
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){isValid=true;}
				if(value.charAt(i)==allowedChars.charAt(j) && value.charAt(i)=='.'){caractersNecessaris--;}
				if(value.charAt(i)==allowedChars.charAt(j) && value.charAt(i)=='@'){caractersNecessaris--;}
            }
		}
        if(caractersNecessaris>0)
        {
			missatgeAlerta+="\nEl camp 'Email' cont� un valor incorrecte";			
        }
	}

	value=document.getElementById('i_mobil').value;
	if(value.length<9)
    {
		missatgeAlerta+="\nEl camp 'Mobil' ha de tenir 9 car�cters";			
    }
	else
	{
       	allowedChars=allowedCharsMobil;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<9)
        {
			missatgeAlerta+="\nEl camp 'Mobil' cont� un valor incorrecte";			
        }
	}
		
	value=document.getElementById('i_compteEcos').value;
	if(value.length!=0 && value.length!=8)
    {
		missatgeAlerta+="\nEl camp 'Compte Ecos' ha de tenir 0 � 8 car�cters";			
    }
	else if( value.length>0) 
	{
	   	allowedChars=allowedCharsCompteEcos_lletres;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<4;i++)
        {
           for(j=0;j<allowedChars.length;j++)
           {
               if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
           }
		}
	}
	
	value=document.getElementById('i_compteCieb').value;
	if(value.length!=0 && value.length!=4)
    {
		missatgeAlerta+="\nEl camp 'Compte Cieb' ha de tenir 0 � 4 car�cters";			
    }
	else if (value.length>0)
	{
	   	allowedChars=allowedCharsMobil;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<4;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<4)
        {
			missatgeAlerta+="\nEl camp 'Compte Cieb' cont� un valor incorrecte";			
        }
	}
		
	if (missatgeAlerta.length==0)
	{
		return true;
	}
  return false;
}	


function checkFormGuardarGrup()
{
	isValid=false;
	missatgeAlerta='';

	
	// camps obligatoris
	value=document.getElementById('i_nomGrup').value;
	if(value.length<3)
	{
		missatgeAlerta+="\nEl camp 'Nom' ha de tenir m�s de 2 car�cters";			
	}
			
	value=document.getElementById('i_codiEcoxarxa').value;
	if(value.length!=4)
	{
		missatgeAlerta+="\nEl camp 'Codi Ecoxarxa' ha de tenir 4 car�cters";			
	}
	else
	{
       	allowedChars=allowedCharsCompteEcos_lletres;
		isValid='false';
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){isValid=true;}
            }
		}
	
	}

	value=document.getElementById('i_adre�a').value;
	if(value.length<3)
	{
		missatgeAlerta+="\nEl camp 'Adre�a' ha de tenir m�s de 3 car�cters";			
	}

	value=document.getElementById('sel_responsableGrupId').value;
	if(value.length=='')
	{
		missatgeAlerta+="\nCal seleccionar un responsable de grup";			
	}

	value=document.getElementById('i_email').value;
	if(value.length<5)
    {
		missatgeAlerta+="\nEl camp 'Email' ha de tenir m�s de 4 car�cters";
    }
	else
	{
       	allowedChars=allowedCharsEmail;
		isValid='false';
		caractersNecessaris=2;        	
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){isValid=true;}
				if(value.charAt(i)==allowedChars.charAt(j) && value.charAt(i)=='.'){caractersNecessaris--;}
				if(value.charAt(i)==allowedChars.charAt(j) && value.charAt(i)=='@'){caractersNecessaris--;}
            }
		}
        if(caractersNecessaris>0)
        {
			missatgeAlerta+="\nEl camp Email cont� un valor incorrecte";			
        }
	}

	value=document.getElementById('i_mobil').value;
	if(value.length<9)
    {
		missatgeAlerta+="\nEl camp Mobil ha de tenir 9 car�cters";			
    }
	else
	{
       	allowedChars=allowedCharsMobil;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<9)
        {
			missatgeAlerta+="\nEl camp 'Mobil' cont� un valor incorrecte";			
        }
	}
		
	value=document.getElementById('i_compteEcos').value;
	if(value.length!=0 && value.length!=8)
    {
		missatgeAlerta+="\nEl camp 'Compte Ecos' ha de tenir 0 � 8 car�cters";			
    }
	else if( value.length>0) 
	{
	   	allowedChars=allowedCharsCompteEcos_lletres;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<4;i++)
        {
           for(j=0;j<allowedChars.length;j++)
           {
               if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
           }
		}
	}
	
	value=document.getElementById('i_compteCieb').value;
	if(value.length!=0 && value.length!=4)
    {
		missatgeAlerta+="\nEl camp 'Compte Cieb' ha de tenir 0 � 4 car�cters";			
    }
	else if (value.length>0)
	{
	   	allowedChars=allowedCharsMobil;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<4;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<4)
        {
			missatgeAlerta+="\nEl camp 'Compte Cieb' cont� un valor incorrecte";			
        }
	}
		
	if (missatgeAlerta.length==0)
	{
		return true;
	}
  return false;
}	


function checkFormNovaCategoria()
{
	isValid=false;
	missatgeAlerta='';
	
	// camps obligatoris
	value=document.getElementById('i_novaCategoriaGrup').value;
	if(value.length<1)
	{
		missatgeAlerta+="\nEl camp Categoria ha de tenir m�s de 0 car�cters";			
	}
	else
	{
       	allowedChars=allowedCharsCategoriesGrups;
		$_ok=0;
		//check for prohibited characters
       	for(i=0;i<value.length;i++)
        {
            for(j=0;j<allowedChars.length;j++)
            {
                if(value.charAt(i)==allowedChars.charAt(j)){$_ok++;}
            }
		}
        if($_ok<value.length)
        {
			missatgeAlerta+="\nEl camp Categoria cont� car�cters no permesos";			
			missatgeAlerta+="\n[car�cters permesos: "+allowedCharsCategoriesGrups;			
        }
	}
	if (missatgeAlerta.length==0)
	{
		return true;
	}
  return false;
}	


function enviarFparsGrupEditar(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='nouGrup.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}


function veureContrasenyaFormNouGrup()
{
	val2307141227=document.getElementById('cb_veureContrasenya').value;
	if(val2307141227==0)
	{
		val2307141227=1;
		document.getElementById('cb_veureContrasenya').value=1;
		document.getElementById('i_contrasenya').type='text';
		document.getElementById('i_contrasenya').checked=true;
	}
	else
	{
		document.getElementById('cb_veureContrasenya').value=0;
		document.getElementById('i_contrasenya').type='password';
		document.getElementById('i_contrasenya').checked=false;
	}
	
	return;	
}
		

function modificarDadesGrup(opId)
{
	val12591610=document.getElementById('op_'+opId).innerHTML;
	respUsuari=val12591610.substring(0,val12591610.indexOf('(')); //nom usuari
	respMailMobil=val12591610.substring(val12591610.indexOf('(')+1,val12591610.indexOf(')')); //mail+mobil
	respMail=respMailMobil.substring(0,respMailMobil.indexOf(';')); //mail
	respMobil=respMailMobil.substring(respMailMobil.indexOf(';')+1,respMailMobil.length); //mail
	
	//chain=respUsuari+'\n'+respMailMobil+'\n'+respMail+'\n'+respMobil;
	document.getElementById('i_email').value=respMail;
	document.getElementById('i_mobil').value=respMobil;
	document.getElementById('i_email').style.borderColor='Red';
	document.getElementById('i_mobil').style.borderColor='Red';
	
	//alert(chain);

	return;
}
		
		
		
					