<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "eines.php";
include "db_comptes.php";
include "db_incidencies.php";
include "db_distribucioGrups.php";
include "db_gestioMagatzems.php";
include "html_ajuda1.php";
include "db_ajuda.php";


//--------------------------------------------------------------------

function html_nomsColumnes()
{
	echo "
		<tr>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Dades:</b></p></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Consum:</b></p></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Producci�:</b></p></td></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><p><b>Balan�:</b></p></td></td>
			<td  bgcolor='#eeeeee' width='15%' valign='bottom'><a title=\"EC - Estoc CAC: Import dels productes d'aquest productor  a l'inventari actual de la CAC\"><p class='albara'>(GE)<br><b>Estoc CAC 'dip�sit':</b></p></a></td>
			<td  bgcolor='orange' width='15%' valign='bottom'><a title=\"EC - Estoc CAC: Si la quantitat de productes en estoc es major que la de les reserves d'aquests productes, la quantitat en estoc que es comptabilitza es igual a les reserves. Es mostra la resta entre el 'Balan�' i 'Estoc CAC 'dip�sit'. Aquest es el balan� final per aquest periode. Les dades son te�riques ja que amb la resoluci� d'incid�ncies poden variar en el transcurs d'un mes.\"><p class='albara'>(GE)<br><b>Recompte:</b></p></a></td>
		</tr>
	";

	return;
}

//--------------------------------------------------------------------


$mParsCAC=array();
$quantitatTotalCac=0;
$quantitatTotalRebosts=0;
$mCr=array();
$missatgeAlerta='';
$mMissatgeAlerta['missatge']='';
$mMissatgeAlerta['result']='';

$mProblemaAmb=array();

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();
unset($mPars['compte_ecos']);
unset($mPars['compte_cieb']);


$parsChain=makeParsChain($mPars);
$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}
$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];

getConfig($db); //inicialitza variables anteriors;

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=0;
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';

	$mPars['fPagamentUms']=0;
	$mPars['fPagamentEcos']=0;
	$mPars['fPagamentEb']=0;
	$mPars['fPagamentEu']=0;
	$mPars['fPagamentEuMetalic']=0;
	$mPars['fPagamentEuTransf']=0;
	$mPars['fPagamentEcosIntegralCES']=0;

	$mPars['fPagamentEcosPerc']=0;
	$mPars['fPagamentEbPerc']=0;
	$mPars['fPagamentEuPerc']=0;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['paginacio']=-1;
	if(!isset($mPars['pagS'])){$mPars['pagS']=0;}
	if(!isset($mPars['pagFiltre'])){$mPars['pagFiltre']=$mPars['sortBy'].$mPars['veureProductesDisponibles'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vProductor'].$mPars['vSubCategoria'].$mPars['vCategoria'];}
	if(!isset($mPars['numProdsPag'])){$mPars['numProdsPag']=10;}
	if(!isset($mPars['numPags'])){$mPars['numPags']=0;} 

post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['html.php']=db_getAjuda('html.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);
$mAjuda['gestioProductes.php']=db_getAjuda('gestioProductes.php',$db);

$llistaId=@$_GET['sL']; //selector de ruta
if(isset($llistaId))
{
	$mPars['selLlistaId']=$llistaId;
}
else
{
	$llistaId=@$_POST['i_selLlista'];

	if(isset($llistaId))
	{
		$mPars['selLlistaId']=$llistaId;
	}
	else
	{
		$mPars['selLlistaId']=0;
	}
}
$mPeriodesLocalsInfo=db_getPeriodesLocalsInfo($db);

$mRuta=explode('_',$mPars['selRutaSufix']);
//*v36-condicio
if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

if($mPars['selLlistaId']==0)
{
	$mRuta=explode('_',$mPars['selRutaSufix']);
	if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

	$mPars['taulaProductes']='productes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandes']='comandes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandesSeg']='comandes_seg';
	$mPars['taulaIncidencies']='incidencies_'.$mPars['selRutaSufix'];
	if(count($mRuta)==2)//periode especial
	{
		$mPars['taulaProductors']='productors_'.$mPars['selRutaSufix'];
	}
	else
	{
		$mPars['taulaProductors']='productors';
	}
	
}
else
{
	$mPars['taulaProductes']='productes_grups';
	$mPars['taulaProductors']='productors';
	$mPars['taulaComandes']='comandes_grups';
	$mPars['taulaComandesSeg']='comandes_seg_grups';
	$mPars['taulaIncidencies']='incidencies_grups';

}

$plId=@$_GET['plId'];
if(isset($plId))
{
	$mPars['perfil_id']=$plId;
}

$mUsuari=db_getUsuari($mPars['usuari_id'],$db);

if
(
	!checkLogin($db)
	||
	(
		substr_count($mUsuari['perfils_productor'],','.$plId.',')==0
		&&
		$mPars['nivell']=='usuari'
	)
	||
	$mPars['nivell']=='visitant'
)
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}




$mProductes=db_getProductes2($db);
$mUsuarisRef=db_getUsuarisRef($db);
//*v36-18-12-15 modificar funcio assignacio
$mPerfilsRef=db_getPerfilsRef($db); //agafa $mPars['perfil_id'] si no es buit
$mPars['grup_id']=$mPerfilsRef[$mPars['selPerfilRef']]['grup_vinculat'];

$mPropietatsGrup=@getPropietatsGrup($mGrupsRef[$mPars['selLlistaId']]['propietats']);

$estatPerfilAllistaLocal='';


if($mPars['selLlistaId']!=0)
{
	if(substr_count($mPropietatsGrup['idsPerfilsActiusLocal'],','.$mPars['selPerfilRef'].',')>0)
	{
		$estatPerfilAllistaLocal="<font color='green'><b>ACTIU</b></font>";
	}
	else
	{
		$estatPerfilAllistaLocal="<font color='red'><b>INACTIU</b></font>";
	}
}

$mGrupsRef=db_getGrupsRef($db);  //agafa grup_id de pars si no es buit
$mLlistesRef=$mGrupsRef;//db_getLlistesRef($db);

$mPerfil=db_getPerfil($db);
$mPerfilResp=db_getUsuari($mPerfil['usuari_id'],$db);
$mGrupsAssociatsRef=db_getGrupsAssociatsPerfilRef($db);

//if($mPars['usuari_id']==$mPerfilResp['id'])
//{
	if($mPars['selLlistaId']==0)
	{
		$mGrupsRef_=$mGrupsAssociatsRef;
		//$mGrupsRef_[$mPerfil['grup_vinculat']]=$mGrupsRef[$mPerfil['grup_vinculat']];
		$mGrupsRef=$mGrupsRef_;
		$mGrupsRef[0]=array('id'=>0,'nom'=>'CAC');
		//$mLlistesRef[0]=array('id'=>0,'nom'=>'CAC');
		$mLlistesRef=$mGrupsRef;
	}
	else
	{
		$mGrupsRef=$mGrupsAssociatsRef;
		$mGrupsRef[0]=array('id'=>0,'nom'=>'CAC');
		$mLlistesRef=$mGrupsRef;//db_getLlistesRef($db);
		$mPerfilsRef[$mPerfil['id']]=$mPerfil;
	}
//}


	$mPars['paginacio']=0;
$mRebostsAmbComandaResum=db_getRebostsAmbComandaResum2($db); 



/*
if($mPars['nivell']=='sadmin')
{
	$afp_grupId=@$_POST['i_afp_grupId'];
	if(isset($afp_grupId) && $afp_grupId!='')
	{
		$mMissatgeAlerta=db_actualitzarFormesPagamentUsuaris(1,$afp_grupId,$db);
	}
	else
	{
		$afp_grupSid=@$_POST['i_afp_grupSid'];
		if(isset($afp_grupSid) && $afp_grupSid=='TOTS')
		{
			$mMissatgeAlerta=db_actualitzarFormesPagamentGrups(1,$db);
		}
	}
}
*/

$mRutesSufixes=getRutesSufixes($db);
//*v36 5-1-16 assignacio
$mPeriodesInfo=db_getPeriodesInfo($db);

$mComandaPerProductors=db_getComandaPerProductors($db);
$mPeticionsGrup=getPeticionsGrup();
$mComptesGrup=db_getComptesGrup($db);


echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>"; echo $htmlTitolPags; echo " - Comptes GRUP</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_comptes.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
ruta=".$ruta.";
</SCRIPT>
</head>

<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('comptesGrup.php?');
echo "
	<table align='center' style='width:90%'  bgcolor='".$mColors['table']."'>
	<tr>
		<td style='width:100%;' align='center'>
		<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
		".$mContinguts['index']['titol1']."</b>
		</td>
	</tr>
</table>

";
$mRutesSufixes=getRutesSufixes($db);
//*v36 5-1-16 assignacio
$mPeriodesInfo=db_getPeriodesInfo($db);

//*v36.5-funcio call
if($mPars['selLlistaId']==0)
{
	echo "
<center><p style='font-size:16px;'>[ Ruta <b>".$mPars['selRutaSufix']."</b>: Gesti� de Productes ]</p></center>
<table   width='50%' align='center'  bgcolor='".$mColors['table']."'>
	<tr>
		<td align='center' valign='top' width='100%'>
		<table width='100%' align='center' >
			<tr>
				<td width='50%' align='right' valign='top'>
	";
	mostrarSelectorLlistaPerProductores('comptesGrup.php','comptesGrupLocal.php',$db);
	echo "
				</td>
				<td width='50%' align='left'  valign='top'>
	";
	mostrarSelectorRuta(1,'comptesGrup.php');
	echo "
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
	";
}
else
{
	echo "
<table   width='50%' align='center' bgcolor='".$mColors['table']."'>
	<tr>
		<td align='center' valign='top' width='100%'>
		<table width='100%' align='center' >
			<tr>
				<td width='50%' align='right' valign='top'>
	";
	mostrarSelectorLlistaPerProductores('comptesGrup.php','comptesGrupLocal.php',$db);
	echo "
				</td>
				<td width='50%' align='left'  valign='top'>
	";
	mostrarSelectorPeriodeLocalInfo(1);
	echo "
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
	";
}
echo "


<table width='90%' align='center' bgcolor='".$mColors['table']."'>
	<tr>
		<td id='td_missatgeAlerta' style=' width:100%;' align='center'  valign='bottom'>
		".$mMissatgeAlerta['missatge']."
		</td>
	</tr>
</table>

<table align='center' width='90%' bgcolor='".$mColors['table']."'>
	<tr>
		<td>
		<p>Perfil de productor: <b>".(urldecode($mPerfil['projecte']))."</b> 
		";
		if($mPars['selLlistaId']!=0)
		{
			echo " 
			[".$estatPerfilAllistaLocal."] ".(html_ajuda1($mAjuda['gestioProductes.php'][1]))."
			";
		}
		echo "
		</p>
		<p>
		Usuari:<b> ".(urldecode($mPars['usuari']))."</b> (".$mPars['nivell'].")
		<br>
		<font style='font-size:10px;' >
		Funcions: 
		";
		if($mUsuari['id']==$mPerfil['usuari_id'])
		{
			echo "
			Responsable del Perfil
			";
		}
		
		if($mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'])
		{
			echo "
			Responsable de la llista del grup
			";
		}
		
		echo "
		</p>
		</td>

		<td valign='bottom' style='width:25%;'>
		</td>
		
		<td valign='middle' align='left' style='width:25%;'>
		<div style='visibility:"; if(strlen($missatgeAlerta)>0){echo "inherit";}else{echo "hidden";} echo "; width:100%; height:70px; overflow-y:scroll; overflow-x:hidden;'>
		<p style='text-align:left;'>".$missatgeAlerta."</p>
		</div>
		</td>
	</tr>
</table>
<br>

<table align='center' width='90%'  bgcolor='".$mColors['table']."'>
	<tr>
		<td width='50%' align='left'>
	";
	if
	(
		$mPars['selRutaSufix']*1>=$mParametres['periodeIniciRecolzamentSelectiu']['valor']
		&&
		$mComptesGrup['saldo_ecos_r']>0
	)
	{
		echo "
		<p>Ecos recolzats utilitzats: ".(number_format($mComptesGrup['utilitzat_ecos_r'],2,'.',''))."/".(number_format($mComptesGrup['saldo_ecos_r'],2,'.',''))."<p>
		";
	}
	echo "	
		</td>
		<td width='50%' align='right'>
		<p>".(date('H:i:s d/m/Y'))."<p>
		</td>
	</tr>
</table>

<table align='center' bgcolor='#dddddd' width='90%'  bgcolor='".$mColors['table']."'>
";

html_nomsColumnes();

$contLinies=0;

$kgT=0;
$umsTT=0;
$euTT=0;
$ebTT=0;
$ecosTT=0;
$umsTTfp=0;

$cAp_umsT=0;

$cAp_umsTT=0;

$cAp_umsCtT=0;
$cAp_ecosCtT=0;
$cAp_eurosCtT=0;

$cAp_umsFdT=0;
$cAp_ecosFdT=0;
$cAp_eurosFdT=0;

$sumaAbonamentsCarrecsUmsTT=0;
$sumaAbonamentsCarrecsEcosTT=0;
$sumaAbonamentsCarrecsEurosTT=0;

$cont=0;

$sumaAbonamentsCarrecsEcosT=0;
$sumaAbonamentsCarrecsEbT=0;
$sumaAbonamentsCarrecsEuT=0;
$sumaAbonamentsCarrecsUmsT=0;
		
$mColorLinia['haProduit']='#CEF6FB';
$mColorLinia['haProduit2']='#77ffff';
$mColorLinia['haConsumit']='#DCFBCE';
$mColorLinia['haProduitIconsumit']='#F1CEFB';
$colorLinia='#ffffff';

$cpUms=0; //import distribucio cac a grups segons forma pagament dels grups, inclou abonaments i c�rrecs (ums)
$cpEcos=0;
$cpEb=0;
$cpEu=0;

$cpUmsT=0; //Total imports distribucio cac a grups segons forma pagament dels grups, inclou abonaments i c�rrecs (ums)
$cpEcosT=0;
$cpEbT=0;
$cpEuT=0;

$GEdistribUmsT=0; //GE- Total imports distribucio cac a grups segons % ms productes (ums)
$GEdistribEcosT=0;
$GEdistribEbT=0;
$GEdistribEuT=0;

$cDp_umsT=0;
$cDp_ecosT=0;
$cDp_eurosT=0;
$cDp_umsCtT=0;
$cDp_ecosCtT=0;
$cDp_eurosCtT=0;
$cDp_umsFdT=0;
$cDp_ecosFdT=0;
$cDp_eurosFdT=0;

	$cAp_umsT=0;
	$cAp_ecosT=0;
	$cAp_eurosT=0;

	$invUms=0.0;
	$invEcos=0.0;
	$invEuros=0.0;
		
	$invUmsT=0.0;
	$invEcosT=0.0;
	$invEurosT=0.0;

$mMagatzems=db_getMagatzems($db);

$mInventariGT=db_getInventari('0',$db);

$mProductesJaEntregats=db_getProductesJaEntregats2($db);
$mProductesJaEntregatsTot=getProductesJaEntregatsTOT($mProductesJaEntregats);
$totalJaEntregatUms=0;
while(list($grupRef,$mGrup)=each($mGrupsRef))
{
	if
	(
		(
			$mPars['nivell']=='sadmin'
			||
			$mPars['nivell']=='admin'
			||
			$mPars['nivell']=='coord'
			||
			$mPars['usuari_id']==$mPerfilResp['id']
		)
		&&
		$grupRef==$mPerfil['grup_vinculat']
	)
	{
	
	$haProduit=0;
	$haConsumit=0;

	$totalEnEstocUms=0;
	$totalEnEstocEcos=0;
	$totalEnEstocEuros=0;

	$totalQuedaEnEstocUms=0;
	$totalQuedaEnEstocEcos=0;
	$totalQuedaEnEstocEuros=0;
	
	$sumaAbonamentsCarrecsUmsT=0;	

	if($grupRef!=0) //si es cac no es suma
	{
		//consum
		if(!isset($mRebostsAmbComandaResum[$grupRef]))
		{
			$mRebostsAmbComandaResum[$grupRef]['kgT']=0;
			$mFormaPagament=array();
			$mFormaPagamentMembres=array();
			$mPars['fPagamentEcos']=0;
			$mPars['fPagamentEb']=0;
			$mPars['fPagamentEu']=0;
			$mPars['fPagamentUms']=0;
			$mPars['fPagamentEcosPerc']=100;
			$mPars['fPagamentEbPerc']=0;
			$mPars['fPagamentEuPerc']=0;

			$cDp_ums=0;
			$cDp_ecos=0;
			$cDp_euros=0;
			$cDp_umsCt=0;
			$cDp_ecosCt=0;
			$cDp_eurosCt=0;
			$cDp_umsFd=0;
			$cDp_ecosFd=0;
			$cDp_eurosFd=0;

			
			$mRebostsAmbComandaResum[$grupRef]['umsT']=0;
			$mRebostsAmbComandaResum[$grupRef]['ecosT']=0;
			$mRebostsAmbComandaResum[$grupRef]['eurosT']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctkTums']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctrTums']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctkTecos']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctrTecos']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']=0;
			$mRebostsAmbComandaResum[$grupRef]['ctrTeuros']=0;
			$mRebostsAmbComandaResum[$grupRef]['umstFDC']=0;
			$mRebostsAmbComandaResum[$grupRef]['ecostFDC']=0;
			$mRebostsAmbComandaResum[$grupRef]['eurostFDC']=0;
			$mRebostsAmbComandaResum[$grupRef]['compte_ecos']='';
			$mRebostsAmbComandaResum[$grupRef]['compte_cieb']='';
			$mRebostsAmbComandaResum[$grupRef]['propietats']='';
		}
		else
		{
			$cDp_ums=-1*$mRebostsAmbComandaResum[$grupRef]['umsT'];
			$cDp_ecos=-1*$mRebostsAmbComandaResum[$grupRef]['ecosT'];
			$cDp_euros=-1*$mRebostsAmbComandaResum[$grupRef]['eurosT'];
			$cDp_umsCt=-1*($mRebostsAmbComandaResum[$grupRef]['ctkTums']+$mRebostsAmbComandaResum[$grupRef]['ctrTums']);
			$cDp_ecosCt=-1*($mRebostsAmbComandaResum[$grupRef]['ctkTecos']+$mRebostsAmbComandaResum[$grupRef]['ctrTecos']);
			$cDp_eurosCt=-1*($mRebostsAmbComandaResum[$grupRef]['ctkTeuros']+$mRebostsAmbComandaResum[$grupRef]['ctrTeuros']);
			$cDp_umsFd=-1*$mRebostsAmbComandaResum[$grupRef]['umstFDC'];
			$cDp_ecosFd=-1*$mRebostsAmbComandaResum[$grupRef]['ecostFDC'];
			$cDp_eurosFd=-1*$mRebostsAmbComandaResum[$grupRef]['eurostFDC'];

			$cDp_umsT-=$mRebostsAmbComandaResum[$grupRef]['umsT'];
			$cDp_ecosT-=$mRebostsAmbComandaResum[$grupRef]['ecosT'];
			$cDp_eurosT-=$mRebostsAmbComandaResum[$grupRef]['eurosT'];
			$cDp_umsCtT-=$mRebostsAmbComandaResum[$grupRef]['ctkTums']+$mRebostsAmbComandaResum[$grupRef]['ctrTums'];
			$cDp_ecosCtT-=$mRebostsAmbComandaResum[$grupRef]['ctkTecos']+$mRebostsAmbComandaResum[$grupRef]['ctrTecos'];
			$cDp_eurosCtT-=$mRebostsAmbComandaResum[$grupRef]['ctkTeuros']+$mRebostsAmbComandaResum[$grupRef]['ctrTeuros'];
			$cDp_umsFdT-=$mRebostsAmbComandaResum[$grupRef]['umstFDC'];
			$cDp_ecosFdT-=$mRebostsAmbComandaResum[$grupRef]['ecostFDC'];
			$cDp_eurosFdT-=$mRebostsAmbComandaResum[$grupRef]['eurostFDC'];

			$haConsumit=1;
		}
		$mPars['grup_id']=$grupRef;
		$mRebost=db_getRebost($db);
		$mFormaPagament=db_getFormaPagament($db); // afegeix info a $mPars
		$mFormaPagamentMembres=db_getFormaPagamentMembres($grupRef,$db);
		//revisio forma pagament grup guardada:
		if
		(
			$mRebostsAmbComandaResum[$grupRef]['umsT']==0
			&&
			$mFormaPagamentMembres['fPagamentUms']!=0
		)
		{
			$grupNoHaGuardatFpagament=true;
		}
		else
		{
			$grupNoHaGuardatFpagament=false;
		}
		$mAbonamentsCarrecs=db_getAbonamentsCarrecs($mRebost['id'],$db); // nom�s de la cac a� grup
		
		$mPars['vRutaIncd']=$mPars['selRutaSufix']; // per compatibilitzar amb db_getIncidencies();
		$mPars['vEstatIncd']='pendent'; // per compatibilitzar amb db_getIncidencies();
		$mPars['vGrupIncd']=$grupRef;
		$mIncidenciesIncd=db_getIncidencies(true,'incd',' * ',$db);		
		$mPars['vEstatIncd']='aplicat'; // per compatibilitzar amb db_getIncidencies();
		$mIncidenciesAbca=db_getIncidencies(true,'abCa',' * ',$db);	
		$incidenciesIncdText='';
		$incidenciesAbcaText='';
		$productesEspecialsText='';
		if(count($mIncidenciesIncd)>0){	$incidenciesIncdText="<font style='color:#ff0000;'><b>".(count($mIncidenciesIncd))."</b></font>";}
		else {	$incidenciesIncdText="<font style='color:#000000;'><b>".(count($mIncidenciesIncd))."</b></font>";}
		if(count($mIncidenciesAbca)>0){	$incidenciesAbcaText="<font style='color:#0000aa;'><b>".(count($mIncidenciesAbca))."</b></font>";}
		else {	$incidenciesAbcaText="<font style='color:#000000;'><b>".(count($mIncidenciesAbca))."</b></font>";}

		if(@$mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials']*1>0)
		{
			$productesEspecialsText="
			<p onClick=\"enviarFpars('resumProductesEspecials.php?gRef=".$mRebost['ref']."','_blank');\" style='cursor:pointer;'><u>comandes&nbsp;productes&nbsp;especials</u>&nbsp;&nbsp;[<font style='color:#ff0000;'><b>".$mRebostsAmbComandaResum[$grupRef]['comandesProductesEspecials']."</b></font>]</p>
			";
		}
		
		
		//produccio
		$mPerfilVinculat=db_getPerfilVinculatAgrup($grupRef,$db);
		$cAp_ums=0;
		$cAp_ecos=0;
		$cAp_euros=0;
		$cAp_umsCt=0;
		$cAp_ecosCt=0;
		$cAp_eurosCt=0;
		$cAp_umsFd=0;
		$cAp_ecosFd=0;
		$cAp_eurosFd=0;

		$invUms=0.0;
		$invEcos=0.0;
		$invEuros=0.0;
		while(list($producteId,$mComandaAproductor)=@each($mComandaPerProductors[$mPerfilVinculat['id']]))
		{
			$cAp_ums+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
			$cAp_ecos+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
			$cAp_euros+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
			/*
			$cAp_umsCt+=$mComandaAproductor['quantitatT']*$mComandaAproductor['pes']*$mComandaAproductor['cost_transport_intern_kg'];
			$cAp_ecosCt+=$mComandaAproductor['quantitatT']*$mComandaAproductor['pes']*$mComandaAproductor['cost_transport_intern_kg']*$mComandaAproductor['ms_ctik']/100;
			$cAp_eurosCt+=$mComandaAproductor['quantitatT']*$mComandaAproductor['pes']*$mComandaAproductor['cost_transport_intern_kg']*(100-$mComandaAproductor['ms_ctik'])/100;
			$cAp_umsFd+=$mComandaAproductor['preu']*$mParametres['FDCpp']['valor']/100;
			$cAp_ecosFd-=$mComandaAproductor['preu']*($mParametres['FDCpp']['valor']/100)*$mParametres['msFDCpp']['valor']/100;
			$cAp_eurosFd-=$mComandaAproductor['preu']*($mParametres['FDCpp']['valor']/100)*(100-$mParametres['msFDCpp']['valor'])/100;
			*/

				//productes 'dip�sit' en estoc
				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0 && $mPerfilVinculat['id']!='63')
				{
					if(!isset($mInventariGT['inventariRef'][$producteId])){$mInventariGT['inventariRef'][$producteId]=0;}
					
					if(!isset($mProductesJaEntregatsTot[$producteId])){$mProductesJaEntregatsTot[$producteId]=0;}
					
					if($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId]>=$mComandaAproductor['quantitatT'])
					{
						$invUms-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
						$invEcos-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEuros-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;

						$totalQuedaEnEstocUms+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId]-$mComandaAproductor['quantitatT'])*$mComandaAproductor['preu'];
						$totalQuedaEnEstocEcos+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId]-$mComandaAproductor['quantitatT'])*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$totalQuedaEnEstocEuros+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId]-$mComandaAproductor['quantitatT'])*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
					else
					{
						$invUms-=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu'];
						$invEcos-=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEuros-=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
					$totalEnEstocUms+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu'];
					$totalEnEstocEcos+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
					$totalEnEstocEuros+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
				}
		}
		if($cAp_ums>0){$haProduit=1;}
		
		$colorLinia='#ffffff';
		if($haProduit && $haConsumit){$colorLinia=$mColorLinia['haProduitIconsumit'];}
		else
		if($haConsumit){$colorLinia=$mColorLinia['haConsumit'];}
		else
		if($haProduit){$colorLinia=$mColorLinia['haProduit'];}



		//nom�s mostrar linies amb algun valor diferent de zero


			// suma abonaments/carrecs:
			$sumaAbonamentsCarrecsEcos=0;
			$sumaAbonamentsCarrecsEb=0;
			$sumaAbonamentsCarrecsEu=0;
			$sumaAbonamentsCarrecsUms=0;
			while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
			{
				if($mAbonamentCarrec['tipus']=='abonamentCAC')
				{
					$sumaAbonamentsCarrecsEcos+=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb+=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu+=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms+=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
				else if($mAbonamentCarrec['tipus']=='carrecCAC')
				{
					$sumaAbonamentsCarrecsEcos-=$mAbonamentCarrec['producte_id'];
					$sumaAbonamentsCarrecsEb-=$mAbonamentCarrec['demanat'];
					$sumaAbonamentsCarrecsEu-=$mAbonamentCarrec['rebut'];
					$sumaAbonamentsCarrecsUms-=$mAbonamentCarrec['producte_id']+$mAbonamentCarrec['demanat']+$mAbonamentCarrec['rebut'];
				}
			}
			
				$sumaAbonamentsCarrecsEcosT+=$sumaAbonamentsCarrecsEcos;
				$sumaAbonamentsCarrecsEbT+=$sumaAbonamentsCarrecsEb;
				$sumaAbonamentsCarrecsEuT+=$sumaAbonamentsCarrecsEu;
				$sumaAbonamentsCarrecsUmsT+=$sumaAbonamentsCarrecsUms;
//*v36-16-12-15 modificar 1 assignacio
				$cpUms=-1*($mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu'])+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
				$cpEcos=-1*$mPars['fPagamentEcos']+$sumaAbonamentsCarrecsEcos;
//*v36-16-12-15 modificar 1 assignacio
				$cpEb=-1*$mPars['fPagamentEb']+$sumaAbonamentsCarrecsEb;
				$cpEu=-1*$mPars['fPagamentEu']+$sumaAbonamentsCarrecsEu;

				$GEdistribUms=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
				$GEdistribEcos=$cDp_ecos+$cDp_ecosCt+$cDp_ecosFd+$sumaAbonamentsCarrecsEcos;
				$GEdistribEb=0+$sumaAbonamentsCarrecsEb;
				$GEdistribEu=$cDp_euros+$cDp_eurosCt+$cDp_eurosFd+$sumaAbonamentsCarrecsEu;



	if($sumaAbonamentsCarrecsUms!=0 || $cpUms!=0 || $GEdistribUms!=0 || $cAp_ums!=0)
	{
		if($contLinies==1)
		{	
			html_nomsColumnes();
			$contLinies=0;
		}
		echo "
		<tr bgcolor='".$colorLinia."'>

			<td valign='top'>
			<p>[".$cont."]<br></p>
			<b>".(urldecode($mRebost['nom']))."</b>
			<p class='p_micro'>
			Responsable: <b>".(urldecode($mUsuarisRef[$mRebost['usuari_id']]['usuari']))."</b>
			<br>
			email: ".(urldecode($mRebost['email_rebost']))."
			<br>
			mobil: ".$mRebost['mobil']."
			<br>
			adre�a: ".(urldecode($mRebost['adressa']))."
			</p>
			<table style='width:100%;'>
				<tr>
					<td  style='width:60%;' align='left' valign='top'>
					";
					$mGrup=$mRebost;
					$mPeticionsGrup=getPeticionsGrup();
					if(count($mPeticionsGrup)>0){$peticionsText="<font style='color:red;'><b>".(count($mPeticionsGrup))."</b></font>";}else{$peticionsText='<b>0</b>';}
					echo "
					<p>&nbsp;&nbsp;[ Peticions pendents: ".$peticionsText."]</p>
					</td>
				</tr>
			</table>
			<p class='p_micro' >Productor vinculat:<br><font color='blue'><b>".(urldecode($mPerfilVinculat['projecte']))."</b></font></p>
			<br>


			";
			if($grupRef==62)
			{
				echo "
				<p class='nota'>* els imports d'aquest grup no s'inclouen al resum.</p>
				";
			}
			echo "
			</td>

			
			<td valign='top'>
			";
			$styleTotalUms='';
			$alertaTotalUms='';
			$botoActualitzarFormesPagamentUsuaris=false;
			if(number_format($mPars['fPagamentUms'],2)!=number_format($mFormaPagamentMembres['fPagamentUms'],2))
			{
				$styleTotalUms=" style='color:#ff0000; font-weight: bold;' ";
				$alertaTotalUms="Atenci�: El Grup no ha actualitzat la forma de pagament";
				$botoActualitzarFormesPagamentUsuaris=true;
			}
			
			echo "
			";
			if($grupNoHaGuardatFpagament)
			{
				echo "
				<p class='p_micro' style='color:red;'>Atenci�: el grup no ha guardat<br>la forma de pagament</p>
				";
			
			}
			echo "
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cpUms,2))."</b> ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cpEcos,2))." ecos  ".$mRebostsAmbComandaResum[$grupRef]['compte_ecos']; if($mPars['fPagamentEcosIntegralCES']=='1'){echo "&nbsp;<font size='1'><i><a title='INTEGRALCES'>I-CES</a></i></font> ";}else{echo "&nbsp;<font size='1'><i><a title='CES'>CES</a></i></font> ";} echo "
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cpEb,2))." ".$mParametres['moneda3Abrev']['valor']." ".$mRebostsAmbComandaResum[$grupRef]['compte_cieb']."&nbsp;<font size='1'><i><a title='INTEGRALCES'>I-CES</a></i></font></p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cpEu,2))." euros "; if($mPars['fPagamentEuMetalic']==1){echo " <i>en met�l.lic</i> ";}else{echo " <i>transfer�ncia</i> ";} echo "
					".$productesEspecialsText."
					<p onClick=\"enviarFpars('vistaAlbara.php?sR=".$mPars['selRutaSufix']."&gRef=".$mRebost['ref']."&op=totals','_blank');\" style='cursor:pointer;'><u>albar�</u></p>
					<p>&nbsp;&nbsp;[ Incidencies: ".$incidenciesIncdText."]
					<br>&nbsp;&nbsp;[ Abonaments/c�rrecs: ".$incidenciesAbcaText."]
					<br>
					";
					if($cpUms!=0)
					{
						echo "
					Punt d'entrega:
					<br>";
						$puntEntrega=db_getPuntEntrega($grupRef,$db);
						if($puntEntrega=='no_assignat')
						{
							echo "<b><font color='red'>".$puntEntrega."</font></b>";
						}
						else
						{
							echo "<b>".(@urldecode($mGrupsRef[$puntEntrega]['nom']))."</b>";
							if(@substr_count($mGrupsRef[$puntEntrega]['recepcions'],','.$grupRef.',')>0)
							{
								echo " (<font color='green'>ACCEPTAT</font>)";
							}
							else
							{
								echo " (<font color='red'>PENDENT</font>)";
							}
						}
					}
					echo "
					</p>
					</td>
				</tr>
			</table>
			";
			$styleAC='';
			if($sumaAbonamentsCarrecsUms!=0){$styleAC=" style='color:#0000aa;' ";}else{$styleAC='';}
			echo "
			<a title=\"Suma d'abonaments i c�rrecs de la CAC al Grup per aquesta comanda\"><p class='compacte'>A/C:<font  ".$styleAC.">".(number_format($sumaAbonamentsCarrecsUms,2))."</font> ums<p></a>
			<p class='compacte'>".(number_format($mRebostsAmbComandaResum[$grupRef]['kgT'],2))." kg<p>
			<p class='compacte'>&nbsp;</p>
			";
			if($cAp_ums>0)
			{
				echo "<p class='albara'>data comanda:<br>".(formatarData(getDataComanda($db,$mRebost['id'])))."</p>";
			}
			echo "
			</td>

			
			
			
			<td valign='top'>
			<br>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cAp_ums,2))."</b> ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cAp_ecos),2))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cAp_euros),2))." euros</p>
					</td>
				</tr>
				<tr>
					<td>
					";
					if($haProduit)
					{
						echo "
						<p class='compacte' onClick=\"enviarFpars('distribucioGrups.php?getP=".$mPars['perfil_id']."','_blank');\" style='color:#000000;  cursor:pointer;'><u>Total Reservat</u></p>

					<p> ".(number_format($mPerfilVinculat['acord1'],2))." % ms Acord<br>
						";
						if(number_format($mPerfilVinculat['acord1'],2)!=number_format(($cAp_ecos/$cAp_ums)*100,2))
						{
							echo "
						<a style='color:#ff0000;' title=\"el % ms promig productes reservats NO coincideix amb l'acord amb el productor\">(<b>".(number_format(($cAp_ecos/$cAp_ums)*100,2))."</b> % ms reserves)</a>
							";
						}
						else
						{
							echo "
						<a title=\"el % ms promig productes reservats coincideix amb l'acord amb el productor\">(".(number_format(($cAp_ecos/$cAp_ums)*100,2))." % ms reserves)</a>
							";
						}
						echo "
					</p>
						";	
					}			
					echo "
					</td>
				</tr>
			</table>
			</td>
			
			
			<td valign='top'>
			<br>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format(($cpUms+$cAp_ums),2))." </b> ums</p>
					</td>
				</tr>
";
if($mPars['selRutaSufix']*1<$mParametres['periodeIniciRecolzamentSelectiu']['valor'])
{
			if(($cpUms+$cAp_ums)>0) //la CAC ha de pagar i s'aplica l'acord de compra. Si l'acord es menor que el %ms dels prods oferits, la cac aplica el %ms dels prods oferitys 
			{
				echo "
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*($cAp_ecos/$cAp_ums),2))." ecos
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*(100-($cAp_ecos/$cAp_ums)*100)/100,2))." euros</p>
					<br>
					<p class='p_micro2'>
					* CAC paga<br>	aplicat: ".(number_format(($cAp_ecos/$cAp_ums)*100,2))."% ms segons productes</p>
					</td>
				</tr>
				";
			}
			else  //la CAC ha de cobrar i s'aplica la forma de pagament del grup
			{
				echo "
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*$mPars['fPagamentEcosPerc']/100,2))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*$mPars['fPagamentEbPerc']/100,2))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$cAp_ums)*$mPars['fPagamentEuPerc']/100,2))." euros</p>
					<br>
					<p class='p_micro2'>
					* CAC cobra<br> %ms i %".$mParametres['moneda3Abrev']['valor']." segons forma pagament del grup
					</p>
					</td>
				</tr>
				";
			}	
}
else
{
	echo "
				<tr>
					<td>
					<p>".(number_format(($cpEcos+$cAp_ecos),2,'.',''))." ecos
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpEu+$cAp_euros),2,'.',''))." euros</p>
					</td>
				</tr>
	";
}
		echo "
				</tr>
			</table>
			</td>

			<td valign='top'  bgcolor='#eeeeee'>
			<table width='100%'>
				<tr>
					<td>
					<p>Total reservat de l'estoc: (ums)</p>
					</td>
				</tr>
				<tr>
					<td>
					<p><b>".(@number_format($invUms,2,'.',''))."</b> ums</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(@number_format(($invEcos),2,'.',''))." ecos
					</td>
				</tr>

				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(@number_format(($invEuros),2,'.',''))." euros</p>
					</td>
				</tr>
				<tr>
					<td>
					<br>
					<p><i>Total queda en estoc: (ums) </i></p>
					</td>
				</tr>
				<tr>
					<td>
					<p><i><b>".(@number_format(-1*($totalQuedaEnEstocUms),2,'.',''))."</b> ums</i></p>
					</td>
				</tr>

				<tr>
					<td>
					<p><i>".(@number_format(-1*($totalQuedaEnEstocEcos),2,'.',''))." ecos</i></p>
					</td>
				</tr>

				<tr>
					<td>
					<p><i>0.00 ".$mParametres['moneda3Abrev']['valor']."</i></p>
					</td>
				</tr>

				<tr>
					<td>
					<p><i>".(@number_format(-1*($totalQuedaEnEstocEuros),2,'.',''))." euros</i></p>
					</td>
				</tr>
			</table>
			";
			if(isset($mPerfilVinculat['id']) && $mPerfilVinculat['id']!='' && $invUms!=0)
			{
				echo "
					<p onClick=\"enviarFpars('inventariPerProductors.php?sR=".$mPars['selRutaSufix']."&mRef=0&vPr=".$mPerfilVinculat['id']."','_blank');\" style='cursor:pointer;'><u>Estoc CAC</u></p>
				";
			}
			echo "
			</td>
			
			<td valign='top'  bgcolor='orange'>
			<br>
			<table width='100%'>
			";
			//-------------------
			//recompte
			//-------------------
			//si l'estoc es menor o igual que la producci� es fa la resta
			//if(ABS($cpUms)<=ABS($invUms)){$invUms=ABS($cpUms);}

			if(($cpUms+$invUms+$cAp_ums)!=0) //la CAC ha de pagar i s'aplica l'acord de compra. Si l'acord es menor que el %ms dels prods oferits, la cac aplica el %ms dels prods oferitys 
			{
				if($mPars['selRutaSufix']*1<$mParametres['periodeIniciRecolzamentSelectiu']['valor'])
				{
					echo "
				<tr>
					<td>
					<p><b>".(number_format(($cpUms+$invUms+$cAp_ums),2))." ums</b></p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpEcos+$invEcos+$cAp_ecos),2))." ecos
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpEu+$invEuros+$cAp_euros),2))." euros</p>
					<br>
					<p class='p_micro2'>
					* CAC paga<br>	aplicat: ".(number_format(($cAp_ecos/$cAp_ums)*100,2))."% ms segons productes</p>
					</td>
				</tr>
					";
				}	
				else
				{
					echo "
				<tr>
					<td>
					<p><b>".(number_format(($cpUms+$invUms+$cAp_ums),2,'.',''))." ums</b></p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpEcos+$invEcos+$cAp_ecos),2,'.',''))." ecos
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpEu+$invEuros+$cAp_euros),2,'.',''))." euros</p>
					<br>
					<p class='p_micro2'>
					* CAC paga<br> (% ms segons productes)</p>
					</td>
				</tr>
					";
				}
			}
			else  //la CAC ha de cobrar i s'aplica la forma de pagament del grup
			{
				echo "
				<tr>
					<td>
					<p><b>".(number_format(($cpUms+$invUms+$cAp_ums),2))." ums</b></p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$invUms+$cAp_ums)*$mPars['fPagamentEcosPerc']/100,2))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$invUms+$cAp_ums)*$mPars['fPagamentEbPerc']/100,2))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$invUms+$cAp_ums)*$mPars['fPagamentEuPerc']/100,2))." euros</p>
					<br>
					<p class='p_micro2'>
					* CAC cobra<br> %ms i %".$mParametres['moneda3Abrev']['valor']." segons forma pagament del grup
					</p>
					</td>
				</tr>
				";
			}

		echo "
			</table>
			</td>
		</tr>
		";
		$contLinies++;
		$cont++;
	}
	
		if($grupRef!=62) //si es CAC -despeses-ruta, la distribuci� i l'abastiment no es comptabilitzen  
		{
			//totals kg
			$kgT+=$mRebostsAmbComandaResum[$grupRef]['kgT'];
			
		
			// totals FP distribucio
			$sumaAbonamentsCarrecsUmsTT+=$sumaAbonamentsCarrecsUmsT;

			$cpUmsT+=$cpUms;
			$cpEcosT+=$cpEcos;
			$cpEbT+=$cpEb;
			$cpEuT+=$cpEu;

			// totals GE distribucio
			$GEdistribUmsT+=$GEdistribUms;
			$GEdistribEcosT+=$GEdistribEcos;
			$GEdistribEbT+=$GEdistribEb;
			$GEdistribEuT+=$GEdistribEu;

			//totals GE abastiment
			$cAp_umsT+=$cAp_ums;
			$cAp_ecosT+=$cAp_ecos;
			$cAp_eurosT+=$cAp_euros;
			$cAp_umsCtT+=$cAp_umsCt;
			$cAp_ecosCtT+=$cAp_ecosCt;
			$cAp_eurosCtT+=$cAp_eurosCt;
			$cAp_umsFdT+=$cAp_umsFd;
			$cAp_ecosFdT+=$cAp_ecosFd;
			$cAp_eurosFdT+=$cAp_eurosFd;
			

		}
	    unset($mComandaPerProductors[62]); //grup CAC-despeses-ruta
		
		//extraiem de la matriu el productors amb grup vinculat
		unset($mComandaPerProductors[$mPerfilVinculat['id']]);
		
	}
	}


}
reset($mRebostsAmbComandaResum);


		

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

// repetim pels productors sense grup vinculat
reset($mComandaPerProductors);
while(list($perfilRef,$mComandaProductor)=each($mComandaPerProductors))
{
	$totalEnEstocUms=0;
	$totalEnEstocEcos=0;
	$totalEnEstocEuros=0;

	$totalQuedaEnEstocUms=0;
	$totalQuedaEnEstocEcos=0;
	$totalQuedaEnEstocEuros=0;

if($perfilRef==$mPars['perfil_id'])
{
	
	$haProduit=0;
	$haConsumit=0;

	$bUms=0;

		//consum
			$mRebostsAmbComandaResum[$perfilRef]['kgT']=0;
			$mFormaPagament=array();
			$mFormaPagamentMembres=array();
			$mPars['fPagamentEcos']=0;
			$mPars['fPagamentEb']=0;
			$mPars['fPagamentEu']=0;
			$mPars['fPagamentUms']=0;
			$mPars['fPagamentEcosPerc']=100;
			$mPars['fPagamentEbPerc']=0;
			$mPars['fPagamentEuPerc']=0;
			//$mAbonamentsCarrecs=array();
		
		//produccio
		$cAp_ums=0;
		$cAp_ecos=0;
		$cAp_euros=0;
		while(list($producteId,$mComandaAproductor)=@each($mComandaPerProductors[$perfilRef]))
		{
			$cAp_ums+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
			$cAp_ecos+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
			$cAp_euros+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;;

				//productes 'dip�sit' en estoc
				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0 && $perfilRef!='63')
				{
					if(!isset($mInventariGT['inventariRef'][$producteId])){$mInventariGT['inventariRef'][$producteId]=0;}
					
					if(!isset($mProductesJaEntregatsTot[$producteId])){$mProductesJaEntregatsTot[$producteId]=0;}
					
					if($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId]>=$mComandaAproductor['quantitatT'])
					{
						$invUms-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
						$invEcos-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEuros-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;

						$totalQuedaEnEstocUms+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId]-$mComandaAproductor['quantitatT'])*$mComandaAproductor['preu'];
						$totalQuedaEnEstocEcos+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId]-$mComandaAproductor['quantitatT'])*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$totalQuedaEnEstocEuros+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId]-$mComandaAproductor['quantitatT'])*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
					else
					{
						$invUms-=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu'];
						$invEcos-=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEuros-=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
					$totalEnEstocUms+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu'];
					$totalEnEstocEcos+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
					$totalEnEstocEuros+=($mInventariGT['inventariRef'][$producteId]+$mProductesJaEntregatsTot[$producteId])*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
				}
		}
		if($cAp_ums<0){$haProduit=1;}
		
		$colorLinia='#ffffff';
		$colorLinia=$mColorLinia['haProduit2'];

	
	if( $cAp_ums!=0)
	{
		if($contLinies==1)
		{	
			html_nomsColumnes();
			$contLinies=0;
		}

		echo "
		<tr bgcolor='".$colorLinia."'>
			<td valign='top'>
			<p>[".$cont."]<br></p>
			<b>".(urldecode($mPerfilsRef[$perfilRef]['projecte']))."</b> 	
			<p class='p_micro'>
			Responsable: <b>".(urldecode(@$mUsuarisRef[$mPerfilsRef[$perfilRef]['usuari_id']]['nom']))."</b>
			<br>
			email: ".@$mUsuarisRef[$mPerfilsRef[$perfilRef]['usuari_id']]['email']."
			<br>
			mobil: ".@$mUsuarisRef[$mPerfilsRef[$perfilRef]['usuari_id']]['mobil']."
			<br>
			adre�a: ".(urldecode($mPerfilsRef[$perfilRef]['adressa']))."
			</p>
			</td>
			";
			
			//$cpUms=$mPars['fPagamentEcos']+$mPars['fPagamentEb']+$mPars['fPagamentEu']+$sumaAbonamentsCarrecsEcos+$sumaAbonamentsCarrecsEb+$sumaAbonamentsCarrecsEu;
			
			
			echo "
			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>0.00</b> ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 euros</p>
					</td>
				</tr>
			</table>
			</td>

			

			
			
			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cAp_ums,2))."</b> ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cAp_ecos,2))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cAp_euros,2))." euros</p>
					</td>
				</tr>
				<tr>
					<td>
						<p class='compacte' onClick=\"enviarFpars('distribucioGrups.php?getP=".$mPars['perfil_id']."','_blank');\" style='color:#000000;  cursor:pointer;'><u>Total Reservat</u></p>
					<p> ".(number_format($mPerfilsRef[$perfilRef]['acord1'],2))." % ms Acord<br>
					";
					if(number_format($mPerfilsRef[$perfilRef]['acord1'],2)!=number_format(($cAp_ecos/$cAp_ums)*100,2))
					{
						echo "
						<a style='color:#ff0000;' title=\"el % ms promig productes reservats NO coincideix amb l'acord amb el productor\">(<b>".(number_format(($cAp_ecos/$cAp_ums)*100,2))."</b> % ms reserves)</a>
						";
					}
					else
					{
						echo "
						<a title=\"el % ms promig productes reservats coincideix amb l'acord amb el productor\">(".(number_format(($cAp_ecos/$cAp_ums)*100,2))." % ms reserves)</a>
						";
					}
					echo "
					</p>
					</td>
				</tr>
			</table>
			</td>
			
			
			";

			//la CAC ha de pagar i s'aplica l'acord de compra

			echo "
			<td valign='top'>
			<table width='100%'>
				<tr>
					<td>
					<p><b>".(number_format($cAp_ums,2))."</b> ums</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cAp_ums*$mPerfilsRef[$perfilRef]['acord1']/100,2))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format($cAp_ums*(100-$mPerfilsRef[$perfilRef]['acord1'])/100,2))." euros</p>
					</td>
				</tr>
			</table>
			</td>


			<td valign='top'  bgcolor='#eeeeee'>
			<table width='100%'>
				<tr>
					<td>
					<p>Total reservat de l'estoc: (ums)</p>
					</td>
				</tr>
				<tr>
					<td>
					<p><b>".(@number_format($invUms,2,'.',''))."</b> ums</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(@number_format(($invEcos),2,'.',''))." ecos
					</td>
				</tr>

				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>

				<tr>
					<td>
					<p>".(@number_format(($invEuros),2,'.',''))." euros</p>
					</td>
				</tr>
				<tr>
					<td>
					<br>
					<p><i>Total queda en estoc: (ums) </i></p>
					</td>
				</tr>
				<tr>
					<td>
					<p><i><b>".(@number_format(-1*($totalQuedaEnEstocUms),2,'.',''))."</b> ums</i></p>
					</td>
				</tr>

				<tr>
					<td>
					<p><i>".(@number_format(-1*($totalQuedaEnEstocEcos),2,'.',''))." ecos</i></p>
					</td>
				</tr>

				<tr>
					<td>
					<p><i>0.00 ".$mParametres['moneda3Abrev']['valor']."</i></p>
					</td>
				</tr>

				<tr>
					<td>
					<p><i>".(@number_format(-1*($totalQuedaEnEstocEuros),2,'.',''))." euros</i></p>
					</td>
				</tr>
			</table>
			";
			if(isset($perfilRef) && $perfilRef!='' && $invUms!=0)
			{
				echo "
					<p onClick=\"enviarFpars('inventariPerProductors.php?sR=".$mPars['selRutaSufix']."&mRef=0&vPr=".$perfilRef."','_blank');\" style='cursor:pointer;'><u>Estoc CAC</u></p>
				";
			}
			echo "
			</td>
			
			<td valign='top'  bgcolor='orange'>
			<table width='100%'>
			";
			//-------------------
			//recompte
			//-------------------
			//si l'estoc es menor o igual que la producci� es fa la resta
			//if(ABS($cpUms)<=ABS($invUms)){$invUms=ABS($cpUms);}

			if(($cpUms+$invUms+$cAp_ums)>0) //la CAC ha de pagar i s'aplica l'acord de compra. Si l'acord es menor que el %ms dels prods oferits, la cac aplica el %ms dels prods oferitys 
			{
				echo "
				<tr>
					<td>
					<p><b>".(number_format(($cpUms+$invUms+$cAp_ums),2))." ums</b></p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$invUms+$cAp_ums)*($cAp_ecos/$cAp_ums),2))." ecos
					</td>
				</tr>
				<tr>
					<td>
					<p>0.00 ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$invUms+$cAp_ums)*(100-($cAp_ecos/$cAp_ums)*100)/100,2))." euros</p>
					<br>
					<p class='p_micro2'>
					* CAC paga<br>	aplicat: ".(number_format(($cAp_ecos/$cAp_ums)*100,2))."% ms segons productes</p>
					</td>
				</tr>
				";
			}
			else  //la CAC ha de cobrar i s'aplica la forma de pagament del grup
			{
				echo "
				<tr>
					<td>
					<p><b>".(number_format(($cpUms+$invUms+$cAp_ums),2))." ums</b></p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$invUms+$cAp_ums)*$mPars['fPagamentEcosPerc']/100,2))." ecos</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$invUms+$cAp_ums)*$mPars['fPagamentEbPerc']/100,2))." ".$mParametres['moneda3Abrev']['valor']."</p>
					</td>
				</tr>
				<tr>
					<td>
					<p>".(number_format(($cpUms+$invUms+$cAp_ums)*$mPars['fPagamentEuPerc']/100,2))." euros</p>
					<br>
					<p class='p_micro2'>
					* CAC cobra<br> %ms i %".$mParametres['moneda3Abrev']['valor']." segons forma pagament del grup
					</p>
					</td>
				</tr>
				";
			}	
		echo "
			</table>
			</td>
		</tr>
		";
		$cAp_umsT+=$cAp_ums;
		$cAp_ecosT+=$cAp_ecos;
		$cAp_eurosT+=$cAp_euros;
		$contLinies++;
		$cont++;
	}


}
}
reset($mComandaPerProductors);



echo "
</table>

<div style='width:80%;' align='center'>
<BR><BR>
<table style='width:80%;'>
	<tr>
		<td>
		</td>
		<td>
		<p style='text-align:left;'>Marques visuals:</p>
		<td>
	</tr>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haConsumit']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que el Grup ha fet comanda per� no ha produ�t per la CAC en aquesta ruta.
		<td>
	</tr>
</table>
<table style='width:80%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haProduit']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que el Grup ha produit per la CAC per� no ha fet comanda en aquesta ruta.
		<td>
	</tr>
</table>
<table style='width:80%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haProduitIconsumit']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que el Grup ha fet comanda i ha produ�t per la CAC en aquesta ruta, entrant en l'intercanvi directe.
		<td>
	</tr>
</table>
<table style='width:80%;'>
	<tr>
		<td width='50px;' bgcolor='".$mColorLinia['haProduit2']."'>
		<p>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>Aquest color indica que aquest Productor no t� a la CAC un Grup Vinculat (pel c�lcul del balan� producci�/consum), per� ha produit per la CAC.
		<td>
	</tr>
</table>
<table style='width:80%;'>
	<tr>
		<td width='50px;' valign='top'>
		<p style='background-color:orange;'>&nbsp;</p>
		</td>
		<td>
		<p class='nota' style='text-align:justify;'>
		La columna '<b>recompte</b>' apareix de color taronja. Aquesta columna mostra import producci� - import consum - import productes en estoc(CAC)
		<br>
		Si la CAC t� productes d'aquest productor en estoc es distribu�ran abans que els rebuts aquest mes. Per tant per saber quina es la liquidaci� 
		que ha de fer la CAC a aquesta productora per aquest periode, cal restar del balan� producci�/consum, l'import dels productes en estoc.
		<br>
		  La CAC pot tenir productes 'dip�sit' en estoc a causa de diferents tipus d'incid�ncies de distribuci�.
		<br>
		  Cal tenir en compte que la informaci� d'aquesta p�gina pot variar. La CAC triga un mes a resoldre les incid�ncies que tenen lloc a cada periode, 
		per tant, modifica comandes (reserves) i inventari.
		<br>
		   <b>Per aix� es molt important que quan feu el lliurament dels productes reservats ens entregueu l'albar� de totals en m�, i us en quedeu una c�pia f�sica o virtual.</b>
		  
		  
		<td>
	</tr>
</table>
</div>

</center>

<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
";
html_helpRecipient();

echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='i_afp_grupId' name='i_afp_grupId' value=''>
<input type='hidden' id='i_afp_grupSid' name='i_afp_grupSid' value=''>
</form>
</body>
</html>
";
?>

		