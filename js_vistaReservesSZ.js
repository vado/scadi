

function seleccionarZona(cbId,cbVal,zona_)
{
	if (cbVal==0)
	{
		document.getElementById(cbId).checked()=true;
		document.getElementById(cbId).value=1;
		zona=zona_;
	}
	document.getElementById('i_zona').value=zona_;
	enviarFpas('vistaReservesSZ.php','_self');

	return;
}

cadenaRecepcionsAConfirmar='';
function afegirRecepcioAconfirmar(grupId,opt)
{
	if(opt*1==-1)
	{
		cadenaRecepcionsAConfirmar=cadenaRecepcionsAConfirmar.replace(','+grupId+',','');
	}
	else (opt*1==1)
	{
		cadenaRecepcionsAConfirmar+=','+grupId+',';
	}
	
	return;

}

function seleccionarDesti(elmDesti)
{
	szDesti=document.getElementById('sel_szDesti').value;
	zDesti=document.getElementById('sel_zDesti').value;
	grupDesti=document.getElementById('sel_grupDesti').value;
	grupOrigen=document.getElementById('sel_grupOrigen').value;
	szOrigen=document.getElementById('sel_szOrigen').value;

	if(szDesti=='' && zDesti=='' && grupDesti=='')
	{
		document.getElementById('d_origen').style.visibility='hidden';
		document.getElementById('d_origen').style.position='absolute';
	}
	if(szDesti!='' || zDesti!='' || grupDesti!='')
	{
		document.getElementById('d_origen').style.visibility='inherit';
		document.getElementById('d_origen').style.position='relative';
	}
	if(elmDesti=='sel_szDesti')
	{
		document.getElementById('sel_zDesti').value='';
		document.getElementById('sel_grupDesti').value='';

		document.getElementById('sel_zDesti').style.color='black';
		document.getElementById('sel_grupDesti').style.color='black';
		document.getElementById('sel_szDesti').style.color='green';

		document.getElementById('sel_zOrigen').value='';
		document.getElementById('sel_zOrigen').disabled=true;
		document.getElementById('sel_zOrigen').style.backgroundColor='#eeeeee';
		
		document.getElementById('sel_grupOrigen').value='';
		document.getElementById('sel_grupOrigen').disabled=true;
		document.getElementById('sel_grupOrigen').style.backgroundColor='#eeeeee';
		
		document.getElementById('b_enviar').style.backgroundColor='orange';

		document.getElementById('sel_szOrigen').value=szOrigen;
		document.getElementById('sel_szOrigen').disabled=false;
		document.getElementById('sel_szOrigen').style.backgroundColor='#ffffff';
	
	}
	else if(elmDesti=='sel_zDesti')
	{
		document.getElementById('sel_szDesti').value='';
		document.getElementById('sel_grupDesti').value='';
		document.getElementById('sel_zDesti').style.color='green';
		document.getElementById('sel_grupDesti').style.color='black';
		document.getElementById('sel_szDesti').style.color='black';

		document.getElementById('b_enviar').style.backgroundColor='orange';

		zDesti=document.getElementById('sel_zDesti').value;
		val=document.getElementById('sel_zOrigen_'+zDesti).innerHTML;
		document.getElementById('sel_zOrigen').innerHTML=val;
		document.getElementById('sel_zOrigen').value=zOrigen;
		document.getElementById('sel_zOrigen').disabled=false;
		document.getElementById('sel_zOrigen').style.backgroundColor='#ffffff';
		
		document.getElementById('sel_grupOrigen').value='';
		document.getElementById('sel_grupOrigen').disabled=true;
		document.getElementById('sel_grupOrigen').style.backgroundColor='#eeeeee';

	}
	else if(elmDesti=='sel_grupDesti')
	{
		document.getElementById('sel_szDesti').value='';
		document.getElementById('sel_zDesti').value='';
		document.getElementById('sel_zDesti').style.color='black';
		document.getElementById('sel_grupDesti').style.color='green';
		document.getElementById('sel_szDesti').style.color='black';

		document.getElementById('b_enviar').style.backgroundColor='orange';

		grupDesti=document.getElementById('sel_grupDesti').value;
		zDesti=mGrupsRef[grupDesti]['zona'];//zona grup
		
		val=document.getElementById('sel_zOrigen_'+zDesti).innerHTML;
		document.getElementById('sel_zOrigen').innerHTML=val;

		document.getElementById('sel_zOrigen').value='';
		document.getElementById('sel_zOrigen').disabled=true;
		document.getElementById('sel_zOrigen').style.backgroundColor='#eeeeee';

		document.getElementById('sel_szOrigen').value='';
		document.getElementById('sel_szOrigen').disabled=true;
		document.getElementById('sel_szOrigen').style.backgroundColor='#eeeeee';

		val=document.getElementById('sel_grupOrigen_'+zDesti).innerHTML;
		document.getElementById('sel_grupOrigen').innerHTML=val;

		document.getElementById('sel_grupOrigen').value=grupOrigen;
		document.getElementById('sel_grupOrigen').disabled=false;
		document.getElementById('sel_grupOrigen').style.backgroundColor='#ffffff';

	}
	else
	{
		if(szDesti!=''){document.getElementById('sel_szDesti').style.color='green';}
		if(zDesti!=''){document.getElementById('sel_zDesti').style.color='green';}
		if(grupDesti!=''){document.getElementById('sel_grupDesti').style.color='green';}
		seleccionarOrigen('');
	}


	return;
}

function seleccionarOrigen(elmOrigen)
{
	szOrigen=document.getElementById('sel_szOrigen').value;
	zOrigen=document.getElementById('sel_zOrigen').value;
	grupOrigen=document.getElementById('sel_grupOrigen').value;

	if(elmOrigen=='sel_szOrigen')
	{
		document.getElementById('sel_zOrigen').value='';
		document.getElementById('sel_grupOrigen').value='';

		document.getElementById('sel_zOrigen').style.color='black';
		document.getElementById('sel_grupOrigen').style.color='black';
		document.getElementById('sel_szOrigen').style.color='blue';

		document.getElementById('b_enviar').style.backgroundColor='orange';
	}
	else if(elmOrigen=='sel_zOrigen')
	{
		document.getElementById('sel_szOrigen').value='';
		document.getElementById('sel_grupOrigen').value='';
		document.getElementById('sel_zOrigen').style.color='blue';
		document.getElementById('sel_grupOrigen').style.color='black';
		document.getElementById('sel_szOrigen').style.color='black';

		document.getElementById('b_enviar').style.backgroundColor='orange';
	}
	else if(elmOrigen=='sel_grupOrigen')
	{
		document.getElementById('sel_szOrigen').value='';
		document.getElementById('sel_zOrigen').value='';
		document.getElementById('sel_zOrigen').style.color='black';
		document.getElementById('sel_grupOrigen').style.color='blue';
		document.getElementById('sel_szOrigen').style.color='black';
		document.getElementById('b_enviar').style.backgroundColor='orange';
	}
	else
	{
		if(szOrigen!=''){document.getElementById('sel_szOrigen').style.color='blue';}
		if(zOrigen!=''){document.getElementById('sel_zOrigen').style.color='blue';}
		if(grupOrigen!=''){document.getElementById('sel_grupOrigen').style.color='blue';}
	}


	return;
}

function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='vistaReservesSZ.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}
		