<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "html_ajuda1.php";
include "db_ajuda.php";
include "eines.php";
include "html_vistaAlbaraLocal.php";
include "db_vistaAlbara.php";
include "db_incidencies.php";
include "db_gestioProductes.php";
include "db_gestioMagatzems.php";
include "db_mail.php";
	require_once('phpmailer/class.phpmailer.php');
	include("phpmailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded


$mParsCAC=array();
$mComanda=array();
$quantitatTotalCac=0;
$quantitatTotalRebosts=0;
$mCr=array();
$form='';
$mAfegirIncidencia=array();
$missatgeAlerta='';

$mostrarIncidencies=1;

//------------------------------------------------------------------------------

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$mRuta=explode('_',$mPars['selRutaSufix']);

if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);

$ruta_=@$_GET['sR']; //selector de ruta

if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}

getConfig($db); //inicialitza variables anteriors;

	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';

	$mPars['fPagamentUms']=0.00;
	$mPars['fPagamentEcos']=0.00;
	$mPars['fPagamentEb']=0.00;
	$mPars['fPagamentEu']=0.00;
	$mPars['fPagamentEuMetalic']=0;
	$mPars['fPagamentEuTransf']=0;
	$mPars['fPagamentEcosIntegralCES']=0;
	

	$mPars['fPagamentEcosPerc']=0.00;
	$mPars['fPagamentEbPerc']=0.00;
	$mPars['fPagamentEuPerc']=0.00;


if($mPars['selLlistaId']==0)
{
	$mPars['vRutaIncd']=$mPars['selRutaSufix'];
}
else
{
	$mPars['vRutaIncd']=$mPars['sel_periode_comanda_local'];
}
	
	$mPars['albaraVistaImpressio']=0;
if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}

post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['movimentsProductes.php']=db_getAjuda('movimentsProductes.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$llistaText=" llista LOCAL ";

$mPropietatsPeriodesLocals=array();

	$mPars['taulaProductes']='productes_grups';
	$mPars['taulaComandes']='comandes_grups';
	$mPars['taulaComandesSeg']='comandes_seg_grups';
	$mPars['taulaIncidencies']='incidencies_grups';

	$mPropietatsPeriodesLocals=db_getPropietatsPeriodesLocals($db);

		
$opcio='totals'; //vista albara de grup, no desglossat per usuaris, per defecte
//GETS
if($gRef=@$_GET['gRef']) //prov� de links a resumRuta.php
{
	$mRebost['ref']=$gRef;
	$mPars['grup_id']=$gRef;
	$mRebost=db_getRebost($db);
}

//GETS
if($sUid=@$_GET['sUid']) //prov� de links a comptesGrupLocal.php
{
	$mPars['selUsuariId']=$sUid;
}

	$opcio=@$_GET['op'];//vista albara de grup, opcio totals/desglossat
	$mRebost=db_getRebost($db);

$mPropietatsGrup=@getPropietatsGrup($mRebost['propietats']);

	db_getFormaPagamentLocal($db); // afegeix info a $mPars

	$mProductes2=db_getProductes2($db);

$mPuntsEntrega=db_getPuntsEntrega($db,'ref');
$puntEntregaId=db_getPuntEntrega($mPars['grup_id'],$db);
$mRebostsRef=db_getRebostsRef($db);
$mRutesSufixes=getRutesSufixes($db);
$mUsuari=db_getUsuari($mPars['usuari_id'],$db);
$mUsuarisRef=db_getUsuarisRef($db);
$mSelUsuari=@db_getUsuari($mPars['selUsuariId'],$db);
$mUsuarisGrupRef=db_getUsuarisGrupRef($db);
$mAnticsUsuarisGrupAmbComanda=db_getAnticsUsuarisGrupAmbComanda($db); // rep $mUsuarisGrupRef com a global


	if(@$mPars['selUsuariId']!='0' && $opcio!='desglossat')
	{
		$mComandaCopiar=db_getComandaCopiar($mPars['grup_id'],$db);
		if(!isset($mComandaCopiar['compte_ecos']) || $mComandaCopiar['compte_ecos']=='')
		{
			$mPars['compte_ecos']=$mSelUsuari['compte_ecos'];
		}
		else
		{
			$mPars['compte_ecos']=$mComandaCopiar['compte_ecos'];
		}

		if(!isset($mComandaCopiar['compte_cieb']) || $mComandaCopiar['compte_cieb']=='')
		{
			$mPars['compte_cieb']=$mSelUsuari['compte_cieb'];
		}
		else
		{
			$mPars['compte_cieb']=$mComandaCopiar['compte_cieb'];
		}
		
	}
	else
	{
			$selUsuariMem=$mPars['selUsuariId'];
			$mPars['selUsuariId']='0';
		$mComandaCopiar=db_getComandaCopiar($mPars['grup_id'],$db);
			$mPars['selUsuariId']=$selUsuariMem;
		
		if(!isset($mComandaCopiar['compte_ecos']) || $mComandaCopiar['compte_ecos']=='')
		{
			$mPars['compte_ecos']=$mRebost['compte_ecos'];
		}
		else
		{
			$mPars['compte_ecos']=$mComandaCopiar['compte_ecos'];
		}

		if(!isset($mComandaCopiar['compte_cieb']) || $mComandaCopiar['compte_cieb']=='')
		{
			$mPars['compte_cieb']=$mRebost['compte_cieb'];
		}
		else
		{
			$mPars['compte_cieb']=$mComandaCopiar['compte_cieb'];
		}
	}
$mComandaCopiar=db_getComandaCopiar($mPars['grup_id'],$db);

$mPropietatsPeriodesLocals=db_getPropietatsPeriodesLocals($db);

$mPropietatsPeriodeLocal=@$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']];
$mPeriodesLocalsInfo=db_getPeriodesLocalsInfo($db);
$form=@$_POST['i_form'];
if($form=='afegirIncidencia')
{
	$mAfegirIncidencia['grup_id']=$mPars['grup_id'];
	$mAfegirIncidencia['usuari_id']=$mPars['usuari_id'];
	$mAfegirIncidencia['data']=@$_POST['i_aIdata'];
	$mAfegirIncidencia['producte_id']=@$_POST['i_producteId'];
	$mAfegirIncidencia['tipus']=@$_POST['i_tipusIncidencia'];
	$mAfegirIncidencia['demanat']=@$_POST['i_producteDemanat'];
	$mAfegirIncidencia['rebut']=@$_POST['i_producteRebut'];
	$mAfegirIncidencia['comentaris']=urlencode(@$_POST['ta_comentaris']);
	$mAfegirIncidencia['ruta']=$mPars['sel_periode_comanda_local'];
	$missatgeAlerta=db_afegirIncidenciaLocal($mAfegirIncidencia,$db);
	
}
else if($form=='abonamentGrup')
{
	$mAplicarAbonament['tipus']=$form;
	$mAplicarAbonament['autor']=$mPars['usuari_id'];
//*v36-15-12-15 1 assignacio
	$mAplicarAbonament['usuari']=@$_POST['sel_usuariIncidenciaAa'];
	$mAplicarAbonament['grup']=$mPars['grup_id'];//
	$mAplicarAbonament['data']=@$_POST['i_aAdata'];
	$mAplicarAbonament['ecos']=@$_POST['i_ecosAa'];
	$mAplicarAbonament['eb']=@$_POST['i_ebAa'];
	$mAplicarAbonament['euros']=@$_POST['i_eurosAa'];
	$mAplicarAbonament['comentaris']=urlencode(@$_POST['ta_comentarisAa']);
	$mAplicarAbonament['ruta']=$mPars['sel_periode_comanda_local'];
	$missatgeAlerta=db_aplicarAbonament($mAplicarAbonament,$db);
}
else if($form=='carrecGrup')
{
	$mAplicarCarrec['tipus']=$form;
	$mAplicarCarrec['autor']=$mPars['usuari_id'];
//*v36-15-12-15 1 assignacio
	$mAplicarCarrec['usuari']=@$_POST['sel_usuariIncidenciaAc'];
	$mAplicarCarrec['grup']=$mPars['grup_id'];//
	$mAplicarCarrec['data']=@$_POST['i_aCdata'];
	$mAplicarCarrec['ecos']=@$_POST['i_ecosAc'];
	$mAplicarCarrec['eb']=@$_POST['i_ebAc'];
	$mAplicarCarrec['euros']=@$_POST['i_eurosAc'];
	$mAplicarCarrec['comentaris']=urlencode(@$_POST['ta_comentarisAc']);
	$mAplicarCarrec['ruta']=$mPars['sel_periode_comanda_local'];
	$missatgeAlerta=db_aplicarCarrec($mAplicarCarrec,$db);
}
else if($form=='anularReserva')
{
	$mAnularReserva['tipus']=$form;
	$mAnularReserva['autor']=$mPars['usuari_id'];
	$mAnularReserva['usuari']=@$_POST['i_anularUs'];
	$mAnularReserva['grup']=$mPars['grup_id'];//
	$mAnularReserva['data']=@$_POST['i_aRdata'];
	$mAnularReserva['anularId']=@$_POST['i_anularId'];
	$mAnularReserva['anularUts']=@$_POST['i_anularUts'];
	$mAnularReserva['ruta']=$mPars['sel_periode_comanda_local'];
	$mMissatgeAlerta=db_anularReservesUsuari($mAnularReserva,$db);
	$missatgeAlerta=$mMissatgeAlerta['missatgeAlerta'];
}
else
{
	$opt_=@$_GET['opt'];
	if(isset($opt_)){$opt=$opt_;}
	if(isset($opt) && $opt=='eliminar')
	{
		$inId=@$_GET['inId'];
		$missatgeAlerta=db_eliminarIncidencia($inId,$db);
	}
	else if( isset($opt) && $opt=='guardar')
	{
		$inId=@$_GET['inId'];
		$numB=@$_GET['numB'];
		$inComentaris=urlencode(@$_POST['ta_comentaris_'.$numB]);
		$missatgeAlerta=db_guardarIncidencia($inComentaris,$inId,$db);
	}
}



if($mPars['selUsuariId']==0)
{
	$mProductes=getProductesVistaAlbaraGrup($opcio,$db);
}
else if($mPars['selUsuariId']!=0)
{
	if($opcio=='desglossat')
	{
		$mPars['albaraVistaImpressio']=1;
		$mostrarIncidencies=0;
		$mProductes=getProductesVistaAlbaraGrup($opcio,$db);
	}
	else
	{
		$mProductes=getProductesVistaAlbaraUsuariLocal($db);
	}
}
	$mPars['vEstatIncd']='aplicat';
$mAbonamentsCarrecs=db_getAbonamentsCarrecs($mRebost['id'],$db);
	$mPars['vEstatIncd']='pendent';
echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>"; echo $htmlTitolPags; echo " - Vista Albar�</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_vistaAlbaraLocal.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
";
if($mPars['selLlistaId']==0)
{
	echo "
ruta=".$mPars['selRutaSufix'].";
rutaPeriode=".$mPars['selRutaSufixPeriode'].";
	";
}
else
{
	echo "
ruta='".$mPars['sel_periode_comanda_local']."';
rutaPeriode=ruta;
	";
}

	
//*v36.2-declaracio
echo "
gRef='".$mPars['grup_id']."';
op='".$opcio."';
selUsuariId='".$mPars['selUsuariId']."';
parametresMoneda3Activa='".$mParametres['moneda3Activa']['valor']."';
parametresMoneda3Nom='".$mParametres['moneda3Nom']['valor']."';
parametresMoneda3Abrev='".$mParametres['moneda3Abrev']['valor']."';
parametresMoneda3DigitsCodi='".$mParametres['moneda3DigitsCodi']['valor']."';
numBotonsEditar=0;
mostrarIncidencies=".$mostrarIncidencies.";
moneda3Activa=".$mParametres['moneda3Activa']['valor'].";
</SCRIPT>
</head>
<body onLoad=\"
";
if($mPars['albaraVistaImpressio']==1)
{
	echo "
	vistaImpressio2();
	";
}
echo "
actualitzarFpagament();\" bgcolor='".$mColors['body']."'>
";
html_demo('vistaAlbaraLocal.php?op='.$opcio.'&');

$mPars['vGrupIncd']=$mPars['grup_id'];
$mIncidencies=db_getIncidencies(true,'incd',' * ',$db);

$mProductesJaEntregatsGrup=array();

if($mPars['selUsuariId']==0)
{
	//$mIncidencies=db_getIncidenciesAlbara($mPars['grup_id'],$db);
	if($opcio=='totals')
	{
		$mProductesJaEntregatsGrup=db_getProductesJaEntregatsGrup($db);
		html_mostrarAlbaraGrupTotals($mProductes,$db);
	}
	else if($opcio=='desglossat')
	{
		html_mostrarAlbaraGrupDesglossat($mProductes,$db);
	}
	else if($opcio=='resumUsuaris')
	{
		html_mostrarAlbaraResumUsuaris($mProductes,$db);
	}
}
else if($mPars['selUsuariId']!=0)
{
	if($opcio=='desglossat')
	{
			$selUsuariIdMem=$mPars['selUsuariId'];
			$mPars['selUsuariId']='0';
		db_getFormaPagament($db); // afegeix info a $mPars
		//$mIncidencies=db_getIncidenciesAlbara($mPars['grup_id'],$db);
		$mostrarIncidencies=false;
		html_mostrarAlbaraGrupDesglossat($mProductes,$db);
			$mPars['selUsuariId']=$selUsuariIdMem;
			db_getFormaPagament($db); // afegeix info a $mPars
	}
	else
	{
		//$mIncidencies=db_getIncidenciesSelUsuari($mPars['grup_id'],$db);
		html_mostrarAlbaraUsuari($mProductes,$db);
		//html_mostrarNotesAlbara();
	}
}


$parsChain=makeParsChain($mPars);

html_helpRecipient();

echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>
";
	

?>

		