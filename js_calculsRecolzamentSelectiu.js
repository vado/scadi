allowedCharsPositiu='0123456789.';


function mostrarResum()
{
	trg=document.getElementById('p_trg').innerHTML;
	srg=PPREGM-trg;
	trgp=document.getElementById('p_trgp').innerHTML;
	srgp=PPREGPM-trgp;
	trr=document.getElementById('p_trr').innerHTML;
	srr=PPRERM-trr;

	chain=document.getElementById('d_resum1').innerHTML;
	document.getElementById('d_resum2').innerHTML=chain;
	return;
}

function mostrarCalculRecolzamentProductors()
{
	ngp=document.getElementById('i_ngp').value;
	sumaFactorRp=document.getElementById('i_sumaFactorRp').value;
	sumaEcosRGrupsP_calcul=0;
	recolzamentGPchain='';
	for(i=0;i<ngp;i++)
	{
		factorRP=document.getElementById('p_factorRP_'+i).innerHTML;
		recolzamentRP_actual=document.getElementById('p_recolzamentGP_actual_'+i).innerHTML;
		recolzamentRP_calcul=(PPREGPM/sumaFactorRp)*factorRP;
		document.getElementById('p_recolzamentGP_calcul_'+i).innerHTML=(Math.round(recolzamentRP_calcul*100)/100);
		sumaEcosRGrupsP_calcul+=recolzamentRP_calcul;
		gpId=document.getElementById('i_gp_id_'+i).value;
		recolzamentGPchain+=gpId+':'+recolzamentRP_calcul+';';
		if(Math.round(recolzamentRP_actual*100)/100!=Math.round(recolzamentRP_calcul*100)/100)
		{
			document.getElementById('p_recolzamentGP_calcul_'+i).style.color='red';
			document.getElementById('p_recolzamentGP_actual_'+i).style.color='red';
		}
		else
		{
			document.getElementById('p_recolzamentGP_calcul_'+i).style.color='blue';
			document.getElementById('p_recolzamentGP_actual_'+i).style.color='blue';
		}
		
	}
	document.getElementById('p_sumaEcosRGrupsP_calcul').innerHTML=(Math.round(sumaEcosRGrupsP_calcul*100)/100);
	document.getElementById('i_distribucioGrupsProductors').value=recolzamentGPchain;

	return;
}

function comprovarCanvisRecolzamentGrupNp(elementId,elementId2)
{
	ng=document.getElementById('i_ng').value;
	trg_=0;
	for(i=0;i<ng;i++)
	{
		valor=document.getElementById('i_grupNpR_'+i).value;
		trg_+=valor*1;
	}
	if(trg_>PPREGM)
	{
		exces=trg_-PPREGM;
		alert('ATENCI�: no hi ha prou recursos per assignar a aquest grup (exc�s: '+exces+' ecos)'); 
		valor=document.getElementById(elementId2).value;
		document.getElementById(elementId).value=valor;
		return false;
	}
	return true;
}

function comprovarCanvisRecolzamentRebost(elementId,elementId2)
{
	nr=document.getElementById('i_nr').value;
	trr_=0;
	for(i=0;i<nr;i++)
	{
		valor=document.getElementById('i_grupCAL_'+i).value;
		trr_+=valor*1;
	}
	if(trr_>PPRERM)
	{ 	
		exces=trr_-PPRERM;
		alert('ATENCI�: no hi ha prou recursos per assignar a aquest Rebost-CAL (exc�s: '+exces+' ecos)'); 
		valor=document.getElementById(elementId2).value;
		document.getElementById(elementId).value=valor;
		return false;
	}
	return true;
}

function aplicarDistribucioGrupsProductors()
{
	document.getElementById('i_distribucioGrupsProductors').value=recolzamentGPchain;
	document.getElementById('i_aplicarDistribucioGrupsProductors').value=1;
	enviarFpars('calculsRecolzamentSelectiu.php','_self');
	return;
}

function aplicarDistribucioComissions()
{
	document.getElementById('i_distribucioComissions').value=distribucioComissions;
	enviarFpars('calculsRecolzamentSelectiu.php','_self');
	return;
}

function aplicarRecolzamentGrup(elementId,grupId)
{
	saldoEcosR=document.getElementById(elementId).value;
	document.getElementById('i_saldoEcosR').value=saldoEcosR;
	document.getElementById('i_grupRid').value=grupId;

	enviarFpars('calculsRecolzamentSelectiu.php','_self');

	return;
}

function guardarParametre(parametreNom)
{
	missatgeAlerta='';
	parametreValor=document.getElementById('i_'+parametreNom).value;
	document.getElementById('i_parametreValor').value=parametreValor;
	if(parametreNom=='PPRECM')
	{
		p_tmcValor=document.getElementById('p_tmc').innerHTML;
		
		if (parametreValor.length>0)
		{
   			allowedChars=allowedCharsPositiu;
			$_ok=0;
			//check for prohibited characters
   			for(i=0;i<parametreValor.length;i++)
       		{
           		for(j=0;j<allowedChars.length;j++)
           		{
               		if(parametreValor.charAt(i)==allowedChars.charAt(j)){$_ok++;}
           		}
			}
       		if($_ok<parametreValor.length)
       		{
				missatgeAlerta+="\nEl camp 'Membres Comissions' cont� un valor incorrecte (car�cters admesos: "+allowedCharsPositiu+")";			
       		}
		}
		else if(parametreValor*1<p_tmcValor*1)
		{
			missatgeAlerta+="\nEl pressupost pel recolzament a membres de comissions no pot ser menor que el que tenen assignat (PPRECM minim="+p_tmcValor+")";
		}
	}
	else if(parametreNom=='PPRERM')
	{
		p_trrValor=document.getElementById('p_trr').innerHTML;
		
		if (parametreValor.length>0)
		{
   			allowedChars=allowedCharsPositiu;
			$_ok=0;
			//check for prohibited characters
   			for(i=0;i<parametreValor.length;i++)
       		{
           		for(j=0;j<allowedChars.length;j++)
           		{
               		if(parametreValor.charAt(i)==allowedChars.charAt(j)){$_ok++;}
           		}
			}
       		if($_ok<parametreValor.length)
       		{
				missatgeAlerta+="\nEl camp 'Rebosts-CAL' cont� un valor incorrecte (car�cters admesos: "+allowedCharsPositiu+")";			
       		}
		}
		else if(parametreValor*1<p_trrValor*1)
		{
			missatgeAlerta+="\nEl pressupost pel recolzament a grups productors no pot ser menor que el que tenen assignat (PPRERM minim="+p_trrValor+")";
		}
	}
	else if(parametreNom=='PPREGPM')
	{
		p_trgpValor=document.getElementById('p_trgp').innerHTML;
		if (parametreValor.length>0)
		{
   			allowedChars=allowedCharsPositiu;
			$_ok=0;
			//check for prohibited characters
   			for(i=0;i<parametreValor.length;i++)
       		{
           		for(j=0;j<allowedChars.length;j++)
           		{
               		if(parametreValor.charAt(i)==allowedChars.charAt(j)){$_ok++;}
           		}
			}
       		if($_ok<parametreValor.length)
       		{
				missatgeAlerta+="\nEl camp 'Grups Productors' cont� un valor incorrecte (car�cters admesos: "+allowedCharsPositiu+")";			
       		}
		}
		else if(parametreValor*1<p_trgpValor*1)
		{
			missatgeAlerta+="\nEl pressupost pel recolzament a grups productors no pot ser menor que el que tenen assignat (PPREGPM minim="+p_trgpValor+")";
		}
	}
	else if(parametreNom=='PPREGM')
	{
		p_trgValor=document.getElementById('p_trg').innerHTML;
		if (parametreValor.length>0)
		{
   			allowedChars=allowedCharsPositiu;
			$_ok=0;
			//check for prohibited characters
   			for(i=0;i<parametreValor.length;i++)
       		{
           		for(j=0;j<allowedChars.length;j++)
           		{
               		if(parametreValor.charAt(i)==allowedChars.charAt(j)){$_ok++;}
           		}
			}
       		if($_ok<parametreValor.length)
       		{
				missatgeAlerta+="\nEl camp 'Grups NO Productors' cont� un valor incorrecte (car�cters admesos: "+allowedCharsPositiu+")";			
       		}
		}
		else if(parametreValor*1<p_trgValor*1)
		{
			missatgeAlerta+="\n\nEl pressupost pel recolzament a grups NO productors no pot ser menor que el que tenen assignat (PPREGM minim="+p_trgValor+")";
		}
	}
	
	if(missatgeAlerta=='')
	{
		document.getElementById('i_parametreNom').value=parametreNom;
		enviarFpars('calculsRecolzamentSelectiu.php','_self');
	}
	else
	{
		alert(missatgeAlerta);
	}


	return;
}

function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='calculsRecolzamentSelectiu.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}
		