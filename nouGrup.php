<?php

/*
formulari grup de nou grup
*/

include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "eines.php";
include "html_ajuda1.php";
include "db_ajuda.php";



$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$parsChain=makeParsChain($mPars);
$ruta_=@$_GET['sR'];
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	if(!(isset($mPars['selRutaSufix'])))
	{
		$mPars['selRutaSufix']=$ruta;
	}
}
$mRuta=explode('_',$mPars['selRutaSufix']);
if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

	$mPars['taulaProductes']='productes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandes']='comandes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandesSeg']='comandes_seg';
	$mPars['taulaIncidencies']='incidencies_'.$mPars['selRutaSufix'];
	if(count($mRuta)==2)//periode especial
	{
		$mPars['taulaProductors']='productors_'.$mPars['selRutaSufix']; //productes especials
	}
	else
	{
		$mPars['taulaProductors']='productors';
	}

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
getConfig($db); //inicialitza variables anteriors;
$mPars['vRutaIncd']=$mPars['selRutaSufix'];

$mRutesSufixes=getRutesSufixes($db);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['nouGrup.php']=db_getAjuda('nouGrup.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);


$mPeriodesInfo=db_getPeriodesInfo($db);


$mGrup=array();
$mGrup['id']='';
$mGrup['ref']='';
$mGrup['nom']='';
$mGrup['adressa']='';
$mGrup['municipiId']='';
$mGrup['bioregio']='';
$mGrup['codi_ecoxarxa']='';
$mGrup['categoria']='';
//$mGrup['gestor_magatzem']='';
$mGrup['usuari_id']='';
$mGrup['compte_ecos']='';
$mGrup['compte_cieb']='';
//$mGrup['compte_bank']='';
$mGrup['email_rebost']='';
$mGrup['productors_associats']='';
$mGrup['notes']='';
//$mGrup['c_ecos']='';
//$mGrup['c_eb']='';
//$mGrup['c_eu']='';
//$mGrup['p_ecos']='';
//$mGrup['p_eb']='';
//$mGrup['p_eu']='';
//$mGrup['s_ecos']='';
//$mGrup['s_eb']='';
//$mGrup['s_eu']='';


$opcio1='';
$opcio2='';

$mCategoriesGrups=getCategoriesGrups($db);
$mGrups=getGrups($db,'tots');
$mMunicipis=db_getMunicipis2Id($db);
$mProductors=db_getProductors($db);
$missatgeAlerta='';

$opcio1=@$_GET['op'];
$opcio2=@$_POST['i_opcio'];
if($opcio1!=''){$opcio=$opcio1;}
else if($opcio2!=''){$opcio=$opcio2;}
else {$opcio='';}
if ($opcio=='crearGrup')
{
	$mGrup['nom']=urlencode(@$_POST['i_nomGrup']);
	$mGrup['categoria']=@$_POST['sel_categoria']; //camp nivell de confian�a taula rebosts // matriu - multiselect
	$mGrup['municipiId']=@$_POST['sel_municipi'];
	$mGrup['bioregio']=@$mMunicipis[$mGrup['municipiId']]['comarca'];
	$mGrup['codi_ecoxarxa']=@$_POST['i_codiEcoxarxa'];
	$mGrup['adressa']=urlencode(@$_POST['i_adre�a']);
	$mGrup['email_rebost']=@$_POST['i_email'];
	$mGrup['notes']=urlencode(@$_POST['ta_notes']);
	$mGrup['productors_associats']='';
	$mGrup['mobil']=@$_POST['i_mobil'];
	$mGrup['compte_cieb']=@$_POST['i_compteCieb'];
	$mGrup['compte_ecos']=@$_POST['i_compteEcos'];

	if(!putNouGrup($opcio,$mGrup,$db))
	{
		$missatgeAlerta="Atenci�: no s'ha pogut crear el nou grup";
		error_log ( date('d:m:Y')." Error DB - putNouGrup() - nouGrup.php - l:77" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta="S'ha creat el nou grup correctament.";
		$mGrups=getGrups($db,'tots');
	}
	$opcio='';
}
else  if($opcio=='guardarGrup')
{
	$mGrup['id']=$mPars['grup_id'];
	$mGrup['nom']=urlencode(@$_POST['i_nomGrup']);
	$mGrup['categoria']=@$_POST['sel_categoria']; //camp nivell de confian�a taula rebosts // matriu - multiselect
	$mGrup['municipiId']=@$_POST['sel_municipi'];
	$mGrup['municipi']=@$mMunicipis[$mGrup['municipiId']]['municipi'];
	$mGrup['bioregio']=@$mMunicipis[$mGrup['municipiId']]['comarca'];
	$mGrup['codi_ecoxarxa']=@$_POST['i_codiEcoxarxa'];
	$mGrup['adressa']=urlencode(@$_POST['i_adre�a']);
	$mGrup['usuari_id']=urlencode(@$_POST['sel_responsableGrupId']);
	$mGrup['email_rebost']=@$_POST['i_email'];
	$mGrup['notes']=urlencode(@$_POST['ta_notes']);
	$mGrup['productors_associats']=$_POST['sel_productorsAssociats'];
	$mGrup['mobil']=@$_POST['i_mobil'];
	$mGrup['compte_cieb']=@$_POST['i_compteCieb'];
	$mGrup['compte_ecos']=@$_POST['i_compteEcos'];
	$mPropietats=array();
	$mGrup['c_ecos']='{';
	while(list($propietat,$mOpcions)=each($mPropietatsGrups))
	{
		$mPropietats[$propietat]=@$_POST['sel_'.$propietat];
		$mGrup['c_ecos'].=$propietat.':'.$mPropietats[$propietat].';';
	}
	reset($mPropietatsGrups);
	$mGrup['c_ecos'].='}';
	$mUsuarisRef=db_getUsuarisRef($db);
	if(!putNouGrup($opcio,$mGrup,$db))
	{
		$missatgeAlerta="Atenci�: no s'ha pogut guardar el grup";
		error_log ( date('d:m:Y')." Error DB - putNouGrup() - nouGrup.php - l:77" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta="S'ha guardat el grup correctament.";
		$mGrups=getGrups($db,'tots');
	}
	$opcio='editarGrup';
	//$mGrup['ref']='';
	//$mPars['selGrupRef']='';
}
else if($opcio=='afegirCategoriaGrup')
{
	$novaCategoria=@$_POST['i_novaCategoriaGrup'];
	if(!db_afegirCategoriaGrup($db,$novaCategoria))
	{
		$missatgeAlerta="Atenci�: no s'ha pogut afegir la nova categoria de grup";
		error_log ( date('d:m:Y')." Error DB - putNouGrup() - nouGrup.php - l:134" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta="S'ha afegit la categoria de grup correctament.";
		unset($mCategoriesGrups);
		$mCategoriesGrups=getCategoriesGrups($db);
	}
	$opcio='editarCategoriesGrup';
}
else if($opcio=='eliminarGrup')
{
	$mPars['selGrupRef']=$_GET['iGed'];
	$mPars['grup_id']=$mPars['selGrupRef'];
	$mGrup=getGrup($db);
	if(!db_eliminarGrup($db))
	{
		$missatgeAlerta="Atenci�: no s'ha pogut eliminar el grup";
		error_log ( date('d:m:Y')." Error DB - putNouGrup() - nouGrup.php - l:151" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta="S'ha eliminat grup correctament.";
		$mGrups=getGrups($db,'tots');
	}
	$opcio='';
	$mGrup['id']='';
	$mPars['selGrupRef']='';
}
else if($opcio=='activarGrup')
{
	$mPars['selGrupRef']=$_GET['iGed'];
	$mPars['grup_id']=$mPars['selGrupRef'];
	$mGrup=getGrup($db);
	if(!db_activarGrup(1,$db))
	{
		$missatgeAlerta="Atenci�: no s'ha pogut activar el grup";
		error_log ( date('d:m:Y')." Error DB - putNouGrup() - nouGrup.php - l:167" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta="S'ha activat el grup correctament.";
		$mGrups=getGrups($db,'tots');
	}
	$opcio='';
	$mGrup['id']='';
	$mPars['selGrupRef']='';

}
else if($opcio=='desactivarGrup')
{
	$mPars['selGrupRef']=$_GET['iGed'];
	$mPars['grup_id']=$mPars['selGrupRef'];
	$mGrup=getGrup($db);
	if(!db_activarGrup(0,$db))
	{
		$missatgeAlerta="Atenci�: no s'ha pogut desactivar el grup";
		error_log ( date('d:m:Y')." Error DB - putNouGrup() - nouGrup.php - l:183" ,0,'errors.php');
	}
	else
	{
		$missatgeAlerta="S'ha desactivat el grup correctament.";
		$mGrups=getGrups($db,'tots');
	}
	$opcio='';
	$mGrup['id']='';
	$mPars['selGrupRef']='';
}


if($opcio=='editarGrup')
{
	if(@$selGrupId=$_GET['iGed'])
	{
		$mPars['selGrupRef']=$selGrupId;
	}
	else
	{
		$mPars['selGrupRef']=$mGrup['id'];
	}
	$mPars['grup_id']=$mPars['selGrupRef'];
	$mGrup=getGrup($db);
	$mUsuarisRef=db_getUsuarisRef($db);
	$parsChain=makeParsChain($mPars);
}



echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>"; echo $htmlTitolPags; echo " - GRUPS</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js1_3.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>

<script type='text/javascript'  language='JavaScript'>
navegador();
missatgeAlerta=\"".$missatgeAlerta."\";
</script>

</head>

<body onLoad=\"javascript: if(missatgeAlerta!=''){alert(missatgeAlerta);}\"  bgcolor='".$mColors['body']."'>
";
html_demo('nouGrup.php?');
echo "
	<table align='center' style='width:80%' bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			<p>(".$mContinguts['form']['titol'].")</p>
			</td>
		</tr>
	</table>
	
	<table align='center' bgcolor='".$mColors['table']."'>
		<tr>
			<th style='width:100%;' align='center'>
";
//*v36.5-funcio call
mostrarSelectorRuta(1,'nouGrup.php');
echo "
			</td>
		</tr>
	</table>
	<table border='1' align='center' style='width:80%' bgcolor='".$mColors['table']."'>
		<tr>
			<td align='center' style='width:100%'>
			<table width='100%'>
				<tr>
					<td align='center' width='33%'  valign='top'>
					<p>Selecciona un grup per editar-lo:<br>
					<select id='sel_grupId' name='sel_grupId' onChange=\"this.value; enviarFparsGrupEditar('nouGrup.php?sR=".$mPars['selRutaSufix']."&iGed='+this.value+'&op=editarGrup','_self');\">
					";
					$selected1='';
					$selected2='selected';
					while(list($key,$mGrup_)=each($mGrups))
					{
						if($mGrup_['id']==$mGrup['id'])
						{
							$selected1='selected';$selected2='';}else{$selected1='';
						}
						echo "
					<option "; if($mGrup_['estat']=='actiu'){echo " class='oGrupActiu' ";}else if($mGrup_['estat']=='inactiu'){echo " class='oGrupInactiu' ";} echo " ".$selected1." value='".$mGrup_['id']."'>".(urldecode($mGrup_['nom']))."</option>
						";					
					}
					reset($mGrups);
					echo "
					<option ".$selected2." value=''></option>
					</select>
					<br>
					( * en verd els grups actius)
					</p>
					</td>
					<td align='center' width='33%' valign='middle'>
					<input type='button' value='Crear nou grup' onClick=\"enviarFparsGrupEditar('nouGrup.php?sR=".$mPars['selRutaSufix']."&op=nouGrup','_self');\">
					</td>
					<td align='center' width='33%' valign='middle'>
					<input type='button' value='editar categories de grup' onClick=\"enviarFparsGrupEditar('nouGrup.php?op=editarCategoriesGrup','_self');\">
					</td>
				</tr>
			</table>					
			</td>
		</tr>
";
if ($opcio=='nouGrup')
{
	echo "
		<tr>
			<td align='left' style='width:100%'>
			<center><p>[Crear Grup]</p></center>
			<table  align='center'  style='width:70%'>
				<tr>
					<td style='width:100%' align='left'>
					<form id='f_nouGrup' name='f_nouGrup' method='POST' action='nouGrup.php?sR=".$mPars['selRutaSufix']."' target='_self' >
					<input type='hidden' name='i_opcio' value='crearGrup'>
					<table align='center'  style='width:100%'>
						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							<p  style='font-size:15px;'><i>- Dades del grup -</i></p>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Nom </p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_nomGrup' name='i_nomGrup' size='30' value=\"".(urldecode($mGrup['nom']))."\">
							</td>
						</tr>

						<tr>
							<td align='left' valign='top'>
							<p><b>categoria</b></p>
							</td>
							<td align='left' valign='top'>
							<table width='100%'>
								<tr>
									<td> 
									<select id='sel_categoria' name='sel_categoria[]' multiple>
					";
					$selected1='';
					$selected2='selected';
					while(list($index,$categoria)=each($mCategoriesGrups))
					{
						if(substr_count($mGrup['categoria'],$categoria)>0)
						{
							$selected1='selected';$selected2='';}else{$selected1='';
						}
						echo "
									<option ".$selected1." value='".$categoria."'>".$categoria."</option>
						";
					}
					reset($mCategoriesGrups);
					echo "
									<option ".$selected2." value=''></option>
									</select>
									</td>
									<td>
									<p>&nbsp;&nbsp;</p>
									</td>
									<td>
									<p class='nota'>
									* les categories amb els mateixos prefixes son incompatibles entre elles
									<br>
									** la categoria magatzem, ha d'estar vinculada a una categoria de zona (tipus 2)
									<br>
									*** nom�s els grups amb categoria 'rebost' poden tenir categoria 'magatzem'
									</p>
									</td>
								</tr>
							</table>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Municipi</p>
							</th>
							<td align='left' valign='top'>
							<select id='sel_municipi' name='sel_municipi'>
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mMunicipi)=each($mMunicipis))
					{
						if($mMunicipi['municipi']==$mGrup['municipi'])
						{
							$selected1='selected';
							$selected2='';
						}
						else
						{
							$selected1='';
						}
						echo "
							<option ".$selected1." value='".$mMunicipi['id']."'>".(urldecode($mMunicipi['municipi']))."</option>
						";
					}
					reset($mMunicipis);
					echo "
							<option ".$selected2." value=''></option>
							</select>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Codi ecoxarxa</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_codiEcoxarxa' name='i_codiEcoxarxa' value='".$mGrup['codi_ecoxarxa']."'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Adre�a del grup:
							<br>(per a entregues)
							</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_adre�a' name='i_adre�a' size='30' value=\"".(urldecode($mGrup['adressa']))."\">
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Peticions alta/baixa usuaries</p>
							</th>
							<td align='left' valign='top'>
							<textArea id='ta_notes'  name='ta_notes' cols='50' rows='7'>".(urldecode($mGrup['notes']))."</textArea>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Email</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_email' name='i_email' size='30' value='".$mGrup['email_rebost']."'>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Mobil</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_mobil' name='i_mobil' size='9' value='".$mGrup['mobil']."'>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Compte Ecos</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_compteEcos' name='i_compteEcos' size='8' value='".$mGrup['compte_ecos']."'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Compte Ecobasics</p>
							</th>
							<td align='left' valign='top'>
							<p>CIEB<input type='text' id='i_compteCieb' name='i_compteCieb' size='4' value='".$mGrup['compte_cieb']."'></p>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>&nbsp;</p>
							</th>
							<td align='left' valign='top'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							<input type='button' onClick=\"if(!checkFormCrearGrup()){alert(missatgeAlerta);}else{document.getElementById('f_nouGrup').submit();}\" value='crear nou grup'>
							</td>
						</tr>
					</table>
					<input type='hidden'  name='i_pars' value='".$parsChain."'>
					</form>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	";
}
else if($opcio=='editarGrup')
{
	echo "
		<tr>
			<td align='left' style='width:100%'>
			<center><p>[Editar Grup]</p></center>
			<form id='f_nouGrup' name='f_nouGrup' method='POST' action='nouGrup.php' target='_self' >
			<input type='hidden' name='i_opcio' value='guardarGrup'>
			<table  align='center'  style='width:70%'>
				<tr>
					<td style='width:100%' align='left'>
					<table align='center'  style='width:100%'>
						<tr>
							<th align='left' valign='top'>
							</th>
							<td align='left' valign='top'>
							<p  style='font-size:15px;'><i>- Dades del grup -</i></p>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Nom </p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_nomGrup' name='i_nomGrup' size='30' value=\"".(urldecode($mGrup['nom']))."\">
							</td>
						</tr>
						<tr>
							<td align='left' valign='top'>
							<p><b>categoria</b></p>
							</td>
							<td align='left' valign='top'> 
							<table width='100%'>
								<tr>
									<td width='25%' valign='top'> 
									<select id='sel_categoria' name='sel_categoria[]' multiple>
					";
					$selected1='';
					$selected2='selected';
					while(list($index,$categoria)=each($mCategoriesGrups))
					{
						if(substr_count($mGrup['categoria'],$categoria.',')>0)
						{
							$selected1='selected';
							$selected2='';
						}
						else
						{
							$selected1='';
						}
						echo "
									<option ".$selected1." value='".$categoria."'>".$categoria."</option>
						";
					}
					reset($mCategoriesGrups);
					echo "
									<option ".$selected2." value=''></option>
									</select>
									</td>
									<td>
									<p>&nbsp;</p>
									</td>
									<td valign='top' align='left' width='25%'> 
									<p style='color:#aaaaaa;'>
														";
					while(list($index,$categoria)=each($mCategoriesGrups))
					{
						if(substr_count($mGrup['categoria'],$categoria.',')>0)
						{
						echo "
									<font><img src='imatges/okp.gif'>&nbsp;".$categoria."</font><br>
						";
						}
					}
					reset($mCategoriesGrups);
					echo "
									</p>
									</td>
									<td width='50%' valign='top'>  
									<p class='nota'>
									* les categories amb els mateixos prefixes son incompatibles entre elles
									<br>
									** la categoria magatzem, ha d'estar vinculada a una categoria de zona (tipus 2)
									<br>
									*** nom�s els grups amb categoria 'rebost' poden tenir categoria 'magatzem'
									</p>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Productors Associats</p>
							</th>
							<td align='left' valign='top'>
							<table width='100%'>
								<tr>
									<td width='25%' valign='top'> 
									<select id='sel_productorsAssociats' name='sel_productorsAssociats[]' multiple>
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mProductor)=each($mProductors))
					{
						if(substr_count($mGrup['productors_associats'],','.$mProductor['id'].',')>0)
						{
							$selected1='selected';
							$selected2='';
						}
						else
						{
							$selected1='';
						}
						echo "
									<option ".$selected1." value='".$mProductor['id']."'>".(urldecode($mProductor['projecte']))."</option>
						";
					}
					reset($mProductors);
					echo "
							<option ".$selected2." value=''></option>
									</select>
									</td>
									<td>
									<p>&nbsp;</p>
									</td>
									<td valign='top' align='left' width='25%'> 
									<p style='color:#aaaaaa;'>
														";
					while(list($id,$mProductor)=each($mProductors))
					{
						if(substr_count($mGrup['productors_associats'],','.$mProductor['id'].',')>0)
						{
							echo "
									<font><img src='imatges/okp.gif'>&nbsp;".(urldecode($mProductor['projecte']))."</font><br>
							";
						}
					}
					reset($mProductors);
					echo "
									</p>
									</td>
									<td valign='top' align='left' width='25%'> 
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Municipi</p>
							</th>
							<td align='left' valign='top'>
							<select id='sel_municipi' name='sel_municipi'>
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mMunicipi)=each($mMunicipis))
					{
						if($mMunicipi['municipi']==$mGrup['municipi'])
						{
							$selected1='selected';
							$selected2='';
						}
						else
						{
							$selected1='';
						}
						echo "
							<option ".$selected1." value='".$mMunicipi['id']."'>".(urldecode($mMunicipi['municipi']))."</option>
						";
					}
					reset($mMunicipis);
					echo "
							<option ".$selected2." value=''></option>
							</select>
							</td>
						</tr>
						<tr>
							<td align='left' valign='top'>
							<p><b>propietats</b></p>
							</td>
							<td align='left' valign='top'>
							<table width='100%' border='0'>
								<tr>
	";
	$disabled=" DISABLED ";

	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'){$disabled='';}
	echo "
									<td width='100%' > 
									<table width='100%'>
	";
	$mPropietatsGrup=getPropietats($mGrup['c_ecos']);
	$mCategoriesGrup=getCategoriesGrup($mGrup,$db);
	$selected1='';
	$selected2='selected';
	while(list($propietat,$mOpcions)=each($mPropietatsGrups))
	{
		if(is_array($mOpcions)){sort($mOpcions);}
		if(@$mCategoriesGrup[1]=='rebost' && $propietat=='CAL')
		{
			echo "
										<tr>			
											<td align='right'>
											<p>".$propietat.":</p>
											</td>
											<td align='left'>
											<select id='sel_".$propietat."' name='sel_".$propietat."' ".$disabled.">
			";

			while(list($index,$opcio)=each($mOpcions))
			{
				if(array_key_exists($propietat,$mPropietatsGrup) && $index==$mPropietatsGrup[$propietat])
				{
					$selected1='selected';
					$selected2='';
				}
				else
				{
					$selected1='';
				}
				echo "
											<option ".$selected1." value='".$index."'>".$opcio."</option>
				";
			}
			reset($mOpcions);
						
			echo "
											<option ".$selected2." value=''></option>
											</select>
											</td>
										</tr>
			";
		}
		else if($propietat!='CAL')
		{
			echo "
										<tr>			
											<td align='right'>
											<p>".$propietat.":</p>
											</td>
											<td align='left'>
											<input type='text' size='10' id='sel_".$propietat."' name='sel_".$propietat."' ".$disabled." value='".@$mPropietatsGrup[$propietat]."'>
											</td>
										</tr>
			";
		}
	}
	reset($mPropietatsGrups);
				
	echo "
									</table>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Codi ecoxarxa</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_codiEcoxarxa' name='i_codiEcoxarxa' value='".$mGrup['codi_ecoxarxa']."'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Adre�a del grup:
							<br>(per a entregues)
							</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_adre�a' name='i_adre�a' size='30' value=\"".(urldecode($mGrup['adressa']))."\">
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Responsable de Grup</p>
							</th>
							<td align='left' valign='top'>
							<table width='100%'>
								<tr>
									<td width='25%' valign='top'> 
									<select id='sel_responsableGrupId' name='sel_responsableGrupId' onChange=\"javascript:modificarDadesGrup(this.value);\">
					";
					$selected1='';
					$selected2='selected';
					
					while(list($id,$mUsuariGrup)=each($mUsuarisRef))
					{
						if($mGrup['usuari_id']==$id)
						{
							$selected1='selected';
							$selected2='';
						}
						echo "
									<option id='op_".$id."' ".$selected1." value='".$id."'>".(urldecode($mUsuariGrup['usuari']))." (".$mUsuariGrup['email'].";".$mUsuariGrup['mobil'].")</option>
						";
						$selected1='';
					}
					reset($mProductors);
					echo "
							<option ".$selected2." value=''></option>
									</select>
									</td>
								</tr>
							</table>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Peticions pendents:</p>
							</th>
							<td align='left' valign='top'>
							<textArea id='ta_notes' readonly style='background-color:#eeeeee;' name='ta_notes' cols='50' rows='7'>".(urldecode($mGrup['notes']))."</textArea>
							</td>
						</tr>

						<tr>
							<th align='left' valign='top'>
							<p>Email</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_email' name='i_email' size='30' value='".$mGrup['email_rebost']."'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Mobil</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_mobil' name='i_mobil' size='9' value='".$mGrup['mobil']."'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Compte Ecos</p>
							</th>
							<td align='left' valign='top'>
							<input type='text' id='i_compteEcos' name='i_compteEcos' size='8' value='".$mGrup['compte_ecos']."'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>Compte Ecobasics</p>
							</th>
							<td align='left' valign='top'>
							<p>CIEB<input type='text' id='i_compteCieb' name='i_compteCieb' size='4' value='".$mGrup['compte_cieb']."'></p>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<p>&nbsp;</p>
							</th>
							<td align='left' valign='top'>
							</td>
						</tr>
						<tr>
							<th align='left' valign='top'>
							<table width='100%' border='1'>
								<tr>
									<td align='center' valign='top'>
									";
									if($mGrup['estat']=='inactiu'  || $mGrup['estat']=='')
									{
										echo "
										<p class='pAlertaNo4'>GRUP INACTIU</p>
										<input type='button' onClick=\"enviarFparsGrupEditar('nouGrup.php?iGed='+".$mGrup['id']."+'&op=activarGrup','_self')\" value='activar'>
										";
									} 
									else if($mGrup['estat']=='actiu')
									{
										echo "
										<p class='pAlertaOk4'>GRUP ACTIU</p>
										<input type='button' onClick=\"enviarFparsGrupEditar('nouGrup.php?iGed='+".$mGrup['id']."+'&op=desactivarGrup','_self')\" value='desactivar'>
										";
									} 
									echo "
									</td>
								</tr>
							</table>
							</th>
							<td align='left' valign='top'>
							<table width='100%'>
								<tr>
									<td align='center' valign='top'>
									<input type='button' onClick=\"if(!checkFormGuardarGrup()){alert(missatgeAlerta);}else{document.getElementById('f_nouGrup').submit();}\" value='guardar grup'>
									</td>
									<td align='center' valign='top'>
									<input type='button' onClick=\"enviarFparsGrupEditar('nouGrup.php?iGed='+".$mGrup['id']."+'&op=eliminarGrup','_self')\" value='eliminar grup'><br>(* nom�s si no t� comanda en curs)
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					<input type='hidden' name='i_pars' value='".$parsChain."'>
					</form>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	";
}
else if($opcio=='editarCategoriesGrup')
{
	echo "
		<tr>
			<td align='center' style='width:100%'>
			<center><p>[Editar Categories de Grup]</p></center>
			<form id='f_categoriesGrup1' name='categoriesGrup1' method='POST' action='nouGrup.php' target='_self' >
			<input type='hidden' name='i_opcio' value='editarCategoriesGrup'>
			<table  align='center'  style='width:30%'>
				<tr>
					<td style='width:100%' align='left'>
					<p  style='font-size:15px;'><i>- Categories de Grup -</i></p>
					<table align='center'  style='width:100%'>
					";
					while(list($index,$categoria)=each($mCategoriesGrups))
					{
						echo "
						<tr>
							<td align='left' valign='top'>
							<p>".$categoria."</p>							
							</td>
							<td align='left' valign='top'>
							<input type='button' value='eliminar'>
							</td>
						</tr>
						";
					}
					reset($mCategoriesGrups);
					echo "
					</table>
					<input type='hidden' name='i_pars' value='".$parsChain."'>
					<p> [inhabilitat: irreversible. Elimina la categoria del perfil de tots els grups]</p>
					</form>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align='center' style='width:100%'>
			<center><p>[Afegir Nova Categoria de Grup]</p></center>
			<form id='f_categoriesGrup2' name='f_categoriesGrup2' method='POST' action='nouGrup.php' target='_self' >
			<input type='hidden' name='i_opcio' value='afegirCategoriaGrup'>
			<table  align='center'  style='width:30%'>
				<tr>
					<td style='width:100%' align='left'>
					<p  style='font-size:15px;'><i>- Afegir Nova Categoria de Grup -</i></p>
					<table align='center'  style='width:100%'>
						<tr>
							<td align='left' valign='top'>
							<input type='text' id='i_novaCategoriaGrup' name='i_novaCategoriaGrup'value=''>							
							</td>
							<td align='left' valign='top'>
							<input type='button'  onClick=\"if(!checkFormNovaCategoria()){alert(missatgeAlerta);}else{document.getElementById('f_categoriesGrup2').submit();}\"  value='afegir'>
							</td>
						</tr>
					</table>
					<input type='hidden' name='i_pars' value='".$parsChain."'>
					</form>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	";
}
echo "
	</table>
	";
	html_helpRecipient();
echo "
<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='nouGrup.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</div>
	
</body>
</html>
";
?>
	




		