<?php
//------------------------------------------------------------------------------
function guardarTransferenciaInterna($OptGET,$timeGuardarRevisio,$TIpO,$TIpD,$TIq,$TIc,$db)
{
	global $mPars,$mProductes;
	
	$missatgeAlerta='';
	$mMissatgeAlerta['missatge']='';
	$mMissatgeAlerta['result']=true;
	
	$mInventariG=db_getInventari($mPars['grup_id'],$db);
	$inventariId1=$mInventariG['id'];

	// comprovar estoc origen
	if(substr_count($mInventariG['resum'],'producte_'.$TIpO.':')>0)	// comprovar que el producte esta amb estoc positiu a l'inventari
	{
		$pos=strpos($mInventariG['resum'],'producte_'.$TIpO.':')+strlen('producte_'.$TIpO.':');
		$quantitatOold_=substr($mInventariG['resum'],$pos);
		$quantitatOold=substr($quantitatOold_,0,strpos($quantitatOold_,';'));

		if($quantitatOold*1<$TIq*1) //l'estoc d'origen es menor que la quantitat a transferir
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Interna;<br>l'estoc d'origen es menor que la quantitat a transferir</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}
		else if(($TIq/$mProductes[$TIpO]['format'])!=(floor($TIq/$mProductes[$TIpO]['format']))) //la quant. a transf. no es un multiple del format d'origen
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Interna;<br>la quant. a transf. no es un multiple del format d'origen</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}		
		else if(($TIq/$mProductes[$TIpD]['format'])!=(floor($TIq/$mProductes[$TIpD]['format']))) //la quant. a transf. no es un multiple del format de desti
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Interna;<br>la quant. a transf. no es un multiple del format de desti</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}		
		else if($mProductes[$TIpO]['categoria10']!=$mProductes[$TIpD]['categoria10']) //la subcategoria d'origen no coincideix amb la de desti
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Interna;<br>la subcategoria d'origen no coincideix amb la de desti</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}		
		else
		{
			//restar quantitat a origen		
			$mInventariG['resum']=str_replace('producte_'.$TIpO.':'.$quantitatOold.';','producte_'.$TIpO.':'.($quantitatOold-$TIq).';',$mInventariG['resum']);

			//comprovar estoc desti i afegir quantitat a desti		
			
			// 1. si estoc desti>0
			if(substr_count($mInventariG['resum'],'producte_'.$TIpD.':')>0)	// comprovar que el producte esta amb estoc positiu a l'inventari
			{
				$pos=strpos($mInventariG['resum'],'producte_'.$TIpD.':')+strlen('producte_'.$TIpD.':');
				$quantitatDold_=substr($mInventariG['resum'],$pos);
				$quantitatDold=substr($quantitatDold_,0,strpos($quantitatDold_,';'));

				$mInventariG['resum']=str_replace('producte_'.$TIpD.':'.$quantitatDold.';','producte_'.$TIpD.':'.($quantitatDold+$TIq).';',$mInventariG['resum']);
			}
			else // 2. si estoc desti=0
			{
				$mInventariG['resum'].='producte_'.$TIpD.':'.(@$quantitatDold+$TIq).';';
			}
		}
		//actualitzar l'�ltim inventari
		$mInventariG2=db_getInventari($mPars['grup_id'],$db);
		if($inventariId1!=$mInventariG2['id'])
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Interna;<br> s'ha generat un nou inventari durant aquesta transferencia</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}
		else
		{
			$revisio="[".$OptGET.";".$timeGuardarRevisio.";".$mPars['usuari'].";".$TIpO.";".$TIq.";".$TIpD.";".$TIc."]";
			
			//comprovar si ja s'ha enviat una revisio amb la mateixa marca de temps:
			
			$result=@mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." where LOCATE('".$timeGuardarRevisio."',revisions)>0 AND id='".$mInventariG2['id']."'",$db);
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			if(!$mRow)
			{
				//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
				if(!$result=@mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set resum='".$mInventariG['resum']."', revisions=CONCAT('".$revisio."',revisions) where id='".$mInventariG2['id']."'",$db))
				{
					//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
	
					$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha pogut guardar la Transferencia Interna</p>";
					$mMissatgeAlerta['result']=false;
			
					return $mMissatgeAlerta;
				}
				else
				{
					$mMissatgeAlerta['missatge']="<p class='pAlertaOk'><i>S'ha pogut guardar la Transferencia Interna correctament</p>";
					$mMissatgeAlerta['result']=true;
			
					return $mMissatgeAlerta;
				}
			}
			else
			{
				$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha guardat la Transferencia Interna;<br>no es pot repetir l'operaci� recarregant la p�gina</p>";
				$mMissatgeAlerta['result']=false;
			
				return $mMissatgeAlerta;
			}
		}
	}
	else
	{
		$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Interna;<br>No s'ha trobat el producte d'origen.</p>";
		$mMissatgeAlerta['result']=false;
			
		return $mMissatgeAlerta;
	}

	$mMissatgeAlerta['missatge']=$missatge;

	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function guardarTransferenciaExterna($OptGET,$timeGuardarRevisio,$TEpO,$TEq,$TEc,$db)
{
	global $mPars,$mProductes;
	
	$mMissatgeAlerta['result']=false;
	$mMissatgeAlerta['missatge']='';

	$mInventariG=db_getInventari($mPars['grup_id'],$db);
	$inventariId1=$mInventariG['id'];

	$mInventariGDesti=db_getInventari($mPars['refMagatzemDesti'],$db);
	$inventariId2=$mInventariGDesti['id'];

	// comprovar estoc origen
	if(substr_count($mInventariG['resum'],'producte_'.$TEpO.':')>0)	// comprovar que el producte esta amb estoc positiu a l'inventari
	{
		$pos=strpos($mInventariG['resum'],'producte_'.$TEpO.':')+strlen('producte_'.$TEpO.':');
		$quantitatOold_=substr($mInventariG['resum'],$pos);
		$quantitatOold=substr($quantitatOold_,0,strpos($quantitatOold_,';'));

		if($quantitatOold*1<$TEq*1) //l'estoc d'origen es menor que la quantitat a transferir
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa;<br>l'estoc d'origen es menor que la quantitat a transferir</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}
		else if(($TEq/$mProductes[$TEpO]['format'])!=(floor($TEq/$mProductes[$TEpO]['format']))) //la quant. a transf. no es un multiple del format d'origen
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa;<br>la quant. a transf. no es un m�ltiple del format d'origen</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}		
		else
		{
			//restar quantitat a origen		
			$mInventariG['resum']=str_replace('producte_'.$TEpO.':'.$quantitatOold.';','producte_'.$TEpO.':'.($quantitatOold-$TEq).';',$mInventariG['resum']);

			//comprovar estoc desti i afegir quantitat a desti		
			
			// 1. si estoc desti>0
			if(substr_count($mInventariGDesti['resum'],'producte_'.$TEpO.':')>0)	// comprovar que el producte esta amb estoc positiu a l'inventari
			{
				$pos=strpos($mInventariGDesti['resum'],'producte_'.$TEpO.':')+strlen('producte_'.$TEpO.':');
				$quantitatDold_=substr($mInventariGDesti['resum'],$pos);
				$quantitatDold=substr($quantitatDold_,0,strpos($quantitatDold_,';'));

				$mInventariGDesti['resum']=str_replace('producte_'.$TEpO.':'.$quantitatDold.';','producte_'.$TEpO.':'.($quantitatDold+$TEq).';',$mInventariGDesti['resum']);
			}
			else // 2. si estoc desti=0
			{
				if(!isset($quantitatDold)){$quantitatDold=0;}
				$mInventariGDesti['resum'].='producte_'.$TEpO.':'.($quantitatDold+$TEq).';';
			}
		}
		
		//actualitzar l'�ltim inventari del magatzem d'origen
		$mInventariG2=db_getInventari($mPars['grup_id'],$db);
		if($inventariId1!=$mInventariG2['id'])
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa;<br>s'ha generat un nou inventari al magatzem Origen durant aquesta transferencia</p>";
			$mMissatgeAlerta['result']=false;
			
			return $mMissatgeAlerta;
		}
		else
		{
			$revisio="[".$OptGET.";".$timeGuardarRevisio.";".$mPars['usuari'].";".$mPars['grup_id'].";".$TEpO.";".$TEq.";".$TEpO.";".$TEc.";".$mPars['refMagatzemDesti'].";pendent;".$mInventariG2['id'].";".$mInventariGDesti['id']."]";
			
			//comprovar si ja s'ha enviat una revisio amb la mateixa marca de temps:
			
			$result=@mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." where LOCATE('".$timeGuardarRevisio."',revisions)>0 AND id='".$mInventariG2['id']."'",$db);
			$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
			if(!$mRow)
			{
				//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
				if(!$result=@mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set resum='".$mInventariG['resum']."', revisions=CONCAT('".$revisio."',revisions) where id='".$mInventariG2['id']."'",$db))
				{
					//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
	
					$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha pogut guardar la Transferencia Externa</p>";
					$mMissatgeAlerta['result']=false;
			
					return $mMissatgeAlerta;
				}
				else
				{
					//actualitzar l'�ltim inventari del magatzem de desti
					$mInventariG2=db_getInventari($mPars['refMagatzemDesti'],$db);
					if($inventariId2!=$mInventariG2['id'])
					{
						$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa;<br> s'ha generat un nou inventari al magatzem Desti durant aquesta transferencia</p>";
						$mMissatgeAlerta['result']=false;
				
						return $mMissatgeAlerta;
					}
					else
					{
						//$revisio="[".$OptGET.";".$timeGuardarRevisio.";".$mPars['usuari'].";".$mPars['grup_id'].";".$TEpO.";".$TEq.";".$TEpD.";".$TEc.";".$mPars['refMagatzemDesti']."]";
			
						//comprovar si ja s'ha enviat una revisio amb la mateixa marca de temps:
			
						$result=@mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." where LOCATE('".$timeGuardarRevisio."',revisions)>0 AND id='".$mInventariG2['id']."'",$db);
						$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
						if(!$mRow)
						{
							//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
							//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
							if(!$result=@mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set resum='".$mInventariGDesti['resum']."', revisions=CONCAT('".$revisio."',revisions) where id='".$mInventariG2['id']."'",$db))
							{
								//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
								//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
	
								$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha pogut guardar la Transferencia Externa al Desti</p>";
								$mMissatgeAlerta['result']=false;
				
								return $mMissatgeAlerta;
							}
						}
						else
						{
							$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha guardat la Transferencia Externa;<br> no es pot repetir l'operaci� recarregant la p�gina</p>";
							$mMissatgeAlerta['result']=false;
				
							return $mMissatgeAlerta;
						}
					}

					$mMissatgeAlerta['missatge']="<p class='pAlertaOk'><i>S'ha pogut guardar la Transferencia Externa correctament</p>";
					$mMissatgeAlerta['result']=true;
				
					return $mMissatgeAlerta;
				}
			}
			else
			{
				$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha guardat la Transferencia Externa;<br> no es pot repetir l'operaci� recarregant la p�gina</p>";
				$mMissatgeAlerta['result']=false;
				
				return $mMissatgeAlerta;
			}
		}
	}
	else
	{
		$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa;<br No s'ha trobat el producte d'origen.</p>";
		$mMissatgeAlerta['result']=false;
				
		return $mMissatgeAlerta;
	}

	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function db_validarTE($VR,$iIo,$iId,$db)
{
	global $mPars,$mUsuari;
	
	$mMissatgeAlerta['result']=false;
	$mMissatgeAlerta['missatge']='';

	$mRevisionsRef=array();
	
	if($result=mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." where LOCATE('".$VR."',revisions)>0 AND id='".$iId."'",$db))
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		if($mRow)
		{
			 $mRevisionsRef=getRevisionsRef($mRow['revisions']);
			 $revisioMod='[';
			 $revisio='[';
			 while(list($key,$val)=each($mRevisionsRef[$VR]))
			 {
			 	 $revisio.=$val.';';
				 if($key==9 && $val=='pendent')
				 {
				 	$val='val.lidada (<i>'.$mUsuari['usuari'].'</i>, uId:'.$mPars['usuari_id'].', '.(date('d/m/y H:i:s')).')';
				 }
			 	 $revisioMod.=$val.';';
				 
			 }
			 $revisioMod=substr($revisioMod,0,strlen($revisioMod)-1);
			 $revisioMod.=']';
			 $revisio=substr($revisio,0,strlen($revisio)-1);
			 $revisio.=']';
	
			//echo "<br>"."update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set revisions=REPLACE(revisions,'".$revisio."','".$revisioMod."') where id='".$iId."'  OR id='".$iIo."'";
			if($result=@mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set revisions=REPLACE(revisions,'".$revisio."','".$revisioMod."') where id='".$iId."' OR id='".$iIo."'",$db))
			{
				$mMissatgeAlerta['missatge']="<p class='pAlertaOk'>S'ha val.lidat la recepci� del producte correctament</p>";
				$mMissatgeAlerta['result']=true;
			}

		}
	}
	else
	{
		//echo "<br> 456 db_movimentsEstocs.php ".mysql_errno() . ": " . mysql_error(). "\n";
	}

	return	$mMissatgeAlerta;
}

//------------------------------------------------------------------------------

function db_anularTE($AR,$iIo,$iId,$db)
{
	global $mPars,$mInventariG,$mRevisionsRef,$mUsuari;
	

	$mMissatgeAlerta['result']=false;
	$mMissatgeAlerta['missatge']='';
	
	$mInventariGOrigen=db_getInventari($mRevisionsRef[$AR][3],$db);

	//modificar la revisio
	 $revisioMod='[';
	 $revisio='[';
	 while(list($key,$val)=each($mRevisionsRef[$AR]))
	 {
	 	 $revisio.=$val.';';
		 if($key==9)
		 {
		 	if
			(
			 	$val=='pendent'
				||
				(
					(
						$mPars['nivell']=='sadmin' 
						|| 
						$mPars['nivell']=='admin'
					)
					&& 
					substr($val,0,3)=='val'
				)
			)
			{
			 	$val='anul.lada (<i>'.$mUsuari['usuari'].'</i>, uId:'.$mPars['usuari_id'].', '.(date('d/m/y H:i:s')).')]';
		 	}
		 }
	 	 $revisioMod.=$val.';';
	 }
	 $revisioMod=substr($revisioMod,0,strlen($revisioMod)-1);
	 $revisioMod.=']';
	 $revisio=substr($revisio,0,strlen($revisio)-1);
	 $revisio.=']';
	
	//modificar inventari magatzem desti (receptor local)

	$mInventariG_['resum']=str_replace('producte_'.$mRevisionsRef[$AR][4].':'.$mInventariG['inventariRef'][$mRevisionsRef[$AR][4]].';','producte_'.$mRevisionsRef[$AR][4].':'.($mInventariG['inventariRef'][$mRevisionsRef[$AR][4]]-$mRevisionsRef[$AR][5]).';',$mInventariG['resum']);
	
	if(!$result=@mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set resum='".$mInventariG_['resum']."', revisions=REPLACE(revisions,'".$revisio."','".$revisioMod."') where id='".$iId."'",$db))
	{
		//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');

		$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha pogut retrocedir la transferencia a l'inventari del magatzem receptor (local). La Transferencia no s'ha anul.lat</p>";
		$mMissatgeAlerta['result']=false;
				
		return $mMissatgeAlerta;
	}
	
	
	//modificar inventari magatzem origen (emisor remot)
	$mInventariGOrigen_['resum']=str_replace('producte_'.$mRevisionsRef[$AR][4].':'.$mInventariGOrigen['inventariRef'][$mRevisionsRef[$AR][4]].';','producte_'.$mRevisionsRef[$AR][4].':'.($mInventariGOrigen['inventariRef'][$mRevisionsRef[$AR][4]]+$mRevisionsRef[$AR][5]).';',$mInventariGOrigen['resum']);
	
	if(!$result=@mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set resum='".$mInventariGOrigen_['resum']."', revisions=REPLACE(revisions,'".$revisio."','".$revisioMod."') where id='".$iIo."'",$db))
	{
		//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');

		$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha pogut retrocedir la transferencia a l'inventari del magatzem emisor (remot)</p>";
		$mMissatgeAlerta['result']=false;
				
		return $mMissatgeAlerta;

		//recular la modificacio de l'inventari al magatzem desti (receptor local)
		if(!$result=@mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set resum='".$mInventariG['resum']."', revisions=REPLACE(revisions,'".$revisioMod."','".$revisio."') where id='".$iId."'",$db))
		{
			//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');

			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha pogut retrocedir la transferencia a l'inventari del magatzem receptor (local). La Transferencia ha provocat un error a l'inventari del magatzem local.</p>";
			$mMissatgeAlerta['result']=false;
				
			return $mMissatgeAlerta;
		}
	}

	
	$mMissatgeAlerta['missatge']="<p class='pAlertaOk'>S'ha anul.lat la transfer�ncia del producte correctament</p>";
	$mMissatgeAlerta['result']=true;
	
	

	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function guardarTransferenciaExternaIO($OptGET,$timeGuardarRevisio,$TEIOpO,$TEIOq,$TEIOc,$db)
{
	global $mPars,$mProductes;

	$mMissatgeAlerta['result']=false;
	$mMissatgeAlerta['missatge']='';

	$mInventariG=db_getInventari($mPars['grup_id'],$db);
	$inventariId1=$mInventariG['id'];

	if($mPars['refMagatzemDesti']=='EA' || $mPars['refMagatzemDesti']=='ER')
	{
		// comprovar estoc origen
		if(substr_count($mInventariG['resum'],'producte_'.$TEIOpO.':')>0)	// comprovar que el producte esta amb estoc positiu a l'inventari
		{
			$pos=strpos($mInventariG['resum'],'producte_'.$TEIOpO.':')+strlen('producte_'.$TEIOpO.':');
			$quantitatOold_=substr($mInventariG['resum'],$pos);
			$quantitatOold=substr($quantitatOold_,0,strpos($quantitatOold_,';'));
		}
		else
		{
			$quantitatOold=0;
		}
		if(($TEIOq/$mProductes[$TEIOpO]['format'])!=(floor($TEIOq/$mProductes[$TEIOpO]['format']))) //la quant. a transf. no es un multiple del format d'origen
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa (E/S);<br>la quant. a transf. no es un multiple del format d'origen</p>";
			$mMissatgeAlerta['result']=false;
				
			return $mMissatgeAlerta;
		}		
		else
		{
			//actualitzar quantitat a origen		
			if($quantitatOold==0)
			{
				$mInventariG['resum'].='producte_'.$TEIOpO.':'.($quantitatOold+$TEIOq).';';
			}
			else
			{
				$mInventariG['resum']=str_replace('producte_'.$TEIOpO.':'.$quantitatOold.';','producte_'.$TEIOpO.':'.($quantitatOold+$TEIOq).';',$mInventariG['resum']);
			}
		}
		//actualitzar l'�ltim inventari del magatzem d'origen
		$mInventariG2=db_getInventari($mPars['grup_id'],$db);
		if($inventariId1!=$mInventariG2['id'])
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa (E/S);<br>s'ha generat un nou inventari al magatzem Origen durant aquesta transferencia</p>";
			$mMissatgeAlerta['result']=false;
				
			return $mMissatgeAlerta;
		}
		else
		{
			if($mPars['refMagatzemDesti']=='EA') //entrada abastiment
			{
				$revisio="[".$OptGET.";".$timeGuardarRevisio.";".$mPars['usuari'].";".$mPars['grup_id'].";".$TEIOpO.";".$TEIOq.";".$TEIOpO.";(Abastiment)".$TEIOc.";".$mPars['refMagatzemDesti']."]";
			}
			else if($mPars['refMagatzemDesti']=='ER') //entrada recuperacio
			{
				$revisio="[".$OptGET.";".$timeGuardarRevisio.";".$mPars['usuari'].";".$mPars['grup_id'].";".$TEIOpO.";".$TEIOq.";".$TEIOpO.";(Recuperaci�)".$TEIOc.";".$mPars['refMagatzemDesti']."]";
			}
		}
	}
	else if($mPars['refMagatzemDesti']=='SD' || $mPars['refMagatzemDesti']=='SP')
	{
		// comprovar estoc origen
		if(substr_count($mInventariG['resum'],'producte_'.$TEIOpO.':')>0)	// comprovar que el producte esta amb estoc positiu a l'inventari
		{
			$pos=strpos($mInventariG['resum'],'producte_'.$TEIOpO.':')+strlen('producte_'.$TEIOpO.':');
			$quantitatOold_=substr($mInventariG['resum'],$pos);
			$quantitatOold=substr($quantitatOold_,0,strpos($quantitatOold_,';'));

			if(($TEIOq/$mProductes[$TEIOpO]['format'])!=(floor($TEIOq/$mProductes[$TEIOpO]['format']))) //la quant. a transf. no es un multiple del format d'origen
			{
				$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa (E/S);<br>la quant. a transf. no es un multiple del format d'origen</p>";
				$mMissatgeAlerta['result']=false;
				
				return $mMissatgeAlerta;
			}		
			else
			{
				//actualitzar quantitat a origen		
				$mInventariG['resum']=str_replace('producte_'.$TEIOpO.':'.$quantitatOold.';','producte_'.$TEIOpO.':'.($quantitatOold-$TEIOq).';',$mInventariG['resum']);
			}		
			//actualitzar l'�ltim inventari del magatzem d'origen
			$mInventariG2=db_getInventari($mPars['grup_id'],$db);
			if($inventariId1!=$mInventariG2['id'])
			{
				$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa (E/S);<br>s'ha generat un nou inventari al magatzem Origen durant aquesta transferencia</p>";
				$mMissatgeAlerta['result']=false;
				
				return $mMissatgeAlerta;
			}
			else
			{
				if($mPars['refMagatzemDesti']=='SD') //sortida distribucio
				{
					$revisio="[".$OptGET.";".$timeGuardarRevisio.";".$mPars['usuari'].";".$mPars['grup_id'].";".$TEIOpO.";".$TEIOq.";".$TEIOpO.";(Distribuci�)".$TEIOc.";".$mPars['refMagatzemDesti']."]";
				}
				else if($mPars['refMagatzemDesti']=='SP') //sortida p�rdues
				{
					$revisio="[".$OptGET.";".$timeGuardarRevisio.";".$mPars['usuari'].";".$mPars['grup_id'].";".$TEIOpO.";".$TEIOq.";".$TEIOpO.";(P�rdua)".$TEIOc.";".$mPars['refMagatzemDesti']."]";
				}
			}
		}
		else
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>Atenci�: No s'ha pogut guardar la Transferencia Externa (E/S);<br No s'ha trobat el producte d'origen.</p>";
			$mMissatgeAlerta['result']=false;
				
			return $mMissatgeAlerta;
		}
	}
	//comprovar si ja s'ha enviat una revisio amb la mateixa marca de temps:
			
	$result=@mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." where LOCATE('".$timeGuardarRevisio."',revisions)>0 AND id='".$mInventariG2['id']."'",$db);
	//echo "<br> 312 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	if(!$mRow)
	{
		//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');
		//echo "<br>update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set resum='".$mInventariG['resum']."', revisions=CONCAT('".$revisio."',revisions) where id='".$mInventariG2['id']."'";
		if(!$result=@mysql_query("update inventaris_".(substr($mPars['selRutaSufix'],0,2))." set resum='".$mInventariG['resum']."', revisions=CONCAT('".$revisio."',revisions) where id='".$mInventariG2['id']."'",$db))
		{
			//echo "<br> 411 db_gestioMagatzems.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'100','db.php');

			$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha pogut guardar la Transferencia Externa (E/S)</p>";
			$mMissatgeAlerta['result']=false;
				
			return $mMissatgeAlerta;
		}
		else	
		{
			$mMissatgeAlerta['missatge']="<p class='pAlertaOk'><i>S'ha pogut guardar la Transferencia Externa (E/S) correctament</p>";
			$mMissatgeAlerta['result']=true;
				
			return $mMissatgeAlerta;
		}
	}
	else
	{
		$mMissatgeAlerta['missatge']="<p class='pAlertaNo'><i>No s'ha guardat la Transferencia Externa (E/S);<br> no es pot repetir l'operaci� recarregant la p�gina</p>";
		$mMissatgeAlerta['result']=false;
				
		return $mMissatgeAlerta;
	}

	return $mMissatgeAlerta;
}




//------------------------------------------------------------------------------
function getResumRevisionsProductes($mRevisions)
{
	global $mPars,$mProductes,$mInventariG;

	$mResumRevisionsProductes=array();
	$mResumTotalsRevisionsProductes=array();
		$mResumTotalsRevisionsProductes['EA']=0;
		$mResumTotalsRevisionsProductes['ER']=0;
		$mResumTotalsRevisionsProductes['SD']=0;
		$mResumTotalsRevisionsProductes['SP']=0;
	while(list($key,$mRevisio)=each($mRevisions))
	{
		@$mResumRevisionsProductes[$mRevisio[4]]['producte']=$mProductes[$mRevisio[4]]['producte'];
		
		//$mResumRevisionsProductes[$mRevisio[4]]['estoc']=$mInventariG['inventariRef'][$mRevisio[4]];

		if($mRevisio[8]=='EA')
		{
			//$mResumRevisionsProductes[$mRevisio[4]]['estoc']+=$mRevisio[5];
			@$mResumRevisionsProductes[$mRevisio[4]]['EA']+=$mRevisio[5];
			$mResumTotalsRevisionsProductes['EA']+=$mRevisio[5];
		}
		else if($mRevisio[8]=='ER')
		{
			//$mResumRevisionsProductes[$mRevisio[4]]['estoc']+=$mRevisio[5];
			@$mResumRevisionsProductes[$mRevisio[4]]['ER']+=$mRevisio[5];
			@$mResumTotalsRevisionsProductes['ER']+=$mRevisio[5];
		}
		else if($mRevisio[8]=='SD')
		{
			//$mResumRevisionsProductes[$mRevisio[4]]['estoc']-=$mRevisio[5];
			@$mResumRevisionsProductes[$mRevisio[4]]['SD']-=$mRevisio[5];
			@$mResumTotalsRevisionsProductes['SD']-=$mRevisio[5];
		}
		else if($mRevisio[8]=='SP')
		{
			//$mResumRevisionsProductes[$mRevisio[4]]['estoc']-=$mRevisio[5];
			@$mResumRevisionsProductes[$mRevisio[4]]['SP']-=$mRevisio[5];
			@$mResumTotalsRevisionsProductes['SP']-=$mRevisio[5];
		}
	}
	reset($mRevisions);

	$mRR[0]=$mResumRevisionsProductes;
	$mRR[1]=$mResumTotalsRevisionsProductes;

	return $mRR;
}





?>

		