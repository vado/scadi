<?php

include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include "html_ajuda1.php";
include "db_ajuda.php";

include "html_gestioTramsDemandes.php";
include "db_gestioTramsDemandes.php";
include "text_transport.php";

$mMissatgeAlerta=array();
$mMissatgeAlerta['result']=false;
$mMissatgeAlerta['missatge']='';

$mActiu=array();
$mActiu[0]='inactiva';
$mActiu[1]='activa';
//POST

$parsChain=@$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

//DB
$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}

// a transport nom�s es pot accedir a la ruta en curs o a les anteriors
$data1= new DateTime();
$t1=$data1->format('y').$data1->format('m');

if(($t1*1-$mPars['selRutaSufix']*1)<0)
{
	$mPars['selRutaSufix']=$t1;
}


getConfig($db);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	$login=false;
}
else
{
	$login=true;

}
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['gestioTramsDemandes.php']=db_getAjuda('gestioTramsDemandes.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$selVehicle_=@$_GET['vId'];
if(isset($selVehicle_)){$mPars['selVehicleId']=$selVehicle_;}

$selTram_=@$_GET['tId'];
if(isset($selTram_)){$mPars['selTramId']=$selTram_;}

$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
$mPars['sortBy']='sortida';
$mPars['ascdesc']='DESC';
$mPars['vTipus']='DEMANDES';
$mPars['vUsuariId']=$mPars['usuari_id'];
$mPars['vVehicle']='TOTS';
$mPars['vCategoria']='TOTS';
$mPars['vSubCategoria']='TOTS';
$mPars['etiqueta']='TOTS';
$mPars['etiqueta2']='CAP';

$mPars['contractamentTancat']=$mParametres['contractamentTransportTancat']['valor'];

$mPars['veureTramsActius']=-1;
$mPars['veureVehiclesActius']=-1;


$mPars['paginacio']=-1;
if(!isset($mPars['pagS'])){$mPars['pagS']=0;}
if(!isset($mPars['pagFiltre'])){$mPars['pagFiltre']=$mPars['sortBy'].$mPars['veureTramsActius'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vVehicle'].$mPars['vSubCategoria'].$mPars['vCategoria'];}
if(!isset($mPars['numItemsPag'])){$mPars['numItemsPag']=10;}
if(!isset($mPars['numPags'])){$mPars['numPags']=0;} 

$mPars['veureTramsNoCaducats']=0;


$vUsuariId_=@$_GET['vUs'];
if(!isset($vUsuariId_))
{
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
	{
		if(!isset($mPars['vUsuariId']))
		{
			$mPars['vUsuariId']='TOTS';
		}
	}
	else{$mPars['vUsuariId']=$mPars['usuari_id'];}
}
else
{
	$mPars['vUsuariId']=$vUsuariId_;
}


$vTipus_=@$_GET['vTt'];
if(isset($vTipus_))
{
	$mPars['vTipus']=$vTipus_;
}

	$mTram=array();
	$mTram['categoria0']='';
$selTramId_=@$_GET['tId'];
if(isset($selTramId_)){$mPars['selTramId']=$selTramId_;}
if(isset($mPars['selTramId']))
{
	$mTram=db_getTram($db);
	if($mTram['usuari_id']!=$mPars['usuari_id'] || $mTram['tipus']!='DEMANDES')
	{
		$mPars['selTramId']='';
	}
}

$mMunicipis=db_getMunicipis2Id($db);
$mCategoriesTrams=getCategoriesTrams($db);
$mTrams=db_getTrams($db);
$mUsuarisRef=db_getUsuarisRef($db);
$mUsuari=db_getUsuari($mPars['usuari_id'],$db);


$opt='vell';
$opt_=@$_GET['opt']; //ve de menu trams
if(isset($opt_)){$opt=$opt_;}

if($opt=='nou')
{
	$mPars['selTramId']='';
	
	$mTram=array(
'id'=>'',
'codi'=>'',
'sortida'=>'',
'municipi_origen'=>'',
'arribada'=>'',
'municipi_desti'=>'',
'municipis_ruta'=>'',
'periodicitat'=>'',
'actiu'=>'0',
'vehicle_id'=>'',
'usuari_id'=>$mPars['usuari_id'],
'categoria0'=>'',
'categoria10'=>'',
'tipus'=>'DEMANDES',
'capacitat_pes'=>'0',
'pes_disponible'=>'0',
'capacitat_volum'=>'0',
'volum_disponible'=>'0',
'capacitat_places'=>'0',
'places_disponibles'=>'0',
'km' => '0',
'estat'=>'',
'descripcio'=>'',
'acords_explicits'=>'',
'preu_pes'=>'',
'preu_volum'=>'',
'preu_places'=>'',
'preu_combustible'=>'',
'pc_ms'=>'',
'propietats'=>''
);
	
}


//VARS
$guardarProducteId='';
$missatgeAlerta='';
$login=false;

$mColsProductes3=array(
//'id',
//'codi',
'sortida',
'municipi_origen',
'arribada',
'municipi_desti',
'municipis_ruta',
//'periodicitat',
'actiu',
//'vehicle_id',
//'usuari_id',
'categoria0',
//'categoria10',
//'tipus',
'capacitat_pes',
'pes_disponible',
'capacitat_volum',
'volum_disponible',
'capacitat_places',
'places_disponibles',
'km',
//'estat',
//'descripcio',
//'acords_explicits',
'preu_pes',
'preu_volum',
'preu_places',
'preu_combustible',
'pc_ms'
//'propietats'
);


$mNomsColumnes3=array(
'actiu'=>'activa',
'id'=>'id',
'codi'=>'codi',
'sortida'=>'sortida',
'municipi_origen'=>'municipi origen',
'arribada'=>'arribada',
'municipi_desti'=>'municipi desti',
'municipis_ruta'=>'passant per',
'periodicitat'=>'periodicitat',
'vehicle_id'=>'vehicle',
'usuari_id'=>'responsable',
'categoria0'=>'etiquetes1',
'categoria10'=>'etiquetes2',
'tipus'=>'tipus',
'capacitat_pes'=>'total pes',
'pes_disponible'=>'kg pendents',
'capacitat_volum'=>'total volum',
'volum_disponible'=>'litres pendents',
'capacitat_places'=>'total places',
'places_disponibles'=>'places pendents',
'km' => 'km',
'estat'=>'estat',
'descripcio'=>'descripci�',
'acords_explicits'=>'acords expl�cits',
'preu_pes'=>'preu/ut.pes',
'preu_volum'=>'preu/ut.volum',
'preu_places'=>'preu/pla�a',
'preu_combustible'=>'preu<br>combustible',
'pc_ms'=>'% ms',
'propietats'=>'propietats'
);



	$mGuardarTram=array();

	if($opcio=@$_POST['i_opcio'])
	{
		if($opcio=='guardar')
		{
			$guardarTramId=@$_POST['i_guardarTram_id'];
			$mPars['selTramId']=$guardarTramId;
			$campsGuardar=@$_POST['i_campsGuardar'];
			$mCampsGuardar=explode(',',$campsGuardar);
			
			$mGuardarTram['id']=$guardarTramId;
			while(list($key,$val)=each($mCampsGuardar))
			{
				if($val=='acords_explicits')
				{
					$mGuardarTram[$val]=urlencode(@$_POST['i_'.$val]);
				}
				else 
				{
					$mGuardarTram[$val]=@$_POST['i_'.$val];
				}
			}
			//control de dates:
			if(time()>=strtotime($mGuardarTram['sortida']) && $mGuardarTram['categoria0']!='CAC'  && $mGuardarTram['categoria0']!='CAC-personal')
			{
				$missatgeAlerta="<p  class='pAlertaNo3'><i>Atenci�: error en guardar la demanda de transport. No s'ha acceptat la data de sortida</i></p>";
			}
			else if(time()>=strtotime($mGuardarTram['arribada']) && $mGuardarTram['categoria0']!='CAC'  && $mGuardarTram['categoria0']!='CAC-personal')
			{
				$missatgeAlerta="<p  class='pAlertaNo3'><i>Atenci�: error en guardar la demanda de transport. No s'ha acceptat la data d'arribada</i></p>";
			}
			else
			{
				$mMissatgeAlerta=db_guardarTram($mGuardarTram,$db);
				if(!$mMissatgeAlerta['result'])
				{
					$missatgeAlerta="<p  class='pAlertaNo3'><i>".$mMissatgeAlerta['missatge']."</i></p>";
				}
				else
				{
					$missatgeAlerta="<p  class='pAlertaOk3'><i>La demanda de transport s'ha guardat correctament. Recorda activar-la si no ho est�.</i></p>";
					$mTram=db_getTram($db);
					$mTrams=db_getTrams($db);
				}
			}
		}
		else if($opcio=='crear')
		{
			$campsGuardar=@$_POST['i_campsGuardar'];
			$mCampsGuardar=explode(',',$campsGuardar);
			while(list($key,$val)=each($mCampsGuardar))
			{
				if($val=='acords_explicits')
				{
					$mGuardarTram[$val]=urlencode(@$_POST['i_'.$val]);
				}
				else 
				{
					$mGuardarTram[$val]=@$_POST['i_'.$val];
				}
			}
			if(!db_crearTram($mGuardarTram,$db))
			{
				$missatgeAlerta="<p  class='pAlertaNo4'><i>Atenci�: error en generar  la demanda de transport</i></p>";
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk4'><i>La demanda de transport s'ha creat correctament. Recorda activar-la</i></p>";
				$mTrams=db_getTrams($db);
			}
		}
		else if($opcio=='eliminar')
		{
			if(!db_eliminarTram($mPars['selTramId'],$db))
			{
				$missatgeAlerta="<p  class='pAlertaNo4'><i>Atenci�: error en eliminar  la demanda de transport. <br> Es possible que s'hagi contractat</i></p>";
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk4'><i>La demanda de transport s'ha eliminat correctament.</i></p>";
				unset($mPars['selTramId']);
				$mTrams=db_getTrams($db);
			}
		}
		else if($opcio=='anularContractes')
		{
			$mResult=db_anularContractesTram($mPars['selTramId'],$db);
			if(!$mResult['result'])
			{
				$missatgeAlerta="<p  class='pAlertaNo4'><i>Atenci�: error en anular els contractes d'aquesta demanda de transport.</i><br>".$mResult['missatgeAlerta']."</p>";
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk4'><i>S'han eliminat correctament els contractes d'aquesta demanda de transport.</i><br>".$mResult['missatgeAlerta']."</p>";
				$mTrams=db_getTrams($db);
			}
			$mTram=db_getTram($mPars['selTramId'],$db);
		}
		else if($opcio=='repetir')
		{
			$periodeDiaNum=@$_POST['sel_periode_diaNum'];
			$periodeNumMesos=@$_POST['sel_periode_numMesos'];
			$periodeDiaSetmana=@$_POST['sel_periode_diaSetmana'];

			$mMissatgeAlerta=db_repetirTram($periodeDiaNum,$periodeDiaSetmana,$periodeNumMesos,$mPars['selTramId'],$db);
			if(!$mMissatgeAlerta['result'])
			{
				$missatgeAlerta="<p class='pAlertaNo4'><i>Atenci�: error en crear el conjunt de demandes.</i><br>".$mMissatgeAlerta['missatge']."</p>";
			}
			else
			{
				$missatgeAlerta="<p class='pAlertaOk4'><i>S'ha creat correctament el conjunt de demandes.</i></p>";
			}
			$mTrams=db_getTrams($db);
		}
	}
	
$parsChain=makeParsChain($mPars);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_gestioTramsDemandes.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<script type='text/javascript'>
nivell='".$mPars['nivell']."';
mMunicipis=new Array();
caducada=1;
caducada_=0;
categoria0='".$mTram['categoria0']."';

";
$i=0;
while(list($key,$mVal)=each($mMunicipis))
{
	echo "
mMunicipis[".$key."]=\"".(urldecode($mVal['municipi']))."\";
	";
}
reset($mMunicipis);

echo "
</script>

<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('gestioTramsDemandes.php?');
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<th style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			</td>
		</tr>
	</table>

<center><p style='font-size:16px;'>[ Ruta <b>".$mPars['selRutaSufix']."</b>: Gesti� Transport - DEMANDES ]</p></center>
";

$mRutesSufixes=getRutesSufixes($db);
//*v36-4-12-15 call
$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);
if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
{
//*v36-4-12-15 parametres
	mostrarSelectorRuta('0','gestioTramsDemandes.php');
	echo "<center>".$mPars['nivell']."</center>";
}


echo "
<table style='width:80%;' align='center'  bgcolor='".$mColors['table']."'>
	<tr>
		<td style='width:25%;' align='left'>
		<a title='P�gina Serveis de Transport'><img src='imatges/cotxep.gif' ALT=\"p�gina d'usuari\" style='cursor:pointer' onClick=\"javascript: enviarFpars('contractesTrams.php?tip=DEMANDES','_self')\"></a>
		<p>
		Gesti� de <b>DEMANDES</b> de transport
		</p>
		<p>
		Usuari:<b> ".(urldecode($mPars['usuari']))."</b> (".$mPars['nivell'].")
		</p>
		</td>

		<td valign='bottom' style='width:25%;'>
";
if($mPars['contractamentTancat']==1)
{
	echo "
		<center><p style='background-color:#ffaa00;'>- El periode de contractament est� TANCAT - </p><center>
	";
}
else
{
	echo "
		<center><p style='background-color:#ffaa00;'>- El periode de contractament est� OBERT - </p><center>
	";
}
echo "	
		</td>
		<td valign='middle' align='left' style='width:25%;'>
		<p style='text-align:left;'>".$missatgeAlerta."</p>
		</td>
	</tr>
</table>
";


html_menuTrams();

//editar i guardar Tram
if
(
	(
		isset($mPars['selTramId'])
		&&
		$mPars['selTramId']!=''
	)
	||
	$opt=='nou'
)
{
	echo "
<form id='f_guardarTram' name='f_guardarTram' method='post' action='gestioTramsDemandes.php' target='_self' >
<table style='width:80%;' align='center' border='1'  bgcolor='".$mColors['table']."'>
	<tr>
		<td  style='width:49%;' valign='top' >
		<table style='width:100%;' >
	";
	$campsGuardar='';
	$mUnitatsContractades=array();
	$mUnitatsContractades['pes']=0;
	$mUnitatsContractades['volum']=0;
	$mUnitatsContractades['places']=0;


	$mUnitatsContractades=getUnitatsContractadesTram($db);

	$mSortida=array();
	
	$date = new DateTime();
	$t1=$date->getTimestamp();
	$mSortida['Y']=date('Y',strtotime($mTram['sortida']));
	$mSortida['m']=date('m',strtotime($mTram['sortida']));
	$mSortida['d']=date('d',strtotime($mTram['sortida']));
	$mSortida['h']=date('H',strtotime($mTram['sortida']));
	$mSortida['i']=date('i',strtotime($mTram['sortida']));
		
	$instantSortida = new DateTime();
	$instantSortida->setDate($mSortida['Y'], $mSortida['m'], $mSortida['d'] );
	$instantSortida->setTime($mSortida['h'], $mSortida['i'], 0 );
	$t2=$instantSortida->getTimestamp();
	$mTram['caducada']=0;
	if(@$mPars['selTramId']!='' && $mTram['usuari_id']==$mPars['usuari_id'])	
	{
		echo "
		<tr>
			<td id='td_alertaJs'>
			</td>

			<td align='right'>
			<table style='width:100%;' bgcolor='#9CE6E3'>
				<tr>
					<td style='width:70%;'>
					<p>[".$mTram['id']."] sortida: ".(date('H:i d-m-Y',strtotime($mTram['sortida'])))."<br>origen: ".(urldecode($mMunicipis[$mTram['municipi_origen']]['municipi']))."<br>desti: ".(urldecode($mMunicipis[$mTram['municipi_desti']]['municipi']))."</p>			
					</td>
					
					<td style='width:30%;' valign='top'>
		";
		if($opt!='nou')
		{
			if($mTram['actiu']==1 && ($t2-$t1)>0)
			{
				echo " <p style='color:#00aa00;'>ACTIVA</p>";
			} 	
			else if($mTram['actiu']==0)
			{
				echo " <p style='color:red;'>INACTIVA</p>";
			} 
			else if(($t2-$t1)<=0)
			{
				echo " <p style='color:red;'>CADUCADA</p>";
				$mTram['caducada']=1;
			} 
			if($mTram['categoria0']=='CAC'  || $mTram['categoria0']=='CAC-personal')
			{
				echo " <p style='background-color:orange; color:white; text-align:center;'>".$mTram['categoria0']."</p>";
			}

			if($mUnitatsContractades['total']>0)
			{
				echo "
						<p class='p_micro2' style='color:red;'><b>-contractada-<br>(no editable)</b></p>	
				";
			}
			else
			{
				echo "
						<p class='p_micro2' style='color:green;'><b>-no contractada-<br>(editable)</b></p>	
				";
			}
		}
		echo "
					</td>
				</tr>
			</table>	
			</td>
		</tr>
	";
	}

	while (list($key,$val)=each($mNomsColumnes3))
	{
		$val=$mTram[$key];
		
		$inputSize='50';
		$mUnitatsContractadesText['capacitat_pes']="pes contractat: <b>".$mUnitatsContractades['pes']."</b> (kg)";
		$mUnitatsContractadesText['capacitat_volum']="volum contractat: <b>".$mUnitatsContractades['volum']."</b> (litres)";	
		$mUnitatsContractadesText['capacitat_places']="places contractades: <b>".$mUnitatsContractades['places']."</b> (persones)";

		if(in_array($key,$mColsProductes3))
		{
			if($key=='sortida' || $key=='arribada')
			{
				html_introduirInstant($key);
			}
			else if ($key=='capacitat_pes' || $key=='capacitat_volum' || $key=='capacitat_places' )
			{
				echo "
			<tr>
				";
				if($key=='capacitat_pes'){$utsCapacitat='kg';$inputSize='5';}
				if($key=='capacitat_volum'){$utsCapacitat='litres';$inputSize='5';}
				if($key=='capacitat_places'){$utsCapacitat='persones';$inputSize='5';}
			
				if
				(
					(
						(
							$mTram['caducada']==0 
							&& 
							$mUnitatsContractades['total']==0
						)
						||
						(
							$mTram['caducada']==1
						)
					)						
					&& 
					(
						$mPars['nivell']=='sadmin'
						||
						(
							$mPars['selRutaSufix']==date('ym')
							&&
							$mPars['usuari_id']==$mTram['usuari_id']
						)
					)
				)
				{
					echo "
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				
				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'>
				<input type='text' id='i_".$key."' name='i_".$key."' size='".$inputSize."' 
				onChange=\"javascript:recordatoriGuardar();\" value=\"".(urldecode($val))."\">
					"; 
					if($mPars['nivell']=='sadmin'){	echo " [sadmin]";} 
					echo "
				<br>".$mUnitatsContractadesText[$key]."</p>";
					echo "
				</td>
					";
				}
				else
				{
					echo "
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				
				<td style='width:66%;' align='left'  valign='top'>
				<p class='nota'>
				<input type='text'  id='i_".$key."' name='i_".$key."' READONLY  style='background-color:#eeeeee;' 
				size='".$inputSize."' value=\"".(urldecode($val))."\">
				".$mUnitatsContractadesText[$key]."</p>
				</td>
					";
				}
				$campsGuardar.=','.$key;
				echo "
			</tr>
				";
			}
			else if($key=='actiu')
			{
				if($opt!='nou')
				{
					$selected1='';
					$selected2='selected';
			
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='middle'>
				<select 
					";
					if($mPars['nivell']=='sadmin' || ($mPars['selRutaSufix']==date('ym')  && $mPars['usuari_id']==$mTram['usuari_id']))
					{
						if( $mTram[$key]==1 && $mUnitatsContractades['total']>0)
						{
							echo " DISABLED style='background-color:#eeeeee;' ";
						}
						else 
						{
							echo " name='i_".$key."'  onChange=\"javascript:recordatoriGuardar();\" ";
							$campsGuardar.=','.$key;
						}
					}

					echo "
					id='i_".$key."' size='1'>
					";
					for($i=0;$i<2;$i++)
					{
						if($mTram['actiu']==$i)
						{
							$selected1='selected';$selected2='';}else{$selected1='';
						}
						echo "
				<option ".$selected1."  value=\"".$i."\">".$mActiu[$i]."</option>
						";
					}
					echo "
				</select>
					";
				
					if($mTram['actiu']=='1')
					{
						echo "
				<img src='imatges/okp.gif'>
						";
					}
					else
					if($mTram['actiu']=='0')
					{
						echo "
				<img src='imatges/nop.gif'>
						";
					}
					echo "
				</td>
			</tr>
					";
				}
			}
			else if(in_array($key,array('categoria0','categoria10','municipi_origen','municipi_desti'))) //selects gen�ric
			{
				if($key=='categoria0')
				{
					$mOpcions=$mCategoria0[$mPars['nivell']]; 
					$nota='';
					$selectSize=(count($mOpcions)+1);
				}
				else if($key=='categoria10')
				{
					$mOpcions=$mCategoria10[$mPars['nivell']]; $nota='';$selectSize=(count($mOpcions)+1);
					$nota='';
					$selectSize=(count($mOpcions)+1);
				}
				else if($key=='municipi_origen' || $key=='municipi_desti')
				{
					$mOpcions=$mMunicipis; $nota='';$selectSize=5;
				}
				else 
				{
					$mOpcions=array();
					$nota='';
				}
			
				echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				";
				if
				(
					(
						(
							$mTram['caducada']==0 
							&& 
							$mUnitatsContractades['total']==0
						)
						||
						(
							$mTram['caducada']==1
						)
					)						
					&&
					(
						$mPars['nivell']=='sadmin' 
						|| 
						(
							$mPars['selRutaSufix']==date('ym')
							&&
							$mPars['usuari_id']==$mTram['usuari_id']
						)
					)
				)
				{
					echo "
				<p>
				<select  size='".$selectSize."' id='i_".$key."' name='i_".$key."' onChange=\"javascript:recordatoriGuardar();\">
					";
					$selected='';
					$selected2='selected';
					while(list($key2,$val2)=each($mOpcions))
					{
						$val2_=$val2;
						$val3_=$val2;
				
						if($key=='municipi_origen' || $key=='municipi_desti')
						{
							$val2_=$key2;$val3_=urldecode($mMunicipis[$key2]['municipi']);
							if($val2_==$val){$selected='selected';$selected2='';}else{$selected='';}
						}
						else
						{				
							if($val2_==urldecode($val)){$selected='selected';$selected2='';}else{$selected='';}
						}
						echo "
				<option ".$selected." value='".$val2_."'>".$val3_."</option>					
						";
					}
					reset($mOpcions);
					echo "
				<option ".$selected2." value=''></option>					
				</select>
				".$nota;
					if($mPars['nivell']=='sadmin'){echo "[sadmin]";}
					echo "
				</p>
					";
				}
				else
				{
					if($key=='municipi_origen' || $key=='municipi_desti')
					{
						$val2_=urldecode($mMunicipis[$mTram[$key]]['municipi']);
					}
					else
					{
						$val2_=$mTram[$key];
					}
					
					echo "
				<p>".$val2_."</p>
				<input type='hidden' id='i_".$key."' name='i_".$key."' size='30'  value=\"".$mTram[$key]."\">
					";
				}
					$campsGuardar.=','.$key;

				echo "
				</td>
			</tr>
				";
			}
			else if($key=='preu_pes' || $key=='preu_volum' || $key=='preu_places' || $key=='pc_ms' )
			{
				$mPreuText['preu_pes']="ums/kg ";
				$mPreuText['preu_volum']="ums/l ";
				$mPreuText['preu_places']="ums/pla�a ";
				$mPreuText['pc_ms']="%";
				
				$inputSize='5';
				if
				(
					(
						(
							$mTram['caducada']==0 
							&& 
							$mUnitatsContractades['total']==0
						)
						||
						(
							$mTram['caducada']==1
						)
					)						
					&&
					(
						$mPars['nivell']=='sadmin' 
						|| 
						(
							$mPars['selRutaSufix']==date('ym')
							&&
							$mPars['usuari_id']==$mTram['usuari_id']
						)
					)
				)
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<p><input type='text' id='i_".$key."' name='i_".$key."' size='".$inputSize."'  onChange=\"javascript:recordatoriGuardar();\"  value=\"".(urldecode($val))."\"> ".$mPreuText[$key]." [sadmin]</p>
				</td>
			</tr>
					";
				}
				else
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<input type='text' READONLY style='background-color:#eeeeee;' size='".$inputSize."' id='i_".$key."' name='i_".$key."' value=\"".(urldecode($val))."\"> ".$mPreuText[$key]."
				</td>
			</tr>
					";
				}
					$campsGuardar.=','.$key;
			}
			else if($key=='preu_combustible')
			{
				$inputSize='5';
				if
				(
					(
						(
							$mTram['caducada']==0 
							&& 
							$mUnitatsContractades['total']==0
						)
						||
						(
							$mTram['caducada']==1
						)
					)						
					&& 
					(
						$mPars['nivell']=='sadmin'
						||
						(
							$mPars['selRutaSufix']==date('ym')
							&&
							$mPars['usuari_id']==$mTram['usuari_id']
						)
					)
				)
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<p><input type='text' id='i_".$key."' name='i_".$key."'  onChange=\"javascript:recordatoriGuardar();\"  size='".$inputSize."' value=\"".(urldecode($val))."\"> ums/l.</p>
				</td>
			</tr>
				";
				}
				else
				{
					echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p class='nota'>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<p><input type='text' readonly  style='background-color:#dddddd;' id='i_".$key."' name='i_".$key."' size='".$inputSize."' value=\"".(urldecode($val))."\"> ums/l.</p>
				</td>
			</tr>
				";
				}
				$campsGuardar.=','.$key;
			}
			else if($key=='km')
			{
				$inputSize='5';
				if
				(
					(
						(
							$mTram['caducada']==0 
							&& 
							$mUnitatsContractades['total']==0
						)
						||
						(
							$mTram['caducada']==1
						)
					)						
					&&
					(
						$mPars['nivell']=='sadmin' 
						|| 
						(
							$mPars['selRutaSufix']==date('ym')
							&&
							$mPars['usuari_id']==$mTram['usuari_id']
						)
					)
				)
				{
					$readonly='';
					$onChange=" onChange=\"javascript:recordatoriGuardar();\" ";
				}
				else
				{
					$readonly="readonly  style='background-color:#dddddd;'";
					$onChange="";
				}
					$campsGuardar.=','.$key;
				
				echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<input type='text' ".$readonly." size='".$inputSize."' ".$onChange." id='i_".$key."' name='i_".$key."' value=\"".$val."\">
				</td>
			</tr>
				";
			}
			else if($key=='pes_disponible' || $key=='volum_disponible' || $key=='places_disponibles')
			{
				$inputSize='5';
				echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<input type='text' readonly style='background-color:#dddddd;' size='".$inputSize."' id='i_".$key."' value=\"".$val."\">
				</td>
			</tr>
				";
			}
			else if($key=='municipis_ruta')
			{
	
				echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				";
				if
				(
					(
						(
							$mTram['caducada']==0 
							&& 
							$mUnitatsContractades['total']==0
						)
						||
						(
							$mTram['caducada']==1
						)
					)						
					&& 
					(
						$mPars['nivell']=='sadmin'
						||
						(
							$mPars['selRutaSufix']==date('ym')
							&&
							$mPars['usuari_id']==$mTram['usuari_id']
						)
					)
				)
				{
					$disabled='';
					$onChange=" onChange=\"javascript:recordatoriGuardar();\" ";
					$onClickAtrassar=" onClick=\"javascript:municipisRuta('atrassar',".$i.");\" ";
					$onClickAvansar=" onClick=\"javascript:municipisRuta('avan�ar',".$i.");\" ";
					$visibilityBotons="inherit";
				}
				else
				{
					$disabled='DISABLED';
					$onChange="";
					$onClickAtrassar="";
					$onClickAvansar="";
					$visibilityBotons="hidden";
				}
					$campsGuardar.=','.$key;
				
				$nota='* seleccionar i afegir/treure municipi intermig de ruta';

				$val=str_replace(',,',',',$val);
				$val=trim($val,',');
				$mMunicipisRuta=explode(',',$val);
								
				echo "
				<p>

				<table id='t_municipis_ruta'>
				";
				for($i=0;$i<count($mMunicipisRuta);$i++)
				{
					if($mMunicipisRuta[$i]!='')
					{
						echo "
					<tr>
						<td align='left'>
						<a title=\"situar en una posici� anterior\"><img src='imatges/upp.gif' style='cursor:pointer;' ".$onClickAtrassar."></a>
						</td>
						<td align='left'>
						<p style='color:blue;'> ".(urldecode($mMunicipis[$mMunicipisRuta[$i]]['municipi']))."</p>
						</td>
						<td align='left'>
						<a title=\"situar en una posici� posterior\"><img src='imatges/downp.gif' style='cursor:pointer;' ".$onClickAvansar."></a>
						</td>
					</tr>
						";
					}
				}
				echo "
				</table>
				<br>
				<select ".$disabled." size='5' id='sel_municipi_ruta' ".$onChange." name='sel_municipi_ruta'>
				";
				$selected='';
				$selected2='selected';
				while(list($key2,$val2)=each($mMunicipis))
				{
					if($val2==$val){$selected='selected';$selected2='';}else{$selected='';}

					
					echo "
				<option ".$selected."  value='".$key2."'>".(urldecode($mMunicipis[$key2]['municipi']))."</option>					
					";
				}
				reset($mMunicipis);
				echo "
				<option ".$selected2." value=''></option>					
				</select>
				
				<table>
					<tr>
						<td>
						<input type='button' onClick=\"javascript:municipisRuta('afegir','');\" value=\"afegir\" style='visibility:".$visibilityBotons.";'>
						</td>
						<td>
						<input type='button' onClick=\"javascript:municipisRuta('treure','');\" value=\"treure\"  style='visibility:".$visibilityBotons.";'>
						</td>
					</tr>
				</table>
				
				<input type='hidden' id='i_".$key."' name='i_".$key."'  value=\"".$val."\">
				".$nota;
				if($mPars['nivell']=='sadmin'){echo "[sadmin]";}
				echo "
				</p>
				</td>
			</tr>
				";
			}
			else
			{
				$inputSize='5';
				echo "
			<tr>
				<th style='width:33%;' align='left' valign='top'>
				<p>".$mNomsColumnes3[$key]."</p>
				</th>
				<td style='width:66%;' align='left'  valign='top'>
				<input type='text' readonly style='background-color:#dddddd;' size='".$inputSize."' id='i_".$key."' value=\"".$val."\">
				</td>
			</tr>
				";
			}
		}
	}
	reset($mNomsColumnes3);
	echo "
			<tr>
				<th style='width:33%;' align='left' valign='bottom'>
				</th>
				<td style='width:66%;' align='left'  valign='bottom'>
	";
	if($mPars['nivell']=='sadmin')
	{
		echo "
				<input type='button' onClick=\"javascript: anularContractes('".$mPars['selTramId']."');\" value='anul.lar contractes'>	[admin]			
		";
	}
	echo "
				</td>
			</tr>
			<tr>
				<td style='background-color:#eeeeee;' align='left' valign='bottom'>
				<p class='p_micro'>forma c�lcul de <b>l'import del transport</b> demanat:<p>
				</td>
				<td style='background-color:#eeeeee;' align='left' valign='bottom'>
				<p class='p_micro'><i>import_transport=pes_reservat*preu_pes+ volum_reservat*preu_volum+ places_reservades*preu_pla�a</i></p>
				</td>
			</tr>
	";
	$ingressosT=$mTram['capacitat_pes']*$mTram['preu_pes']+$mTram['capacitat_volum']*$mTram['preu_volum']+$mTram['capacitat_places']*$mTram['preu_places'];
	$ingressosR=$mUnitatsContractades['pes']*$mTram['preu_pes']+$mUnitatsContractades['volum']*$mTram['preu_volum']+$mUnitatsContractades['places']*$mTram['preu_places'];
	$despeses=$mTram['km']*$mVehicle['consum']/100*$mTram['preu_combustible'];
	echo "
			<tr>
				<td style='background-color:#eeeeee;' align='left' valign='bottom'>
				<p class='p_micro'><b>import del transport demanat</b> segons preus guardats i tots els recursos demanats contractats:<p>
				</td>
				<td style='background-color:#eeeeee;' align='left' valign='bottom'>
				<p class='p_micro'>import=".$ingressosT." ums, ".$mTram['pc_ms']."% (".(number_format(($ingressosT*$mTram['pc_ms']/100),2))." ecos, ".(number_format(($ingressosT*(100-$mTram['pc_ms'])/100),2))." euros)</p>
				</td>
			</tr>
			<tr>
				<td style='background-color:#eeeeee;' align='left' valign='bottom'>
				<p class='p_micro'><b>import del transport</b> segons preus guardats i els recursos actuals demanats que s'han contractat(incloent els contractes realitzats i els pendents d'acceptar):<p>
				</td>
				<td style='background-color:#eeeeee;' align='left' valign='bottom'>
				<p class='p_micro'>".$ingressosR." ums, ".$mTram['pc_ms']."% (".(number_format(($ingressosR*$mTram['pc_ms']/100),2))." ecos, ".(number_format(($ingressosR*(100-$mTram['pc_ms'])/100),2))." euros)</p>
				</td>
			</tr>
			<tr>
				<td style='background-color:#eeeeee;' align='left' valign='bottom'>
				<p class='p_micro'><b>cost del transport</b> segons el consum del vehicle i el preu combustible:<p>
				</td>
				<td style='background-color:#eeeeee;' align='left' valign='bottom'>
				<p class='p_micro'>".(number_format($despeses,2))." (".$mVehicle['consum']." l./100km);
				</td>
			</tr>
		</table>
	";		
	if($mPars['nivell']=='sadmin')
	{
		html_mostrarPropietatsTram($db);
	}

	echo "
		</td>
		<td style='width:50%;' valign='top' align='center'>
		";

	if
	(
					(
						(
							$mTram['caducada']==0 
							&& 
							$mUnitatsContractades['total']==0
						)
						||
						(
							$mTram['caducada']==1
						)
					)						
					&&
		(
			$mPars['nivell']=='sadmin' 
			|| 
			(
				$mPars['selRutaSufix']==date('ym')
				&&
				$mPars['usuari_id']==$mTram['usuari_id']
			)
		)
	)
	{
		echo "
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�:</b></p>
		<center><p class='p_micro3'>( * ha d'incloure tots els acords expl�cits del contracte )</p></center>
		<div style='width:100%; height:250px; overflow-y:scroll; overflow-x:hidden;'>
		<table style='width:100%;'>
			<tr>
				<td style='width:100%;'>
				<p>".(urldecode($mTram['acords_explicits']))."<br><br></p>
				</td>
			</tr>
		</table>
		</div>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�: (HTML)</b></p>
		<textArea cols='60' rows='20' type='text' id='i_acords_explicits' name='i_acords_explicits'  onChange=\"javascript:recordatoriGuardar();\"  >".(urldecode($mTram['acords_explicits']))."</textArea>[sadmin]
		";
	}
	else
	{
		echo "
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�:</b></p>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* ha d'incloure tots els acords expl�cits del contracte</p>
		<div style='width:100%; height:250px; overflow-y:scroll; overflow-x:hidden;'>
		<table style='width:100%;'>
			<tr>
				<td style='width:100%;'>
				<p>".(urldecode($mTram['acords_explicits']))."</p>
				</td>
			</tr>
		</table>
		</div>
		<p style='text-align:left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descripci�: (HTML)</b></p>
		<textArea cols='60' rows='20' READONLY style='background-color:#eeeeee;' type='text' id='i_acords_explicits' name='i_acords_explicits'>".(urldecode($mTram['acords_explicits']))."</textArea>
		";
	}
		$campsGuardar.=',acords_explicits';
 	html_notesHTML();
 	echo "
		<br>
		<br>
		<table style='width:80%;' align='center'>
			<tr>
				<td style='width:100%;' >
				<p style='text-align:justify;'>
				<b>Acords impl�cits a l'�s del servei:</b>
				<br>
				<br>
				<i>".$mTransportText['acords_implicits']."</i>
				</p>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align='center'>
	";
	
	if
	(
			$mPars['nivell']=='sadmin'
			||
			(
				$mPars['selRutaSufix']==date('ym')
				&&
				$mPars['usuari_id']==$mTram['usuari_id']
			)
	)
	{
		if($opt=='vell')
		{
			if($mUnitatsContractades['total']==0)
			{
				echo "
		<br>
		<input type='button' onClick=\"javascript:caducada_=".$mTram['caducada']."; if(checkFormGuardarTrams()){guardarTram();}\" value='guardar demanda de transport'>
		<br>
				";
			}
			echo "
		<br>
		<a title=\"En guardar la demanda actual com a nova demanda es canviar� l'identificador, i la demanda de partida no es modificar�\"><input type='button' onClick=\"javascript: if(checkFormGuardarTrams()){crearTram();}\" value='guardar com a nova demanda de transport'></a>
		<br>
			";
		}
	}

	if($opt=='nou')
	{
		echo "
		<br>
		<input type='button' onClick=\"javascript: if(checkFormGuardarTrams()){crearTram();}\" value='crear la nova demanda de transport'>
		<br>
			";		
	}

	if($opt=='vell' && $mUnitatsContractades['total']==0)
	{
		echo "
		<br>
		<a title=\"Aquesta demanda de transport nom�s es pot eliminar si no ha estat contractada\"><input type='button' onClick=\"javascript: eliminarTram('".$mPars['selTramId']."');\" value='eliminar demanda de transport'></a>
		<br>
		";
	}
	$campsGuardar.=',tipus';
	$campsGuardar=substr($campsGuardar,1);
	echo "
		<input type='hidden' id='i_guardarTram_id' name='i_guardarTram_id' value=\"".$mPars['selTramId']."\">
		<input type='hidden' id='i_opcio' name='i_opcio' value='guardar'>
		<input type='hidden' name='i_pars' value='".$parsChain."'>
		<input type='hidden' name='i_campsGuardar' value='".$campsGuardar."'>
		<input type='hidden' id='i_unitatsContractadesPes' value='".$mUnitatsContractades['pes']."'>
		<input type='hidden' id='i_unitatsContractadesVolum' value='".$mUnitatsContractades['volum']."'>
		<input type='hidden' id='i_unitatsContractadesPlaces' value='".$mUnitatsContractades['places']."'>
		<input type='hidden' name='i_tipus' value='".$mPars['vTipus']."'>
		<br><br>
</form>
		</td>
		<td>
		";
			//-------------------------------------------------------------------
			if(@$mPars['selTramId']!='' && $mTram['usuari_id']==$mPars['usuari_id'])	
			{
				echo "
			<table width='100%;'border='0' bgcolor='#9CE6E3'>
				<tr>
					<td width='1000%' valign='top' align='center'>
					<br>
			
					<form id='f_crearConjuntDemandes' name='f_crearConjuntDemandes' method='POST' target='_self' action='gestioTramsDemandes.php?opt=repetir'>
					<p>Crear un conjunt de DEMANDES peri�diques a partir de l'actual:</p>
					<table width='100%' >
						<tr>
							<td width='20%' valign='top' align='middle'>
							<p > cada dia/es </p>
							</td>
							
							<td width='10%' valign='top'  align='middle'>
							<select id='sel_periode_diaNum' name='sel_periode_diaNum[]' multiple onChange=\"javascript: compatibilitatSelDia('sel_periode_diaNum');\">
				";
				for ($i=1;$i<=31;$i++)
				{
					echo "
							<option value='".$i."'>".$i."</option>
					";
				}
				echo "
							<option selected value=''></option>
							</select>
							</td>

							<td width='15%' valign='top' align='middle'>
							<p> o cada 
							</td>

							<td width='15%' valign='top'  align='middle'>
							<select id='sel_periode_diaSetmana' name='sel_periode_diaSetmana[]' multiple onChange=\"javascript: compatibilitatSelDia('sel_periode_diaSetmana');\">
				";
				while(list($key,$diaSetmana)=each($mDies))
				{
					echo "
							<option value='".$key."'>".$diaSetmana."</option>
					";
				}
				echo "
							<option selected value=''></option>
							</select>
							</p>
							</td>
								
							<td width='15%' valign='top' align='middle'>
							<p> durant </p>
							</td>

							<td width='15%' valign='top'  align='middle'>
							<select id='sel_periode_numMesos' name='sel_periode_numMesos'>
				";
				for ($i=1;$i<=12;$i++)
				{
					echo "
							<option value='".$i."'>".$i."</option>
					";
				}
				echo "
							<option selected value=''></option>
							</select>
							</td>

							<td width='15%' valign='top' align='middle'>
							<p>mesos</p> <p class='p_micro2'>(incloent l'actual, i a partir d'ara <b>".(date('H:i:00 d/m/Y'))."</b>)</p>
							</td>
						</tr>
					</table>
					<br>
					<input type='hidden' id='i_pars2' name='i_pars' value='".$parsChain."'>
					<input type='hidden' name='i_opcio' value='repetir'>
					<input type='button' onClick=\"
					javascript: if(checkFormCrearConjuntDemandes())
					{
						document.getElementById('f_crearConjuntDemandes').submit();
					}
					\" value=\"crear el conjunt de demandes\">
					</form>
					<br>
					<br>
					<p class='p_micro'>* Les demandes que es crearan es guardaran com a inactives.</p>
					</td>
				</tr>		
			</table>
				";
			}
			echo "
		</td>
	</tr>
</table>
<br>
	";
}
html_notesApropietarisTramsDemandes();
html_helpRecipient();

echo "
<br><br><br><br>&nbsp;
<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='gestioTramsDemandes.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</div>


</body>
</html>
";

?>




		