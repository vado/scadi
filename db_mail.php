<?php
function crearObjectePhpMailer()
{
 	global $mParams,$mPars;
	 
 	$mail             = new PHPMailer(true);
	if($mParams['mailNotificacionsActives']==1)
	{ 
		//$body             = file_get_contents('contents.html');
		//$body             = eregi_replace("[\]",'',$body);
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = $mParams['mailHost']; // SMTP server
			$mail->SMTPAuth	  = true;
			$mail->AuthType	  = true;
			$mail->Port		  = $mParams['mailPort'];
		$mail->Username   = $mParams['mailAutoUser']; // SMTP account username
		$mail->Password   = $mParams['mailAutoPassword'];        // SMTP account password
		$mail->From   = $mParams['mailAutoUser']; // SMTP account username
		$mail->SMTPSecure   = $mParams['mailSec'];
	                     // enables SMTP debug information (for testing)
		// 1 = errors and messages
		// 2 = messages only
		
			//if($mPars['nivell']=='sadmin'){$mail->SMTPDebug  = 1;	}
            	      // enable SMTP authentication
		//$mail->Host       = $mParams['mailHost']; // sets the SMTP server
		//$mail->Port       = $mParams['mailPort'];                    // set the SMTP port for the GMAIL server
			$mail->IsHTML(true);
		$mail->Timeout=30;
	}
	
	return $mail;


}


//------------------------------------------------------------------------------
// GESTIO USUARIS
//------------------------------------------------------------------------------
function mail_enviarMissatgeUsuaris($missatgeUsuaris,$subjecte2,$cadenaUsuaris,$db)
{
	global $mPars,$mParams,$mParametres,$mUsuarisRef,$mGrupsRef;

	$mMissatgeAlerta=array();

	if($mParams['mailNotificacionsActives']==1)
	{
	
	$mCadenaUsuaris=explode(',',$cadenaUsuaris);

	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
	{
		if(!array_key_exists($mPars['usuari_id'],$mUsuarisRef))
		{
			$mUsuarisRef[$mPars['usuari_id']]=db_getUsuari($mPars['usuari_id'],$db);
		}
	}
	$missatge='';
	while(list($index,$id)=each($mCadenaUsuaris))
	{
		if(isset($mUsuarisRef[$id]))
		{
			$mPars['grups_usuari']=$mUsuarisRef[$id]['grups'];
			$mGrupsUsuari=db_getGrupsUsuari($db);
			$mPars['perfils_productor']=$mUsuarisRef[$id]['perfils_productor'];
			$mPerfilsUsuari=db_getPerfilsUsuari($db);
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
			{
				$subjecte="CAC(GC) -".(urldecode($subjecte2));
				
				$missatge1="
				Hola ".(urldecode($mUsuarisRef[$id]['usuari'])).",<br>
				<br>
				l'administrador del gestor de comandes de la CAC t'envia el seg�ent missatge:
				<br>
				";
			}
			else
			{
				$subjecte="CAC(GC) -".(urldecode($mGrupsRef[$mPars['grup_id']]['nom']))." (Resp. Grup): ".(urldecode($subjecte2));
				
				$missatge1="
				Hola ".(urldecode($mUsuarisRef[$id]['usuari'])).",<br>
				<br>
				el responsable del teu grup al gestor de comandes de la CAC t'envia el seg�ent missatge:
				<br>
				";
			}
	
			
			$missatge="
			<html>
			<head>
			<title></title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				<tr>
					<td width='100%'>
					<br>
					<br>
					<p>".(urldecode($missatgeUsuaris))."</p>
					</td>
				</tr>
				<tr>
					<td width='100%'>
					<br><br>
					<p><i>Gestor comandes CAC: http://cac.cooperativa.cat 
					<br>
					<br>
					
					Informaci� sobre el teu usuari:
					<br>
					<br>
					Membre dels Grups:
					<br>
			";
			while(list($grupId,$mGrupUsuari)=each($mGrupsUsuari))
			{
				$esResponsableGrup='';
				if($id==@$mGrupsRef[$grupId]['usuari_id'])
				{
					$esResponsableGrup='&nbsp;[Responsable de Grup]';
				}
				else
				{
					$esResponsableGrup='&nbsp;[usuari]';
				}
				$missatge.=" - ".(urldecode($mGrupUsuari['nom']))." ".$esResponsableGrup."<br>";
			}
			reset($mGrupsUsuari);

			$missatge.="
					<br><br>
					Perfils de Productor:
					<br>
			";
			if(count($mPerfilsUsuari)>0)
			{
				while(list($perfilId,$mPerfilUsuari)=each($mPerfilsUsuari))
				{
					$esResponsablePerfil='';
					if($id==@$mPerfilUsuari['usuari_id'])
					{
						$esResponsablePerfil='[Responsable del Perfil]';
					}
					else
					{
						$esResponsablePerfil='[usuari]';
					}
					$missatge.=" - ".(urldecode($mPerfilUsuari['projecte']))." ".$esResponsablePerfil."<br>";
				}
			}
			else
			{
				$missatge.=" ( cap perfil de productor associat ) <br>";
			}

			$missatge.="
					</i>
					</p>
					</td>
				</tr>
			</table>
			</body>
			";
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Sender	  = $mParams['mailAutoReplyToAddress'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->Username   = $mParams['mailAutoUser'];
			$mail->Password   = $mParams['mailAutoPassword'];
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$id]['email'], $mUsuarisRef[$id]['usuari']);
//*v36 8-1-16 1 condicio
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
			{
				$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			}
			else //resp grup
			{
				$mail->AddReplyTo($mUsuarisRef[$mPars['usuari_id']]['email'],$mUsuarisRef[$mPars['usuari_id']]['usuari']);
			}
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
		//if($mPars['nivell']=='sadmin'){vd($mail);}

//*v36-4-12-15 treure 1 condicional
				if($mail->Send())
				{
					$mMissatgeAlerta[$id]['result']='ok';
					$mMissatgeAlerta[$id]['email']=$mUsuarisRef[$id]['email'];
				}
				else
				{
					$mMissatgeAlerta[$id]['result']='ha fallat - Error:'. $mail->ErrorInfo;;
					$mMissatgeAlerta[$id]['email']=$mUsuarisRef[$id]['email'];
				}
				
		}
	}
	reset($mCadenaUsuaris);
	$mail=crearObjectePhpMailer();
	$mail->From		  = $mParams['mailAutoUser'];
	$mail->FromName	  = $mParams['mailAutoName'];
	$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
	$mail->MsgHTML($missatge);
	$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
//*v36 8-1-16 1 condicio
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
	{
		$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
	}
	else //resp grup
	{
		$mail->AddReplyTo($mUsuarisRef[$mPars['usuari_id']]['email'],$mUsuarisRef[$mPars['usuari_id']]['usuari']);
	}

	$mail->Send();
		//if($mPars['nivell']=='sadmin'){vd($mail);}
	
	}	
	
	return $mMissatgeAlerta;
}


//------------------------------------------------------------------------
function mail_enviarAccesAusuaris($cadenaUsuaris,$db)
{
	global $mPars,$mParams,$mParametres,$mUsuarisRef,$mGrupsRef;

	$mMissatgeAlerta=array();
	
	if($mParams['mailNotificacionsActives']==1)
	{

	$mCadenaUsuaris=explode(',',$cadenaUsuaris);

	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
	{
		if(!array_key_exists($mPars['usuari_id'],$mUsuarisRef))
		{
			$mUsuarisRef[$mPars['usuari_id']]=db_getUsuari($mPars['usuari_id'],$db);
		}
	}
	
	
	$missatge='';
	while(list($index,$id)=each($mCadenaUsuaris))
	{
		if(isset($mUsuarisRef[$id]))
		{
			$mPars['grups_usuari']=$mUsuarisRef[$id]['grups'];
			$mGrupsUsuari=db_getGrupsUsuari($db);
			$mPars['perfils_productor']=$mUsuarisRef[$id]['perfils_productor'];
			$mPerfilsUsuari=db_getPerfilsUsuari($db);

			$subjecte='CAC(GC) - dades usuari (SCADI)';
			$missatge1="
	Hola ".(urldecode($mUsuarisRef[$id]['usuari'])).",<br>
	<br>
	en/la ".$mUsuarisRef[$mPars['usuari_id']]['usuari']." (".$mPars['nivell'].", id:".$mPars['id'].") t'envia les dades d'acc�s al teu usuari a l'SCADI:
	<br>
			";

			$missatge="
			<html>
			<head>
			<title>CAC(GC) - Dades d'acc�s</title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				<tr>
					<td width='100%'>
					<br>
					<p>http://cac.cooperativa.cat<br><br>
					Usuari:<b>".(urldecode($mUsuarisRef[$id]['usuari']))."</b><br>
					Contrasenya:<b>".$mUsuarisRef[$id]['contrassenya']."</b><br>
					Nivell:<b>".$mUsuarisRef[$id]['nivell']."</b>
					<br><br><br>
					Membre dels Grups:<br>
					";
					while(list($grupId,$mGrupUsuari)=each($mGrupsUsuari))
					{
						$esResponsableGrup='';
						if($id==@$mGrupsRef[$grupId]['usuari_id']){$esResponsableGrup='&nbsp;[Responsable de Grup]';}else{$esResponsableGrup='&nbsp;[usuari]';}
						$missatge.=" - ".(urldecode($mGrupUsuari['nom']))." ".$esResponsableGrup."<br>";
					
					}
					reset($mGrupsUsuari);

					$missatge.="
					<br><br>
					Perfils de Productor:<br>
					";
					if(count($mPerfilsUsuari)>0)
					{
						while(list($perfilId,$mPerfilUsuari)=each($mPerfilsUsuari))
						{
							$esResponsablePerfil='';
							if($id==@$mPerfilUsuari['usuari_id']){$esResponsablePerfil='[Responsable del Perfil]';}else{$esResponsablePerfil='[usuari]';}
							$missatge.=" - ".(urldecode($mPerfilUsuari['projecte']))." ".$esResponsablePerfil."<br>";
						}
						reset($mPerfilsUsuari);
					}
					else
					{
						$missatge.=" ( cap perfil de productor associat ) <br>";
					}

					$missatge.="
					</p>
					</td>
				</tr>
				<tr>
					<td>
					<br>
					<p>Recorda canviar la contrasenya si es la primera vegada que reps aquest acc�s, o fer-ho de tant en tant. Ho pots fer editant el perfil d'usuari, una vegada accedeixis al gestor amb la contrasenya actual.
					<br>
					<br>
					Salut!
					</p>
					</td>
				</tr>
			</table>
			</body>
			";
			
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$id]['email'], $mUsuarisRef[$id]['usuari']);
			$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

			
			if(!$mail->Send())
			{
				$mMissatgeAlerta[$id]['result']='ha fallat';
				$mMissatgeAlerta[$id]['email']=$mUsuarisRef[$id]['email'];
			}
			else
			{
				$mMissatgeAlerta[$id]['result']='ok';
				$mMissatgeAlerta[$id]['email']=$mUsuarisRef[$id]['email'];
			}
			
		}
	}
	reset($mCadenaUsuaris);
	
	}
	
	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function mails_novaPeticio($opcio,$opGi,$db)
{
	global $mPars,$mParams,$mUsuari;


	$missatgeAlerta=false;

	if($mParams['mailNotificacionsActives']==1)
	{

	$grupId_=$mPars['grup_id'];
		$mPars['grup_id']=$opGi;
	$mGrup=getGrup($db);
	
		$mPars['grup_id']=$grupId_;
	if($mPars['nivell']!='visitant')
	{
		$mUsuari=db_getUsuari($mPars['usuari_id'],$db);
	}
	$mUsuariResp=db_getUsuari($mGrup['usuari_id'],$db);
	
	//pel responsable de grup:
	
	$subjecte="CAC(GC) - Grup '".(urldecode($mGrup['nom']))."' > 1 peticio pendent - '".$opcio."'";
	$missatge1="
	Hola ".(urldecode($mUsuariResp['usuari'])).",<br>
	<br>
	hi ha una nova petici� '".$opcio."' enviada per ".(urldecode($mUsuari['usuari'])).", que espera la teva aprovaci�.
	<br>
	Pots posar-te en contacte amb ell/a responent directament a aquest correu.	
	<br>
	<br>
	salutacions
		";
		

		$missatge="
			<html>
			<head>
			<title></title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				</tr>
			</table>
			</body>
			";
		
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddReplyTo($mUsuari['email'],$mUsuari['usuari']);
			$mail->AddAddress($mUsuariResp['email'], $mUsuariResp['usuari']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

			$missatgeAlerta=false;
			
			if($mail->Send())
			{
				$missatgeAlerta=true;
			}

			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mUsuari['email'],$mUsuari['usuari']);
			$mail->Send();
			
	

	//pel nou usuari:
	
	$subjecte="CAC(GC) - has enviat una petici� '".$opcio."' al grup ".(urldecode($mGrup['nom']));
	$missatge1="
	Hola ".(urldecode($mUsuari['usuari'])).",<br>
	<br>
	has enviat una petici� al grup ".(urldecode($mGrup['nom'])).". 
	<br>
	<br>El responsable de grup ha rebut una notificaci�.
	<br>
	Quan aprovi o refusi la teva petici� rebr�s un mail informatiu. 
	<br>
	<br>
	salutacions
		";
		

		$missatge="
			<html>
			<head>
			<title></title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				</tr>
			</table>
			</body>
			";
			
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddReplyTo($mUsuari['email'],$mUsuari['usuari']);
			$mail->AddAddress($mUsuariResp['email'], $mUsuariResp['usuari']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

			$missatgeAlerta=false;
			
			if($mail->Send())
			{
				$missatgeAlerta=true;
			}

			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mUsuariResp['email'],$mUsuariResp['usuari']);
			$mail->Send();
			
	}	
	
	return $missatgeAlerta;
}

//------------------------------------------------------------------------------
function mailAusuariPeticioAprovada($tipus,$uGi,$grupId,$db) //tupus:'altaGrup' o 'baixaGrup'
{
	global $mPars,$mParams;


	$missatgeAlerta=false;
	
	if($mParams['mailNotificacionsActives']==1)
	{
	
	$grupId_=$mPars['grup_id'];
		$mPars['grup_id']=$grupId;
	$mGrup=getGrup($db);
		$mPars['grup_id']=$grupId_;
	$mUsuari=db_getUsuari($uGi,$db);
	$mUsuariResp=db_getUsuari($mGrup['usuari_id'],$db);

	
	//per l'usuari:
	
	$subjecte="CAC(GC) - la teva petici� de '".$tipus."' al grup ".(urldecode($mGrup['nom']))." ha estat aprovada.";
	$missatge1="
	Hola ".$mUsuari['usuari'].",<br>
	<br>
	la teva petici� de '".$tipus."' al grup ".(urldecode($mGrup['nom']))." ha estat aprovada pel responsable de grup.
	<br>
	<br>
	Seguidament rebr�s un correu amb les teves dades d'acc�s actualitzades.
	<br>
	<br>
	salutacions!
		";
		
		$missatge="
			<html>
			<head>
			<title></title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				</tr>
			</table>
			</body>
			";
			
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddReplyTo($mUsuari['email'],$mUsuari['usuari']);
			$mail->AddAddress($mUsuariResp['email'], $mUsuariResp['usuari']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
		
			if($mail->Send())
			{
				mail_enviarAccesAusuaris(','.$uGi.',',$db);
		
				//copia a vado
				$mail=crearObjectePhpMailer();
				$mail->From		  = $mParams['mailAutoUser'];
				$mail->FromName	  = $mParams['mailAutoName'];
				$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
				$mail->MsgHTML($missatge);
				$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
				$mail->AddReplyTo($mUsuariResp['email'],$mUsuariResp['usuari']);
				$mail->Send();
				$missatgeAlerta=true;
			}
			
	}		
	
	return $missatgeAlerta;

}


//------------------------------------------------------------------------------
// GESTIO COMANDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function mail_recepcioAcceptada($cadenaRecepcionsAConfirmar,$grupId,$db)
{
	global $mPars,$mParams,$mRebostsRef,$mMissatgeAlerta;

	$mMissatgeAlerta['missatge11']='';
	$mMissatgeAlerta['result11']=false;

	if($mParams['mailNotificacionsActives']==1)
	{


	$mUsuariResp=db_getUsuari($mRebostsRef[$grupId]['usuari_id'],$db); // responsable del grup receptor
	
	$mCadenaRecepcionsAConfirmar=explode(',',$cadenaRecepcionsAConfirmar);
	while(list($key,$grupIdRecull)=each($mCadenaRecepcionsAConfirmar))
	{
		if($grupIdRecull!='')
		{
			$mUsuari=db_getUsuari($mRebostsRef[$grupIdRecull]['usuari_id'],$db); //responsable del grup que recull;

			$subjecte="CAC(GC) - ".$mPars['selRutaSufix']." - punt d'entrega acceptat.";
			$missatge1="
	Hola ".$mUsuari['usuari'].",<br>
	<br>
	la recepci� de la comanda del teu grup '".(urldecode($mRebostsRef[$grupIdRecull]['nom']))."' ha estat acceptada pel responsable del grup '".(urldecode($mRebostsRef[$grupId]['nom']))."'.
	<br>
	<br>
	Pots comunicar-te amb el responsable del grup del teu punt d'entrega responent a aquest mateix correu.
	<br>
	<br>
	salutacions!
		";
		
		$missatge="
			<html>
			<head>
			<title></title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				</tr>
			</table>
			</body>
			";
			
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuari['email'], $mUsuari['usuari']);
			$mail->AddReplyTo($mUsuariResp['email'],$mUsuariResp['usuari']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

			
			if($mail->Send())
			{
				$mMissatgeAlerta['missatge1'].="<p class='pAlertaOk4'>s'ha comunicat l'acceptaci� de la recepci� al responsable del grup '".(urldecode($mRebostsRef[$grupIdRecull]['nom']))."' </p>";
				$mMissatgeAlerta['result11']=true;
		
				$mail=crearObjectePhpMailer();
				$mail->From		  = $mParams['mailAutoUser'];
				$mail->FromName	  = $mParams['mailAutoName'];
				$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
				$mail->MsgHTML($missatge);
				$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
				$mail->AddReplyTo($mUsuariResp['email'],$mUsuariResp['usuari']);
				$mail->Send();
			}
			else
			{
				$mMissatgeAlerta['missatge1'].="<p class='pAlertaOk4'>Atenci�: No s'ha pogut comunicar l'acceptaci� de la recepci� al responsable del grup '".(urldecode($mRebostsRef[$grupIdRecull]['nom']))."' </p>";
				$mMissatgeAlerta['result11']=false;
			}
		}
	}
	reset($mCadenaRecepcionsAConfirmar);
	
	}

	return $mMissatgeAlerta;

}


//------------------------------------------------------------------------------
function mail_anulacioReserves($mComandaModificada,$producteId,$db) // anulacio total de le comandes d'un producte de tots els grups
{
	global $mPars,$mParams,$mParametres,$mGrupsRef,$mUsuarisRef,$mProductes;

	$missatge='';

	$result=false;
	if($mParams['mailNotificacionsActives']==1)
	{

	$result=false;
	
	$mProducteAnular=db_getProducte($producteId,$db);
	
	$grupId=substr($mComandaModificada['2'],0,strpos($mComandaModificada['2'],'-'));
	if($grupId!=0)
	{
		$subjecte="CAC(GC) (ruta ".$mPars['selRutaSufix'].") - Notificaci� anul.laci� de reserva del producte id:".$producteId." - ".(urldecode($mProductes[$producteId]['producte']))." ";
		$missatge1="
	Hola ".(urldecode($mUsuarisRef[$mComandaModificada[1]]['usuari'])).",<br>
	<br>
	L'administrador ha modificat la teva comanda al grup <b>".(urldecode($mComandaModificada['2']))."</b> 
	<br>
	anul.lant la reserva del producte <b>id:".$producteId."</b>, ".(urldecode($mProducteAnular['producte']))."</b>,  del que havies demanat <b>".$mComandaModificada[4]."</b> unitats,
	<br>
	 ja que aquest producte ha deixat d'estar estar disponible. 
	<br>
	<p style='color:red;'>
	<i>Atenci�: Aquest canvi provablement afecta la forma de pagament que heu escollit (% ms). 
	<br>
	Comproveu al SADI (gestor comandes) que la forma de pagament de la vostra comanda es tal com voleu. 
	<br>
	Si no es aix� i no podeu actualitzar-la vosaltres perqu� el periode de reserves ja est� tancat, comuniqueu 
	a l'administrador quina forma de pagament voleu (respectant el maxim % d'ecos amb el que podeu pagar).
	<br>
	<br>
	Per comunicar-vos amb l'administrador tant sols heu de respondre a aquest correu donant els indicacions que voleu comunicar.
	</i>
	</p>
	<br><br>
	<p>salutacions</p>
	
		";

		$missatge="
			<html>
			<head>
			<title></title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				</tr>
			</table>
			</body>
			";
			
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$mComandaModificada[1]]['email'], $mUsuarisRef[$mComandaModificada[1]]['usuari']);
			$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

			//copia a resp grup
			if($mail->Send())
			{
				$result=true;
			}
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML('[c�pia pel responsable del grup]<br><br>'.$missatge);
			$mail->AddAddress($mUsuarisRef[$mGrupsRef[$grupId]['usuari_id']]['email'],$mUsuarisRef[$mGrupsRef[$grupId]['usuari_id']]['usuari']);
			
			$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->Send();
			
			//copia a admin
			
			if($mail->Send())
			{
				$result=true;
			}
			
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML("[c�pia per l'administrador]<br><br>".$missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mUsuarisRef[$mComandaModificada[1]]['email'], $mUsuarisRef[$mComandaModificada[1]]['usuari']);
			$mail->Send();
	}

	}

	return $result;
}

//------------------------------------------------------------------------------
function mail_anulacioReservesUsuari($mComandaModificada,$mAr,$db)  //anulacio total o parcial de la comanda d'un producte d'un usuari en un grup
{
	global $mPars,$mParams,$mRebostsRef,$mUsuarisRef;

	$result=true;
	
	$missatge='';

	if($mParams['mailNotificacionsActives']==1)
	{

	$mProducteAnular=db_getProducte($mPars['selProducteId'],$db);

	$subjecte="CAC(GC) (ruta ".$mPars['selRutaSufix'].") - Notificaci� anul.laci� de reserva del producte id:".$mPars['selProducteId']."-".(urldecode($mProducteAnular['producte']));
	$missatge1="
	Hola ".$mUsuarisRef[$mComandaModificada[1]]['usuari'].",<br>
	<br>
	L'administrador ha modificat la teva comanda al grup <b>".(urldecode($mComandaModificada['2']))."</b> 
	<br>
	anul.lant <b>".$mComandaModificada[5]." uts</b> de la reserva del producte <b>id:".$mPars['selProducteId'].",  
	".(urldecode($mProducteAnular['producte']))."</b>, del que havies demanat <b>".$mComandaModificada[4]." uts</b>,
	ja que aquest producte ha deixat d'estar estar disponible. 
	<br>
	<br>
	* el criteri per anul.laci� de reserves quan falta parcialment un producte reservat es per data i hora de l'�ltima revisi� de la comanda del producte
	<br><br>
	salutacions
	<br><br>
	<i>".(urldecode($mUsuarisRef[$mPars['usuari_id']]['usuari']))."<br>administrador gestor comandes CAC</i>
		";

		$missatge="
			<html>
			<head>
			<title></title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				</tr>
			</table>
			</body>
			";
			

			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$mComandaModificada[1]]['email'], $mUsuarisRef[$mComandaModificada[1]]['usuari']);
			$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment


			//copia a resp grup
			$result=false;
			if($mail->Send())
			{
				$result=true;
			}
						
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML('[c�pia pel responsable del grup]<br><br>'.$missatge);
			$mail->AddAddress($mUsuarisRef[$mRebostsRef[$mAr['grup']]['usuari_id']]['email'],$mUsuarisRef[$mRebostsRef[$mAr['grup']]['usuari_id']]['usuari']);
			$mail->AddReplyTo($mUsuarisRef[$mComandaModificada[1]]['email'], $mUsuarisRef[$mComandaModificada[1]]['usuari']);
			$mail->Send();

			
			//copia a admin
			$result=false;
			if($mail->Send())
			{
				$result=true;
			}
						
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML("[c�pia per l'administrador]<br><br>".$missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mUsuarisRef[$mComandaModificada[1]]['email'], $mUsuarisRef[$mComandaModificada[1]]['usuari']);
			$mail->Send();
			
	}	
	
	return $result;
}



//------------------------------------------------------------------------------
//  SERVEI TRANSPORT
//------------------------------------------------------------------------------
function mail_missatgeAresposta($mConfirmarResposta,$mTram,$mVehicle)
{
	//el propietari d'una demanda o oferta de transport envia una resposta de confirmacio o descartament a un usuari
	//el contracte pendent de l'usuari contractant canvia d'estat a confirmat o be s'esborra

		global $mPars,$mParams,$mUsuarisRef,$mMunicipis,$mStTipusPluralAsingular,$mParametres;
		

		$missatge='';

	if($mParams['mailNotificacionsActives']==1)
	{
		if($mConfirmarResposta[4]=='c')
		{
			$estat='ACCEPTAT';
			$nota="<b>El contracte s'ha realitzat</b>";
		}
		else
		{
			$estat='DESCARTAT';
			$nota="<b>El contracte NO s'ha realitzat</b>";
		}
			
			
		$subjecte="CAC(ST) - ".$mStTipusPluralAsingular[$mTram['tipus']].": ".$mTram['id'].", resposta de la part que voleu contractar";
		
		$mMunicipisRuta=explode(',',$mTram['municipi_desti']);
		$missatge1="
		Hola ".$mUsuarisRef[$mConfirmarResposta[2]]['usuari'].",<br><br>
		en ".(urldecode($mUsuarisRef[$mTram['usuari_id']]['usuari']))." (".$mUsuarisRef[$mTram['usuari_id']]['email']."), 
		propietari de la ".$mStTipusPluralAsingular[$mTram['tipus']]." de transport que voleu contractar:
		</p>
		<p style='font-size:11px;'>
		id tram-".$mStTipusPluralAsingular[$mTram['tipus']].": ".$mTram['id']."
		<br>
		hora sortida: ".$mTram['sortida']."
		<br>
		municipi origen: <b>".(urldecode($mMunicipis[$mTram['municipi_origen']]))."</b>
		<br>
		<br>
		hora arribada: ".$mTram['arribada']."
		<br>
		municipi desti: <b>".(urldecode($mMunicipis[$mTram['municipi_desti']]))."</b>
		<br>
		passant per: <br>";
		while(list($key,$val)=each($mMunicipisRuta))
		{
			if($val!='' && $val!='undefined')
			{
				$missatge1.="
				<p>".(substr(urldecode($mMunicipis[$val]),0,20))."</p>
				";
			}
		}
		reset($mMunicipisRuta);
		
		$missatge1.="
		<br>
		prioritat: ".$mTram['categoria0']."
		</p>
		<br>
		descripci�:
		<br>
		<p style='background-color:#eeffff;'>
		".(urldecode($mTram['acords_explicits']))."
		</p>
		<br>
		ha <b>".$estat."</b> la teva resposta:
		<p style='font-size:11px;'>
		vehicle: ".$mVehicle['marca'].", ".$mVehicle['model']."
		";
		if($mConfirmarResposta[2]=='417' || $mTram['usuari_id']=='417') 
		{
			$total=	$mTram['km']*$mParametres['tarifaCac']['valor'];
			
			$missatge1.="
		<br>
		places: ".$mConfirmarResposta['places_reservades']." places
		<br>
		pes: ".$mConfirmarResposta['pes_reservat']." kg
		<br>
		volum: ".$mConfirmarResposta['volum_reservat']." litres
		</p>
		<p>
		import a cobrar per la part que ofereix el transport:		
		<br>
		".$total."ums<br>(".($total*($mTram['pc_ms'])/100)." ecos + ".($total*(100-$mTram['pc_ms'])/100)." euros)
			";
		
		}
		else
		{
			$missatge1.="
		<br>
		places: ".$mConfirmarResposta['places_reservades']." places a ".$mTram['preu_places']." ums/pla�a
		<br>
		pes: ".$mConfirmarResposta['pes_reservat']." kg a ".$mTram['preu_pes']." ums/kg
		<br>
		volum: ".$mConfirmarResposta['volum_reservat']." litres  a ".$mTram['preu_volum']." ums/litre
		</p>
		<p>
		import a cobrar per la part que ofereix el transport:		
		<br>
		".$total."ums<br>(".($total*($mTram['pc_ms'])/100)." ecos + ".($total*(100-$mTram['pc_ms'])/100)." euros)
			";
		}
		$missatge1.="
		<br><br>
		</p>
		<p style='font-size:11px;'>
		* El contracte es considera tancat entre les dues parts
		<br>
		** si una de les parts vol modificar-lo cal que es posi en contacte amb l'altra part
		<br>
		*** si hi ha desacord o greu inconveni�ncia qualsevol de les parts pot sol.licitar a 
		l'administrador que ho resolgui aplicant els seus propis criteris
		<br>
		**** La CAC nom�s ofereix aquest servei per facilitar que es trobin les ofertes i les demandes. 
		La CAC no gestionar� cap altre oferta o demanda de transport que les que ella mateixa generi 
		com a usuaria del sistema. Per tant, els cobraments d'aquests serveis son responsabilitat de les
		parts contractades i contractants, exclussivament.
		</p>
		<br>
		<p>	".$nota."
		<br>
		<br>
		salutacions!
		</p>
		";

	
			$missatge="
				<html>
				<head>
				<title></title>
				</head>
				<body>
				<table width='70% align='center'>
					<tr>
						<td width='100%'>
						<p>".$missatge1."</p>
						</td>
					</tr>
					</tr>
				</table>
				</body>
				";
				
		//missatge a la part contractant

			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$mConfirmarResposta[2]]['email'], $mUsuarisRef[$mConfirmarResposta[2]]['usuari']);
			$mail->AddReplyTo($mUsuarisRef[$mConfirmarResposta[0]]['email'],$mUsuarisRef[$mConfirmarResposta[0]]['usuari']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
			$result=true;
			
			if(!$mail->Send())
			{
				$result=false;
			}
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mUsuarisRef[$mConfirmarResposta[0]]['email'],$mUsuarisRef[$mConfirmarResposta[0]]['usuari']);
			$mail->Send();

	
			$missatge="
				<html>
				<head>
				<title></title>
				</head>
				<body>
				<table width='70% align='center'>
					<tr>
						<td width='100%'>
						<p>[c�pia per la part contractada('".(urldecode($mUsuarisRef[$mTram['usuari_id']]['usuari']))."') del missatge enviat a la part contractant ('".(urldecode($mUsuarisRef[$mConfirmarResposta[2]]['usuari']))."')]</p>
						<br>
						<p>".$missatge1."</p>
						</td>
					</tr>
					</tr>
				</table>
				</body>
				";
		//copia a la part contractant
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$mConfirmarResposta[0]]['email'], $mUsuarisRef[$mConfirmarResposta[0]]['usuari']);
			$mail->AddReplyTo($mUsuarisRef[$mConfirmarResposta[2]]['email'],$mUsuarisRef[$mConfirmarResposta[2]]['usuari']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

			if(!$mail->Send())
			{
				$result=false;
			}
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mUsuarisRef[$mConfirmarResposta[2]]['email'],$mUsuarisRef[$mConfirmarResposta[2]]['usuari']);
			$mail->Send();
			
	}

		return $result;
}


//------------------------------------------------------------------------------

function mail_missatgeNovaResposta($opcio,$indexTram,$mContractesCombinadaTram,$db)
{
	// l'usuari respon a una demanda o una oferta de transport d'un altre usuari
	// s'envia un correu de notificacio de la resposta al propietari de l'oferta o demanda, i una copia a l'�suari que ha respost a aquesta
	// l'usuari queda a l'espera que el propietari de l'oferta o demanda li confirmi o decarti la resposta
		global $mPars,$mParams,$mUsuarisRef,$mMunicipis,$mVehicles,$mParametres;
		
		$missatge='';

	if($mParams['mailNotificacionsActives']==1)
	{
		$mPars['selTramId']=$indexTram;
		$mTram=db_getTram($db);
		
		$mText['responedor']['quantitat_pes']['anterior']=$mContractesCombinadaTram['anterior']['quantitat_pes'];
		$mText['responedor']['quantitat_volum']['anterior']=$mContractesCombinadaTram['anterior']['quantitat_volum'];
		$mText['responedor']['quantitat_places']['anterior']=$mContractesCombinadaTram['anterior']['quantitat_places'];
		$mText['responedor']['quantitat_pes']['actual']=$mContractesCombinadaTram['actual']['quantitat_pes'];
		$mText['responedor']['quantitat_volum']['actual']=$mContractesCombinadaTram['actual']['quantitat_volum'];
		$mText['responedor']['quantitat_places']['actual']=$mContractesCombinadaTram['actual']['quantitat_places'];

		if($mTram['tipus']=='DEMANDES')
		{
			$mText['propietari']['tipus_tram']='DEMANDA';
			$mText['propietari']['vehicle_id']='';
			$mText['responedor']['vehicle_id']=$mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['marca']." ".$mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['model'];
		}
		else
		{
			$mText['propietari']['tipus_tram']='OFERTA';
			$mText['propietari']['vehicle_id']=$mVehicles[$mTram['vehicle_id']]['marca']." ".$mVehicles[$mTram['vehicle_id']]['model'];
			$mText['responedor']['vehicle_id']='';
		}

		if($opcio=='contracteNou')
		{
			$subjecte="
			CAC(ST) - resposta de en/la ".$mUsuarisRef[$mPars['usuari_id']]['usuari']." a la teva ".$mText['propietari']['tipus_tram'].", id:".$mTram['id'];
			
			$mText['propietari']['missatge']="
			Hola, ".(urldecode($mUsuarisRef[$mTram['usuari_id']]['usuari'])).",
			<br>
			en/la ".$mUsuarisRef[$mPars['usuari_id']]['usuari']." t'ha enviat una resposta a la teva ".$mText['propietari']['tipus_tram']." de transport.
			";

			$mText['responedor']['subjecte']="CAC(ST) Has enviat una resposta resposta a una ".$mText['propietari']['tipus_tram'];
			$mText['responedor']['missatge']="
			Hola, ".$mUsuarisRef[$mPars['usuari_id']]['usuari'].",
			<br>
			has enviat una resposta a la ".$mText['propietari']['tipus_tram']." de transport id:".$mTram['id'].".
			";
		}
		else if($opcio=='contracteModificat')
		{
			$subjecte="CAC(ST) - l'usuari ".$mUsuarisRef[$mPars['usuari_id']]['usuari']." ha modificat la seva resposta a la teva <b>".$mText['propietari']['tipus_tram']."</b> de transport id:".$mTram['id'].".";
			$mText['propietari']['missatge']="
			Hola, ".$mUsuarisRef[$mTram['usuari_id']]['usuari'].",
			<br>
			en/la ".$mUsuarisRef[$mPars['usuari_id']]['usuari']." ha modificat la seva resposta a la teva ".$mText['propietari']['tipus_tram']." de transport.
			";

			$mText['responedor']['subjecte']="CAC(ST) Has modificat la teva resposta a una ".$mText['propietari']['tipus_tram'];
			$mText['responedor']['missatge']="";
		}
			
		//pel propietari de l'oferta o demanda
		$mMunicipisRuta=explode(',',$mTram['municipi_desti']);
		$missatge1="
		Hola ".(urldecode($mUsuarisRef[$mTram['usuari_id']]['usuari'])).",<br><br>
		en/la ".(urldecode($mUsuarisRef[$mPars['usuari_id']]['usuari']))." (".$mUsuarisRef[$mPars['usuari_id']]['email']."),
		ha respost a la teva ".$mText['propietari']['tipus_tram']." de transport:
		</p>
		<p style='font-size:11px;'>
		id: ".$mTram['id']."
		<br>
		hora sortida: ".$mTram['sortida']."
		<br>
		municipi origen: <b>".(urldecode($mMunicipis[$mTram['municipi_origen']]))."</b>
		<br>
		<br>
		hora arribada: ".$mTram['arribada']."
		<br>
		municipi desti: <b>".(urldecode($mMunicipis[$mTram['municipi_desti']]))."</b>
		<br>
		passant per: <br>";
		while(list($key,$val)=each($mMunicipisRuta))
		{
			if($val!='' && $val!='undefined')
			{
				$missatge1.="
				".(substr(urldecode($mMunicipis[$val]),0,20))."
				";
			}
		}
		reset($mMunicipisRuta);
		
		$missatge1.="
		<br>
		prioritat: ".$mTram['categoria0']."
		</p>
		<br>
		descripci�:
		<br>
		<font style='background-color:#eeffff;'>
		".(urldecode($mTram['acords_explicits']))."
		</font>
		";

		if($mTram['tipus']=='DEMANDES')
		{
			if($mPars['usuari_id']=='417' || $mTram['usuari_id']=='417') 
			{
				$total=	$mTram['km']*$mParametres['tarifaCac']['valor'];

				$missatge1.="
		<br>
		oferint un transport per a:
		<br>
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_places']."</b> places,
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_pes']."</b> kg,
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_volum']."</b> litres,
		<br>
		amb el vehicle <i>".(urldecode($mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['marca']))." ".(urldecode($mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['model']))." (".$mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['consum']." %)</i>
		<br>
		<br>
		total km: ".$mTram['km']."
		<br>
		<br>
		import a cobrar per la part que ofereix el transport (aplicant tarifa cac de ".$mParametres['tarifaCac']['valor']." ums/km al ".$mTram['pc_ms']."% ms):		
		<br>
		<b>".$total."ums  (".($total*(100-$mTram['pc_ms'])/100)." ecos + ".($total*(100-$mTram['pc_ms'])/100)." euros)</b>
		<br><br>
				";	
			}
			else
			{
				$total=
				$mContractesCombinadaTram['actual']['quantitat_places']*$mTram['preu_places']
				+
				$mContractesCombinadaTram['actual']['quantitat_pes']*$mTram['preu_pes']
				+
				$mContractesCombinadaTram['actual']['quantitat_volum']*$mTram['preu_volum'];
			
				$missatge1.="
		<br>
		oferint un transport per a:
		<br>
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_places']."</b> places a ".$mTram['preu_places']." ums/pla�a,
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_pes']."</b> kg a ".$mTram['preu_pes']." ums/kg,
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_volum']."</b> litres  a ".$mTram['preu_volum']." ums/litre,
		<br>
		amb el vehicle <i>".(urldecode($mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['marca']))." ".(urldecode($mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['model']))." (".$mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['consum']." %)</i>
		<br>
		<br>
		total km: ".$mTram['km']."
		<br>
		<br>
		import a cobrar per la part que ofereix el transport:		
		<br>
		<b>".$total."ums  (".($total*($mTram['pc_ms'])/100)." ecos + ".($total*(100-$mTram['pc_ms'])/100)." euros)</b>
		<br><br>
				";	
			}
			
		}
		else if($mTram['tipus']=='OFERTES')
		{
			if($mPars['usuari_id']=='417' || $mTram['usuari_id']=='417') 
			{
				$total=	$mTram['km']*$mParametres['tarifaCac']['valor'];

				$missatge1.="
		<p style='font-size:11px;'>
		reservant transport per a:
		<br>
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_places']."</b> places,
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_pes']."</b> kg,
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_volum']."</b> litres,
		<br>
		amb el vehicle <i>".(urldecode($mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['marca']))
		." ".(urldecode($mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['model']))
		." (".$mVehicles[$mContractesCombinadaTram['actual']['vehicle_id']]['consum']." %)</i>
		<br>
		<br>
		total km: ".$mTram['km']."
		<br>
		<br>
		import a cobrar per la part que ofereix el transport (aplicant tarifa cac de ".$mParametres['tarifaCac']['valor']." ums/km al ".$mTram['pc_ms']."% ms):		
		<br>
		<b>".$total."ums  (".($total*($mTram['pc_ms'])/100)." ecos + ".($total*(100-$mTram['pc_ms'])/100)." euros)</b>
		<br><br>
				";	
			}
			else
			{
				$total=
				$mContractesCombinadaTram['actual']['quantitat_places']*$mTram['preu_places']
				+
				$mContractesCombinadaTram['actual']['quantitat_pes']*$mTram['preu_pes']
				+
				$mContractesCombinadaTram['actual']['quantitat_volum']*$mTram['preu_volum'];
			$missatge1.="
		<p style='font-size:11px;'>
		reservant transport per a:
		<br>
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_places']."</b> places a ".$mTram['preu_places']." ums/pla�a,
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_pes']."</b> kg a ".$mTram['preu_pes']." ums/kg,
		<br>
		<b>".$mContractesCombinadaTram['actual']['quantitat_volum']."</b> litres  a ".$mTram['preu_volum']." ums/litre,
		<br>
		amb el vehicle <i>".(urldecode($mVehicles[$mTram['vehicle_id']]['marca']))." ".(urldecode($mVehicles[$mTram['vehicle_id']]['model']))." (".$mVehicles[$mTram['vehicle_id']]['consum']." %)</i>
		<br>
		<br>
		total km: ".$mTram['km']."
		<br>
		<br>
		import a cobrar per la part que ofereix el transport:		
		<br>
		<b>".$total."ums  (".($total*(100-$mTram['pc_ms'])/100)." ecos + ".($total*(100-$mTram['pc_ms'])/100)." euros)</b>
		<br><br>
			";	
			}
		}
		$missatge1.="
		</p>
		<p style='font-size:11px;'>
		<i>
		<b>Important</b>:
		<br>
		- La part contractant espera que acceptis o refusis la seva resposta. Mentre no ho facis, 
		els recursos reservats per la part contractant no podran ser reservats per d'altres usuaris.
		<br><br>
		- Quan acceptis o rebutgis la resposta de la part contractant, ambdues parts rebreu un correu de confirmaci� 
		indicant que el contracte s'ha realitzat.
		<br><br>
		- A partir d'aquest moment, la part contractada no podr� modificar les propietats de la seva ".$mText['propietari']['tipus_tram']." 
		i la part contractada no podr� retirar la seva reserva, per� s� podr� modificar-la per a reservar m�s recursos si aquests estan disponibles. 
		<br>
		En aquest cas, es repetir� el proc�s d'acceptaci� o rebuig, com si no hi hagu�s cap contracte previ.
		</b>
		</p>
		<br>
		<p>
		salutacions!
		<br><br>
		</p>
		";
	
			$missatge="
				<html>
				<head>
				<title></title>
				</head>
				<body>
				<table width='70% align='center'>
					<tr>
						<td width='100%'>
						<p>".$missatge1."</p>
						</td>
					</tr>
					</tr>
				</table>
				</body>
				";
				
		//missatge a la part contractada

			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$mTram['usuari_id']]['email'],$mUsuarisRef[$mTram['usuari_id']]['usuari']);
			$mail->AddReplyTo($mUsuarisRef[$mPars['usuari_id']]['email'],$mUsuarisRef[$mPars['usuari_id']]['usuari']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
				
			$result=true;
			if(!$mail->Send())
			{
				$result=false;
			}
							
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mUsuarisRef[$mPars['usuari_id']]['email'],$mUsuarisRef[$mPars['usuari_id']]['usuari']);
			$mail->Send();
				
		//-----------------------------------------------------------------------
	
			$missatge="
				<html>
				<head>
				<title></title>
				</head>
				<body>
				<table width='70% align='center'>
					<tr>
						<td width='100%'>
						<p>[c�pia per la part contractant('".(urldecode($mUsuarisRef[$mPars['usuari_id']]['usuari']))."') del missatge enviat a la part contractada('".(urldecode($mUsuarisRef[$mTram['usuari_id']]['usuari']))."')]</p>
						<br>
						<p>".$missatge1."</p>
						</td>
					</tr>
					</tr>
				</table>
				</body>
				";
		//copia a la part contractant

			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$mPars['usuari_id']]['email'],$mUsuarisRef[$mPars['usuari_id']]['usuari']);
			$mail->AddReplyTo($mUsuarisRef[$mTram['usuari_id']]['email'],$mUsuarisRef[$mTram['usuari_id']]['usuari']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
				
			if(!$mail->Send())
			{
				$result=false;
			}
							
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mUsuarisRef[$mTram['usuari_id']]['email'],$mUsuarisRef[$mTram['usuari_id']]['usuari']);
			$mail->Send();
		

	}
	return $result;

}

//------------------------------------------------------------------------------
//db_contractesTrams.php
function mail_anulacioContractes($tramId,$db) // anulacio total dels contractes d'una oferta o demanda de tots els usuaris
{
	global $mPars,$mParams,$mParametres,$mUsuarisRef,$mUnitatsContractades,$mStTipusPluralAsingular;
	
	$missatge='';

	if($mParams['mailNotificacionsActives']==1)
	{

		$tramId_=@$mPars['selTramId'];
		$mPars['selTramId']=$tramId;
	$mTram=db_getTram($db);
		$mPars['selTramId']=@$tramId_;


	//pel propietari del tram:

	$subjecte="CAC(ST) - anul.laci� contractes ".$mStTipusPluralAsingular[$mTram['tipus']].", id:".$mTram['id'];

	$missatge1="
	Hola ".$mUsuarisRef[$mTram['usuari_id']]['usuari'].",<br>
	<br>
	L'administrador ha anul.lat tots els contractes vinculats a la teva ".$mStTipusPluralAsingular[$mTram['tipus']].", id:".$mTram['id']." 
	<br>
	<br>
	salutacions
		";

		$missatge="
			<html>
			<head>
			<title></title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				</tr>
			</table>
			</body>
			";
			

			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$mTram['usuari_id']]['email'],$mUsuarisRef[$mTram['usuari_id']]['usuari']);
			$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
			$mail->Send();
							
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->Send();
		
	
	
	while(list($contracteId,$mContracte)=@each($mUnitatsContractades))
	{
		//pel responedor:

		$subjecte="CAC(ST) - anul.laci� contractes ".$mStTipusPluralAsingular[$mTram['tipus']].", id:".$mTram['id'];

		$missatge1="
	Hola ".$mUsuarisRef[$mContracte['usuari_id']]['usuari'].",<br>
	<br>
	L'administrador ha anul.lat tots els contractes vinculats a la teva ".$mStTipusPluralAsingular[$mTram['tipus']].", id:".$mTram['id']." 
	<br>
	<br>
	salutacions
		";

		$missatge="
			<html>
			<head>
			<title></title>
			</head>
			<body>
			<table width='70% align='center'>
				<tr>
					<td width='100%'>
					<p>".$missatge1."</p>
					</td>
				</tr>
				</tr>
			</table>
			</body>
			";
			
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mUsuarisRef[$mContracte['usuari_id']]['email'],$mUsuarisRef[$mContracte['usuari_id']]['usuari']);
			$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			//$mail->AddAttachment("images/phpmailer.gif");      // attachment
			//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
			$mail->Send();
							
			$mail=crearObjectePhpMailer();
			$mail->From		  = $mParams['mailAutoUser'];
			$mail->FromName	  = $mParams['mailAutoName'];
			$mail->Subject    = $subjecte." [".(date('H:i:s d/m/Y'))."]";
			$mail->MsgHTML($missatge);
			$mail->AddAddress($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->AddReplyTo($mParams['mailAutoReplyToAddress'],$mParams['mailAutoReplyToName']);
			$mail->Send();
					
	}
	@reset($mUnitatsContractades);
	
	}
	return true;
}




	
				
?>

		