<?php
//------------------------------------------------------------------------------
function put_peticioGrup($tipus,$opGi,$db)
{
	global $mPars;
	
	$mRutesSufixes=getRutesSufixes($db);

	$lastRuta=array_pop($mRutesSufixes);
	//nom�s provenen d'usuaris actius (?)
	
	$peticio='{pI='.(time()).';uI='.$mPars['usuari_id'].';t='.$tipus.';d='.date('d-m-Y h:m:s').'}';
	//echo "<br>"."update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='".$opGi."' ";
	if($mPars['selLlistaId']==0)
	{
		$result=@mysql_query("update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='".$opGi."' ",$db);
		//echo "<br>  12 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		$result=@mysql_query("update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='".$opGi."' ",$db);
		//echo "<br>  12 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
	}
	
	return true;
}
//------------------------------------------------------------------------------
function put_peticioGrupPlantilles($numPlantilles,$comentariNP,$opGi,$db)
{
	global $mPars,$mGrupsRef;

	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	
	$tipus='novesPlantilles-'.$mPars['perfilId'].'-'.$numPlantilles;
	$mRutesSufixes=getRutesSufixes($db);
	$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);
	$lastRuta=array_pop($mRutesSufixes);
	
	//nom�s provenen d'usuaris actius (?)
	
	$peticio='{pI='.(time()).';uI='.$mPars['usuari_id'].';m='.$comentariNP.';t='.$tipus.';d='.date('d-m-Y h:m:s').'}';
	//echo "<br>update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='".$opGi."' ";
	
	if($opGi==0)
	{
		if(!$result=@mysql_query("update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='62' ",$db))
		{
			//echo "<br>  49 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
			$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>enviar petici� crearPlantilles(".$numPlantilles.") a la llista del grup <b>".(urldecode($mGrupsRef[$opGi]['nom']))."</b>: ha fallat</p>";
		}
		else
		{
			$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>enviar petici� crearPlantilles(".$numPlantilles.") a la llista del grup <b>".(urldecode($mGrupsRef[$opGi]['nom']))."</b>: ok</p>";

			if(!mails_novaPeticio('novesPlantilles',$mPars['selLlistaId'],$db))
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'><b>notificar</b> petici� crearPlantilles(".$numPlantilles.") a la llista del grup <b>".(urldecode($mGrupsRef[$opGi]['nom'])).": ha fallat</p>";
			}
			else
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'><b>notificar</b> petici� crearPlantilles(".$numPlantilles.") a la llista del grup <b>".(urldecode($mGrupsRef[$opGi]['nom'])).": ok</p>";
			}
		}
	}
	else
	{
		//echo "<br>"."update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='".$opGi."' ";
		if(!$result=@mysql_query("update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='".$opGi."' ",$db))
		{
			//echo "<br>  70 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
			$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>enviar petici� crearPlantilles(".$numPlantilles.") a la llista del grup <b>".(urldecode($mGrupsRef[$opGi]['nom']))."</b>: ha fallat</p>";
		}
		else
		{
			$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>enviar petici� crearPlantilles(".$numPlantilles.") a la llista del grup <b>".(urldecode($mGrupsRef[$opGi]['nom']))."</b>: ok</p>";

			if(!mails_novaPeticio('novesPlantilles',$mPars['selLlistaId'],$db))
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'><b>notificar</b> petici� crearPlantilles(".$numPlantilles.") a la llista del grup <b>".(urldecode($mGrupsRef[$opGi]['nom'])).": ha fallat</p>";
			}
			else
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'><b>notificar</b> petici� crearPlantilles(".$numPlantilles.") a la llista del grup <b>".(urldecode($mGrupsRef[$opGi]['nom'])).": ok</p>";
			}
		}
	}
	
	return $mMissatgeAlerta;
}

//v37-funcio modificada
//------------------------------------------------------------------------------
function put_peticioGrupTransferirFitxes($idLlistesCadena,$idFitxesCadena,$periodeDesti,$db)
{
	global $mPars,$mGrupsRef;


	//echo "<br>".$idLlistesCadena.','.$idFitxesCadena.','.$periodeDesti;
	
	$mMissatgeAlerta=array();
	$mMissatgeAlerta['result']=true;
	$mMissatgeAlerta['missatge']='';

	$tipus='transferirFitxes-'.$mPars['selLlistaId'].'-'.$idLlistesCadena.'-'.$idFitxesCadena.'-'.$periodeDesti;
	$mRutesSufixes=getRutesSufixes($db);
	if($periodeDesti=='')
	{
		$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);
		$lastRuta=array_pop($mRutesSufixes);
		$taulaProductors='productors';
	}
	else
	{
		$lastRuta=$periodeDesti;
		if(explode('-',$mPars['selRutaSufix'])==2)//ruta especial
		{
			$taulaProductors='productors_'.$mPars['selRutaSufix'];
		}
		else
		{
			$taulaProductors='productors';
		}
	}

	
	$peticio='{pI='.(time()).';uI='.$mPars['usuari_id'].';t='.$tipus.';d='.date('d-m-Y h:m:s').'}';
	
	$mLlistes=explode(',',$idLlistesCadena);
	
	while(list($key,$llistaId)=each($mLlistes))
	{
		if($llistaId==0)
		{
			if(!$result=@mysql_query("update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='62' ",$db))
			{
				//echo "<br>  84 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>enviar petici� <b>transferirFitxes</b> a la llista del grup <b>".(urldecode($mGrupsRef[$llistaId]['nom']))."</b>: ha fallat</p>";
			}
			else
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>enviar petici� <b>transferirFitxes</b> a la llista del grup <b>".(urldecode($mGrupsRef[$llistaId]['nom']))."</b>: ok</p>";

				if(!mails_novaPeticio('transferirFitxes-'.$mPars['selLlistaId'].'-'.$idLlistesCadena.'-'.$idFitxesCadena,$llistaId,$db))
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>notificar petici� <b>transferirFitxes</b> a la llista del grup <b>".(urldecode($mGrupsRef[$llistaId]['nom']))."</b>: ha fallat</p>";
				}
				else
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>notificar petici� <b>transferirFitxes</b> a la llista del grup <b>".(urldecode($mGrupsRef[$llistaId]['nom']))."</b>: ok</p>";
				}
			}
		}
		else
		{
			if(!$result=@mysql_query("update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='".$llistaId."' ",$db))
			{
				//echo "<br>  84 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>enviar petici� <b>transferirFitxes</b> a la llista del grup <b>".(urldecode($mGrupsRef[$llistaId]['nom']))."</b>: ha fallat</p>";
			}
			else
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>enviar petici� <b>transferirFitxes</b> a la llista del grup <b>".(urldecode($mGrupsRef[$llistaId]['nom']))."</b>: ok</p>";

				if(!mails_novaPeticio('transferirFitxes-'.$mPars['selLlistaId'].'-'.$idLlistesCadena.'-'.$idFitxesCadena,$llistaId,$db))
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>notificar petici� <b>transferirFitxes</b> a la llista del grup <b>".(urldecode($mGrupsRef[$llistaId]['nom']))."</b>: ha fallat</p>";
				}
				else
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>notificar petici� <b>transferirFitxes</b> a la llista del grup <b>".(urldecode($mGrupsRef[$llistaId]['nom']))."</b>: ok</p>";
				}
			}
		}
	}
	
	return $mMissatgeAlerta;
}

//------------------------------------------------------------------------------
function put_peticioAssociarPerfilAgrups($idGrupsCadena,$db)
{
	global $mPars,$mGrupsRef;

	$mMissatgeAlerta=array();
	$mMissatgeAlerta['missatge']='';
	
	$tipus='associarPerfil-'.$mPars['perfilId'].'-'.$idGrupsCadena;
	$mRutesSufixes=getRutesSufixes($db);
	$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);
	$lastRuta=array_pop($mRutesSufixes);
	
	$peticio='{pI='.(time()).';uI='.$mPars['usuari_id'].';t='.$tipus.';d='.date('d-m-Y h:m:s').'}';
	
	$mGrups=explode(',',$idGrupsCadena);
	
	while(list($key,$grupId)=each($mGrups))
	{
		if($grupId!='')
		{
		
		if($grupId==0)
		{
			if(!$result=@mysql_query("update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='62' ",$db))
			{
				//echo "<br>  140 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>enviar petici� <b>associarPerfil</b> al grup <b>".(urldecode($mGrupsRef[$grupId]['nom']))."</b>: ha fallat</p>";
			}
			else
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>enviar petici� <b>associarPerfil</b> al grup <b>".(urldecode($mGrupsRef[$grupId]['nom']))."</b>: ok</p>";

				if(!mails_novaPeticio('associarPerfil-'.$mPars['perfilId'].'-'.$grupId,$grupId,$db))
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>notificar petici� <b>associarPerfil</b> al grup <b>".(urldecode($mGrupsRef[$grupId]['nom']))."</b>: ha fallat</p>";
				}
				else
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>notificar petici� <b>associarPerfil</b> al grup <b>".(urldecode($mGrupsRef[$grupId]['nom']))."</b>: ok</p>";
				}
			}
		}
		else if($grupId!='')
		{
			if(!$result=@mysql_query("update rebosts_".$lastRuta." set notes=CONCAT(notes,'".$peticio."') where ref='".$grupId."' ",$db))
			{
				//echo "<br>  163 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
				$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>enviar petici� <b>associarPerfil</b> al grup <b>".(urldecode($mGrupsRef[$grupId]['nom']))."</b>: ha fallat</p>";
			}
			else
			{
				$mMissatgeAlerta['missatge'].="<p class='pAlertaOk4'>enviar petici� <b>associarPerfil</b> al grup <b>".(urldecode($mGrupsRef[$grupId]['nom']))."</b>: ok</p>";

				if(!mails_novaPeticio('associarPerfil-'.$mPars['perfilId'].'-'.$grupId,$grupId,$db))
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>notificar petici� <b>associarPerfil</b> al grup <b>".(urldecode($mGrupsRef[$grupId]['nom']))."</b>: ha fallat</p>";
				}
				else
				{
					$mMissatgeAlerta['missatge'].="<p class='pAlertaNo4'>notificar petici� <b>associarPerfil</b> al grup <b>".(urldecode($mGrupsRef[$grupId]['nom']))."</b>: ok</p>";
				}
			}
		}
		}
	}
	
	return $mMissatgeAlerta;
}
//------------------------------------------------------------------------------
//v37-funcio modificada
function get_peticionsGrup($opGi,$db) //existeix tb db_get_peticionsGrup(db.php) i getPeticionsGrup() (db.php)
{
	global $mPars,$mParams;

	$mRutesSufixes=getRutesSufixes($db);
	$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);
	$lastRuta=array_pop($mRutesSufixes);
	//echo "<br>select notes from rebosts_".$lastRuta." where ref='".$opGi."'";
	
	if($opGi==0){$opGi=$mParams['grupGestioCac'];}
	
	//echo "<br> select notes from rebosts_".$lastRuta." where ref='".$opGi."'";
	if(!$result=mysql_query("select notes from rebosts_".$lastRuta." where ref='".$opGi."'",$db))
	{
		//echo "<br>  128 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
	
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		$mPeticionsGrup=get_peticionsCadena($mRow['notes']);
	}
	return $mPeticionsGrup;
}

//------------------------------------------------------------------------------
function guardarPeticionsGrup($uGi,$peticionsGrupA,$peticionsGrupP,$peticionsGrupR,$db)
{
	global $mPars,$mParametres,$mGrup;

	$mRutesSufixes=getRutesSufixes($db);
	$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);
	$lastRuta=array_pop($mRutesSufixes);

	//>>>>>>>>>>>>>>>>>>   comprovar si han entrat peticions noves i afegir les noves a la matriu abans de fer la cadena

	//executar peticions aprovades
	
	$mPeticionsGrupA=get_peticionsCadena($peticionsGrupA);//aprovades
	$mPeticionsGrupR=get_peticionsCadena($peticionsGrupR);//rebutjades
	$mPeticionsGrupP=get_peticionsCadena($peticionsGrupP);//pendents
	$mPeticionsGrupFallides=array();
	$mPeticionsGrupGuardar=array();

	while(list($idPeticio,$mPeticioGrupA)=each($mPeticionsGrupA))
	{
		if(isset($mPeticioGrupA['t']))
		{
			if
			(
				substr_count($mPeticioGrupA['t'],'novesPlantilles')<=0
				&&
				substr_count($mPeticioGrupA['t'],'transferirFitxes')<=0
				&&
				substr_count($mPeticioGrupA['t'],'associarPerfil')<=0
			)
			{
				if(@$mPars['grupId']==0)
				{
					//netejar:
					if(!$result=mysql_query("update usuaris set grups=REPLACE(grups,',".$uGi.",',','), grups=REPLACE(grups,',,',',') where id='".$mPeticioGrupA['uI']."' ",$db))
					{
						//echo "<br>  73 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
						return false;
					}
					//echo "<br>update usuaris set grups=REPLACE(grups,',".$uGi.",',','), grups=REPLACE(grups,',,',',') where id='".$mPeticioGrupA['uI']."' ";
					if($mPeticioGrupA['t']=='altaGrup')
					{
						//afegir al grup:
						if(!$result=mysql_query("update usuaris set grups=CONCAT(grups,',','".$uGi."',','), estat='actiu' WHERE id='".$mPeticioGrupA['uI']."'",$db))
						{
							//echo "<br>  79 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
							$mPeticionsGrupFallides[$idPeticio]=$mPeticioGrupA;
						}
						else
						{
							mailAusuariPeticioAprovada('altaGrup',$mPeticioGrupA['uI'],$uGi,$db);
							unset($mPeticionsGrupA[$idPeticio]);
						}				
						//echo "<br>	update usuaris set grups=CONCAT(grups,',','".$uGi."',','), estat='actiu' WHERE id='".$mPeticioGrupA['uI']."'";
					}
					else if($mPeticioGrupA['t']=='baixaGrup')
					{
						//si l'usuari no ha fet comanda
						$result=mysql_query("select comandes_".$lastRuta." where usuari_id='".$mPeticioGrupA['uI']."' and grup_id='".$uGi."'",$db);
						//echo "<br>  84 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
				
						//echo "<br> select comandes_".$mPars['selRutaSufix']." where usuari_id='".$mPeticioGrupA['uI']."' and grup_id='".$uGi."'";
					
						$mRow=@mysql_fetch_array($result,MYSQL_ASSOC);
						if(!$mRow)
						{
							//treure del grup:
							if(!$result=mysql_query("update usuaris set grups=REPLACE(grups,',".$uGi.",',',') where id='".$mPeticioGrupA['uI']."'",$db))
							{
								//echo "<br>  79 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
								$mPeticionsGrupFallides[$idPeticio]=$mPeticioGrupA;
							}
							else
							{
								mailAusuariPeticioAprovada('baixaGrup',$mPeticioGrupA['uI'],$uGi,$db);
								unset($mPeticionsGrupA[$idPeticio]);
							}				
						}
						else
						{
							//$mPeticionsGrupFallides[$idPeticio]=$mPeticioGrupA;
							return false;
						}
					}
				}
			}
			else if(substr_count($mPeticioGrupA['t'],'novesPlantilles')>0)
			{
				$mTipus=explode('-',$mPeticioGrupA['t']);
				$perfilId=$mTipus[1];
					$mPars['selPerfilRef']=$perfilId;
				$mPerfil=db_getPerfil($db);
				$numPlantilles=@$mTipus[2];

				if($mPars['selLlistaId']==0)
				{
					$taulaProductes='productes_'.$lastRuta;
					$tipus=',dip�sit,';
				}
				else
				{
					$taulaProductes='productes_grups';
					$tipus='';
				}
				$cont=0;
				
				for($i=0;$i<$numPlantilles;$i++)
				{
					$historial='{p:novaPlantilla('.$i.'); us:'.$mPars['usuari_id'].';dt:'.date('d/m/Y H:i:s').';}';
					$mysqlChain="('',0,'plantilla-".(date('d/m/y H:i:s')).'-'.$cont."','0','".$mPerfil['id'].'-'.$mPerfil['projecte']."','alimentaci�','altres','".$tipus."','1 ut','1','1','1','1','50','0','0','0','','','','0','0','0','0','".$historial."','".$mPars['grup_id']."','')";
					
					//echo "<br>"."insert into ".$taulaProductes." values ".$mysqlChain;
					if($result=mysql_query("insert into ".$taulaProductes." values ".$mysqlChain,$db))
					{
						$cont++;
					}
					else
					{
						//echo "<br>  201 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
					}
				}
				if($cont==$numPlantilles)
				{
					mailAusuariPeticioAprovada('novesPlantilles('.$numPlantilles.')',$mPeticioGrupA['uI'],$uGi,$db);
					unset($mPeticionsGrupA[$idPeticio]);
				}
				else
				{
					//$mPeticionsGrupFallides[$idPeticio]=$mPeticioGrupA;				
				}				
			}
			else if(substr_count($mPeticioGrupA['t'],'transferirFitxes')>0)		
			{
			
				$mTipus=explode('-',$mPeticioGrupA['t']);
				$llistaIdOrigen=$mTipus[1];
				$mLlistes=explode(',',$mTipus[2]);
				$mFitxes=explode(',',$mTipus[3]);

				
					$grupId_=$mPars['grup_id'];
					$mPars['grup_id']=$llistaIdOrigen;
				$mGrupOrigen=getGrup($db);
					$mPars['grup_id']=$grupId_;
				
				$contFitxes=0;
				$contLlistes=0;
			while(list($key,$llistaId)=each($mLlistes))
				{
					if($llistaId!='')
					{
						if($llistaId==0)
						{
							$taulaProductes='productes_'.$lastRuta;
						}
						else
						{
							$taulaProductes='productes_grups';
						}
						
						//associar el perfil al grup desti si no ho est�
						$mProductes=db_getProductesLlista($llistaIdOrigen,$mTipus[3],$db);
						$mProductesKeys=array_keys($mProductes);
						$perfilId=substr($mProductes[$mProductesKeys[0]]['productor'],0,strpos($mProductes[$mProductesKeys[0]]['productor'],'-'));
						if(@substr_count($mGrup['productors_associats'],','.$perfilId.',')==0)
						{
							if(!db_associarPerfilAgrup($perfilId,$mGrup['id'],$db))
							{
								return false;
							}
						}
					
						if($mTipus[3]!='')
						{
							$mProductes=db_getProductesLlista($llistaIdOrigen,$mTipus[3],$db);
						
							while(list($key2,$mProducte)=each($mProductes))
							{
								$historial='{p:transferirFitxa;llistaOrigen: '.$mGrupOrigen['nom'].'; us:'.$mPars['usuari_id'].';;dt:'.date('d/m/Y H:i:s').';}';
						
								if($llistaId*1==0)
								{
						
						$mysqlChain="(	'',
										0,
										'".$mProducte['producte']."',
										'0',
										'".$mProducte['productor']."',
										'".$mProducte['categoria0']."',
										'".$mProducte['categoria10']."',
										'',
										'".$mProducte['unitat_facturacio']."',
										'".$mProducte['format']."',
										'".$mProducte['pes']."',
										'".$mProducte['volum']."',
										'".$mProducte['preu']."',
										'".$mProducte['ms']."',
										'0', 	
										'0',	
										'0',	
										'".$mProducte['notes_rebosts']."',
										'',
										'',
										'0',
										'0',
										'".$mParametres['varCostTransport']['valor']."',
										'0',
										'".$historial."',
										'".$llistaId."',
										''
										)";
										
								//echo "<br>"."insert into ".$taulaProductes." values ".$mysqlChain;
								}
								else
								{
						$mysqlChain="(	'',
										0,
										'".$mProducte['producte']."',
										'0',
										'".$mProducte['productor']."',
										'".$mProducte['categoria0']."',
										'".$mProducte['categoria10']."',
										'',
										'".$mProducte['unitat_facturacio']."',
										'".$mProducte['format']."',
										'".$mProducte['pes']."',
										'".$mProducte['volum']."',
										'".$mProducte['preu']."',
										'".$mProducte['ms']."',
										'0', 	
										'0',	
										'0',	
										'".$mProducte['notes_rebosts']."',
										'',
										'',
										'0',
										'0',
										'0',
										'0',
										'".$historial."',
										'".$llistaId."',
										''
										)";
						
								}					
								if($result=mysql_query("insert into ".$taulaProductes." values ".$mysqlChain,$db))
								{
									$contFitxes++;
								}
								else
								{
									//echo "<br>  490 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
								}
							}
							reset($mProductes);
						}
						$contLlistes++;
					}
				}
				reset($mLlistes);
				
				mailAusuariPeticioAprovada($mPeticioGrupA['t'],$mPeticioGrupA['uI'],$uGi,$db);
				unset($mPeticionsGrupA[$idPeticio]);
			}
			else if(substr_count($mPeticioGrupA['t'],'associarPerfil')>0)
			{
				$mTipus=explode('-',$mPeticioGrupA['t']);
				$perfilId=$mTipus[1];
				
				//echo "<br>"."update rebosts_".$lastRuta." set productors_associats=REPLACE(productors_associats,CONCAT(',','".$perfilId."',','),''), productors_associats=CONCAT(productors_associats,',".$perfilId.",') WHERE id='".$mPars['grup_id']."'";
				if($result=mysql_query("update rebosts_".$lastRuta." set productors_associats=REPLACE(productors_associats,CONCAT(',','".$perfilId."',','),''), productors_associats=CONCAT(productors_associats,',".$perfilId.",'), productors_associats=REPLACE(productors_associats,',,','') WHERE id='".$mPars['grup_id']."'",$db))
				{
					mailAusuariPeticioAprovada('associarPerfil a grup '.(urldecode($mGrup['nom'])),$mPeticioGrupA['uI'],$uGi,$db);

					unset($mPeticionsGrupA[$idPeticio]);
				}
				else
				{
					//echo "<br> 428 db_gestioGrup.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//$mPeticionsGrupFallides[$idPeticio]=$mPeticioGrupA;				
				}				
			}			
		}
	}
	reset($mPeticionsGrupA);

	//les rebutjades, simplement s'esborren ( o no s'escriuen)

	//actualitzar peticions pendents
	$cadenaGuardar='';
	while(list($idPeticio,$mPeticio)=each($mPeticionsGrupA)) //potser innecessari
	{
		if(count($mPeticio)>0)
		{
		
			$cadenaGuardar.='{pI='.$mPeticio['pI'].';uI='.$mPeticio['uI'].';t='.$mPeticio['t'].';d='.date('d:m:Y-h:m:s').'}';
		}
	}	
	reset($mPeticionsGrupA);
	
	while(list($idPeticio,$mPeticio)=each($mPeticionsGrupP)) //necessari
	{
		if(count($mPeticio)>0)
		{
			$cadenaGuardar.='{pI='.$mPeticio['pI'].';uI='.$mPeticio['uI'].';t='.$mPeticio['t'].';d='.date('d:m:Y-h:m:s').'}';
		}
	}	
	reset($mPeticionsGrupP);
	
	while(list($idPeticio,$mPeticio)=each($mPeticionsGrupFallides)) //necessari
	{
		if(count($mPeticio)>0)
		{
			$cadenaGuardar.='{pI='.$mPeticio['pI'].';uI='.$mPeticio['uI'].';t='.$mPeticio['t'].';d='.date('d:m:Y-h:m:s').'}';
		}
	}	
	reset($mPeticionsGrupFallides);

	
	if($mPars['grup_id']!=0)
	{
		if(!$result=mysql_query("update rebosts_".$lastRuta." set notes='".$cadenaGuardar."' where id='".$uGi."'",$db))
		{
			//echo "<br>  154 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
		
			return false;
		}
	}
	else	
	{
		if(!$result=mysql_query("update rebosts_".$lastRuta." set notes='".$cadenaGuardar."' where id='62'",$db))
		{
			//echo "<br>  154 db_gestioGrup.php".mysql_errno() . ": " . mysql_error(). "\n";
		
			return false;
		}
	}
	

	return true;
}

//------------------------------------------------------------------------------
function db_getProductesLlista($llistaOrigen,$fitxesIdCadena,$db)
{
	global $mPars;

	if($llistaOrigen==0)
	{
		$mPars['taulaProductes']="productes_".$mPars['selRutaSufix'];
	}
	else
	{
		$mPars['taulaProductes']='productes_grups';
	}
	
	$mProductes=array();
	

	//echo "<br>"."select * from ".$mPars['taulaProductes']." where LOCATE(CONCAT(',',id,','),' ,".$fitxesIdCadena.",')>0";
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where LOCATE(CONCAT(',',id,','),' ,".$fitxesIdCadena.",')>0 ",$db))
	{
		//echo "<br> 416 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'61','db.php');
    }
    else
    {
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mProductes[$mRow['id']]=$mRow;
		}
	}

	return $mProductes;
}

?>

		