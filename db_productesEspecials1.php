<?php
//------------------------------------------------------------------------------
function getEcosTotalFacturaComanda($mComandaCopiar,$db)
{
	global $mParametres,$mPars;
	
	$totalEcos=0;

	
	$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';
	$mProductes=db_getProductes2($db);

	$umsT=0;
	$ecosT=0;
	$eurosT=0;
	$pesT=0;

	$ctkTums=0;
	$ctkTecos=0;
	$ctkTeuros=0;
						
	$ctrTums=0;
	$ctrTecos=0;
	$ctrTeuros=0;
	
	$quantitatTotalCac=0;
	$quantitatTotalRebosts=0;


	if(!$result=mysql_query("select sum(estoc_previst*pes) from productes_".$mPars['selRutaSufix']." where actiu='1'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalCac+=$mRow[0];
	}

	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2)),$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$quantitatTotalRebosts+=$mRow['quantitat'];
		}
	}
	
	if(isset($mComandaCopiar['resum']) && $mComandaCopiar['resum']!='')
	{
		$mProductesComanda=explode(';',$mComandaCopiar['resum']);

		for($i=0;$i<count($mProductesComanda);$i++)
		{
			$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
			$key=str_replace('producte_','',$mIndexQuantitat[0]);
			$quantitat=@$mIndexQuantitat[1];
			if($key!='' && substr_count(@$mProductes[$key]['tipus'],'especial')==0)
			{
				$ums_=$mProductes[$key]['preu']*$quantitat;
				//echo "<br>id:".$key." producte:".$mProductes[$key]['producte']." preu:".$mProductes[$key]['preu']." quanti:".$quantitat;
				$umsT+=$ums_;

				$ecos_=$ums_*$mProductes[$key]['ms']/100;
				$ecosT+=$ecos_;
				$eurosT+=$ums_-$ecos_;

				$pesT+=$quantitat*$mProductes[$key]['pes'];
				
				$ctkTums+=$mProductes[$key]['pes']*$quantitat*($mProductes[$key]['cost_transport_extern_kg']+$mProductes[$key]['cost_transport_intern_kg']);
				$ctkTecos+=$mProductes[$key]['pes']*$quantitat*(($mProductes[$key]['cost_transport_extern_kg']*$mProductes[$key]['ms_ctek']/100)+($mProductes[$key]['cost_transport_intern_kg']*$mProductes[$key]['ms_ctik']/100));
				$ctkTeuros+=$mProductes[$key]['pes']*$quantitat*(($mProductes[$key]['cost_transport_extern_kg']*(100-$mProductes[$key]['ms_ctek'])/100)+($mProductes[$key]['cost_transport_intern_kg']*(100-$mProductes[$key]['ms_ctik'])/100));
			
				$ctrTums+=  $mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']+$mParametres['cost_transport_intern_repartit']['valor'])/($quantitatTotalCac+$quantitatTotalRebosts));
				$ctrTecos+= $mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']*$mParametres['ms_ctear']['valor']/100+$mParametres['cost_transport_intern_repartit']['valor']*$mParametres['ms_ctiar']['valor']/100)/($quantitatTotalCac+$quantitatTotalRebosts));
				$ctrTeuros+=$mProductes[$key]['pes']*$quantitat*(($mParametres['cost_transport_extern_repartit']['valor']*(100-$mParametres['ms_ctear']['valor'])/100+$mParametres['cost_transport_intern_repartit']['valor']*(100-$mParametres['ms_ctiar']['valor'])/100)/($quantitatTotalCac+$quantitatTotalRebosts));
			}
			
			//$mComanda['kgT']+=$pesT;
			//$mComanda['umsT']+=$umsT;
			//$mComanda['ecosT']+=$ecosT;
			//$mComanda['eurosT']+=$eurosT;
			//$mComanda['ctkTums']+=$ctkTums;
			//$mComanda['ctkTecos']+=$ctkTecos;
			//$mComanda['ctkTeuros']+=$ctkTeuros;
			//$mComanda['ctrTums']+=$ctrTums;
			//$mComanda['ctrTecos']+=$ctrTecos;
			//$mComanda['ctrTeuros']+=$ctrTeuros;
			//$mComanda['umstFDC']+=$umsT*$mParametres['FDCpp']['valor']/100;
			//$mComanda['ecostFDC']+=($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
			//$mComanda['eurostFDC']+=($umsT*$mParametres['FDCpp']['valor']/100)-($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
			//$mComanda['totalUms']+=$umsT+$ctkTums+$ctrTums+($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
			$totalEcos+=$ecosT+$ctkTecos+$ctrTecos+($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
			//$mComanda['totalEuros']+=$eurosT+$ctkTeuros+$ctrTeuros+($umsT*$mParametres['FDCpp']['valor']/100)-($umsT*$mParametres['FDCpp']['valor']/100)*($mParametres['msFDCpp']['valor']/100);
		}
	}
	
	return $totalEcos;
}



?>

		