allowedCharsEmail='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.@';
allowedCharsCompteEcos_lletres='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
emailNeededCharacters='@.';
allowedCharsMobil='0123456789';
allowedCharsCategoriesGrups='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-';
		
function checkFormPerfil()
{
	isValid=false;
	missatgeAlerta='';
	
	// camps obligatoris
	value=document.getElementById('i_projecte').value;
	if(value.length==0 || value.length<3)
	{
		missatgeAlerta+="\nCal introduir un nom de projecte (ha de tenir m�s de 2 car�cters)";			
	}

	value=document.getElementById('i_compteEcos').value;
	if(value.length!=8)
    {
		missatgeAlerta+="\nEl camp 'compte ecos' ha de tenir 8 car�cters";			
    }
	else if( value.length>0) 
	{
	   	allowedChars=allowedCharsCompteEcos_lletres;
		ok=0;
		//check for prohibited characters
       	for(i=0;i<4;i++)
        {
           for(j=0;j<allowedChars.length;j++)
           {
               if(value.charAt(i)==allowedChars.charAt(j)){ok++;}
           }
		}
	   	allowedChars=allowedCharsMobil;
		if(ok<4)
		{
			missatgeAlerta+="\nCamp 'compte ecos': els primers 4 d�gits han de ser lletres (car�cters permesos: "+allowedChars+" )";			
		}
		ok=0;
		//check for prohibited characters
       	for(i=4;i<9;i++)
        {
           for(j=0;j<allowedChars.length;j++)
           {
               if(value.charAt(i)==allowedChars.charAt(j)){ok++;}
           }
		}
		if(ok<4)
		{
			missatgeAlerta+="\nCamp 'compte ecos': els �ltims 4 d�gits han de ser nombres (car�cters permesos: "+allowedChars+" )";			
		}
	}

	value=document.getElementById('sel_usuariId').value;
	if(value=='')
	{
		missatgeAlerta+="\nCal seleccionar un responsable del perfil";			
	}
		
	if(missatgeAlerta.length!=0)
	{
		alert(missatgeAlerta);
	}
	else
	{
		document.getElementById('f_nouPerfil').submit();
	}
  
	return;
}	

function transferirPerfilAperiode()
{
	missatgeAlerta='';
	
	if(nivell=='sadmin')
	{
		if(perfilId=='')
		{
			missatgeAlerta+="\nCal editar un perfil per poder-lo transferir";			
		}
		
		value=document.getElementById('sel_rutaOnTransferir').value;
		if(value.lenght==0)
		{
			missatgeAlerta+="\nCal seleccionar un periode especial de desti";			
		}
		
		if (missatgeAlerta.length==0)
		{
			enviarFparsPerfilEditar('nouPerfilProductor.php?pId='+perfilId+'&sP='+value,'_self');
		}
	}
	return false;
}


function mostrarFormAccio(mousex,mousey,opcio)
{
	if(opcio=='segellEcocicPerfil')
	{
		document.getElementById('t_segellEcocicPerfil').style.position='absolute';
		document.getElementById('t_segellEcocicPerfil').style.top=mousey-50;
		document.getElementById('t_segellEcocicPerfil').style.left=mousex+10;
		document.getElementById('t_segellEcocicPerfil').style.visibility='inherit';
	}


	return;
}

function ocultarFormAccio(elementId)
{
	document.getElementById(elementId).style.position='absolute';
	document.getElementById(elementId).style.top=0;
	document.getElementById(elementId).style.left=0;
	document.getElementById(elementId).style.visibility='hidden';

	return;
}
function enviarFparsPerfilEditar(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='nouPerfilProductor.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}


		

		
		
		
					