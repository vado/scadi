<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "html.php";
include "eines.php";
include "html_ajuda1.php";
include "db_ajuda.php";

$mProblemaAmbProducte=array();
$mProblemaAmbGrup=array();

$parsChain=@$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db);
	$mPars['etiqueta']='TOTS';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';
	$mPars['sortBy']='categoria0,categoria10';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['paginacio']=-1;
	$mPars['vProducte']='TOTS';
	if(!isset($mPars['pagS'])){$mPars['pagS']=0;}
	if(!isset($mPars['pagFiltre'])){$mPars['pagFiltre']=$mPars['sortBy'].$mPars['veureProductesDisponibles'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vProductor'].$mPars['vSubCategoria'].$mPars['vCategoria'];}
	if(!isset($mPars['numItemsPag'])){$mPars['numItemsPag']=10;}
	if(!isset($mPars['numPags'])){$mPars['numPags']=0;} 

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}
post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['resumDeRuta.php']=db_getAjuda('resumDeRuta.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$parsChain=makeParsChain($mPars);

$mPuntsEntrega=db_getPuntsEntrega($db,'ref');
$mRebostsRef=db_getRebostsRef($db);
$mProductes=db_getProductes2($db);

$cG2='';
if($mPars['grup_id']!='0')
{
	$cG2=@$_GET['cG2'];
}
$mGrupsAmbComandaPerZonesPuntsEntrega=db_getGrupsAmbComandaPerZonesPuntsEntrega($db);
$mProductesJaEntregats=array();
if($mPars['excloureProductesJaEntregats']=='1')
{
	$mProductesJaEntregats=db_getProductesJaEntregats($db);
}

$mRecepcions=getRecepcions($db);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>Resum Comandes(Zones)</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js1.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
</head>
<body bgcolor='".$mColors['body']."'>
";
html_demo('resumDeRuta.php?');
echo "
	<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			<p>".$mContinguts['form']['titol']."</p>
			</th>
		</tr>
	</table>
	<table border='0' align='center' style='width:80%' bgcolor='".$mColors['table']."'>
		<tr>
			<td align='left' style='width:100%'>
			<center><p>&nbsp;&nbsp;[Resum de Ruta] </p></center>
";

$recepcionsNoAcceptades=0;

if($mPars['excloureProductesJaEntregats']*1==1)
{
	echo "<p class='nota' style='text-align:center;'>* s'exclouen els productes ja entregats </p>";
}
else
{
	echo "<p class='nota' style='text-align:center;'>* <b>no</b> s'exclouen els productes ja entregats </p>";
}

echo "	
			<table bgcolor='#FFFFFF' BORDER='0' align='center'  style='width:60%'>
		";
	$kgComandaGrups=0;	
	$kgComandaGrup=0;
	$kgComandaPuntEntrega=0;
	$kgComandaZona=0;
	$ctComandaGrups=0;
	$ctComandaGrup=0;
	$cont=0;

	if(count($mGrupsAmbComandaPerZonesPuntsEntrega)>0)
	{
		while(list($zona,$mGrupsAmbComandaPerPuntEntrega)=each($mGrupsAmbComandaPerZonesPuntsEntrega))
		{
			if($zona!='')
			{
				echo "
				<tr>
					<td bgcolor='#99DEE8'><p class='albara' style='font-size:20px;'><b>zona: ".$zona."</b></p></td>
					<td bgcolor='#99DEE8'><p class='albara'><b>Comanda</b></p></td>
					<td bgcolor='#99DEE8'><p class='albara'><b>Cost Transport</b></p></td>
				</tr>
				";
				while(list($refPuntEntrega,$mGrupsAmbComanda)=each($mGrupsAmbComandaPerPuntEntrega))
				{
					if(!isset($mPuntsEntrega[$refPuntEntrega]))
					{
						array_push($mProblemaAmbGrup,$refPuntEntrega);
					}
					else if($refPuntEntrega!='' && $refPuntEntrega!=0)
					{
					
					echo "
				<tr>
					<td bgcolor='#B4F1FA'>
					<table>
						<tr>
							<td>
							<p class='albara'>
							<b>Punt d'entrega:<br>".(urldecode($mPuntsEntrega[$refPuntEntrega]['nom']))."</b>
							</p>
							</td>
						</tr>
						<tr>
							<td>
							<p class='albara'>".(urldecode($mPuntsEntrega[$refPuntEntrega]['adressa']))."(".(urldecode($mPuntsEntrega[$refPuntEntrega]['municipi'])).")</p>
							</td>
						</tr>
						<tr>
							<td>
							<p class='albara'>".(urldecode($mPuntsEntrega[$refPuntEntrega]['gestor_magatzem']))." (".(urldecode($mPuntsEntrega[$refPuntEntrega]['mobil'])).")</p>
							</td>
						</tr>
					</table>
					</td>
					<td bgcolor='#B4F1FA'>
					<td bgcolor='#B4F1FA'>
				</tr>
					
					";
					while(list($refgrup,$mGrupAmbComanda)=each($mGrupsAmbComanda))
					{
						$mComanda=db_getComanda($refgrup,$db);
						if(count($mComanda)>0)
						{
							while(list($key,$mComandaProducte)=each($mComanda))
							{			
								
								if($mComandaProducte['quantitat']>0)
								{
									if(!isset($mProductes[$mComandaProducte['index']]) && $mComandaProducte['index']!='' && $mComandaProducte['index']!=0)
									{
													
										if(!array_key_exists($key,$mProblemaAmbProducte))
										{
											array_push($mProblemaAmbProducte,$mComandaProducte['index']);
										}
									}
									else
									{
										$quantitat=$mComandaProducte['quantitat'];
										if($mPars['excloureProductesJaEntregats']*1==1)
										{
											if(array_key_exists($index,$mProductesJaEntregats))
											{
												$quantitat-=$mProductesJaEntregats[$index]['rebut'];
											}
										}
										$kgComandaGrup+=$quantitat*$mProductes[$mComandaProducte['index']]['pes'];
										$ctComandaGrup+=$quantitat*$mProductes[$mComandaProducte['index']]['pes']*$mProductes[$mComandaProducte['index']]['cost_transport_intern_kg'];
									}
								}
							}
						}
			
						$mComandaCopiar=db_getComandaCopiar($refgrup,$db);
						//$mIntercanvisRebost=db_getIntercanvisRebost($db);
						//vd($mIntercanvisRebost);
		
						echo "
				<tr>
					<td  bgcolor='#eeeeee' ><p class='albara'>
					<p>
					<b>[".$cont."]</b>
					<br>
					<table>
						<tr>
							<td>
							";
							if($mRecepcions[$mGrupAmbComanda['ref']]['acceptada']==0)
							{
								echo "
							<p class='pAlertaNo2'><b>".(urldecode($mGrupAmbComanda['nom']))."</b><br> (".(urldecode($mGrupAmbComanda['municipi'])).")</p>
							<p class='pAlertaNo4'>Punt Entrega No Acceptat</p>
								";
								$recepcionsNoAcceptades++;
							}
							else
							{
								echo "
							<p><b>".(urldecode($mGrupAmbComanda['nom']))."</b><br> (".(urldecode($mGrupAmbComanda['municipi'])).")</p>
							<p class='pAlertaOk4'>Punt Entrega Acceptat</p>
								";
							}
							
							echo "
							</td>
							<td valign='top' align='right'>
							<p onClick=\"enviarFpars('vistaAlbara.php?gRef=".$mGrupAmbComanda['ref']."&op=totals','_blank');\" style='cursor:pointer;'>&nbsp;&nbsp;[<u>albar�</u>]&nbsp;&nbsp;</p>
							</td>
						</tr>
					</table>
					<br>".(urldecode($mGrupAmbComanda['gestor_magatzem']))." (".$mGrupAmbComanda['mobil'].")
					<br>
					&nbsp;
					</p>
					";
					
					echo "
					</td>
					<td  bgcolor='#eeeeee' ><p class='albara'>&nbsp;&nbsp;&nbsp;".(number_format($kgComandaGrup,2))."&nbsp;kg&nbsp;&nbsp;</p></td>
					<td  bgcolor='#eeeeee' ><p class='albara'>&nbsp;&nbsp;&nbsp;".(number_format($ctComandaGrup,2))."&nbsp;ums&nbsp;&nbsp;</p></td>
				</tr>
						";
						$kgComandaGrups+=$kgComandaGrup;
						$ctComandaGrups+=$ctComandaGrup;
						$kgComandaPuntEntrega+=$kgComandaGrup;
						$kgComandaGrup=0;
						$ctComandaGrup=0;
						$cont++;
					}
					reset($mGrupsAmbComanda);
					echo "
				<tr>
					<td bgcolor='#eeeeee' align='right'><p><i>Total kg Punt Entrega <b>".(urldecode($mPuntsEntrega[$refPuntEntrega]['nom']))."</b>: </i></p></td>
					<td bgcolor='#eeeeee'><p class='albara'><b>&nbsp;&nbsp;&nbsp;".(number_format($kgComandaPuntEntrega,2))."&nbsp;kg&nbsp;&nbsp;</b></p></td>
					<td bgcolor='#eeeeee'><p class='albara'></td>
				</tr>
					";
					$kgComandaZona+=$kgComandaPuntEntrega;
					$kgComandaPuntEntrega=0;
				}
				}
				reset($mGrupsAmbComandaPerPuntEntrega);
		
				echo "
				<tr>
					<td bgcolor='#cccccc' align='right'><p><i>Total kg zona <b>".$zona."</b>: </i></p></td>
					<td bgcolor='#cccccc'><p class='albara'><b>&nbsp;&nbsp;&nbsp;".(number_format($kgComandaZona,2))."&nbsp;kg&nbsp;&nbsp;</b></p></td>
					<td bgcolor='#cccccc'><p class='albara'></td>
				</tr>
				<tr>
					<td><p>&nbsp;</p></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><p>&nbsp;</p></td>
					<td></td>
					<td></td>
				</tr>
			";
			$kgComandaZona=0;
			}
			
		}
		reset($mGrupsAmbComandaPerZonesPuntsEntrega);
		
		echo "
				<tr>
					<th align='right'><p>Total kg Comandes: </p></th>
					<th  class='albara'><p class='albara'>&nbsp;&nbsp;&nbsp;".(number_format($kgComandaGrups,2))." kg</p></th>
					<td  class='albara'></td>
				</tr>
				<tr>
					<th align='right'><p>Total cost transport: </p></th>
					<td  class='albara'></td>
					<th><p class='albara'>&nbsp;&nbsp;&nbsp;".(number_format($ctComandaGrups,2))." ums</p></th>
				</tr>
			
		";
	}
	else
	{
		if($cG2=='')
		{
			echo "
				<tr>
					<td align='center'>
					<br><br>
					<p>
					No hi ha entregues CAC
					</p>
					</td>
				</tr>
			";
		}
		else
		{
			echo "
				<tr>
					<td align='center'>
					<br><br>
					<p>
					No hi ha entregues CAC en la vostra zona (".$cG2.").
					</p>
					</td>
				</tr>
			";
		}
	}	
		echo "
			</table>
			</td>
		</tr>
	</table>
";
if(count($mProblemaAmbProducte)>0)
{
	while(list($key,$val)=each($mProblemaAmbProducte))
	{
		$mProducte=db_getProducte($val,$db);
		$mProblemaAmbProducte[$key]=urldecode($mProducte['producte']);
	}
	reset($mProblemaAmbProducte);
	
	$problemaAmbProducte=implode('<br>',$mProblemaAmbProducte);
	echo "
	<br>
	<table style='border:1px solid red;' align='center' bgcolor='".$mColors['table']."'>
		<tr>
			<td align='center'>
			<p style='color:red;'>ATENCIO, detectat problema amb els productes:
			<br>
			<b>".$problemaAmbProducte."</b>
			<br>
			Es possible que aquests productes hagin estat
			<br>
			reservats, pero no estan actius i no s'estan oferint
			</p>
			</td>
		</tr>
	</table>
	";
}

	echo "
	<br>
	<table style='border:1px solid red;' align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td align='center'>
			<p>Recepcions no acceptades pel responsable del grup Punt Entrega:</p>
			</td>
			<td align='center'>
			<p style='color:red;'><b>".$recepcionsNoAcceptades."</b>
			</p>
			</td>
		</tr>
	</table>
	";

if(count($mProblemaAmbGrup)>0)
{
	while(list($key,$val)=each($mProblemaAmbGrup))
	{
		$mPars['grup_id']=$val;
		$mGrup=getGrup($db);
		$mProblemaAmbGrup[$key]=urldecode($mGrup['nom']);
	}
	reset($mProblemaAmbGrup);
	$problemaAmbGrup=implode('<br>',$mProblemaAmbGrup);
	echo "
	<br>
	<table style='border:1px solid red;' align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td align='center'>
			<p style='color:red;'>ATENCIO, detectat problema amb els grups:
			<br>
			<b>".$problemaAmbGrup."</b>
			<br>
			Es possible que aquest grup no hagi guardat mai la forma de pagament, i per defecte
			<br>
			surti com al seu punt d'entrega, quan potser no n'�s un
			</p>
			</td>
		</tr>
	</table>
	";
}
echo "
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
";
html_helpRecipient();

echo "	
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
</body>
</html>
";
	

?>

		