<?php

//------------------------------------------------------------------------
function db_getUsuarisAmbComandaResum2($db)
{
	global 	$mPars,
			$mProductes,
			$mParametres,
			$mGrupsRef,
			$mProblemaAmb,
			$mPropietatsPeriodesLocals,
			$mPeriodesLocalsInfo;


	$mUsuarisAmbComandaResum=array();

	$umsT=0;
	$ecosT=0;
	$eurosT=0;
	$pesT=0;

	$ctkTums=0;
	$ctkTecos=0;
	$ctkTeuros=0;
						
	$ctrTums=0;
	$ctrTecos=0;
	$ctrTeuros=0;
	
	$quantitatTotalGrup=0;


	if(!$result=mysql_query("select sum(estoc_previst*pes) from ".$mPars['taulaProductes']." WHERE llista_id='".$mPars['selLlistaId']."' AND actiu='1'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalGrup+=$mRow[0];
	}


	//echo "<br>select * from ".$mPars['taulaComandes']."  WHERE llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' AND usuari_id!='0' order by id DESC;";
	if(!$result=@mysql_query("select * from ".$mPars['taulaComandes']."  WHERE llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' AND usuari_id!='0' order by id DESC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";

		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$usuariId=$mRow['usuari_id'];
			if(!isset($mUsuarisAmbComandaResum[$usuariId]))
			{
				$mUsuarisAmbComandaResum[$usuariId]=array();
				$mUsuarisAmbComandaResum[$usuariId]['kgT']=0;
				$mUsuarisAmbComandaResum[$usuariId]['umsT']=0;
				$mUsuarisAmbComandaResum[$usuariId]['ecosT']=0;
				$mUsuarisAmbComandaResum[$usuariId]['eurosT']=0;
				$mUsuarisAmbComandaResum[$usuariId]['ctkTums']=0;
				$mUsuarisAmbComandaResum[$usuariId]['ctkTecos']=0;
				$mUsuarisAmbComandaResum[$usuariId]['ctkTeuros']=0;
				$mUsuarisAmbComandaResum[$usuariId]['umstFDC']=0;
				$mUsuarisAmbComandaResum[$usuariId]['ecostFDC']=0;
				$mUsuarisAmbComandaResum[$usuariId]['eurostFDC']=0;
				$mUsuarisAmbComandaResum[$usuariId]['totalUms']=0;
				$mUsuarisAmbComandaResum[$usuariId]['totalEcos']=0;
				$mUsuarisAmbComandaResum[$usuariId]['totalEuros']=0;
					
			}


				$mProductesComanda=explode(';',$mRow['resum']);

				for($i=0;$i<count($mProductesComanda);$i++)
				{
					$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
					$key=str_replace('producte_','',$mIndexQuantitat[0]);
					$quantitat=@$mIndexQuantitat[1];
				
					if(!isset($mProductes[$key]) && $key!='' && $key!=0)
					{
						if(!array_key_exists($key,$mProblemaAmb))
						{
							array_push($mProblemaAmb,$key);
						}
					}
					else if($key!='' && $key!=0)
					{
							$ums_=$mProductes[$key]['preu']*$quantitat;
							//echo "<br>id:".$key." producte:".$mProductes[$key]['producte']." preu:".$mProductes[$key]['preu']." quanti:".$quantitat;
							$umsT+=$ums_;

							$ecos_=$ums_*$mProductes[$key]['ms']/100;
							$ecosT+=$ecos_;
	
							$eurosT+=$ums_-$ecos_;

							$pesT+=$quantitat*$mProductes[$key]['pes'];
				
							$ctkTums+=$mProductes[$key]['pes']*$quantitat*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ctikLocal'];
							$ctkTecos+=$mProductes[$key]['pes']*$quantitat*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ctikLocal']*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_ctikLocal']/100;
							$ctkTeuros+=$mProductes[$key]['pes']*$quantitat*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ctikLocal']*(100-$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_ctikLocal'])/100;
		
					}
				
				$mUsuarisAmbComandaResum[$usuariId]['kgT']+=$pesT;
				$mUsuarisAmbComandaResum[$usuariId]['umsT']+=$umsT;
				$mUsuarisAmbComandaResum[$usuariId]['ecosT']+=$ecosT;
				$mUsuarisAmbComandaResum[$usuariId]['eurosT']+=$eurosT;
				$mUsuarisAmbComandaResum[$usuariId]['ctkTums']+=$ctkTums;
				$mUsuarisAmbComandaResum[$usuariId]['ctkTecos']+=$ctkTecos;
				$mUsuarisAmbComandaResum[$usuariId]['ctkTeuros']+=$ctkTeuros;
				$mUsuarisAmbComandaResum[$usuariId]['umstFDC']+=$umsT*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100;
				$mUsuarisAmbComandaResum[$usuariId]['ecostFDC']+=$umsT*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100)*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_fdLocal']/100);
				$mUsuarisAmbComandaResum[$usuariId]['eurostFDC']+=$umsT*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100)*((100-$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_fdLocal'])/100);
				$mUsuarisAmbComandaResum[$usuariId]['totalUms']+=$umsT+$ctkTums+$umsT*$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100;
				$mUsuarisAmbComandaResum[$usuariId]['totalEcos']+=$ecosT+$ctkTecos+$umsT*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100)*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_fdLocal']/100);
				$mUsuarisAmbComandaResum[$usuariId]['totalEuros']+=$eurosT+$ctkTeuros+$umsT*($mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['fdLocal']/100)*((100-$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['ms_fdLocal'])/100);

				$umsT=0;
				$ecosT=0;
				$eurosT=0;

				$pesT=0;

				$ctkTums=0;
				$ctkTecos=0;
				$ctkTeuros=0;
			}
		}
	}
	return $mUsuarisAmbComandaResum;
}


//------------------------------------------------------------------------------
function db_getPesos($db)
{
	global $mPars;

	$mPesos=array();
	
	if(!$result=mysql_query("select DISTINCT(pes) from ".$mPars['taulaComandes']."  WHERE llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' AND actiu='1' order by pes DESC",$db))
	{
		//echo "<br> 138 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			array_push($mPesos,$mRow[0]);
		}
	}
	
	return $mPesos;
}

//------------------------------------------------------------------------------
function db_getFormats($db)
{
	global $mPars;

	$mFormats=array();
	
	if(!$result=mysql_query("select DISTINCT(format) from ".$mPars['taulaComandes']."  WHERE llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."' AND actiu='1' order by format DESC",$db))
	{
		//echo "<br> 138 db.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'2','db.php');
	}
	else
  	{
		while($mRow=mysql_fetch_array($result,MYSQL_NUM))
		{
			array_push($mFormats,$mRow[0]);
		}
	}
	
	return $mFormats;
}

?>

		