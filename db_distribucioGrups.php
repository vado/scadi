<?php
//-----------------------------------------------------------------------
function db_getComandaPerProductors($db)
{
//*v36-17-12-15 - global
	global $mProductes,$mPars,$mUsuarisRef,$mInfoRutaAbastCsv,$mPerfilsRef,$mGrupsZones;
	


	$mComandaPerProductors=array();
	$mComandaTMP=array();
	$mInfoRutaAbastCsv_=array();
	
	$linCsv=0;
	
	if($mPars['selLlistaId']==0)
	{
		$andPeriodeLocal='';
	}
	else
	{
		$andPeriodeLocal=" AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mPars['sel_periode_comanda_local']."'";
	}

	if(!$result=mysql_query("select * from ".$mPars['taulaComandes']." where usuari_id!='0' ".$andPeriodeLocal,$db))
	{
		echo "<br> 27 db_distribucioGrups.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if($mRow['resum']!='')
			{
				$mProductesComanda=explode(';',$mRow['resum']);

				for($i=0;$i<count($mProductesComanda);$i++)
				{
					$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
		
					$id=str_replace('producte_','',$mIndexQuantitat[0]);
					$quantitat=@$mIndexQuantitat[1];
					if($id!='' && $id!=0)
					{
						$rebostRef=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
						if(!isset($mComandaTMP[$rebostRef]))
						{
							$mComandaTMP[$rebostRef]=array();
						}
						if(!isset($mComandaTMP[$rebostRef][$id]))
						{
							$mComandaTMP[$rebostRef][$id]=$quantitat;
						}
						else
						{
							$mComandaTMP[$rebostRef][$id]+=$quantitat;
						}
					}
				}
			}
		}
	}
	reset($mProductes);
	// ordenar per productor i afegir TMP i rebosts
	while(list($index,$mProducte)=each($mProductes))
	{
		if($mPars['paginacio']==1)
		{
			if(($mProducte['actiu']==1 && $mProducte['visible']==1) || substr_count($mProducte['tipus'],'especial')>0)
			{
			
			while(list($rebostRef,$mComandaRebost)=each($mComandaTMP))
			{
				while(list($id,$quantitat)=each($mComandaRebost))
				{
					if($id==$mProducte['id'])
					{
						$perfilId=substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'));
						/*
						if(!isset($mInfoRutaAbastCsv_[$perfilId]))
						{
							//$mInfoRutaAbastCsv_[$perfilId]['perfilId']=$perfilId;
							//$mInfoRutaAbastCsv_[$perfilId]['pesT']=0;
						}
						*/
						$mComandaPerProductors[$perfilId][$id][$rebostRef]=$quantitat;
						if(!isset($mComandaPerProductors[$perfilId][$id]['quantitatT']))
						{
							$mComandaPerProductors[$perfilId][$id]['quantitatT']=0;
							$mComandaPerProductors[$perfilId][$id]['producte']=$mProducte['producte'];
							$mComandaPerProductors[$perfilId][$id]['id']=$mProducte['id'];
							$mComandaPerProductors[$perfilId][$id]['tipus']=$mProducte['tipus'];
							$mComandaPerProductors[$perfilId][$id]['preu']=$mProducte['preu'];
							$mComandaPerProductors[$perfilId][$id]['ms']=$mProducte['ms'];
							$mComandaPerProductors[$perfilId][$id]['pes']=$mProducte['pes'];
							$mComandaPerProductors[$perfilId][$id]['ms_ctik']=$mProducte['ms_ctik'];
							$mComandaPerProductors[$perfilId][$id]['cost_transport_intern_kg']=$mProducte['cost_transport_intern_kg'];
						}

						if($rebostRef!='0')
						{
							$mComandaPerProductors[$perfilId][$id]['quantitatT']+=$quantitat;
							//$mInfoRutaAbastCsv_[$perfilId]['pesT']+=$quantitat*$mProducte['pes'];
						}
					}
				}
				reset($mComandaRebost);
				$linCsv++;
			}
			reset($mComandaTMP);
			}
		}
		else
		{
			if(($mProducte['actiu']==1) || substr_count($mProducte['tipus'],'especial')>0)
			{
			
			while(list($rebostRef,$mComandaRebost)=each($mComandaTMP))
			{
				while(list($id,$quantitat)=each($mComandaRebost))
				{
					if($id==$mProducte['id'])
					{
						$perfilId=substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'));
						/*
						if(!isset($mInfoRutaAbastCsv_[$perfilId]))
						{
							$mInfoRutaAbastCsv_[$perfilId]['perfilId']=$perfilId;
							$mInfoRutaAbastCsv_[$perfilId]['pesT']=0;
						}
						*/
						$mComandaPerProductors[$perfilId][$id][$rebostRef]=$quantitat;
						if(!isset($mComandaPerProductors[$perfilId][$id]['quantitatT']))
						{
							$mComandaPerProductors[$perfilId][$id]['quantitatT']=0;
							$mComandaPerProductors[$perfilId][$id]['producte']=$mProducte['producte'];
							$mComandaPerProductors[$perfilId][$id]['id']=$mProducte['id'];
							$mComandaPerProductors[$perfilId][$id]['tipus']=$mProducte['tipus'];
							$mComandaPerProductors[$perfilId][$id]['preu']=$mProducte['preu'];
							$mComandaPerProductors[$perfilId][$id]['ms']=$mProducte['ms'];
							$mComandaPerProductors[$perfilId][$id]['pes']=$mProducte['pes'];
							$mComandaPerProductors[$perfilId][$id]['ms_ctik']=$mProducte['ms_ctik'];
							$mComandaPerProductors[$perfilId][$id]['cost_transport_intern_kg']=$mProducte['cost_transport_intern_kg'];
							
						}
						if($rebostRef!='0' && isset($mPerfilsRef[$perfilId]))
						{
							$mComandaPerProductors[$perfilId][$id]['quantitatT']+=$quantitat;
							$sZona=@getSuperZona($mPerfilsRef[$perfilId]['zona']);
							if
							(
								!isset($mInfoRutaAbastCsv_[$sZona.'-'.$mPerfilsRef[$perfilId]['zona']])
							)
							{
								$mInfoRutaAbastCsv_[$sZona.'-'.$mPerfilsRef[$perfilId]['zona']]=array();
								$mInfoRutaAbastCsv_[$sZona.'-'.$mPerfilsRef[$perfilId]['zona']]=array();
							}
							if(!isset($mInfoRutaAbastCsv_[$sZona.'-'.$mPerfilsRef[$perfilId]['zona']][$perfilId])){$mInfoRutaAbastCsv_[$sZona.'-'.$mPerfilsRef[$perfilId]['zona']][$perfilId]=array();}
							if(!isset($mInfoRutaAbastCsv_[$sZona.'-'.$mPerfilsRef[$perfilId]['zona']][$perfilId]['pesT'])){$mInfoRutaAbastCsv_[$sZona.'-'.$mPerfilsRef[$perfilId]['zona']][$perfilId]['pesT']=0;}							
							$mInfoRutaAbastCsv_[$sZona.'-'.$mPerfilsRef[$perfilId]['zona']][$perfilId]['pesT']+=$quantitat*$mProducte['pes'];
						}
					}
				}
				reset($mComandaRebost);
			}
			reset($mComandaTMP);
			}
		}
	}
	reset($mProductes);
	
	$pesT=0;

//*v36-17-12-15 - condicio
	if(!isset($mPars['calculsRecolzament']))
	{
		$linCsv=3;
		$mInfoRutaAbastCsv[0][0]='';
		$mInfoRutaAbastCsv[0][1]=0;
		$mInfoRutaAbastCsv[1][0]='';

			@$mInfoRutaAbastCsv[2][0]='Szona';
			@$mInfoRutaAbastCsv[2][1]='zona';
			@$mInfoRutaAbastCsv[2][2]='Productora';
			@$mInfoRutaAbastCsv[2][3]='PesT';
			@$mInfoRutaAbastCsv[2][4]='usuaria';
			@$mInfoRutaAbastCsv[2][5]='email';
			@$mInfoRutaAbastCsv[2][6]='mobil';

		while(list($key,$mVal)=each($mInfoRutaAbastCsv_))
		{
			while(list($perfilId,$mPerfil)=each($mVal))
			{
				@$mInfoRutaAbastCsv[$linCsv][0]=getSuperZona($mPerfilsRef[$perfilId]['zona']);
				@$mInfoRutaAbastCsv[$linCsv][1]=$mPerfilsRef[$perfilId]['zona'];
				@$mInfoRutaAbastCsv[$linCsv][2]=urldecode($mPerfilsRef[$perfilId]['projecte']);
				@$mInfoRutaAbastCsv[$linCsv][3]=number_format($mPerfil['pesT'],2,',','');
				@$mInfoRutaAbastCsv[$linCsv][4]=$mUsuarisRef[$mPerfilsRef[$perfilId]['usuari_id']]['usuari'];
				@$mInfoRutaAbastCsv[$linCsv][5]=$mUsuarisRef[$mPerfilsRef[$perfilId]['usuari_id']]['email'];
				@$mInfoRutaAbastCsv[$linCsv][6]=$mUsuarisRef[$mPerfilsRef[$perfilId]['usuari_id']]['mobil'];
				$pesT+=@$mPerfil['pesT'];
				$linCsv++;
			}
		}
		reset($mInfoRutaAbastCsv_);

			@$mInfoRutaAbastCsv[$linCsv][0]='';
			@$mInfoRutaAbastCsv[$linCsv][1]='';
			@$mInfoRutaAbastCsv[$linCsv][2]='';
			@$mInfoRutaAbastCsv[$linCsv][3]='';
			@$mInfoRutaAbastCsv[$linCsv][4]='';
			@$mInfoRutaAbastCsv[$linCsv][5]='';
			@$mInfoRutaAbastCsv[$linCsv][6]='';
			$linCsv++;

			@$mInfoRutaAbastCsv[$linCsv][0]='';
			@$mInfoRutaAbastCsv[$linCsv][1]='';
			@$mInfoRutaAbastCsv[$linCsv][2]='';
			@$mInfoRutaAbastCsv[$linCsv][3]=number_format($pesT,2,',','').' kg';
			@$mInfoRutaAbastCsv[$linCsv][4]='';
			@$mInfoRutaAbastCsv[$linCsv][5]='';
			@$mInfoRutaAbastCsv[$linCsv][6]='';
			$linCsv++;

		//$mInfoRutaAbastCsv[1][0]=substr($mInfoRutaAbastCsv[1][0],0,strlen($mInfoRutaAbastCsv[1][0])-1);
	}

	return $mComandaPerProductors; 
}
//-----------------------------------------------------------------------
function db_getReservesAproductor($perfilId,$db)
{
	global $mPars;

	$mReservesAproductor=array();
	
	if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where actiu='1' AND estoc_previst!=estoc_disponible AND SUBSTRING_INDEX(productor,'-',1)='".$perfilId."'",$db))
	{
		//echo "<br> 231 db_distribucioGrups.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mReservesAproductor[$mRow['id']]=$mRow['estoc_previst']-$mRow['estoc_disponible'];
		}
	}
	
	return $mReservesAproductor;
}
//-----------------------------------------------------------------------
function db_getInventariPerProductors($db)
{
	global $mProductes,$mPars;

	
	$mInventariPerProductors=array();
	$mInventariTMP=array();
	$mInventaris=array();
	
	if(!$result=@mysql_query("select * from inventaris_".(substr($mPars['selRutaSufix'],0,2))." where periode_comanda='".$mPars['periode_comanda']."' order by id DESC",$db))
	{
		//echo "<br> 19".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			if(!isset($mInventaris[$mRow['magatzem_ref']])){$mInventaris[$mRow['magatzem_ref']]=$mRow;}
		}
		
		while(list($magatzemRef,$mInventari)=each($mInventaris))			
		{
			$mProductesInventari=explode(';',$mInventari['resum']);

			for($i=0;$i<count($mProductesInventari);$i++)
			{
				$mIndexQuantitat=explode(':',$mProductesInventari[$i]);
		
				$id=str_replace('producte_','',$mIndexQuantitat[0]);
				$quantitat=@$mIndexQuantitat[1];
				if($id!='' && $id!=0 && $quantitat>0)
				{
					if(!isset($mInventariTMP[$magatzemRef]))
					{
						$mInventariTMP[$magatzemRef]=array();
					}
					$mInventariTMP[$magatzemRef][$id]=$quantitat;
				}
			}
		}
		reset($mInventaris);
	}
		
	// ordenar per productor i afegir TMP i rebosts
	if($mPars['veureProductesDisponibles']==1)
	{
		if($mPars['paginacio']==1)
		{
		
		while(list($index,$mProducte)=each($mProductes))
		{
			if($mProducte['actiu']==1 && $mProducte['visible']==1)
			{
				while(list($magatzemRef,$mInventariMagatzem)=each($mInventariTMP))
				{
					while(list($id,$quantitat)=each($mInventariMagatzem))
					{
						if($id==$mProducte['id'])
						{
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id][$magatzemRef]=$quantitat;
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['producte']=$mProducte['producte'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['id']=$mProducte['id'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['tipus']=$mProducte['tipus'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['actiu']=$mProducte['actiu'];
						}
					}
					reset($mInventariMagatzem);
				}
				reset($mInventariTMP);
			}
		}
		reset($mProductes);
		}
		else
		{
		while(list($index,$mProducte)=each($mProductes))
		{
			if($mProducte['actiu']==1)
			{
				while(list($magatzemRef,$mInventariMagatzem)=each($mInventariTMP))
				{
					while(list($id,$quantitat)=each($mInventariMagatzem))
					{
						if($id==$mProducte['id'])
						{
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id][$magatzemRef]=$quantitat;
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['producte']=$mProducte['producte'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['id']=$mProducte['id'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['tipus']=$mProducte['tipus'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['actiu']=$mProducte['actiu'];
						}
					}
					reset($mInventariMagatzem);
				}
				reset($mInventariTMP);
			}
		}
		reset($mProductes);
		}
	}
	else
	{
		if($mPars['paginacio']==1)
		{
		while(list($index,$mProducte)=each($mProductes))
		{
			if($mProducte['visible']==1)
			{
				while(list($magatzemRef,$mInventariMagatzem)=each($mInventariTMP))
				{
					while(list($id,$quantitat)=each($mInventariMagatzem))
					{
						if($id==$mProducte['id'])
						{
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id][$magatzemRef]=$quantitat;
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['producte']=$mProducte['producte'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['id']=$mProducte['id'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['tipus']=$mProducte['tipus'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['actiu']=$mProducte['actiu'];
						}
					}
					reset($mInventariMagatzem);
				}
				reset($mInventariTMP);
			}
		}
		reset($mProductes);
		}
		else
		{
		while(list($index,$mProducte)=each($mProductes))
		{
				while(list($magatzemRef,$mInventariMagatzem)=each($mInventariTMP))
				{
					while(list($id,$quantitat)=each($mInventariMagatzem))
					{
						if($id==$mProducte['id'])
						{
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id][$magatzemRef]=$quantitat;
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['producte']=$mProducte['producte'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['id']=$mProducte['id'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['tipus']=$mProducte['tipus'];
							$mInventariPerProductors[substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))][$id]['actiu']=$mProducte['actiu'];
						}
					}
					reset($mInventariMagatzem);
				}
				reset($mInventariTMP);
		}
		reset($mProductes);
		}
	}
	
	return $mInventariPerProductors; 
}
//------------------------------------------------------------------------------
function db_getInfoFullRutaAbastimentCSV($db)
{
	global $mPars,$mPerfilsRef,$mInfoRutaAbastCsv;
	

	
	
	$dir0=getcwd();
	$dir="docs".$mPars['selRutaSufix'];
	if(!@chdir($dir))
	{
		mkdir($dir);
	}
	else
	{
		if(!chdir('reserves')){mkdir('reserves');}
		chdir($dir0);
	}
	@unlink("docs".$mPars['selRutaSufix']."/reserves/infoFullRutaAbastiment_".$mPars['usuari_id'].".csv");

	if(!$fp=fopen("docs".$mPars['selRutaSufix']."/reserves/infoFullRutaAbastiment_".$mPars['usuari_id'].".csv",'a'))
	{
		return false;
	}


	$mH1=array('info per a full ruta abastiment');
	fputcsv($fp, $mH1,',','"');

	
//*v36.2-TOT->TOTS
	if(isset($mPars['vProductor']) && $mPars['vProductor']!='' && $mPars['vProductor']!='TOTS')
	{
		$mH1=array("* nom�s es mostren els productes de la productora: ".(urldecode($mPerfilsRef[$mPars['vProductor']]['projecte'])));
	fputcsv($fp, $mH1,',','"');
	}

	if(isset($mPars['vCategoria']) && $mPars['vCategoria']!='' && $mPars['vCategoria']!='TOT')
	{
		$mH1=array("* nom�s es mostren els productes de 'categoria': ".$mPars['vCategoria']);
	fputcsv($fp, $mH1,',','"');
	}

	if(isset($mPars['vSubCategoria']) && $mPars['vSubCategoria']!='' && $mPars['vSubCategoria']!='TOT')
	{
		$mH1=array("* nom�s es mostren els productes de 'sub-categoria': ".$mPars['vSubCategoria']);
	fputcsv($fp, $mH1,',','"');
	}

	if(isset($mPars['etiqueta']) && $mPars['etiqueta']!='' && $mPars['etiqueta']!='CAP')
	{
		$mH1=array("* nom�s es mostren els productes de 'tipus': ".$mPars['etiqueta']);
	fputcsv($fp, $mH1,',','"');
	}

	if(isset($mPars['etiqueta2']) && $mPars['etiqueta2']!='' && $mPars['etiqueta2']!='CAP')
	{
		$mH1=array("* no es mostren els productes de 'tipus': ".$mPars['etiqueta2']);
	fputcsv($fp, $mH1,',','"');
	}

	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	
	$mH1=array(date('d/m/Y h:i'));
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	
	
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');

	ksort($mInfoRutaAbastCsv);
	
	while(list($key,$mVal)=each($mInfoRutaAbastCsv))
	{
		if($key>1)
    	{
			if(!fputcsv($fp, $mVal,',','"'))
			{
				return false;
			}
			$mH1=array('');
			fputcsv($fp, $mH1,',','"');
		}
	}	
	reset($mInfoRutaAbastCsv);
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');

	fclose($fp);
	return;
}



//*v36-15-11-26- funcio
//------------------------------------------------------------------------------
function db_getInfoFullRutaAbastimentCSV2($db)
{
	global $mPars,$mPerfilsRef,$mRebostsRef,$mInfoAnulacioReservesCSV;


	$dir0=getcwd();
	$dir="docs".$mPars['selRutaSufix'];
	if(!@chdir($dir))
	{
		mkdir($dir);
	}
	else
	{
		if(!chdir('reserves')){mkdir('reserves');}
		chdir($dir0);
	}
	@unlink("docs".$mPars['selRutaSufix']."/reserves/infoFullRutaAbastiment2_".$mPars['usuari_id'].".csv");

	if(!$fp=fopen("docs".$mPars['selRutaSufix']."/reserves/infoFullRutaAbastiment2_".$mPars['usuari_id'].".csv",'a'))
	{
		return false;
	}


	$mH1=array('info per a comunicat anul.laci� reserves');
	fputcsv($fp, $mH1,',','"');

	
//*v36.2-TOT->TOTS
	if(isset($mPars['vProductor']) && $mPars['vProductor']!='' && $mPars['vProductor']!='TOTS')
	{
		$mH1=array("* nom�s es mostren els productes de la productora: ".(urldecode($mPerfilsRef[$mPars['vProductor']]['projecte'])));
	fputcsv($fp, $mH1,',','"');
	}

	if(isset($mPars['vCategoria']) && $mPars['vCategoria']!='' && $mPars['vCategoria']!='TOT')
	{
		$mH1=array("* nom�s es mostren els productes de 'categoria': ".$mPars['vCategoria']);
	fputcsv($fp, $mH1,',','"');
	}

	if(isset($mPars['vSubCategoria']) && $mPars['vSubCategoria']!='' && $mPars['vSubCategoria']!='TOT')
	{
		$mH1=array("* nom�s es mostren els productes de 'sub-categoria': ".$mPars['vSubCategoria']);
	fputcsv($fp, $mH1,',','"');
	}

	if(isset($mPars['etiqueta']) && $mPars['etiqueta']!='' && $mPars['etiqueta']!='CAP')
	{
		$mH1=array("* nom�s es mostren els productes de 'tipus': ".$mPars['etiqueta']);
	fputcsv($fp, $mH1,',','"');
	}

	if(isset($mPars['etiqueta2']) && $mPars['etiqueta2']!='' && $mPars['etiqueta2']!='CAP')
	{
		$mH1=array("* no es mostren els productes de 'tipus': ".$mPars['etiqueta2']);
	}
	fputcsv($fp, $mH1,',','"');

	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	
	$mH1=array(date('d/m/Y hh:mm'));
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	
	
//*v36 30-12-15 marca1 1
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');

	//titols columnes:
	$mVal2=array('productora','producte','grup','usuari','quantitat','fPagamentEcosPerc','unitat_facturacio','email usuaria');
	if(!fputcsv($fp, $mVal2,',','"'))
	{
		return false;
	}
//*v36 30-12-15 marca1 2
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');

	while(list($productor,$mProductor_)=each($mInfoAnulacioReservesCSV))
	{
		while(list($producteId,$mProducte_)=each($mProductor_))
		{
			while(list($grupId,$mGrup_)=each($mProducte_))
			{
					$mPars['grup_id']=$grupId;
				db_getFormaPagament($db);//carrega fp a mPars
				
				while(list($usuariId,$mUsuari_)=each($mGrup_))
				{
//*v36-30-12-15 1 assignaci�
					$mVal2=array(urldecode($mPerfilsRef[$productor]['projecte']),urldecode($mUsuari_['producte']),@urldecode($mRebostsRef[$grupId]['nom']),urldecode($mUsuari_['usuari']),$mUsuari_['quantitat'],number_format($mPars['fPagamentEcosPerc'],2,',',''),(urldecode($mUsuari_['unitat_facturacio'])),$mUsuari_['email']);
					if(!fputcsv($fp, $mVal2,',','"'))
					{
						return false;
					}
					$mH1=array('');
					fputcsv($fp, $mH1,',','"');
				}
				reset($mGrup_);
			}
			reset($mProducte_);
		}
		reset($mProductor_);
	}	
	reset($mInfoAnulacioReservesCSV);
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	//$mInfoAnulacioReservesCSV[0][1].=' uts facturaci�';
	//fputcsv($fp, $mInfoAnulacioReservesCSV[0],',','"');
	//$mH1=array('');
	//fputcsv($fp, $mH1,',','"');
	//$mH1=array('');
	//fputcsv($fp, $mInfoAnulacioReservesCSV[1],',','"');

	fclose($fp);
	return;
}


?>

		