<?php
/*
taula trans ruta:

id
municipi origen
municipi desti
km
ruta
*/
/*
taula vehicles:

id
vehicle
conductor
carrega_max
matricula
estat
notes
*/
include "config.php";
include "db.php";
include "db_gestioVehicles.php";
include "eines.php";

$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}

$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
$mPars['sortBy']='id';
$mPars['ascdesc']='ASC';
$mPars['vUsuariId']='TOTS';
$mPars['vVehicle']='TOTS';
$mPars['vCategoria']='TOTS';
$mPars['vSubCategoria']='CAC';
$mPars['etiqueta']='TOTS';
$mPars['etiqueta2']='CAP';
$mPars['veureVehiclesActius']=1;
$mPars['vistaImpressio']=1;
$mPars['vh_sortBy']='id';

$opcio='';
$mUsuarisRef=db_getUsuarisRef($db);
$mMunicipis2Id=db_getMunicipis2Id($db);
$missatgeAlerta='';
$mTramsRuta=db_getTramsRuta($db);
$mParametresRuta=db_getParametresRuta($db);

$OhoraIniciRuta= new DateTime("2014-05-26 07:00:00");
$OhoraIniciRuta->setTimeZone(new DateTimeZone('Europe/Madrid'));

$mVehicles=db_getVehicles($db);
//vd($mVehicles);
//$mVehiclesId=db_getVehiclesId($db);


$vistaImpressio_=@$_POST['i_vistaImpressio'];
if(isset($vistaImpressio_)){$mPars['vistaImpressio']=$vistaImpressio_;}
if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
{
	if($mTramRuta['municipi_origen']=@$_POST['sel_nouOrigen'])
	{
		$mTramRuta['ordre']=@$_POST['i_nouOrdre'];
		$mTramRuta['municipi_desti']=@$_POST['sel_nouDesti'];
		$mTramRuta['km']=@$_POST['i_nouKm'];
		$mTramRuta['ruta']=urlencode(@$_POST['i_nouRuta']);
		$mTramRuta['hores']=urlencode(@$_POST['i_nouHores']);
		$mTramRuta['minuts']=urlencode(@$_POST['i_nouMinuts']);
		$mTramRuta['temps_parada']=urlencode(@$_POST['i_nouTempsParada']);
		$mTramRuta['vehicle_id']=@$_POST['sel_nouVehicleId'];
		$mTramRuta['notes']=urlencode(@$_POST['ta_nouNotes']);
	
		$missatgeAlerta=guardarNouTramRuta($mTramRuta,$db);
		$mTramsRuta=db_getTramsRuta($db);
		$mPars['vistaImpressio']=0;
	}
	else if($esborrarTramRutaId=@$_POST['i_esborrarTramRutaId'])
	{
		$missatgeAlerta=esborrarTramRuta($esborrarTramRutaId,$db);
		$mTramsRuta=db_getTramsRuta($db);
		$mPars['vistaImpressio']=0;
	}
	else if($editarTramRutaId=@$_POST['i_editarTramRutaId'])
	{
		$opcio='editar';
		$mTramRutaEditar=db_getTramRuta($editarTramRutaId,$db);
		$mPars['vistaImpressio']=0;
	}
	else if($guardarTramRuta=@$_POST['i_guardarTramRuta'])
	{
		$mGuardarTram['id']=@$_POST['i_guardarTramId'];
		$mGuardarTram['ordre']=@$_POST['i_guardarTramOrdre'];
		$mGuardarTram['data_hora']=@$_POST['i_guardarDataHora'];
		$mGuardarTram['municipi_origen']=@$_POST['sel_guardarTramOrigen'];
		$mGuardarTram['municipi_desti']=@$_POST['sel_guardarTramDesti'];
		$mGuardarTram['km']=@$_POST['i_guardarTramKm'];
		$mGuardarTram['ruta']=(urldecode(@$_POST['i_guardarTramRuta']));
		$mGuardarTram['hores']=urlencode(@$_POST['i_guardarTramHores']);
		$mGuardarTram['minuts']=urlencode(@$_POST['i_guardarTramMinuts']);
		$mGuardarTram['temps_parada']=urlencode(@$_POST['i_guardarTramTempsParada']);
		$mGuardarTram['actiu']=@$_POST['sel_guardarTramActiu'];
		$mGuardarTram['vehicle_id']=@$_POST['sel_guardarVehicleId'];
		$mGuardarTram['notes']=urlencode(@$_POST['ta_guardarNotes']);

		$missatgeAlerta=db_guardarTramRuta($mGuardarTram,$db);
		$mTramsRuta=db_getTramsRuta($db);
		$mPars['vistaImpressio']=0;
	}
	else if($activarTramRutaId=@$_POST['i_activarTramRutaId'])
	{
		$activarTramRuta=@$_POST['i_activarTramRuta'];
		db_activarTramRuta($activarTramRutaId,$activarTramRuta,$db);
		$mTramsRuta=db_getTramsRuta($db);
		$mPars['vistaImpressio']=0;
	}
	else if($mParametre['id']=@$_POST['i_parametreId'])
	{	
		$mParametre['valor']=@$_POST['i_parametreValor'];
		$mParametre['notes']=urlencode(@$_POST['i_parametreNotes']);

		$missatgeAlerta=db_guardarParametreRuta($mParametre,$db);
		$mParametresRuta=db_getParametresRuta($db);
	}
}

$mTramsActiusRuta=db_getTramsActiusRuta($db);
$mTramsRutaPerTram=db_getTramsRutaPerTram($db);
$mTextActivar[0]='-';
$mTextActivar[1]='+';


echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<title>"; echo $htmlTitolPags; echo " - Editar Ruta</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js1_4.js' CHARSET='ISO-8859-1'></SCRIPT>

<script type='text/javascript'  language='JavaScript'>

navegador();

var sel_rebost='';
missatgeAlerta=\"".$missatgeAlerta."\";
vistaImpressio_='".$mPars['vistaImpressio']."';
function mostraMissatgeAlerta()
{
	if(missatgeAlerta!='')
	{
		document.getElementById('td_missatgeAlerta').innerHTML=\"<p class='pAlertaNo3'>\"+missatgeAlerta+\"</p>\";
	}

}

</script>

</head>

<body onLoad=\"javascript: vistaImpressio();mostraMissatgeAlerta();\">
	<table align='center' style='width:90%;'>
		<tr>
			<th style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			<p>(".$mContinguts['form']['titol'].")</p>
			</th>
		</tr>
		<tr>
			<td style='width:100%;' align='center'>
			<table style='width:100%;' align='center'>
				<tr>
					<td align='left'>
					<p>".(date('d-m-Y'))."</p>			
					</td>
					<td align='right'>
";
$cb_vistaImpressioVisb='hidden';
if(($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord'))
{
	$cb_vistaImpressioVisb='inherit';
}
	echo "
					<p style='visibility:".$cb_vistaImpressioVisb.";' ><input type='checkbox' id='cb_vistaImpressio' value='".$mPars['vistaImpressio']."' onClick=\"javascript:canviarVista();\"> vista impressi�</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td id='td_missatgeAlerta' style='width:100%;' align='center'>
			</td>
		</tr>
	</table>

	<table border='0' align='center' style='width:90%;'>
		<tr>
			<td align='left' style='width:100%;'>
			<center><p>&nbsp;&nbsp;[Trams de Ruta] </p></center>
			<center><p>&nbsp;&nbsp;<b>Fulla de Ruta</b> </p></center>
			<table border='1' bgcolor='#ffffff' align='center'  style='width:100%;'>
				<tr>
					<th class='albara'>
					<p class='albara'>Id</p>
					</th>
					<th class='albara'>
					<p class='albara'>ordre</p>
					</th>
					<th class='albara'>
					<p class='albara'>reinici<br>temps</p>
					</th>
					<th class='albara'>
					<p class='albara'>origen/dest�</p>
					</th>
					<th class='albara'>
					<p class='albara'>hores:<br>sortida/arribada</p>
					</th>
					<th class='albara'>
					<p class='albara'>km</p>
					</th>
					<th class='albara'>
					<p class='albara'>hores</p>
					</th>
					<th class='albara'>
					<p class='albara'>minuts</p>
					</th>
					<th class='albara'>
					<p class='albara'>temps<br>parada<br>(minuts)</p>
					</th>
					<th class='albara'>
					<p class='albara'>ruta</p>
					</th>
					<th class='albara'>
					<p class='albara'>vehicle</p>
					</th>
					<th class='albara' style='width:200px;'>
					<p class='albara'>notes</p>
					</th>
					<th class='albara'>
					</th>
				</tr>
				";
				
				$totalHores=0;
				$totalMinuts=0;
				$totalKm=0;
				$totalTempsParada=0;
				$numBotonsEditar=0;
				$tempsParadaOld=0;
				if(count($mTramsActiusRuta)>0)
				{
					while(list($key,$mVal)=each($mTramsActiusRuta))
					{
						if(!isset($mVehicles[$mVal['vehicle_id']]['kilometratge']))
						{
							$mVehicles[$mVal['vehicle_id']]['kilometratge']=0;
						}
						$mVehicles[$mVal['vehicle_id']]['kilometratge']+=$mVal['km'];
						echo "
				<tr>
					<td class='albara' valign='top'>
					<p class='albara'>".$mVal['id']."</p>
					</td>

					<td class='albara' valign='top'>
					<p class='albara'>".$mVal['ordre']."</p>
					</td>
					
					<td class='albara' valign='top'>
					<p class='albara'>".$mVal['data_hora']."</p>
					</td>

					<td class='albara' valign='top'>
					<p class='albara'>origen: ".(urldecode($mMunicipis2Id[$mVal['municipi_origen']]['municipi']))."</p>
					<p class='albara'>desti: ".(urldecode($mMunicipis2Id[$mVal['municipi_desti']]['municipi']))."</p>
					</td>

					<td class='albara' valign='top'>
				";
				if($mVal['data_hora']=='')
				{
					echo "
					<p class='albara'>sortida:<br> ".(calculaHoresRuta(0,$tempsParadaOld))."</p>
					<p class='albara'>arribada:<br> ".(calculaHoresRuta($mVal['hores'],$mVal['minuts']))."</p>
					";
				}
				else
				{
					$tempsParadaOld=0;
					$mDataHora=explode('-',$mVal['data_hora']);
					$OhoraIniciRuta->setDate($mDataHora[0],$mDataHora[1],$mDataHora[2]);
					$OhoraIniciRuta->setTime($mDataHora[3],$mDataHora[4],$mDataHora[5]);
					$OhoraIniciRuta->setTimeZone(new DateTimeZone('Europe/Madrid'));
					
					echo "
					<p class='albara'>sortida:<br> ".(calculaHoresRuta(0,$tempsParadaOld))."</p>
					<p class='albara'>arribada:<br> ".(calculaHoresRuta($mVal['hores'],$mVal['minuts']))."</p>
					";
				}
					echo "
					</td>

					<td class='albara' valign='top'>
					<p class='albara'>".$mVal['km']."</p>
					</td>

					<td class='albara' valign='top'>
					<p class='albara'>".$mVal['hores']."</p>
					</td>
					
					<td class='albara' valign='top'>
					<p class='albara'>".$mVal['minuts']."</p>
					</td>
					
					<td class='albara' valign='top'>
					<p class='albara'>".$mVal['temps_parada']."</p>
					</td>

					<td class='albara' valign='top'>
					<p class='albara'>".(urldecode($mVal['ruta']))."</p>
					</td>

				";
					$colorBg=@substr(md5($mVal['vehicle_id']),0,6);
					//$colorFont=dechex(hexdec('FFFFFF')-hexdec($colorBg));
					$colorFont='ffffff';
					
					echo "
					<td  BGCOLOR='#".$colorBg."' valign='top'>
					<p style='color:#".$colorFont."; text-shadow: 1px 1px #000000;' >".@$mUsuarisRef[$mVehicles[$mVal['vehicle_id']]['usuari_id']]['usuari']."</p>
					</td>

					<td class='albara' valign='top'>
					<p class='albara'>".(urldecode($mVal['notes']))."</p>
					</td>

					<td class='albara' valign='top'>
					<input class='i_botoEditar' id='i_botoEditar_".$numBotonsEditar."' type='button' onClick=\"javascript: activarTramRuta('".$mVal['id']."','0');\" value='".$mTextActivar[0]."'>
					</td>
				</tr>
						";
						$numBotonsEditar++;
				
						$totalHores+=$mVal['hores'];
						$totalMinuts+=$mVal['minuts'];
						$totalKm+=$mVal['km'];
						$totalTempsParada+=$mVal['temps_parada'];
						$tempsParadaOld=+$mVal['temps_parada'];
					}
					reset($mTramsActiusRuta);
					$minuts=$totalHores*60+$totalMinuts+$totalTempsParada;
					$hores=floor($minuts/60);
					$minuts=$minuts-$hores*60;
					$totalHores=$hores;
					$totalMinuts=$minuts;
					
					echo "
				<tr>
					<td class='albara'>
					<p class='albara'></p>
					</td>

					<td class='albara'>
					<p class='albara'></p>
					</td>
					
					<td class='albara'>
					<p class='albara'></p>
					</td>

					<td class='albara'>
					<p class='albara'><b>totals</b></p>
					</td>

					<td class='albara'>
					<p class='albara'><b>".(floor($totalKm))." km</b></p>
					</td>

					<td class='albara'>
					<p class='albara'><b>".(floor($totalHores))." h.</b></p>
					</td>
					
					<td class='albara'>
					<p class='albara'><b>".(floor($totalMinuts))." min.</b></p>
					</td>
					
					<td class='albara'>
					</td>
					
					<td class='albara'>
					</td>

					<td class='albara'>
					</td>

					<td class='albara'>
					</td>

					<td class='albara'>
					</td>

				</tr>
					
				<tr>
					<td class='albara'>
					<p class='albara'></p>
					</td>

					<td class='albara'>
					<p class='albara'></p>
					</td>
					
					<td class='albara'>
					<p class='albara'></p>
					</td>

					<td class='albara'>
					<p class='albara'><b>cost km ruta<br>(".$mParametres['varCostTransport']['valor']." ums/km)</b></p>
					</td>

					<td class='albara'>
					<p class='albara'><b>".(number_format($totalKm*$mParametres['varCostTransport']['valor'],2))."<br>(ums)</b></p>
					</td>

					<td class='albara'>
					<p class='albara'></p>
					</td>
					
					<td class='albara'>
					<p class='albara'></p>
					</td>
					
					<td class='albara'>
					</td>
					
					<td class='albara'>
					</td>

					<td class='albara'>
					</td>

					<td class='albara'>
					</td>

					<td class='albara'>
					</td>
				</tr>

			</table>
					";
				}				
				else
				{
					echo "
			</table>
			<center><p>Encara no hi ha cap tram de ruta actiu</p></center>
					";
				}
				echo "
				<script>numBotonsEditar=".$numBotonsEditar.";</script>
			<br>&nbsp;
			";
						
			echo "
			<center><p>&nbsp;&nbsp;<b>Resum kilometratge vehicles</b> </p></center>
			<table border='1' align='center'>
				<tr>
					<th align='left'>
					<p>Vehicle</p/>
					</th>
					<th align='left'>
					<p>Total<br>Kilometres<br>ruta</p/>
					</th>
					<th align='left'>
					<p>kilometratge<br>(ums)</p/>
					</th>
				</tr>
				";
				while(list($key,$mVehicle)=each($mVehicles))
				{
					if(!isset($mVehicle['kilometratge'])){$mVehicle['kilometratge']=0;}

					$colorBg=@substr(md5($mVehicle['id']),0,6);
					//$colorFont=dechex(hexdec('FFFFFF')-hexdec($colorBg));
					$colorFont='ffffff';
					
					echo "
				<tr>
					<td align='left' bgcolor='#".$colorBg."'>
					<p style='color:#".$colorFont."; text-shadow: 1px 1px #000000;' >";
					if(@$mVehicle['usuari_id']==0)
					{
						echo @urldecode($mVehicle['vehicle']);
					}
					else
					{
						echo $mUsuarisRef[$mVehicle['usuari_id']]['usuari'];
					}
					echo "</p>
					</td>
					<td align='left'>
					<p>".$mVehicle['kilometratge']."</p/>
					</td>
					<td align='left'>
					<p>".($mVehicle['kilometratge']*$mParametres['varCostTransport']['valor'])." ums</p/>
					</td>
				</tr>
					";
				}
				reset($mVehicles);
				echo "
				</tr>
			</table>
			<br>
			
			
			<div id='d_parametresRuta' style='z-index:0; position:relative; top:0; visibility:inherit;'>
			<form id='f_parametresRuta' name='f_parametresRuta' method='post' action='tramsRuta.php' target='_self'>
			<center><p>&nbsp;&nbsp;<b>canviar Par�metres Ruta</b> </p></center>
			<table bgcolor='#dddddd' align='center'  style='width:100%;'>
				<tr>
					<th class='albara'>
					<p class='albara'>par�metre</p>
					</th>
					<th class='albara'>
					<p class='albara'>valor</p>
					</th>
					<th class='albara'>
					<p class='albara'>notes</p>
					</th>
					<th class='albara'>
					</th>
				</tr>
				";
				$i=0;
				while(list($key,$mParametreRuta)=each($mParametresRuta))
				{
					echo "
				<tr>
					<td class='albara'>
					<p class='albara'>".$mParametreRuta['parametre']."</p>
					<input id='i_parametreId_".$i."' name='i_parametreId' type='hidden' size='10' value='".$mParametreRuta['id']."'>
					</td>
					<td class='albara'>
					<input id='i_parametreValor_".$i."' name='i_parametreValor' type='text' size='10' value='".$mParametreRuta['valor']."'>
					</td>
					<td class='albara'>
					<textArea id='ta_parametreNotes_".$i."' name='ta_parametreNotes' Cols='10' rows='2'>".(urldecode($mParametreRuta['notes']))."</textArea>
					</td>
					<td class='albara'>
					<input type='button' onClick=\"javascript: if(!checkFormParametresRuta('".$i."')){mostraMissatgeAlerta();}else{document.getElementById('f_parametresRuta').submit();} \" value='guardar'>
					</td>
				</tr>
				";
				$i++;
				}
				reset($mParametresRuta);
				
				echo "
			</table>
			<input type='hidden' name='i_pars' value='".$parsChain."'>
			
			</form>	
			</div>
			";

	if($opcio=='')
	{
		echo "
			<div id='d_nouTram' style='z-index:0; position:relative;'>
			<form id='f_nouTram' name='f_nouTram' method='post' action='tramsRuta.php' target='_self'>
			<center><p>&nbsp;&nbsp;<b>Nou Tram</b> </p></center>
			<table id='t_nouTram' border='1' bgcolor='#ffffff' align='center'  style='width:100%;'>
				<tr>
					<th class='albara'>
					<p class='albara'>ordre</p>
					</th>
					<th class='albara'>
					<p class='albara'>origen/dest�</p>
					</th>
					<th class='albara'>
					<p class='albara'>km</p>
					</th>
					<th class='albara'>
					<p class='albara'>hores</p>
					</th>
					<th class='albara'>
					<p class='albara'>minuts</p>
					</th>
					<th class='albara'>
					<p class='albara'>temps<br>parada</p>
					</th>
					<th class='albara'>
					<p class='albara'>ruta</p>
					</th>
					<th class='albara'>
					<p class='albara'>vehicle</p>
					</th>
					<td class='albara'>
					</td>
				</tr>
				<tr>
					<td class='albara'  valign='top' align='left'>
					<input id='i_nouOrdre' name='i_nouOrdre' type='text' size='10' value=''>
					</td>
					<td class='albara' valign='top' align='left'>
					<p>origen:<br> 
					<select id='sel_nouOrigen' name='sel_nouOrigen'>
						";
						while(list($key2,$mVal2)=each($mMunicipis2Id))
						{
							echo "
					<option value='".$key2."'>".(urldecode($mVal2['municipi']))."</p>
							";
						}
						reset($mMunicipis2Id);
						echo "
					<option value='' selected></p>
					</select>
					</p>

					<p>dest�:<br> 
					<select id='sel_nouDesti' name='sel_nouDesti'>
						";
						while(list($key2,$mVal2)=each($mMunicipis2Id))
						{
							echo "
					<option value='".$key2."'>".(urldecode($mVal2['municipi']))."</p>
							";
						}
						reset($mMunicipis2Id);
						echo "
					<option value='' selected></p>
					</select>
					</p>
					<p><b>Notes:</b><br>
					<textarea id='ta_nouNotes' name='ta_nouNotes' cols='50' rows='4'></textarea>
					</p>
					</td>
					<td class='albara' valign='top' align='left'>
					<input id='i_nouKm' name='i_nouKm' type='text' size='10' value=''>
					</td>
					<td class='albara' valign='top' align='left'>
					<input id='i_nouHores' name='i_nouHores' type='text' size='10' value=''>
					</td>
					<td class='albara' valign='top' align='left'>
					<input id='i_nouMinuts' name='i_nouMinuts' type='text' size='10' value=''>
					</td>
					<td class='albara' valign='top' align='left'>
					<input id='i_nouTempsParada' name='i_nouTempsParada' type='text' size='10' value=''>
					</td>
					<td class='albara' valign='top' align='left'>
					<input id='i_nouRuta' name='i_nouRuta' type='text' size='10' value=''>
					</td>
					<td class='albara' valign='top' align='left'>
					<select id='sel_nouVehicleId' name='sel_nouVehicleId'>
						";
						while(list($key2,$mVal2)=each($mVehicles))
						{
							if($key2!='')
							{
								echo "
					<option value='".@$mVal2['id']."'>";
										if(@$mVal2['usuari_id']==0)
										{
											echo urldecode(@$mVal2['vehicle']);
										}
										else
										{
											echo $mUsuarisRef[$mVal2['usuari_id']]['usuari'];
										}
										echo "</option>
								";
							}
						}
						reset($mVehicles);
						echo "
					<option value='' selected></option>
					</select>
					</td>
					<td class='albara' valign='top' align='left'>
					<input type='button' onClick=\"javascript: if(!checkFormNouTramRuta()){mostraMissatgeAlerta();}else{document.getElementById('f_nouTram').submit();} \" value='enviar'>
					</td>
				</tr>
			</table>
			<input type='hidden' name='i_pars' value='".$parsChain."'>
			
			</form>	
			</div>
			";
		}
		else if ($opcio=='editar')
		{
			echo "
			<div id='d_nouTram' style='z-index:0; position:relative;'>
			<form id='f_guardarTram' name='f_guardarTram' method='post' action='tramsRuta.php' target='_self'>
			<center><p>&nbsp;&nbsp;<b>Guardar Tram</b> </p></center>
			<table id='t_nouTram' bgcolor='#ffffff' border='1' align='center'  style='width:100%;'>
				<tr>
					<th class='albara'>
					<p class='albara'>id</p>
					</th>
					<th class='albara'>
					<p class='albara'>ordre</p>
					</th>
					<th class='albara'>
					<p class='albara'>reinici<br>temps</p>
					</th>
					<th class='albara'>
					<p class='albara'></p>
					</th>
					<th class='albara'>
					<p class='albara'>km</p>
					</th>
					<th class='albara'>
					<p class='albara'></p>
					</th>
					<th class='albara'>
					<p class='albara'>ruta</p>
					</th>
					<th class='albara'>
					<p class='albara'>actiu</p>
					</th>
					<th class='albara'>
					<p class='albara'>vehicle</p>
					</th>
					<td class='albara'>
					</td>
				</tr>
				<tr>
					<td class='albara' valign='top' align='left'>
					<input readonly id='i_guardarTramId' name='i_guardarTramId' type='text' size='4' value='".$mTramRutaEditar['id']."'>
					</td>
					<td class='albara' valign='top' align='left'>
					<input class='valorEnEdicio'  id='i_guardarTramOrdre' name='i_guardarTramOrdre' type='text' size='4' value='".$mTramRutaEditar['ordre']."'>
					</td>
					<td class='albara' valign='top' align='left'>
					<input class='valorEnEdicio'  id='i_guardarDataHora' name='i_guardarDataHora' type='text' size='19' value='".$mTramRutaEditar['data_hora']."'>
					</td>
					<td class='albara' valign='top' align='left'>
					<p><b>origen: </b><br>
					<select class='valorEnEdicio'  id='sel_guardarTramOrigen' name='sel_guardarTramOrigen'>
						";
						$selected='selected';
						while(list($key2,$mVal2)=each($mMunicipis2Id))
						{
							if($key2==$mTramRutaEditar['municipi_origen'])
							{
								echo "
					<option selected value='".$key2."'>".(urldecode($mVal2['municipi']))." (".(urldecode($mVal2['comarca'])).")</p>
								";
								$selected='';
							}
							else
							{
								echo "
					<option value='".$key2."'>".(urldecode($mVal2['municipi']))." (".(urldecode($mVal2['comarca'])).")</p>
							";
							}
						}
						reset($mMunicipis2Id);
						echo "
					<option value='' ".$selected."></p>
					</select>
					</p>
					<p><b>dest�: </b><br>
					<select class='valorEnEdicio'  id='sel_guardarTramDesti' name='sel_guardarTramDesti'>
						";
						$selected='selected';
						while(list($key2,$mVal2)=each($mMunicipis2Id))
						{
							if($key2==$mTramRutaEditar['municipi_desti'])
							{
								echo "
					<option selected value='".$key2."'>".(urldecode($mVal2['municipi']))." (".(urldecode($mVal2['comarca'])).")</p>
								";
								$selected='';
							}
							else
							{
								echo "
					<option value='".$key2."'>".(urldecode($mVal2['municipi']))." (".(urldecode($mVal2['comarca'])).")</p>
							";
							}
						}
						reset($mMunicipis2Id);
						echo "
					<option value='' ".$selected."></p>
					</select>
					</p>
					<p><b>Notes:</b><br>
					<textarea id='ta_guardarNotes' name='ta_guardarNotes' cols='60'  rows='4'>".(urldecode($mTramRutaEditar['notes']))."</textarea>
					</p>
					</td>
					<td class='albara' valign='top' align='left'>
					<input class='valorEnEdicio'  id='i_guardarTramKm' name='i_guardarTramKm' type='text' size='4' value='".$mTramRutaEditar['km']."'>
					</td>
					<td class='albara' valign='top' align='left'>
					<p>hores:<input class='valorEnEdicio'  id='i_guardarTramHores' name='i_guardarTramHores' type='text' size='10' value='".$mTramRutaEditar['hores']."'>
					<br>
					minuts:<input class='valorEnEdicio'  id='i_guardarTramMinuts' name='i_guardarTramMinuts' type='text' size='10' value='".$mTramRutaEditar['minuts']."'>
					<br>
					TempsParada:<input class='valorEnEdicio'  id='i_guardarTramTempsParada' name='i_guardarTramTempsParada' type='text' size='10' value='".$mTramRutaEditar['temps_parada']."'>
					</p>
					</td>
					<td class='albara' valign='top' align='left'>
					<input class='valorEnEdicio'  id='i_guardarTramRuta' name='i_guardarTramRuta' type='text' size='10' value='".(urldecode($mTramRutaEditar['ruta']))."'>
					</td>
					<td class='albara' valign='top' align='left'>
					<select class='valorEnEdicio'  id='sel_guardarTramActiu' name='sel_guardarTramActiu'>
					";
					$selected='';
					$selected2='selected';
					if($mTramRutaEditar['actiu']){$selected='selected';$selected2='';}
					echo "
					<option ".$selected." value='1'>si</option>
					<option ".$selected2." value='0'>no</option>
					</select>
					</td>

					<td class='albara' valign='top' align='left'>
					<select class='valorEnEdicio'  id='sel_guardarVehicleId' name='sel_guardarVehicleId'>
					";
					$selected='';
					while(list($key2,$mVal2)=each($mVehicles))
					{
						if($mTramRutaEditar['vehicle_id']==$mVal2['id']){$selected='selected';}else{$selected='';}
						echo "
					<option ".$selected." value='".$mVal2['id']."'>";
										if($mVal2['usuari_id']==0)
										{
											echo urldecode($mVal2['vehicle']);
										}
										else
										{
											echo $mUsuarisRef[$mVal2['usuari_id']]['usuari'];
										}
										echo "</p>
							";
					}
					reset($mVehicles);
						
					echo "
					</select>
					</td>
					<td class='albara' valign='top' align='left'>
					<input type='button' onClick=\"javascript: if(!checkFormGuardarTramRuta()){mostraMissatgeAlerta();}else{document.getElementById('f_guardarTram').submit();} \" value='enviar'>
					</td>
				</tr>
			</table>
			<input type='hidden' name='i_pars' value='".$parsChain."'>
			
			</form>	
			</div>
			";
		}
		
		echo "					
			<div id='d_registreTrams' style='z-index:0; position:relative;'>
			<center><p>&nbsp;&nbsp;<b>Registre de Trams</b> </p></center>
			<table id='t_registreTrams' bgcolor='#ffffff' border='1' align='center'  style='width:90%;'>
				<tr>
					<th class='albara'>
					<p class='albara'>Id</p>
					</th>
					<th class='albara'>
					<p class='albara'>ordre</p>
					</th>
					<th class='albara'>
					<p class='albara'>reinici<br>temps</p>
					</th>
					<th class='albara'>
					<p class='albara'>origen/dest�</p>
					</th>
					<th class='albara'>
					<p class='albara'>km</p>
					</th>
					<th class='albara'>
					<p class='albara'>hores</p>
					</th>
					<th class='albara'>
					<p class='albara'>minuts</p>
					</th>
					<th class='albara'>
					<p class='albara'>temps<br>Parada</p>
					</th>
					<th class='albara'>
					<p class='albara'>ruta</p>
					</th>
					<td class='albara'>
					<p class='albara'>vehicle</p>
					</td>
					<td class='albara'>
					<p class='albara'>notes</p>
					</td>
					<td class='albara'>
					</td>
					<td class='albara'>
					</td>
					<td class='albara'>
					</td>
				</tr>
				";
								
				if(count($mTramsRutaPerTram)>0)
				{
					while(list($key,$mVal)=each($mTramsRutaPerTram))
					{
						echo "
				<tr>
					<td class='albara'>
					<p class='albara'>".$mVal['id']."</p>
					</td>

					<td class='albara'>
					<p class='albara'>".$mVal['ordre']."</p>
					</td>
					
					<td class='albara'>
					<p class='albara'>".$mVal['data_hora']."</p>
					</td>

					<td class='albara'>
					<p class='albara'><b>".(urldecode($mMunicipis2Id[$mVal['municipi_origen']]['municipi']))."</b>>>>".(urldecode($mMunicipis2Id[$mVal['municipi_desti']]['municipi']))."</p>
					</td>
					
					<td class='albara'>
					<p class='albara'>".$mVal['km']."</p>
					</td>

					<td class='albara'>
					<p class='albara'>".$mVal['hores']."</p>
					</td>

					<td class='albara'>
					<p class='albara'>".$mVal['minuts']."</p>
					</td>
					
					<td class='albara'>
					<p class='albara'>".$mVal['temps_parada']."</p>
					</td>

					<td class='albara'>
					<p class='albara'>".(urldecode($mVal['ruta']))."</p>
					</td>
					
					<td class='albara'>
					<p class='albara'>";
										if(isset($mVal['vehicle_id']) && $mVal['vehicle_id']!='')
										{
										if(!isset($mVehicles[$mVal['vehicle_id']]['usuari_id']) || $mVehicles[$mVal['vehicle_id']]['usuari_id']==0)
										{
											echo urldecode($mVehicles[$mVal['vehicle_id']]['vehicle']);
										}
										else
										{
											$usuariId=$mVehicles[$mVal['vehicle_id']]['usuari_id'];
											echo $mUsuarisRef[$usuariId]['usuari'];
										}
										}
										echo "</p>
					</td>
					
					<td class='albara'>
					<p class='albara'>".(urldecode($mVal['notes']))."</p>
					</td>
					
					<td class='albara'>
					";
					if($mVal['actiu']=='0')
					{
						echo "
					<input type='button' onClick=\"javascript: activarTramRuta('".$mVal['id']."','1');\" value='".$mTextActivar[1]."'>
						";
					}
					echo "
					</td>
					<td class='albara'>
					<input type='button' onClick=\"javascript:editarTramRuta('".$mVal['id']."');\" value='Ed'>
					</td>
					<td class='albara'>
					<input type='button' onClick=\"javascript:esborrarTramRuta('".$mVal['id']."');\" value='DEL'>
					</td>
				</tr>
						";
					}
					echo "
			</table>
					";
				}				
				else
				{
					echo "
			</table>
			<center><p>Encara no hi ha cap tram de ruta enregistrat</p></center>
					";
				}
				echo "
				</div>
			<br>&nbsp;
			</td>
		</tr>
	</table>			

";
$parsChain=makeParsChain($mPars);

echo "
<div style='visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='tramsRuta.php' target='_self'>
<input id='i_editarTramRutaId' name='i_editarTramRutaId' type='hidden' value=''>
<input id='i_activarTramRutaId' name='i_activarTramRutaId' type='hidden' value=''>
<input id='i_activarTramRuta' name='i_activarTramRuta' type='hidden' value=''>
<input id='i_esborrarTramRutaId' name='i_esborrarTramRutaId' type='hidden' value=''>
<input id='i_vistaImpressio' name='i_vistaImpressio' type='hidden' value='".$mPars['vistaImpressio']."'>
<input type='hidden' name='i_pars' value='".$parsChain."'>
</form>
</div>

</body>
</html>
";

?>

		