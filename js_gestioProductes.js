allowedCharsEmail='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.@';
allowedCharsCompteEcos_lletres='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
emailNeededCharacters='@.';
allowedCharsEstocPrevist='0123456789';
allowedCharsFormat='0123456789';
allowedCharsFloat='0123456789.';
allowedCharsCategories='A��BCDE��FGHI��JKLMNO��PQRSTU��VWXYZa��bc�de��fghi��jklmno��pqrstu��vwxyz0123456789- ';
allowedCharsProducte="A��BC�DE��FGHI��JKLMNO��PQRSTU��VWXYZa��bc�de��fghi��jklmno��pqrstu��vwxyz0123456789%-+':,.�� ()/";

function hihaCaractersNoPermesos(caracters,cadena,camp_)
{
   	allowedChars=caracters;
	missatgeAlerta1='';
	$_ok=0;
	
	//check for prohibited characters
    for(i=0;i<cadena.length;i++)
    {
       for(j=0;j<allowedChars.length;j++)
       {
           if(cadena.charAt(i)==allowedChars.charAt(j)){$_ok++;}
       }
	}
	if($_ok<cadena.length)
    {
		missatgeAlerta1="\nEl camp "+camp_+" cont� un valor incorrecte \n\n(caracters permesos: "+allowedChars+")\n";			
    }
	
	return missatgeAlerta1;
}
		
function checkFormGuardarProductes()
{
	isValid=false;
	missatgeAlerta='';

	tipus=document.getElementById('i_tipus').value;
	actiu=document.getElementById('i_actiu').value;
	
	value=document.getElementById('i_estoc_previst').value;
	unitatsReservades=document.getElementById('i_unitatsReservades').value;
	unitatsInventari=document.getElementById('i_unitatsInventari').value;

	if(value*1<unitatsReservades*1)
	{
		missatgeAlerta+="\nEl camp 'estoc_previst' ha de tenir un valor igual o major al nombre d'unitats reservades\n";			
	}
	
	if(selLlistaId==0)
	{
		if(value*1<unitatsInventari*1 && tipus.indexOf('dip�sit')>-1)
		{
			missatgeAlerta+="\nEl camp 'estoc_previst' ha de tenir un valor igual o major al nombre d'unitats que la CAC t� a l'inventari\n";			
		}
	}

	if(actiu*1==1 && value*1==0)
	{
		missatgeAlerta+="\nPer activar el producte el camp 'estoc_previst' ha de contenir un valor positiu\n";			
	}

	missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsEstocPrevist,value,'estoc_previst');
		
	value=document.getElementById('i_productor').value;
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'productor' no pot estar buit\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsProducte,value,'productor');
	}

	value=document.getElementById('i_format').value;
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'format' no pot estar buit\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFormat,value,'format');
	}

	value=document.getElementById('i_categoria0').value;
	value2=document.getElementById('i_ncategoria0').value;
	if(value.length==0 && value2.length==0)
	{
		missatgeAlerta+="\nEl camp 'categoria0' no pot estar buit\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsCategories,value2,'categoria0');
	}

	value=document.getElementById('i_categoria10').value;
	value2=document.getElementById('i_ncategoria10').value;
	if(value.length==0 && value2.length==0)
	{
		missatgeAlerta+="\nEl camp 'categoria10' no pot estar buit\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsCategories,value2,'categoria10');
	}

	value=document.getElementById('i_producte').value;
	valueL=value.toLowerCase();
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'producte' no pot estar buit\n";			
	}
	else if (valueL.indexOf('plantilla')>-1 && nivell!='sadmin' && nivell!='admin')
	{
		missatgeAlerta+="\n 'plantilla' es una paraula clau i no es pot usar en el nom del producte";
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsProducte,value,'producte');
	}

	value=document.getElementById('i_pes').value;
	if(value.length==0 || value==0)
	{
		if(tipus.indexOf('jjaa')==-1 && tipus.indexOf('gratu�t')==-1)
		{
			missatgeAlerta+="\nEl camp 'pes' ha de contenir un valor positiu\n";			
		}
	}
	else if(tipus.indexOf('jjaa')>-1)
	{
		missatgeAlerta+="\nPels productes de tipus 'jjaa', el camp 'pes' ha de ser igual a zero\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'pes');
	}

	value=document.getElementById('i_unitat_facturacio').value;
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'unitat_facturacio' no pot estar buit\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsProducte,value,'unitat_facturaci�');
	}

	value=document.getElementById('i_preu').value;
	if(value.length==0 || value<=0)
	{
		if(tipus.indexOf('jjaa')==-1 && tipus.indexOf('gratu�t')==-1)
		{
			missatgeAlerta+="\nEl camp 'preu' ha de contenir un valor positiu\n";			
		}
	}
	else if(tipus.indexOf('jjaa')>-1)
	{
		missatgeAlerta+="\nPels productes de tipus 'jjaa', el camp 'preu' ha de ser igual a zero\n";			
	}
	else if(tipus.indexOf('gratu�t')>-1)
	{
		missatgeAlerta+="\nPels productes de tipus 'gratu�t', el camp 'preu' ha de ser igual a zero\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'preu');
	}
	

	value=document.getElementById('i_ms').value;
	if(value.length==0)
	{
		missatgeAlerta+="\nEl camp 'ms' no pot estar buit\n";			
	}
	else
	{
		missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'ms');
		
		if(selLlistaId==0 && value*1<acord1)
		{
			missatgeAlerta+="\nEl camp 'ms' ha de contenir un valor igual o superior a l'acord establert amb la CAC \n\n(acord actual: "+acord1+" % moneda social)\n";			
		}
	}

	//value=document.getElementById('i_volum').value;
	//if(value.length==0)
	//{
	//	missatgeAlerta+="\nEl camp 'volum' no pot estar buit\n";			
	//}
	//else
	//{
		//missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'volum');
	//}
	
	//value=document.getElementById('i_estoc_disponible').value;
	//if(value.length==0)
	//{
	//	missatgeAlerta+="\nEl camp 'estoc_disponible' no pot estar buit\n";			
	//}
	//else
	//{
	//	missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsEstocPrevist,value,'estoc_disponible');
	//}

	
	value=document.getElementById('i_cost_transport_intern_kg').value;
	if(selLlistaId==0)
	{
		if(value.length==0)
		{
			missatgeAlerta+="\nEl camp 'cost_transport_intern_kg' no pot estar buit\n";			
		}
		else
		{
			missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsFloat,value,'cost_transport_intern_kg');
		}
	}
		
	if (missatgeAlerta.length==0)
	{
		return true;
	}

	alert(missatgeAlerta);
	
  return false;
}	


function checkActivacioRapida()
{
	missatgeAlerta='';
	
	numProductesActivacioRapida=document.getElementById('i_numProductesActivacioRapida').value;
	
	ids_ar_chain='';

	for(i2=0;i2<numProductesActivacioRapida*1;i2++)
	{
		i_ar_id_=document.getElementById('i_ar_id_'+i2).value;
		i_ar_ep_=document.getElementById('i_ar_ep_'+i2).value;
		i_ar_ep_mem=document.getElementById('i_ar_ep_'+i2+'_mem').value;
		cb_ar_ac_=document.getElementById('cb_ar_ac_'+i2).value;
		cb_ar_ac_mem=document.getElementById('cb_ar_ac_'+i2+'_mem').value;
		cb_ar_d_=document.getElementById('cb_ar_d_'+i2).value;
		cb_ar_d_mem=document.getElementById('cb_ar_d_'+i2+'_mem').value;
		if
		(
			i_ar_ep_*1!=i_ar_ep_mem*1
			||
			cb_ar_ac_*1!=cb_ar_ac_mem*1
			||
			cb_ar_d_*1!=cb_ar_d_mem*1
		)
		{
			if(i_ar_ep_*1==0 && cb_ar_ac_*1==1)
			{
				missatgeAlerta+="\nno es pot activar el producte id:"+i_ar_id_+" perqu� t� 'estoc_previst'=0";
			}
			else
			{
				missatgeAlerta+=hihaCaractersNoPermesos(allowedCharsEstocPrevist,i_ar_ep_,'id:'+i_ar_id_);
				ids_ar_chain+=','+i_ar_id_;
			}
		}
		else
		{
			document.getElementById('i_ar_id_'+i2).disabled=true;
			document.getElementById('i_ar_ep_'+i2).disabled=true;
			document.getElementById('cb_ar_ac_'+i2).disabled=true;
			document.getElementById('cb_ar_d_'+i2).disabled=true;
		}
	}
	if(missatgeAlerta=='')
	{
		document.getElementById('i_ids_ar_chain').value=ids_ar_chain;
		document.getElementById('f_activacioRapida').submit();
	}
	else
	{
		alert(missatgeAlerta);
	}
}


function resetActivacioRapida()
{
	missatgeAlerta='';
	
	numProductesActivacioRapida=document.getElementById('i_numProductesActivacioRapida').value;
	
	ids_ar_chain='';

	for(i2=0;i2<numProductesActivacioRapida*1;i2++)
	{
		i_ar_id_=document.getElementById('i_ar_id_'+i2).value;
		i_ar_ep_mem=document.getElementById('i_ar_ep_'+i2+'_mem').value;
		document.getElementById('i_ar_ep_'+i2).value=i_ar_ep_mem;
		cb_ar_ac_mem=document.getElementById('cb_ar_ac_'+i2+'_mem').value;
		document.getElementById('cb_ar_ac_'+i2).value=cb_ar_ac_mem;
		if(cb_ar_ac_mem*1==-1){document.getElementById('cb_ar_ac_'+i2).checked=false;}else{document.getElementById('cb_ar_ac_'+i2).checked=true;}
		cb_ar_d_mem=document.getElementById('cb_ar_d_'+i2+'_mem').value;
		document.getElementById('cb_ar_d_'+i2).value=cb_ar_d_mem
		if(cb_ar_d_mem*1==-1){document.getElementById('cb_ar_d_'+i2).checked=false;}else{document.getElementById('cb_ar_d_'+i2).checked=true;}
	}
}

function guardarProducte()
{
	document.getElementById('f_guardarProducte').submit();
	return;
}

function guardarNouProducte()
{
	document.getElementById('i_opcio').value='crear';
	document.getElementById('f_guardarProducte').submit();
	return;
}

function eliminarProducte(producteId)
{
	document.getElementById('i_opcio').value='eliminar';
	document.getElementById('f_guardarProducte').submit();

	return;
}

function anularReserves(producteId)
{
	document.getElementById('i_opcio').value='anularReserves';
	document.getElementById('f_guardarProducte').submit();

	return;
}

function peticioNovesPlantilles()
{
	numNovesPlantilles=document.getElementById('i_numPlantilles').value; 
	if(numNovesPlantilles*1>0)
	{
		comentari=document.getElementById('ta_comentariNP').value;
		document.getElementById('i_comentari').value=comentari; 
		enviarFpars('gestioProductes.php?numNpl='+numNovesPlantilles+'&pId='+perfilId,'_self')
	}
	else
	{
		alert("Atenci�: el nombre de plantilles que soliciteu ha de ser major que 0");
	}
	return;
}


function  peticioTransferenciaProductes()
{
	
	document.getElementById('f_transferirFitxesProductes').submit();	

	return;
}

function enviarPeticioAssociarAperfilAgrup()
{
	chainGrupsAssociar='';
	
	for (i=0, len=sel_grupsAssociar.options.length; i<len; i++)
	{
		opt=sel_grupsAssociar.options[i];
		if(opt.value!='' && opt.selected==true)
		{	
			chainGrupsAssociar+=opt.value+',';
		}
	}
	
	if(chainGrupsAssociar=='')
	{
		alert("Atenci�: cal seleccionar algun grup a qui enviar la petici�");
	}
	else
	{
		enviarFpars('gestioProductes.php?grAss='+chainGrupsAssociar+'&pId='+perfilId,'_self');
	}
	
	return;
}


function ocultarNotes()
{
	value=document.getElementById('ck_ocultarNotes').value;
	if(value==0)
	{
		document.getElementById('t_notes').style.visibility='hidden';
		document.getElementById('t_notes').style.position='absolute';
		//document.getElementById('d_totals').style.visibility='hidden';
		//document.getElementById('d_totals').style.position='absolute';
		document.getElementById('d_inventari').style.visibility='hidden';
		document.getElementById('d_inventari').style.position='absolute';
		
		document.getElementById('ck_ocultarNotes').value=1;
	}
	else
	{
		document.getElementById('t_notes').style.visibility='inherit';
		document.getElementById('t_notes').style.position='relative';
		//document.getElementById('d_totals').style.visibility='inherit';
		//document.getElementById('d_totals').style.position='relative';

		document.getElementById('d_inventari').style.visibility='inherit';
		document.getElementById('d_inventari').style.position='relative';

		document.getElementById('ck_ocultarNotes').value=0;
	}

	return;
}

function ocultarNotesGrup()
{
	value=document.getElementById('ck_ocultarNotes').value;
	if(value==0)
	{
		document.getElementById('t_notes').style.visibility='hidden';
		document.getElementById('t_notes').style.position='absolute';
		
		document.getElementById('ck_ocultarNotes').value=1;
	}
	else
	{
		document.getElementById('t_notes').style.visibility='inherit';
		document.getElementById('t_notes').style.position='relative';

		document.getElementById('ck_ocultarNotes').value=0;
	}

	return;
}


function mostrarFormAccio(mousex,mousey,opcio)
{
	
	ocultarFormAccio();
	document.getElementById('t_'+opcio).style.position='absolute';
	document.getElementById('t_'+opcio).style.top=mousey+30;
	document.getElementById('t_'+opcio).style.left=screen.width/4;
	document.getElementById('t_'+opcio).style.visibility='inherit';

	return;
}

function ocultarFormAccio()
{
		document.getElementById('t_segellEcocicProducte').style.position='absolute';
		document.getElementById('t_segellEcocicProducte').style.top=0;
		document.getElementById('t_segellEcocicProducte').style.left=0;
		document.getElementById('t_segellEcocicProducte').style.visibility='hidden';

		document.getElementById('t_associarPerfilAgrup').style.position='absolute';
		document.getElementById('t_associarPerfilAgrup').style.top=0;
		document.getElementById('t_associarPerfilAgrup').style.left=0;
		document.getElementById('t_associarPerfilAgrup').style.visibility='hidden';

		document.getElementById('t_transferirFitxes').style.position='absolute';
		document.getElementById('t_transferirFitxes').style.top=0;
		document.getElementById('t_transferirFitxes').style.left=0;
		document.getElementById('t_transferirFitxes').style.visibility='hidden';

		document.getElementById('t_peticioNovesPlantilles').style.position='absolute';
		document.getElementById('t_peticioNovesPlantilles').style.top=0;
		document.getElementById('t_peticioNovesPlantilles').style.left=0;
		document.getElementById('t_peticioNovesPlantilles').style.visibility='hidden';
		
		document.getElementById('t_activacioRapida').style.position='absolute';
		document.getElementById('t_activacioRapida').style.top=0;
		document.getElementById('t_activacioRapida').style.left=0;
		document.getElementById('t_activacioRapida').style.visibility='hidden';
		
		document.getElementById('t_pujarImatge').style.position='absolute';
		document.getElementById('t_pujarImatge').style.top=0;
		document.getElementById('t_pujarImatge').style.left=0;
		document.getElementById('t_pujarImatge').style.visibility='hidden';

	return;
}

function reordenarActivacioRapida(ordreAr)
{
	document.getElementById('i_ordreAr').value=ordreAr;
	enviarFpars('gestioProductes.php','_self');
	return;
}

function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
		
	return;
}

		