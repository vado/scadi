<?php
//------------------------------------------------------------------------------
function db_getIncidencies($filtre,$opt2,$select,$db)
{
	global $mPars,$mUsuarisGrupRef,$mRebost,$mRebostsRef,$mPropietatsPeriodesLocals;

	$missatgeAlerta='';
	$mIncidencies=array();
	$mWhere=array();
	$cont=0;
	$where1='';
	$where2='';
	$andF='';
	if($filtre)
	{
		//filtre recerca -----------------------
		
		////vSelUsuariIncd
		//vista usuari, es membre del grup
		

		if(isset($mPars['vSelUsuariIncd']))
		{
		 	if($mPars['vSelUsuariIncd']!='TOTS' && $mPars['vSelUsuariIncd']!='0')
			{
			 	$mWhere[$cont]=" sel_usuari_id='".$mPars['vSelUsuariIncd']."' ";
				$cont++;
			}
		}
		
		if(isset($mPars['vUsuariIncd']))
		{
		 	if($mPars['vUsuariIncd']!='TOTS')
		 	{
			 	$mWhere[$cont]=" usuari_id='".$mPars['vUsuariIncd']."' ";
				$cont++;
			}
		}

		if(isset($mPars['vTipusIncd']))
		{
		 	if($mPars['vTipusIncd']!='TOTS')
		 	{
			 	$mWhere[$cont]=" tipus='".$mPars['vTipusIncd']."' ";
				$cont++;
			}
		}

		if(isset($mPars['vProducteIdIncd']))
		{
		 	if($mPars['vProducteIdIncd']!='TOTS')
		 	{
			 	$mWhere[$cont]=" producte_id='".$mPars['vProducteIdIncd']."' ";
				$cont++;
			}
		}
		if(!isset($mPars['vEstatIncd']) || $mPars['vEstatIncd']=='')
		{
			if($opt2=='incd'){$mPars['vEstatIncd']='pendent';}
			else if($opt2=='abCa'){$mPars['vEstatIncd']='aplicat';}
		}
		
		if(isset($mPars['vEstatIncd']))
		{
			if(@$mPars['vEstatIncd']!='TOTS')
			{
				//if($opt2=='incd'){$mPars['vEstatIncd']='pendent';}
				//else if($opt2=='abCa'){$mPars['vEstatIncd']='aplicat';}
			 	$mWhere[$cont]=" estat='".$mPars['vEstatIncd']."' ";
				$cont++;
			}
		}

		if
		(
			isset($mPars['vRutaIncd'])
			&& 
			(
				!isset($mPars['vPeriodeLocalIncd'])
				|| 
				$mPars['vPeriodeLocalIncd']==''
			)
		)
		{
		
	 		if($mPars['vRutaIncd']!='TOTS')
	 		{
		 		$mWhere[$cont]=" ruta='".$mPars['vRutaIncd']."' ";
				$cont++;
			}
		}
		else if(isset($mPars['vPeriodeLocalIncd']))
		{
		 	if($mPars['vPeriodeLocalIncd']!='TOTS')
		 	{
			 	$mWhere[$cont]=" ruta='".$mPars['vPeriodeLocalIncd']."' ";
				$cont++;
			}
		}
		if(isset($mPars['vGrupIncd']))
		{
		 	if($mPars['vGrupIncd']!='TOTS' && $mPars['vGrupIncd']!='0')
		 	{
			 	$mWhere[$cont]=" grup_id='".$mPars['vGrupIncd']."' ";
				$cont++;
			}
		}


		$where1=implode(' AND ',$mWhere);
		if(strlen($where1)>0){$where1=' WHERE '.$where1;}
	
		// base recerca -------------------------
	
		if($mPars['selLlistaId']=='0')
		{
		
		if($mPars['grup_id']==0) //CAC
		{
			if($opt2=='incd')
			{
				$where2=" tipus!='abonamentCAC' AND tipus!='abonamentGrup' AND tipus!='carrecCAC' AND tipus!='carrecGrup'";
			}
			else if($opt2=='abCa')
			{
				$where2=" tipus='abonamentCAC' OR tipus='abonamentGrup' OR tipus='carrecCAC' OR tipus='carrecGrup'";
			}
		}
		else if($mPars['selUsuariId']==0)//GRUP
		{
			if($opt2=='incd')
			{
				$where2="  tipus!='abonamentCAC' AND tipus!='abonamentGrup' AND tipus!='carrecCAC' AND tipus!='carrecGrup'";
			}
			else if($opt2=='abCa')
			{
				$where2="  (tipus='abonamentCAC' OR tipus='carrecCAC' OR tipus='abonamentGrup' OR tipus='carrecGrup')";
			}	
		}
		else if($mPars['selUsuariId']!=0)//Usuari
		{
			if($opt2=='incd')
			{
				$where2="   tipus!='abonamentCAC' AND tipus!='abonamentGrup' AND tipus!='carrecCAC' AND tipus!='carrecGrup'";
			}
			else if($opt2=='abCa')
			{
				$where2=" (tipus='abonamentCAC' OR tipus='carrecCAC' OR 'abonamentGrup' OR tipus='carrecGrup')";
			}
		}
		} //listes locals
		else
		{
		
			if($opt2=='incd')
			{
				$where2=" tipus!='abonamentCAC' AND tipus!='abonamentGrup' AND tipus!='carrecCAC' AND tipus!='carrecGrup'";
			}
			else if($opt2=='abCa')
			{
				$where2=" tipus='abonamentGrup' OR tipus='carrecGrup'";
			}
		}
	}
	if($where1!='')
	{
		$andF='AND';
	}

	if($where1=='' && $where2=='')
	{
		$andL="WHERE llista_id='".$mPars['selLlistaId']."'";
	}
	else
	{
		$andL=" AND llista_id='".$mPars['selLlistaId']."'";
	}
	
	
	//echo "<br>select ".$select." from ".$mPars['taulaIncidencies']." ".$where1." ".$andF." ".$where2." ".$andL." order by id DESC";
	$result=mysql_query("select ".$select." from ".$mPars['taulaIncidencies']." ".$where1." ".$andF." ".$where2." ".$andL." order by id DESC",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		//$vEstatIncd_mem=$mPars['vEstatIncd'];
		//$mPars['vEstatIncd']='pendent';
	$i=0;
	while($mRow=@mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mIncidencies[$i]=$mRow;
		$i++;
	}

//*v36.4- 2 condicions
	if($filtre && $mPars['selLlistaId']==0)
	{
		//afegir filtre de Szona
		$mIncidencies_=array();
		if(isset($mPars['vSzonaIncd']) && $mPars['vSzonaIncd']!='TOTS')
		{
			$i=0;
			while(list($key,$mIncidencia)=each($mIncidencies))
			{
				$mCategoriesGrup=getCategoriesGrup($mRebostsRef[$mIncidencia['grup_id']]);
				$sZona=getSuperZona($mCategoriesGrup[2]);
				if($sZona==$mPars['vSzonaIncd'])
				{
					$mIncidencies_[$i]=$mIncidencia;
					$i++;
				}
			}
			reset($mIncidencies);
			$mIncidencies=$mIncidencies_;
			unset($mIncidencies_);
			$mIncidencies_=array();
		}

		//afegir filtre de Szona
		if(isset($mPars['vZonaIncd']) && $mPars['vZonaIncd']!='TOTS')
		{
			$i=0;
			while(list($key,$mIncidencia)=each($mIncidencies))
			{
				$mCategoriesGrup=getCategoriesGrup($mRebostsRef[$mIncidencia['grup_id']]);
				if($mCategoriesGrup[2]==$mPars['vZonaIncd'])
				{
					$mIncidencies_[$i]=$mIncidencia;
					$i++;
				}
			}
			reset($mIncidencies);
			$mIncidencies=$mIncidencies_;
		}
	}

	return $mIncidencies;
}


//------------------------------------------------------------------------------
function db_afegirIncidencia($mIncidencia,$db)
{
	global $mPars,$mUsuari,$mSelUsuari;

	$missatgeAlerta='';
	
	$mRow2=false;
	
	//evitar reenviament form
	$result=mysql_query("select * from ".$mPars['taulaIncidencies']."  where data='".$mIncidencia['data']."' and usuari_id='".$mUsuari['id']."' and grup_id='".$mIncidencia['grup_id']."' and tipus='".$mIncidencia['tipus']."' and producte_id='".$mIncidencia['producte_id']."' and demanat='".$mIncidencia['demanat']."' and rebut='".$mIncidencia['rebut']."' and comentaris='".$mIncidencia['comentaris']."' and tipus='".$mIncidencia['tipus']."' and ruta='".$mIncidencia['ruta']."' and estat='pendent' AND llista_id='".$mPars['selLlistaId']."'",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

	if($mIncidencia['tipus']=='jaEntregat' || $mIncidencia['tipus']=='pendentEntrega')
	{
		//evitar reenviament d'incidencia de tipus jaEntregat sobre el mateix producte i la mateixa comanda
		$result=mysql_query("select * from ".$mPars['taulaIncidencies']."  where grup_id='".$mIncidencia['grup_id']."' and producte_id='".$mIncidencia['producte_id']."' and tipus='jaEntregat' and estat='pendent'  AND llista_id='".$mPars['selLlistaId']."'",$db);
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mRow2=mysql_fetch_array($result,MYSQL_ASSOC);
	}
	
	if($mIncidencia['tipus']=='jaEntregat'){$estat='resolta';}else{$estat='pendent';}

	if($mRow===false && $mRow2===false)
	{
		//guardar incidencia
		if(!$result=mysql_query("insert into ".$mPars['taulaIncidencies']." values ('','".$mIncidencia['data']."','".$mUsuari['usuari']."','".$mUsuari['id']."','".$mSelUsuari['id']."','".$mIncidencia['grup_id']."','".$mIncidencia['tipus']."','".$mIncidencia['producte_id']."','".$mIncidencia['demanat']."','".$mIncidencia['rebut']."','".$mIncidencia['comentaris']."','".$estat."','".$mPars['selRutaSufix']."','".$mPars['selLlistaId']."')",$db))
		{
			//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut afegir la incid�ncia</p>";
		}
		else
		{
			$missatgeAlerta="<p  class='pAlertaOk'>la incid�ncia s'ha afegit correctament</p>";
		}
	}
	else
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut guardar la incid�ncia perqu� ja n'existeix alguna que �s incompatible amb aquesta</p>";
	}

	return $missatgeAlerta;
}

//------------------------------------------------------------------------------
function db_afegirIncidenciaLocal($mIncidencia,$db)
{
	global $mPars,$mUsuari,$mSelUsuari;

	$missatgeAlerta='';
	
	$mRow2=false;
	
	//evitar reenviament form
	$result=mysql_query("select * from ".$mPars['taulaIncidencies']."  where data='".$mIncidencia['data']."' and usuari_id='".$mUsuari['id']."' and grup_id='".$mIncidencia['grup_id']."' and tipus='".$mIncidencia['tipus']."' and producte_id='".$mIncidencia['producte_id']."' and demanat='".$mIncidencia['demanat']."' and rebut='".$mIncidencia['rebut']."' and comentaris='".$mIncidencia['comentaris']."' and tipus='".$mIncidencia['tipus']."' and ruta='".$mIncidencia['ruta']."' and estat='pendent' AND llista_id='".$mPars['selLlistaId']."'",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);

	if($mIncidencia['tipus']=='jaEntregat' || $mIncidencia['tipus']=='pendentEntrega')
	{
		//evitar reenviament d'incidencia de tipus jaEntregat sobre el mateix producte i la mateixa comanda
		$result=mysql_query("select * from ".$mPars['taulaIncidencies']."  where grup_id='".$mIncidencia['grup_id']."' and producte_id='".$mIncidencia['producte_id']."' and tipus='jaEntregat' and estat='pendent'  AND llista_id='".$mPars['selLlistaId']."'",$db);
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mRow2=mysql_fetch_array($result,MYSQL_ASSOC);
	}
	
	if($mIncidencia['tipus']=='jaEntregat'){$estat='resolta';}else{$estat='pendent';}

	if($mRow===false && $mRow2===false)
	{
		//guardar incidencia
		if(!$result=mysql_query("insert into ".$mPars['taulaIncidencies']." values ('','".$mIncidencia['data']."','".$mUsuari['usuari']."','".$mUsuari['id']."','".$mSelUsuari['id']."','".$mIncidencia['grup_id']."','".$mIncidencia['tipus']."','".$mIncidencia['producte_id']."','".$mIncidencia['demanat']."','".$mIncidencia['rebut']."','".$mIncidencia['comentaris']."','".$estat."','".$mIncidencia['ruta']."','".$mPars['selLlistaId']."')",$db))
		{
			//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
			$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut afegir la incid�ncia</p>";
		}
		else
		{
			$missatgeAlerta="<p  class='pAlertaOk'>la incid�ncia s'ha afegit correctament</p>";
		}
	}
	else
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut afegir la incid�ncia perqu� ja n'existeix alguna que �s incompatible amb aquesta</p>";
	}

	return $missatgeAlerta;
}

//------------------------------------------------------------------------------
function db_eliminarIncidencia($inId,$db)
{
	global $mPars,$mUsuari;

	$missatgeAlerta='';

	if(!$result=mysql_query("select * from ".$mPars['taulaIncidencies']." where id='".$inId."'   AND llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut llegir la incid�ncia</p>";
		
		return $missatgeAlerta;
	}

	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
					
	if
	(	
		$mPars['usuari_id']==$mRow['usuari_id']
		|| 
		$mPars['nivell']=='sadmin' 
		|| 
		$mPars['nivell']=='admin'
		||
		$mPars['nivell']=='coord'
	)
	{
		if($mRow['estat']=='resolta' || $mRow['estat']=='fet')
		{
			if(!$result=mysql_query("delete from ".$mPars['taulaIncidencies']." where id='".$inId."'   AND llista_id='".$mPars['selLlistaId']."'",$db))
			{
				//echo "<br> 147 db_incidencies.php ".mysql_errno() . ": " . mysql_error(). "\n";
				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut eliminar la incid�ncia</p>";
			}	
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk'>la incid�ncia s'ha eliminat correctament</p>";
			}
		}
		else if($mRow['estat']=='pendent')
		{
			$incdComentari=(urlencode("<p class='p_micro' style='color:#00".(substr(MD5($mUsuari['usuari'].$mUsuari['id']),0,4)).";'><b>[".(date('d/m/Y H:i')).", autor: ".$mUsuari['usuari'].", ".$mPars['usuari_id'].", ".$mPars['nivell'].", AUTO]</b>Resolta</p>"));
			
			if(!$result=mysql_query("update ".$mPars['taulaIncidencies']." set estat='resolta',comentaris=CONCAT(comentaris,'".$incdComentari."') where id='".$inId."'   AND llista_id='".$mPars['selLlistaId']."'",$db))
			{
				//echo "<br> 159 db_incidencies.php ".mysql_errno() . ": " . mysql_error(). "\n";
				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut resoldre de la incid�ncia</p>";
			}	
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk'>la incid�ncia s'ha resolt correctament</p>";
			}
		}
	}
	else
	{
		$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha perm�s eliminar la incid�ncia.</p>";
	}

	return $missatgeAlerta;
}

//------------------------------------------------------------------------------
function db_guardarIncidencia($inComentaris,$inId,$db)
{
	global $mPars;
	
	if(!$result=mysql_query("update ".$mPars['taulaIncidencies']." set comentaris='".$inComentaris."' where id='".$inId."'  AND llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut guardar la incid�ncia</p>";
	}
	else
	{
		$missatgeAlerta="<p  class='pAlertaOk'>la incid�ncia s'ha guardat correctament</p>";
	}
	
	return $missatgeAlerta;
}

//------------------------------------------------------------------------------
function db_aplicarIncidencia($inId,$db)
{
	global $mPars;
	
	if(!$result=mysql_query("update ".$mPars['taulaIncidencies']." set estat='aplicat' where id='".$inId."'  AND llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut aplicar la incid�ncia</p>";
	}
	else
	{
		$missatgeAlerta="<p  class='pAlertaOk'>la incid�ncia s'ha aplicat correctament</p>";
	}
	
	return $missatgeAlerta;
}
//------------------------------------------------------------------------------
function db_noAplicarIncidencia($inId,$db)
{
	global $mPars;
	
	if(!$result=mysql_query("update ".$mPars['taulaIncidencies']." set estat='noAplicat' where id='".$inId."'  AND llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut desaplicar la incid�ncia</p>";
	}
	else
	{
		$missatgeAlerta="<p  class='pAlertaOk'>la incid�ncia s'ha desaplicat correctament</p>";
	}
	
	return $missatgeAlerta;
}

//------------------------------------------------------------------------------
function db_aplicarAbonament($mAbonament,$db)
{
	global $mPars,$mUsuari;
	
	$missatgeAlerta='';
	
	//evitar reenviament form
	//echo "<br>select * from ".$mPars['taulaIncidencies']."  WHERE data='".$mAbonament['data']."' AND sel_usuari_id='".$mAbonament['usuari']."' AND usuari_id='".$mAbonament['autor']."' AND grup_id='".$mAbonament['grup']."' AND tipus='".$mAbonament['tipus']."' and estat='aplicat' and  comentaris='".$mAbonament['comentaris']."'  AND llista_id='".$mPars['selLlistaId']."'";
	$result=mysql_query("select * from ".$mPars['taulaIncidencies']."  WHERE data='".$mAbonament['data']."' AND sel_usuari_id='".$mAbonament['usuari']."' AND usuari_id='".$mAbonament['autor']."' AND grup_id='".$mAbonament['grup']."' AND tipus='".$mAbonament['tipus']."' and estat='aplicat' and  comentaris='".$mAbonament['comentaris']."'  AND llista_id='".$mPars['selLlistaId']."'",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	if($mRow===false)
	{
		//guardar incidencia
		if($mPars['selLlistaId']==0)
		{
			//guardar incidencia
			if(!$result=mysql_query("insert into ".$mPars['taulaIncidencies']." values ('','".$mAbonament['data']."','".$mUsuari['usuari']."','".$mAbonament['autor']."','".$mAbonament['usuari']."','".$mAbonament['grup']."','".$mAbonament['tipus']."','".$mAbonament['ecos']."','".$mAbonament['eb']."','".$mAbonament['euros']."','".$mAbonament['comentaris']."','aplicat','".$mAbonament['ruta']."','".$mPars['selLlistaId']."')",$db))
			{
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut aplicar l'abonament</p>";
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk'>l'abonament s'ha aplicat correctament</p>";
			}
		}
		else
		{
			//guardar incidencia
			if(!$result=mysql_query("insert into ".$mPars['taulaIncidencies']." values ('','".$mAbonament['data']."','".$mUsuari['usuari']."','".$mAbonament['autor']."','".$mAbonament['usuari']."','".$mAbonament['grup']."','".$mAbonament['tipus']."','".$mAbonament['ecos']."','".$mAbonament['eb']."','".$mAbonament['euros']."','".$mAbonament['comentaris']."','aplicat','".$mAbonament['ruta']."','".$mPars['selLlistaId']."')",$db))
			{
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut aplicar l'abonament</p>";
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk'>l'abonament s'ha aplicat correctament</p>";
			}
		}
		
	}

	return $missatgeAlerta;
}

//-------------------------------------------------------------
function db_anularReservesUsuari($mAr,$db)
{
	global $mPars,$mUsuarisRef,$mRebostsRef,$mPeriodesLocalsInfo,$mPropietatsPeriodeLocal,$mParametres,$mParams;
	
	$missatgeAlerta='';
	$unitatsReservades_=0;
	$unitatsReservades=0;
	$mResult=array();
	$mResult['result']=false;
	$mResult['missatgeAlerta']='';
	//$unitatsReservades=getUnitatsReservades($db);
	//$mGP['unitatsReservades']=$unitatsReservades;
	$mGP['grupsReservants']='';
	//echo "<br>select id,usuari_id,rebost,f_pagament,SUBSTRING(resum,LOCATE('producte_".$mAr['anularId'].":',resum)) from ".$mPars['taulaComandes']." where SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mAr['grup']."' AND usuari_id='".$mAr['usuari']."' AND LOCATE('producte_".$mAr['anularId'].":',CONCAT(' ',resum))>0  AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mAr['ruta']."'";
	if(!$result=mysql_query("select id,usuari_id,rebost,f_pagament,SUBSTRING(resum,LOCATE('producte_".$mAr['anularId'].":',resum)) from ".$mPars['taulaComandes']." where SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mAr['grup']."' AND usuari_id='".$mAr['usuari']."' AND LOCATE('producte_".$mAr['anularId'].":',CONCAT(' ',resum))>0  AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mAr['ruta']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mResult['missatgeAlerta']="<br>- error en llegir la comanda.";
		$mResult['result']=false;
		return $mResult;;
	}
	else
	{
		$mComandaModificada=mysql_fetch_array($result,MYSQL_NUM);
		if($mComandaModificada)
		{
			$unitatsReservades_=substr($mComandaModificada[4],strpos($mComandaModificada[4],':')+1);
			$unitatsReservades_=substr($unitatsReservades_,0,strpos($unitatsReservades_,';'));
			$cadena=substr($mComandaModificada[4],0,strpos($mComandaModificada[4],';')+1);
			if($unitatsReservades_-$mAr['anularUts']==0)
			{
				$cadena2='';
			}
			else
			{
				$cadena2="producte_".$mAr['anularId'].":".($unitatsReservades_-$mAr['anularUts']).";";
			}
			$mComandaModificada[4]=$unitatsReservades_;
			$mComandaModificada[5]=$mAr['anularUts'];

			$mProducteAnular=db_getProducte($mAr['anularId'],$db);
			if(($mAr['anularUts']*1/$mProducteAnular['format']*1)!=round($mAr['anularUts']*1/$mProducteAnular['format']*1))
			{
				$mResult['missatgeAlerta'].="<p  class='pAlertaNo'> error: el nombre d'unitats a anul.lar (".$mAr['anularUts']." uts) no es m�ltiple del format del producte (".$mProducteAnular['format']." uts).</p>";
				$mResult['result']=false;
				
				return $mResult;
			}			
		
			if($mAr['anularUts']>$unitatsReservades_)
			{
				$mResult['missatgeAlerta'].="<p  class='pAlertaNo'> error: el nombre d'unitats a anul.lar (".$mAr['anularUts']." uts) es major que les unitats reservades (hi ha ".$unitatsReservades_." uts reservades).</p>";
				$mResult['result']=false;
				
				return $mResult;
			}
			
			//echo "<br>"."update comandes_".$mPars['selRutaSufix']." set resum=REPLACE(resum,'".$cadena."','".$cadena2."') where id='".$mComandaModificada[0]."'";
			if(!$result=mysql_query("update ".$mPars['taulaComandes']." set resum=REPLACE(resum,'".$cadena."','".$cadena2."') where id='".$mComandaModificada[0]."'  AND llista_id='".$mPars['selLlistaId']."'",$db))
			{
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				$mResult['missatgeAlerta'].="<p  class='pAlertaNo'> error en modificar la comanda.</p>";
				$mResult['result']=false;
				
				return $mResult;
			}
			else
			{	
				$mResult['missatgeAlerta']="<p  class='pAlertaOk' style='tex-align:justify;'> ok, anul.lada reserva id:".$mComandaModificada[0]."<br>grup:".(urldecode($mComandaModificada[2]))."<br>usuari:".$mUsuarisRef[$mAr['usuari']]['usuari'].", producte:".$mAr['anularId']."<br>reserva inicial: ".$unitatsReservades_.", reserva final: ".($unitatsReservades_-$mAr['anularUts'])."</p>";
				$mGP['grupReservant']="<br>grup:".($mComandaModificada[2]).",usuari:".$mAr['usuari']."(".$mUsuarisRef[$mAr['usuari']]['usuari'].") reservat: ".$unitatsReservades_;
			}
		}
		else
		{
			$mResult['missatgeAlerta'].="<p  class='pAlertaNo'> no hi ha cap reserva del producte id:".$mAr['anularId']." per l'usuari ".$mUsuarisRef[$mAr['usuari']]['usuari']." en aquest grup</p>";
			$mResult['result']=false;

			return $mResult;
		}
	}
	//actualitzar el producte fins on no hagi fallat:

	$mProducte=db_getProducte($mAr['anularId'],$db);

//echo "<br>"."update productes_".$mPars['selRutaSufix']." set estoc_disponible=estoc_disponible+".$mAr['anularUts']."  where id='".$mAr['anularId']."'";
	if(!$result=mysql_query("update ".$mPars['taulaProductes']." set estoc_disponible=estoc_disponible+".$mAr['anularUts']."  where id='".$mAr['anularId']."'  AND llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mResult['missatgeAlerta'].="<p  class='pAlertaNo'> error actualitzar l'estoc_disponible del producte.</p>";
		$mResult['result']=false;
		
		return $mResult;
	}
	
	$mResult['result']=true;

	$mPars['selProducteId']=$mAr['anularId'];
	//registrar incidencia
	$result=mysql_query("insert into ".$mPars['taulaIncidencies']." values ('','".(date('Y-m-d H:i:s'))."','".$mUsuarisRef[$mPars['usuari_id']]['usuari']."','".$mPars['usuari_id']."','".$mAr['usuari']."','".(substr($mComandaModificada['2'],0,strpos($mComandaModificada[2],'-')))."','anulacioComanda','".$mAr['anularId']."','".$unitatsReservades_."','".$mAr['anularUts']."','<p><b>[CAC:AUTO]</b></p>','pendent','".$mAr['ruta']."','".$mPars['selLlistaId']."')",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	
	// si tot be: 
	////actualitzar forma de pagament de la usuaria
	$mFpagament=explode(',',$mComandaModificada[3]);
	if(count($mFpagament)>0)
	{
		$ecosT=$mFpagament[0]*1;
		$ebT=$mFpagament[1]*1;
		$eurosT=$mFpagament[2]*1;
		$umsT=$ecosT+$ebT+$eurosT;
	}
	
	if($mPars['selLlistaId']==0)
	{
		$ecosProdAr=$mAr['anularUts']*$mProducte['preu']*$mProducte['ms']/100;
		$ecosCTprodAr=$mAr['anularUts']*$mProducte['pes']*$mProducte['cost_transport_intern_kg']*$mProducte['ms_ctik']/100;
		$ecosFDprodAr=$mAr['anularUts']*$mProducte['preu']*($mParametres['FDCpp']['valor']/100)*$mParametres['msFDCpp']['valor']/100;
		$ecosTprodAr=$ecosProdAr+$ecosCTprodAr+$ecosFDprodAr;
		
		$eurosProdAr=$mAr['anularUts']*$mProducte['preu']*(100-$mProducte['ms'])/100;
		$eurosCTprodAr=$mAr['anularUts']*$mProducte['pes']*$mProducte['cost_transport_intern_kg']*(100-$mProducte['ms_ctik'])/100;
		$eurosFDprodAr=$mAr['anularUts']*$mProducte['preu']*($mParametres['FDCpp']['valor']/100)*(100-$mParametres['msFDCpp']['valor'])/100;
		$eurosTprodAr=$eurosProdAr+$eurosCTprodAr+$eurosFDprodAr;

			$umsTar=$ecosTprodAr+$eurosTprodAr;
			
			$ecosT_=$ecosT-$umsTar*$mFpagament[6]/100;
			$ebT_=$ebT;
			$eurosT_=$eurosT-$umsTar*(100-$mFpagament[6])/100;
			$umsT_=$ecosT_+$eurosT_+$ebT_;
		
			if($umsT_<=0.01)
			{
				$ecosT_=0;
				$eurosT_=0;
		
				$ppEcos=0;
				$ppEb=0;
				$ppEuros=0;
			}
			else
			{
				$ecosT_=number_format($ecosT_,2,'.','');
				$eurosT_=number_format($eurosT_,2,'.','');
		
				$ppEcos=number_format(($ecosT_*100/$umsT_),2,'.','');
				$ppEb=number_format(($ebT_*100/$umsT_),2,'.','');
				$ppEuros=number_format(($eurosT_*100/$umsT_),2,'.','');
			}
		
		$f_pagament=$ecosT_.','.$ebT_.','.$eurosT_.','.$mFpagament[3].','.$mFpagament[4].','.$mFpagament[5].','.$ppEcos.','.$ppEb.','.$ppEuros.','.$mFpagament[9];
	}
	else
	{
		$ecosProdAr=$mAr['anularUts']*$mProducte['preu']*$mProducte['ms']/100;
		$ecosCTprodAr=$mAr['anularUts']*$mProducte['pes']*$mPropietatsPeriodeLocal['ctikLocal']*$mPropietatsPeriodeLocal['ms_ctikLocal']/100;
		$ecosFDprodAr=$mAr['anularUts']*$mProducte['preu']*$mPropietatsPeriodeLocal['fdLocal']*$mPropietatsPeriodeLocal['ms_fdLocal']/100;
		$ecosTprodAr=$ecosProdAr+$ecosCTprodAr+$ecosFDprodAr;
		
		$eurosProdAr=$mAr['anularUts']*$mProducte['preu']*(100-$mProducte['ms'])/100;
		$eurosCTprodAr=$mAr['anularUts']*$mProducte['pes']*$mPropietatsPeriodeLocal['ctikLocal']*(100-$mPropietatsPeriodeLocal['ms_ctikLocal'])/100;
		$eurosFDprodAr=$mAr['anularUts']*$mProducte['preu']*$mPropietatsPeriodeLocal['fdLocal']*(100-$mPropietatsPeriodeLocal['ms_fdLocal'])/100;
		$eurosTprodAr=$eurosProdAr+$eurosCTprodAr+$eurosFDprodAr;

			$umsTar=$ecosTprodAr+$eurosTprodAr;
			
			$ecosT_=$ecosT-$umsTar*$mFpagament[6]/100;
			$ebT_=$ebT;
			$eurosT_=$eurosT-$umsTar*(100-$mFpagament[6])/100;
			$umsT_=$ecosT_+$eurosT_+$ebT_;
		
			if($umsT_<=0.01)
			{
				$ecosT_=0;
				$eurosT_=0;
		
				$ppEcos=0;
				$ppEb=0;
				$ppEuros=0;
			}
			else
			{
				$ecosT_=number_format($ecosT_,2,'.','');
				$eurosT_=number_format($eurosT_,2,'.','');
		
				$ppEcos=number_format(($ecosT_*100/$umsT_),2,'.','');
				$ppEb=number_format(($ebT_*100/$umsT_),2,'.','');
				$ppEuros=number_format(($eurosT_*100/$umsT_),2,'.','');
			}
		
		$f_pagament=$ecosT_.','.$ebT_.','.$eurosT_.','.$mFpagament[3].','.$mFpagament[4].','.$mFpagament[5].','.$ppEcos.','.$ppEb.','.$ppEuros.','.$mFpagament[9];
	}
	if(!$result=mysql_query("update ".$mPars['taulaComandes']." set f_pagament='".$f_pagament."' WHERE SUBSTRING(rebost,1,LOCATE('-',rebost)-1)='".$mAr['grup']."' AND usuari_id='".$mAr['usuari']."' AND llista_id='".$mPars['selLlistaId']."' AND periode_comanda='".$mAr['ruta']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mResult['missatgeAlerta']="<p  class='pAlertaNo'> error en actualitzar la forma de pagament de la usuaria '".$mAr['usuari']."' (id:".$mPars['usuari_id'].")</p>";
		$mResult['result']=false;
		return $mResult;;
	}
	
	
	////enviar mail a usuari amb comanda modificada
	$result=false;
	if($mParams['mailNotificacionsActives']==1)
	{	
		$result=mail_anulacioReservesUsuari($mComandaModificada,$mAr,$db);
		if(!$result)
		{
			$mResult['missatgeAlerta'].="<p  class='pAlertaNo'> error en enviar notificaci� a ".$mUsuarisRef[$mComandaModificada[1]]['email']."</p>";
			$mResult['result']=false;
		}
		else
		{
			$mResult['missatgeAlerta'].="<p  class='pAlertaOk'> ok, enviada notificaci� a ".$mUsuarisRef[$mComandaModificada[1]]['usuari']." (".$mUsuarisRef[$mAr['usuari']]['email'].", comanda a grup ".(urldecode($mComandaModificada['2'])).")</p>";
			$mResult['result']=true;
		}
	}
	else
	{
		$mResult['missatgeAlerta'].="<p  class='pAlertaNo'> [Notificacions inactives] Error en enviar notificaci� a ".$mUsuarisRef[$mComandaModificada[1]]['email']."</p>";
		$mResult['result']=false;
	}
	
	return $mResult;
}

//------------------------------------------------------------------------------
function db_aplicarCarrec($mCarrec,$db)
{
	global $mPars,$mUsuari;
	$missatgeAlerta='';
	
	//evitar reenviament form
	$result=mysql_query("select * from ".$mPars['taulaIncidencies']."  WHERE data='".$mCarrec['data']."' AND sel_usuari_id='".$mCarrec['usuari']."' AND usuari_id='".$mCarrec['autor']."'  AND grup_id='".$mCarrec['grup']."' AND tipus='".$mCarrec['tipus']."' AND estat='aplicat' AND  comentaris='".$mCarrec['comentaris']."'  AND llista_id='".$mPars['selLlistaId']."'",$db);
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
	if($mRow===false)
	{
		//guardar incidencia
		if($mPars['selLlistaId']==0)
		{
			if(!$result=mysql_query("insert into ".$mPars['taulaIncidencies']." values ('','".$mCarrec['data']."','".$mUsuari['usuari']."','".$mCarrec['autor']."','".$mCarrec['usuari']."','".$mCarrec['grup']."','".$mCarrec['tipus']."','".$mCarrec['ecos']."','".$mCarrec['eb']."','".$mCarrec['euros']."','".$mCarrec['comentaris']."','aplicat','".$mCarrec['ruta']."','".$mPars['selLlistaId']."')",$db))
			{
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut aplicar el c�rrec</p>";
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk'>El c�rrec s'ha aplicat correctament</p>";
			}
		}
		else
		{
			if(!$result=mysql_query("insert into ".$mPars['taulaIncidencies']." values ('','".$mCarrec['data']."','".$mUsuari['usuari']."','".$mCarrec['autor']."','".$mCarrec['usuari']."','".$mCarrec['grup']."','".$mCarrec['tipus']."','".$mCarrec['ecos']."','".$mCarrec['eb']."','".$mCarrec['euros']."','".$mCarrec['comentaris']."','aplicat','".$mCarrec['ruta']."','".$mPars['selLlistaId']."')",$db))
			{
				//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
				$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut aplicar el c�rrec</p>";
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk'>El c�rrec s'ha aplicat correctament</p>";
			}
		}
	}

	return $missatgeAlerta;
}

//------------------------------------------------------------------------------
function db_afegirComentariIncd($inId,$incdComentari,$db)
{
	global $mPars,$mUsuari;

	$incdComentari=(urlencode("<p class='p_micro' style='color:#00".(substr(MD5($mUsuari['usuari'].$mUsuari['id']),0,4)).";'><b>[".(date('d/m/Y H:i')).", autor: ".$mUsuari['usuari'].", ".$mPars['usuari_id'].", ".$mPars['nivell']."]</b>&nbsp;&nbsp;")).$incdComentari."</p>";

	//echo "<br>update  ".$mPars['taulaIncidencies']." set comentaris=CONCAT(comentaris,'".$incdComentari."') WHERE id='".$inId."'";
	if(!$result=mysql_query("update  ".$mPars['taulaIncidencies']." set comentaris=CONCAT(comentaris,'".$incdComentari."') WHERE id='".$inId."'",$db))
	{
		//echo "<br>  747 db_incidencies.php. ".mysql_errno().": ".mysql_error()."\n";
		$missatgeAlerta="<p class='pAlertaNo'>Atenci�: no s'ha pogut afegir el comentari</p>";
	}
	else
	{
		$missatgeAlerta="<p  class='pAlertaOk'>S'ha afegit el comentari correctament</p>";
	}
	
	return $missatgeAlerta;
}


?>

		