allowedCharsMobil='0123456789';
allowedCharsFloat='0123456789.';
allowedCharsCategories='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,-';


function f_missatgeAlerta()
{
	document.getElementById('td_missatgeAlerta').innerHTML=missatgeAlerta;
	return;
}


function guardarParametre(id,parametre)
{
	valor1232=document.getElementById('i_parametre_'+parametre).value
	document.getElementById('i_gPid').value=id;
	document.getElementById('i_gPv').value=valor1232;
	document.getElementById('i_gPp').value=parametre;
	document.getElementById('f_pars').submit();

	return;
}


function checkFormGuardarParametre(id)
{
	missatgeAlerta='Atenci�:\n\n';
	
	valPar=document.getElementById('i_parametreValor_'+id).value;
	
	//comprobar validesa:


	if(id=='limitUtsSelectorProductes' || id=='totalKmRuta' || id=='comandaMinima' || id=='thumbsWidth'  || id=='thumbsHeight'   || id=='patternImageMaxWidth') //enter
	{
		if(valPar.length==0)
    	{
			missatgeAlerta+="\nEl camp "+id+" ha de contenir un valor";	
    	}
		else if(valPar.length>0)
		{
	   		allowedChars=allowedCharsMobil;
			$_ok=0;
			//check for prohibited characters
   			for(i=0;i<valPar.length;i++)
       		{
           		for(j=0;j<allowedChars.length;j++)
           		{
               		if(valPar.charAt(i)==allowedChars.charAt(j)){$_ok++;}
           		}
			}
       		if($_ok==valPar.length)
       		{
				missatgeAlerta+="\nEl camp "+id+" cont� un valor incorrecte";			
       		}
		}
	}
	else if(id=='diesLimitReservesNegatiu')
	{
		if(valPar.length==0 || valPar.length>2)
    	{
			missatgeAlerta+="\nEl camp diesLimitReservesNegatiu ha de contenir un valor d'un o dos digits";	
    	}
		else
		{
	   		allowedChars=allowedCharsMobil;
			$_ok=0;
			//check for prohibited characters
   			for(i=0;i<valPar.length;i++)
       		{
	       		for(j=0;j<allowedChars.length;j++)
    	   		{
        	   		if(valPar.charAt(i)==allowedChars.charAt(j)){$_ok++;}
       			}
       		}
			if($_ok==2)
       		{
				missatgeAlerta+="\nEl camp diesLimitReservesNegatiu cont� un valor incorrecte ";			
       		}
		}
	}
	else if(id=='factorParticipacioRutaCac' || id=='varCostTransport') //float
	{
		if(valPar.length==0)
    	{
			missatgeAlerta+="\nEl camp "+id+" ha de contenir un valor";	
    	}
		else
		{
	   		allowedChars=allowedCharsFloat;
			$_ok=0;
			//check for prohibited characters
   			for(i=0;i<valPar.length;i++)
       		{
	       		for(j=0;j<allowedChars.length;j++)
    	   		{
        	   		if(valPar.charAt(i)==allowedChars.charAt(j)){$_ok++;}
       			}
       		}
			if($_ok==valPar.length)
       		{
				missatgeAlerta+="\nEl camp "+id+" cont� un valor incorrecte ("+allowedCharsFloat+")";			
       		}
		}
	}
	else if(id=='categories_grups') //float
	{
		if(valPar.length==0)
    	{
			missatgeAlerta+="\nEl camp "+id+" ha de contenir almenys una categoria entre comes";	
    	}
		else
		{
	   		allowedChars=allowedCharsCategories;
			$_ok=0;
			//check for prohibited characters
   			for(i=0;i<valPar.length;i++)
       		{
	       		for(j=0;j<allowedChars.length;j++)
    	   		{
        	   		if(valPar.charAt(i)==allowedChars.charAt(j)){$_ok++;}
       			}
       		}
			if($_ok==valPar.length)
       		{
				missatgeAlerta+="\nEl camp "+id+" cont� un valor incorrecte ("+allowedCharsFloat+")";			
       		}
		}
	}


	if(missatgeAlerta!='Atenci�:\n\n')
	{
		alert(missatgeAlerta);
	}
	else
	{
		adressAndGet="parametres.php?pId="
						+id
						+"&pV="
						+valPar
		sendTarget="_self";						
						
		enviarFpars(adressAndGet,sendTarget);
	}

	return;
}

//*v36.2-funcio
function checkFormCrearRutaEspecial()
{
	missatgeAlerta='Atenci�:\n\n';
	
	
	etiquetaRutaEspecial=document.getElementById('sel_etiquetaRutaEspecial').value;
	codiPeriode=document.getElementById('sel_codiPeriode').value;
	condicionsPeriode=document.getElementById('ta_condicionsPeriode').value;
	
	perfilsIdChain=',';
	for (i=0, len=sel_perfilsId.options.length; i<len; i++)
	{
		opt=sel_perfilsId.options[i];
		if(opt.selected)
		{	
			perfilsIdChain+=opt.value+',';
		}
	}
	if(etiquetaRutaEspecial.length==0)
	{
		missatgeAlerta+="\nCal seleccionar una etiqueta de periode especial";			
	}

	if(codiPeriode.length==0)
	{
		missatgeAlerta+="\nCal seleccionar una codi de periode";			
	}

	if(perfilsIdChain.length==1)
	{
		missatgeAlerta+="\nCal seleccionar algun perfil de productora per mantenir a la nova ruta especial";			
	}

	if(condicionsPeriode.length==1)
	{
		missatgeAlerta+="\nCal introduir l'explicaci� i condicions d'aquest periode especial";			
	}


	
	if(missatgeAlerta!='Atenci�:\n\n')
	{
		alert(missatgeAlerta);
	}
	else
	{
		adressAndGet="parametres.php?cp="
						+codiPeriode
						+"&er="
						+etiquetaRutaEspecial
						+"&pids="
						+perfilsIdChain
						+"&conds="
						+condicionsPeriode
		sendTarget="_self";						
						
		enviarFpars(adressAndGet,sendTarget);
	}
}

function recordatoriGuardar(id)
{
	valPar=document.getElementById('i_parametreValor_'+id).value;
	document.getElementById('i_guardar_'+id).style.backgroundColor='ff7700';

	return;
}

function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='parametres.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}

