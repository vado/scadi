<?php
include "config.php";
include "db.php";
include "eines.php";

//------------------------------------------------------------------------
function recepcioFitxer($db)
{
    global $mParametres,$mPars;

    $missatgeError='';

    if($_FILES['f_fitxer']['name']!='')
    {
        if (is_uploaded_file($_FILES['f_fitxer']['tmp_name']))
        {
            //mirar si l'extensio �s correcta:
            $size=@pathinfo($_FILES['f_fitxer']['name']);

            $nomFitxer=$_FILES['f_fitxer']['name'];
			rename($nomFitxer, 'old_'.$nomFitxer);
            if(!copy($_FILES['f_fitxer']['tmp_name'],$nomFitxer))
            {
                $missatgeError.="<br> Atenci�: Error en copiar el fitxer al directori.";
				rename('old_'.$nomFitxer,$nomFitxer);
            }
		}
    }

    if(strlen($missatgeError)==0)
	{
        $missatgeError.="<br> Ok: El fitxer s'ha guardat correctament.";
    }

return $missatgeError;
}
//--------------------------------------------------------------------


$missatgeAlerta='';


$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);

$parsChain=makeParsChain($mPars);
$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];

$ruta_=@$_GET['sR']; //selector de ruta
if(isset($ruta_))
{
	$mPars['selRutaSufix']=$ruta_;
}
else
{
	$ruta_=@$_POST['i_selRuta'];

	if(isset($ruta_))
	{
		$mPars['selRutaSufix']=$ruta_;
	}
}
getConfig($db); //inicialitza variables anteriors;

if(!checkLogin($db) || $mPars['nivell']!='sadmin')
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	exit();
}

$mRutesSufixes=getRutesSufixes($db);
$mMissatgeAlerta=array();

$op=@$_POST['i_op'];
if(isset($op))
{
	if($op=='pujarFitxer')
	{
		$missatgeAlerta=recepcioFitxer($db);
	}
}

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>

<head>
<title>pujar fitxers</title>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
ruta=".$ruta.";
</SCRIPT>
</head>
<body onLoad=\"\">

	<table align='center' style='width:80%'>
		<tr>
			<th style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</p>
			</td>
		</tr>
	</table>

	<table align='center' style='width:80%'>
		<tr>
			<th style='width:100%;' align='center'>
			".$missatgeAlerta."
			</td>
		</tr>
	</table>
<center><p style='font-size:16px;'>[ Ruta <b>".$mPars['selRutaSufix']."</b>: Pujar fitxers ]</p></center>
";

if(count($mMissatgeAlerta)>0)
{
	echo "
	<table align='center' border='1' width='60%'>
		<tr>
			<td  bgcolor='#eeeeee'  align='left'  width='30%' >
			<br>
			<p>pujades:</p>
			<br>
	";
	while(list($key,$mVal)=each($mMissatgeAlerta))
	{
		echo "
			<p>".$mVal['fitxer']." >  ".$mVal['result']."</p>
			";
	}
	reset($mMissatgeAlerta);
	echo "
			</td>
		</tr>
	</table>
	";
}
echo "
	<table align='center' bgcolor='#dddddd' width='70%' align='center'>
		<tr>
			<td  bgcolor='#eeeeee'  width='100%' >
			<table width='80%' border='0' align='center'>
				<tr>
					<td width='90%' align='center'>
					<table width='100%'>
					<FORM name='f_pujarFitxer' METHOD='post' action='uploadF2.php' target='_self'  ENCTYPE='multipart/form-data'>
					<INPUT TYPE='file' size='8' NAME='f_fitxer'>
					<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
					<input type='hidden' id='i_op' name='i_op' value='pujarFitxer'>
					<input type='submit' value='pujar fitxer'>
					</form>
					</table>
					<br>
					</p>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
<br>&nbsp;
</body>
</html>
";

?>

		