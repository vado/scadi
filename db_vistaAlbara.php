<?php
//------------------------------------------------------------------------------
function getProductesVistaAlbaraCAC($db)
{
	global 	$mPars,$sortBy,$ascdesc,$mComanda,$quantitatTotalCac,$quantitatTotalRebosts;

	$cadenaIds=',,';
	$quantitatTotalCac='';
	$mProductes=array();

	if(!$result=mysql_query("select sum(estoc_previst*pes) from ".$mPars['taulaProductes']." WHERE llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalCac+=$mRow[0];
	}

	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2)),$db))
	{
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
//*v36.2-comentari
		//return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$quantitatTotalRebosts+=$mRow['quantitat'];
		}
	}


	if(!$result=mysql_query("select resum from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='0'",$db))
	{
		echo "<br> 2 ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
   	}
   	else
	{
		$comanda=mysql_fetch_array($result,MYSQL_ASSOC);
		$mComanda_tmp=explode(';',$comanda['resum']);
		while(list($key,$val)=each($mComanda_tmp))
		{
			$mTmp=@explode('_',$val);
			$mTmp2=@explode(':',$mTmp[1]);
			if($mTmp2[0]!='')
			{
				$mComanda[$mTmp2[0]]=$mTmp2[1];
				$cadenaIds.=$mTmp2[0].',';
			}
		}
		reset($mComanda_tmp);

		$cadenaIds2=',,';
	
		if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where LOCATE(CONCAT(',',id,','),'".$cadenaIds."')>0 order by ".$mPars['sortBy']." ".$mPars['ascdesc'],$db))
		{
			//echo "<br> 2 ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
			return false;
		}
		else
		{
			$i=0;
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mProductes[$i]=$mRow;
				$i++;
				$cadenaIds2.=$mRow['id'].',';
			}
		}
	}

	return $mProductes;
}

//------------------------------------------------------------------------------
function getProductesVistaAlbaraGrup($opcio,$db)
{
	global 	$mPars,$sortBy,$ascdesc,$mComanda,$quantitatTotalCac,$quantitatTotalRebosts;

	$cadenaIds=',,';
	$quantitatTotalCac='';
	$mProductes=array();

	if(!$result=mysql_query("select sum(estoc_previst*pes) from ".$mPars['taulaProductes']." WHERE llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br> 92 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalCac+=$mRow[0];
	}

	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2)),$db))
	{
//*v36.2, comentari
	//echo "<br> 103 db_vistaAlbara.php  ".mysql_errno() . ": " . mysql_error(). "\n";
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$quantitatTotalRebosts+=$mRow['quantitat'];
		}
	}

	if($mPars['selLlistaId']==0)
	{
		$andPeriodeLocal='';
	}
	else
	{
		$andPeriodeLocal=" AND periode_comanda='".$mPars['sel_periode_comanda_local']."'";
	}
	
	//echo "<br>select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id!='0' AND llista_id='".$mPars['selLlistaId']."' ".$andPeriodeLocal;
	if(!$result=mysql_query("select * from ".$mPars['taulaComandes']." where SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id!='0' AND llista_id='".$mPars['selLlistaId']."' ".$andPeriodeLocal,$db))
	{
		//echo "<br>  127 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
		return false;
   	}
   	else
	{
		if($opcio=='totals')
		{
			while($comanda=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mComanda_tmp=explode(';',$comanda['resum']);
				while(list($key,$val)=each($mComanda_tmp))
				{
					$mTmp=@explode('_',$val);
					$mTmp2=@explode(':',$mTmp[1]);
					if($mTmp2[0]!='')
					{
						if(!isset($mComanda[$mTmp2[0]]))
						{
							$mComanda[$mTmp2[0]]=$mTmp2[1];
							$cadenaIds.=$mTmp2[0].',';
						}
						else
						{
							$mComanda[$mTmp2[0]]+=$mTmp2[1];
						}
					}
				}
				reset($mComanda_tmp);
			}
			$cadenaIds2=',,';
	
			if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where llista_id='".$mPars['selLlistaId']."' AND LOCATE(CONCAT(',',id,','),'".$cadenaIds."')>0 order by ".$mPars['sortBy']." ".$mPars['ascdesc'],$db))
			{
				//echo "<br>select * from productes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',id,','),'".$cadenaIds."')>0 order by ".$mPars['sortProductBy']." ".$mPars['ascdesc'];
				//echo "<br>  151 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
				return false;
			}
			else
			{
				$i=0;
				while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
				{
					$mProductes[$i]=$mRow;
					$i++;
					$cadenaIds2.=$mRow['id'].',';
				}
			}
		}
		else if($opcio=='desglossat')
		{
			$cadenaIds2=',,';
			while($comanda=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mComanda_tmp=explode(';',$comanda['resum']);
				while(list($key,$val)=each($mComanda_tmp))
				{
					$mTmp=@explode('_',$val);
					$mTmp2=@explode(':',$mTmp[1]);
					if($mTmp2[0]!='')
					{
						$mComanda[$mTmp2[0]][$comanda['usuari_id']]=$mTmp2[1];
						$cadenaIds.=$mTmp2[0].',';
					}
				}
				reset($mComanda_tmp);
			}
	
			//echo "<br> select * from ".$mPars['taulaProductes']." where llista_id='".$mPars['selLlistaId']."' AND LOCATE(CONCAT(',',id,','),'".$cadenaIds."')>0 order by ".$mPars['sortBy']." ".$mPars['ascdesc'];
			if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where llista_id='".$mPars['selLlistaId']."' AND LOCATE(CONCAT(',',id,','),'".$cadenaIds."')>0 order by ".$mPars['sortBy']." ".$mPars['ascdesc'],$db))
			{
				//echo "<br>  187 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
				//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
				return false;
			}
			else
			{
				$i=0;
				while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
				{
					$mProductes[$i]=$mRow;
					$i++;
					$cadenaIds2.=$mRow['id'].',';
				}
			}
		}
	}
	return $mProductes;
}
//------------------------------------------------------------------------------
function getProductesVistaAlbaraUsuari($db)
{
	global 	$mPars,$sortBy,$ascdesc,$mComanda,$quantitatTotalCac,$quantitatTotalRebosts;

	$cadenaIds=',,';
	$quantitatTotalCac='';
	$mProductes=array();

	if(!$result=mysql_query("select sum(estoc_previst*pes) from ".$mPars['taulaProductes']." WHERE llista_id='".$mPars['selLlistaId']."'",$db))
	{
		//echo "<br> 217 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
		return false;
	}
	else
	{
		$mRow=mysql_fetch_array($result);
		$quantitatTotalCac+=$mRow[0];
	}

	if(!$result=mysql_query("select * from intercanvis_rebosts_".(substr($mPars['selRutaSufix'],0,2)),$db))
	{
//*v36.2-comentari
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		//return false;
	}
	else
	{
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$quantitatTotalRebosts+=$mRow['quantitat'];
		}
	}


	if($mPars['selUsuariId']=='0') //albara grup
	{

		if(!$result=mysql_query("select resum from ".$mPars['taulaComandes']." where llista_id='".$mPars['selLlistaId']."' AND SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id!='0'",$db))
		{
			//echo "<br>  245 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
			return false;
    	}
    	else
		{
			if($opcio=='totals')
			{
				while($comanda=mysql_fetch_array($result,MYSQL_ASSOC))
				{
					$mComanda_tmp=explode(';',$comanda['resum']);
					while(list($key,$val)=each($mComanda_tmp))
					{
						$mTmp=@explode('_',$val);
						$mTmp2=@explode(':',$mTmp[1]);
						if($mTmp2[0]!='')
						{
							if(!isset($mComanda[$mTmp2[0]]))
							{
								$mComanda[$mTmp2[0]]=$mTmp2[1];
								$cadenaIds.=$mTmp2[0].',';
							}
							else
							{
								$mComanda[$mTmp2[0]]+=$mTmp2[1];
							}
						}
					}
					reset($mComanda_tmp);
				}
				$cadenaIds2=',,';
	
				if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where llista_id='".$mPars['selLlistaId']."' AND LOCATE(CONCAT(',',id,','),'".$cadenaIds."')>0 order by ".$mPars['sortBy']." ".$mPars['ascdesc'],$db))
				{
					//echo "<br>  279 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
					return false;
				}
				else
				{
					$i=0;
					while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
					{
						$mProductes[$i]=$mRow;
						$i++;
						$cadenaIds2.=$mRow['id'].',';
					}
				}
			}
			else if($opcio=='desglossat')
			{
				while($comanda=mysql_fetch_array($result,MYSQL_ASSOC))
				{
					$mComanda_tmp=explode(';',$comanda['resum']);
					while(list($key,$val)=each($mComanda_tmp))
					{
						$mTmp=@explode('_',$val);
						$mTmp2=@explode(':',$mTmp[1]);
						if($mTmp2[0]!='')
						{
							$mComanda[$mTmp2[0]][$comanda['usuari_id']]=$mTmp2[1];
							$cadenaIds.=$mTmp2[0].',';
						}
					}
					reset($mComanda_tmp);
				}
				$cadenaIds2=',,';
	
				if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where llista_id='".$mPars['selLlistaId']."' AND LOCATE(CONCAT(',',id,','),'".$cadenaIds."')>0 order by ".$mPars['sortBy']." ".$mPars['ascdesc'],$db))
				{
					//echo "<br>  315 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
					//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
					return false;
				}
				else
				{
					$i=0;
					while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
					{
						$mProductes[$i]=$mRow;
						$i++;
						$cadenaIds2.=$mRow['id'].',';
					}
				}
			}
		}
	}
	else //es l'usuari
	{
		if(!$result=mysql_query("select resum from ".$mPars['taulaComandes']." where llista_id='".$mPars['selLlistaId']."' AND SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."'",$db))
		{
			//echo "<br>  336 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
			return false;
    	}
    	else
		{
			$comanda=mysql_fetch_array($result,MYSQL_ASSOC);
		}

		$mComanda_tmp=explode(';',$comanda['resum']);
		$i=0;
		while(list($key,$val)=each($mComanda_tmp))
		{
			$mTmp=@explode('_',$val);
			$mTmp2=@explode(':',$mTmp[1]);
			if($mTmp2[0]!='')
			{
				$mComanda[$mTmp2[0]]=$mTmp2[1];
				$cadenaIds.=$mTmp2[0].',';
			}
		}
		reset($mComanda);
		$cadenaIds2=',,';
	
		if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where llista_id='".$mPars['selLlistaId']."' AND LOCATE(CONCAT(',',id,','),'".$cadenaIds."')>0 order by ".$mPars['sortBy']." ".$mPars['ascdesc'],$db))
		{
			//echo "<br>  362 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
			return false;
		}
		else
		{
			$i=0;
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mProductes[$i]=$mRow;
				$i++;
				$cadenaIds2.=$mRow['id'].',';
			}
		}
	}

	return $mProductes;
}

//------------------------------------------------------------------------------
function getProductesVistaAlbaraUsuariLocal($db)
{
	global 	$mPars,$sortBy,$ascdesc,$mComanda;

	$cadenaIds=',,';
	$mProductes=array();

		if(!$result=mysql_query("select resum from ".$mPars['taulaComandes']." where llista_id='".$mPars['selLlistaId']."' AND SUBSTRING_INDEX(rebost,'-',1)='".$mPars['grup_id']."' AND usuari_id='".$mPars['selUsuariId']."'  AND periode_comanda='".$mPars['sel_periode_comanda_local']."'",$db))
		{
			//echo "<br>  336 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
			return false;
    	}
    	else
		{
			$comanda=mysql_fetch_array($result,MYSQL_ASSOC);
		}

		$mComanda_tmp=explode(';',$comanda['resum']);
		$i=0;
		while(list($key,$val)=each($mComanda_tmp))
		{
			$mTmp=@explode('_',$val);
			$mTmp2=@explode(':',$mTmp[1]);
			if($mTmp2[0]!='')
			{
				$mComanda[$mTmp2[0]]=$mTmp2[1];
				$cadenaIds.=$mTmp2[0].',';
			}
		}
		reset($mComanda);
		$cadenaIds2=',,';
	
		if(!$result=mysql_query("select * from ".$mPars['taulaProductes']." where llista_id='".$mPars['selLlistaId']."' AND LOCATE(CONCAT(',',id,','),'".$cadenaIds."')>0   order by ".$mPars['sortBy']." ".$mPars['ascdesc'],$db))
		{
			//echo "<br>  362 db_vistaAlbara.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*2 */',mysql_errno().'--'.mysql_error(),'57','eines_productes.php');
			return false;
		}
		else
		{
			$i=0;
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$mProductes[$i]=$mRow;
				$i++;
				$cadenaIds2.=$mRow['id'].',';
			}
		}

	return $mProductes;
}

//------------------------------------------------------------------------------
function db_getIncidenciesTot($db)
{
	global $mPars,$mFiltre;
	
	$missatgeAlerta='';
	$mIncidencies=array();


	$cont=0;
	$contF=0;
	$where='';
	$and='';
	

	if(@$mPars['vSelUsuariIncd'] && $mPars['vSelUsuariIncd']!='TOTS'){$contF++;}
	if(@$mPars['vUsuariIncd'] && $mPars['vUsuariIncd']!='TOTS'){$contF++;}
	if(@$mPars['vTipusIncd'] && $mPars['vTipusIncd']!='TOTS'){$contF++;}
	if(@$mPars['vProducteIdIncd'] && $mPars['vProducteIdIncd']!='TOTS'){$contF++;}
	if(@$mPars['vEstatIncd'] && $mPars['vEstatIncd']!='TOTS'){$contF++;}
	if(@$mPars['vRutaIncd'] && $mPars['vRutaIncd']!='TOTS'){$contF++;}
	if(@$mPars['vGrupIncd'] && $mPars['vGrupIncd']!='TOTS'){$contF++;}

	if($contF>0){ $where=' where ';}
	if($contF>1){ $and=' AND ';}

	if(@$mPars['vSelUsuariIncd'] && $mPars['vSelUsuariIncd']!='TOTS'){$where.=" sel_usuari_id='".$mPars['vSelUsuariIncd']."' ".$and; $cont++; if($cont>=$contF-1){ $and='';}}
	if(@$mPars['vUsuariIncd'] && $mPars['vUsuariIncd']!='TOTS'){$where.=" usuari_id='".$mPars['vUsuariIncd']."' ".$and; $cont++; if($cont>=$contF-1){ $and='';}}
	if(@$mPars['vTipusIncd'] && $mPars['vTipusIncd']!='TOTS'){$where.=" tipus='".$mPars['vTipusIncd']."' ".$and; $cont++; if($cont>=$contF-1){ $and='';}}
	if(@$mPars['vProducteIdIncd'] && $mPars['vProducteIdIncd']!='TOTS'){$where.=" producte_id='".$mPars['vProducteIdIncd']."' ".$and; $cont++; if($cont>=$contF-1){ $and='';}}
	if(@$mPars['vEstatIncd'] && $mPars['vEstatIncd']!='TOTS'){$where.=" estat='".$mPars['vEstatIncd']."' ".$and; $cont++; if($cont>=$contF-1){ $and='';}}
	if(@$mPars['vRutaIncd'] && $mPars['vRutaIncd']!='TOTS'){$where.=" ruta='".$mPars['vRutaIncd']."' ".$and; $cont++; if($cont>=$contF-1){ $and='';}}
	if(@$mPars['vGrupIncd'] && $mPars['vGrupIncd']!='TOTS'){$where.=" grup_id='".$mPars['vGrupIncd']."' ".$and; $cont++; if($cont>=$contF-1){ $and='';}}
	
	
	$i=0;
	$result=mysql_query("select * from ".$mPars['taulaIncidencies']." ".$where." order by id DESC",$db);
	//echo "select * from incidencies_".$mPars['selRutaSufix']." ".$where." order by id DESC";
	//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mIncidencies[$i]=$mRow;
		$i++;
	}

	return $mIncidencies;
}

//------------------------------------------------------------------------------
function db_getIncidenciesAlbara($grupId,$db)
{
	global $mPars;
	
	$missatgeAlerta='';
	$mIncidencies=array();
	$i=0;
	if($grupId=='0') //CAC
	{
		$result=mysql_query("select * from ".$mPars['taulaIncidencies']." where grup_id='0' AND estat='pendent' order by tipus ASC",$db);
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		$mRow=mysql_fetch_array($result,MYSQL_ASSOC);
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mIncidencies[$i]=$mRow;
			$i++;
		}
	}
	else
	{
		$result=mysql_query("select * from ".$mPars['taulaIncidencies']." where grup_id='".$grupId."' AND estat='pendent' order by tipus ASC",$db);
		//echo "<br>  ".mysql_errno() . ": " . mysql_error(). "\n";
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$mIncidencies[$i]=$mRow;
			$i++;
		}
	}

	return $mIncidencies;
}



//------------------------------------------------------------------------------
function db_getIncidenciesSelUsuari($grupId,$db)
{
	global $mPars;
	
	$missatgeAlerta='';
	$mIncidencies=array();
	$i=0;
	
	$result=mysql_query("select * from ".$mPars['taulaIncidencies']." where grup_id='".$grupId."' AND estat='pendent' AND sel_usuari_id='".$mPars['selUsuariId']."' and estat='pendent' order by tipus ASC",$db);
	//echo "<br>179 db_incidencies.php  ".mysql_errno() . ": " . mysql_error(). "\n";
	while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
	{
		$mIncidencies[$i]=$mRow;
		$i++;
	}

	return $mIncidencies;
}

?>

		