<?php

//------------------------------------------------------------------------------
function getReserves($zona,$db)
{
	global $mPars,$mProductes,$mReservesComandes,$jaEntregatT;
	
	
	$mReservesProductes=array();
	$puntsEntregaZona='';
	$grupsPuntsEntregaZona='';
	if($zona!='')
	{
		//get grups dins zona
		if(!$result=mysql_query("select ref from rebosts_".$mPars['selRutaSufix']." WHERE LOCATE('".$zona."',categoria)>0",$db))
		{
			//echo "<br> 17 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		}
		else
		{ 
			$i=0;
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$puntsEntregaZona.=','.$mRow['ref'].',';
			}
		}
	}
	else
	{
		$zona='totes';
		//get grups dins zona
		if(!$result=mysql_query("select ref from rebosts_".$mPars['selRutaSufix'],$db))
		{
			//echo "<br> 34 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		}
		else
		{ 
			$i=0;
			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
			{
				$puntsEntregaZona.=','.$mRow['ref'].',';
			}
		}
	}
	$puntsEntregaZona=str_replace(',,',',',$puntsEntregaZona);
	
	//get grups amb punts entrega en els grups anteriors
	if(!$result=mysql_query("select rebost,punt_entrega from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',punt_entrega,','),CONCAT(' ','".$puntsEntregaZona."'))>0",$db))
	{
			//echo "<br> 54 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			$grupsPuntsEntregaZona.=','.$grupId.',';
			$mComandesGrups[$grupId]['punt_entrega']=$mRow['punt_entrega'];
		}
	}
	$grupsPuntsEntregaZona=str_replace(',,',',',$grupsPuntsEntregaZona);
	
	//getComandes dels usuaris dels grups dels puntsentrega dins zona
	
	if(!$result=mysql_query("select * from comandes_".$mPars['selRutaSufix']." where LOCATE(CONCAT(',',SUBSTRING_INDEX(rebost,'-',1),','),CONCAT(' ','".$grupsPuntsEntregaZona."'))>0 AND usuari_id!=0",$db))
	{
			//echo "<br> 70 db_vistaReserves.php ".mysql_errno() . ": " . mysql_error(). "\n";
		//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
	}
	else
	{ 
		$i=0;
		while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$grupId=substr($mRow['rebost'],0,strpos($mRow['rebost'],'-'));
			if(!isset($mReservesComandes[$zona]))
			{
				$mReservesComandes[$zona]=array();
			}
			if(!isset($mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']]))
			{
				$mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']]=array();
			}
			if(!isset($mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]))
			{
				$mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]=array();
			}
			$mReservesComandes[$zona][$mComandesGrups[$grupId]['punt_entrega']][$grupId][$mRow['usuari_id']]['resum']=$mRow['resum'];

			$mComanda=explode(';',$mRow['resum']);
			while(list($key,$mVal)=each($mComanda))
			{
				$mIndexQuantitat=explode(':',$mVal);
				$index=str_replace('producte_','',$mIndexQuantitat[0]);
				$quantitat=@$mIndexQuantitat[1];
				if($index!='')
				{
					if(array_key_exists($index,$mReservesProductes))
					{
						$mReservesProductes[$index]+=$quantitat;
					}
					else
					{
						$mReservesProductes[$index]=$quantitat;
					}
				}
			}
		}
	}
	
	if($mPars['excloureProductesJaEntregats']==1)
	{
		$mProductesJaEntregats=db_getProductesJaEntregats($db);
		while(list($index,$mVal)=each($mProductesJaEntregats))
		{
			if(substr_count($grupsPuntsEntregaZona,','.$mVal['grup_id'].',')>0)
			{
				$mReservesProductes[$index]-=$mVal['rebut'];
				$jaEntregatT+=$mVal['rebut'];
			}
		}
		reset($mProductesJaEntregats);
	}

	//incorporar reserves a productes
	
	while(list($index,$mProducte)=each($mProductes))
	{
		if(isset($mReservesProductes[$index]))
		{
			$mProductes[$index]['quantitat']=$mReservesProductes[$index];
		}
		else
		{
			$mProductes[$index]['quantitat']=0;
		}
	}
	reset($mProductes);


	return;
}



?>

		