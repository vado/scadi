<?php
include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";
include 'html_estadistiques.php';
include "html_ajuda1.php";
include "db_ajuda.php";

$mSb=array('index','producte','productor','quantitat','unitat_facturacio','pes','ums','ms','ecos','euros');
$mSbd=array('ascendent','descendent');


$parsChain=$_POST['i_pars'];
$mPars=getPars($parsChain);
$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

$parsChain=makeParsChain($mPars);

$mRuta=explode('_',$mPars['selRutaSufix']);

if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}

$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
$ruta=$mPars['selRutaSufix'];
getConfig($db); //inicialitza variables anteriors;

post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['resumProductesJJAA.php']=db_getAjuda('resumProductesJJAA.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

$mGrupsRef=db_getGrupsRef($db);

$mConsumXcsv=array();

	//$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
	$mPars['sortBy']='id';
	$mPars['ascdesc']='ASC';
	$mPars['veureProductesDisponibles']=1;
	$mPars['excloureProductesJaEntregats']=-1;
	$mPars['etiqueta']='jjaa';
	$mPars['etiqueta2']='CAP';
	$mPars['vProductor']='TOTS';
	$mPars['vCategoria']='TOTS';
	$mPars['vSubCategoria']='TOTS';
	$mPars['paginacio']=-1;
	$mPars['vProducte']='TOTS';

$mGrupsZones=getGrupsZones($mParametres['agrupamentZones']['valor']);
$mSzones=array_keys($mGrupsZones);
$mZones=db_getZones($db);
	$mRutesSufixes=getRutesSufixes($db);

$mSelRutes=array();
$mSelRutes[0]=$ruta;

$mSelRutes_=@$_POST['sel_rutes'];
if(isset($mSelRutes_)){$mSelRutes=$mSelRutes_;}
$sb_=@$_POST['sel_sb'];
if(isset($sb_)){$sb=$sb_;}else{$sb='euros';}
$sbd_=@$_POST['sel_sbd'];
if(isset($sbd_)){$sbd=$sbd_;}else{$sbd='descendent';}



//------------------------------------------------------------------------
function db_getConsum($db)
{
	global $mPars,$mSelRutes,$mProductes,$sb,$sbd;
	
	$mConsum=array();
	$rutaActual=$mPars['selRutaSufix'];
	$cont=0;
	while(list($key,$selRuta)=each($mSelRutes))
	{
		if(!$result=mysql_query("select * from comandes_".$selRuta." where rebost!='0-CAC' AND usuari_id!='0' order by id ASC",$db))
		{
			//echo "<br> 28 estadistiques.php ".mysql_errno() . ": " . mysql_error(). "\n";
			//err__('DB/*19.1*/',mysql_errno().'--'.mysql_error(),'102','db.php');
		
			return false;
		}
		else
		{
			$mPars['selRutaSufix']=$selRuta;
			$mProductes=db_getProductes2($db);

			while($mRow=mysql_fetch_array($result,MYSQL_ASSOC))		
			{
				$mProductesComanda=explode(';',$mRow['resum']);
				for($i=0;$i<count($mProductesComanda);$i++)
				{
					$mIndexQuantitat=explode(':',$mProductesComanda[$i]);
					$index=str_replace('producte_','',$mIndexQuantitat[0]);
					$quantitat=@$mIndexQuantitat[1];
					if($index!='' && $index!=0 && array_key_exists($index,$mProductes))
					{
						if(!isset($mConsum[$index]))
						{
							$mConsum[$index]=array();
							$mConsum[$index]['index']=$index;
							$mConsum[$index]['ms']=number_format($mProductes[$index]['ms'],2);
							$mConsum[$index]['producte']=$mProductes[$index]['producte'];
							$mConsum[$index]['productor']=$mProductes[$index]['productor'];
							$mConsum[$index]['quantitat']=$quantitat;
							$mConsum[$index]['unitat_facturacio']=$mProductes[$index]['unitat_facturacio'];
							$mConsum[$index]['pes']=number_format($quantitat*$mProductes[$index]['pes'],2);
							$mConsum[$index]['ums']=number_format($quantitat*$mProductes[$index]['preu'],2);
							$mConsum[$index]['ecos']=number_format($quantitat*$mProductes[$index]['preu']*$mProductes[$index]['ms']/100,2);
							$mConsum[$index]['euros']=number_format($quantitat*$mProductes[$index]['preu']*(100-$mProductes[$index]['ms'])/100,2);
						}
						else
						{
							$mConsum[$index]['quantitat']+=$quantitat;
							$mConsum[$index]['pes']+=number_format($quantitat*$mProductes[$index]['pes'],2);
							$mConsum[$index]['ums']+=number_format($quantitat*$mProductes[$index]['preu'],2);
							$mConsum[$index]['ecos']+=number_format($quantitat*$mProductes[$index]['preu']*$mProductes[$index]['ms']/100,2);
							$mConsum[$index]['euros']+=number_format($quantitat*$mProductes[$index]['preu']*(100-$mProductes[$index]['ms'])/100,2);
						}
					}
				}
			}
		}

		$mPars['selRutaSufix']=$rutaActual;
		
		//generar matrius ordenades per clau
		$mConsumX=array();
		
		while(list($index,$mVal)=each($mConsum))
		{
			if(!isset($mConsumX[$mVal[$sb]])){$mConsumX[$mVal[$sb]]=array();}
			$mConsumX[$mVal[$sb]][$mVal['index']]=$mVal;
		}
		reset($mConsum);

		if(in_array($sb,array('producte','productor','unitat_facturacio')))
		{
			ksort($mConsumX,SORT_STRING);
		}
		else
		{
			ksort($mConsumX,SORT_NUMERIC);
		}

		if($sbd=='descendent')
		{
			$mConsumX=array_reverse($mConsumX,true);
		}
	}
	reset($mSelRutes);

	
	return $mConsumX;
}

//------------------------------------------------------------------------

function db_getEstadistiquesCSV($db)
{
	global $mPars,$mConsumXcsv,$mSb,$mSelRutes,$sb,$sbd;
	
	$dir0=getcwd();
	$dir="docs".$mPars['selRutaSufix'];
	if(!@chdir($dir))
	{
		mkdir($dir);
	}
	else
	{
		chdir($dir0);
	}
	@unlink("docs".$mPars['selRutaSufix']."/estadistiques1.csv");

	if(!$fp=fopen("docs".$mPars['selRutaSufix']."/estadistiques1.csv",'w'))
	{
		return false;
	}

	$mH1=array('','resum sobre rutes: '.(implode(',',$mSelRutes)));
	fputcsv($fp, $mH1,',','"');
	$mH1=array('','ordenat per: '.$sb);
	fputcsv($fp, $mH1,',','"');
	$mH1=array('','en sentit: '.$sbd);
	fputcsv($fp, $mH1,',','"');
	$mH1=array('',date('d/m/Y hh:mm'));
	fputcsv($fp, $mH1,',','"');
	$mH1=array('');
	fputcsv($fp, $mH1,',','"');
	fputcsv($fp, $mSb,',','"');
	while(list($key,$mProducteLlista)=each($mConsumXcsv))
	{
    	if(!fputcsv($fp, $mProducteLlista,',','"'))
		{
			return false;
		}
	}	
	reset($mConsumXcsv);
	fclose($fp);
	return;
}

//------------------------------------------------------------------------


echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript'>
function enviarFpars(adressAndGet,sendTarget)
{
	document.getElementById('f_pars').action=adressAndGet;
	document.getElementById('f_pars').target=sendTarget;
	document.getElementById('f_pars').submit();
	document.getElementById('f_pars').action='comandes.php';
	document.getElementById('f_pars').target='_self';
		
	return;
}
</SCRIPT>
<head>
<title>Resum productes JJAA</title>
</head>
<body onLoad=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('resumProductesJJAA.php?');
echo "
	<table align='center' style='width:80%' bgcolor='".$mColors['table']."'>
		<tr>
			<td style='width:100%;' align='center'>
			<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
			".$mContinguts['index']['titol1']."</b>
			</td>
		</tr>
		<tr>
			<td style='width:100%;' align='center'>
			<br>
			<p>[ Resum Productes JJAA ] </p>
			<br>
";
mostrarSelectorRutaC('resumProductesJJAA.php');

if(count($mSelRutes)>0)
{
	$mConsumX=db_getConsum($db);
	html_consum();
	db_getEstadistiquesCSV($db);
}
else
{
	echo "(no s'han seleccionat rutes cac per a generar les dades)";
}
echo "
			<br>
			<br>
			</td>
		</tr>
	</table>

";
html_helpRecipient();
echo "
<form id='f_pars' name='f_pars' method='post' action=''>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
</form>
	
</body>
</html>

";

?>

		