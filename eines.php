<?php
function html_demo($fitxer)
{
// Coloca una linia dalt la p�gina per entrar o sortir del mode 'demo'



	global $mPars,$mModuls, $mUsuari;

	if
	(
		$mPars['nivell']=='sadmin'
		||
		$mPars['nivell']=='admin'
		||
		$mPars['nivell']=='coord'
		||
		$mPars['DEMO_nivell']=='sadmin'
		||
		$mPars['DEMO_nivell']=='admin'
		||
		$mPars['DEMO_nivell']=='coord'
		||
		(isset($mPars['idPerfilsUsuariResponsableChain']) && $mPars['idPerfilsUsuariResponsableChain']!='') //es responsable d'algun perfil llistes locals o CAB
		||
		(isset($mPars['idGrupsUsuariResponsableChain']) && $mPars['idGrupsUsuariResponsableChain']!='') //es responsable d'algun grup
		||
		(isset($mPars['DEMO_idPerfilsUsuariResponsableChain']) && $mPars['DEMO_idPerfilsUsuariResponsableChain']!='') //es responsable d'algun perfil llistes locals o CAB mode DEMO
		||
		(isset($mPars['DEMO_idGrupsUsuariResponsableChain']) && $mPars['DEMO_idGrupsUsuariResponsableChain']!='') //es responsable d'algun grup  mode DEMO
	)
	{
		if($mModuls['modeProves']==1)
		{
			$mEstatText=array();
			$mEstatText['1']="<font color='red'><b> ACTIU </b></font>";
			$mEstatText['-1']="";
			echo "
	<table align='center' style='width:100%' bgcolor='LightSteelBlue'>
					<tr>
						<td align='center'>
						<p style='font-size:12px;'>
						<input type='checkbox' 	";
			if($mPars['demo']==1)//demo
			{
				echo " CHECKED ";
				$value=1;
			} 
			else
			{
				$value=-1;
			} 
			echo "
									id='ck_demo' onClick=\"valDemo=this.value;valDemo=valDemo*(-1); enviarFpars('".$fitxer."demo='+valDemo,'_self');\" value='".$value."'> 
						MODE DE PROVES ".$mEstatText[$mPars['demo'].'']." &nbsp;".(html_ajuda1('eines.php',5))."
						</p>
						</td>
					</tr>
	</table>
			";
		}
	}

	return;
}
//------------------------------------------------------------------------------
function html_menuUsuari()
{
	global $mPars,$mColors;
	
	echo "
<table  width='80%' align='center' bgcolor='".$mColors['table']."'>
	<tr>
		<td  width='100%' valign='middle' align='center'  >
		<table  width='100%' align='center'>
			<tr>
				<td width='10%' align='center'>
				<img  src='imatges/casap.jpeg' title=\"Tornar a la p�gina d'inici\" style='cursor:pointer' onClick=\"javascript: enviarFpars('index.php?us=".$mPars['usuari_id']."&pasw=".$mPars['pasw']."','_self')\">
				</td>
				<td width='80%' align='left' valign='bottom'>
				<p class='compacte' >
				Usuaria:<b> ".(urldecode($mPars['usuari']))."</b> (".$mPars['nivell'].")
				</p>
				</td>
				<td width='10%' align='right'>
				<img src='imatges/out2p.jpg'  title=\"Sortir\" style='cursor:pointer' onClick=\"javascript: document.getElementById('i_pars').value='';enviarFpars('index.php','_self')\">
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td width='100%' height='1px' bgcolor='grey'>
		</td>
	</tr>
</table>
	";

	return;
}
//------------------------------------------------------------------------------
function mostrarSelectorMes($desti)
{
	global $mColors,$mMesosTrams,$mMesosResumContractes,$ruta,$mPars,$mMesos,$parsChain;
	echo "
	<form id='f_selMesos' method='POST' action='".$desti."' target='_self'>
	<table width='30%' align='center' bgcolor='".$mColors['table']."'>
		<tr>
			<td  width='40%' align='right'>
			<select class='seleccionada2' name='sel_mesos[]' multiple>
	";
	$selected='';
	while(list($index,$ruta_)=each($mMesosTrams))
	{
		if(in_array($ruta_,$mMesosResumContractes)){$selected='selected';}else{$selected='';}
		if($ruta_==date('ym',mktime(0,0,0,date('m'),date('d'),date('Y'))))//ruta en curs
		{
			$text=' (MES-EN-CURS)';
		}
		else
		{
			$text='';
		}
		echo "
			<option ".$selected." value='".$ruta_."'>".($mMesos[(substr($ruta_,2,2))])." ".(substr($ruta_,0,2)).$text."</option>
		";
	}
	reset($mMesosTrams);
	echo "
			</select>
			<center><input type='submit' value='enviar'></center>
			</td>
			
			<td  width='60%' align='left' valign='top'>
			<p class='p_micro'>
			* Selecciona un o varis mesos per mostrar en el resum nom�s les ofertes o demandes contractades i amb data sortida inclosa dins algun dels mesos seleccionats.
			</p>
			</td>
		</tr>
	</table>
	<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
	</form>
	";

	return;
}


//------------------------------------------------------------------------------
//*v36 5-1-16 modificat php dins select
//*v36.5-funcio
//*v36.2-funcio
function mostrarSelectorRuta($opt,$desti)
{
	global  $mAjuda,
			$mColors,
			$mRutesSufixes,
			$mPars,
			$mMesos,
			$mParametres,
			$mPropietatsGrup,
			$mPropietatsPeriodeLocal,
			$mPeriodesInfo;
	
	$mRutesSufixes_=array_reverse($mRutesSufixes);
	echo "
	<table  align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td   align='center'  valign='top'>
			<table><tr><td><p>Selecciona un periode:</p></td><td> ".(html_ajuda1('eines.php',2))."</td></tr></table>
			<select class='seleccionada2' name='sel_ruta' onChange=\"javascript:	if(this.value!=''){enviarFpars('".$desti."?sR='+this.value,'_self');}\">
";
	$selected='';
	while(list($index,$ruta_)=each($mRutesSufixes_))
	{
		$mRuta_=explode('_',$ruta_);
		if(count($mRuta_)>=2) //ruta especial
		{
			if($mRuta_[1]!='grups')
			{
				if($ruta_==$mPars['selRutaSufix']){$selected='selected';}else{$selected='';}
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp=$mRuta_[0];
				$color='blue';
				echo "
			<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[1],2,2))])." ".(substr($mRuta_[1],0,2)).$text."</option>
				";
			}
		}
		else
		{
			if($ruta_!='grups')
			{
				if($ruta_==$mPars['selRutaSufix']){$selected='selected';}else{$selected='';}
				$color='black';
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp='';
				echo "
					<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[0],2,2))])." ".(substr($mRuta_[0],0,2)).$text."</option>
				";
			}
		}
	}
	reset($mRutesSufixes_);
	echo "
			</select>
			</p>
			</td>
			
			
			<td valign='middle' align='left'>
			";
//*v36.5-condicio
			if($opt==1)
			{
				if($mPars['selLlistaId']==0)
				{
					if($mParametres['precomandaTancada']['valor']*1==1)
					{
						$imatgeSemafor='semafor_vermell.gif';
						$semaforText="Aquest periode de reserves a la llista de CAC est� TANCAT";
					}
					else
					{
						$imatgeSemafor='semafor_verd.gif';
						$semaforText="Aquest periode de reserves a la llista de la CAC est� OBERT";
					}
				}
				else
				{
					if($mPropietatsPeriodeLocal['comandesLocalsTancades']*1==1)
					{
						$imatgeSemafor='semafor_vermell.gif';
						$semaforText="Aquest periode de reserves a la llista LOCAL est� TANCAT";
					}
					else
					{
						$imatgeSemafor='semafor_verd.gif';
						$semaforText="Aquest periode de reserves a la llista LOCAL est� OBERT";
					}
				}
				
			echo "
			<a title=\"".$semaforText."\"><img src='imatges/".$imatgeSemafor."'></a>
			";
			}
			echo "
			</td>
		</tr>
	</table>
	";

	return;
}

//------------------------------------------------------------------------------
//*v36.5-funcio
//v37-funcio

function mostrarSelectorRuta2($opt,$desti)
{
	global $mColors,$mRutesSufixes,$mPars,$mMesos,$mParametres,$mPropietatsGrup,$mPropietatsPeriodeLocal,$mPeriodesInfo;
	
	$mRutesSufixes_=array_reverse($mRutesSufixes);
	echo "
	<table  align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td   align='center'  valign='top'>
			<p>
			Selecciona un periode:
			<br>
			<select class='seleccionada2' name='sel_ruta2' onChange=\"javascript:	if(this.value!=''){enviarFpars('".$desti."?sR='+this.value,'_self');}\">
";
	$selected='';
	while(list($index,$ruta_)=each($mRutesSufixes_))
	{
		$mRuta_=explode('_',$ruta_);
		if(count($mRuta_)>=2) //ruta especial
		{
			if($mRuta_[1]!='grups')
			{
				if($ruta_==$mPars['selRutaSufix']){$selected='selected';}else{$selected='';}
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp=$mRuta_[0];
				$color='blue';
				echo "
					<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[1],2,2))])." ".(substr($mRuta_[1],0,2)).$text."</option>
				";
			}
		}
		else
		{
			if($ruta_!='grups')
			{
				if($ruta_==$mPars['selRutaSufix']){$selected='selected';}else{$selected='';}
				$color='black';
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp='';
				echo "
					<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[0],2,2))])." ".(substr($mRuta_[0],0,2)).$text."</option>
				";
			}
		}
	}
	reset($mRutesSufixes_);
	echo "
					<option  value=''></option>
			</select>
			</p>
			</td>
			
			
			<td valign='middle' align='left'>
			";
//*v36.5-condicio
			if($opt==1)
			{
				if($mPars['selLlistaId']==0)
				{
					if($mParametres['precomandaTancada']['valor']*1==1)
					{
						$imatgeSemafor='semafor_vermell.gif';
						$semaforText="Aquest periode de reserves a la llista de CAC est� TANCAT";
					}
					else
					{
						$imatgeSemafor='semafor_verd.gif';
						$semaforText="Aquest periode de reserves a la llista de la CAC est� OBERT";
					}
				}
				else
				{
					if($mPropietatsPeriodeLocal['comandesLocalsTancades']*1==1)
					{
						$imatgeSemafor='semafor_vermell.gif';
						$semaforText="Aquest periode de reserves a la llista LOCAL est� TANCAT";
					}
					else
					{
						$imatgeSemafor='semafor_verd.gif';
						$semaforText="Aquest periode de reserves a la llista LOCAL est� OBERT";
					}
				}
				
			echo "
			<a title=\"".$semaforText."\"><img src='imatges/".$imatgeSemafor."'></a>
			";
			}
			echo "
			</td>
		</tr>
	</table>
	";

	return;
}

//------------------------------------------------------------------------------

function mostrarPerfilsTransferir($opt,$desti)
{
	global $mColors,$mRutesSufixes,$mPars,$mMesos,$mParametres,$mPropietatsGrup,$mPropietatsPeriodeLocal,$mPeriodesInfo;
	
		$mRutesSufixes_mem=$mRutesSufixes;
	$mRutesSufixes=extreureRutesNormals($mRutesSufixes);
	$mRutesSufixes_=array_reverse($mRutesSufixes);
	echo "
	<table  align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td   align='center'  valign='top'>
			<p>
			Selecciona un periode:
			<br>
			<select class='seleccionada2' id='sel_rutaOnTransferir' name='sel_rutaOnTransferir'>
";
	$selected='';
	while(list($index,$ruta_)=each($mRutesSufixes_))
	{
		$mRuta_=explode('_',$ruta_);
		if(count($mRuta_)>=2) //ruta especial
		{
			if($mRuta_[1]!='grups')
			{
				if($ruta_==$mPars['selRutaSufix']){$selected='selected';}else{$selected='';}
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp=$mRuta_[0];
				$color='blue';
				echo "
					<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[1],2,2))])." ".(substr($mRuta_[1],0,2)).$text."</option>
				";
			}
		}
		else
		{
			if($ruta_!='grups')
			{
				if($ruta_==$mPars['selRutaSufix']){$selected='selected';}else{$selected='';}
				$color='black';
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp='';
				echo "
					<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[0],2,2))])." ".(substr($mRuta_[0],0,2)).$text."</option>
				";
			}
		}
	}
	reset($mRutesSufixes_);
	echo "
					<option  value=''></option>
			</select>
			</p>
			</td>
			
			
			<td valign='middle' align='left'>
			";
//*v36.5-condicio
			if($opt==1)
			{
				if($mPars['selLlistaId']==0)
				{
					if($mParametres['precomandaTancada']['valor']*1==1)
					{
						$imatgeSemafor='semafor_vermell.gif';
						$semaforText="Aquest periode de reserves a la llista de CAC est� TANCAT";
					}
					else
					{
						$imatgeSemafor='semafor_verd.gif';
						$semaforText="Aquest periode de reserves a la llista de la CAC est� OBERT";
					}
				}
				else
				{
					if($mPropietatsPeriodeLocal['comandesLocalsTancades']*1==1)
					{
						$imatgeSemafor='semafor_vermell.gif';
						$semaforText="Aquest periode de reserves a la llista LOCAL est� TANCAT";
					}
					else
					{
						$imatgeSemafor='semafor_verd.gif';
						$semaforText="Aquest periode de reserves a la llista LOCAL est� OBERT";
					}
				}
				
			echo "
			<a title=\"".$semaforText."\"><img src='imatges/".$imatgeSemafor."'></a>
			";
			}
			echo "
			</td>
		</tr>
	</table>
	";
		$mRutesSufixes=$mRutesSufixes_mem;

	return;
}
//------------------------------------------------------------------------------
function mostrarSelectorLlista($desti)
{
	global $mPars,$mAjuda,$mColors,$mRutesSufixes;
	if ($mPars['selLlistaId']==$mPars['grup_id'] && $mPars['grup_id']!=0)
	{
		$selected0='';
		$selected1='selected';
	}
	else
	{
		$selected0='selected';
		$selected1='';
	}

	$mRutesSufixes=extreureRutesEspecials($mRutesSufixes);
	$lastRuta=array_pop($mRutesSufixes);

	echo "
	<table  align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td   align='left'  valign='top'>
			<p>
			<table><tr><td><p>Llista de productes:</p></p></td><td> ".(html_ajuda1('eines.php',1))."</td></tr></table>
			<select class='seleccionada2' id='sel_llista' name='sel_llista' onChange=\"javascript: sR='';val=this.value; if(val!='0'){sR='".$lastRuta."'}else{sR='".$mPars['selRutaSufix']."';}	enviarFpars('".$desti."?sL='+this.value+'&sR='+sR,'_self');\">
			<option ".$selected0." value='0'>CAC</option>
			";
			if($mPars['grup_id']!=0)
			{
				echo "
			<option ".$selected1." value='".$mPars['grup_id']."'>LOCAL</option>
				";
			}
			echo "
			</select>
			</p>
			</td>
		</tr>
	</table>
	";

	return;
}

//------------------------------------------------------------------------------
function mostrarSelectorLlistesLocals($desti)
{
	global $mPars,$mAjuda,$mLlistesRef, $mColors;


	echo "
	<table  align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td   align='left'  valign='top'>
			<p>
			<table><tr><td><p>Llistes locals:</p></p></td><td> ".(html_ajuda1('eines.php',3))."</td></tr></table>
			<select class='seleccionada2' id='sel_llista' name='sel_llista' onChange=\"javascript:	enviarFpars('".$desti."?sL='+this.value,'_self');\">
	";
	while(list($llistaId,$mLlista)=each($mLlistesRef))
	{
		if ($mPars['selLlistaId']==$llistaId)
		{
			$selected0='';
			$selected1='selected';
		}
		else
		{
			$selected0='selected';
			$selected1='';
		}

		echo "
			<option ".$selected1." value='".$llistaId."'>".(urldecode($mLlista['nom']))."</option>
		";
	}
	reset($mLlistesRef);
	echo "
			</select>
			</p>
			</td>
		</tr>
	</table>
	";

	return;
}
//------------------------------------------------------------------------------
//*v36 5-1-16 modificat php dins select
//*v36.5-funcio
//*v36.2-funcio
function mostrarSelectorPeriodeLocal($opt,$desti)
{
	global 	$mColors,
			$mPars,
			$mMesos,
			$mParametres,
			$mPropietatsPeriodeLocal,
			$mPeriodesInfo,
			$mPeriodesLocalsInfo,
			$mPropietatsPeriodesLocals,
			$mAjuda;
	//$mRutesSufixes_=array_reverse($mRutesSufixes);
	echo "
	<table  align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td   align='center'  valign='top'>
					<table><tr><td><p>Selecciona un periode local:</p></td><td> ".(html_ajuda1('eines.php',2))."</td></tr></table>
					<select class='seleccionada2' name='sel_periodeLocal' onChange=\"javascript:	if(this.value!=''){enviarFpars('".$desti."?sPl='+this.value,'_self');}\">
	";
	$selected1='';
	$selected2='selected';

	while(list($id,$mPeriodeLocal)=each($mPeriodesLocalsInfo))
	{
		if(isset($mPropietatsPeriodesLocals[$id]) && $id==$mPars['sel_periode_comanda_local']){$selected1='selected';$selected2='';}else{$selected1='';}
		
		
		if(!isset($mPropietatsPeriodesLocals[$id]))
		{
			$text=' (?)';
			$id='';
			$mPeriodeLocal['periode_comanda']='';
		}
		else if(@$mPropietatsPeriodesLocals[$id]['comandesLocalsTancades']==-1)
		{
			$text=' (OBERT)';
		}
		else if(@$mPropietatsPeriodesLocals[$id]['comandesLocalsTancades']==1)
		{
			$text='(TANCAT)';
		}
		else
		{
			$text='';
		}
		echo "
					<option style='' ".$selected1." value='".$id."'>".$mPeriodeLocal['periode_comanda']." (".$id.") ".$text."</option>
							";
	}
	reset($mPeriodesLocalsInfo);
	echo "
					<option style='' ".$selected2." value=''></option>
					</select>
					</p>
			</td>
		</tr>
	</table>
	";
	return;
}
//------------------------------------------------------------------------------
function mostrarSelectorPeriodeLocalInfo($opt) //gestioProductes.php llista local seleccionada
{
	global 	$mColors,
			$mAjuda,
			$mPars,
			$mMesos,
			$mParametres,
			$mPropietatsGrup,
			$mPeriodesInfo,
			$mPeriodesLocalsInfo,
			$mPropietatsPeriodesLocals;
	echo "
	<table  align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td valign='top'>
			<p>
			<table><tr><td><p>Periodes locals de la llista:</p></td><td> ".(html_ajuda1('eines.php',4))."</td></tr></table>
			<select class='seleccionada2'>
	";
	$selected1='';
	$selected2='selected';
	$cont=0;
	while(list($id,$mPeriodeLocal)=each($mPeriodesLocalsInfo))
	{
		
		if
		(
			$cont==0
			&&
			(
				!isset($mPars['sel_periode_comanda_local']) 
				|| 
				$mPars['sel_periode_comanda_local']==''
			)
		)
		{
			$mPars['sel_periode_comanda_local']==$id;
		}
		
		if
		(
			$id==$mPars['sel_periode_comanda_local']
		)
		{
			$selected1='selected';
			$selected2='';
			$class=" class='o_selected' ";
		}
		else
		{
			$selected1='';
			$class="";
		}
		
		if($mPropietatsPeriodesLocals[$id]['comandesLocalsTancades']==-1)
		{
			$text=' (OBERT)';
		}
		else if($mPropietatsPeriodesLocals[$id]['comandesLocalsTancades']==1)
		{
			$text='(TANCAT)';
		}
		else
		{
			$text='';
		}
		echo "
			<option DISABLED ".$class." style='' ".$selected1." value='".$id."'>".$mPeriodesLocalsInfo[$id]['periode_comanda'].' ('.$id.') '.$text."</option>
		";
		$cont++;
	}
	reset($mPeriodesLocalsInfo);
	echo "
			<option style='' ".$selected2." value=''></option>
			</select>
			</p>
			<p class='p_micro'>&nbsp;</p>
			</td>
			<td valign='top' align='left'>
			";
//*v36.5-condicio
			if($opt==1)
			{
				if
				(
					@$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['comandesLocalsTancades']*1==1
					||
					@$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['comandesLocalsTancades']*1==0
				)
				{
					$imatgeSemafor='semafor_vermell.gif';
					$semaforText="Aquest periode de reserves a la llista LOCAL est� TANCAT";
				}
				else
				{
					$imatgeSemafor='semafor_verd.gif';
					$semaforText="Aquest periode de reserves a la llista LOCAL est� OBERT";
				}
			
			echo "
			<a title=\"".$semaforText."\"><img src='imatges/".$imatgeSemafor."'></a>
			";
			}
			echo "
			</td>
		</tr>
	</table>
	";
	return;
}
//------------------------------------------------------------------------------
function mostrarSelectorLlistaPerProductores($desti,$destiLocal,$db) //gestioProductes.php
{
	global $mColors,$mPars,$mGrupsRef,$mPerfil,$mAjuda,$mLlistesRef,$mGrupsAssociatsRef;

	$selected1='';
	$selected2='selected';
	$afegirCac=true;
	$afegirNota1=true;
	
	echo "
	<table align='center'  bgcolor='".$mColors['table']."'>
		<tr>
			<td  align='center' valign='top'>
			<p>
			<table><tr><td><p>Selecciona una llista de productes:</p></td><td> ".(html_ajuda1('eines.php',1))."</td></tr></table>
			<select class='seleccionada2' id='sel_llista2' name='sel_llista2' onChange=\"javascript:desti=this.value;if(desti!='' && desti*1==0){enviarFpars('".$desti."?sL='+desti,'_self');}else if(desti!=''){enviarFpars('".$destiLocal."?sL='+desti,'_self');}\">
		";
			$color='black';
			$bgColor='white';
			$disabled='';
		
				$taulaProductes_mem=$mPars['taulaProductes'];
				$llistaId_mem=$mPars['selLlistaId'];

		while(list($llistaId,$mLlista)=each($mLlistesRef))
		{

			$color='black';
			$bgColor='white';
			
			if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
			{
				if(!@in_array($llistaId,$mGrupsAssociatsRef))
				{
					$textNivell="[".$mPars['nivell']."]";
				}
				else
				{
					$textNivell='';
					$color='black';
					$bgColor='LightOrange';
				}
			}
			else
			{
				$textNivell='';
			}
	

			if($llistaId_mem==$llistaId)
			{
				$selected1='selected';
				$selected2='';
			}
			else
			{
				$selected1='';
			}
			
			if($llistaId==0)
			{
				$mPars['taulaProductes']='productes_'.$mPars['selRutaSufix'];
			}
			else
			{
				$mPars['taulaProductes']='productes_grups';
				$mPars['selLlistaId']=$llistaId;
			}
				
			if($llistaId!=0)
			{
					$mPars['selLlistaId']=$llistaId;
				$mPeriodesLocalsInfo=db_getPeriodesLocalsInfo($db);
				if(count($mPeriodesLocalsInfo)>0)
				{
					$numProductes=db_getNumProductesPerfilAllista($mPerfil['id'],$llistaId,$db);
					if($numProductes>0){$color='white'; $bgColor='green';}

					echo "
			<option id='opt_selLlista_".$llistaId."' ".$selected1." ".$disabled." value='".$llistaId."' style='color:".$color."; backGround-color:".$bgColor.";'>".(urldecode($mGrupsRef[$llistaId]['nom']))." (".$numProductes." productes) </option>
					";
					$numProductes=0;
				}
			}
			else
			{
				$numProductes=db_getNumProductesPerfilAllista($mPerfil['id'],$llistaId,$db);
				if($numProductes>0){$color='white'; $bgColor='green';}

				echo "
			<option id='opt_selLlista_".$llistaId."' ".$selected1." ".$disabled." value='".$llistaId."' style='color:".$color."; backGround-color:".$bgColor.";'>".(urldecode($mGrupsRef[$llistaId]['nom']))." (".$numProductes." productes) </option>
				";
				$numProductes=0;
			}
		}
		reset($mLlistesRef);
				
				$mPars['taulaProductes']=$taulaProductes_mem;
				$mPars['selLlistaId']=$llistaId_mem;
		
		echo "
			<option ".$selected2." value=''></option>
			</select>
			</p>
		";
		if($afegirNota1)
		{
			echo "
				<p class='p_micro3'>
				* s'indica en <font color='DarkGreen'><b>verd</b></font> el grup que t� productes de 
				<br><b>".(urldecode($mPerfil['projecte']))."</b> a la seva llista local (a part de a la CAC)
				</p>
			";
		}
		echo "
			</td>
		</tr>
	</table>
	";
	

	return;
}

//------------------------------------------------------------------------------

function posaColor1($cadena)
{
	global $mEtiquetes;
	
	for($i=0;$i<count($mEtiquetes);$i++)
	{
		$cadena=str_replace($mEtiquetes[$i]['nom'],"<font style='color: ".$mEtiquetes[$i]['color']."'>".$mEtiquetes[$i]['nom']."</font>",urldecode($cadena));
		//$cadena=str_replace(urlencode($mEtiquetes[$i]['nom']),"<font style='color: ".$mEtiquetes[$i]['color']."'>".$mEtiquetes[$i]['nom']."</font>",$cadena);
	}
	$cadena=str_replace(',','<br>',$cadena);
	
	return $cadena;
}



function vd($var)
{
	echo "<pre>";
	var_dump($var);
	echo "</pre>";

	return;
}

function vdj($var)
{
	echo "
	<script>
	var vdj=\"";
	var_dump($var);
	echo "\";
	</script>";

	return;
}

function f_reset($mPars)
{
	while(list($key,$val)=each($mPars))
	{
		$mPars[$key]='';
	}
	reset($mPars);

	return $mPars;
}

function afegir_dc_a_valors($mProductesComandaRebost)
{
	$mProductesComandaRebostTmp=array();
	
	while(list($key,$val)=each($mProductesComandaRebost))
	{
		while(list($key2,$val2)=each($val))
		{
			$mProductesComandaRebostTmp[$key][$key2]='"'.$val2.'"';
		}
	}
	reset($mProductesComandaRebost);


	return $mProductesComandaRebostTmp;
}

function getComandaMinima($mComanda)
{
	global $mParsCAC,$totalKmRuta,$mQuantitatsRebost;
	
	$comandaMinimaCalc=0;
	$quantitat=0;
	
	$mParsCAC['desplasamentEuros']=@number_format($mParsCAC['cost_transport_r_euros']/$totalKmRuta*$mParsCAC['distancia_anclatge']*2,2);
	$mParsCAC['�/km']=@number_format($mParsCAC['cost_transport_r_euros']/$totalKmRuta,2);


	$total=0;
	while(list($key,$val)=each($mQuantitatsRebost))
	{
		$total+=$val;
	}
	$mParsCAC['total_quantitat_rebost']=$total;
	return ;
}

function html_notesHTML()
{
	echo "
		<table align='center'>
		<tr>
		<td>
	<br>
		<p class='p_micro'>* <b>Notes per a l'edici� (HTML)</b>:
		<br>
		<br>
		- per fer text en negreta:
		<br>&#60;b&#62;<b>negreta</b>&#60;/b&#62;
		<br>
		<br>
		- per fer text en it�l.lica:
		<br>&#60;i&#62;<i>it�l.lica</i>&#60;/i&#62;
		<br>
		<br>
		- per fer text en subratllat:
		<br>&#60;u&#62;<u>subratllat</u>&#60;/u&#62;
		<br>
		<br>
		- per fer un canvi de linia:
		<br>linia1&#60;br&#62;<br>linia2
		</p>
		</td>
		</tr>
		</table>
	";
	
	return;
}

function estaDinsIntervalMesos($mMesos,$dataSortidaTram)
{
	while(list($key,$mesAny)=each($mMesos))
	{
		$mes=substr($mesAny,0,2);
		$any=substr($mesAny,2,4);
		if(substr($dataSortidaTram,2,5)==$mes.'-'.$any){return true;}
	}
	reset($mMesos);
	
	return false;
}

//resumOfertesDemandes.php

function getResumUsuaris($mContractes)
{
	global $mPars,$mResumUsuaris,$mTram,$mVehicles,$mParametres;

	
	if($mTram['tipus']=='OFERTES')
	{
		if($mTram['usuari_id']==$mPars['usuari_id']) //ofertes meves
		{
			$usuariId=$mContractes['usuari_id'];
			$vehicleId=$mTram['vehicle_id'];
			$cobrar=1;
		}
		else //ofertes d'altres
		{
			$usuariId=$mTram['usuari_id'];
			$vehicleId=$mTram['vehicle_id'];
			$cobrar=-1;
		}
	}
	else if($mTram['tipus']=='DEMANDES')
	{
		if($mTram['usuari_id']==$mPars['usuari_id']) //demandes meves
		{
			$usuariId=$mContractes['usuari_id'];
			$vehicleId=$mContractes['vehicle_id'];
			$cobrar=-1;
		}
		else //demandes d'altres
		{
			$usuariId=$mTram['usuari_id'];
			$vehicleId=$mContractes['vehicle_id'];
			$cobrar=1;
		}
	}

	if(!isset($mResumUsuaris[$usuariId])){	$mResumUsuaris[$usuariId]=array();}

	if(!isset($mResumUsuaris[$usuariId][$vehicleId])) 
	{	$mResumUsuaris[$usuariId][$vehicleId]=array();}

	if(!isset($mResumUsuaris[$usuariId][$vehicleId]['tKm'])) 
	{	$mResumUsuaris[$usuariId][$vehicleId]['tKm']=0;}

	$mResumUsuaris[$usuariId][$vehicleId]['tKm']+=1*$mTram['km'];

	if(!isset($mResumUsuaris[$usuariId][$vehicleId]['tUms'])) 
	{	$mResumUsuaris[$usuariId][$vehicleId]['tUms']=0;}

	if(!isset($mResumUsuaris[$usuariId][$vehicleId]['tEcos'])) 
	{	$mResumUsuaris[$usuariId][$vehicleId]['tEcos']=0;}

	if(!isset($mResumUsuaris[$usuariId][$vehicleId]['tEuros'])) 
	{	$mResumUsuaris[$usuariId][$vehicleId]['tEuros']=0;}

	if($cobrar==1)
	{
		if($usuariId==417 || $mContractes['usuari_id']==417)
		{
			$mResumUsuaris[$usuariId][$vehicleId]['ums']=$mTram['km']*$mParametres['tarifaCac']['valor'];
		}
		else
		{
			$mResumUsuaris[$usuariId][$vehicleId]['ums']=			
						$mContractes['quantitat_pes']*$mTram['preu_pes']
						+$mContractes['quantitat_volum']*$mTram['preu_volum']
						+$mContractes['quantitat_places']*$mTram['preu_places'];
		}	
		$mResumUsuaris[$usuariId][$vehicleId]['tUms']+=$mResumUsuaris[$usuariId][$vehicleId]['ums'];

		$mResumUsuaris[$usuariId][$vehicleId]['ecos']=$mResumUsuaris[$usuariId][$vehicleId]['ums']*$mTram['pc_ms']/100;
		$mResumUsuaris[$usuariId][$vehicleId]['tEcos']+=$mResumUsuaris[$usuariId][$vehicleId]['ecos'];

		$mResumUsuaris[$usuariId][$vehicleId]['euros']=$mResumUsuaris[$usuariId][$vehicleId]['ums']-$mResumUsuaris[$usuariId][$vehicleId]['ecos'];
		$mResumUsuaris[$usuariId][$vehicleId]['tEuros']+=$mResumUsuaris[$usuariId][$vehicleId]['euros'];
	}
	else if($cobrar==-1)
	{
		if($usuariId==417 || $mContractes['usuari_id']==417)
		{
			$mResumUsuaris[$usuariId][$vehicleId]['ums']=$mTram['km']*$mParametres['tarifaCac']['valor'];
		}
		else
		{
			$mResumUsuaris[$usuariId][$vehicleId]['ums']=			
						$mContractes['quantitat_pes']*$mTram['preu_pes']
						+$mContractes['quantitat_volum']*$mTram['preu_volum']
						+$mContractes['quantitat_places']*$mTram['preu_places'];
		}			
		
		$mResumUsuaris[$usuariId][$vehicleId]['tUms']-=$mResumUsuaris[$usuariId][$vehicleId]['ums'];
	
		$mResumUsuaris[$usuariId][$vehicleId]['ecos']=$mResumUsuaris[$usuariId][$vehicleId]['ums']*$mTram['pc_ms']/100;
		$mResumUsuaris[$usuariId][$vehicleId]['tEcos']-=$mResumUsuaris[$usuariId][$vehicleId]['ecos'];

		$mResumUsuaris[$usuariId][$vehicleId]['euros']=$mResumUsuaris[$usuariId][$vehicleId]['ums']-$mResumUsuaris[$usuariId][$vehicleId]['ecos'];
		$mResumUsuaris[$usuariId][$vehicleId]['tEuros']-=$mResumUsuaris[$usuariId][$vehicleId]['euros'];
	}

	return $mResumUsuaris;
}

//------------------------------------------------------------------------------
function suma1MesAdata($iniciRutaAbastiment)
{
	global $mPars;
	
	$mIniciRutaAbastiment['Y']=date('Y',strtotime($iniciRutaAbastiment));
	$mIniciRutaAbastiment['m']=date('m',strtotime($iniciRutaAbastiment));
	$mIniciRutaAbastiment['d']=date('d',strtotime($iniciRutaAbastiment));
		
	$oIniciRutaAbastiment= new DateTime();
	$oIniciRutaAbastiment->setDate($mIniciRutaAbastiment['Y'], $mIniciRutaAbastiment['m']+1, $mIniciRutaAbastiment['d'] );

	$ts1=$oIniciRutaAbastiment->getTimestamp();

	$iniciRutaAbastiment_=date('d-m-Y',$ts1); //10-12-2015
	
	return $iniciRutaAbastiment_;
}
//------------------------------------------------------------------------------
function getRevisionsRef($cadenaRevisions)
{
	global $mPars, $recepcionsPendents, $enviamentsPendents;
	
	$mRevisionsRef=array();
	
	$mRevisions_=explode('][',$cadenaRevisions);
	$n=0;
	while(list($key,$cadenaRevisio)=each($mRevisions_))
	{
		$cadenaRevisio=str_replace('[','',$cadenaRevisio);
		$cadenaRevisio=str_replace(']','',$cadenaRevisio);
		$mRevisio=explode(';',$cadenaRevisio);
		$mRevisionsRef[@$mRevisio[1]]=$mRevisio;
		if(@$mRevisio[9]=='pendent' && @$mRevisio[3]==$mPars['grup_id'])
		{
			$enviamentsPendents++;
		}
		else if(@$mRevisio[9]=='pendent' && @$mRevisio[8]==$mPars['grup_id'])
		{
			$recepcionsPendents++;
		}
	}
	reset($mRevisions_);
	//$mRevisions=array_reverse($mRevisions);
	
	return $mRevisionsRef;
}


//------------------------------------------------------------------------------
function getProductesJaEntregatsZones($db)
{
	global $mPars,$mProductesJaEntregats,$mGrupsPerSzIzonaRef,$mRebostsRef,$mGrupsZones;
	
	
	$mProductesJaEntregatsZones=$mGrupsPerSzIzonaRef;
//*v36.2-condicio
	if($mProductesJaEntregats)
	{

	while(list($producteId,$mProductesJaEntregatsPEs)=each($mProductesJaEntregats))
	{
		while(list($puntEntrega,$mProductesJaEntregatsPE)=each($mProductesJaEntregatsPEs))
		{
			while(list($grupId,$mProductesJaEntregatsGrup)=each($mProductesJaEntregatsPE))
			{
				while(list($incId,$mProducteJaEntregat)=each($mProductesJaEntregatsGrup))
				{
					if(@substr_count($mRebostsRef[$puntEntrega]['categoria'],',2-'))
					{
						$z=substr($mRebostsRef[$puntEntrega]['categoria'],strpos($mRebostsRef[$puntEntrega]['categoria'],',2-')+3);
						$z=substr($z,0,strpos($z,','));
						$sz=getSuperZona($z);
					}
					else
					{	
						$z='';
						$sz='';
					}
					if(isset($mProductesJaEntregatsZones[$sz][$z]) && array_key_exists($puntEntrega,$mProductesJaEntregatsZones[$sz][$z]))
					{
						$mProductesJaEntregatsZones[$sz][$z][$puntEntrega][$producteId]=$mProducteJaEntregat['rebut'];
					}
					else
					{
						$mProductesJaEntregatsZones[$sz][$z][$puntEntrega][$producteId]=0;
					}
				}
				reset($mProductesJaEntregatsGrup); 
			}
			reset($mProductesJaEntregatsPE); 
		}	
		reset($mProductesJaEntregatsPEs); 
	}
	reset($mProductesJaEntregats); 
	
	}	
	return $mProductesJaEntregatsZones;
}

//------------------------------------------------------------------------------
function getProductesJaEntregatsTOT($mProductesJaEntregats)
{
	global $mRebostsRef;
	
	$mProductesJaEntregatsTot=array();
	
	while(list($producteId,$mProductesJaEntregatsPEs)=each($mProductesJaEntregats))
	{
		if(!isset($mProductesJaEntregatsTot[$producteId])){$mProductesJaEntregatsTot[$producteId]=0;}
		
		while(list($puntEntrega,$mProductesJaEntregatsPE)=each($mProductesJaEntregatsPEs))
		{
			while(list($grupId,$mProductesJaEntregatsGrup)=each($mProductesJaEntregatsPE))
			{
				while(list($incId,$mProducteJaEntregat)=each($mProductesJaEntregatsGrup))
				{
					$mProductesJaEntregatsTot[$producteId]+=$mProducteJaEntregat['rebut'];
					
					if(substr_count($mRebostsRef[$puntEntrega]['categoria'],',2-'))
					{
						$z=substr($mRebostsRef[$puntEntrega]['categoria'],strpos($mRebostsRef[$puntEntrega]['categoria'],',2-')+3);
						$z=substr($z,0,strpos($z,','));
						$sz=getSuperZona($z);
					}
					else
					{	
						$z='';
						$sz='';
					}

					if(isset($mProductesJaEntregatsZones[$sz][$z]) && array_key_exists($puntEntrega,$mProductesJaEntregatsZones[$sz][$z]))
					{
						$mProductesJaEntregatsZones[$sz][$z][$puntEntrega][$producteId]=$mProducteJaEntregat['rebut'];
					}
					else
					{
						$mProductesJaEntregatsZones[$sz][$z][$puntEntrega][$producteId]=0;
					}
				}
				reset($mProductesJaEntregatsGrup); 
			}
			reset($mProductesJaEntregatsPE); 
		}	
		reset($mProductesJaEntregatsPEs); 
	}
	reset($mProductesJaEntregats); 
	
	return $mProductesJaEntregatsTot;
}

//------------------------------------------------------------------------------
function getProductesJaEntregatsZona($mProductesJaEntregats,$producteId,$zona)
{
	global $mRebostsRef;
	
	$quantitat=0;
		
	if(isset($mProductesJaEntregats[$producteId]))
	{
		while(list($puntEntrega,$mProductesJaEntregatsPE)=each($mProductesJaEntregats[$producteId]))
		{
			if(substr_count($mRebostsRef[$puntEntrega]['categoria'],',2-'))
			{
				$z=substr($mRebostsRef[$puntEntrega]['categoria'],strpos($mRebostsRef[$puntEntrega]['categoria'],',2-')+3);
				$z=substr($z,0,strpos($z,','));
				$sz=getSuperZona($z);
			}
			else
			{	
				$z='';
				$sz='';
			}
			if($z==$zona)
			{
				while(list($grupId,$mProductesJaEntregatsGrup)=each($mProductesJaEntregatsPE))
				{
					while(list($incId,$mProducteJaEntregat)=each($mProductesJaEntregatsGrup))
					{
						$quantitat+=$mProducteJaEntregat['rebut'];
					}
					reset($mProductesJaEntregatsGrup); 
				}
				reset($mProductesJaEntregatsPE); 
			}
		}	
		reset($mProductesJaEntregats[$producteId]); 
	}
	
	return $quantitat;
}
//------------------------------------------------------------------------------
function getPropietats($cadena)
{
	$mPropietats_=array();
	$mPropietats=array();
	
	$cadena=substr($cadena,1,strlen($cadena)-2);
	$mPropietats_=explode(';',$cadena);
	while(list($key,$val)=each(	$mPropietats_))
	{
		$propietat=substr($val,0,strpos($val,':'));
		$valor=substr($val,strpos($val,':')+1);
		$mPropietats[$propietat]=$valor;
	}
	reset($mPropietats_);
	return $mPropietats;
	
}

//------------------------------------------------------------------------------
function makePropietats($mPropietats)
{
	$propietats='{';
	
	while(list($key,$val)=each($mPropietats))
	{
		$propietats.=$key.':'.$val.';';
	}
	reset($mPropietats);

	$propietats.='}';

	return $propietats;
	
}
//------------------------------------------------------------------------------
function getPropietatsGrup($cadena)
{
	global $mPropietatsGrupConfig,$mGrup;
	
	$mPropietats_=array();
	$mPropietats=array();
	
	$cadena=substr($cadena,1,strlen($cadena)-2);
	$mPropietats_=explode(';',$cadena);
	while(list($key,$val)=each(	$mPropietats_))
	{
		$propietat=substr($val,0,strpos($val,':'));
		$valor=substr($val,strpos($val,':')+1);
		$mPropietats[$propietat]=$valor;
	}
	reset($mPropietats_);

	while(list($key,$val)=each($mPropietatsGrupConfig))
	{
		if(!isset($mPropietats[$key]))
		{
			$mPropietats[$key]=$val;
		}
	}
	reset($mPropietatsGrupConfig);
	

	return $mPropietats;
	
}

//------------------------------------------------------------------------------
function makePropietatsGrup($mPropietats)
{
	global $mPropietatsGrupConfig;
	
	
	$propietats='{';
	
	while(list($key,$val)=each($mPropietatsGrupConfig))
	{
		if(!isset($mPropietats[$key]))
		{
			$propietats.=$key.':'.$val.';';
		}
		else
		{
			$propietats.=$key.':'.$mPropietats[$key].';';
		}
	}
	reset($mPropietatsGrupConfig);

	$propietats.='}';

	return $propietats;
	
}

//------------------------------------------------------------------------------
function getPropietatsPeriodeLocal($cadena)
{
	global $mPropietatsPeriodeLocalConfig,$mGrup;
	
	$mPropietats_=array();
	$mPropietats=array();
	
	$cadena=substr($cadena,1,strlen($cadena)-2);
	$mPropietats_=explode(';',$cadena);
	while(list($key,$val)=each(	$mPropietats_))
	{
		$propietat=substr($val,0,strpos($val,':'));
		$valor=substr($val,strpos($val,':')+1);
		$mPropietats[$propietat]=$valor;
	}
	reset($mPropietats_);

	while(list($key,$val)=each($mPropietatsPeriodeLocalConfig))
	{
		if(!isset($mPropietats[$key]))
		{
			$mPropietats[$key]=$val;
		}
	}
	reset($mPropietatsPeriodeLocalConfig);
	
	if($mPropietats['apeLocal']=='')
	{
		$mPropietats['apeLocal']=@$mGrup['adressa'].'('.@$mGrup['municipi'].')';
	}

	return $mPropietats;
	
}

//------------------------------------------------------------------------------
function makePropietatsPeriodeLocal($mPropietats)
{
	global $mPropietatsPeriodeLocalConfig;
	
	
	$propietats='{';
	
	while(list($key,$val)=each($mPropietatsPeriodeLocalConfig))
	{
		//if($val!='')
		//{
			if(!isset($mPropietats[$key]))
			{
				$propietats.=$key.':'.$val.';';
			}
			else
			{
				$propietats.=$key.':'.$mPropietats[$key].';';
			}
		//}
	}
	reset($mPropietatsPeriodeLocalConfig);

	$propietats.='}';

	return $propietats;
	
}

//------------------------------------------------------------------------------
function getPropietatsSegellPerfil($cadena)
{
	global $mPropietatsSegellPerfilConfig;

	$mPropietats_=array();
	$mPropietats=array();
	$mPropietatsSegellPerfilConfig_=$mPropietatsSegellPerfilConfig;
	
	if($cadena!='')
	{
		$cadena=substr($cadena,8,strlen($cadena)-3); //id:SEGELL=
		$mPropietats_=explode(';',$cadena);
		while(list($key,$val)=each($mPropietats_))
		{
			if($val!='')
			{
				$index=substr($val,0,strpos($val,':'));
				$valor=substr($val,strpos($val,':')+1);
				if($index!='' && $valor!='')
				{
					$mPropietats[$index]['estat']=$valor;
				}
			}
		}
		reset($mPropietats_);
	}
	else
	{
		$mPropietats=$mPropietatsSegellPerfilConfig;
	}
	while(list($key,$mVal)=each($mPropietats))
	{
		$mPropietatsSegellPerfilConfig_[$key]['estat']=$mVal['estat'];
	}
	reset($mPropietats);
	return $mPropietatsSegellPerfilConfig_;
	
}

//------------------------------------------------------------------------------
function makePropietatsSegellPerfil($mPropietats)
{
	
	$propietats='{SEGELL=';
	
	while(list($key,$mVal)=each($mPropietats))
	{
		$propietats.=$key.':'.$mVal['estat'].';';
	}
	reset($mPropietats);

	$propietats.='}';

	return $propietats;
	
}
//------------------------------------------------------------------------------
function getPropietatsSegellProducte($cadena)
{
	global $mPropietatsSegellProducteConfig;

	$mPropietats_=array();
	$mPropietats=array();
	$mPropietatsSegellProducteConfig_=$mPropietatsSegellProducteConfig;
	if($cadena!='')
	{
		$cadena=substr($cadena,8,strlen($cadena)-3);//id:{SEGELL=
		$mPropietats_=explode(';',$cadena);
		while(list($key,$val)=each($mPropietats_))
		{
			if($val!='')
			{
				$index=substr($val,0,strpos($val,':'));
				$valor=substr($val,strpos($val,':')+1);
				if($index!='' && $valor!='')
				{
					$mPropietats[$index]['estat']=$valor;
				}
			}
		}
		reset($mPropietats_);
	}
	else
	{
		$mPropietats=$mPropietatsSegellProducteConfig;
	}
	while(list($key,$mVal)=each($mPropietats))
	{
		$mPropietatsSegellProducteConfig_[$key]['estat']=$mVal['estat'];
	}
	reset($mPropietats);
	return $mPropietatsSegellProducteConfig_;
	
}

//------------------------------------------------------------------------------
function makePropietatsSegellProducte($mPropietats)
{
	$propietats='{SEGELL=';
	
	
	while(list($key,$mVal)=each($mPropietats))
	{
		$propietats.=$key.':'.$mVal['estat'].';';
	}
	reset($mPropietats);

	$propietats.='}';
	return $propietats;
	
}

//------------------------------------------------------------------------------
function getValorMaxSegell($mPropietatsSegell,$mCategoria0)
{

	$valor=0;
	while(list($index,$mPropietat)=each($mPropietatsSegell))
	{
		if($mPropietat['categoria0']=='tots' || array_key_exists($mPropietat['categoria0'],$mCategoria0))
		{
			$valor+=$mPropietat['valor'];
		}
	}
	reset($mPropietatsSegell);
	
	if($valor==0){$valor=1;}
	return $valor;
}

//------------------------------------------------------------------------------
function getValorMaxSegellTot($mCategoria0)
{
	global $mPropietatsSegellProducteConfig,$mPropietatsSegellPerfilConfig;
	
	
	$valor=0;
	while(list($index,$mPropietat)=each($mPropietatsSegellProducteConfig))
	{
	
		if($mPropietat['categoria0']=='tots' || array_key_exists($mPropietat['categoria0'],$mCategoria0))
		{
			$valor+=$mPropietat['valor'];
		}
	}
	reset($mPropietatsSegellProducteConfig);
	while(list($index,$mPropietat)=each($mPropietatsSegellPerfilConfig))
	{
		if($mPropietat['categoria0']=='tots' || array_key_exists($mPropietat['categoria0'],$mCategoria0))
		{
			$valor+=$mPropietat['valor'];
		}
	}
	reset($mPropietatsSegellPerfilConfig);
	
	if($valor==0){$valor=1;}
	return $valor;
}

//------------------------------------------------------------------------------
function getValorSegell($mPropietatsSegell,$mCategoria0)
{
	$valor=0;
	while(list($index,$mPropietat)=each($mPropietatsSegell))
	{
		if($mPropietat['categoria0']=='tots' || array_key_exists($mPropietat['categoria0'],$mCategoria0))
		{
			$valor+=$mPropietat['estat']*$mPropietat['valor'];
		}
	}
	reset($mPropietatsSegell);
	
	if($valor==0){$valor=1;}
	
	return $valor;
}



//------------------------------------------------------------------------------
function dir_getBasesGrup()
{
	global $mPars;
	
	$mBasesGrups=scandir('basesGrups');
	ksort($mBasesGrups);
	rsort($mBasesGrups);
	while(list($key,$periode)=each($mBasesGrups))
	{
		if(is_file('basesGrups/'.$periode.'/'.$mPars['grup_id'].'.pdf'))
		{
			return $periode;
		}
	}
	reset($mBasesGrups);

	return false;
}
//------------------------------------------------------------------------------
function dir_getBasesGrups()
{
	global $mPars,$mGrupsRef;
	
	$mPeriodeGrups=array();

	$mBasesGrups=scandir('basesGrups');
	ksort($mBasesGrups);
	rsort($mBasesGrups);

	while(list($grupId,$mGrup)=each($mGrupsRef))
	{
		$mPeriodeGrups[$grupId]='';
		while(list($key,$periode)=each($mBasesGrups))
		{
			if(is_file('basesGrups/'.$periode.'/'.$grupId.'.pdf'))
			{
				$mPeriodeGrups[$grupId]=$periode;
				//break;
			}
		}
		reset($mBasesGrups);
	}
	reset($mGrupsRef);
	

	return $mPeriodeGrups;
}
//------------------------------------------------------------------------------
function obtenirCadenaReset($resum)
{
	global $mInventariG;
	
	$cadenaReset='';
	while(list($key,$mInventariProducte)=each($mInventariG['inventari']))
	{
		$cadenaReset.="producte_".$mInventariProducte['index'].":0;";
	}
	reset($mInventariG['inventari']);

	return $cadenaReset;
}

//------------------------------------------------------------------------------
function getUsuarisGrupNoCom()	// retorna num usuaris no membres de comissio, o membres de comissio amb recolzament de zero ecos
{
	global $mUsuarisGrupRef;
	
	$numUsuarisGrupNoCom=0;
	
	while(list($key,$mUsuariGrupRef)=each($mUsuarisGrupRef))
	{
		if(isset($mUsuariGrupRef['propietats']) && $mUsuariGrupRef['propietats']!='')
		{
			$mPropietats=explode(';',$mUsuariGrupRef['propietats']);
			if
			(
				(
					$mUsuariGrupRef['id']!='' 
					&& 
					!isset($mPropietats['comissio']
				) 
				|| 
				$mPropietats['comissio']='')
				||
				(
					!isset($mComptesUsuari['saldoEcosR'])
					||
					$mComptesUsuari['saldoEcosR']==0
					||
					$mComptesUsuari['saldoEcosR']==''
				)
			)
			{
				$numUsuarisGrupNoCom++;
			}
		}
		else
		{
				$numUsuarisGrupNoCom++;
		}
	}
	reset($mUsuarisGrupRef);
	
	if($numUsuarisGrupNoCom==0){$numUsuarisGrupNoCom=1;}

	return $numUsuarisGrupNoCom;
}

//------------------------------------------------------------------------------
function getRecolzamentComandesUsuari($utilitzatEcosRchain)
{
	global $mPars;
	$mRecolzamentComandesUsuari=array();
	$mRecolzamentComandesUsuari_=explode(',',$utilitzatEcosRchain);
	
	
	while(list($key,$val)=each($mRecolzamentComandesUsuari_))
	{
		if($val!='')
		{
			$idComanda=substr($val,0,strpos($val,'-'));
			$ecosRecolzatsComanda=str_replace($idComanda.'-','',$val);
			//$ecosRecolzatsComanda=substr($val,strpos($val,':'+1));
			//echo "<br>".$idComanda.":".$ecosRecolzatsComanda;
			$mRecolzamentComandesUsuari[$idComanda]=$ecosRecolzatsComanda;
		}
	}
	reset($mRecolzamentComandesUsuari_);
	return $mRecolzamentComandesUsuari;
}

//------------------------------------------------------------------------------
function makeChainRecolzamentComandesUsuari($mRecolzamentComandesUsuari)
{
	global $mPars,$sumaRecolzamentComandesSelUsuari;

	$chain='';
	
	while(list($idComanda,$ecosRecolzatsComanda)=each($mRecolzamentComandesUsuari))
	{
		if(isset($idComanda) && $idComanda!='' && isset($ecosRecolzatsComanda) && $ecosRecolzatsComanda!='')
		{
			$chain.=$idComanda."-".$ecosRecolzatsComanda.",";
			$sumaRecolzamentComandesSelUsuari+=$ecosRecolzatsComanda;
		}
	}
	reset($mRecolzamentComandesUsuari);

	return $chain;
}

//------------------------------------------------------------------------------
function getIdsUsuarisGrupChain()
{
	global $mUsuarisGrupRef;
	
	$idUsuarisGrupChain='';

	while(list($usuariId,$mUsuariGrupRef)=each($mUsuarisGrupRef))
	{
		$idUsuarisGrupChain.=','.$usuariId.",";
	}
	reset($mUsuarisGrupRef);

	return $idUsuarisGrupChain;
}

//------------------------------------------------------------------------------
function db_getUtilitzatAquiEcosRmembresComissio()
{
	global $mPars, $mUsuarisGrupRef, $mComptesUsuarisGrupRef;
	
	$totalUtilitzatAquiEcosRmembresComissio=0;


	while(list($usuariId,$mComptesUsuariGrupRef)=each($mComptesUsuarisGrupRef))
	{
		$mPropietats=getPropietats($mUsuarisGrupRef[$usuariId]['usuari']['propietats']);
		if(isset($mPropietats['comissio']) && $mPropietats['comissio']!='' && $mComptesUsuariGrupRef['saldo_ecos_r']>0)
		{
			//$totalUtilitzatAquiEcosRmembresComissio+=0;
			if(isset($mUsuarisGrupRef[$usuariId]['comanda'][$mPars['grup_id']]['id']))
			{
				$mRecolzamentComandesUsuari=getRecolzamentComandesUsuari($mComptesUsuariGrupRef['utilitzat_ecos_r_chain']);
				
				$totalUtilitzatAquiEcosRmembresComissio+=$mRecolzamentComandesUsuari[$mUsuarisGrupRef[$usuariId]['comanda'][$mPars['grup_id']]['id']];
			}
		}
	}
	reset($mComptesUsuarisGrupRef);

	return $totalUtilitzatAquiEcosRmembresComissio;
}

//------------------------------------------------------------------------------
function afegirPerfilsVinculats($mGrupsProductorsRef)
{
	global $mPerfilsRef;

	while(list($grupId,$mGrup)=each($mGrupsProductorsRef))
	{
		$mGrupsProductorsRef[$grupId]['perfil_vinculat']='';
		while(list($perfilId,$mPerfil)=each($mPerfilsRef))
		{
			if($mPerfil['grup_vinculat']==$grupId)
			{
				$mGrupsProductorsRef[$grupId]['perfil_vinculat']=$perfilId;
				$mGrupsProductorsRef[$grupId]['acord1']=$mPerfil['acord1'];
			}
		}
		reset($mPerfilsRef);
	}
	reset($mGrupsProductorsRef);
	
	return $mGrupsProductorsRef;
}

//------------------------------------------------------------------------------
function get_peticionsCadena($cadena)
{

	$mPeticionsGrup=array();
	if($cadena==''){return $mPeticionsGrup;}
	
	$mPeticionsGrup_=explode('{',urldecode($cadena));
	while(list($key,$peticioGrup)=each($mPeticionsGrup_))
	{
		if($peticioGrup!='')
		{
			$peticioGrup1=substr($peticioGrup,0,strlen($peticioGrup)-1);
			$mPeticioGrup=explode(';',$peticioGrup1);
			$mPeticioGrup_=array();
			while(list($key2,$qualitat)=each($mPeticioGrup))
			{
				$propietat=substr($qualitat,0,strpos($qualitat,'='));
				$valor=substr($qualitat,strpos($qualitat,'=')+1);
			
				if($propietat)
				{
					$mPeticioGrup_[$propietat]=$valor;
				}
			}
			reset($mPeticioGrup);
			array_push($mPeticionsGrup,$mPeticioGrup_);
		}
	}
	reset($mPeticionsGrup_);

	return $mPeticionsGrup;
}

//------------------------------------------------------------------------------
function extreureRutesEspecials($mRutesSufixes)
{
	$mRutesSufixes_=array();
	$i=0;
	
	while(list($key,$val)=each($mRutesSufixes))
	{
		if(substr_count($val,'_')==0)
		{
			$mRutesSufixes_[$i]=$val;
			$i++;
		}
	}
	reset($mRutesSufixes);
	return $mRutesSufixes_;
}

//------------------------------------------------------------------------------
function extreureRutesNormals($mRutesSufixes)
{
	$mRutesSufixes_=array();
	$i=0;
	
	while(list($key,$val)=each($mRutesSufixes))
	{
		if(substr_count($val,'_')>0)
		{
			$mRutesSufixes_[$i]=$val;
			$i++;
		}
	}
	reset($mRutesSufixes);
	return $mRutesSufixes_;
}

//------------------------------------------------------------------------------
function deixarNomesRutesNormals($mRutesSufixes)
{
	$mRutesSufixes_=array();
	$i=0;
	
	while(list($key,$val)=each($mRutesSufixes))
	{
		if(substr_count($val,'_')==0 && substr_count($val,'grups')==0)
		{
			$mRutesSufixes_[$i]=$val;
			$i++;
		}
	}
	reset($mRutesSufixes);
	return $mRutesSufixes_;
}

//------------------------------------------------------------------------------
function getDatesReservesLocals()
{
	global $mPars,$mPeriodesLocalsInfo;
	$mDatesReservesLocals=array();
	if(!isset($mPars['sel_periode_comanda_local']) || $mPars['sel_periode_comanda_local']=='')
	{
		$mDataIniciReservesLocals=array(date('d'),date('m'),date('y'));
		$mDataFiReservesLocals=array(date('d')+5,date('m'),date('y'));
		$mPeriodesLocalsInfo[$mPars['sel_periode_comanda_local']]['periode_comanda']=(date('d-m-y')).':'.(date('d')+1).'-'.(date('m-y'));
	}
	else
	{
		$mDates=@explode(':',$mPeriodesLocalsInfo[$mPars['sel_periode_comanda_local']]['periode_comanda']);
		$mDataIniciReservesLocals=@explode('-',$mDates[0]);
		$mDataFiReservesLocals=@explode('-',$mDates[1]);
	}
	

	if(count($mDataIniciReservesLocals)!=0)
	{
		$dataIniciReservesLocals = new DateTime();

		@$dataIniciReservesLocals->setDate('20'.$mDataIniciReservesLocals[2], $mDataIniciReservesLocals[1], $mDataIniciReservesLocals[0] );
		//$dataIniciReservesLocals->setTime($mDataIniciReservesLocals[3], $mDataIniciReservesLocals[4], 0 );
		$instantInici=$dataIniciReservesLocals->getTimestamp();
		$mDatesReservesLocals['idia']=date('d',$instantInici);
		$mDatesReservesLocals['imes']=date('m',$instantInici);
		$mDatesReservesLocals['iany']=date('y',$instantInici);
		//$mDatesReservesLocals['ihora']=date('H',$instantInici);
		//$mDatesReservesLocals['iminuts']=date('i',$instantInici);
		$mDatesReservesLocals['itimestamp']=$instantInici;
	
		$dataFiReservesLocals = new DateTime();

		@$dataFiReservesLocals->setDate('20'.$mDataFiReservesLocals[2], $mDataFiReservesLocals[1], $mDataFiReservesLocals[0] );
		//$dataFiReservesLocals->setTime($mDataFiReservesLocals[3], $mDataFiReservesLocals[4], 0 );
		$instantFi=$dataFiReservesLocals->getTimestamp();
		$mDatesReservesLocals['fdia']=date('d',$instantFi);
		$mDatesReservesLocals['fmes']=date('m',$instantFi);
		$mDatesReservesLocals['fany']=date('y',$instantFi);
		//$mDatesReservesLocals['fhora']=date('H',$instantFi);
		//$mDatesReservesLocals['fminuts']=date('i',$instantFi);
		$mDatesReservesLocals['ftimestamp']=$instantFi;
	}
	
	return $mDatesReservesLocals;
}

//------------------------------------------------------------------------------
function getDatesReservesLocalsNouPeriode($irl,$frl)
{
	global $mPars,$mPropietatsPeriodesLocals;

	$mDatesReservesLocals=array();

	$mDataIniciReservesLocals=explode(',',$irl);
	$mDataFiReservesLocals=explode(',',$frl);

	$dataIniciReservesLocals = new DateTime();

	$dataIniciReservesLocals->setDate('20'.$mDataIniciReservesLocals[2], $mDataIniciReservesLocals[1], $mDataIniciReservesLocals[0] );
	//$dataIniciReservesLocals->setTime($mDataIniciReservesLocals[3], $mDataIniciReservesLocals[4], 0 );
	$instantInici=$dataIniciReservesLocals->getTimestamp();
	$mDatesReservesLocals['idia']=date('d',$instantInici);
	$mDatesReservesLocals['imes']=date('m',$instantInici);
	$mDatesReservesLocals['iany']=date('y',$instantInici);
	//$mDatesReservesLocals['ihora']=date('H',$instantInici);
	//$mDatesReservesLocals['iminuts']=date('i',$instantInici);
	$mDatesReservesLocals['itimestamp']=$instantInici;
	
	$dataFiReservesLocals = new DateTime();

	$dataFiReservesLocals->setDate('20'.$mDataFiReservesLocals[2], $mDataFiReservesLocals[1], $mDataFiReservesLocals[0] );
	//$dataFiReservesLocals->setTime($mDataFiReservesLocals[3], $mDataFiReservesLocals[4], 0 );
	$instantFi=$dataFiReservesLocals->getTimestamp();
	$mDatesReservesLocals['fdia']=date('d',$instantFi);
	$mDatesReservesLocals['fmes']=date('m',$instantFi);
	$mDatesReservesLocals['fany']=date('y',$instantFi);
	//$mDatesReservesLocals['fhora']=date('H',$instantFi);
	//$mDatesReservesLocals['fminuts']=date('i',$instantFi);
	$mDatesReservesLocals['ftimestamp']=$instantFi;
	
	return $mDatesReservesLocals;
}

//*v36-15-12-15 +1 funcio
//------------------------------------------------------------------------------
function getAbonamentsCarrecsUsuari($selUsuariId)
{
	global $mPars,$mAbonamentsCarrecs;
	
	$mAbonamentsCarrecsUsuari=array();
	while(list($key,$mAbonamentCarrec)=each($mAbonamentsCarrecs['grup']))
	{
		if($mAbonamentCarrec['sel_usuari_id']==$selUsuariId)
		{
			array_push($mAbonamentsCarrecsUsuari,$mAbonamentCarrec);		
		}
	}
	reset($mAbonamentsCarrecs['grup']);

	return $mAbonamentsCarrecsUsuari;
}

//------------------------------------------------------------------------------
function getHiHaUnAltrePeriodeNoTancat($mPropietatsPeriodesLocals)
{
	global $mPars,$mPeriodesLocalsInfo;
	$hiHaUnAltrePeriodeNoTancat=-1;
	while(list($id,$mPeriodeLocal)=each($mPeriodesLocalsInfo))
	{
		if
		(
			$id!=''
			&&
			$id!=$mPars['sel_periode_comanda_local']
			&&
			(
				$mPropietatsPeriodesLocals[$id]['comandesLocalsTancades']==-1
				||
				$mPropietatsPeriodesLocals[$id]['comandesLocalsTancades']==0
			)
		)
		{
			$hiHaUnAltrePeriodeNoTancat=1;
			
		}
	
	}
	reset($mPeriodesLocalsInfo);
	
	return $hiHaUnAltrePeriodeNoTancat;
}

//------------------------------------------------------------------------------
function getHiHaAlgunPeriodeNoTancat($mPropietatsPeriodesLocals)
{
	global $mPars,$mPeriodesLocalsInfo;

	$hiHaAlgunPeriodeNoTancat=-1;
	
	while(list($id,$mPeriodeLocal)=each($mPeriodesLocalsInfo))
	{
	
		if
		(
			$id!=''
			&&
			(
				$mPropietatsPeriodesLocals[$id]['comandesLocalsTancades']==-1
				||
				$mPropietatsPeriodesLocals[$id]['comandesLocalsTancades']==0
			)
		)
		{
			$hiHaAlgunPeriodeNoTancat=1;
			
		}
	
	}
	reset($mPeriodesLocalsInfo);
	
	return $hiHaAlgunPeriodeNoTancat;
}

//------------------------------------------------------------------------------
function filtrarProductesLocal($mProductes_,$db) //filtre + paginaci�
{
	global $mPars,$mCampsProductes;
	
	$mProductes=array();
	
	$perfilsRefActiusChain=db_getPerfilsRefActiusChain($db);
	if($mPars['etiqueta']==$mPars['etiqueta2']){$mPars['etiqueta']='TOTS';}

	if($mPars['etiqueta2']!="CAP"){$mPars['etiqueta']='TOTS';}

	$categoria0=substr($mPars['vSubCategoria'],0,strpos($mPars['vSubCategoria'],'-'));
	$categoria10=substr($mPars['vSubCategoria'],strpos($mPars['vSubCategoria'],'-')+1);
	

	// filtre //////////////////////////////////////////////////////////////////

	$j=0;
	while(list($key,$mProducte_)=each($mProductes_))
	{
		$exclou=true;
		if
		(
			($mPars['veureProductesDisponibles']=='1' && $mProducte_['actiu']==1)
			||
			($mPars['veureProductesDisponibles']!='1')
		)
		{
		
			if
			(
				$mProducte_['llista_id']!=0 //llista
				&&
				substr_count($perfilsRefActiusChain,','.(substr($mProducte_['llista_id'],0,strpos($mProducte_['llista_id'],'-'))).',')>0  //perfilproductor actiu
				&&
				(
					$mPars['vProductor']=='TOTS'
					||
					$mPars['vProductor']!='TOTS' && substr($mProducte_['productor'],0,strpos($mProducte_['productor'],'-'))==$mPars['vProductor']  //productor
				)	
				&&
				(
					$mPars['vSubCategoria']=='TOTS'
					||
					(
						$mPars['vSubCategoria']!='TOTS' 
						&& 
						$mProducte_['categoria0']==$categoria0 
						&& 
						$mProducte_['categoria10']==$categoria10
					)
				)
				&&
				(
					$mPars['etiqueta']=='TOTS'
					||
					$mPars['etiqueta']!='TOTS' && substr_count($mProducte_['tipus'],','.$mPars['etiqueta'].',')>0 
				)
				&&
				(
					$mPars['etiqueta2']=='CAP'
					||
					$mPars['etiqueta2']!='CAP' && substr_count($mProducte_['tipus'],','.$mPars['etiqueta2'].',')==0 
				)
			)
			{
				$exclou=false;
			}

		if(!$exclou)
		{
			$mProductes[$j]=$mProducte_;
			$j++;
		}
		}
	}
	reset($mProductes_);

	
	//paginacio ////////////////////////////////////////////////////////////////

	$mPars['numItemsF']=count($mProductes);
	$mPars['numPags']=CEIL($mPars['numItemsF']/$mPars['numItemsPag']);

	for($i=0;$i<count($mProductes);$i++)
	{
		$mProductes[$i]['visible']=0;
		if($i>=$mPars['pagS']*$mPars['numItemsPag'] && $i<($mPars['pagS']*$mPars['numItemsPag']+$mPars['numItemsPag']))
		{
			$mProductes[$i]['visible']=1;
		}
	}

	// ordre i sentit //////////////////////////////////////////////////////////

	return $mProductes;
}

////////////////////////////////////////////////////////////////////////////////
function getPeriodeLocalNoTancat()
{
	global $mPars,$mPropietatsPeriodesLocalsLlistesAssociades;

	if
	(
		isset($mPropietatsPeriodesLocalsLlistesAssociades)
		&&
		isset($mPropietatsPeriodesLocalsLlistesAssociades[$mPars['selLlistaId']])
		&&
		count($mPropietatsPeriodesLocalsLlistesAssociades[$mPars['selLlistaId']])>0
	)
	{
		while(list($periode,$mPeriode)=each($mPropietatsPeriodesLocalsLlistesAssociades[$mPars['selLlistaId']]))
		{
			if($mPeriode['comandesLocalsTancades']!=1)
			{
				return $mPeriode;			
			}
		}
		reset($mPropietatsPeriodesLocalsLlistesAssociades[$mPars['selLlistaId']]);
	}

	
	return false;
}

////////////////////////////////////////////////////////////////////////////////
function html_selectorFitxer($id,$mFitxers)
{
	global $mPars,$mTipusUpload;

	asort($mFitxers);
	echo "
	<p style='font-size:10px;'>&nbsp;&nbsp;".$mTipusUpload[$id]['nom'].":
	<br>
	&nbsp;&nbsp;<select  style='font-size:10px;' onChange=\"javascript: val=this.value; if(val!=''){window.location.href=this.value;}\">
	";
	asort($mFitxers);
	rsort($mFitxers);
	while(list($key,$val)=each($mFitxers))
	{
		echo "
		
		";
		if($val!='.' && $val!='..')
		{
			echo "
	<option value='".$mTipusUpload[$id]['nomFitxer']."/".$val."' target='_blank'>".$val."</option>
			";
		}
	}
	reset($mFitxers);
	echo "
	<option selected value=''></option>
	</select>
	</p>
	";

	
	return;
}

//------------------------------------------------------------------------------
function marcarRecerca($cadena)
{
	global $mPars;
	
	$crPrText=$mPars['crPrText'];
	$cadena3='';
	$mSearch=array(
	0=>'/'.$crPrText.'/i'
	);
	if($mPars['crPrText']!='')
	{
	$mReplace=array(
	0=>"<font style='color:black; background-color:orange;'><b>".$crPrText."</b></font>"
	);
		$cadena3=preg_replace($mSearch,$mReplace,$cadena);
	}
	else
	{
		$cadena3=$cadena;
	}

	return $cadena3;
}




//------------------------------------------------------------------------------
function afegirValorProductes()
{
	global $mParametres,$mInventariGT,$mProductes,$mPars,$mPerfilsRef,$mComandaPerProductors,$mRebostsAmbComandaResum;

	while(list($perfilId,$mComandaPerProductor)=each($mComandaPerProductors))
	{
		if($perfilId!='')
		{
		
		$cAp_ums=0;
		$cAp_ecos=0;
		$cAp_euros=0;
		$cAp_umsCt=0;
		$cAp_ecosCt=0;
		$cAp_eurosCt=0;
		$cAp_umsFd=0;
		$cAp_ecosFd=0;
		$cAp_eurosFd=0;

		$invUms=0.0;
		$invEcos=0.0;
		$invEuros=0.0;
		
		$invUmsR=0.0;
		$invEcosR=0.0;
		$invEurosR=0.0;
		while(list($producteId,$mComandaAproductor)=@each($mComandaPerProductor))
		{
			if(substr_count($mProductes[$producteId]['tipus'],'especial')==0)
			{
				$cAp_ums-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
				$cAp_ecos-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
				$cAp_euros-=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
				$cAp_umsCt-=$mComandaAproductor['quantitatT']*$mComandaAproductor['pes']*$mComandaAproductor['cost_transport_intern_kg'];
				$cAp_ecosCt-=$mComandaAproductor['quantitatT']*$mComandaAproductor['pes']*$mComandaAproductor['cost_transport_intern_kg']*$mComandaAproductor['ms_ctik']/100;
				$cAp_eurosCt-=$mComandaAproductor['quantitatT']*$mComandaAproductor['pes']*$mComandaAproductor['cost_transport_intern_kg']*(100-$mComandaAproductor['ms_ctik'])/100;
				$cAp_umsFd-=$mComandaAproductor['preu']*$mParametres['FDCpp']['valor']/100;
				$cAp_ecosFd-=$mComandaAproductor['preu']*($mParametres['FDCpp']['valor']/100)*$mParametres['msFDCpp']['valor']/100;
				$cAp_eurosFd-=$mComandaAproductor['preu']*($mParametres['FDCpp']['valor']/100)*(100-$mParametres['msFDCpp']['valor'])/100;

				//productes 'dip�sit' en estoc
//				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0 && $mPerfilVinculat['ref']!='63')
				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0)
				{
					if(!isset($mInventariGT['inventariRef'][$producteId])){$mInventariGT['inventariRef'][$producteId]=0;}
					$invUms+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu'];
					$invEcos+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
					$invEuros+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
				}

				//productes 'dip�sit' en estoc que s'han reservat
//				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0 && $mPerfilVinculat['ref']!='63')
				if(substr_count($mComandaAproductor['tipus'],'dip�sit')>0)
				{
					if($mComandaAproductor['quantitatT']<$mInventariGT['inventariRef'][$producteId])
					{
						$invUmsR+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu'];
						$invEcosR+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEurosR+=$mComandaAproductor['quantitatT']*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
					else
					{
						$invUmsR+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu'];
						$invEcosR+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*$mComandaAproductor['ms']/100;
						$invEurosR+=$mInventariGT['inventariRef'][$producteId]*$mComandaAproductor['preu']*(100-$mComandaAproductor['ms'])/100;
					}
				}
			}
	
		}
		$mComandaPerProductors[$perfilId]['ecosTp']=$cAp_ecos+$invEcos;
			
		//si ha fet comanda, agafar el del seu grup vinculat
		if
		(
			$mPerfilsRef[$perfilId]['grup_vinculat']!='' 
			&& 
			$mPerfilsRef[$perfilId]['grup_vinculat']!=0
			&& 
			@array_key_exists($mPerfilsRef[$perfilId]['grup_vinculat'],$mRebostsAmbComandaResum)
		)
		{
			$mComandaPerProductors[$perfilId]['compte_ecos']=@$mRebostsAmbComandaResum[$mPerfilsRef[$perfilId]['grup_vinculat']]['compte_ecos'];
		}
		else// si no, agafar el del seu perfil
		{
			$mComandaPerProductors[$perfilId]['compte_ecos']=$mPerfilsRef[$perfilId]['compte_ecos'];
		}
		
		}
	}
	reset($mComandaPerProductors);
	
	return $mComandaPerProductors;
}

//------------------------------------------------------------------------------
function getProductesRefByKey($key,$sentit,$mProductes)
{
	global $mPars;
	
	
	$mProductesRefKey=array();
	$mProductesRefKey_=array();
	
	while(list($id,$mProducte)=each($mProductes))	
	{
		if($key=='id' || $key=='format' || $key=='preu')
		{
			$mProductesRefKey_[(($mProducte[$key]*10000)+$id)]=$id;
		}
		else
		{
			$mProductesRefKey_[((strtolower($mProducte[$key])).'000'.$id)]=$id;
		}
		
	}		
	reset($mProductes);
	//vd($mProductesRefKey_);
	if($sentit=='DESC')
	{
		ksort($mProductesRefKey_);
		krsort($mProductesRefKey_);
	}
	else
	{
		ksort($mProductesRefKey_);
	}
	while(list($key_,$id)=each($mProductesRefKey_))	
	{
		$mProductesRefKey[$id]=$mProductes[$id];
	}		
	reset($mProductesRefKey_);

	return $mProductesRefKey;
}
//------------------------------------------------------------------------------
function getTotalsOferitsProductes()
{
	global $mPars,$mProductes;
	
	$mTotalsOferitsProductes=array();
	$mTotalsOferitsProductes['ums']=0;
	$mTotalsOferitsProductes['ecos']=0;
	$mTotalsOferitsProductes['euros']=0;
	$mTotalsOferitsProductes['uts']=0;
	$mTotalsOferitsProductes['kg']=0;
	
	while(list($key,$mProducte)=each($mProductes))	
	{
		$mTotalsOferitsProductes['ums']+=$mProducte['estoc_previst']*$mProducte['preu'];
		$mTotalsOferitsProductes['ecos']+=$mProducte['estoc_previst']*$mProducte['preu']*$mProducte['ms']/100;
		$mTotalsOferitsProductes['euros']+=$mProducte['estoc_previst']*$mProducte['preu']*(100-$mProducte['ms'])/100;
		$mTotalsOferitsProductes['kg']+=$mProducte['estoc_previst']*$mProducte['pes'];
		$mTotalsOferitsProductes['uts']+=$mProducte['estoc_previst'];
	}		
	reset($mProductes);

	return $mTotalsOferitsProductes;
}
?>

		