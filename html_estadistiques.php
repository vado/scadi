<?php
//------------------------------------------------------------------------------
function mostrarSelectorRutaC($desti)
{
	global 	$mRutesSufixes,
			$mSelRutes,
			$mPars,
			$mMesos,
			$parsChain,
			$sb,
			$sbd,
			$mSb,
			$mSbd,
			$sg,
			$mGrupsRef,
			$mPeriodesInfo,
			$mSzones,
			$mZones,
			$sZ,
			$sSz;
	echo "
	<form id='f_selRutes' action='".$desti."' method='POST' target='_self'>
	<table width='70%' align='center'>
		<tr>
			<td align='left' valign='top' width='16%'>
			<p>
			SuperZona:
			<br> 
			<select id='sel_sZona' name='sel_sZona'  onChange=\"javascript:document.getElementById('s_enviar1').style.backgroundColor='orange';document.getElementById('sel_zona').value='TOTS';document.getElementById('sel_grup').value='TOTS';\">
		";
			$selected='';
			$selected2='selected';
			while(list($index,$sZona)=each($mSzones))
			{
				if($sSz==$sZona)
				{
					$selected='selected';$selected2='';}else{$selected='';
				}
					
				echo "
			<option ".$selected." value='".$sZona."'>".$sZona."</option>
				";
			}
			reset($mSzones);
			echo "
			<option ".$selected2." value='TOTS'>- totes les SuperZones - </option>
			</select>
			<br>
			</td>
			
			<td align='left' valign='top' width='16%'>
			<p>
			Zona:
			<br> 
			<select id='sel_zona' name='sel_zona'  onChange=\"javascript:document.getElementById('s_enviar1').style.backgroundColor='orange';document.getElementById('sel_sZona').value='TOTS';document.getElementById('sel_grup').value='TOTS';\">
		";
			$selected='';
			$selected2='selected';
			while(list($index,$zona)=each($mZones))
			{
				if($sZ==$zona)
				{
					$selected='selected';$selected2='';}else{$selected='';
				}
					
				echo "
			<option ".$selected." value='".$zona."'>".$zona."</option>
				";
			}
			reset($mZones);
			echo "
			<option ".$selected2." value='TOTS'>- totes les Zones - </option>
			</select>
			</p>		
			</td>

			<td  width='16%' align='left' valign='top'>
			<p>
			Periode:
			<br>
			<select class='seleccionada2' name='sel_rutes[]'  multiple>
	";
	$selected='';
//*v36-while

	while(list($index,$ruta_)=each($mRutesSufixes))
	{
		$mRuta_=explode('_',$ruta_);
		if(count($mRuta_)>=2) //ruta especial
		{
			if($mRuta_[1]!='grups')
			{
				if(in_array($ruta_,$mSelRutes)){$selected='selected';}else{$selected='';}
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp=$mRuta_[0];
				$color='blue';
				echo "
			<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[1],2,2))])." ".(substr($mRuta_[1],0,2)).$text."</option>
				";
			}
		}
		else
		{
			if($mRuta_[0]!='grups')
			{
				if(in_array($ruta_,$mSelRutes)){$selected='selected';}else{$selected='';}
				$color='black';
				if($mPeriodesInfo[$ruta_]['precomandaTancada']=='0')
				{
					$text=' (OBERT)';
				}
				else
				{
					$text='';
				}
				$textRutaEsp='';
				echo "
					<option style='color:".$color.";' ".$selected." value='".$ruta_."'>".$textRutaEsp." ".($mMesos[(substr($mRuta_[0],2,2))])." ".(substr($mRuta_[0],0,2)).$text."</option>
				";
			}
		}
	}
	reset($mRutesSufixes);
	echo "
			</select>
			</td>

			<td  width='16%' align='left' valign='top'>
			<p>
			Grup:
			<br>
			<select class='seleccionada2' id='sel_grup' name='sel_grup'  onChange=\"javascript:document.getElementById('s_enviar1').style.backgroundColor='orange';document.getElementById('sel_sZona').value='TOTS';document.getElementById('sel_zona').value='TOTS';\"
	";
	$selected='';
	$selected2='selected';
	while(list($grupId,$mGrup)=each($mGrupsRef))
	{
		if($grupId!='0')
		{
			if($grupId==$sg){$selected='selected';$selected2='';}else{$selected='';}
			echo "
			<option ".$selected." value='".$grupId."'>".(urldecode($mGrup['nom']))."</option>
			";
		}
	}
	reset($mRutesSufixes);
	echo "
			<option ".$selected2." value=''>- tots els grups -</option>
			</select>
			</td>

			<td  width='16%' align='left' valign='top'>
			<p> ordenar per:<br>
			<select id='sel_sb' name='sel_sb'>
			";
			$selected='';
			while(list($key,$val)=each($mSb))
			{
				if($val==$sb){$selected='selected';}else{$selected='';}
				echo "
			<option ".$selected." value='".$val."'>".$val."</option>
				";
			}
			reset($mSb);
			echo "
			</select>
			</p>
			</td>

			<td  width='16%' align='left' valign='top'>
			<p>en sentit:<br>
			<select id='sel_sbd' name='sel_sbd'>
			";
			$selected='';
			while(list($key,$val)=each($mSbd))
			{
				if($val==$sbd){$selected='selected';}else{$selected='';}
				echo "
			<option ".$selected." value='".$val."'>".$val."</option>
				";
			}
			reset($mSbd);
			echo "
			</select>
			</p>

			<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
			<input type='submit' id='s_enviar1' value='enviar'\">
			</p>
			</td>
		</tr>
	</table>
	</form>
	";

	return;
}

//------------------------------------------------------------------------------
function html_consum()
{
	global $mRutesSufixes,$mSelRutes,$mPars,$mConsumX,$sb,$sbd,$mSb,$mSbd,$mConsumXcsv,$mGrupsRef,$sg;

		$mTotals=array();
		$mBgColor['-1']='#ffffff';
		$mBgColor['1']='#dddddd';
		$colorIndex=-1;

	echo "
	<table style='width:100%;'>
		<tr>
			<td style='width:100%;'>
			<table border='0' style='width:100%;'>
				<tr>
					<td widht='50%' align='left'>
	";
	if($sg!='')
	{
		echo "
					<p style='text-align:'center;'>* consum de productes del GRUP <b>'".(urldecode($mGrupsRef[$sg]['nom']))."</b>'</p>
		";
	}
	echo "
					<p style='text-align:'center;'>* consum de productes ordenat per <b>'".$sb."</b>' en sentit <b>'".$sbd."'</b></p>
					</td>

					<td widht='50%' align='right'>
	";
	if(count($mSelRutes)>0)
	{
		echo "
					<a href='docs".$mPars['selRutaSufix']."/estadistiques_".$mPars['usuari_id'].".csv' target='_blank'>descarregar .csv (vista actual)</a>
		";
	}
	echo "
					</td>
				</tr>
			</table>
			
			<table border='1' style='width:100%;'>
				<tr>
					";
					//$mConsumXcsv[0]=array();
					while(list($key,$val)=each($mSb))
					{
					//echo $val.'<br>';
						if($sb==$val){echo "<td bgcolor='#9CE6E3' align='center'><p style='font-size:15px;'><b>".$val."</b></p></td>";}
						else {echo "<td bgcolor='#9CE6E3' align='center'><p  style='font-size:15px;'>".$val."</p></td>";}
					//	array_push($mConsumXcsv[0],$val);
					}
					reset($mSb);
					echo "
				</tr>			
					";

				
				while(list($quantitat,$mConsumQt_)=each($mConsumX))
				{
					while(list($index,$mConsumProducte)=each($mConsumQt_))
					{
					
						echo "
				<tr >
						";
						$mConsumProducte_=array();
						while(list($key,$val)=each($mSb))
						{
							if($sb==$val)
							{
								if($val=='index' ||  $val=='productor' || $val=='producte' || $val=='unitat_facturacio' || $val=='ms')
								{
									$mConsumProducte[$val]=urldecode($mConsumProducte[$val]);
									echo "
					<td style='background-color:".$mBgColor[$colorIndex].";'><p><b>".$mConsumProducte[$val]."</b></p></td>
									";
								}
								else
								{
									if(!isset($mTotals[$val])){$mTotals[$val]=$mConsumProducte[$val];}
									else {$mTotals[$val]+=$mConsumProducte[$val];}
									echo "
					<td style='background-color:".$mBgColor[$colorIndex].";'><p><b>".$mConsumProducte[$val]."</b></p></td>
									";
									$mConsumProducte[$val]=str_replace('.',',',$mConsumProducte[$val]);
									
								}
							}
							else 
							{
								if($val=='index' || $val=='productor' || $val=='producte'  || $val=='unitat_facturacio' || $val=='ms')
								{
									$mConsumProducte[$val]=urldecode($mConsumProducte[$val]);
									echo "
					<td style='background-color:".$mBgColor[$colorIndex].";'><p>".$mConsumProducte[$val]."</p></td>
									";
								}
								else
								{
									if(!isset($mTotals[$val])){$mTotals[$val]=$mConsumProducte[$val];}
									else {$mTotals[$val]+=$mConsumProducte[$val];}
									echo "
					<td style='background-color:".$mBgColor[$colorIndex].";'><p>".$mConsumProducte[$val]."</p></td>
									";
									$mConsumProducte[$val]=str_replace('.',',',$mConsumProducte[$val]);
								}
							}
							$mConsumProducte_[$val]=$mConsumProducte[$val];
						}
						reset($mSb);
						array_push($mConsumXcsv,$mConsumProducte_);
						echo "
				<tr>			
						";
						$colorIndex*=-1;
					}
					reset($mConsumQt_);
				}
				reset($mConsumX);
				echo "
				<tr>
					";
					//$mConsumXcsv[0]=array();
					while(list($key,$val)=each($mSb))
					{
					//echo $val.'<br>';
						if($sb==$val){echo "<td bgcolor='#9CE6E3' align='center'><p style='font-size:15px;'><b>".$val."</b></p></td>";}
						else {echo "<td bgcolor='#9CE6E3' align='center'><p  style='font-size:15px;'>".$val."</p></td>";}
					//	array_push($mConsumXcsv[0],$val);
					}
					reset($mSb);
					echo "
				</tr>			
				<tr>
					";
					while(list($key,$val)=each($mSb))
					{
						if(array_key_exists($val,$mTotals))
						{
							echo "
					<td ><p style='font-size:15px;'><b>".(number_format($mTotals[$val],2))."</b></p></td>";
						}
						else 
						{
							echo "
					<td ><p  style='font-size:15px;'></p></td>
							";
						}
					}
					reset($mSb);
					echo "
				</tr>			
			</table>
			</td>
		</tr>
	</table>
	";


	return;
}
?>

		