<?php

include "config.php";
include "einesConfig.php";
include "db.php";
include "eines.php";

include "html_gestioProductes.php";
include "html_segells.php";
include "db_gestioProductes.php";
include "db_gestioGrup.php";
include "db_gestioMagatzems.php";
include "db_mail.php";
include "upload.php";
include "html_ajuda1.php";
include "db_ajuda.php";

	require_once('phpmailer/class.phpmailer.php');
	include("phpmailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded


//variables:

$mOpcionsOrdreActivacioRapida=array();
$mOpcionsOrdreActivacioRapida['id-ASC']='id (ASC)';
$mOpcionsOrdreActivacioRapida['id-DESC']='id (DESC)';
$mOpcionsOrdreActivacioRapida['producte-ASC']='producte (ASC)';
$mOpcionsOrdreActivacioRapida['producte-DESC']='producte (DESC)';
$mOpcionsOrdreActivacioRapida['format-ASC']='format (ASC)';
$mOpcionsOrdreActivacioRapida['format-DESC']='format (DESC)';
$mOpcionsOrdreActivacioRapida['unitat_facturacio-ASC']='unitat_facturacio (ASC)';
$mOpcionsOrdreActivacioRapida['unitat_facturacio-DESC']='unitat_facturacio (DESC)';
$mOpcionsOrdreActivacioRapida['preu-ASC']='preu (ASC)';
$mOpcionsOrdreActivacioRapida['preu-DESC']='preu (DESC)';
$mOpcionsOrdreActivacioRapida['ms-ASC']='% ms (ASC)';
$mOpcionsOrdreActivacioRapida['ms-DESC']='% ms (DESC)';
$mOpcionsOrdreActivacioRapida['estat-ASC']='estat (ASC)';
$mOpcionsOrdreActivacioRapida['estat-DESC']='estat (DESC)';


//POST

$parsChain=@$_POST['i_pars'];
$mPars=getPars($parsChain);
$ordreAr_=@$_POST['i_ordreAr'];
if(isset($ordreAr_)){$mPars['ordreAr']=$ordreAr_;}
else if(!isset($mPars['ordreAr'])){$mPars['ordreAr']='producte-ASC';}

$demo=@$_GET['demo'];
if(	isset($demo)){$demo*=1;	$mPars['demo']=$demo;unset($mPars['selProducteId']);}
if(	!isset($mPars['demo'])){	$mPars['demo']=-1;}
$mParams=getParams();

//DB
$ruta=@$_GET['sR']; //selector de ruta
if(isset($ruta))
{
	$mPars['selRutaSufix']=$ruta;
}
else
{
	$ruta=@$_POST['i_selRuta'];

	if(isset($ruta))
	{
		$mPars['selRutaSufix']=$ruta;
	}
}

$llistaId_=@$_GET['sL']; //selector de llista
if(isset($llistaId_))
{
	$mPars['selLlistaId']=$llistaId_;
}
else
{
	$llistaId=@$_POST['i_selLlista'];

	if(isset($llistaId))
	{
		$mPars['selLlistaId']=$llistaId;
	}
}


if(!isset($mPars['selLlistaId'])){$mPars['selLlistaId']=0;}

if($mPars['selLlistaId']==0)
{
	$mPars['taulaProductes']='productes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandes']='comandes_'.$mPars['selRutaSufix'];
	$mPars['taulaComandesSeg']='comandes_seg';
}
else
{
	$mPars['taulaProductes']='productes_grups';
	$mPars['taulaComandes']='comandes_grups';
	$mPars['taulaComandesSeg']='comandes_seg_grups';
}


//*v36-declaracio
$mRuta=explode('_',$mPars['selRutaSufix']);
//*v36-condicio
if(count($mRuta)==2){$mPars['selRutaSufixPeriode']=$mRuta[1];}else{$mPars['selRutaSufixPeriode']=$mRuta[0];}


$db=db_conect($mParams);
selectDb($mParams['bd'],$db);
getConfig($db); //inicialitza variables anteriors;
$mPars['vRutaIncd']=$mPars['selRutaSufix'];

$mRutesSufixes=getRutesSufixes($db);
//*v36 5-1-16 assignacio
	if($mPars['selLlistaId']!=0)
	{
		$mPropietatsPeriodesLocals=db_getPropietatsPeriodesLocals($db);
		$mPeriodesLocalsInfo=db_getPeriodesLocalsInfo($db);
		if(!isset($mPars['sel_periode_comanda_local']) || $mPars['sel_periode_comanda_local']=='')
		{
			$mPropietatsPeriodesLocals_=array_keys($mPropietatsPeriodesLocals);
			$periodeLocal=array_shift($mPropietatsPeriodesLocals_);
			$mPars['periode_comanda_local']=$periodeLocal;
			$mPars['sel_periode_comanda_local']=$mPars['periode_comanda_local'];
		}
		$mDatesReservesLocals=getDatesReservesLocals();
	}
	else
	{
		$mPars['periode_comanda_local']='';
		$mPars['sel_periode_comanda_local']='';
		$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']]['comandesLocalsTancades']=1;
	}
$mPeriodesInfo=db_getPeriodesInfo($db);


post_guardarAjuda($db); //rep i guarda canvis ajuda
$mAjuda['html_gestioProductes.php']=db_getAjuda('html_gestioProductes.php',$db);
$mAjuda['gestioProductes.php']=db_getAjuda('gestioProductes.php',$db);
$mAjuda['eines.php']=db_getAjuda('eines.php',$db);

if(!checkLogin($db))
{
	echo "
	<p>Usuari no autoritzat</p>
	";
	$login=false;
}
else
{
	$login=true;

}

$mUsuari=db_getUsuari($mPars['usuari_id'],$db);


$modificarValorsImportantsProductes=false;

if($mPars['selLlistaId']==0)
{
	$mDataFiPrecomandes=explode('-',$mParametres['dataFiPrecomandes']['valor']);
	$dataFiPrecomandes = new DateTime();
	$dataFiPrecomandes->setDate('20'.$mDataFiPrecomandes[2]*1 , $mDataFiPrecomandes[1]*1 , $mDataFiPrecomandes[0]*1 );
	$date = new DateTime();
	$t1=$date->getTimestamp();
	$dataFiPrecomandes->setTime(21,0,0); //hora predefinida de tancament del periode
	$t2=$dataFiPrecomandes->getTimestamp();
	
	if(($t2-$t1)>0){$modificarValorsImportantsProductes=true;}
}
else
{
	$modificarValorsImportantsProductes=true;
}
	

$selPerfil_=@$_GET['plId'];

if(isset($selPerfil_)){$mPars['selPerfilRef']=$selPerfil_;}
$mPerfil=db_getPerfil($db);
$mPars['perfil_id']=$mPerfil['id'];

$mPerfilResp=db_getUsuari($mPerfil['usuari_id'],$db);
$mGrupsAssociatsRef=db_getGrupsAssociatsPerfilRef($db);
$mGrupsRef=db_getGrupsRef($db);  //agafa grup_id de pars si no es buit

					
if($mPars['selLlistaId']==0)
{	
	if
	(
		$mPars['usuari_id']==$mPerfil['usuari_id']
		||
		substr_count($mUsuari['perfils_productor'],','.$mPerfil['id'].',')>0
	)
	{
		$numProductesPerfilAllista=db_getNumProductesPerfilAllista($mPerfil['id'],$mPars['selLlistaId'],$db);
		$mLlistesRef=$mGrupsAssociatsRef;//db_getLlistesRef($db);
			$mLlistesRef[0]=array('id'=>0,'nom'=>'CAC');
		$mPerfilsRef=array($mPerfil['id']=>$mPerfil);
	}
	
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
	{
		$numProductesPerfilAllista=db_getNumProductesPerfilAllista($mPerfil['id'],$mPars['selLlistaId'],$db);
		$mLlistesRef=$mGrupsAssociatsRef;//db_getLlistesRef($db);
			$mLlistesRef[0]=array('id'=>0,'nom'=>'CAC');
		//$mPerfilsRef=array($mPerfil['id']=>$mPerfil);
		$mPerfilsRef=db_getPerfilsRefTots($db);
	}
}
else //lista!=0
{

	$llistesAssociadesChain=implode(',',(array_keys($mGrupsAssociatsRef)));
	$llistesAssociadesChain=','.$llistesAssociadesChain.',';

	$mPropietatsPeriodesLocalsLlistesAssociades=db_getPropietatsPeriodesLocalsLlistesAssociades($llistesAssociadesChain,$db);
	$mPeriodeNoTancat=getPeriodeLocalNoTancat();

		//resp perfil
	if
	(
		$mPars['usuari_id']==$mPerfil['usuari_id']
		&&
		substr_count($mUsuari['perfils_productor'],','.$mPerfil['id'].',')>0
	)
	{
		$numProductesPerfilAllista=db_getNumProductesPerfilAllista($mPerfil['id'],$mPars['selLlistaId'],$db);
		$mLlistesRef=$mGrupsAssociatsRef;//db_getLlistesRef($db);
			$mLlistesRef[0]=array('id'=>0,'nom'=>'CAC');
		$mPerfilsRef=db_getPerfilsUsuariAgrup($db);
		
	}
	
	// responsable grup
	if($mPars['usuari_id']==$mGrupsRef[$mPars['selLlistaId']]['usuari_id'])
	{
		$mLlistesRef[$mPars['selLlistaId']]=$mGrupsRef[$mPars['selLlistaId']];
			//$mLlistesRef[0]=array('id'=>0,'nom'=>'CAC');
			$mPars['grup_id']=$mPars['selLlistaId'];
		$mGrup=getGrup($db);
		$mPerfilsRef=db_getPerfilsRefGrup($db);
	}
	
	//coordinadors i admins
	if($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin' || $mPars['nivell']=='coord')
	{
		$mLlistesRef=$mGrupsRef;
			$mLlistesRef[0]=array('id'=>0,'nom'=>'CAC');
			$mPars['grup_id']=$mPars['selLlistaId'];
		$mGrup=getGrup($db);
		$mPerfilsRef=db_getPerfilsRefGrup($db);
	}
}


if
(
	isset($mPars['selLlistaId'])
	&& 
	$mPars['selLlistaId']!=''
	&& 
	$mPars['selLlistaId']!=0
)
{
	$mPars['grup_id']=$mPars['selLlistaId'];
	$mGrup=getGrup($db);
}
else
{
	$mGrup=array();
}
$mPars['periode_comanda']=$mParametres['periodeComanda']['valor'];
$mPars['sortBy']='id';
$mPars['ascdesc']='ASC';
$mPars['vProductor']=$mPerfil['id'];
$mPars['vCategoria']='TOTS';
$mPars['vSubCategoria']='TOTS';
$mPars['etiqueta']='TOTS';
$mPars['etiqueta2']='CAP';
$mPars['vProducte']='TOTS';

$mPars['veureProductesDisponibles']=-1;
$mPars['excloureProductesJaEntregats']=-1;

$mPars['paginacio']=-1;
if(!isset($mPars['pagS'])){$mPars['pagS']=0;}
if(!isset($mPars['pagFiltre'])){$mPars['pagFiltre']=$mPars['sortBy'].$mPars['veureProductesDisponibles'].$mPars['etiqueta2'].$mPars['etiqueta'].$mPars['vProductor'].$mPars['vSubCategoria'].$mPars['vCategoria'];}
if(!isset($mPars['numItemsPag'])){$mPars['numItemsPag']=10;}
if(!isset($mPars['numPags'])){$mPars['numPags']=0;} 
$mSubCategories=getSubCategories2($db);


$mPropietatsPeriodesLocals=db_getPropietatsPeriodesLocals($db);
$mPropietatsPeriodeLocal=@$mPropietatsPeriodesLocals[$mPars['sel_periode_comanda_local']];

	$mProductes=db_getProductes3($db);

$mUsuarisRef=db_getUsuarisRef($db);
$mGrupsRef=db_getGrupsRef($db);


$mPars['perfils_productor']=$mUsuari['perfils_productor'];
$mPerfilsUsuari=db_getPerfilsUsuari($db);
$mMagatzems=db_getMagatzems($db);
$mZones=db_getZones($db);
$mInventari=db_getInventari('0',$db);
$mPropietatsGrup=@getPropietatsGrup($mGrupsRef[$mPars['selLlistaId']]['propietats']);

//obtenir inventari per zones
	$mInventariPerZones=array();

	while(list($key,$mMagatzem)=each($mMagatzems))
	{
		$zona=db_getZonaGrup($mMagatzem['id'],$db);
		
		if(!isset($mInventariPerZones[$zona])){$mInventariPerZones[$zona]=array();}
		$mInventari_=db_getInventari($mMagatzem['id'],$db);
//*v36.2-condicio
		if($mInventari_)
		{
			while(list($producteId,$quantitat)=@each($mInventari_['inventariRef']))
			{
				if(!isset($mInventariPerZones[$zona][$producteId])){$mInventariPerZones[$zona][$producteId]=0;}
				if($quantitat*1>0)
				{
					$mInventariPerZones[$zona][$producteId]+=$quantitat;
				}
			}
			@reset($mInventari_['inventariRef']);
		}
	}
	@reset($mMagatzems);
$opt='inici';
$opt_=@$_GET['opt']; //ve de menu productes
if(isset($opt_)){$opt=$opt_;}
$selProducteId_=@$_GET['pId'];
if(isset($selProducteId_)){$mPars['selProducteId']=$selProducteId_;}
//if(isset($ruta_) || isset($llistaId_)){$mPars['selProducteId']='';}

if(isset($mPars['selProducteId'])){$mProducte=db_getProducte($mPars['selProducteId'],$db);}else{$mProducte=array();}
if(@$mPars['selProducteId']!='' && $mProducte && $opt!='aEg'&& $opt!='aE'){$opt='vell';}
//if(isset($ruta_) || isset($llistaId_)){$opt='inici';}
if($opt=='inici' || $opt=='nou')
{
	$mPars['selProducteId']='';
	
	$mProducte=array(
'id'=>'',
'segell'=>0,
'producte'=>'',
'actiu'=>'0',
'productor'=>$mPerfil['id'].'-'.$mPerfil['projecte'],
'categoria0'=>'',
'categoria10'=>'',
'tipus'=>'',
'unitat_facturacio'=>'',
'format'=>'1',
'pes'=>'1',
'volum'=>'1',
'preu'=>'',
'ms'=>'50',
'estoc_previst'=>'0',
'estat'=>'0',
'estoc_disponible'=>'0',
'notes_rebosts'=>'',
'imatge'=>'',
'notes'=>'',
'cost_transport_extern_kg'=>'',
'ms_ctek'=>'',
'cost_transport_intern_kg'=>$mParametres['varCostTransport']['valor'],
'ms_ctik'=>'',
'propietats'=>'',
);
	
}
//VARS
$guardarProducteId='';
$missatgeAlerta='';
$login=false;

$mColsProductes3=array('producte','productor','actiu','tipus','categoria0','categoria10','unitat_facturacio','format','pes','volum','preu','ms','estoc_previst','estoc_disponible','imatge','cost_transport_extern_kg','cost_transport_intern_kg','propietats');

$mNomsColumnes3=array(
'segell'=>'Segell ECOCIC',
'producte'=>'Producte',
'productor'=>'Productor',
'actiu'=>'Actiu',
'tipus'=>'Tipus',
'categoria0'=>'Categoria',
'categoria10'=>'Sub-categoria',
'unitat_facturacio'=>'Ut.fact',
'format'=>'Format(pack uts)',
'pes'=>'Pes ut.fact(kg)',
'volum'=>'Volum ut.fact (l)',
'preu'=>'Preu(uts)',
'ms'=>'% MS',
'estoc_previst'=>'Estoc previst',
'estat'=>'estat',
'estoc_disponible'=>'Estoc dispon.',
'imatge'=>'imatge',
'cost_transport_extern_kg'=>'cost transp extern (uts/kg)',
'ms_ctek'=>'% ms del cost transport extern per kg',
'cost_transport_intern_kg'=>'cost transp intern (uts/kg)',
'ms_ctik'=>'% ms del cost transport intern per kg'
//'propietats'=>'propietats'
);


$mGuardarProducte=array();

	if($opcio=@$_POST['i_opcio'])
	{
		if($opcio=='guardar')
		{
			$guardarProducteId=@$_POST['i_guardarProducte_id'];
			$campsGuardar=@$_POST['i_campsGuardar'];
			$mCampsGuardar=explode(',',$campsGuardar);
			$mGuardarProducte['id']=$guardarProducteId;
			while(list($key,$val)=each($mCampsGuardar))
			{
				$mGuardarProducte[$val]=urlencode(@$_POST['i_'.$val]);
			}
			if(!db_guardarProducte2($mGuardarProducte,$db))
			{
				$missatgeAlerta="<p  class='pAlertaNo4'><i>Atenci�: error en guardar el producte</i></p>";
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk4'><i>El producte s'ha guardat correctament.</i></p>";
				$mPars['selProducteId']=$guardarProducteId;
				$mProducte=db_getProducte($mPars['selProducteId'],$db);
				$mProductes=db_getProductes2($db);
				$mSubCategories=getSubCategories2($db);
			}
			if(substr($mProducte['productor'],0,strpos($mProducte['productor'],'-'))!=$mPerfil['id'])
			{
				$opt='inici';
				unset($mProducte);
				$mPars['selProducteId']='';
			}
			else
			{
				$opt='vell';
			}
		}
		else if($opcio=='crear')
		{
			$campsGuardar=@$_POST['i_campsGuardar'];
			$mCampsGuardar=explode(',',$campsGuardar);
			while(list($key,$val)=each($mCampsGuardar))
			{
				$mGuardarProducte[$val]=urlencode(@$_POST['i_'.$val]);
			}
			
			if($mPars['selLlistaId']!=0)
			{
				$numProductesLlista=db_getNumProductesLlista($mPars['selLlistaId'],$db);
			}
			if($mPars['selLlistaId']==0 || $numProductesLlista<$mParametres['limitNombreProductesLocals']['valor']*1)
			{
				if(!$nouId=db_crearProducte($mGuardarProducte,$db))
				{
					$missatgeAlerta="<p  class='pAlertaNo4'><i>Atenci�: error en crear el producte</i></p>";
				}
				else
				{
					$missatgeAlerta="<p  class='pAlertaOk4'><i>El producte s'ha creat correctament. <font color='DarkOrange'><b>Recorda activar-lo!</b></font></i></p>";
					//$mProducte=db_getProducte($guardarProducteId,$db);
					$mPars['selProducteId']=$nouId;
					$mProducte=db_getProducte($mPars['selProducteId'],$db);
					unset($mProductes);
					$mProductes=db_getProductes2($db);
					$mSubCategories=getSubCategories2($db);
				}
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaNo4'><i>Atenci�: el nombre de productes de la llista ha arribat al l�mit perm�s (".$mParametres['limitNombreProductesLocals']['valor']." productes).
				<br>Revisa si pots eliminar productes d'algun perfil de productor que no s'estiguin utilitzant.</i></p>";
			}
			$opt='vell';
		}
		else if($opcio=='eliminar')
		{
			if(!db_eliminarProducte($mPars['selProducteId'],$db))
			{
				$missatgeAlerta="<p  class='pAlertaNo4'><i>Atenci�: error en eliminar el producte. <br> Es possible que estoc_previst!=0 o estoc_disponible!=0 o s'hagi fet una comanda</i></p>";
				$opt='vell';
			}
			else
			{
				$missatgeAlerta="<p  class='pAlertaOk4'><i>El producte s'ha eliminat correctament.</i></p>";
				unset($mPars['selProducteId']);
				$mProductes=db_getProductes2($db);
				$mSubCategories=getSubCategories2($db);
				$opt='inici';
			}
		}
		else if($opcio=='anularReserves')
		{
			
			$mResult=db_anularReserves($mPars['selProducteId'],$db);
			if(!$mResult['result'])
			{
				$missatgeAlerta="<p   class='pAlertaNo4'><i>Atenci�: error en anular les reserves d'aquest producte.</i><br>".$mResult['missatgeAlerta']."</p>";
			}
			else
			{
				$missatgeAlerta="<p   class='pAlertaOk4'><i>S'han eliminat correctament les reserves d'aquest producte (comandes individuals modificades).</i><br>".$mResult['missatgeAlerta']."</p>";
				$mProductes=db_getProductes2($db);
			}
			$mProducte=db_getProducte($mPars['selProducteId'],$db);
			$opt='vell';
		}
	}
	else if($idProducte=@$_POST['i_imatgeIdProducte'])
	{
		$mPars['selProducteId']=$idProducte;
		$missatgeAlerta=recepcioImatgeProducte($db,$idProducte);
		$mProducte=db_getProducte($idProducte,$db);
		$opt='vell';
	}

	if($ids_ar_chain=@$_POST['i_ids_ar_chain']) //activaci� r�pida
	{
		if(isset($ids_ar_chain) && $ids_ar_chain!='')
		{
			$mIdsArChain=explode(',',substr($ids_ar_chain,1));
			for($i=0;$i<count($mIdsArChain);$i++)
			{
				$id=@$_POST['i_ar_id_'.$mIdsArChain[$i]];
				$estocPrevist=@$_POST['i_ar_ep_'.$mIdsArChain[$i]];
				if($activar=@$_POST['cb_ar_ac_'.$mIdsArChain[$i]])
				{
					$activar=@$_POST['cb_ar_ac_'.$mIdsArChain[$i]];
					$mProductesActivacioRapida[$mIdsArChain[$i]]['activar']=$activar;
				}
				else
				{
					$activar=@$_POST['cb_ar_d_'.$mIdsArChain[$i]];
					$mProductesActivacioRapida[$mIdsArChain[$i]]['activar']=$activar*(-1);
				}
				$mProductesActivacioRapida[$mIdsArChain[$i]]['estoc_previst']=$estocPrevist;
			}
			$mMissatgeAlerta=db_actualitzarProductesAR($mProductesActivacioRapida,$db);
			$missatgeAlerta=$mMissatgeAlerta['missatge'];
			$mProductes=db_getProductes2($db);
			unset($mPars['selProducteId']);
			unset($mProducte);
			$opcio='nou';
		}
	}


if($numPlantilles=@$_GET['numNpl'])
{
	$mPars['perfilId']=@$_GET['pId'];
	$comentariNP=urlencode(@$_POST['i_comentari']);
	$mMissatgeAlerta=put_peticioGrupPlantilles($numPlantilles,$comentariNP,$mPars['selLlistaId'],$db);
	if($mMissatgeAlerta['missatge']!='')
	{
		$missatgeAlerta=$mMissatgeAlerta['missatge'];
	}
	$opt='inici';
}
else if($mFitxesTransferir=@$_POST['sel_fitxes'])
{
	$mLlistesDesti=@$_POST['sel_llistes'];
	$idLlistesCadena=implode(',',$mLlistesDesti);
	$idFitxesCadena=implode(',',$mFitxesTransferir);
	
	$periodeDesti=@$_POST['sel_ruta2'];
	
	$mMissatgeAlerta=put_peticioGrupTransferirFitxes($idLlistesCadena,$idFitxesCadena,$periodeDesti,$db);
	if($mMissatgeAlerta['missatge']!='')
	{
		$missatgeAlerta=$mMissatgeAlerta['missatge'];
	}
	$opt='inici';
	
}
else if($idGrupsCadena=@$_GET['grAss'])
{
	$mPars['perfilId']=@$_GET['pId'];
	$mGrupsAssociar=explode(',',$idGrupsCadena);
	
	$mMissatgeAlerta=put_peticioAssociarPerfilAgrups($idGrupsCadena,$db);
	if($mMissatgeAlerta['missatge']!='')
	{
		$missatgeAlerta=$mMissatgeAlerta['missatge'];
	}
	$opt='inici';
}



//guardar segell producte

		$mCategoria0perfil=db_getCategoria0Perfil($db);
		$mPropietatsSegellPerfil=db_getPropietatsSegellPerfil($db);	
		$maxValorSegellPerfil=getValorMaxSegell($mPropietatsSegellPerfil,$mCategoria0perfil);
		
if($guardarSegellProducteId=@$_POST['i_gSp'])
{
		$mPropietatsSegellProducteGuardar=$mPropietatsSegellProducteConfig;
		$mPropietatsSegellProducte=db_getPropietatsSegellProducte($db);	
		while(list($id,$mSegellProducte)=each($mPropietatsSegellProducteConfig))
		{
			$mPropietatsSegellProducteGuardar[$id]=$mPropietatsSegellProducte[$id];
		}
		reset($mPropietatsSegellProducteConfig);
		
		while(list($id,$mSegellProducte)=each($mPropietatsSegellProducteConfig))
		{
			$propietat=@$_POST['sel_segells_'.$id];
			if(isset($propietat))
			{
				$mPropietatsSegellProducteGuardar[$id]['estat']=$propietat;
			}
		}
		reset($mPropietatsSegellProducteConfig);
		$propietatsSegellProducteChain=makePropietatsSegellProducte($mPropietatsSegellProducteGuardar);
		$valorSegellProducte=getValorSegell($mPropietatsSegellProducteGuardar,array($mProducte['categoria0']=>''));
		$maxValorSegellProducte=getValorMaxSegell($mPropietatsSegellProducteGuardar,array($mProducte['categoria0']=>''));

		$valorSegellPerfil=getValorSegell($mPropietatsSegellPerfil,$mCategoria0perfil);
		
		$valorSegell=($valorSegellProducte+$valorSegellPerfil)*100/($maxValorSegellProducte+$maxValorSegellPerfil);
		if(!db_guardarPropietatsSegellProducte($valorSegell,$propietatsSegellProducteChain,$db))
		{
			$missatgeAlerta="<p class='pAlertaNo4'>Atenci�: No s'ha pogut guardar el segell ECOCIC d'aquest Producte</p>";
		}
		else
		{
			$missatgeAlerta="<p class='pAlertaOk4'>S'ha guardat el segell ECOCIC d'aquest Producte</p>";
		}		
		$opcio='vell';
		$mPropietatsSegellProducte=db_getPropietatsSegellProducte($db);	
	

}

$mPeriodesLocalsInfo=db_getPeriodesLocalsInfo($db);
$mPropietatsPeriodesLocals=db_getPropietatsPeriodesLocals($db);

$numProductesLlista=db_getNumProductesPerfilAllista($mPerfil['id'],$mPars['selLlistaId'],$db);

$mPropietatsSegellProducte=db_getPropietatsSegellProducte($db);	

$parsChain=makeParsChain($mPars);

echo "
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>
<head>
<LINK REL=StyleSheet HREF='css1.css' TYPE='text/css' MEDIA=screen>
<SCRIPT TYPE='text/javascript' src='js_ajuda.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_gestioProductes.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='js_segells.js' CHARSET='ISO-8859-1'></SCRIPT>
<SCRIPT TYPE='text/javascript' src='navs.js' CHARSET='ISO-8859-1'></SCRIPT>
<script type='text/javascript'>
//*v36.2-declaracio
ruta='".$mPars['selRutaSufix']."';
//*v36.2-Declaracio
rutaPeriode='".$mPars['selRutaSufixPeriode']."';
selLlistaId=\"".$mPars['selLlistaId']."\";
nivell='".$mPars['nivell']."';
acord1=";if(isset($mPerfil['acord1'])){echo $mPerfil['acord1'];}else{echo 0;} echo ";
calGuardar=false;
actiu=";if(isset($mProducte['actiu'])){echo $mProducte['actiu'];}else{echo 0;} echo ";
perfilId='".$mPerfil['id']."';
mSegellPerfilGrups=new Array();
";
while(list($propietatId,$mPropietat)=@each($mPropietatsSegellPerfil))
{
	echo "
//grup:
mSegellPerfilGrups[".$propietatId."]=new Array('".(implode("','",$mPropietat['grup']))."');
	";
}
@reset($mPropietatsSegellPerfil);
echo "
mSegellProducteGrups=new Array();
";
while(list($propietatId,$mPropietat)=@each($mPropietatsSegellProducte))
{
	echo "
//grup:
mSegellProducteGrups[".$propietatId."]=new Array('".(implode("','",$mPropietat['grup']))."');
	";
}
@reset($mPropietatsSegellProducte);echo "
</script>


<body   onload=\"\"  bgcolor='".$mColors['body']."'>
";
html_demo('gestioProductes.php?');
html_helpRecipient();
echo "
<table align='center' style='width:80%'  bgcolor='".$mColors['table']."'>
	<tr>
		<td style='width:100%;' align='center'>
		<p style='font-size:20px;'>".$mContinguts['index']['titol0']."<br>
		".$mContinguts['index']['titol1']."</b>
		</td>
	</tr>
</table>

";
$mRutesSufixes=getRutesSufixes($db);
$mPeriodesInfo=db_getPeriodesInfo($db);


echo "
			
<table style='width:80%;' align='center'  bgcolor='".$mColors['table']."'>
	<tr>
		<td style='width:25%;' align='left' valign='top'>
		<a title='Inici: P�gina Gesti� Productes' style='cursor:pointer;' onClick=\"javascript: enviarFpars('gestioProductes.php?opt=inici&plId=".$mPerfil['id']."','_self')\"><img src='imatges/productes_perfilsp.gif' ALT=\"Inici: p�gina gestio productes\" style='cursor:pointer;' onClick=\"javascript: enviarFpars('gestioProductes.php?opt=inici&plId=".$mPerfil['id']."','_self')\">Inici: p�gina gesti� productes</a>
		</td>

		<td style='width:75%;' align='center' valign='bottom'>
		<table width='80%'>
			<tr>
				<td align='left' width='40%' valign='middle'>
				<p>Perfil de productor:&nbsp;".(html_ajuda1('gestioProductes.php',1))."</p>
				<p>Responsable: ".(urldecode($mUsuarisRef[$mPerfil['usuari_id']]['usuari']))."</b>
				<br>
				(".(urldecode($mUsuarisRef[$mPerfil['usuari_id']]['email']))." )
				</p>
				</td>
				
				<td align='center' width='60%' valign='middle'>
				<p style='font-size:30px; color:LightBlue;'><i><b>".(urldecode($mPerfil['projecte']))."</b></i></p>
				</td>

				<td align='center'>
";
if($mPars['selLlistaId']!=0)
{
	if(substr_count($mPropietatsGrup['idsPerfilsActiusLocal'],','.$mPars['selPerfilRef'].',')>0)
	{
		$estatPerfilAllistaLocal="<font style='font-size:10px;' color='green'><b>ACTIU</b></font>";
	}
	else
	{
		$estatPerfilAllistaLocal="<font style='font-size:10px;' color='red'><b>INACTIU</b></font>";
	}
	echo " 
				&nbsp;(".$estatPerfilAllistaLocal.")</p>
	";
}
	echo "
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
";
	if(strlen($missatgeAlerta)>0)
	{
		echo "
<center>
<div style='z-index:0; position:relative; top:0px; visibility:inherit;  width:70%;'>
		";
	}
	else
	{
		echo "
<center>
<div style='z-index:0; position:absolute; top:0px; visibility:hidden;'>
		";
	} 
	echo "
<p style='text-align:center;'>".$missatgeAlerta."</p>
</center>
</div>


<table style='width:80%;' align='center'  bgcolor='white'>
	<tr>
		<td>
		<p>
		Usuari:<b> ".(urldecode($mPars['usuari']))."</b> (nivell: ".$mPars['nivell'].")
		<br>
		<font style='font-size:10px;' >
		Funcions: 
";
if($mUsuari['id']==$mPerfil['usuari_id'])
{
	echo "
			<br>Responsable d'aquest perfil
	";
}
		
if($mUsuari['id']==@$mGrupsRef[$mPars['selLlistaId']]['usuari_id'])
{
	echo "
		<br>Responsable d'aquesta llista local
	";
}

echo "
		</p>
		</td>

		<td valign='middle' style='width:20%;'>
";
if
(
	$mPars['selLlistaId']!=0
	&&
	(
		($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
		||
		$mPars['usuari_id']==@$mGrupsRef[$mPars['grup_id']]['usuari_id']
	)
)
{
	echo "
		<p style='cursor:pointer;' onClick=\"javascript: enviarFpars('nouPerfilProductorLocal.php?sR=".$mPars['selRutaSufix']."&iPed=".$mPerfil['id']."','_blank')\"><u>Editar perfil</u></p>
	";
}
else if($mPars['nivell']=='sadmin'  || $mPars['nivell']=='admin')
{
	echo "
		<p style='cursor:pointer;' onClick=\"javascript: enviarFpars('nouPerfilProductor.php?sR=".$mPars['selRutaSufix']."&iPed=".$mPerfil['id']."','_blank')\"><u>Editar perfil</u></p>
	";
}
else if ($mPars['usuari_id']==$mPerfil['usuari_id'])
{
	echo "
		<p style='cursor:pointer;' onClick=\"javascript: enviarFpars('nouPerfilProductorLocal.php?sR=".$mPars['selRutaSufix']."&iPed=".$mPerfil['id']."','_blank')\"><u>Editar perfil</u></p>
	";
}
		
if($mPars['selLlistaId']==0)
{
	if($numProductesLlista>0)
	{	
		echo "
		<p style='cursor:pointer;' onClick=\"javascript: enviarFpars('gestioProductes.php?opt=aE&plId=".$mPerfil['id']."','_self')\"><u>albar� d'entrega a la CAC</u></p>
		";
	}
	if
	(
		$mPerfil['grup_vinculat']!=0
		&&
		(
			($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'  || $mPars['nivell']=='coord')
			||
			$mPars['usuari_id']==$mPerfil['usuari_id']
		)
	)
	{
		echo "
		<p style='cursor:pointer;' onClick=\"javascript: enviarFpars('comptesGrup.php?plId=".$mPerfil['id']."','_blank')\"><u>Balan� producci�-consum</u></p>
		";
	}
	echo "
		</td>

		
		<td valign='top' align='center' style='width:60%;'>
		<table   width='100%' align='center'>
			<tr>
				<td align='center' valign='top' width='50%'>
		";
		mostrarSelectorLlistaPerProductores('gestioProductes.php','gestioProductes.php',$db);
		echo "
				</td>
				<td align='center' valign='top' width='50%'>
		";
		if
		(
			isset($mPars['selLlistaId']) 
			&& 
			$mPars['selLlistaId']!='' 
		)
		{
			mostrarSelectorRuta(1,'gestioProductes.php');
		}
		echo "
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
	";
}
else
{
	if
	(
		($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'  || $mPars['nivell']=='coord')
		||
		$mPars['usuari_id']==$mPerfil['usuari_id']
		||
		$mPars['usuari_id']==@$mGrupsRef[$mPars['grup_id']]['usuari_id']
		
	)
	{
		if($numProductesLlista>0)
		{
			echo "
		<p style='cursor:pointer;' onClick=\"javascript: enviarFpars('gestioProductes.php?opt=aEg&plId=".$mPerfil['id']."','_self')\"><u>albar� d'entrega al GRUP</u></p>
			";
		}
	
	}

	if(
		($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin'  || $mPars['nivell']=='coord')
		||
		$mPars['usuari_id']==@$mGrupsRef[$mPars['grup_id']]['usuari_id'] //responsable llista
	)
	{
		echo "
		<p style='cursor:pointer;' onClick=\"javascript: enviarFpars('comptesGrupLocal.php?llId=".$mPars['selLlistaId']."','_blank')\"><u>Balan� producci�-consum</u></p>
		";
	}
	else if
	(
		$mPars['usuari_id']==$mPerfil['usuari_id']
	)
	{
		echo "
		<p style='cursor:pointer;' onClick=\"javascript: enviarFpars('comptesGrupLocal.php?plId=".$mPerfil['id']."','_blank')\"><u>Balan� producci�-consum</u></p>
		";
	}

	echo "
		</td>

		
		<td valign='bottom' align='center' style='width:60%;'>
		<table width='100%' align='center' >
			<tr>
				<td align='center' valign='top' width='50%'>
		";
		mostrarSelectorLlistaPerProductores('gestioProductes.php','gestioProductes.php',$db);
		echo "
				</td>
				<td align='center' valign='top' width='50%'>
		";
		if(isset($mPars['selLlistaId']) && $mPars['selLlistaId']!='' && $numProductesLlista>0)
		{
			mostrarSelectorPeriodeLocalInfo(1);
		}
		echo "
				</td>
			</tr>
		</table>
		</td>
		</td>
	</tr>
</table>
	"; 
}


if
(
	isset($mPars['selLlistaId']) 
	&& 
	$mPars['selLlistaId']!='' 
	&&
	isset($mPerfil['id'])
	&&
	$mPerfil['id']!=''
)
{
	echo "
<table width='80%' align='center'  bgcolor='white'>
	<tr>
		<td style='width:100%;' align='center'>
	";
	if($mPars['selLlistaId']==0)
	{
		
		if($mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m'),date('d'),date('Y'))))
		{
			echo "
		<center><p style='color:DarkOrange; size:10px;'> Atenci�: Cal que feu l'entrega dels vostres <b>productes 'dip�sit'</b> reservats abans del dia <b>".$mParametres['iniciRutaAbastiment']['valor']."</b></p></center>
			";
		}
		else
		{
			if(isset($mParametres['iniciRutaAbastiment']['valor']))
			{
				echo "
		<center><p style='color:DarkOrange; size:10px;'> Ruta Abastiment: dia <b>".$mParametres['iniciRutaAbastiment']['valor']."</b> (entrega dels vostres productes reservats de tipus 'dip�sit')</p></center>
				";
			}
		}
	}
	echo "
		</td>
	</tr>
</table>
	";

		if($opt!='aE' & $opt!='aEg')
		{
			html_menuProductes();
		}


		//editar i guardar producte
		// llista CAC
		if($mPars['selLlistaId']==0)
		{
			html_fitxaProducte($db);
		}
		else
		{
			html_fitxaProducteLocal($db);
		}

		$mProductesJaEntregats=db_getProductesJaEntregats2($db);
		$mProductesJaEntregatsTot=getProductesJaEntregatsTOT($mProductesJaEntregats);


		if($opt=='aE')
		{
			html_resumComandaCac($db);
			html_notesAproductors();
		}
		else if($opt=='aEg')
		{
			html_resumComandaGrup($db);
			html_notesAproductorsGrup();
		}
}

	if
	(
		isset($mProducte['id'])
		&&
		$mProducte['id']!=''
		&&
		$mProducte['id']!=0
		&&
		(
			$mUsuari['id']==$mPerfil['usuari_id']
			||	
			($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
		)
	)	
	{
		html_segellEcoCicProducte('form','gestioProductes.php');
	}
	else{echo "<div id='t_segellEcocicProducte'></div>";}
	
	if($mPars['selLlistaId']==0)
	{
		html_activacioRapida($db);
	}
	else
	{
		html_activacioRapidaLocal($db);
	}

	if
	(
		($mPars['nivell']=='sadmin'  || $mPars['nivell']=='admin')
		|| 
		(
			$mPars['perfil_id']!=0
			&&
			$mPerfil['usuari_id']==$mPars['usuari_id'] //responsable del grup/llista
		)
	)
	{
		html_associarPerfilAgrup();
		html_transferirFitxes($db);
	}
	else
	{
		echo "<div id='t_associarPerfilAgrup'></div>";
		echo "<div id='t_transferirFitxes'></div>";
	}
	
	
	if
	(
		($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')
		||
		$mPars['nivell']=='admin'
		||
		$mPars['nivell']=='coord'
		||
		(
			isset($mPars['grup_id'])
			&&
			$mPars['grup_id']!=0
			&&
			$mPars['grup_id']!=''
			&&
			$mGrupsRef[$mPars['grup_id']]['usuari_id']==$mPars['usuari_id'] //responsable del grup/llista
		)
		||
		(
			$mPars['selLlistaId']!=0
			&&
			$mPerfil['usuari_id']==$mPars['usuari_id']
		)
	)
	{
		html_peticioNovesPlantilles();
	}
	else
	{
		echo "<div id='t_peticioNovesPlantilles'></div>";
	}

	if
	(
		$opt=='vell'
		&&
		(
			(
				$mPars['selLlistaId']==0
				&&
				(
					(
						$mPars['selRutaSufixPeriode']==date('ym')
						&&
						$modificarValorsImportantsProductes
					)
					||
					$mPars['selRutaSufixPeriode']==date('ym',mktime(0,0,0,date('m')+1,date('d'),date('Y')))
				)
				&&
				$mUsuari['id']==$mPerfil['usuari_id']
			)
			||
			(
				$mPars['selLlistaId']!=0
				&&
				$mUsuari['id']==$mPerfil['usuari_id']
			)
		)
		||
		($mPars['nivell']=='sadmin' || $mPars['nivell']=='admin')

	)
	{
		html_pujarImatge();
	}
	else
	{
		echo "<div id='t_pujarImatge'></div>";
	}



echo "
		<div id='d_mostrarForms' style='z-index:0; top:0px; position:absolute; visibility:hidden;'>
		</div>

<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
&nbsp;

<div style='position:absolute; z-index:1; top:0px; left:0px; visibility:hidden;'>
<form id='f_pars' name='f_pars' method='post' action='gestioProductes.php' target='_self'>
<input type='hidden' id='i_pars' name='i_pars' value='".$parsChain."'>
<input type='hidden' id='i_selLlista' name='i_selLlista' value='".$mPars['selLlistaId']."'>
<input type='hidden' id='i_selRuta' name='i_selRuta' value='".$mPars['selRutaSufix']."'>
<input type='hidden' id='i_comentari' name='i_comentari' value=''>
<input type='hidden' id='i_ordreAr' name='i_ordreAr' value='".$mPars['ordreAr']."'>
</form>
</div>



</body>
</html>
";

?>




		